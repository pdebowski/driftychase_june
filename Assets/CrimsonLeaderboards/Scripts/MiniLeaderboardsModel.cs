﻿using UnityEngine;
using System.Collections;
using CrimsonLeaderboards;

public class MiniLeaderboardsModel
{
	private LeaderboardEntry playerEntry;
	private LeaderboardEntry opponentEntry;

	public LeaderboardEntry PlayerEntry { get { return playerEntry; } }
	public LeaderboardEntry OpponentEntry { get { return opponentEntry; } }

    public bool NewEntriesCanBeObtained()
	{
        return LeaderboardsManager.MiniLeaderboardsDataProvider.GetPlayerLeaderboardEntry() != null
            && LeaderboardsManager.MiniLeaderboardsDataProvider.GetOpponentLeaderboardEntry() != null;
	}

	public void ObtainNewEntries()
	{
		if (NewEntriesCanBeObtained())
		{
			playerEntry = new LeaderboardEntry(LeaderboardsManager.MiniLeaderboardsDataProvider.GetPlayerLeaderboardEntry());
			opponentEntry = new LeaderboardEntry(LeaderboardsManager.MiniLeaderboardsDataProvider.GetOpponentLeaderboardEntry());
		}
	}

    public int GetCurrentRecord()
    {
        return Mathf.Max(ScoreCounter.Instance.CurrentScore, PlayerEntry != null ? PlayerEntry.score : 0);
    }

    public bool IsBeatedOpponent()
    {
        return SavedEntriesNotNull() && GetCurrentRecord() > OpponentEntry.score && PlayerEntry.position > OpponentEntry.position;
    }

    public bool SavedEntriesNotNull()
    {
        return PlayerEntry != null && OpponentEntry != null;
    }
}
