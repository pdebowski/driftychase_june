﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace CrimsonLeaderboards
{
    public class LeaderboardsManager : MonoBehaviour, LeaderboardsRefresher, LeaderboardsDataContainerProvider, LeaderboardsUserManager, LeaderboardsPendingRewardsManager, MiniLeaderboardsDataProvider
    {
        [SerializeField]
        private string salt;
        [SerializeField]
        private LeaderboardType[] leaderboardTypes;

        private string hostname { get { return GTMParameters.CustomLeaderboardsURL; } }

        private LeaderboardAPI leaderboardAPI;
        private List<LeaderboardController> leaderboardControllers;
        private bool isError = false;

        public static LeaderboardsRefresher LeaderboardRefresher;
        public static LeaderboardsDataContainerProvider LeaderboardDataContainerProvider;
        public static LeaderboardsUserManager LeaderboardUserManager;
        public static LeaderboardsPendingRewardsManager LeaderboardPendingRewardsManager;
        public static MiniLeaderboardsDataProvider MiniLeaderboardsDataProvider;

        void Awake()
        {
            if (GTMParameters.CustomLeaderboardsEnabled)
            {
                LeaderboardRefresher = this;
                LeaderboardDataContainerProvider = this;
                LeaderboardUserManager = this;
                LeaderboardPendingRewardsManager = this;
                MiniLeaderboardsDataProvider = this;

                leaderboardAPI = GetComponent<LeaderboardAPI>();
                HttpExecutor httpExecutor = GetComponent<UnityHttpExecutor>();
                httpExecutor.Init(OnError, OnSuccess);
                leaderboardAPI.Init(httpExecutor, hostname, salt);

                CreateLeaderboardControllers();
                Initialize();
            }
            else
            {
                Destroy(gameObject);
            }
        }

        private void Update()
        {
            leaderboardControllers.ForEach(x => x.JoinNewLeaderboardIfNeededAndPrepareReward());
        }

        //----INTERFACES_METHODS------------------------------------------------------------------------------------//

        //----LeaderboardRefresher------------------------------------//

        public void Initialize()
        {
            if (!IsPlayerCreated)
            {
                CreateUser();
            }
            else
            {
                RefreshLeaderboard();
            }
        }

        public bool IsError()
        {
            return isError;
        }

        public void ReportScore(int score)
        {
            Debug.Log(string.Format("ReportScore (Score:{0})", score));

            foreach (LeaderboardController leaderboard in leaderboardControllers)
            {
                leaderboard.ReportScore(Mathf.Max(score, leaderboard.NotSavedValueInLeaderboard));
            }
        }

        public void RefreshLeaderboard()
        {
            Debug.Log("RefreshLeaderboard");

            foreach (LeaderboardController leaderboard in leaderboardControllers)
            {
                leaderboard.RefreshLeaderboard();
            }
        }

        //----LeaderboardDataContainerProvider--------------------------//

        public LeaderboardDataContainer GetLeaderboardDataContainer(LeaderboardType leaderboardType)
        {
            return leaderboardControllers.Find(x => x.GetLeaderboardType() == leaderboardType) as LeaderboardDataContainer;
        }

        public bool DoesIDBelongsToPlayer(string id)
        {
            return id == UserID;
        }

        //----LeaderboardUserManager------------------------------------//

        public string GetPlayerName()
        {
            return PlayerName;
        }

        public void ChangeLocalName(string newName)
        {
            PlayerName = newName;
        }

        public void ChangeName(string newName, System.Action<bool> changeNameCallback)
        {
            Debug.Log(string.Format("ChangeName (Name:{0})", newName));

            leaderboardAPI.UpdateUser(UserID, newName, res =>
            {
                RefreshLeaderboard();
                changeNameCallback(res.isSuccess);

                if (res.isSuccess)
                {
                    PlayerName = newName;
                }
            }
            );
        }

        //----LeaderboardPendingRewardsManager------------------------------------//

        #region GetPendingRewards TEST
        //public List<PendingReward> GetPendingRewards()
        //{
        //    List<PendingReward> pendingRewards = new List<PendingReward>();

        //    foreach (LeaderboardController leaderboard in leaderboardControllers)
        //    {
        //        PendingReward reward = new PendingReward(UserID, "xxx", Random.Range(10, 50), leaderboard.GetLeaderboardType(), leaderboardAPI);
        //        pendingRewards.Add(reward);
        //    }

        //    return pendingRewards;
        //}
        #endregion

        public List<PendingReward> GetPendingRewards()
        {
            List<PendingReward> pendingRewards = new List<PendingReward>();

            foreach (LeaderboardController leaderboard in leaderboardControllers)
            {
                PendingReward reward = leaderboard.GetPendingReward;
                if (reward != null)
                {
                    pendingRewards.Add(reward);
                }
            }

            return pendingRewards;
        }

        //----MiniLeaderboardsDataProvider------------------------------------//

        public LeaderboardEntry GetPlayerLeaderboardEntry()
        {
            LeaderboardController dailyLeaderboard = leaderboardControllers.Find(x => x.GetLeaderboardType() == LeaderboardType.daily);
            if (dailyLeaderboard != null)
            {
                return dailyLeaderboard.GetPlayerLeaderboardEntry();
            }
            else
            {
                return null;
            }
        }
        public LeaderboardEntry GetOpponentLeaderboardEntry()
        {
            LeaderboardController dailyLeaderboard = leaderboardControllers.Find(x => x.GetLeaderboardType() == LeaderboardType.daily);
            if (dailyLeaderboard != null)
            {
                return dailyLeaderboard.GetOpponentLeaderboardEntry();
            }
            else
            {
                return null;
            }
        }

        //---------------------------------------------------------------------------------------------------------------//

        //-----PRIVATE----------------------------------//

        private void CreateLeaderboardControllers()
        {
            leaderboardControllers = new List<LeaderboardController>();

            LeaderboardType noUserFoundResponsibleType = leaderboardTypes.First();

            foreach (LeaderboardType leaderboardType in leaderboardTypes)
            {
                System.Action OnNoUserFoundCallback = null;

                if(leaderboardType == noUserFoundResponsibleType)
                {
                    OnNoUserFoundCallback = OnNoUserFoundError;
                }

                LeaderboardController leaderboard = new LeaderboardController(leaderboardType, leaderboardAPI, UserID, OnNoUserFoundCallback);
                leaderboardControllers.Add(leaderboard);
            }
        }

        private void CreateUser()
        {
            string nickName = CommonMethods.GetRandomNickname();

            Debug.Log(string.Format("CreateUser (Name:{0})", nickName));

            leaderboardAPI.CreateUser(UserID, nickName, res =>
            {
                if (res.isSuccess || res.responseStatus == ResponseStatus.USER_ALREADY_EXISTS)
                {
                    IsPlayerCreated = true;
                }

                RefreshLeaderboard();
            }
            );
        }

        private void OnNoUserFoundError()
        {
            Debug.Log("OnNoUserFoundError");
            CreateUser();
        }

        private void OnError()
        {
            Debug.Log("OnError");
            isError = true;
        }

        private void OnSuccess()
        {
            isError = false;
        }

        //--PLAYER_PREFS--------------------------------------------------------------//

        private const string IS_PLAYER_CREATED = "CUSTOM_LEADERBOARD_IS_PLAYER_CREATED";
        private const string PLAYER_NAME_KEY = "CUSTOM_LEADERBOARD_PLAYER_NAME";

        private bool IsPlayerCreated
        {
            get
            {
                return System.Convert.ToBoolean(PlayerPrefs.GetInt(IS_PLAYER_CREATED, 0));
            }
            set
            {
                PlayerPrefs.SetInt(IS_PLAYER_CREATED, System.Convert.ToInt32(value));
            }
        }

        private string PlayerName
        {
            get
            {
                return PlayerPrefs.GetString(PLAYER_NAME_KEY, "");
            }
            set
            {
                PlayerPrefs.SetString(PLAYER_NAME_KEY, value);
            }
        }

        private string UserID
        {
            get
            {
#if UNITY_EDITOR
                return SystemInfo.deviceUniqueIdentifier;
#elif UNITY_ANDROID
	        return DeviceIdentifier.generateDeviceUniqueIdentifier(false);
#elif UNITY_IOS
            return SystemInfo.deviceUniqueIdentifier;
#endif
            }
        }

    }

    //--DEFINITIONS-----------------------------------------------------------------//

    public interface LeaderboardsUserManager
    {
        string GetPlayerName();
        void ChangeName(string newName, System.Action<bool> changeNameCallback);
        void ChangeLocalName(string newName);
    }

    public interface LeaderboardsPendingRewardsManager
    {
        List<PendingReward> GetPendingRewards();
    }

    public interface LeaderboardsRefresher
    {
        void Initialize();
        bool IsError();
        void ReportScore(int score);
        void RefreshLeaderboard();
    }

    public interface LeaderboardsDataContainerProvider
    {
        LeaderboardDataContainer GetLeaderboardDataContainer(LeaderboardType leaderboardType);
        bool DoesIDBelongsToPlayer(string id);
    }

    public interface MiniLeaderboardsDataProvider
    {
        LeaderboardEntry GetPlayerLeaderboardEntry();
        LeaderboardEntry GetOpponentLeaderboardEntry();
    }

    public enum LeaderboardType
    {
        daily,
        weekly
    }
}