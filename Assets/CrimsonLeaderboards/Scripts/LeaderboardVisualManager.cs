﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

namespace CrimsonLeaderboards.UI
{
    public class LeaderboardVisualManager : MonoBehaviour
    {
        [SerializeField]
        private LeaderboardType leaderboardType;
        [SerializeField]
        private GameObject contentScreen;
        [SerializeField]
        private GameObject loadingScreen;
        [SerializeField]
        private GameObject errorScreen;
        [SerializeField]
        private OfflineModeVisuals offlineModeVisuals;
        [SerializeField]
        private Text timer;
        [SerializeField]
        private RewardClaimer rewardClaimer;
        [SerializeField]
        private LeaderboardEntriesManager entriesManager;

        private bool initialized = false;
        private LeaderboardDataContainer leaderboard;
        private const float TIMEOUT = 1.5f;
        private GameObject activeScreen;
        private Coroutine loadingScreenCor;

        private void OnEnable()
        {
            if (GTMParameters.CustomLeaderboardsEnabled)
            {
                if (!initialized)
                {
                    initialized = true;
                    Initialize();
                }

                leaderboard.RegisterToDataChange(RefreshVisuals);
            }
            else
            {
                Destroy(gameObject);
            }
        }

        private void Initialize()
        {
            leaderboard = LeaderboardsManager.LeaderboardDataContainerProvider.GetLeaderboardDataContainer(leaderboardType);
            entriesManager.Init(leaderboard, leaderboardType);
            offlineModeVisuals.Init(RetryConnection);
            ShowScreen(contentScreen);
            RefreshVisuals();
        }

        private void OnDisable()
        {
            if (leaderboard != null)
            {
                leaderboard.UnregisterFromDataChange(RefreshVisuals);
            }
        }

        private void Update()
        {
            long timeLeft = leaderboard.GetLeaderboardTimeLeft();

            if (timeLeft >= 0)
            {
                SetTime(timeLeft / 1000);
            }
            else
            {
                SetTime(0);
            }
        }

        //--BUTTONS_AND_EVENTS----------------------------//

        public void RetryWhenError()
        {
            RetryConnection();
            ShowLoadingScreen();
        }

        public void RetryWhenOfflineMode()
        {
            LeaderboardsManager.LeaderboardRefresher.RefreshLeaderboard();
        }

        public void OnCardOpened()
        {
            entriesManager.CenterPlayer();
        }

        //----------------------------------------//

        private void RefreshVisuals()
        {
            if (leaderboard.AreLeaderboardsLoaded())
            {
                RefreshLeaderboardData();
                ShowScreen(contentScreen);
                offlineModeVisuals.SetOfflineMode(LeaderboardsManager.LeaderboardRefresher.IsError());
            }
            else
            {
                if (LeaderboardsManager.LeaderboardRefresher.IsError())
                {
                    ShowScreen(errorScreen);
                }
                else
                {
                    ShowLoadingScreen();
                }

            }
        }

        private void RetryConnection()
        {
            LeaderboardsManager.LeaderboardRefresher.Initialize();
        }

        private void RefreshLeaderboardData()
        {
            entriesManager.RefreshLeaderboardData();
        }

        private void ShowLoadingScreen()
        {
            if (loadingScreenCor == null)
            {
                ShowScreen(loadingScreen);
                loadingScreenCor = StartCoroutine(LoadingCor());
            }
        }

        private IEnumerator LoadingCor()
        {
            yield return new WaitForSeconds(TIMEOUT);
            RefreshVisuals();
            loadingScreenCor = null;
        }

        private void ShowScreen(GameObject screenToShow)
        {
            if (screenToShow != null && screenToShow != activeScreen)
            {
                if (activeScreen != null)
                {
                    activeScreen.SetActive(false);
                }
                screenToShow.SetActive(true);
                activeScreen = screenToShow;
            }
        }

        private void SetTime(double time)
        {
            int secondPerDay = 24 * 60 * 60;
            string stringToShow = "";
            int days = (int)(time / secondPerDay);

            if (days > 1)
            {
                stringToShow = days + " DAYS ";
            }
            else if (days == 1)
            {
                stringToShow = days + " DAY ";
            }

            TimeSpan result = TimeSpan.FromSeconds((int)(time - (days * secondPerDay)));
            stringToShow += result.ToString();

            timer.text = "ENDS IN " + stringToShow;
        }

    }
}