﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using System.Text;

namespace CrimsonLeaderboards.UI
{
    public class ChangeNamePopup : MonoBehaviour
    {
        [SerializeField]
        private InputField inputField;
        [SerializeField]
        private GameObject loadingScreen;

        private LeaderboardsUserManager leaderboardUserManager;
        private Action leaderboardsRefresh;

        void OnEnable()
        {
            leaderboardUserManager = LeaderboardsManager.LeaderboardUserManager;
            inputField.text = leaderboardUserManager.GetPlayerName();
        }

        public void OnInputFieldValueChanged()
        {
            inputField.text = RemoveNotAllowedCharacters(inputField.text);
        }

        public void OnButtonChangeNameClicked()
        {
            if (IsNameValid(inputField.text))
            {
                leaderboardUserManager.ChangeName(inputField.text, res =>
                    {
                        OnSetPlayerDisplayNameResult(res);
                    }
                );

                loadingScreen.SetActive(true);
            }
            else
            {
                inputField.text = "INVALID_NAME";
            }
        }

        public void OnBackButtonClicked()
        {
            gameObject.SetActive(false);
        }

        public void OnChangeNamePopupShowClicked()
        {
            gameObject.SetActive(true);
        }

        private void OnSetPlayerDisplayNameResult(bool result)
        {
            if (result)
            {
                gameObject.SetActive(false);
                loadingScreen.SetActive(false);
            }
            else
            {
                inputField.text = "NO_CONNECTION";
                loadingScreen.SetActive(false);
            }
        }

        private string RemoveNotAllowedCharacters(string s)
        {
            string allowed = "ABCDEFGHIJKLMNOPQRSTUVWXYZ" + "ABCDEFGHIJKLMNOPQRSTUVWXYZ".ToLower() + "0123456789" + " -|*^@&";
            char[] name = s.ToCharArray();

            StringBuilder builder = new StringBuilder();

            for (int i = 0; i < name.Length; i++)
            {
                if (allowed.Contains(name[i].ToString()))
                {
                    builder.Append(name[i]);
                }
            }

            return builder.ToString();
        }

        private bool IsNameValid(string nameString)
        {
            return nameString.Length >= 3 && nameString.Length <= 16;
        }

    }
}