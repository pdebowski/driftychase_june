﻿using UnityEngine;

namespace CrimsonLeaderboards.UI
{
    public class OfflineModeVisuals : MonoBehaviour
    {
        [SerializeField]
        private GameObject holder;
        [SerializeField]
        private GameObject loadingObject;
        [SerializeField]
        private GameObject retryObject;

        private System.Action retryConnection;

        public void Init(System.Action retryConnection)
        {
            this.retryConnection = retryConnection;
            SetOfflineMode(false);
            SetShowRetry(true);
        } 

        public void SetOfflineMode(bool value)
        {
            holder.SetActive(value);
            SetShowRetry(true);
        }

        public void OnRetryButtonClicked()
        {
            retryConnection();
            SetShowRetry(false);
        }

        private void SetShowRetry(bool value)
        {
            retryObject.SetActive(value);
            loadingObject.SetActive(!value);
        }
    }
}