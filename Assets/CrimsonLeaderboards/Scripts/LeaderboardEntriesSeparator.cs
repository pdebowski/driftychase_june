﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace CrimsonLeaderboards.UI
{
    public class LeaderboardEntriesSeparator : MonoBehaviour
    {
        [SerializeField]
        private Text reward;

        public void SetRewardText(string text)
        {
            reward.text = text;
        }
    }
}