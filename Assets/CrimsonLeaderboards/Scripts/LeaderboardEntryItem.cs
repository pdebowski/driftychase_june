﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace CrimsonLeaderboards.UI
{
    public class LeaderboardEntryItem : MonoBehaviour
    {

        [SerializeField]
        private Text positionAndNickText;
        [SerializeField]
        private Text scoreText;
        [SerializeField]
        private Color normalColor;
        [SerializeField]
        private Color highlightedColor;
        [SerializeField]
        private int positionSize = 75;
        [SerializeField]
        private int nickSize = 85;

        private bool isLocalUser = false;

        public void SetValues(int position, string displayName, int score, bool isLocalUser)
        {
            positionAndNickText.text = string.Format("<size={2}>{0}.</size> <size={3}>{1}</size>", FormatPosition(position), displayName, positionSize, nickSize);
            scoreText.text = score.ToString();
            this.isLocalUser = isLocalUser;

            SetTextsColor(isLocalUser ? highlightedColor : normalColor);
        }

        public bool IsLocalUser()
        {
            return isLocalUser;
        }

        public void Clear()
        {
            positionAndNickText.text = "";
            scoreText.text = "";
        }

        private void SetTextsColor(Color color)
        {
            foreach (var text in GetComponentsInChildren<Text>())
            {
                text.color = color;
            }
        }

        private string FormatPosition(int position)
        {
            if (position < 1000)
            {
                return position.ToString();
            }
            else if (position < 1000000)
            {
                return (position / 1000).ToString() + "K+";
            }
            else
            {
                return (position / 1000000).ToString() + "M+";
            }
        }
    }
}