﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UnityHttpExecutor : MonoBehaviour, HttpExecutor
{
    private HttpRequest httpRequest;
    private const int TIMEOUT = 3;
    HttpRequestStatus status;

    private System.Action OnError = delegate { };
    private System.Action OnSuccess = delegate { };

    public void Init(System.Action OnError, System.Action OnSuccess)
    {
        this.OnError += OnError;
        this.OnSuccess = OnSuccess;
    }

    void Start()
    {
        httpRequest = GetComponent<HttpRequest>();
    }

    public void sendRequest(string hostname, Dictionary<string, string> variables)
    {
        status = HttpRequestStatus.WAITING;

        WWWForm form = new WWWForm();
        foreach (KeyValuePair<string, string> entry in variables)
        {
            form.AddField(entry.Key, entry.Value);
        }

        httpRequest.SendRequest(hostname, TIMEOUT, form, () =>
            {
                if (httpRequest.p_error == HttpRequest.RequestError.NONE)
                {
                    status = HttpRequestStatus.OK;
                    OnSuccess();
                }
                else
                {
                    switch (httpRequest.p_error)
                    {
                        case HttpRequest.RequestError.EMPTY_RESPONSE:
                            status = HttpRequestStatus.ERROR_EMPTY_RESPONSE;
                            break;
                        case HttpRequest.RequestError.TIMEOUT:
                            status = HttpRequestStatus.ERROR_TIMEOUT;
                            break;
                        case HttpRequest.RequestError.OTHER:
                            status = HttpRequestStatus.ERROR_OTHER;
                            break;
                    }

                    OnError();
                }
            }
        );
    }

    public bool isReady()
    {
        return status != HttpRequestStatus.WAITING;
    }

    public HttpRequestStatus getStatus()
    {
        return status;
    }

    public string getResponse()
    {
        return httpRequest.p_fileText;
    }
}
