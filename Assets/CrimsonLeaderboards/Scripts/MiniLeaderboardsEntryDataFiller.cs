﻿using UnityEngine;
using System.Collections;
using CrimsonLeaderboards;
using CrimsonLeaderboards.UI;

public class MiniLeaderboardsEntryDataFiller
{
    private LeaderboardEntryItem firstItem;
    private LeaderboardEntryItem secondItem;
    private MiniLeaderboardsModel model;

    public MiniLeaderboardsEntryDataFiller(LeaderboardEntryItem firstItem, LeaderboardEntryItem secondItem)
    {
        this.firstItem = firstItem;
        this.secondItem = secondItem;
        model = new MiniLeaderboardsModel();
    }

    public bool CanShowOnBeggining()
    {
        return model.NewEntriesCanBeObtained();
    }

    public bool CanShowOnEnd()
    {
        return model.SavedEntriesNotNull() && model.IsBeatedOpponent();
    }

    public void FillLeaderboardEntryItems(bool afterSession, bool updatePosition = false)
    {
        if (afterSession)
        {
            if (CanShowOnEnd())
            {
                LeaderboardEntry myEntry = model.PlayerEntry;
                LeaderboardEntry opponentEntry = model.OpponentEntry;

                myEntry.SetScore(model.GetCurrentRecord());

                if (model.IsBeatedOpponent() && updatePosition)
                {
                    myEntry.SetPosition(myEntry.position - 1);
                    opponentEntry.SetPosition(opponentEntry.position + 1);
                }

                SetPlayerLeaderboardToItem(firstItem, opponentEntry, false);
                SetPlayerLeaderboardToItem(secondItem, myEntry, true);
            }
        }
        else
        {
            if (CanShowOnBeggining())
            {
                model.ObtainNewEntries();

                LeaderboardEntry myEntry = model.PlayerEntry;
                LeaderboardEntry opponentEntry = model.OpponentEntry;

                if (myEntry.position > opponentEntry.position)
                {
                    SetPlayerLeaderboardToItem(firstItem, opponentEntry, false);
                    SetPlayerLeaderboardToItem(secondItem, myEntry, true);
                }
                else
                {
                    SetPlayerLeaderboardToItem(firstItem, myEntry, true);
                    SetPlayerLeaderboardToItem(secondItem, opponentEntry, false);
                }
            }
        }
    }

    private void SetPlayerLeaderboardToItem(LeaderboardEntryItem item, LeaderboardEntry player, bool isLocalPlayer)
    {
        item.SetValues(player.position, player.name, player.score, isLocalPlayer);
    }
}
