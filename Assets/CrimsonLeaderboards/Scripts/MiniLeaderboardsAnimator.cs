﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using CrimsonLeaderboards.UI;

public class MiniLeaderboardsAnimator : MonoBehaviour
{
    [SerializeField]
    private LeaderboardEntryItem firstItem;
    [SerializeField]
    private LeaderboardEntryItem secondItem;
    [SerializeField]
    private Transform[] begginingAnims;
    [SerializeField]
    private Transform[] endingAnims;
    [SerializeField]
    private BaseAnimation uiAnimation;

    private bool wasOnEndShowed = false;

    private MiniLeaderboardsEntryDataFiller dataFiller;

    void Awake()
    {
        dataFiller = new MiniLeaderboardsEntryDataFiller(firstItem, secondItem);
        GameplayManager.OnSessionEnded += ResetWasOnEndShowed;
        SelectHorizontalAnims(begginingAnims);
        gameObject.SetActive(false);
    }

    void OnDestroy()
    {
        GameplayManager.OnSessionEnded -= ResetWasOnEndShowed;
    }

    public bool CanShowOnBeggining()
    {
        return dataFiller.CanShowOnBeggining();
    }

    public bool CanShowOnEnd()
    {
        return dataFiller.CanShowOnEnd();
    }

    public void ShowOnBeggining()
    {
        uiAnimation.OnAnimationEnd += OnShowOnBegginingAnimtionEnded;
        gameObject.SetActive(true);
        SelectHorizontalAnims(begginingAnims);
        dataFiller.FillLeaderboardEntryItems(false);
    }

    public void ShowOnEnd()
    {
        if(!wasOnEndShowed)
        {
            wasOnEndShowed = true;
            GameplayManager.OnContinueResultScreenLoading += OnShowOnEndAnimationEnded;
            gameObject.SetActive(true);
            SelectHorizontalAnims(endingAnims);
            SetVerticalAnimsEnabled(true);
            dataFiller.FillLeaderboardEntryItems(true, false);
            StartCoroutine(UpdatePosInHalfAnimCor());
        }
    }

    private IEnumerator UpdatePosInHalfAnimCor()
    {
        PositionAnimation posAnimation = firstItem.GetComponent<PositionAnimation>();
        yield return new WaitForSeconds(posAnimation.delay + posAnimation.duration / 2);
        dataFiller.FillLeaderboardEntryItems(true, true);
    }

    private void OnShowOnEndAnimationEnded(ResultSceneType resultSceneType)
    {
        GameplayManager.OnContinueResultScreenLoading -= OnShowOnEndAnimationEnded;
        OnAnimationEnd();
        SetVerticalAnimsEnabled(false);
    }

    private void OnShowOnBegginingAnimtionEnded()
    {
        uiAnimation.OnAnimationEnd -= OnShowOnBegginingAnimtionEnded;
        OnAnimationEnd();
    }

    private void OnAnimationEnd()
    {
        gameObject.SetActive(false);
    }

    private void ResetWasOnEndShowed()
    {
        wasOnEndShowed = false;
    }

    private void SelectHorizontalAnims(Transform[] anims)
    {
        firstItem.transform.SetParent(anims[0]);
        secondItem.transform.SetParent(anims[1]);

        foreach (var anim in anims)
        {
            anim.GetComponent<CanvasPositionAnimation>().StartAnimation();
        }
    }

    private void SetVerticalAnimsEnabled(bool enabled)
    {
        PositionAnimation firstAnimation = firstItem.GetComponent<PositionAnimation>();
        PositionAnimation secondAnimation = secondItem.GetComponent<PositionAnimation>();
        firstAnimation.enabled = enabled;
        secondAnimation.enabled = enabled;

        if(enabled)
        {
            firstAnimation.StartAnimation();
            secondAnimation.StartAnimation();
        }
        else
        {
            firstAnimation.StopAnimation();
            secondAnimation.StopAnimation();
        }
    }
}
