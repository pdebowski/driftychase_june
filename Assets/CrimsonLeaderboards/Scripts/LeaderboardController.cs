﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace CrimsonLeaderboards
{
    public class LeaderboardController : LeaderboardDataContainer
    {
        private LeaderboardType leaderboardType;
        private LeaderboardAPI leaderboardAPI;
        private string playerID;

        private bool isRefreshingLeaderboard = false;
        private bool isJoiningNewLeaderboard = false;
        private bool isReportingScore = false;
        private LeaderboardInfo leaderboardInfo;
        private PendingReward pendingReward;
        private System.Action OnDataChanged = delegate { };
        private float lastTimeReportedScore = 0;
        private System.Action OnNoUserFound;


        public LeaderboardController(LeaderboardType leaderboardType, LeaderboardAPI leaderboardAPI, string userID, System.Action OnNoUserFound)
        {
            this.leaderboardType = leaderboardType;
            this.leaderboardAPI = leaderboardAPI;
            this.playerID = userID;
            this.OnNoUserFound = OnNoUserFound;
        }

        public void ReportScore(int score)
        {
            if (!isReportingScore)
            {
                if (score > LeaderboardReportedScore)
                {
                    lastTimeReportedScore = Time.time;

                    Debug.Log(string.Format("ReportScore (Type:{0}, ID:{1} Score:{2})", leaderboardType, CurrentLeaderboardID, score));
                    isReportingScore = true;

                    leaderboardAPI.ReportScore(CurrentLeaderboardID, playerID, score, res =>
                        {
                            if (res.isSuccess)
                            {
                                LeaderboardReportedScore = score;
                                NotSavedValueInLeaderboard = 0;
                            }
                            else
                            {
                                NotSavedValueInLeaderboard = score;
                            }

                            OnLeaderboardRefreshFinished(res);
                            isReportingScore = false;
                        }
                    );
                }
                else if (Time.time > lastTimeReportedScore + GTMParameters.CustomLeaderboardsForceRefreshTime)
                {
                    Debug.Log(string.Format("RefreshLeaderboard Forced (Type:{0}, ID:{1})", leaderboardType, CurrentLeaderboardID));
                    RefreshLeaderboard();
                }
            }
        }

        public void RefreshLeaderboard()
        {
            if (!isRefreshingLeaderboard)
            {
                lastTimeReportedScore = Time.time;

                if (CurrentLeaderboardID.Length == 0)
                {
                    JoinNewLeaderboard();
                }
                else
                {
                    Debug.Log(string.Format("RefreshLeaderboard (Type:{0}, ID:{1})", leaderboardType, CurrentLeaderboardID));
                    isRefreshingLeaderboard = true;

                    leaderboardAPI.ReadLeaderboard(playerID, CurrentLeaderboardID, res =>
                    {
                        OnLeaderboardRefreshFinished(res);
                        isRefreshingLeaderboard = false;
                    }
                    );
                }
            }
        }

        public LeaderboardType GetLeaderboardType()
        {
            return leaderboardType;
        }

        public PendingReward GetPendingReward
        {
            get
            {
                if (pendingReward != null && !pendingReward.IsClaimed() && !pendingReward.IsClaiming())
                {
                    return pendingReward as PendingReward;
                }
                else
                {
                    return null;
                }
            }
        }

        public void JoinNewLeaderboardIfNeededAndPrepareReward()
        {
            if (leaderboardInfo != null && leaderboardInfo.GetTimeLeft() < 0 && !isJoiningNewLeaderboard)
            {
                Debug.Log(string.Format("Join New Leaderboard And Prepare Reward (Type:{0}, IsClaimed:{1})", leaderboardType, leaderboardInfo.isClaimed));

                if (!leaderboardInfo.isClaimed && (pendingReward == null || (pendingReward.IsClaimed() && pendingReward.GetLeaderboardID() != leaderboardInfo.leaderboardID)))
                {
                    pendingReward = new PendingReward(playerID, leaderboardInfo.leaderboardID, GetPositionInLeaderboard(), GetLeaderboardType(), leaderboardAPI);
                }

                JoinNewLeaderboard();
            }
        }

        public bool AreLeaderboardsLoaded()
        {
            return leaderboardInfo != null && leaderboardInfo.entries != null;
        }

        public List<LeaderboardEntry> GetLeaderboardEntries()
        {
            if (leaderboardInfo == null)
            {
                return null;
            }
            else
            {
                return leaderboardInfo.entries;
            }
        }

        public long GetLeaderboardTimeLeft()
        {
            if (leaderboardInfo == null)
            {
                return 0;
            }
            else
            {
                return leaderboardInfo.GetTimeLeft();
            }
        }

        public void RegisterToDataChange(System.Action action)
        {
            OnDataChanged += action;
        }

        public void UnregisterFromDataChange(System.Action action)
        {
            OnDataChanged -= action;
        }

        public LeaderboardEntry GetPlayerLeaderboardEntry()
        {
            if(leaderboardInfo != null && leaderboardInfo.entries != null)
            {
                return leaderboardInfo.entries.Find(x => x.userID == playerID);
            }
            else
            {
                return null;
            }
        }

        public LeaderboardEntry GetOpponentLeaderboardEntry()
        {
            LeaderboardEntry playerEntry = GetPlayerLeaderboardEntry();

            if(playerEntry != null)
            {
                if(playerEntry.position > 1)
                {
                    return leaderboardInfo.entries.Find(x => x.position == playerEntry.position - 1);
                }
                else if(playerEntry.position == 1)
                {
                    if(leaderboardInfo.entries.Count > 1)
                    {
                        return leaderboardInfo.entries.Find(x => x.position == 2);
                    }
                    else
                    {
                        return null;
                    }
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        //--PRIVATE--------------------------------------------------------------------------------------//

        private void JoinNewLeaderboard()
        {
            if (!isJoiningNewLeaderboard)
            {
                Debug.Log(string.Format("Join New Leaderboard (Type:{0})", leaderboardType));
                isJoiningNewLeaderboard = true;

                leaderboardAPI.JoinLeaderboard(playerID, leaderboardType.ToString(), res =>
                {
                    if (res.isSuccess)
                    {
                        Debug.Log(string.Format("Join New Leaderboard - Success (Type:{0},ID:{1})", leaderboardType, res.value.leaderboardID));

                        LeaderboardReportedScore = 0;
                        NotSavedValueInLeaderboard = 0;
                    }

                    isJoiningNewLeaderboard = false;
                    OnLeaderboardRefreshFinished(res);
                }
                );
            }
        }

        private void OnLeaderboardRefreshFinished(LeaderboardResponse<LeaderboardInfo> res)
        {
            if (res.isSuccess)
            {
                leaderboardInfo = res.value;
                CurrentLeaderboardID = res.value.leaderboardID;
                SetNameAndScoreFromEntriesIfEmpty();

                if (AreLeaderboardsLoaded())
                {
                    SetScoreToPlayerEntryIfBetter(NotSavedValueInLeaderboard);
                    leaderboardInfo.entries = SetPositionsToEntries(leaderboardInfo.entries);
                }
            }
            else if(res.responseStatus == ResponseStatus.LEADERBOARD_NOT_FOUND)
            {
                CurrentLeaderboardID = "";

                if (OnNoUserFound != null)
                {
                    OnNoUserFound();                    
                }

                return;
            }

            OnDataChanged();
        }

        private int GetPositionInLeaderboard()
        {
            List<LeaderboardEntry> entries = GetLeaderboardEntries().OrderByDescending(x => x.score).ToList();
            LeaderboardEntry player = entries.Find(x => x.userID == playerID);

            return entries.IndexOf(player) + 1;
        }

        private void SetNameAndScoreFromEntriesIfEmpty()
        {
            LeaderboardsUserManager userManager = LeaderboardsManager.LeaderboardUserManager;

            if (userManager.GetPlayerName().Length == 0)
            {
                LeaderboardEntry player = GetLeaderboardEntries().Find(x => x.userID == playerID);
                userManager.ChangeLocalName(player.name);
                int entryScore = player.score;
                LeaderboardReportedScore = Mathf.Max(LeaderboardReportedScore, entryScore);
            }
        }

        private void SetScoreToPlayerEntryIfBetter(int score)
        {
            if (AreLeaderboardsLoaded())
            {
                LeaderboardEntry playerEntry = leaderboardInfo.entries.Find(x => x.userID == playerID);

                if (playerEntry != null && score > playerEntry.score)
                {
                    playerEntry.SetScore(score);
                    OnDataChanged();
                }
            }
        }

        private List<LeaderboardEntry> SetPositionsToEntries(List<LeaderboardEntry> entries)
        {
            entries = entries.OrderByDescending(x => x.score).ToList();
            entries = PrioritizePlayer(entries);

            for (int i = 0; i < entries.Count; i++)
            {
                entries[i].SetPosition(i + 1);
            }

            return entries;
        }

        private List<LeaderboardEntry> PrioritizePlayer(List<LeaderboardEntry> leaderboardEntries)
        {
            int playerIndex = leaderboardEntries.FindIndex(x => x.userID == playerID);

            if (playerIndex >= 0)
            {
                LeaderboardEntry playerEntry = leaderboardEntries[playerIndex];
                int bestEntryWithPlayerScoreIndex = leaderboardEntries.FindIndex(x => x.score == playerEntry.score);

                if (playerIndex != bestEntryWithPlayerScoreIndex)
                {
                    leaderboardEntries.RemoveAt(playerIndex);
                    leaderboardEntries.Insert(bestEntryWithPlayerScoreIndex, playerEntry);
                }
            }

            return leaderboardEntries;
        }

        //--PLAYER_PREFS--------------------------------------------------//

        private const string LEADERBOARD_VALUE = "CRIMSON_LEADERBOARD_VALUE_";
        private const string LEADERBOARD_ID = "CRIMSON_LEADERBOARD_ID_";
        private const string LAST_CLAIMED_LEADERBOARD_ID = "LAST_CLAIMED_LEADERBOARD_ID";
        private const string NOT_SAVED_LEADERBOARD_VALUE = "CRIMSON_NOT_SAVED_LEADERBOARD_VALUE_";

        private int LeaderboardReportedScore
        {
            get
            {
                return PlayerPrefs.GetInt(LEADERBOARD_VALUE + GetLeaderboardType().ToString(), -1);
            }
            set
            {
                PlayerPrefs.SetInt(LEADERBOARD_VALUE + GetLeaderboardType().ToString(), value);
            }
        }

        private string CurrentLeaderboardID
        {
            get
            {
                return PlayerPrefs.GetString(LEADERBOARD_ID + GetLeaderboardType().ToString(), "");
            }
            set
            {
                PlayerPrefs.SetString(LEADERBOARD_ID + GetLeaderboardType().ToString(), value);
            }
        }

        public int NotSavedValueInLeaderboard
        {
            get
            {
                return PlayerPrefs.GetInt(NOT_SAVED_LEADERBOARD_VALUE + GetLeaderboardType().ToString(), -1);
            }
            set
            {
                PlayerPrefs.SetInt(NOT_SAVED_LEADERBOARD_VALUE + GetLeaderboardType().ToString(), value);
            }
        }

    }

    public interface LeaderboardDataContainer
    {
        long GetLeaderboardTimeLeft();
        bool AreLeaderboardsLoaded();
        List<LeaderboardEntry> GetLeaderboardEntries();
        void RegisterToDataChange(System.Action action);
        void UnregisterFromDataChange(System.Action action);
    }
}