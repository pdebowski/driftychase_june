﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace CrimsonLeaderboards
{
    public class PendingReward
    {
        private string userID;
        private string leaderboardID;
        private int positionInLeaderboard;
        private LeaderboardType leaderboardType;
        private LeaderboardAPI leaderboardAPI;
        private bool isClaiming = false;
        private bool isClaimed = false;

        public PendingReward(string userID, string leaderboardID, int positionInLeaderboard, LeaderboardType leaderboardType, LeaderboardAPI leaderboardAPI)
        {
            this.userID = userID;
            this.leaderboardID = leaderboardID;
            this.positionInLeaderboard = positionInLeaderboard;
            this.leaderboardType = leaderboardType;
            this.leaderboardAPI = leaderboardAPI;
        }

        public bool IsClaimed()
        {
            return isClaimed;
        }

        public bool IsClaiming()
        {
            return isClaiming;
        }

        public int GetPosition()
        {
            return positionInLeaderboard;
        }

        public LeaderboardType GetLeaderboardType()
        {
            return leaderboardType;
        }

        public void Claim(System.Action<bool> callback)
        {
            if (!isClaiming)
            {
                isClaiming = true;

                leaderboardAPI.ClaimReward(leaderboardID, userID, res =>
                {
                    if (res.isSuccess || res.responseStatus == ResponseStatus.NOT_CLAIM)
                    {
                        isClaimed = true;
                    }

                    isClaiming = false;
                    callback(res.isSuccess);
                }
                );
            }
        }

        //--DEBUG--------------------------------------//

        public string GetLeaderboardID()
        {
            return leaderboardID;
        }

    }
}