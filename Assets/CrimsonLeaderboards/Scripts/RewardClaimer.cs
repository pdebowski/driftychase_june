﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Linq;
using System;

namespace CrimsonLeaderboards.UI
{
    public class RewardClaimer : MonoBehaviour
    {
        [SerializeField]
        private GameObject holder;
        [SerializeField]
        private Text placeText;
        [SerializeField]
        private Text rewardText;
        [SerializeField]
        private StartScreenKeysManager startScreenKeysManager;

        private LeaderboardsPendingRewardsManager pendingRewardsManager;
        private List<PendingReward> pendingRewards;
        private PendingReward pendingReward;
        private RewardManager.Reward reward;

        private void Awake()
        {
            if(!GTMParameters.CustomLeaderboardsEnabled)
            {
                Destroy(gameObject);
            }
        }

        private void Update()
        {
            ShowPopupIfNeeded();
        }

        public void ShowPopupIfNeeded()
        {
            if (pendingReward == null)
            {
                pendingRewardsManager = LeaderboardsManager.LeaderboardPendingRewardsManager;
                pendingRewards = pendingRewardsManager.GetPendingRewards();

                if (pendingRewards != null && pendingRewards.Count > 0 && !LeaderboardsManager.LeaderboardRefresher.IsError())
                {
                    Debug.Log("ShowPopup");
                    ShowPopup();
                }
            }
        }

        private void ShowPopup()
        {
            startScreenKeysManager.Block();
            holder.SetActive(true);
            SetReward();
        }

        private void HidePopup()
        {
            Debug.Log("HidePopup");

            startScreenKeysManager.Unblock();
            holder.SetActive(false);
        }

        private void SetReward()
        {

            pendingReward = pendingRewards.First();
            reward = RewardManager.Instance.GetReward(pendingReward.GetPosition(), pendingReward.GetLeaderboardType());

            Debug.Log(string.Format("SetReward (ID:{0})", pendingReward.GetLeaderboardID()));

            if (reward == null)
            {
                ClaimPendingReward();
            }
            else
            {
                ShowNextPendingReward();
            }
        }

        private void ShowNextPendingReward()
        {
            string textTemplate = "You had <b><size=120>{0}</size></b> place in\n {1} leaderboard!";
            placeText.text = string.Format(textTemplate, pendingReward.GetPosition().Ordinal(), pendingReward.GetLeaderboardType());
            rewardText.text = reward.GetRewardName();
        }

        private void ClaimPendingReward()
        {
            Debug.Log("ClaimPendingReward");

            pendingReward.Claim(RewardClaimCallback);
        }

        private void MoveNext()
        {
            pendingRewards.Remove(pendingReward);

            if (pendingRewards.Count > 0)
            {
                SetReward();
            }
            else
            {
                Debug.Log("No Rewards Left");

                pendingReward = null;
                HidePopup();
            }
        }

        private void RewardClaimCallback(bool success)
        {
            if(success)
            {
                Debug.Log("RewardClaimCallback-Success");

                PrefsDataFacade.Instance.AddTotalCash(reward.GetCashValue());
                ScoreCounter.OnCashOrScoreChanged();
                MoveNext();
            }
            else
            {
                Debug.Log("RewardClaimCallback-Fail");

                pendingReward = null;
                HidePopup();
            }
        }

        public void OnClaimedRewardButtonClick()
        {           
            ClaimPendingReward();
        }

    }
}