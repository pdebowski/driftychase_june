﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace CrimsonLeaderboards
{
    public class RewardManager : Singleton<RewardManager>
    {
        [SerializeField]
        private List<LeaderboardRewardsDefinition> rewardsDefinitions;

        public Reward GetReward(int position, LeaderboardType leaderboardType)
        {
            LeaderboardRewardsDefinition rewardsDefinition = rewardsDefinitions.Find(x => x.leaderboardType == leaderboardType);

            if (rewardsDefinition != null)
            {
                foreach (var reward in rewardsDefinition.rewards)
                {
                    if (position <= reward.GetMaxPlaceWithThisReward())
                    {
                        return reward;
                    }
                }

                if (rewardsDefinition.rewards.Count > 0)
                {
                    return rewardsDefinition.rewards[rewardsDefinition.rewards.Count - 1];
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        public Reward GetRewardIfEdgePosition(int position, LeaderboardType leaderboardType)
        {
            LeaderboardRewardsDefinition rewardsDefinition = rewardsDefinitions.Find(x => x.leaderboardType == leaderboardType);

            if (rewardsDefinition != null)
            {
                Reward prevReward = null;

                foreach (var reward in rewardsDefinition.rewards)
                {
                    if (position <= reward.GetMaxPlaceWithThisReward() && (prevReward == null || prevReward.GetMaxPlaceWithThisReward() + 1 == position))
                    {
                        return reward;
                    }

                    prevReward = reward;
                }

                return null;
            }
            else
            {
                return null;
            }
        }

        //--DEFINITIONS------------------------------------//

        [System.Serializable]
        public class LeaderboardRewardsDefinition
        {
            public LeaderboardType leaderboardType;
            public List<Reward> rewards;
        }

        [System.Serializable]
        public class Reward
        {
            [SerializeField]
            private int cashValue = 100;
            [SerializeField]
            private int maxPlaceWithThisReward = 5;

            private string rewardFormat = "&   {0}";

            public int GetMaxPlaceWithThisReward()
            {
                return maxPlaceWithThisReward;
            }

            public string GetRewardName()
            {
                return string.Format(rewardFormat, CommonMethods.GetCashStringWithSpacing(cashValue));
            }

            public int GetCashValue()
            {
                return cashValue;
            }
        }
    }
}