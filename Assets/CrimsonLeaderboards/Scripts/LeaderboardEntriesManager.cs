﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;

namespace CrimsonLeaderboards.UI
{
    public class LeaderboardEntriesManager : MonoBehaviour
    {
        [SerializeField]
        private GameObject entryPrefab;
        [SerializeField]
        private GameObject separatorPrefab;
        [SerializeField]
        private Transform inactiveEntriesHolder;
        [SerializeField]
        private ScrollRect scrollRect;
        [SerializeField]
        private GameObject changeNamePopup;

        private LeaderboardType leaderboardType;
        private LeaderboardDataContainer leaderboard;
        private List<LeaderboardEntryItem> visualEntries;
        private Dictionary<int, LeaderboardEntriesSeparator> separators;

        public void Init(LeaderboardDataContainer leaderboard, LeaderboardType leaderboardType)
        {
            this.leaderboard = leaderboard;
            this.leaderboardType = leaderboardType;
        }

        public void RefreshLeaderboardData()
        {
            StartCoroutine(AddEntriesInNextFrame()); // need wait 1 frame to refresh entries position in grid layout
        }

        private IEnumerator AddEntriesInNextFrame()
        {
            yield return null;

            if (visualEntries == null)
            {
                visualEntries = new List<LeaderboardEntryItem>();
            }
            if (separators == null)
            {
                separators = new Dictionary<int, LeaderboardEntriesSeparator>();
            }

            List<LeaderboardEntry> dataEntries = leaderboard.GetLeaderboardEntries();

            GridLayoutGroup leaderboardEntriesLayout = GetComponentInChildren<GridLayoutGroup>(true);

            for (int i = 0; i < dataEntries.Count; i++)
            {
                LeaderboardEntry dataEntry = dataEntries[i];

                if (visualEntries.Count <= i)
                {
                    visualEntries.Add((Instantiate(entryPrefab) as GameObject).GetComponent<LeaderboardEntryItem>());
                    SetSeparatorIfNeeded(i + 1, leaderboardEntriesLayout);
                    visualEntries[i].transform.SetParent(leaderboardEntriesLayout.transform, false);
                }

                if (visualEntries[i].gameObject.GetComponent<Button>() == null)
                {
                    visualEntries[i].gameObject.AddComponent<Button>();
                    visualEntries[i].gameObject.GetComponent<Button>().onClick.AddListener(() => changeNamePopup.SetActive(true));
                }

                bool isPlayerInCurrentEntry = DoesIDBelongsToPlayer(dataEntry.userID);
                visualEntries[i].gameObject.GetComponent<Button>().enabled = isPlayerInCurrentEntry;

                visualEntries[i].SetValues(dataEntry.position, dataEntry.name, dataEntry.score, isPlayerInCurrentEntry);
                visualEntries[i].gameObject.SetActive(true);         

                if (separators.ContainsKey(i))
                {
                    separators[i].gameObject.SetActive(true);
                }
            }

            for (int i = dataEntries.Count; i < visualEntries.Count; i++)
            {
                visualEntries[i].gameObject.SetActive(false);

                if (separators.ContainsKey(i) && separators[i] != null)
                {
                    separators[i].gameObject.SetActive(false);
                }
            }

            CenterPlayer();
        }

        private bool DoesIDBelongsToPlayer(string id)
        {
            return LeaderboardsManager.LeaderboardDataContainerProvider.DoesIDBelongsToPlayer(id);
        }

        private void SetSeparatorIfNeeded(int positionInLeaderboard, GridLayoutGroup gridLayoutGroup)
        {
            RewardManager.Reward reward = GetRewardIfEdgePosition(positionInLeaderboard);

            if (reward != null)
            {
                GameObject separatorObject = Instantiate(separatorPrefab) as GameObject;
                LeaderboardEntriesSeparator separator = separatorObject.GetComponent<LeaderboardEntriesSeparator>();
                separator.SetRewardText(reward.GetRewardName());
                separatorObject.transform.SetParent(gridLayoutGroup.transform, false);
                separators.Add(positionInLeaderboard - 1, separator);
            }
        }

        private RewardManager.Reward GetReward(int positionInLeaderboard)
        {
            return RewardManager.Instance.GetReward(positionInLeaderboard, leaderboardType);
        }

        private RewardManager.Reward GetRewardIfEdgePosition(int positionInLeaderboard)
        {
            return RewardManager.Instance.GetRewardIfEdgePosition(positionInLeaderboard, leaderboardType);
        }

        public void CenterPlayer()
        {
            if (visualEntries != null && visualEntries.Count > 0)
            {
                LeaderboardEntryItem visualEntry = visualEntries.Find(x => x.IsLocalUser());
                if (visualEntry != null)
                {
                    int playerObjectIndex = visualEntry.transform.GetSiblingIndex();
                    int objectsCount = visualEntry.transform.parent.childCount;
                    scrollRect.verticalNormalizedPosition = Mathf.Lerp(-0.1f, 1.1f, 1 - playerObjectIndex / (float)objectsCount);
                }
            }
        }
    }
}
