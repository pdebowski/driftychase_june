﻿using System.Collections;
using SimpleJSON;
using System;
using System.Collections.Generic;
using System.Text;

namespace CrimsonLeaderboards
{
    public class LeaderboardsProtocolImp : LeaderboardsProtocol
    {
        private const int TOKEN_LENGTH = 32;
        private const string UPDATE_USER_URL = "updateuser";
        private const string CREATE_USER_URL = "createuser";
        private const string JOIN_LEADERBOARDS_URL = "joinleaderboard";
        private const string REPORT_SCORE_URL = "reportscore";
        private const string READ_LEADERBOARDS_URL = "readleaderboard";
        private const string CLAIM_REWARD_URL = "claimreward";
        private const int TIMEOUT = 3;
        private const string RESPONSE_STATUS = "responseStatus";
        private const string LEADERBOARD_INFO = "leaderboardInfo";
        private const string LEADERBOARD_TYPE = "leaderboardType";
        private const string LEADERBOARD_ID = "leaderboardId";

        private const string USER_ID = "userId";
        private const string SCORE = "score";
        private const string NAME = "name";
        private const string MESSAGE = "message";
        private const string MESSAGE_ID = "messageId";
        private const string SIGNATURE = "signature";
        public System.Action<AllLeaderboardsInfo> OnLeaderboardsUpdateResponse;


        private string secureSalt;
        private bool executorRunning = false;
        private HttpExecutor httpExecutor;
        private Queue<Request> httpRequests = new Queue<Request>();

        //string address = "http://192.168.0.129:8080/";
        string hostname;

        private class Request
        {
            public Request(string url, System.Action<string> responseHandler)
            {
                variables = new Dictionary<string, string>();
                this.url = url;
                this.responseHandler = responseHandler;
            }

            public void AddVariable(string key, string value)
            {
                variables.Add(key, value);
            }

            public string url { get; set; }
            public Dictionary<string, string> variables { get; set; }
            public System.Action<string> responseHandler { get; set; }
        }

        private class RequestBuilder
        {
            private string secureSalt;
            private Dictionary<string, string> variables = new Dictionary<string, string>();

            public RequestBuilder(string secureSalt)
            {
                this.secureSalt = secureSalt;
            }

            public void AddVariable(string key, string value)
            {
                variables.Add(key, value);
            }

            public Request Build(string url, System.Action<string> responseHandler)
            {
                Request request = new Request(url, responseHandler);

                StringBuilder message = new StringBuilder();
                foreach (KeyValuePair<string, string> entry in variables)
                {
                    if (message.Length != 0)
                    {
                        message.Append(",");
                    }

                    message.Append(entry.Key).Append(":").Append(entry.Value);
                }

                string messageStr = message.ToString();

                request.AddVariable(MESSAGE, messageStr);

                string token = Guid.NewGuid().ToString();
                request.AddVariable(MESSAGE_ID, token);
                request.AddVariable(SIGNATURE, new CustomLeaderboardsProtocolHash().Hash(secureSalt, messageStr, token));

                return request;
            }
        }

        public override void Init(HttpExecutor httpExecutor, string hostname, string secureSalt)
        {
            this.httpExecutor = httpExecutor;
            this.secureSalt = secureSalt;
            this.hostname = hostname;
        }


        // PUBLIC 

        public override void Update()
        {
            if (executorRunning)
            {
                if (httpExecutor.isReady())
                {
                    HttpRequestStatus status = httpExecutor.getStatus();
                    Request request = httpRequests.Dequeue();

                    if (status == HttpRequestStatus.OK)
                    {
                        request.responseHandler.Invoke(httpExecutor.getResponse());
                    }
                    else
                    {
                        request.responseHandler.Invoke(null);
                    }

                    executorRunning = false;
                }
            }

            if (!executorRunning && httpRequests.Count > 0)
            {
                Request request = httpRequests.Peek();
                httpExecutor.sendRequest(hostname + "/" + request.url, request.variables);
                executorRunning = true;
            }
        }

        private ResponseStatus GetResponseStatusFromServer(string response)
        {
            try
            {
                JSONNode jsonResponse = JSON.Parse(response);

                return (ResponseStatus)Enum.Parse(typeof(ResponseStatus), jsonResponse[RESPONSE_STATUS].Value.ToString());
            }
            catch (Exception)
            {
                return ResponseStatus.NOT_LEGIT;
            }

        }

        public override void ReportScore(string leaderboardId, string userID, int score, System.Action<LeaderboardResponse<LeaderboardInfo>> callback)
        {
            RequestBuilder requestBuilder = new RequestBuilder(secureSalt);
            requestBuilder.AddVariable(LEADERBOARD_ID, leaderboardId);
            requestBuilder.AddVariable(USER_ID, userID);
            requestBuilder.AddVariable(SCORE, score.ToString());

            Request request = requestBuilder.Build(REPORT_SCORE_URL, s =>
                {
                    LeaderboardInfo leaderboardInfo = null;
                    bool isSuccess = true;
                    ResponseStatus responseStatus = ResponseStatus.ERROR;

                    try
                    {
                        JSONNode jsonResponse = JSON.Parse(s);
                        responseStatus = GetResponseStatusFromServer(s);

                        if (responseStatus == ResponseStatus.OK)
                        {
                            leaderboardInfo = new CustomLeaderboardsJsonParser().Parse(jsonResponse[LEADERBOARD_INFO]);
                            isSuccess = true;
                        }
                        else
                        {
                            isSuccess = false;
                        }

                    }
                    catch (System.Exception)
                    {
                        isSuccess = false;
                    }

                    LeaderboardResponse<LeaderboardInfo> response = new LeaderboardResponse<LeaderboardInfo>();
                    response.isSuccess = isSuccess;
                    response.responseStatus = responseStatus;
                    response.value = leaderboardInfo;
                    response.rawResponse = s;

                    callback.Invoke(response);
                }
            );

            httpRequests.Enqueue(request);
        }

        public override void JoinLeaderboard(string userId, string leaderboardType, System.Action<LeaderboardResponse<LeaderboardInfo>> callback)
        {
            RequestBuilder requestBuilder = new RequestBuilder(secureSalt);
            requestBuilder.AddVariable(USER_ID, userId);
            requestBuilder.AddVariable(LEADERBOARD_TYPE, leaderboardType);

            Request request = requestBuilder.Build(JOIN_LEADERBOARDS_URL, s =>
            {
                LeaderboardInfo leaderboardsInfo = null;
                bool isSuccess = true;
                ResponseStatus responseStatus = ResponseStatus.ERROR;

                try
                {
                    JSONNode jsonResponse = JSON.Parse(s);
                    responseStatus = GetResponseStatusFromServer(s);

                    if (responseStatus == ResponseStatus.OK)
                    {
                        leaderboardsInfo = new CustomLeaderboardsJsonParser().Parse(jsonResponse[LEADERBOARD_INFO]);
                        isSuccess = true;
                    }
                    else
                    {
                        isSuccess = false;
                    }
                }
                catch (System.Exception)
                {
                    isSuccess = false;
                }

                LeaderboardResponse<LeaderboardInfo> response = new LeaderboardResponse<LeaderboardInfo>();

                response.isSuccess = isSuccess;
                response.responseStatus = responseStatus;
                response.value = leaderboardsInfo;
                response.rawResponse = s;

                callback.Invoke(response);
            }
            );

            httpRequests.Enqueue(request);
        }


        public override void ReadLeaderboard(string userId, string leaderboardId, System.Action<LeaderboardResponse<LeaderboardInfo>> callback)
        {
            RequestBuilder requestBuilder = new RequestBuilder(secureSalt);
            requestBuilder.AddVariable(USER_ID, userId);
            requestBuilder.AddVariable(LEADERBOARD_ID, leaderboardId);

            Request request = requestBuilder.Build(READ_LEADERBOARDS_URL, s =>
                {
                    LeaderboardInfo leaderboardsInfo = null;
                    bool isSuccess = true;
                    ResponseStatus responseStatus = ResponseStatus.ERROR;

                    try
                    {
                        JSONNode jsonResponse = JSON.Parse(s);
                        responseStatus = GetResponseStatusFromServer(s);

                        if (responseStatus == ResponseStatus.OK)
                        {
                            leaderboardsInfo = new CustomLeaderboardsJsonParser().Parse(jsonResponse[LEADERBOARD_INFO]);
                            isSuccess = true;
                        }
                        else
                        {
                            isSuccess = false;
                        }
                    }
                    catch (System.Exception)
                    {
                        isSuccess = false;
                    }

                    LeaderboardResponse<LeaderboardInfo> response = new LeaderboardResponse<LeaderboardInfo>();
                    response.isSuccess = isSuccess;
                    response.responseStatus = responseStatus;
                    response.value = leaderboardsInfo;
                    response.rawResponse = s;

                    callback.Invoke(response);
                }
            );

            httpRequests.Enqueue(request);
        }

        public override void UpdateUser(string userID, string newName, System.Action<LeaderboardResponse<bool>> callback)
        {
            RequestBuilder requestBuilder = new RequestBuilder(secureSalt);
            requestBuilder.AddVariable(USER_ID, userID);
            requestBuilder.AddVariable(NAME, newName);

            Request request = requestBuilder.Build(UPDATE_USER_URL, s =>
                {
                    bool isSuccess = true;
                    ResponseStatus responseStatus = ResponseStatus.ERROR;
                    LeaderboardResponse<bool> response = new LeaderboardResponse<bool>();

                    try
                    {
                        JSONNode jsonResponse = JSON.Parse(s);
                        responseStatus = GetResponseStatusFromServer(s);

                        if (responseStatus == ResponseStatus.OK)
                        {
                            isSuccess = true;
                        }
                        else
                        {
                            isSuccess = false;
                        }
                    }
                    catch (Exception)
                    {
                        isSuccess = false;
                    }

                    response.isSuccess = isSuccess;
                    response.responseStatus = responseStatus;
                    response.rawResponse = s;

                    callback.Invoke(response);
                }
            );

            httpRequests.Enqueue(request);
        }

        public override void ClaimReward(string leaderboardId, string userID, System.Action<LeaderboardResponse<bool>> callback)
        {
            RequestBuilder requestBuilder = new RequestBuilder(secureSalt);
            requestBuilder.AddVariable(USER_ID, userID);
            requestBuilder.AddVariable(LEADERBOARD_ID, leaderboardId);

            Request request = requestBuilder.Build(CLAIM_REWARD_URL, s =>
                {
                    bool isSuccess = true;
                    ResponseStatus responseStatus = ResponseStatus.ERROR;
                    LeaderboardResponse<bool> response = new LeaderboardResponse<bool>();

                    try
                    {
                        JSONNode jsonResponse = JSON.Parse(s);
                        responseStatus = GetResponseStatusFromServer(s);

                        if (responseStatus == ResponseStatus.OK)
                        {
                            isSuccess = true;
                        }
                        else
                        {
                            isSuccess = false;
                        }
                    }
                    catch (Exception)
                    {
                        isSuccess = false;
                    }

                    response.isSuccess = isSuccess;
                    response.responseStatus = responseStatus;

                    callback.Invoke(response);
                }
            );

            httpRequests.Enqueue(request);
        }

        public override void CreateUser(string userID, string name, System.Action<LeaderboardResponse<bool>> callback)
        {
            RequestBuilder requestBuilder = new RequestBuilder(secureSalt);
            requestBuilder.AddVariable(USER_ID, userID);
            requestBuilder.AddVariable(NAME, name);

            Request request = requestBuilder.Build(CREATE_USER_URL, s =>
                {
                    bool isSuccess = true;
                    ResponseStatus responseStatus = ResponseStatus.ERROR;

                    LeaderboardResponse<bool> response = new LeaderboardResponse<bool>();

                    try
                    {
                        JSONNode jsonResponse = JSON.Parse(s);
                        responseStatus = GetResponseStatusFromServer(s);

                        if (responseStatus == ResponseStatus.OK)
                        {
                            isSuccess = true;
                        }
                        else
                        {
                            isSuccess = false;
                        }
                    }
                    catch (Exception)
                    {
                        isSuccess = false;
                    }

                    response.isSuccess = isSuccess;
                    response.responseStatus = responseStatus;
                    response.rawResponse = s;

                    callback.Invoke(response);
                }
            );

            httpRequests.Enqueue(request);
        }
    }
}