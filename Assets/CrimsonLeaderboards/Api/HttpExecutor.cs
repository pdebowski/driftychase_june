﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public enum HttpRequestStatus { OK, WAITING, ERROR_EMPTY_RESPONSE, ERROR_TIMEOUT, ERROR_OTHER }

public interface HttpExecutor
{
    void Init(System.Action OnError, System.Action OnSuccess);
    void sendRequest(string hostname, Dictionary<string, string> variables);
	bool isReady();
	HttpRequestStatus getStatus();
	string getResponse();
}
