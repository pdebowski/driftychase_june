﻿using System.Collections.Generic;
using SimpleJSON;
using System;
using System.Linq;

namespace CrimsonLeaderboards
{

    public class CustomLeaderboardsJsonParser
    {

        private const string ENTRY_NAME = "name";
        private const string ENTRY_SCORE = "score";
        private const string ENTRY_UID = "id";

        private const string LEADERBOARD_ENTRIES = "leaderboardEntries";
        private const string LEADERBOARD_TIME_LEFT = "leaderboardTimeLeft";
        private const string LEADERBOARD_TYPE = "leaderboardType";
        private const string IS_CLAIMED = "claimed";
        private const string LEADERBOARD_ID = "leaderboardId";


        private System.DateTime REFERENCE_TIME = new System.DateTime(2016, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);


        public LeaderboardInfo Parse(JSONNode data)
        {
            try
            {
                return ParseRanking(data);
            }
            catch (Exception)
            {
                return null;
            }

        }

        public AllLeaderboardsInfo ParseMulti(JSONArray data)
        {
            try
            {
                AllLeaderboardsInfo result = new AllLeaderboardsInfo();
                result.LeaderboardsInfo = ParseRankings(data);
                return result;
            }
            catch (Exception)
            {
                return null;
            }

        }

        private List<LeaderboardInfo> ParseRankings(JSONArray data)
        {
            List<LeaderboardInfo> leadeboardsInfo = new List<LeaderboardInfo>();


            for (int i = 0; i < data.Count; i++)
            {
                LeaderboardInfo customLeaderboardInfo = ParseRanking(data.AsArray[i]);
                leadeboardsInfo.Add(customLeaderboardInfo);
            }

            return leadeboardsInfo;
        }

        private LeaderboardInfo ParseRanking(JSONNode data)
        {
            LeaderboardInfo customLeaderboardInfo = new LeaderboardInfo();
            List<LeaderboardEntry> entries = new List<LeaderboardEntry>();

            for (int j = 0; j < data[LEADERBOARD_ENTRIES].Count; j++)
            {
                JSONNode jsonNode = data[LEADERBOARD_ENTRIES].AsArray[j];

                entries.Add(new LeaderboardEntry
                    (jsonNode[ENTRY_NAME].Value,
                    jsonNode[ENTRY_SCORE].AsInt,
                    jsonNode[ENTRY_UID].Value));
            }

            customLeaderboardInfo.entries = entries;
            customLeaderboardInfo.SetLeaderboardTimeLeft(data[LEADERBOARD_TIME_LEFT].AsInt);
            customLeaderboardInfo.type = data[LEADERBOARD_TYPE].Value;
            customLeaderboardInfo.isClaimed = data[IS_CLAIMED].AsBool;
            customLeaderboardInfo.leaderboardID = data[LEADERBOARD_ID];

            return customLeaderboardInfo;
        }

    }
}