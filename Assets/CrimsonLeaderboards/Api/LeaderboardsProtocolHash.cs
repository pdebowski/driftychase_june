﻿using System.Collections;
using SimpleJSON;
using System.Security.Cryptography;
using System.Text;

public class CustomLeaderboardsProtocolHash
{

    public string Hash(params string[] strings)
    {
		string sb = "";
		foreach (var s in strings) {
			sb += s;
		} 
        return HashString(sb.ToString());
    }

    private string HashString(string text)
    {
        SHA256Managed crypter = new SHA256Managed();
        StringBuilder hash = new StringBuilder();
        byte[] crypto = crypter.ComputeHash(Encoding.UTF8.GetBytes(text), 0, Encoding.UTF8.GetByteCount(text));
        foreach (var item in crypto)
        {
            hash.Append(item.ToString("x2"));

		}

        return hash.ToString();
    }


}
