﻿using System.Collections;

namespace CrimsonLeaderboards
{
    public abstract class LeaderboardsProtocol
    {

        public abstract void Init(HttpExecutor httpExecutor, string hostname, string secureSalt);
        public abstract void Update();
        public abstract void CreateUser(string UID, string newName, System.Action<LeaderboardResponse<bool>> callback);
        public abstract void UpdateUser(string UID, string newName, System.Action<LeaderboardResponse<bool>> callback);
        public abstract void JoinLeaderboard(string UID, string leaderboardType, System.Action<LeaderboardResponse<LeaderboardInfo>> callback);
        public abstract void ReportScore(string leaderboardId, string UID, int score, System.Action<LeaderboardResponse<LeaderboardInfo>> callback);

        public abstract void ReadLeaderboard(string userId, string leaderboardId, System.Action<LeaderboardResponse<LeaderboardInfo>> callback);
        public abstract void ClaimReward(string leaderboardId, string userID, System.Action<LeaderboardResponse<bool>> callback);
    }

    public class LeaderboardResponse<T>
    {
        public bool isSuccess;
        public T value;
        public string rawResponse;
        public ResponseStatus responseStatus;

        public LeaderboardResponse()
        {
            responseStatus = ResponseStatus.ERROR;
        }

    }
}