﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace CrimsonLeaderboards
{

    public class LeaderboardAPI : MonoBehaviour
    {
        private string secureSalt;
        private LeaderboardsProtocol protocol;

        public void Init(HttpExecutor httpExecutor, string hostname, string secureSalt)
        {
            this.secureSalt = secureSalt;

            protocol = new LeaderboardsProtocolImp();
            protocol.Init(httpExecutor, hostname, secureSalt);
        }

        private void Update()
        {
            protocol.Update();
        }

        public void CreateUser(string userID, string newName, System.Action<LeaderboardResponse<bool>> callback)
        {
            protocol.CreateUser(userID, newName, callback);
        }

        public void UpdateUser(string userID, string newName, System.Action<LeaderboardResponse<bool>> callback)
        {
            protocol.UpdateUser(userID, newName, callback);
        }

        public void JoinLeaderboard(string userID, string leaderboardType, System.Action<LeaderboardResponse<LeaderboardInfo>> callback)
        {
            protocol.JoinLeaderboard(userID, leaderboardType, callback);
        }

        public void ReportScore(string leaderboardId, string userID, int score, System.Action<LeaderboardResponse<LeaderboardInfo>> callback)
        {
            protocol.ReportScore(leaderboardId, userID, score, callback);
        }

        public void ClaimReward(string leaderboardId, string userID, System.Action<LeaderboardResponse<bool>> callback)
        {
            protocol.ClaimReward(leaderboardId, userID, callback);
        }

        public void ReadLeaderboard(string userId, string leaderboardID, System.Action<LeaderboardResponse<LeaderboardInfo>> callback)
        {
            protocol.ReadLeaderboard(userId, leaderboardID, callback);
        }
    }

    public class AllLeaderboardsInfo
    {
        public List<LeaderboardInfo> LeaderboardsInfo;

        public AllLeaderboardsInfo()
        {
            LeaderboardsInfo = new List<LeaderboardInfo>();
        }
    }

    public class LeaderboardInfo
    {
        public List<LeaderboardEntry> entries;
        public bool isClaimed;
        public string type;
        public string leaderboardID;

        private long leaderboardTimeLeft;
        private long responeArriveTime;


        public void SetLeaderboardTimeLeft(long secondsLeft)
        {
            leaderboardTimeLeft = secondsLeft * 1000;
            responeArriveTime = GetMiliSecondsSince1970();
        }

        public long GetTimeLeft()
        {
            return leaderboardTimeLeft - (GetMiliSecondsSince1970() - responeArriveTime);
        }

        private long GetMiliSecondsSince1970()
        {
            return (long)(System.DateTime.UtcNow - new System.DateTime(1970, 1, 1)).TotalMilliseconds;
        }
    }

    public class LeaderboardEntry
    {
        public string name { get; private set; }
        public int score { get; private set; }
        public string userID { get; private set; }
        public int position { get; private set; }

        public LeaderboardEntry(string name, int score, string uuid)
        {
            this.name = name;
            this.score = score;
            this.userID = uuid;
        }

        public LeaderboardEntry(LeaderboardEntry other)
        {
            name = other.name;
            score = other.score;
            userID = other.userID;
            position = other.position;
        }

        public void SetScore(int score)
        {
            this.score = score;
        }

        public void SetPosition(int position)
        {
            this.position = position;
        }
    }

    public enum ResponseStatus
    {
        OK = 0,
        ERROR = 11,
        NOT_LEGIT = 12,
        DATA_RESPONSE_REQUEST_FAIL = 13,
        NOT_CLAIM = 14,
        USER_ALREADY_EXISTS = 15,
        LEADERBOARD_NOT_FOUND = 16

    }

}