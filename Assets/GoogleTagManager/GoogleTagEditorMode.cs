﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class GoogleTagEditorMode :MonoBehaviour
 {
    public TextAsset jsonFile;

    [System.Serializable]
    public class Value
    {
        public string key;
        public string value;
    }

    public List<Value> editorValues;
    //------------------------------------------------------------//

    public int GetAchievementFrequency()
    {
        string key = "AchievementFrequency";    

#if UNITY_EDITOR
        return int.Parse(editorValues.Find(x => x.key == key).value);
#else
        return (int)GoogleTagManagerPlugin.Instance.GetLong(key);
#endif
    }

    public string GetVariation()
    {
        string key = "siema";

#if UNITY_EDITOR
        return editorValues.Find(x => x.key == key).value;
#else
        return GoogleTagManagerPlugin.Instance.GetString(key);
#endif
    }
}
