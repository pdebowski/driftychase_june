﻿using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;

public class GTMIOSProxy {

	[DllImport("__Internal")]
	private static extern void _Initialize(string containerId, double timeout, bool isDebug);

	[DllImport("__Internal")]
	private static extern string _StringForKey(string key);

	[DllImport("__Internal")]
	private static extern bool _BoolForKey(string key);

	[DllImport("__Internal")]
	private static extern double _DoubleForKey(string key);

	[DllImport("__Internal")]
	private static extern long _LongForKey(string key);

	[DllImport("__Internal")]
	private static extern void _PushValueToDataLayer(string key, string value);

	[DllImport("__Internal")]
	private static extern void _Refresh();

	[DllImport("__Internal")]
	private static extern bool _IsDefault();

	[DllImport("__Internal")]
	private static extern string _GetValueFromDataLayer (string key);

	[DllImport("__Internal")]
	private static extern double _LastRefreshTime ();


	public void Initialize(string contrainerId, double timeout, bool isDebug)
	{
		_Initialize (contrainerId, timeout, isDebug);
	}

	public string StringForKey(string key)
	{
		return _StringForKey (key);
	}

	public bool BoolForKey(string key)
	{
		return _BoolForKey (key);
	}

	public double DoubleForKey(string key)
	{
		return _DoubleForKey (key);
	}

	public long LongForKey(string key)
	{
		return _LongForKey (key);
	}

	public void PushValueToDataLayer(string key, string value)
	{
		_PushValueToDataLayer (key, value);
	}

	public void Refresh()
	{
		_Refresh ();
	}

	public bool IsDefault()
	{
		return _IsDefault ();
	}

	public string GetValueFromDataLayer(string key)
	{
		return _GetValueFromDataLayer (key);	
	}

	public double LastRefreshTime()
	{
		return _LastRefreshTime ();	
	}



}

