﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Linq;

public class GoogleTagManagerPlugin : Singleton<GoogleTagManagerPlugin>
{
    public string containerID;
	public string IOSContainerID;
    public string binaryContainerName;
    public bool debugMode;
#if UNITY_ANDROID
    private AndroidJavaObject GTMPlugin;
#elif UNITY_IOS || UNITY_TVOS
	private GTMIOSProxy gtmIosProxy;
#endif
    private bool loaded = false;
    GoogleTagEditorMode googleTagEditor;

    List<CachedValue<double>> cachedDoubles = new List<CachedValue<double>>();
    List<CachedValue<long>> cachedLongs = new List<CachedValue<long>>();
    List<CachedValue<string>> cachedStrings = new List<CachedValue<string>>();
    List<CachedValue<bool>> cachedBools = new List<CachedValue<bool>>();


    void PushCustomConditionsToDataLayer()
    {
        //put here game specific code ( ex. experiment runs only when player's level is > 10 )
    }

    void Initialize()
    {
        PushCustomConditionsToDataLayer();

        PushValueToDataLayer("expID", PlayerPrefs.GetString("GTM_EXP_ID"));

        bool newPlayer = !PlayerPrefs.HasKey("GTM_OLD_PLAYER");
        PushValueToDataLayer("IsNewPlayer", newPlayer.ToString());
        bool neverHadExp = !PlayerPrefs.HasKey("GTM_HAD_EXP");
        PushValueToDataLayer("NeverHadExp", neverHadExp.ToString());

        LoadContainer();
        string GTMExpID = GetString("expKey",true);

        //GET SOME EXPERIMENT
        if ( !IsDefault() && ( GTMExpID == "" || GTMExpID == null) )
        {           
            string expKey = "";
            string experimentWeights = GetString("ExperimentWeights", true);
            List <string> expIDList = GetExpIDList(experimentWeights);

            //for safety reason - if all weights == 0
            PushValueToDataLayer("expID", "0");

            while (expKey.Length == 0 && expIDList.Count > 0)
            {
                string expID = expIDList[0];
                expIDList.RemoveAt(0);
                PushValueToDataLayer("expID", expID);
                LoadContainer();
                expKey = GetString("expKey", true);     
                
                if(expKey.Length > 0)
                {
                    PlayerPrefs.SetString("GTM_HAD_EXP", "True");
                    break;
                }      
            }

            PlayerPrefs.SetString("GTM_EXP_ID", expKey);
        }
        else
        {
            PlayerPrefs.SetString("GTM_EXP_ID", GTMExpID);
        }

        PlayerPrefs.SetString("GTM_OLD_PLAYER", "True");
    }


    void LoadContainer()
    {
#if UNITY_ANDROID
        loaded = GTMPlugin.Call<bool>("Initialize", containerID, binaryContainerName, debugMode);
#elif UNITY_IOS|| UNITY_TVOS
		gtmIosProxy.Initialize(IOSContainerID,3,Debug.isDebugBuild);
		loaded=true;
#endif
    }

    new void Awake()
    {
        base.Awake();
#if UNITY_EDITOR
        googleTagEditor = GetComponent<GoogleTagEditorMode>();
		return;
#elif UNITY_IOS|| UNITY_TVOS
		debugMode = Debug.isDebugBuild;
		debugMode=false;
		gtmIosProxy=new GTMIOSProxy();
		Invoke ("Refresh", 5);
#else
        AndroidJavaClass unity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject currentActivity = unity.GetStatic<AndroidJavaObject>("currentActivity");

        GTMPlugin = new AndroidJavaObject("com.crimsonpine.gtm.GTMPlugin", currentActivity,gameObject.name);
#endif

		Initialize();
		
		if (loaded == false)
		{
			Debug.LogError("TagManagerLoadError");
		}
		
		if(debugMode)
		{
			Debug.Log("GoogleTagManager IsDefault: " + IsDefault().ToString());
		}
    }




    //----------------VALUES----------------------//

    public string GetString(string valueName, bool forceRefresh = false)
    {
#if UNITY_EDITOR
        return googleTagEditor.editorValues.Find(x => x.key == valueName).value;
#else
        return GetValue(valueName, cachedStrings, forceRefresh);
#endif
    }

    public bool GetBool(string valueName, bool forceRefresh = false)
    {
#if UNITY_EDITOR
        return bool.Parse(googleTagEditor.editorValues.Find(x => x.key == valueName).value);
#else
        return GetValue(valueName, cachedBools, forceRefresh);
#endif
    }

    public int GetInt(string valueName, bool forceRefresh = false)
    {
        return System.Convert.ToInt32(GetLong(valueName,forceRefresh));
    }

    public long GetLong(string valueName, bool forceRefresh = false)
    {
#if UNITY_EDITOR
		return long.Parse(googleTagEditor.editorValues.Find(x => x.key == valueName).value);
#else
        return GetValue(valueName, cachedLongs, forceRefresh);
#endif
    }

    public float GetFloat(string valueName, bool forceRefresh = false)
    {
        return System.Convert.ToSingle(GetDouble(valueName,forceRefresh));
    }

    public double GetDouble(string valueName, bool forceRefresh = false)
    {
#if UNITY_EDITOR
		return double.Parse(googleTagEditor.editorValues.Find(x => x.key == valueName).value);
#else
        return GetValue(valueName, cachedDoubles, forceRefresh);
#endif
    }

    private T GetContainerValue<T>(string valueName)
    {
        if (typeof(T) == typeof(string))
        {
#if UNITY_IOS|| UNITY_TVOS
		return (T)System.Convert.ChangeType(gtmIosProxy.StringForKey(valueName),typeof(T));
#else
            return GTMPlugin.Call<T>("GetString", valueName);
#endif
        }
        else if (typeof(T) == typeof(bool))
        {
#if UNITY_IOS|| UNITY_TVOS
			return (T)System.Convert.ChangeType(gtmIosProxy.BoolForKey(valueName),typeof(T));
#else
            return GTMPlugin.Call<T>("GetBool", valueName);
#endif
        }
        else if (typeof(T) == typeof(long))
        {
#if UNITY_IOS|| UNITY_TVOS
			return (T)System.Convert.ChangeType(gtmIosProxy.LongForKey(valueName),typeof(T));
#else
            return GTMPlugin.Call<T>("GetLong", valueName);
#endif
        }
        else if (typeof(T) == typeof(double))
        {
#if UNITY_IOS|| UNITY_TVOS
			return (T)System.Convert.ChangeType(gtmIosProxy.DoubleForKey(valueName),typeof(T));
#else
            return GTMPlugin.Call<T>("GetDouble", valueName);
#endif
        }
        else
        {
            return default(T);
        }
    }

     private T GetValue<T>(string valueName, List<CachedValue<T>> cachedList, bool forceRefresh)
    {
        bool takenCached = false;
        T result;
        CachedValue<T> cachedValue = cachedList.Find(x => x.name == valueName);

        if ( forceRefresh || cachedValue == null || (System.DateTime.Now - cachedValue.refreshedTime).TotalHours > cachedValue.timeToCache)
        {
            result = GetContainerValue<T>(valueName);           

            if (cachedList.Contains(cachedValue))
            {
                cachedList.Remove(cachedValue);
            }

            cachedValue = new CachedValue<T>(valueName, result);
            cachedList.Add(cachedValue);

            takenCached = false;
        }
        else
        {
            result = cachedValue.value;
            takenCached = true;
        }


        if (debugMode)
        {
            Debug.Log("GoogleTagManager variable ( cached - " + takenCached.ToString() + "): " + valueName + " = " + result.ToString());
        }

        return result;
    }
    
    //--------------DATA_LAYER-------------//

    public void PushValueToDataLayer(string key, string value)
    {
#if UNITY_ANDROID
        GTMPlugin.Call("PushValueToDataLayer", key, value);
#elif UNITY_IOS|| UNITY_TVOS
		gtmIosProxy.PushValueToDataLayer(key,value);
#endif
    }
    public string GetValueFromDataLayer(string key)
    {
#if UNITY_ANDROID
        return GTMPlugin.Call<string>("GetValueFromDataLayer", key);
#elif UNITY_IOS|| UNITY_TVOS
		return gtmIosProxy.GetValueFromDataLayer(key);
#endif
		
    }
    //-------------CALLBACKS-------------//
	/*

    public void CustomTagCallback(string message)
    {
        Debug.Log("GoogleTagManager CustomTagCallback: " + message);

        //string[] commands = message.Split(';');

        //foreach (var command in commands)
        //{
        //    string[] commandPair = command.Split('-');

        //    //if (commandPair[0] == "Prioritize")
        //    //{
        //    //    string actual = PlayerPrefs.GetString("GTM_EXP_ID");
        //    //    string winner = "";
        //    //    string[] candidates = commandPair[1].Split(',');

        //    //    if(StringArrayContains(candidates,actual))
        //    //    {
        //    //        continue;
        //    //    }

        //    //    winner = candidates[Random.Range(0, candidates.Length)];

        //    //    PlayerPrefs.SetString("GTM_EXP_ID", winner);
        //    //    Debug.Log("GoogleTagManager GTM_EXP_ID set to: " + winner);
        //    //}
        //}  
    }


    public void CustomMacroCallback(string message)
    {
        Debug.Log("UNITYXXX Macro Siema Eniuuu: " + message);
    }
	*/
    //-----------OTHERS------------//

    public void Refresh() // reconnect
    {
#if UNITY_EDITOR
#elif UNITY_IOS || UNITY_TVOS
		gtmIosProxy.Refresh();
#else
        GTMPlugin.Call("Refresh");
#endif
    }
    public long GetLastRefreshTime()
    {
#if UNITY_ANDROID
        return GTMPlugin.Call<long>("GetLastRefreshTime");
#elif UNITY_IOS|| UNITY_TVOS
		return System.Convert.ToInt64(gtmIosProxy.LastRefreshTime ());
#endif
    }
    public bool IsDefault() // true if is local version
	{
#if UNITY_EDITOR
		return true;
#elif UNITY_IOS|| UNITY_TVOS
		return gtmIosProxy.IsDefault();
#else
        return GTMPlugin.Call<bool>("IsDefault");
#endif
    }

    public bool IsLoaded()
    {
        return loaded;
    }
	/*
#if UNITY_ANDROID
    public void RegisterTagCallbacks(params string[] names)
    {
        foreach (var name in names)
        {
            GTMPlugin.Call("RegisterTagCallback", name);
        }
        
    }
#endif
*/
    //-------------HELPERS----------------//

    bool StringArrayContains(string[] array, string target)
    {
        foreach (var item in array)
        {
            if(item == target)
            {
                return true;
            }
        }

        return false;
    }

    private List<string> GetExpIDList(string weightsText)
    {
        List<string> result = new List<string>();
        List<string> freeSlots = new List<string>();

        if (weightsText.Length == 0)
        {
            return result;
        }

            weightsText = weightsText.TrimEnd(';');
        string[] weightsSplited = weightsText.Split(';');

        foreach (var item in weightsSplited)
        {
            string[] itemSplited = item.Split('-');

            for (int i = 0; i < int.Parse(itemSplited[1]); i++)
            {
                freeSlots.Add(itemSplited[0]);
            }
        }

        int count = freeSlots.Count;
        for (int i = count-1; i >= 0; i--)
        {
            int index = Random.Range(0, i + 1);
            result.Add(freeSlots[index]);
            freeSlots.RemoveAt(index);
        }

        return result;
    }


    public class CachedValue<T>
    {
        public T value;
        public string name;
        public System.DateTime refreshedTime;
        public float timeToCache;

        public CachedValue(string name, T value)
        {
            this.name = name;
            this.value = value;
            this.refreshedTime = System.DateTime.Now;
            this.timeToCache = Random.Range(1.0f, 1.5f);
        }
    }
}
