﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class GTMTest : MonoBehaviour {

    public Text text;

    void Start ()
    {
        text.enabled = true;
        text.gameObject.SetActive(true);

        StartCoroutine(RefreshText());
    }

    IEnumerator RefreshText()
    {
#if UNITY_EDITOR || UNITY_IOS
        yield return null;
#else
        
        while (true)
        {
            text.text = "";
            text.text += "\n isOnline: " + !GoogleTagManagerPlugin.Instance.IsDefault();
            text.text += "\n ContainerVersion: " + GoogleTagManagerPlugin.Instance.GetString("ContainerVersion");
            text.text += "\n GTM_EXP_ID: " + PlayerPrefs.GetString("GTM_EXP_ID");
            text.text += "\n dataLayer - expID: " + GoogleTagManagerPlugin.Instance.GetValueFromDataLayer("expID");
            text.text += "\n dataLayer - newPlayer: " + GoogleTagManagerPlugin.Instance.GetValueFromDataLayer("IsNewPlayer");
            text.text += "\n dataLayer - neverHadExp: " + GoogleTagManagerPlugin.Instance.GetValueFromDataLayer("NeverHadExp");    

            text.text += "\n ----------";

            text.text += "\n VariableFake11: " + GoogleTagManagerPlugin.Instance.GetString("VariableFake11");
            text.text += "\n VariableFake12: " + GoogleTagManagerPlugin.Instance.GetString("VariableFake12");
            text.text += "\n AdTimeBetweenAds: " + GTMParameters.AdTimeBetweenAds;
            text.text += "\n InterstitialAfterStartFrequency: " + GTMParameters.InterstitialAfterStartFrequency;

            yield return new WaitForSeconds(1);
        }
        
        yield return null;
#endif
    }

}
