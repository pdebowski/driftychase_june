﻿using UnityEngine;
using SimpleJSON;

[RequireComponent(typeof(GoogleTagEditorMode))]
public class GTMLoaderJSON : MonoBehaviour {

    GoogleTagEditorMode GTM;

	public void Reload ()
    {
        GTM = GetComponent<GoogleTagEditorMode>();
        LoadJSON();
        AddBultInVariables();
    }

	
	void LoadJSON()
    {
        if(GTM.jsonFile == null || GTM.jsonFile.text.Length < 1)
        {
            Debug.LogError("JSON file doesn't exist or it's empty");
            return;
        }

        GTM.editorValues.Clear();

        var N = JSON.Parse(GTM.jsonFile.text);

        JSONNode variableNode = N["containerVersion"]["variable"];

        for (int i = 0; i < variableNode.Count; i++)
        {
            if(variableNode[i]["type"].Value == "c")
            {
                LoadVariable(variableNode[i]);
            }

            if (variableNode[i]["type"].Value == "gae")
            {
                LoadExperiment(variableNode[i]);
            }

            if (variableNode[i]["type"].Value == "vc")
            {
                LoadValueCollection(variableNode[i]);
            }
        }
    }

    void LoadVariable(JSONNode node)
    {
        GoogleTagEditorMode.Value variable = new GoogleTagEditorMode.Value();

        variable.key = node["name"].Value;
        variable.value = node["parameter"][0]["value"].Value;

        GTM.editorValues.Add(variable);
    }

    void LoadValueCollection(JSONNode node)
    { 
        string valueString = node["parameter"][0]["value"].Value;
        valueString = valueString.Replace("\n", "");

        string[] values = valueString.Split(',');

        for (int i = 0; i < values.Length; i++)
        {
            values[i] = values[i].Trim(' ', '{', '}');

            string[] value = values[i].Split(':');

            for (int j = 0; j < value.Length; j++)
            {
                value[j] = value[j].Trim('\'', ' ');
            }

            GoogleTagEditorMode.Value variable = new GoogleTagEditorMode.Value();
            variable.key = value[0];
            variable.value = value[1];
            GTM.editorValues.Add(variable);
        }
       
    }

    void LoadExperiment(JSONNode node)
    {
        string valueString = GetNodeWhere(node["parameter"], "key", "variations")["list"][0]["map"][1]["value"].Value;
        valueString = valueString.Replace("\n", "");

        string[] values = valueString.Split(',');

        for (int i = 0; i < values.Length; i++)
        {
            values[i] = values[i].Trim(' ', '{', '}');

            string[] value = values[i].Split(':');

            for (int j = 0; j < value.Length; j++)
            {
                value[j] = value[j].Trim('\'', ' ');
            }

            GoogleTagEditorMode.Value variable = new GoogleTagEditorMode.Value();
            variable.key = value[0];
            variable.value = value[1];
            GTM.editorValues.Add(variable);
        }

    }

    JSONNode GetNodeWhere(JSONNode parentNode, string key, string value)
    {
        foreach (JSONNode node in parentNode.AsArray)
        {
            if (node[key].Value == value)
            {
                return node;
            }
        }

        return null;
    }

    private void AddBultInVariables()
    {
        GoogleTagEditorMode.Value variable = new GoogleTagEditorMode.Value();
        variable.key = "Container Version";
        variable.value = "-1";
        GTM.editorValues.Add(variable);
    }
}
