﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(GTMLoaderJSON))]
public class GTMLoaderJSON_Editor : Editor {

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        GTMLoaderJSON script = (GTMLoaderJSON)target;
      

        if (GUILayout.Button("Reload Default"))
        {
            script.Reload();
        }
    }
}