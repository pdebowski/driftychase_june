﻿using UnityEngine;
using System.Collections;

public class KeyAnimatorSetter : MonoBehaviour {
   
    [SerializeField]
    private Animator[] animators;
    [SerializeField]
    private Animator pendantAnimator;

    private bool isPressed;

    void Update()
    {

#if UNITY_EDITOR
        isPressed = Input.GetMouseButton(0);
#else
        isPressed = Input.touchCount > 0;
#endif

        foreach (var animator in animators)
        {
            animator.SetBool("isPause", isPressed);
        }

        pendantAnimator.SetBool("isClicked", isPressed);
    }
    
}
