﻿using UnityEngine;
using System.Collections;

public class RotationAnimation : BaseAnimation
{

    [SerializeField]
    private AnimationCurve rotationX;
    [SerializeField]
    private AnimationCurve rotationY;
    [SerializeField]
    private AnimationCurve rotationZ;
    [SerializeField]
    private float ratio = 1.0f;

    private Vector3 startRotation;

    protected override void Init()
    {
        startRotation = transform.localRotation.eulerAngles;
    }

    protected override void Calculate(float phase)
    {
        transform.localRotation = Quaternion.Euler(startRotation + new Vector3(rotationX.Evaluate(phase) * ratio, rotationY.Evaluate(phase) * ratio, rotationZ.Evaluate(phase) * ratio));
    }



}
