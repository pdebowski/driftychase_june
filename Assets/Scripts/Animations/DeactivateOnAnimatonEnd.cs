﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(BaseAnimation))]
public class DeactivateOnAnimatonEnd : MonoBehaviour {

	void Start()
    {
        BaseAnimation animation = GetComponent<BaseAnimation>();
        animation.OnAnimationEnd += Deactivate;
    }

    private void Deactivate()
    {
        gameObject.SetActive(false);
    }
}
