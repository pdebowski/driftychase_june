﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public abstract class BaseAnimation : MonoBehaviour
{
    public System.Action OnAnimationStart = delegate { };
    public System.Action OnAnimationEnd = delegate { };

    public float delay = 0;
    public float duration = 1;
    public bool useUnscaledTime = false;
    public bool startOnEnable = true;
    public bool repeat = false;
    public bool randomizePhaseOnStart = false;
    public bool deactivateOnAnimationEnd = false;

    protected float phase = 0;

    private bool animating = false;   
    private bool initialized = false;
    private bool wasEndAnimationReported = false;


    void OnEnable()
    {
        wasEndAnimationReported = false;
        if (startOnEnable)
        {
            StartCoroutine(StartAnim());
        }
    }

    void OnDisable()
    {
        StopAllCoroutines();
        Restart();
    }

    private IEnumerator StartAnim()
    {
        if (delay > 0)
        {
            yield return new WaitForSeconds(delay);
        }

        StartAnimation();
    }

    public void StartAnimation()
    {
        wasEndAnimationReported = false;
        Restart();
        animating = true;
        OnAnimationStart();
    }



    public void StopAnimation()
    {
        animating = false;
    }

    protected virtual void Restart()
    {
        if (!initialized)
        {
            Init();
            initialized = true;
        }

        if (randomizePhaseOnStart)
        {
            phase = Random.Range(0f, 1f);
        }
        else
        {
            phase = 0;
        }
        

        if (!gameObject.activeInHierarchy)
        {
            Calculate(phase);
        }

    }



    void Update()
    {
        if (!animating)
        {
            return;
        }

        Calculate(phase);
        phase += (useUnscaledTime == false ? Time.deltaTime : Time.unscaledDeltaTime) / duration;

        if (phase > 1)
        {
            if (repeat)
            {
                Restart();
                phase = 0;
            }
            else
            {
                if (!wasEndAnimationReported)
                {
                    OnAnimationEnd();
                    wasEndAnimationReported = true;
                    TryDeactivate();
                }
            }
        }
    }

    private void TryDeactivate()
    {
        if (deactivateOnAnimationEnd)
        {
            this.gameObject.SetActive(false);
        }
    }

    protected virtual void Init()
    {

    }

    protected abstract void Calculate(float phase);
}
