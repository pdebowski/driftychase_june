﻿using UnityEngine;
using System.Collections;

public class VelocityAnimation : BaseAnimation {

    [SerializeField]
    private float speed = 5;
    [SerializeField]
    private AnimationCurve velocityX;
    [SerializeField]
    private AnimationCurve velocityY;
    [SerializeField]
    private AnimationCurve velocityZ;

    private Vector3 startPosition;

    protected override void Init()
    {
        startPosition = transform.position;
    }

    protected override void Calculate(float phase)
    {
        transform.position += Quaternion.LookRotation(transform.forward) * new Vector3(velocityX.Evaluate(phase), velocityY.Evaluate(phase), velocityZ.Evaluate(phase)) * Time.deltaTime * speed;
    }


    protected override void Restart()
    {
        base.Restart();
        transform.position = startPosition;
    }


}
