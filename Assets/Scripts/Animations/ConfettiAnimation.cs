﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class ConfettiAnimation : MonoBehaviour
{

    [SerializeField]
    private int ammount;
    [SerializeField]
    private float startSpeed = 100f;
    [SerializeField]
    private float startSpeedRandomFactor = 0.5f;
    [SerializeField]
    private float startRotationSpeed = 10f;
    [SerializeField]
    private float rotationSpeedRandomFactor = 0.5f;
    [SerializeField]
    private float dragFactor = 0.01f;
    [SerializeField]
    private float gravity = 10f;
    [SerializeField]
    private float angleRange;
    [SerializeField]
    private SpawnRect[] spawnRects;

    [SerializeField]
    private GameObject[] prefabs;
    [SerializeField]
    private Color[] colors;

    private List<ConfettiParticle> particles;
    private float scaleFactor;

    void OnEnable()
    {
        scaleFactor = GetComponentInParent<Canvas>().transform.lossyScale.x;
        ClearParticles();
        CreateParticles();
    }

    private void FixedUpdate()
    {
        if (particles != null)
        {
            foreach (var particle in particles)
            {
                particle.velocity -= particle.velocity.normalized * Mathf.Pow(particle.velocity.magnitude, 2) * Time.deltaTime * dragFactor;
                particle.velocity -= new Vector2(0, 1) * gravity * Time.deltaTime;

                particle.transform.position = (Vector2)particle.transform.position + particle.velocity * Time.deltaTime * scaleFactor;
                particle.transform.localPosition = particle.transform.localPosition.ChangeZ(0);
                particle.transform.Rotate(particle.rotationSpeed * Time.deltaTime);
            }
        }
    }



    private void CreateParticles()
    {


        for (int i = 0; i < ammount; i++)
        {
            ConfettiParticle particle = new ConfettiParticle();
            GameObject prefab = prefabs.RandomElement();
            GameObject particleObject = Instantiate(prefab, transform) as GameObject;
            particle.transform = particleObject.transform;
            particle.transform.SetParent(transform);

            int indexOfSpawnRect = i / (ammount / spawnRects.Length);
            SpawnRect spawnRect = spawnRects[Mathf.Clamp(indexOfSpawnRect, 0, spawnRects.Length - 1)];
            particleObject.transform.position = spawnRect.GetRandomPosition();
            particleObject.transform.localPosition = particleObject.transform.localPosition.ChangeZ(0);
            float angle = 0 + Random.Range(-angleRange / 2f, angleRange / 2f);
            particle.velocity = new Vector2(Mathf.Sin(angle * Mathf.Deg2Rad), Mathf.Cos(angle * Mathf.Deg2Rad));
            particle.velocity *= startSpeed * Random.Range(1 - startSpeedRandomFactor, 1 + startSpeedRandomFactor);
            particle.rotationSpeed = new Vector3(Random.value, Random.value, Random.value);
            particle.rotationSpeed *= startRotationSpeed * Random.Range(1 - rotationSpeedRandomFactor, 1 + rotationSpeedRandomFactor);
            particle.transform.GetComponent<Image>().color = colors.RandomElement();
            particle.transform.localScale = prefab.transform.localScale;

            particles.Add(particle);
        }
    }

    private void ClearParticles()
    {
        if (particles != null)
        {
            foreach (var particle in particles)
            {
                Destroy(particle.transform.gameObject);
            }
        }

        particles = new List<ConfettiParticle>();
    }

    private class ConfettiParticle
    {
        public Transform transform;
        public Vector2 velocity;
        public Vector3 rotationSpeed;
    }

    [System.Serializable]
    private class SpawnRect
    {
        public Transform leftPointUp;
        public Transform leftPointDown;
        public Transform rightPointUp;
        public Transform rightPointDown;

        public Vector2 GetRandomPosition()
        {
            Vector2 randomPositionFactor = new Vector2(Random.value, Random.value);
            Vector2 positionUp = Vector2.Lerp(leftPointUp.position, rightPointUp.position, randomPositionFactor.x);
            Vector2 positionDown = Vector2.Lerp(leftPointDown.position, rightPointDown.position, randomPositionFactor.x);
            Vector2 position = Vector2.Lerp(positionDown, positionUp, randomPositionFactor.y);

            return position;
        }
    }
}
