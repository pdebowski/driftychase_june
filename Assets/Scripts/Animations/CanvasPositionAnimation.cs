﻿using UnityEngine;
using System.Collections;
using System;

public class CanvasPositionAnimation : BaseAnimation
{
    [SerializeField]
    private bool xAnimation;
    [SerializeField]
    private bool yAnimation;
    [SerializeField]
    private bool zAnimation;

    [SerializeField]
    private AnimationCurve xPosition;
    [SerializeField]
    private AnimationCurve yPosition;
    [SerializeField]
    private AnimationCurve zPosition;

    [SerializeField]
    private bool useLocalPosition = false;



    protected override void Calculate(float phase)
    {
        Vector3 newPosition;

        if (useLocalPosition)
        {
            newPosition = transform.localPosition;
        }
        else
        {
            newPosition = transform.position;
        }


        if (xAnimation)
        {
            newPosition.x = xPosition.Evaluate(phase) * Screen.width;
        }
        if (yAnimation)
        {
            newPosition.y = yPosition.Evaluate(phase) * Screen.height;
        }
        if (zAnimation)
        {
            newPosition.z = zPosition.Evaluate(phase);
        }


        if (useLocalPosition)
        {
            transform.localPosition = newPosition;
        }
        else
        {
            transform.position = newPosition;
        }


    }
}
