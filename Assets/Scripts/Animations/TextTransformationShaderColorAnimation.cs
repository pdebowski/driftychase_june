﻿using UnityEngine;
using System.Collections;

public class TextTransformationShaderColorAnimation : BaseAnimation
{

    [SerializeField]
    private AnimationCurve alphaCurve;
    [SerializeField]
    private float maxAlpha = 1;

    private TextTransformationShaderHelper textTransformation;

    protected override void Init()
    {
        textTransformation = GetComponent<TextTransformationShaderHelper>();
        SetAlpha(0);
    }

    protected override void Calculate(float phase)
    {
        SetAlpha(alphaCurve.Evaluate(phase) * maxAlpha);
    }


    protected override void Restart()
    {
        base.Restart();
        SetAlpha(0);
    }

    private void SetAlpha(float value)
    {

        Color newColor = textTransformation.TintColor;
        newColor.a = value;
        textTransformation.TintColor = newColor;

    }

    public void SetMaxAlpha(float value)
    {
        maxAlpha = value;
    }
}