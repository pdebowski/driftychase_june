﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class ImageAlphaAnimation : BaseAnimation {

    [SerializeField]
    private AnimationCurve alphaCurve;
    [SerializeField]
    private float maxAlpha = 1;

    private Image image;

    protected override void Init()
    {
        image = GetComponent<Image>();
        SetAlpha(0);
    }

    protected override void Calculate(float phase)
    {
        SetAlpha(alphaCurve.Evaluate(phase) * maxAlpha);
    }


    protected override void Restart()
    {
        base.Restart();
        SetAlpha(0);      
    }

    private void SetAlpha(float value)
    {
        CommonMethods.SetAlphaToImage(image, value);
    }

    public void SetMaxAlpha(float value)
    {
        maxAlpha = value;
    }
}
