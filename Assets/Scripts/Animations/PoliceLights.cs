﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PoliceLights : BaseAnimation
{
    [SerializeField]
    private float maxLightsScale = 1.5f;
    [SerializeField]
    private MeshRenderer[] blueLights;
    [SerializeField]
    private MeshRenderer[] redLights;
    [SerializeField]
    private AnimationCurve alphaCurve;

    protected override void Calculate(float phase)
    {
        float blueAlpha = alphaCurve.Evaluate(phase);
        float phaseWithOffset = phase + 0.5f;
        float redAlpha = alphaCurve.Evaluate(phaseWithOffset);

        SetLights(blueAlpha, blueLights);
        SetLights(redAlpha, redLights);
    }

    private void SetLights(float curveValue, MeshRenderer[] lights)
    {
        foreach (var light in lights)
        {
            light.transform.localScale = new Vector3(maxLightsScale, maxLightsScale, maxLightsScale) * curveValue;
            light.GetComponentInChildren<Light>().intensity = 2 * curveValue;
        }
    }


}
