﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[RequireComponent(typeof(CanvasGroup))]
public class CanvasGroupAlphaAnimation : BaseAnimation
{

    [SerializeField]
    private AnimationCurve alphaCurve;
    [SerializeField]
    private float maxAlpha = 1;

    private CanvasGroup canvasGroup;

    protected override void Init()
    {
        canvasGroup = GetComponent<CanvasGroup>();
        Calculate(0);
    }

    protected override void Calculate(float phase)
    {
        SetAlpha(alphaCurve.Evaluate(phase) * maxAlpha);
    }


    protected override void Restart()
    {
        base.Restart();
        Calculate(0);
    }

    private void SetAlpha(float value)
    {
        canvasGroup.alpha = value;
    }

}
