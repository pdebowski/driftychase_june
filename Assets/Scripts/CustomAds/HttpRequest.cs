using UnityEngine;
using System.Collections;

public class HttpRequest : MonoBehaviour
{
    public enum RequestError { NONE, EMPTY_RESPONSE, TIMEOUT, OTHER }

    public bool p_loading { get; private set; }
    public RequestError p_error { get; private set; }
    public string p_errorMessage { get; private set; }
    public byte[] p_fileData { get; private set; }
    public string p_fileText { get; private set; }

    private float m_timeout = 0;
    private WWW m_wwwRequest = null;
    private System.Action OnRequestFinished;

    //-------------------------------------------------
    void Awake()
    {
        p_loading = false;
        p_errorMessage = null;
        p_fileData = null;
        p_error = RequestError.NONE;
    }
    //-------------------------------------------------
    void Update()
    {

        if (m_wwwRequest != null)
        {
            m_timeout -= Time.deltaTime;

            if (!string.IsNullOrEmpty(m_wwwRequest.error))
            {
                p_errorMessage = m_wwwRequest.error;
                p_error = RequestError.OTHER;
                p_loading = false;
                m_wwwRequest = null;
                p_fileData = null;

                Debug.LogError("m_wwwRequest.error: " + p_errorMessage);
                SendCallback();
            }
            else if (m_wwwRequest.isDone && (m_wwwRequest.bytesDownloaded <= 0 || m_wwwRequest.text == null))
            {
                p_errorMessage = "REQUEST EMPTY RESPONSE";
                p_loading = false;
                m_wwwRequest = null;
                p_fileData = null;
                p_error = RequestError.EMPTY_RESPONSE;
                SendCallback();
            }
            else if (m_wwwRequest.isDone)
            {
                p_errorMessage = null;
                p_loading = false;

                try
                {
                    p_fileData = m_wwwRequest.bytes;
                    p_fileText = m_wwwRequest.text;
                    Debug.Log(string.Format("Respone for request - ({0})\nResponse{1} ", m_wwwRequest.url, p_fileText));
                    p_error = RequestError.NONE;
                }
                catch (System.Exception e)
                {
                    p_errorMessage = e.Message;
                    p_error = RequestError.OTHER;
                }

                m_wwwRequest = null;
                SendCallback();
            }
            else if (m_timeout < 0)
            {
                p_errorMessage = "REQUEST TIMEOUT";
                p_loading = false;
                p_fileData = null;
                p_error = RequestError.TIMEOUT;

                m_wwwRequest.Dispose();
                m_wwwRequest = null;
                SendCallback();
            }
        }
    }

    private void SendCallback()
    {
        if (OnRequestFinished != null)
        {
            OnRequestFinished();
            OnRequestFinished = null;
        }
    }
    //-------------------------------------------------
    public void SendRequest(string url, float timeout, WWWForm form, System.Action callback)
    {
        OnRequestFinished = callback;
        m_timeout = timeout;
        p_error = RequestError.NONE;
        p_errorMessage = null;

        if (form != null)
        {
            m_wwwRequest = new WWW(url, form);
        }
        else
        {
            m_wwwRequest = new WWW(url);
        }
        p_loading = true;
    }

}
