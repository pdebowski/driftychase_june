using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using SimpleJSON;
using System.Linq;
using System.IO;
using UnityEngine.UI;
using CrimsonAnalytics.MicroAnalytics;

public class FullScreenCustomAdsManager : MonoBehaviour
{
	[SerializeField]
	CustomAdsManager customAdsManager;
	[SerializeField]
	Image adsImage;
	[SerializeField]
	LoadingSceneManager loadingSceneManager;
	[SerializeField]
	GameObject Content;

    private bool canOpenAds;
    private int numberOfUpdate;


    void Awake()
    {
        numberOfUpdate = 0;
        canOpenAds = false;
        gameObject.SetActive(true);

    }

    private void Update()
    {
        if (numberOfUpdate > 2)
        {
            canOpenAds = true;
        }

        numberOfUpdate++;
    }

    public void LoadFullScreenAds()
	{
		customAdsManager = FindObjectOfType<CustomAdsManager>();
		if (customAdsManager.CanShowAds())
		{
			SetImage();
		}
		else
		{
			Destroy(gameObject);
		}
	}

	void SetImage()
	{

		string path;

		try
		{
            //	path = Application.persistentDataPath + customAdsManager.contentList.Find(item => item.Key.Contains(customAdsManager.FullScreenAdsGameName) && item.Key.Contains("FullScreen")).Key.Substring(customAdsManager.contentList.Find(item => item.Key.Contains(customAdsManager.FullScreenAdsGameName)).Key.IndexOf('/'));
            path = Application.persistentDataPath + "/" + customAdsManager.FullScreenAdsGameName + ".jpg";
            Texture2D image = new Texture2D(4, 4,TextureFormat.RGB24,false);

			image.LoadImage(File.ReadAllBytes(path));


			float recWidth = image.width * Mathf.Lerp(0.746f, 1, RatioFactor());
			float recHeight = image.height;// * Mathf.Lerp(0.746f, 1, RatioFactor());  // * (Mathf.Lerp(9f, 3, RatioFactor()) / Mathf.Lerp(16f, 4, RatioFactor()));

			float recX = (image.width - recWidth) / 2;
			float recy = (image.height - recHeight) / 2;

			Rect rec = new Rect(recX, 0, recWidth, recHeight);
			Sprite newSprite = Sprite.Create(image, rec, new Vector2(0, 0));
			adsImage.sprite = newSprite;
			Content.SetActive(true);
			gameObject.GetComponent<CanvasGroup>().alpha = 1;
			//gameObject.SetActive(true);
			loadingSceneManager.IsCustomAdShowing = true;

			
            MicroAnalytics.LogEvent("CrossPlatformAds", "FullScreenAdsShow", customAdsManager.FullScreenAdsGameName + "-Show", 1);
            GoogleAnalyticsV4.instance.LogEvent("CrossPlatformAds", "FullScreenAdsShow", customAdsManager.FullScreenAdsGameName + "-Show", 1);
		}
		catch (System.Exception e)
		{
			gameObject.SetActive(false);
            OnCloseAdsClick();
            Debug.Log("CrossPromo: not able " + e.Message);
            //DestroyImmediate(this.gameObject);
        }
    }

	public float RatioFactor()
	{
		float factor = 0.0f;
		float actualWidth = 0;

		float width9_16 = 9 / 16f;
		float width3_4 = 3 / 4f;


		if(IsPortrait())
		{
			actualWidth = (float)Screen.width / (float)Screen.height;
		}
		else
		{
			actualWidth = (float)Screen.height / (float)Screen.width;
		}
		

		float widther = width3_4 - width9_16;
		float actual = actualWidth - width9_16;

		factor = actual / widther;

		return factor;
	}

	bool IsPortrait()
	{
		return (float)Screen.height > (float)Screen.width;
	}

	public void StartGameOrStore()
	{
        if (canOpenAds)
        {

#if UNITY_EDITOR


            Application.OpenURL("https://play.google.com/store/apps/details?id=" + customAdsManager.GetFullScreenGameId());
#elif UNITY_ANDROID
		MicroAnalytics.LogEvent("CrossPlatformAds", "FullScreenAdsClicked", customAdsManager.FullScreenAdsGameName + "-OpenStore", 1);
		GoogleAnalyticsV4.instance.LogEvent("CrossPlatformAds", "FullScreenAdsClicked", customAdsManager.FullScreenAdsGameName + "-OpenStore", 1);
		Application.OpenURL("market://details?id=" + customAdsManager.GetFullScreenGameId());
#elif UNITY_IPHONE
        MicroAnalytics.LogEvent("CrossPlatformAds", "FullScreenAdsClicked", customAdsManager.FullScreenAdsGameName + "-OpenStore", 1);
		GoogleAnalyticsV4.instance.LogEvent("CrossPlatformAds", "FullScreenAdsClicked", customAdsManager.FullScreenAdsGameName + "-OpenStore", 1);
		Application.OpenURL("itms-apps://itunes.apple.com/app/id" + customAdsManager.GetFullScreenGameId());

#endif
            OnCloseAdsClick();
        }
    }

	public void OnCloseAdsClick()
	{
        MicroAnalytics.LogEvent("CrossPlatformAds", "FullScreenAdsClosed", customAdsManager.FullScreenAdsGameName + "-Closed", 1);
        GoogleAnalyticsV4.instance.LogEvent("CrossPlatformAds", "FullScreenAdsClosed", customAdsManager.FullScreenAdsGameName + "-Closed", 1);
		gameObject.SetActive(false);		
		loadingSceneManager.IsCustomAdShowing = false;

		//if (!RankingCloudAdapter.Instance.p_rankingInstance.p_IsAuthenticated)
		//{
		//	Invoke("ReconnectGameCenter", 0.2f);
		//}
	}

	//void ReconnectGameCenter()
	//{
	//	RankingCloudAdapter.Instance.p_rankingInstance.Login();
	//}
}
