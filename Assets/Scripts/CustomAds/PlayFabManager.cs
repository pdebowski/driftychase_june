using UnityEngine;
using System.Collections;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using SimpleJSON;




public class PlayFabManager : Singleton<PlayFabManager>
{
    const string GAME_NAME = "DC";

#if UNITY_EDITOR
    const bool LOCAL_IP = false;
#else
    const bool LOCAL_IP = false;
#endif

    string platformID;
    string FolderUrl
    {
        get
        {
            if (Debug.isDebugBuild && LOCAL_IP)
            {
                return "http://localhost/" + GAME_NAME + platformID + "/";
            }
            else
            {
                return GTMParameters.CrossPromoLink + GAME_NAME + platformID + "/";
            }
        }
    }
    //string FolderUrl { get { return "http://localhost/" + GAME_NAME + platformID + "/"; } }

    string ImagesUrl { get { return FolderUrl + "images/"; } }



    private bool isImageDownloaded = false;
    int howManyLoad;
    //bool loadFirstTime;
    int imageQuantity;

    [SerializeField]
    string Key;
    string localNotificationFile = "NotificationConfig.json";

    string contentVersionKey = "Version";

    HttpRequestPlayFab fileRequest;
    bool canDownloadContent;

    List<string> requestKeys = new List<string>();
    List<GamesInfo> gamesInfo = new List<GamesInfo>();
    List<string> imagesFileNameToDownload = new List<string>();


    private bool wasDownloadStarted;

    public string notificationJSONString { get { return PlayerPrefs.GetString("LocalNotificationConfigJSON", ""); } }


    new void Awake()
    {
        base.Awake();
        canDownloadContent = false;
    }

    void Start()
    {
        Invoke("LoadContent", 0.5f);
    }

    void LoadContent()
    {
        fileRequest = gameObject.AddComponent<HttpRequestPlayFab>();
        howManyLoad = 0;
        StartDownload();
    }

    void Update()
    {
        if (canDownloadContent && imagesFileNameToDownload.Count > 0)
        {
            canDownloadContent = false;

            Debug.Log("url " + ImagesUrl + imagesFileNameToDownload[0]);
            GetFile(ImagesUrl + imagesFileNameToDownload[0]);
        }
    }

    public void StartDownload()
    {

#if UNITY_ANDROID || UNITY_EDITOR
        platformID = "Android";
#elif UNITY_IPHONE
        platformID = "Ios";

#endif

        GetTitleFiles();

    }

    public void GetFile(string URL)
    {
        fileRequest.SendRequest(URL, 60);
        fileRequest.OnRequestFinished = SaveImageOnDisc;
    }


    public void LoadButtonAfterDownload()
    {
        if (imagesFileNameToDownload.Count == 0)
        {
            FindObjectOfType<CustomAdsManager>().LoadButtonAdsAfterDownload();
        }
    }

    public bool IsAllDownload()
    {
        if (howManyLoad == imageQuantity)
        {
            return true;
        }

        return false;
    }

    void OnGetTitleFinished()
    {
        string newGamesInfoStr = PlayerPrefs.GetString("NEW_CROSSPROMO_GAMES_INFO_JSON_STRING", "");

        if (!string.IsNullOrEmpty(newGamesInfoStr))
        {
            try
            {

                CrossPromoSimpleJSON.JSONArray JSONArrayObj = (CrossPromoSimpleJSON.JSONArray)CrossPromoSimpleJSON.JSONArray.Parse(newGamesInfoStr);


                for (int i = 0; i < JSONArrayObj.Count; i++)
                {
                    if (JSONArrayObj.AsArray[i]["GameName"] != null && JSONArrayObj.AsArray[i]["GameID"] != null
                        && JSONArrayObj.AsArray[i]["Probability"] != null && JSONArrayObj.AsArray[i]["Version"] != null)
                    {
                        gamesInfo.Add(new GamesInfo(
                                                    JSONArrayObj.AsArray[i]["GameName"].Value,
                                                    JSONArrayObj.AsArray[i]["GameID"].Value,
                                                    JSONArrayObj.AsArray[i]["HaveIcon"] != null ? JSONArrayObj.AsArray[i]["HaveIcon"].AsBool : false,
                                                    JSONArrayObj.AsArray[i]["Probability"].AsFloat,
                                                    JSONArrayObj.AsArray[i]["Version"].AsFloat
                                                    )
                                                    );
                    }
                    else
                    {
                        Debug.LogError("CrossPromo: JSON Error : " + (JSONArrayObj.AsArray[i]["GameName"] != null) + ":" + (JSONArrayObj.AsArray[i]["GameID"] != null) + ":" +
                         (JSONArrayObj.AsArray[i]["Probability"] != null) + ":" + (JSONArrayObj.AsArray[i]["Version"] != null));
                    }

                    CrossPromoSimpleJSON.JSONArray prevJSONArrayObj;
                    prevJSONArrayObj = (CrossPromoSimpleJSON.JSONArray)CrossPromoSimpleJSON.JSONArray.Parse(PlayerPrefs.GetString("CROSSPROMO_GAMES_INFO_JSON_STRING", "[]"));

                    if (string.IsNullOrEmpty(PlayerPrefs.GetString("CROSSPROMO_GAMES_INFO_JSON_STRING")) || prevJSONArrayObj.AsArray[i] == null
                        || !Mathf.Approximately(JSONArrayObj.AsArray[i]["Version"].AsFloat, prevJSONArrayObj.AsArray[i]["Version"].AsFloat))
                    {

                        imagesFileNameToDownload.Add("FullScreen" + JSONArrayObj.AsArray[i]["GameName"].Value + ".jpg");
                        if (JSONArrayObj.AsArray[i]["HaveIcon"] != null && JSONArrayObj.AsArray[i]["HaveIcon"].AsBool == true)
                        {
                            imagesFileNameToDownload.Add("Icon" + JSONArrayObj.AsArray[i]["GameName"].Value + ".jpg");
                        }
                    }

                }


                PlayerPrefs.SetString("CROSSPROMO_GAMES_INFO_JSON_STRING", newGamesInfoStr);
            }
            catch (System.Exception e)
            {
                Debug.LogError("CrossPromo: on json parsing " + e.Message + " ::: " + e.StackTrace);
                PlayerPrefs.SetString("NEW_CROSSPROMO_GAMES_INFO_JSON_STRING", "");
                PlayerPrefs.SetString("CROSSPROMO_GAMES_INFO_JSON_STRING", "");
            }
            canDownloadContent = true;
        }
        else
        {
            Debug.LogError("CrossPromo: empty or null json" + string.IsNullOrEmpty(newGamesInfoStr));
        }
    }


    void SaveImageOnDisc()
    {

        string errorMessage = fileRequest.p_errorMessage;
        bool isSuccess = fileRequest.p_error == HttpRequestPlayFab.RequestError.NONE;
        HttpRequestPlayFab.RequestError errorType = fileRequest.p_error;

        string imageFileName = imagesFileNameToDownload[0];
        imagesFileNameToDownload.RemoveAt(0);


        try
        {
            if (isSuccess)
            {
                File.WriteAllBytes(Application.persistentDataPath + "/" + imageFileName, fileRequest.p_fileData);
                Debug.Log("CrossPromo saving file : " + imageFileName);
                howManyLoad++;
            }
        }
        catch (System.Exception e)
        {
            isSuccess = false;
            Debug.LogWarning(e.Message);
        }

        if (!isSuccess)
        {
            errorType = HttpRequestPlayFab.RequestError.OTHER;
            Debug.LogWarning(errorMessage + " : " + imageFileName);
        }

        canDownloadContent = true;

        LoadButtonAfterDownload();
    }

    void GetTitleFiles()
    {
        requestKeys = new List<string>();
        requestKeys.Add(Key);
        requestKeys.Add(localNotificationFile);

        fileRequest.OnRequestFinished = OnGetTitleDataSuccess;
        Debug.Log("bbb " + (FolderUrl + requestKeys[0]));
        fileRequest.SendRequest(FolderUrl + requestKeys[0], 60);

    }

    void OnGetTitleDataSuccess()
    {
        string titleKey = requestKeys[0];

        bool isSuccess = fileRequest.p_error == HttpRequestPlayFab.RequestError.NONE;
        requestKeys.RemoveAt(0);

        Debug.Log("Title " + titleKey);

        if (isSuccess)
        {
            if (titleKey.Equals(Key))
            {
                string asString = System.Text.Encoding.UTF8.GetString(fileRequest.p_fileData);
                PlayerPrefs.SetString("NEW_CROSSPROMO_GAMES_INFO_JSON_STRING", asString);
            }
            else if (titleKey.Equals(localNotificationFile))
            {
                string asString = System.Text.Encoding.UTF8.GetString(fileRequest.p_fileData);
                PlayerPrefs.SetString("LocalNotificationConfigJSON", asString);
            }
            else
            {
                Debug.LogError("CrossPromo: Title Data wrong; key: " + titleKey + " left: " + string.Join(",", requestKeys.ToArray()));
            }
        }
        else
        {
            Debug.LogWarning(fileRequest.p_errorMessage);
        }


        if (requestKeys.Count > 0)
        {
            fileRequest.OnRequestFinished = OnGetTitleDataSuccess;
            fileRequest.SendRequest(FolderUrl + requestKeys[0], 60);
        }
        else
        {
            OnGetTitleFinished();
        }
    }
}
