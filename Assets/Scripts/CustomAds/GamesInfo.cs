public class GamesInfo
{
    public string GameName { get; set; }
    public string GameID { get; set; }
    public bool HaveIcon { get; set; }
    public float Probability { get; set; }
    public float Version { get; set; }


    public GamesInfo(string gameName, string gameID, bool haveIcon, float probability, float version)
    {
        GameName = gameName;
        GameID = gameID;
        HaveIcon = haveIcon;
        Probability = probability;
        Version = version;
    }
}
