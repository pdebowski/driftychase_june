﻿using UnityEngine;
using System.Collections;

public class HttpRequestPlayFab : MonoBehaviour {
   // private const string OBTAIN_GIFT_CODE_URL = "asdasD";
   // private const string USE_GIFT_URL = "asdasD";

    public System.Action OnRequestFinished = delegate { };

    public enum RequestError {NONE,EMPTY_RESPONSE,TIMEOUT, OTHER } 

    
    public bool p_loading { get; private set; }
    public RequestError p_error { get; private set; }
    public string p_errorMessage { get; private set; }
    public byte[] p_fileData { get; private set; }
    
    float m_timeout=0;
    WWW m_wwwRequest = null;


	//-------------------------------------------------
    void Awake()
    {
        p_loading = false;
        p_errorMessage = null;
		p_fileData = null;
        p_error = RequestError.NONE;
    }
	//-------------------------------------------------
	void Update()
	{
		
		if (m_wwwRequest != null)
		{
			m_timeout -= Time.deltaTime;
			
			if (!string.IsNullOrEmpty(m_wwwRequest.error))
			{
				p_errorMessage = m_wwwRequest.error;
				p_error = RequestError.OTHER;
				p_loading = false;
				m_wwwRequest = null;
				p_fileData = null;
				OnRequestFinished();
			}
			else if (m_wwwRequest.isDone && (m_wwwRequest.bytesDownloaded <= 0 || m_wwwRequest.text==null))
			{
				p_errorMessage = "REQUEST EMPTY RESPONSE";
				p_loading = false;
				m_wwwRequest = null;
				p_fileData = null;
				p_error = RequestError.EMPTY_RESPONSE;
				OnRequestFinished();
			}
			else if (m_wwwRequest.isDone)
			{
				p_errorMessage = null;
				p_loading = false;
				
				try {
					p_fileData = m_wwwRequest.bytes;
					p_error = RequestError.NONE;
				}
				catch (System.Exception e){
					p_errorMessage=e.Message;
					p_error=RequestError.OTHER;
				}
				
				m_wwwRequest = null;
				OnRequestFinished();
			}
			else if (m_timeout < 0)
			{
				p_errorMessage = "REQUEST TIMEOUT";
				p_loading = false;
				p_fileData = null;
				p_error = RequestError.TIMEOUT;
				
				m_wwwRequest.Dispose();
				m_wwwRequest = null;
				OnRequestFinished();
			}
		}
	}
    //-------------------------------------------------
    public void SendRequest(string url, float timeout,WWWForm form=null)
    {
        m_timeout = timeout;
		p_error = RequestError.NONE;
		p_errorMessage = null;
        if (form != null)
        {
            m_wwwRequest = new WWW(url, form);
        }
        else
        {
            m_wwwRequest = new WWW(url);
        }
        p_loading = true;
    }
    
}
