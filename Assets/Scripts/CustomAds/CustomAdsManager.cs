using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Linq;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;



public class CustomAdsManager : MonoBehaviour
{

	[SerializeField]
	ButtonCustomAdsManager buttonCustomAdsManager;
	[SerializeField]
	FullScreenCustomAdsManager fullScreenCustomAdsManager;

    public List<GamesInfo> gamesInfo;

	[HideInInspector]
	public string FullScreenAdsGameName;
	[HideInInspector]
	public string ButtonAdsGameName;
	bool gameIsSelected;

	bool startDownload;

	void Awake()
	{
        gamesInfo = new List<GamesInfo>();

        bool enableHouseAds;

        try
		{
			enableHouseAds = GTMParameters.EnableHouseAds;
		}
		catch (System.Exception)
		{

			enableHouseAds = false;
        }
		 
		if (enableHouseAds)
		{
			LoadAds();
		}
    }

	void Update()
	{
		
	}

	void StartLoadAds()
	{

	}


	void LoadAds()
	{
		LoadGamesInfo();
		IsDownloadAllContent();

        if (gamesInfo != null)
        {
            SelectGamesToAds();
			//buttonCustomAdsManager.LoadButtonAds();
			FullScreenCustomAdsManager fullScreenCustomAdsManager = FindObjectOfType<FullScreenCustomAdsManager>();

			if(fullScreenCustomAdsManager != null && gameIsSelected==true)
			{
				fullScreenCustomAdsManager.LoadFullScreenAds();
			}
			
        }
		else
		{
			gameIsSelected = false;

			LoadingSceneManager loadingSceneManager = FindObjectOfType<LoadingSceneManager>();
			
			if(loadingSceneManager != null)
			{
				loadingSceneManager.IsCustomAdShowing = false;
			}
			//if (!RankingCloudAdapter.Instance.p_rankingInstance.p_IsAuthenticated)
			//{
			//	Invoke("ReconnectGameCenter", 0.2f);
			//}
		}
	}

	public void LoadButtonAdsAfterDownload()
	{
		LoadGamesInfo();
		SelectGamesToAds();

		//buttonCustomAdsManager.LoadButtonAds();
	}

	void LoadGamesInfo()
	{
        string gamesInfoJson = PlayerPrefs.GetString("CROSSPROMO_GAMES_INFO_JSON_STRING");

        if (gamesInfoJson != "")
        {
            CrossPromoSimpleJSON.JSONArray JSONArrayObj = (CrossPromoSimpleJSON.JSONArray)CrossPromoSimpleJSON.JSONArray.Parse(PlayerPrefs.GetString("CROSSPROMO_GAMES_INFO_JSON_STRING"));


            for (int i = 0; i < JSONArrayObj.Count; i++)
            {
                if (JSONArrayObj.AsArray[i]["GameName"] != null && JSONArrayObj.AsArray[i]["GameID"] != null && JSONArrayObj.AsArray[i]["Probability"] != null
                                    && JSONArrayObj.AsArray[i]["Version"] != null)
                {
                    gamesInfo.Add(new GamesInfo(
                                                JSONArrayObj.AsArray[i]["GameName"].Value,
                                                JSONArrayObj.AsArray[i]["GameID"].Value,
                                                JSONArrayObj.AsArray[i]["HaveIcon"] != null ? JSONArrayObj.AsArray[i]["HaveIcon"].AsBool : false,
                                                JSONArrayObj.AsArray[i]["Probability"].AsFloat,
                                                JSONArrayObj.AsArray[i]["Version"].AsFloat
                                                )
                                                );
                }
                else
                {
                    Debug.LogError("CrossPromo : parsing problem " + (JSONArrayObj.AsArray[i]["GameName"] != null) + " :: " + (JSONArrayObj.AsArray[i]["GameID"] != null)
                        + "::" + (JSONArrayObj.AsArray[i]["Probability"] != null) + "::" + (JSONArrayObj.AsArray[i]["Version"] != null));
                }
            }
        }
    }

	public bool CanShowAds()
	{
		return gamesInfo != null && gameIsSelected && IsDownloadAllContent();
	}

    bool IsDownloadAllContent()
    {
        if (gamesInfo == null || gamesInfo.Count == 0)
        {
            return false;
        }

        foreach (var item in gamesInfo)
        {
            string path = Application.persistentDataPath + "/" + "FullScreen" + item.GameName + ".jpg";
            bool exist = File.Exists(path);

            if (item.HaveIcon)
            {
                exist = exist && File.Exists(Application.persistentDataPath + "/" + "Icon" + item.GameName + ".jpg");
            }


            if (!exist)
            {
                Debug.LogWarning("CrossPromo: clearing bc path doesnt exists " + (Application.persistentDataPath + "/" + "FullScreen" + item.GameName + ".jpg"));
                ClearAll();
                return exist;
            }
        }

        return true;
    }

    void ClearAll()
	{
        PlayerPrefs.SetString("CROSSPROMO_GAMES_INFO_JSON_STRING", "");
        gamesInfo.Clear();
	}

    void SelectGamesToAds()
    {
        if (gamesInfo.Count == 0)
        {
            return;
        }

        string randomGameName = RandomGame();

        if (gamesInfo.First(v => v.GameName.Equals(randomGameName)).HaveIcon)
        {
            ButtonAdsGameName = "Icon" + randomGameName;
        }
        FullScreenAdsGameName = "FullScreen" + randomGameName;

        gameIsSelected = ButtonAdsGameName != null && FullScreenAdsGameName != null ? true : false;
    }

    string RandomGame()
	{
		float random = Random.value;
		float curVal = 0;

		foreach (GamesInfo item in gamesInfo.OrderBy(x => x.Probability))
		{
			curVal += item.Probability;

			if (curVal >= random)
			{
				return item.GameName;
			}
		}

		return gamesInfo.ElementAt(0).GameName;
	}

	public string GetButtonGameId()
	{
		return gamesInfo.Find(x => x.GameName == ButtonAdsGameName.Replace("Icon", "")).GameID;
    }

	public string GetFullScreenGameId()
	{
		return gamesInfo.Find(x => x.GameName == FullScreenAdsGameName.Replace("FullScreen", "")).GameID;
    }

	void ReconnectGameCenter()
	{
		//RankingCloudAdapter.Instance.p_rankingInstance.Login();
	}
}
