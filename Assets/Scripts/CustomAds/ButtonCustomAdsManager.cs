﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;
using System.Linq;
using System.IO;
using UnityEngine.UI;
using CrimsonAnalytics.MicroAnalytics;

public class ButtonCustomAdsManager : MonoBehaviour {

	[SerializeField]
	CustomAdsManager customAdsManager;
	[SerializeField]
	Image icon;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void Awake()
	{
		
    }

	public void LoadButtonAds()
	{
		if (customAdsManager.CanShowAds())
		{
			SetIcon();
		}
		else
		{
			gameObject.SetActive(false);
		}
	}

	void SetIcon()
	{
		string path;

		try
		{
			path = Application.persistentDataPath + "/" + customAdsManager.ButtonAdsGameName + ".jpg";
            Texture2D image = new Texture2D(4, 4);

			image.LoadImage(File.ReadAllBytes(path));

			icon.material.SetTexture("_ImageTex", image);

			gameObject.GetComponent<CanvasGroup>().alpha = 1;

			gameObject.SetActive(true);
		}
		catch (System.Exception)
		{
			gameObject.SetActive(false);
		}
	}

	public void OnButtonAdsClick()
	{
#if UNITY_EDITOR
		Application.OpenURL("https://play.google.com/store/apps/details?id=" + customAdsManager.GetButtonGameId());
#elif UNITY_ANDROID
        MicroAnalytics.LogEvent("CrossPlatformAds", "ButtonAdsClicked", customAdsManager.ButtonAdsGameName, 1);
		GoogleAnalyticsV4.instance.LogEvent("CrossPlatformAds", "ButtonAdsClicked", customAdsManager.ButtonAdsGameName, 1);
		Application.OpenURL("market://details?id=" + customAdsManager.GetButtonGameId());
#elif UNITY_IPHONE
        MicroAnalytics.LogEvent("CrossPlatformAds", "ButtonAdsClicked", customAdsManager.ButtonAdsGameName, 1);
		GoogleAnalyticsV4.instance.LogEvent("CrossPlatformAds", "ButtonAdsClicked", customAdsManager.ButtonAdsGameName, 1);
		Application.OpenURL("itms-apps://itunes.apple.com/app/id" + customAdsManager.GetButtonGameId());
#endif
    }
}
