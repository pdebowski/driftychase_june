﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CornerButton : Button
{

    public StorePageAnimation pageAnimation;

    private bool isActive = true;

    public void Deactivate(bool deactivate)
    {
        isActive = !deactivate;
    }

    public void Init(StorePageAnimation pageAnimation)
    {
        this.pageAnimation = pageAnimation;
    }

    public override void OnPointerDown(UnityEngine.EventSystems.PointerEventData eventData)
    {
        if (isActive)
        {
            pageAnimation.OnPointerDown();
        }
    }

    public override void OnPointerUp(UnityEngine.EventSystems.PointerEventData eventData)
    {
        pageAnimation.OnPointerUp();
    }

}
