﻿using UnityEngine;
using System.Collections;
using System;

public class Screenshot : BaseAnimation
{
    [SerializeField]
    private CanvasGroup fade;
    [SerializeField]
    private float maxFadeAlpha;
    [SerializeField]
    private Transform targetTransform;
    [SerializeField]
    private AnimationCurve animationCurve;

    private Vector3 startPosition;
    private Quaternion startRotation;
    private Vector3 startScale;

    private bool reverse = false;

    protected override void Init()
    {
        startPosition = transform.position;
        startRotation = transform.rotation;
        startScale = transform.localScale;
        OnAnimationStart += OnnStart;
    }

    protected override void Restart()
    {
        base.Restart();
        reverse = false;
    }

    private void OnnStart()
    {
        fade.interactable = true;
        fade.blocksRaycasts = true;
    }



    public void StartReverseAnimation()
    {
        StartAnimation();
        reverse = true;
        fade.interactable = false;
        fade.blocksRaycasts = false;
    }



    protected override void Calculate(float phase)
    {
        if (reverse)
        {
            phase = 1 - phase;
        }

        transform.position = Vector3.Lerp(startPosition, targetTransform.position, animationCurve.Evaluate(phase));
        transform.localRotation = Quaternion.Lerp(startRotation, targetTransform.rotation, animationCurve.Evaluate(phase));
        transform.localScale = Vector3.Lerp(startScale, targetTransform.localScale, animationCurve.Evaluate(phase));
        fade.alpha = Mathf.Lerp(0, maxFadeAlpha, animationCurve.Evaluate(phase));

    }
}
