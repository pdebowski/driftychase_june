﻿using UnityEngine;
using System.Collections;

public class LevelSelectionForceActivator : MonoBehaviour
{

    [SerializeField]
    private MenuCard menuCards;
    [SerializeField]
    private GameModeSelectionManager gameModeSelectionManager;

    void Start()
    {
        if (GameplayManager.CurrentGameMode != null)
        {
            menuCards.ShowInstantCard();
            gameModeSelectionManager.SelectGameMode(GameplayManager.CurrentGameMode.Mode);
        }
    }
}
