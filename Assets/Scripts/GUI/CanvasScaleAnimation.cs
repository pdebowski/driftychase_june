﻿using UnityEngine;
using System.Collections;

public class CanvasScaleAnimation : BaseAnimation {


    [SerializeField]
    private AnimationCurve scaleXCurve;
    [SerializeField]
    private AnimationCurve scaleYCurve;

    Vector3 startScale;

    


    protected override void Init()
    {
        base.Init();
        startScale = transform.localScale;
    }

    protected override void Restart()
    {
        base.Restart();
        Calculate(0);
    }

    protected override void Calculate(float phase)
    {
        Vector3 newScale = new Vector3(scaleXCurve.Evaluate(phase), scaleYCurve.Evaluate(phase), 1);
        transform.localScale = CommonMethods.Vector3Multiply(newScale, startScale);
    }



}
