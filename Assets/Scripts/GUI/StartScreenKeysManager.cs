using UnityEngine;
using System.Collections;

public class StartScreenKeysManager : MonoBehaviour
{
    public StartScreenButtonType LastChoosedButton { get; set; }
    public static bool isLeaderboardsButtonEnabled = true;
    public static bool isStartKeysAnimAvailable = true;

    [SerializeField]
    private Camera canvasCamera;
    [SerializeField]
    private StartGameSceneManager startGameSceneManager;
    [SerializeField]
    private PositionAnimation startAnimation;
    [SerializeField]
    private PositionAnimation endAnimation;
    [SerializeField]
    private AudioSource keysStartAudioSource;
    [SerializeField]
    private ButtonSounds buttonSounds;

    private bool wasGameStarted = false;
    private int blocksCount = 0;
    private bool forceBlock = false;
    private float clickButtonTimeDiff = 2;
    private Coroutine unblockCoroutine = null;

    void Start()
    {
        if (isStartKeysAnimAvailable)
        {
            GetComponent<Animator>().SetTrigger("start");
            transform.position -= new Vector3(0, 100, 0);
            startAnimation.enabled = true;
            StartCoroutine(StartKeysSoundCor(0.4f));
            TryUnblockWithDelay(0.4f);
        }
        else
        {
            TryUnblockWithDelay(0.2f);
        }
    }

    private void TryUnblockWithDelay(float delay)
    {
        if (!forceBlock)
        {
            unblockCoroutine = StartCoroutine(UnblockWithDelay(delay));
        }
    }

    private IEnumerator UnblockWithDelay(float delay)
    {
        yield return new WaitForSeconds(delay);
        Unblock();
    }

    private IEnumerator StartKeysSoundCor(float daley)
    {
        yield return new WaitForSeconds(daley);
        SoundManager.PlaySFX(keysStartAudioSource);
    }

    public void Block()
    {
        forceBlock = true;
        blocksCount++;
        if (unblockCoroutine != null)
        {
            StopCoroutine(unblockCoroutine);
            unblockCoroutine = null;
        }
    }

    public void Unblock()
    {
        if(blocksCount > 0)
        {
            blocksCount--;
        }   
    }

    void Update()
    {
        if (blocksCount > 0)
        {
            return;
        }
        else if(blocksCount <0)
        {
            Debug.LogError("Blocks Count Error. Less than 0");
        }

        clickButtonTimeDiff += Time.unscaledDeltaTime;
        SetFalse();
        var ray = canvasCamera.ScreenPointToRay(Input.mousePosition);
        var hit = new RaycastHit();
        if (Physics.Raycast(ray, out hit) && clickButtonTimeDiff > 1)
        {
            if ((Input.GetMouseButton(0) || Input.GetMouseButtonDown(0)))
            {
                StartScreenButton button = hit.collider.GetComponent<StartScreenButton>();
                if (button == null)
                {
                    return;
                }
                StartScreenButtonType type = button.buttonType;
                LastChoosedButton = type;
                TurnOnAnimation(LastChoosedButton);
            }
            else
            {

                if (LastChoosedButton != StartScreenButtonType.None)
                {
                    InvokeButton(LastChoosedButton);
                }
            }
        }
        else
        {
            LastChoosedButton = StartScreenButtonType.None;
        }
    }

    public void TurnOnAnimation(StartScreenButtonType type)
    {

        switch (type)
        {
            case StartScreenButtonType.Options:
                GetComponent<Animator>().SetBool("Button01", true);
                break;
            case StartScreenButtonType.Leaderboards:
                GetComponent<Animator>().SetBool("Button03", true);
                break;
            case StartScreenButtonType.Achievements:
                GetComponent<Animator>().SetBool("Button02", true);
                break;
            case StartScreenButtonType.Shop:
                GetComponent<Animator>().SetBool("Button04", true);
                break;
            case StartScreenButtonType.RateMe:
            case StartScreenButtonType.Vip:
                GetComponent<Animator>().SetBool("Button03", true);
                break;
            case StartScreenButtonType.GameCenter:
                GetComponent<Animator>().SetBool("Button02", true);
                break;
            case StartScreenButtonType.StartGame:
                GetComponent<Animator>().SetBool("StartEngine", true);


                break;
            default:
                break;
        }
    }



    public void InvokeButton(StartScreenButtonType type)
    {

        if (type != StartScreenButtonType.StartGame)
        {
            buttonSounds.Play();
        }

        switch (type)
        {
            case StartScreenButtonType.Options:
                startGameSceneManager.OnOptionsButtonClick();
                clickButtonTimeDiff = 0;
                break;
            case StartScreenButtonType.GameCenter:
            case StartScreenButtonType.Leaderboards:
                if (isLeaderboardsButtonEnabled)
                {
                    startGameSceneManager.OnLeaderboardsButtonClick();
                    clickButtonTimeDiff = 0;
                }
                break;
            case StartScreenButtonType.Achievements:
                startGameSceneManager.OnAchievementsButtonClick();
                clickButtonTimeDiff = 0;
                break;
            case StartScreenButtonType.Shop:
                startGameSceneManager.OnStoreButtonClick();
                clickButtonTimeDiff = 0;
                break;
            case StartScreenButtonType.RateMe:
                startGameSceneManager.RateMe();
                clickButtonTimeDiff = 0;
                break;
            case StartScreenButtonType.Vip:
                startGameSceneManager.OnVIPButtonClick();
                clickButtonTimeDiff = 0;
                break;
            case StartScreenButtonType.StartGame:
                startGameSceneManager.StartButtonClick();
                endAnimation.enabled = true;
                clickButtonTimeDiff = 0;
                if (!wasGameStarted)
                {
                    PrefabReferences.Instance.Player.GetComponent<PlayerSounds>().EngineStart();
                    wasGameStarted = true;
                }
                break;
            default:
                break;
        }

    }



    private void SetFalse()
    {
        GetComponent<Animator>().SetBool("Button01", false);
        GetComponent<Animator>().SetBool("Button02", false);
        GetComponent<Animator>().SetBool("Button03", false);
        GetComponent<Animator>().SetBool("Button04", false);
        GetComponent<Animator>().SetBool("StartEngine", false);
    }
}
