﻿using UnityEngine;
using System.Collections;

public class PauseButtonStateManager : MonoBehaviour
{
    [SerializeField]
    private GameObject holder;

    void Update()
    {
        if (GameManager.Instance.CurrentState == GameManager.GameState.Playing && !holder.activeInHierarchy)
        {
            holder.SetActive(true);
        }
        else if (GameManager.Instance.CurrentState != GameManager.GameState.Playing && holder.activeInHierarchy)
        {
            holder.SetActive(false);
        }
    }
}
