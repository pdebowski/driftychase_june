﻿using UnityEngine;
using System.Collections;

public class RankingTabButton : MonoBehaviour {

	public void OnGlobalRankingTabButtonClick()
    {
        if (CrimsonPlugins.RankingCloudAdapter.Instance.p_rankingInstance.p_IsAuthenticated)
        {
            CrimsonPlugins.RankingCloudAdapter.Instance.p_rankingInstance.ShowLeaderboards();
        }
        else
        {
            CrimsonPlugins.RankingCloudAdapter.Instance.p_rankingInstance.Login();
        }
    }
}
