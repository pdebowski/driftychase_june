﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GridLayoutHeightAdjuster : MonoBehaviour
{
    [SerializeField]
    private bool alignToTop = true;
    [SerializeField]
    private ScrollRect parentScrollRect;
    [SerializeField]
    private AnimationCurve alphaCurve;
    [SerializeField]
    private float duration = 0.15f;

    private float actualWidth = 0;
    private float actualHeight = 0;
    private GridLayoutGroup gridLayoutGroup;
    private CanvasGroup canvasGroup;

    void Awake()
    {
        gridLayoutGroup = GetComponent<GridLayoutGroup>();
        canvasGroup = GetComponent<CanvasGroup>();
    }

    void OnDisable()
    {
        GetComponent<CanvasGroup>().alpha = 0;
        StopAllCoroutines();
    }

    void OnEnable()
    {
        StartCoroutine(AlphaAnimCor());
    }

    void Start()
    {
        Refresh(true);
    }

    void Update()
    {
        Refresh();
    }

    private void Refresh(bool force = false)
    {
        if (force || !Mathf.Approximately(actualWidth, gridLayoutGroup.minWidth) || !Mathf.Approximately(actualHeight, gridLayoutGroup.minHeight))
        {
            RectTransform rect = GetComponent<RectTransform>();
            actualWidth = gridLayoutGroup.minWidth;
            actualHeight = gridLayoutGroup.minHeight;
            rect.sizeDelta = new Vector2(actualWidth, actualHeight);

            if (alignToTop)
            {
                float yPos = -(rect.sizeDelta.y - transform.parent.GetComponent<RectTransform>().sizeDelta.y) / 2;
                rect.anchoredPosition = new Vector2(rect.anchoredPosition.x, yPos);
                parentScrollRect.velocity = Vector2.zero;
            }
        }
    }

    private IEnumerator AlphaAnimCor()
    {
        yield return null;
        float phase = 0;

        while (phase < 1)
        {
            phase += Time.deltaTime / duration;
            canvasGroup.alpha = alphaCurve.Evaluate(phase);
            yield return null;
        }
    }
}
