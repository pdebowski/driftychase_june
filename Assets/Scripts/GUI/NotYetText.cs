using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class NotYetText : MonoBehaviour
{

    private const float SHOW_TIME = 0.5f;

    [SerializeField]
    private GameObject textGameObject;

    private float activeTimeLeft = 0;

    void Awake()
    {
        textGameObject.SetActive(false);
    }

    void OnEnable()
    {
        PlayerInput.OnTurnButtonClickedDisabled += OnTurnButtonClicked;
    }

    void OnDisable()
    {
        PlayerInput.OnTurnButtonClickedDisabled -= OnTurnButtonClicked;
    }

    void Update()
    {
        activeTimeLeft -= Time.unscaledDeltaTime;

        if ((activeTimeLeft <= 0 && textGameObject.activeInHierarchy == true) || Time.timeScale == 0)
        {
            textGameObject.SetActive(false);
        }
    }

    private void OnTurnButtonClicked()
    {
        if (Tutorial.IsTutorial && Time.timeScale != 0)
        {
            textGameObject.SetActive(false);
            textGameObject.SetActive(true);
            textGameObject.GetComponent<RectTransform>().position = Input.mousePosition.AddY(25 * Mathf.Max(Screen.dpi, 200) / 100);
            if (PlatformRecognition.IsTV)
            {
                textGameObject.GetComponent<RectTransform>().localPosition = Vector3.zero;
            }
            activeTimeLeft = SHOW_TIME;
        }
    }

    public void HideText()
    {
        textGameObject.SetActive(false);
    }
}
