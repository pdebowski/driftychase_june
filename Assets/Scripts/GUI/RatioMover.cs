﻿using UnityEngine;
using System.Collections;

public class RatioMover : MonoBehaviour
{

    public Vector2 localPos16_9;
    public Vector2 localPos4_3;
    public bool local;
    public bool anchored;

    void OnEnable()
    {
        Refresh(PrefabReferences.Instance.AdAdapter.BannerActive);
        PrefabReferences.Instance.AdAdapter.OnBannerVisible += Refresh;
    }

    void OnDisable()
    {
        PrefabReferences.Instance.AdAdapter.OnBannerVisible -= Refresh;
    }

    void Refresh(bool visible)
    {
        if (!PlatformRecognition.IsTV)
        {
            if (local)
            {
                Vector2 newLocalPos = Vector2.Lerp(localPos16_9, localPos4_3, CommonMethods.IsPortrait() ? CommonMethods.GetRatioFactor(visible) : 0);
                transform.localPosition = newLocalPos;
            }
            else if (anchored)
            {
                RectTransform rectTransform = GetComponent<RectTransform>();
                Vector2 newLocalPos = Vector2.Lerp(localPos16_9, localPos4_3, CommonMethods.IsPortrait() ? CommonMethods.GetRatioFactor(visible) : 0);
                rectTransform.anchoredPosition = newLocalPos;
            }
            else
            {
                Vector2 newLocalPos = Vector2.Lerp(localPos16_9, localPos4_3, CommonMethods.IsPortrait() ? CommonMethods.GetRatioFactor(visible) : 0);
                transform.position = newLocalPos;
            }
        }
    }

}
