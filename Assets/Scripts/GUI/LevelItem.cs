﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LevelItem : MonoBehaviour
{

    [SerializeField]
    private GameObject bgWhite;
    [SerializeField]
    private GameObject bgGreen;
    [SerializeField]
    private GameObject okTick;
    [SerializeField]
    private GameObject lockIcon;
    [SerializeField]
    private Text levelNumberText;
    [SerializeField]
    private GameObject videoAdIcon;

    private int levelNumber = -1;
    private bool isUnlocked = false;
    private bool isAvailableForVideoAd = false;

    public void SetItem(bool isUnlocked, bool isCompleted, bool isAvailableForVideoAd, int levelNumber)
    {
        this.levelNumber = levelNumber;
        this.isUnlocked = isUnlocked;
        this.isAvailableForVideoAd = isAvailableForVideoAd;
        levelNumberText.text = (levelNumber + 1).ToString();
        bgWhite.SetActive(!isCompleted);
        bgGreen.SetActive(isCompleted);
        okTick.SetActive(isCompleted);
        lockIcon.SetActive(!isUnlocked);
        videoAdIcon.SetActive(isAvailableForVideoAd);
        levelNumberText.gameObject.SetActive(isUnlocked);
    }

    public void SetThisLevel()
    {
        if (isUnlocked)
        {
            GetComponentInParent<LevelSelectionManager>().SetLevel(levelNumber);
            return;
        }

        if (isAvailableForVideoAd && RewardAd.IsReady())
        {
            RewardAd.Show(null, OnVideoAdDisplayd);
            return;
        }

    }

    private void OnVideoAdDisplayd(bool wasFullyDisplayd)
    {
        if (wasFullyDisplayd)
        {
            GameplayManager.CurrentGameMode.Unlock(levelNumber);
            PrefsDataFacade.Instance.SaveGameStateCloud();

            GetComponentInParent<LevelSelectionManager>().SelectGameplayMode(GameplayManager.CurrentGameMode.Mode);
        }
    }
}
