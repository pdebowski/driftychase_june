﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.Events;

public class ButtonInsideScroll : MonoBehaviour, IPointerClickHandler
{
    [SerializeField]
    private UnityEvent PointerClick;

    public void OnPointerClick(PointerEventData eventData)
    {
        if (PointerClick != null && this.isActiveAndEnabled)
        {
            PointerClick.Invoke();
        }

    }
}
