﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class DailyGiftCounterText : MonoBehaviour
{
    [SerializeField]
    private string availableText = "AVAILABLE";
    [SerializeField]
    private string alternativeColor = "#FFCC00FF";
    [SerializeField]
    private string beforeTimeSpanText = "In";
    [SerializeField]
    private Text text;

    void Awake()
    {
        text = GetComponent<Text>();
    }

    void Update()
    {

        if (PrefabReferences.Instance.DailyGiftManager.IsAvailable())
        {
            text.text = string.Format("<color={0}>{1}</color>", alternativeColor, availableText);
        }
        else
        {
            text.text = string.Format("{0}<color={1}>{2}</color>", beforeTimeSpanText, alternativeColor, PrefabReferences.Instance.DailyGiftManager.GetTimeLeft());
        }
    }
}
