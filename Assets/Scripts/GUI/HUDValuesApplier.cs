﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HUDValuesApplier : MonoBehaviour
{

    private const string TOTAL_CASH_TEXT = "TOTAL:  ";

    [SerializeField]
    private Text scoreText;
    [SerializeField]
    private Text sessionCashText;
    [SerializeField]
    private Text totalCashText;

    private bool isFirstOnEnable = true;

    void OnEnable()
    {
        if (!isFirstOnEnable)
        {
            Refresh();
        }

        ScoreCounter.OnCashOrScoreChanged += Refresh;
        isFirstOnEnable = false;
    }

    void OnDisable()
    {
        Refresh();
        ScoreCounter.OnCashOrScoreChanged -= Refresh;
    }

    public void Refresh()
    {
        scoreText.text = ScoreCounter.Instance.CurrentScore.ToString();
        sessionCashText.text = CommonMethods.GetCashStringWithSpacing(ScoreCounter.Instance.SessionCash);
        totalCashText.text = TOTAL_CASH_TEXT + CommonMethods.GetCashStringWithSpacing(PrefsDataFacade.Instance.GetTotalCash());
    }
}
