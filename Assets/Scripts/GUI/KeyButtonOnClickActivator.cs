﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class KeyButtonOnClickActivator : MonoBehaviour
{

    [SerializeField]
    private KeyCode[] keys;
    [SerializeField]
    private PlayerController playerController;

    private Button button;


    void Awake()
    {
        button = GetComponent<Button>();
    }

    bool pressed = false;
    void Update()
    {
        foreach (var key in keys)
        {
            if (Input.GetKeyDown(key))
            {
                playerController.OnTurnButtonClicked();
                break;
            }
        }

        if (Input.touchCount > 0 || PadController.Instance.isDown(ControllerButtons.BUTA))
        {
            if (!pressed)
            {
                playerController.OnTurnButtonClicked();
            }
            pressed = true;
        }
        else
        {
            pressed = false;
        }
    }


}
