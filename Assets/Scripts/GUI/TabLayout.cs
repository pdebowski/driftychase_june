﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TabLayout : MonoBehaviour {

    [SerializeField]
    private Color normalColor;
    [SerializeField]
    private Color highlightedColor;

    public void SelectCard(TabLayoutCard selectedCard)
    {
        foreach (var card in GetComponentsInChildren<TabLayoutCard>())
        {
            if(card.transform.parent == transform)
            {
                if(card == selectedCard)
                {
                    card.transform.SetAsLastSibling();
                    card.SetTabColor(highlightedColor);
                }
                else
                {
                    card.SetTabColor(normalColor);
                }
            }
        }
    }

}
