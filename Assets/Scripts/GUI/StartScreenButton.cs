using UnityEngine;
using System.Collections;

public class StartScreenButton : MenuNavigation.ClickableElement
{
    public StartScreenButtonType buttonType;

    [SerializeField]
    private StartScreenKeysManager keysManager;

    private bool selected = false;

    public override void Activate()
    {
        base.Activate();
        selected = true;
    }

    public override void Deactivate()
    {
        base.Deactivate();
        selected = false;
    }

    public override bool IsElementAvailable()
    {
        return true;
    }

    public override void PerformActionClick()
    {

        if (this.gameObject.activeInHierarchy == true)
        {
            base.PerformActionClick();
            keysManager.InvokeButton(buttonType);

            StartCoroutine(TurnOnCor());
        }
    }

    private IEnumerator TurnOnCor()
    {
        while (selected)
        {
            keysManager.LastChoosedButton = buttonType;
            keysManager.TurnOnAnimation(buttonType);
            yield return null;
        }
    }
}

public enum StartScreenButtonType
{
    None,
    Options,
    Leaderboards,
    Achievements,
    Shop,
    StartGame,
    Vip,
    RateMe,
    GameCenter
}
