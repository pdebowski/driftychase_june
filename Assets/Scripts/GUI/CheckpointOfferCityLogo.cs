﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CheckpointOfferCityLogo : MonoBehaviour {

    [SerializeField]
    private Image cityImage;

    void OnEnable()
    {
        int currentCarStartWorldID = PrefabReferences.Instance.CarStatsDB.GetCarStats(PlayerPrefsAdapter.SelectedCar).startWorld;
        WorldType currentCarStartWorld = PrefabReferences.Instance.WorldOrder.GetWorldType(currentCarStartWorldID);
        string resourcePath = PrefabReferences.Instance.WorldPopups.GetWorldPopupResourcePath(currentCarStartWorld);
        cityImage.sprite = Resources.Load<Sprite>(resourcePath);
    }

    void Disable()
    {
        Resources.UnloadAsset(cityImage.sprite);
        cityImage.sprite = null;
    }
}
