﻿using UnityEngine;
using System.Collections;

public class RankingCardManager : MonoBehaviour {

    [SerializeField]
    private GameObject changeNamePopup;

    void Awake()
    {
        if(!PlayerPrefsAdapter.WasRankingCardDisplayed)
        {
            changeNamePopup.SetActive(true);
            PlayerPrefsAdapter.WasRankingCardDisplayed = true;
        }
        else
        {
            changeNamePopup.SetActive(false);
        }
    }

    public void OnChangeNameButtonClicked()
    {
        changeNamePopup.SetActive(true);
    }
}
