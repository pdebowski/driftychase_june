﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScreenshotShare : MonoBehaviour
{
    private const string DRIFTY_CHASE_TITLE = "DRIFTY CHASE";

    [SerializeField]
    private RawImage rawImage;
    [SerializeField]
    private Screenshot screenShotAnimation;
    [SerializeField]
    private GameObject fullScreenButton;

    private Texture2D screenShot;
    private bool isMaximalized = false;
    private RenderTexture renderTexture;

    public void Init()
    {
        if (renderTexture == null)
        {
            renderTexture = RenderTexture.GetTemporary(1024, 1024, 24);
            PrefabReferences.Instance.RenderTextureCamera.targetTexture = renderTexture;

            if (rawImage.texture != null)
            {
                DestroyImmediate(rawImage.texture, true);
            }

            rawImage.texture = renderTexture;
        }
    }

    void OnDisable()
    {
        PrefabReferences.Instance.RenderTextureCamera.targetTexture = null;
        RenderTexture.ReleaseTemporary(renderTexture);
        if (screenShot != null)
        {
            DestroyImmediate(screenShot, true);
        }
    }

    private void Zoom()
    {
        if (!isMaximalized)
        {
            screenShotAnimation.StartAnimation();
            screenShotAnimation.OnAnimationEnd += OnMaximalided;
        }

        fullScreenButton.SetActive(true);
        isMaximalized = true;
    }

    private void OnMaximalided()
    {
        if (screenShot != null)
        {
            DestroyImmediate(screenShot, true);
        }

        screenShot = CommonMethodsVisual.GetTexture2D(renderTexture);
        rawImage.texture = screenShot;

        CommonMethods.SetTimeScaleSmoothed(0);
        screenShotAnimation.OnAnimationEnd -= OnMaximalided;
    }

    private void Unzoom()
    {
        screenShotAnimation.OnAnimationEnd -= OnMaximalided;
        CommonMethods.SetTimeScaleSmoothed(1);
        isMaximalized = false;
        screenShotAnimation.StartReverseAnimation();
        rawImage.texture = renderTexture;
        fullScreenButton.SetActive(false);
    }

    public void OnToggleZoomClick()
    {
        if (isMaximalized)
        {
            Unzoom();
        }
        else
        {
            Zoom();
        }
    }

    public void OnShareClick()
    {
        if (!isMaximalized)
        {
            Zoom();
        }
        else
        {
            string messageText = "...";
            Texture2D texture = screenShot;
#if UNITY_IOS || UNITY_TVOS
		IOSSocialManager.Instance.ShareMedia(messageText,texture);
#else
            AndroidSocialGate.StartShareIntent(DRIFTY_CHASE_TITLE, messageText, texture, "apk");
#endif
        }
    }
}
