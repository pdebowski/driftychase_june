﻿using UnityEngine;
using System.Collections;

public class AndroidTVSwaper : MonoBehaviour
{

    [SerializeField]
    private GameObject defaultObject;
    [SerializeField]
    private GameObject androidTVObject;

    void Awake()
    {
        defaultObject.SetActive(!PlatformRecognition.IsAndroidTV);
        androidTVObject.SetActive(PlatformRecognition.IsAndroidTV);
    }
}
