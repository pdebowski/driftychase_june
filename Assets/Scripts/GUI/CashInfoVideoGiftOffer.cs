﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CashInfoVideoGiftOffer : MonoBehaviour {

    private const string NEXT_CAR_TEXT = "NEXT CAR: ";

    [SerializeField]
    private Text totalCashText;
    [SerializeField]
    private Text nextCarCashText;
    [SerializeField]
    private GameObject holder;

    void OnEnable()
    {
        Refresh();
    }

    private void Refresh()
    {

        int nextCarCost = PrefabReferences.Instance.CarStatsDB.GetNextCarToBuyCost();
        bool isNextCarAvailableToBuy = PrefabReferences.Instance.CarStatsDB.IsNextCarToBuyAvailable();
        int availableCash = PrefsDataFacade.Instance.GetTotalCash() + ScoreCounter.Instance.SessionCash;

        if (nextCarCost > 0 && isNextCarAvailableToBuy)
        {
            holder.SetActive(true);
            nextCarCashText.text = NEXT_CAR_TEXT + CommonMethods.GetCashStringWithSpacing(nextCarCost);
            totalCashText.text = CommonMethods.GetCashStringWithSpacing(availableCash);
        }
        else
        {
            holder.SetActive(false);
        }

    }
}
