using UnityEngine;
using System.Collections;

public class GameModeSelectionManager : MonoBehaviour
{

    [SerializeField]
    private LevelSelectionManager levelSelectionManager;

    public void SelectGameMode(GameplayMode gameplayMode)
    {
        levelSelectionManager.SelectGameplayMode(gameplayMode);
    }
}
 