using UnityEngine;
using System.Collections;

public class KeyChooser : MonoBehaviour
{

    public GameObject leaderboard, like, rateMe, vip, achievements, gameCenter;
    public StartScreenButton buttonLeft, buttonRight;

    void OnEnable()
    {
        Refresh();
    }

    public void Refresh()
    {
        leaderboard.SetActive(false);
        like.SetActive(false);
        rateMe.SetActive(false);
        vip.SetActive(false);
        achievements.SetActive(false);
        gameCenter.SetActive(false);

#if UNITY_ANDROID
        leaderboard.SetActive(true);
        achievements.SetActive(true);
#else
        
        gameCenter.SetActive(true);
        buttonLeft.buttonType = StartScreenButtonType.GameCenter;

        if ((Random.value < GTMParameters.VipRateOnKeyProp && !VIP.IsVIPAccount)
            || (PlayerPrefsAdapter.WasRateMeClicked && !VIP.IsVIPAccount))
        {
            vip.SetActive(true);
            buttonRight.buttonType = StartScreenButtonType.Vip;
        }
        else
        {
            rateMe.SetActive(true);
            buttonRight.buttonType = StartScreenButtonType.RateMe;
        }
#endif
    }

}
