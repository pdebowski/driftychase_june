﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerInput : MonoBehaviour
{
    public static bool isEnabled { get; set; }
    public static System.Action OnTurnButtonClickedEnabled = delegate { };
    public static System.Action OnTurnButtonClickedDisabled = delegate { };

    [SerializeField]
    private KeyCode[] keys;

    private bool pressed = false;

    void Awake()
    {
        isEnabled = false;
    }

    void Update()
    {
        foreach (var key in keys)
        {
            if (Input.GetKeyDown(key))
            {
                OnTurnButtonClicked();
                break;
            }
        }
        bool appleRemoteTouch = AppleRemoteController.Instance.GetButton(AppleRemoteController.Buttons.TouchButton);
        bool padAnalogInput = IsAnalogAxisInput(ControllerAnalogs.LEFTX) || IsAnalogAxisInput(ControllerAnalogs.RIGHTX);
        if (((Input.touchCount > 0 || Input.GetMouseButtonDown(0)) && Input.mousePosition.y < Screen.height * 0.85f)
            || PadController.Instance.isDown(ControllerButtons.BUTA)
            || appleRemoteTouch
            || padAnalogInput)
        {
            if (!pressed || appleRemoteTouch)
            {
                OnTurnButtonClicked();
            }
            pressed = true;
        }
        else
        {
            pressed = false;
        }
    }

    private bool IsAnalogAxisInput(ControllerAnalogs analog)
    {
        return Mathf.Abs(PadController.Instance.getAnalog(analog)) > 0.5f;
    }

    private void OnTurnButtonClicked()
    {
        if (isEnabled)
        {
            OnTurnButtonClickedEnabled();
        }
        else
        {
            OnTurnButtonClickedDisabled();
        }
    }


}
