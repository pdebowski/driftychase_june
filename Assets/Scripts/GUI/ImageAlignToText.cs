using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ImageAlignToText : MonoBehaviour
{

    [SerializeField]
    private Text text;
    [SerializeField]
    private bool alignToLeft = false;
    [Header("Custom Options")]
    [SerializeField]
    private float additionalOffsetWhenEmptyText = 0;
    [SerializeField]
    private bool forceAlignToTextValue = false;
    [SerializeField]
    private string forcedTextValue;

    private float moveFactor;
    private Vector3 startPosition;
    private float startOffset;
    private Canvas canvas;

    void Start()
    {
        Initialize();
    }

    public void Update()
    {
        Align();
    }

    private void Initialize()
    {
        canvas = GetComponentInParent<Canvas>();

        if (text.alignment == TextAnchor.LowerCenter || text.alignment == TextAnchor.UpperCenter || text.alignment == TextAnchor.MiddleCenter)
        {
            moveFactor = 0.5f;
        }
        else
        {
            moveFactor = 1f;
        }

        string oldText = "";

        if (forceAlignToTextValue)
        {
            oldText = text.text;
            text.text = forcedTextValue;
        }

        startPosition = transform.position - transform.right * GetOffset();

        if (forceAlignToTextValue)
        {
            text.text = oldText;
        }
    }

    private void Align()
    {
        float emptyTextOffset = 0;

        if (text.text.Length == 0)
        {
            emptyTextOffset = canvas.transform.lossyScale.x * additionalOffsetWhenEmptyText;
        }

        transform.position = startPosition + transform.right * (GetOffset() + emptyTextOffset);
    }

    private float GetOffset()
    {
        float scaleFactor = canvas.transform.lossyScale.x * moveFactor * text.transform.localScale.x;
        float offset = text.preferredWidth;
        float result = offset * scaleFactor;

        return alignToLeft ? result : -result;
    }


}
