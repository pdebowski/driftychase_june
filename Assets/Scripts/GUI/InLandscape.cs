﻿using UnityEngine;
using System.Collections;

public class InLandscape : MonoBehaviour {


	[SerializeField]
	private Vector2 scale;



	void Awake()
	{
		if(PlatformRecognition.IsTV)
		{
			transform.localScale = scale;
		}
	}
}
