﻿using UnityEngine;
using System.Collections;

public class InLandscapePosition : MonoBehaviour {

	public float y;

	void Awake()
	{
		if(PlatformRecognition.IsTV)
		{
			transform.localPosition = new Vector3(transform.localPosition.x, y, 0);
		}
		
	}
}
