using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CheckpointPanelAdjuster : MonoBehaviour
{

    [SerializeField]
    private GameObject panelLocked;
    [SerializeField]
    private GameObject panelUnlocked;
    [SerializeField]
    private GameObject panelActivateWithAd;
    [SerializeField]
    private GameObject panelActivateWithAdDisabled;
    [SerializeField]
    private GameObject panelActivateForFree;
    [SerializeField]
    private GameObject panelDeactivate;
    [SerializeField]
    private Text timeLeftText;

    private bool isUnlocked;
    private bool isBought;
    private bool isCheckpointAvailable;
    private bool isVideoAdReady;
    private CarDefinitions.CarEnum carEnum;

    void OnEnable()
    {
        StartCoroutine(RefreshIsVideoAdReadyCor());
    }

    void Update()
    {

        if (panelDeactivate.activeInHierarchy)
        {
            RefreshTime();
        }
    }

    public void Set(bool isUnlocked, bool isBought, CarDefinitions.CarEnum carEnum, bool isCheckpointAvailable)
    {
        this.isUnlocked = isUnlocked;
        this.isBought = isBought;
        this.carEnum = carEnum;
        this.isCheckpointAvailable = isCheckpointAvailable;
        Refresh();
    }

    public void Set(bool isUnlocked, bool isBought, CarDefinitions.CarEnum carEnum)
    {
        this.isUnlocked = isUnlocked;
        this.isBought = isBought;
        this.carEnum = carEnum;
        Refresh();
    }

    public void DeactivateCheckpoint()
    {
        PrefabReferences.Instance.CheckpointsManager.SetIsCheckpointActivated(carEnum, false);
        Refresh();
    }

    public void ActivateCheckpointForFree()
    {
        PrefabReferences.Instance.CheckpointsManager.SetIsCheckpointActivated(carEnum, true);
        Refresh();
    }

    public void ActivateCheckpointWithAd()
    {
        PrefabReferences.Instance.GameSceneManager.RewardedAdShow();
        RewardAd.Show(null, ActivateCheckpointWithAdReward);
    }

    public void ActivateCheckpointWithAdReward(bool wasFullyWatched)
    {
        if (wasFullyWatched)
        {
            CrimsonAnalytics.Transaction.RewardedVideo.Displayed.LogEvent();
            CrimsonAnalytics.Offers.ActivateCheckpointInStore.Collected.LogEvent();
            PrefabReferences.Instance.CheckpointsManager.SetIsCheckpointActivated(carEnum, true);
            PrefabReferences.Instance.CheckpointsManager.SetCheckpointActivatedTimeNow();
            Refresh();
        }
        PrefabReferences.Instance.GameSceneManager.ClosedRewardedAd(wasFullyWatched);
    }

    private void Refresh()
    {
        GameObject selected = null;
        CheckpointsManager checkpointsManager = PrefabReferences.Instance.CheckpointsManager;

        bool isCheckpointTimeLeft = checkpointsManager.IsCheckpointTimeLeft();
        bool isCheckpointEnabledForCar = checkpointsManager.GetIsCheckpointActivated(carEnum);

        if (!isUnlocked)
        {
            panelLocked.SetActive(true);
            selected = panelLocked;
        }
        else if (!isBought)
        {
            panelUnlocked.SetActive(true);
            selected = panelUnlocked;
        }
        else
        {
            if (!isCheckpointAvailable)
            {
                DisableAll();
                return;
            }

            if (isCheckpointTimeLeft || VIP.IsVIPAccount)
            {
                if (isCheckpointEnabledForCar)
                {
                    panelDeactivate.SetActive(true);
                    selected = panelDeactivate;
                }
                else
                {
                    panelActivateForFree.SetActive(true);
                    selected = panelActivateForFree;
                }
            }
            else
            {
                if (isVideoAdReady)
                {
                    panelActivateWithAd.SetActive(true);
                    selected = panelActivateWithAd;
                }
                else
                {
                    panelActivateWithAdDisabled.SetActive(true);
                    selected = panelActivateWithAdDisabled;
                }
            }
        }

        DisableAllExcluding(selected);
    }

    private void RefreshTime()
    {
        if (!VIP.IsVIPAccount)
        {
            System.TimeSpan timeLeftSpan = PrefabReferences.Instance.CheckpointsManager.GetCheckpointEnabledTimeSpan();
            timeLeftText.text = string.Format("{0:00}", timeLeftSpan.Minutes) + ":" + string.Format("{0:00}", timeLeftSpan.Seconds);
        }
        else
        {
            timeLeftText.text = "";
        }
    }


    private IEnumerator RefreshIsVideoAdReadyCor()
    {
        while (true)
        {
            isVideoAdReady = RewardAd.IsReady();
            yield return new WaitForSeconds(1);
        }
    }

    private void DisableAll()
    {
        panelLocked.SetActive(false);
        panelUnlocked.SetActive(false);
        panelActivateWithAd.SetActive(false);
        panelActivateWithAdDisabled.SetActive(false);
        panelActivateForFree.SetActive(false);
        panelDeactivate.SetActive(false);
    }

    private void DisableAllExcluding(GameObject toExclude)
    {
        DeactivateIfDifferent(panelLocked, toExclude);
        DeactivateIfDifferent(panelUnlocked, toExclude);
        DeactivateIfDifferent(panelActivateWithAd, toExclude);
        DeactivateIfDifferent(panelActivateWithAdDisabled, toExclude);
        DeactivateIfDifferent(panelActivateForFree, toExclude);
        DeactivateIfDifferent(panelDeactivate, toExclude);
    }

    private void DeactivateIfDifferent(GameObject toDeactivate, GameObject toCompare)
    {
        if (toDeactivate != toCompare)
        {
            toDeactivate.SetActive(false);
        }
    }
}
