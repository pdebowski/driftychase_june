﻿using UnityEngine;
using System.Collections;

public class LogoPosition : MonoBehaviour {

	void Start () {

		this.gameObject.transform.localScale = new Vector3(
															this.gameObject.transform.localScale.x - 0.2f * CommonMethods.GetRatioFactor(false),
															this.gameObject.transform.localScale.y - 0.2f * CommonMethods.GetRatioFactor(false),
															this.gameObject.transform.localScale.z - 0.2f * CommonMethods.GetRatioFactor(false));

		gameObject.transform.localPosition= new Vector3(gameObject.transform.localPosition.x,
													gameObject.transform.localPosition.y - 77 * CommonMethods.GetRatioFactor(false),
													gameObject.transform.localPosition.z);
	}
}