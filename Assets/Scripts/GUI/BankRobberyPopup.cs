using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BankRobberyPopup : MonoBehaviour
{

    [SerializeField]
    private GameObject tapToPlayHolder;
    [SerializeField]
    private Button button;
    [SerializeField]
    private BaseAnimation popupAnimation;

    private UnityEngine.Events.UnityAction onButtonClickAction = null;

    void OnEnable()
    {
        PlayerInput.OnTurnButtonClickedDisabled += OnClick;
    }

    void OnDisable()
    {
        PlayerInput.OnTurnButtonClickedDisabled -= OnClick;

    }

    private void OnClick()
    {
        if (tapToPlayHolder.activeInHierarchy && button.gameObject.activeInHierarchy)
        {
            if (onButtonClickAction != null)
            {
                onButtonClickAction();
                onButtonClickAction = null;
            }
        }
    }

    public void SetTutorialMode(UnityEngine.Events.UnityAction onButtonClick)
    {
        tapToPlayHolder.SetActive(true);
        popupAnimation.enabled = false;
        this.onButtonClickAction = onButtonClick;
        this.gameObject.SetActive(true);
        StartCoroutine(ActivateButtonWithDelay(button, 0.5f));
    }



    private IEnumerator ActivateButtonWithDelay(Button button, float delay)
    {
        yield return StartCoroutine(CommonMethods.WaitForUnscaledSeconds(delay));
        button.gameObject.SetActive(true);
    }

    public void SetNormalMode()
    {
        tapToPlayHolder.SetActive(false);
        transform.position -= Vector3.left * 1000;
        popupAnimation.enabled = true;
        button.gameObject.SetActive(false);

        this.gameObject.SetActive(true);
    }


}
