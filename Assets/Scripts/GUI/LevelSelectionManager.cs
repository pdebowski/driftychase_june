﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LevelSelectionManager : MonoBehaviour
{

    [SerializeField]
    private StartGameSceneManager startGameSceneManager;
    [SerializeField]
    private GameObject gameModeSelectionHolder;
    [SerializeField]
    private GameObject levelSelectionHolder;
    [SerializeField]
    private GameObject levelItemPrefab;
    [SerializeField]
    private Transform layout;
    [SerializeField]
    private GameObject keys;
    [SerializeField]
    private GameObject mainCanvas;

    private List<LevelItem> levelItems = new List<LevelItem>();


    void Awake()
    {
        gameObject.SetActive(false);
    }

    public void SelectGameplayMode(GameplayMode gameplayMode)
    {
        GameplayManager.CurrentGameMode = PrefabReferences.Instance.OtherGames.GetGameController(gameplayMode);
        GenerateLevels();
        ShowScreen(true);
    }

    public void BackButtonClicked()
    {
        GameMode currentGame = GameplayManager.CurrentGameMode;
        GameplayManager.CurrentGameMode = null;

        currentGame.BackToNormalMode();

        ShowScreen(false);

        BackgroundBlur.OnBlurNeedRefresh();
    }

    private void GenerateLevels()
    {
        int ammount = GameplayManager.CurrentGameMode.GetLevelCount();

        levelItems.ForEach(x => Destroy(x.gameObject));
        levelItems.Clear();
        int highestAvailableLevel = GameplayManager.CurrentGameMode.GetHighestAvailableLevel();

        for (int i = 0; i < ammount; i++)
        {
            LevelItem item = (Instantiate(levelItemPrefab) as GameObject).GetComponent<LevelItem>();
            levelItems.Add(item);
            item.transform.SetParent(layout);

            bool isUnlocked = GameplayManager.CurrentGameMode.IsAvailable(i);
            bool isCompleted = GameplayManager.CurrentGameMode.IsCompleted(i);
            bool isAvailableForVideoAd = (highestAvailableLevel + 1) == i && RewardAd.IsReady();

            item.SetItem(isUnlocked, isCompleted, isAvailableForVideoAd, i);
            item.transform.localScale = Vector3.one;
            item.transform.localPosition = item.transform.localPosition.ChangeZ(0);
        }
    }

    private void ShowScreen(bool show)
    {
        gameModeSelectionHolder.SetActive(!show);
        levelSelectionHolder.SetActive(show);
    }

    public void SetLevel(int levelNumber)
    {
        GameplayManager.CurrentLevel = levelNumber;
        GameplayManager.CurrentGameMode.InitLevel(levelNumber);

        keys.SetActive(false);
        mainCanvas.SetActive(false);

        startGameSceneManager.StartButtonClick();

        DL.Log("SetGameMode and Level: " + GameplayManager.CurrentGameMode.GetType() + "   " + levelNumber);
    }

}
