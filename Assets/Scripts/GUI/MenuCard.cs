﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;
public class MenuCard : MonoBehaviour
{
    [SerializeField]
    private UnityEvent OnButtonDown;
    [SerializeField]
    private StartScreenKeysManager startScreenKeysManager;
    [SerializeField]
    private GameObject holder;
    [SerializeField]
    private int direction = 1;
    [SerializeField]
    private Canvas canvas;
    [SerializeField]
    private AnimationCurve animationCurve;
    [SerializeField]
    private float scrollDuration = 1;

    private bool animating;
    private bool dragging = false;
    private float phase;
    private Vector3 startMousePosition;
    private ScrollDirection scrollDirection;

    void Awake()
    {
        scrollDirection = ScrollDirection.Forward;
    }

    public void ShowInstantCard()
    {
        ShowCard();
        phase = 1;
        animating = true;
        AnimationStep();
        holder.SetActive(true);
    }

    void FixedUpdate()
    {
        if (animating)
        {
            AnimationStep();
        }
    }

    private void AnimationStep()
    {
        phase = Mathf.Clamp01(phase);

        MoveToCurrentPhase(true);

        if ((phase >= 1 && scrollDirection == ScrollDirection.Forward) || (phase <= 0 && scrollDirection == ScrollDirection.Backward))
        {
            animating = false;
            if(scrollDirection == ScrollDirection.Backward)
            {
                holder.SetActive(false);
            }
        }

        float phaseDelta = (Time.deltaTime / scrollDuration) * (scrollDirection == ScrollDirection.Forward ? 1 : -1);
        phase += phaseDelta;
    }

    private void MoveToCurrentPhase(bool useAnimationCurve)
    {
        float lerpFactor = useAnimationCurve ? animationCurve.Evaluate(phase) : phase;
        Vector3 newPosition = Vector3.Lerp(StartPosition, EndPosition, lerpFactor);
        gameObject.transform.localPosition = newPosition;
    }

    private void ShowCard()
    {
        transform.SetAsLastSibling();
    }

    public void OnPointerDown()
    {
		OnButtonDown.Invoke();

        startScreenKeysManager.Block();
        ShowCard();

        startMousePosition = Input.mousePosition;
    }

    public void OnDrag()
    {
        dragging = true;
        Vector3 mousePositionDelta = Input.mousePosition - startMousePosition;

        if (IsMoveInRightDirection(mousePositionDelta))
        {
            phase = Mathf.InverseLerp(0, Screen.width, Mathf.Abs(mousePositionDelta.x));

            MoveToCurrentPhase(false);
        }
    }

    private bool IsMoveInRightDirection(Vector3 mouseOffset)
    {
        return Mathf.Sign(mouseOffset.x) == direction ? false : true;
    }

    public void OnPointerUp()
    {
        if (dragging)
        {
            SetBackwardDirectionIfLower(0.15f);
            phase = CommonMethods.GetXFromAnimationCurve(0, 1, 0.01f, phase, animationCurve);

            if (scrollDirection == ScrollDirection.Backward)
            {
                startScreenKeysManager.Unblock();
            }
        }
        else
        {
            phase = 0;
            SetBackwardDirectionIfLower(-1);
            ShowCard();
        }

        dragging = false;
        animating = true;
    }

    private void SetBackwardDirectionIfLower(float value)
    {
        if (phase < value)
        {
            scrollDirection = ScrollDirection.Backward;
        }
        else
        {
            scrollDirection = ScrollDirection.Forward;
        }
    }

    public void OnBackButtonClick()
    {
        phase = 1;
        animating = true;
        SetBackwardDirectionIfLower(2);
        startScreenKeysManager.Unblock();
    }

    private Vector3 StartPosition
    {
        get { return new Vector3(direction * canvas.GetComponent<RectTransform>().rect.width, 0, 0); }
    }

    private Vector3 EndPosition
    {
        get { return Vector3.zero; }
    }


    private enum ScrollDirection
    {
        Forward,
        Backward
    }

}
