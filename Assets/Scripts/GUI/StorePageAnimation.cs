using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class StorePageAnimation : MonoBehaviour
{
    private const float REFERENCED_DPI = 90;

    public float Duration { get { return duration; } }
    public bool IsDuringAnimation { get; set; }

    [SerializeField]
    private Camera pageCamera;
    [SerializeField]
    private Camera canvasCamera;
    [SerializeField]
    private StoreBuilder storeBuilder;
    [SerializeField]
    private Canvas canvas;
    [SerializeField]
    private StoreManager storeManager;
    [SerializeField]
    private GameObject storeItem;
    [SerializeField]
    private GameObject inputBlockImage;
    [SerializeField]
    private GameObject[] objectsToHideWhenScreenshoting;

    [Header("Fixed Animation")]
    [SerializeField]
    private AnimationCurve scrolling;
    [SerializeField]
    private float duration;
    [SerializeField]
    private float startZeroPoint;
    [SerializeField]
    private float endZeroPoint;

    [Header("Analog Animation")]
    [SerializeField]
    private float maxTiltOffset;
    [SerializeField]
    private float minZeroPoint;
    [SerializeField]
    [Range(0.0f, 1.0f)]
    private float offsetNeededToNextPage = 0.5f;
    [SerializeField]
    private float sensitivity = 1;
    [SerializeField]
    private float inchesToScrollFullPage = 1f;

    private RawImage curtain;
    private Texture2D texture;
    private RenderTexture pageRenderTexture;
    private bool cornerHolding = false;
    private bool isTouch = false;
    private bool swiping = false;
    private Vector3 startSwipingPosition;
    private Vector2 inches;
    private float swipingDirection;
    private float pxPerInchHorizontal;
    private Texture2D pageAsImage;

    public void Init()
    {
        storeItem.GetComponent<PageAnimationParts>().CornerButton.Init(this);
        float textureSizeScaler = GetScaler();
        float screenSize = Screen.width;
#if UNITY_TVOS
        screenSize = Screen.height;
#endif
        pageRenderTexture = RenderTexture.GetTemporary((int)(screenSize / textureSizeScaler), (int)(screenSize / textureSizeScaler), 24);
        pageCamera.targetTexture = pageRenderTexture;
        pageCamera.fieldOfView = PlatformRecognition.IsTV ? 46.5f : 90;

        CalculateSensitivity();
    }

    private void CalculateSensitivity()
    {
        inches.x = 1 / (Mathf.Max(Screen.dpi, 150) / Screen.width);
        inches.y = 1 / (Mathf.Max(Screen.dpi, 150) / Screen.height);

        pxPerInchHorizontal = Screen.width / inches.x;
    }

    private float GetScaler()
    {
        float additionalDPI = Mathf.Max(Screen.dpi, 150) - REFERENCED_DPI;
        return 1 + additionalDPI / REFERENCED_DPI;
    }

    public void PrepareEffect()
    {
        PageAnimationParts storeItemAnimationParts = storeItem.GetComponentInChildren<PageAnimationParts>(true);

        Rect cameraRect = new Rect();

        cameraRect = canvas.worldCamera.rect;



        objectsToHideWhenScreenshoting.SetActive(false);

        pageCamera.transform.SetParent(storeItemAnimationParts.Page);
        pageCamera.transform.localPosition = Vector3.zero;
        pageCamera.transform.localPosition = pageCamera.transform.localPosition.ChangeZ(-530);
        pageCamera.transform.localRotation = Quaternion.identity;

        canvas.renderMode = RenderMode.ScreenSpaceCamera;
        canvas.worldCamera = pageCamera;

        pageCamera.gameObject.SetActive(true);
        pageCamera.Render();

        if (pageAsImage != null)
        {
            DestroyImmediate(pageAsImage, true);
        }
        pageAsImage = CommonMethodsVisual.GetTexture2D(pageRenderTexture);
        pageCamera.gameObject.SetActive(false);



        canvas.worldCamera = canvasCamera;
        canvas.worldCamera.rect = cameraRect;

        objectsToHideWhenScreenshoting.SetActive(true);

        curtain = storeItemAnimationParts.Curtain;

        texture = pageAsImage;
    }

    void OnDisable()
    {
        texture = null;
        if (pageAsImage != null)
        {
            DestroyImmediate(pageAsImage, true);
        }
        pageCamera.targetTexture = null;
        RenderTexture.ReleaseTemporary(pageRenderTexture);
    }

    public void NextPage()
    {
        StartCoroutine(ScrollPageCor(-1));
    }

    public void PreviousPage()
    {
        StartCoroutine(ScrollPageCor(1));
    }

    private IEnumerator ScrollPageCor(int direction)
    {
        IsDuringAnimation = true;
        bool reverse = false;
        if (direction == 1)
        {
            reverse = true;
        }

        if (!reverse)
        {
            StoreManager.OnNextCarShown();
        }

        curtain.material.SetFloat("ZeroPoint", reverse == false ? startZeroPoint : endZeroPoint);
        curtain.texture = texture;

        float phase = 0;
        while (phase < 1)
        {
            yield return null;
            curtain.gameObject.SetActive(true);

            phase += Time.deltaTime / duration;
            curtain.material.SetFloat("_ZeroPoint", Mathf.Lerp(startZeroPoint, endZeroPoint, scrolling.Evaluate(reverse == false ? phase : 1 - phase)));
        }
        curtain.material.SetFloat("ZeroPoint", 0);
        yield return null;

        curtain.gameObject.SetActive(false);
        curtain.texture = null;
        IsDuringAnimation = false;



    }

    void Update()
    {
        if (Input.touchCount > 0 || Input.GetMouseButton(0))
        {
            if (!isTouch)
            {
                startSwipingPosition = Input.mousePosition;
            }
            isTouch = true;

            if (MousePositionDeltaFrom(startSwipingPosition).magnitude > DistanceNeededToStartScroll() && !swiping)
            {
                swipingDirection = Mathf.Sign(MousePositionDeltaFrom(startSwipingPosition).x);
                OnPointerDown();
            }
        }
        else
        {
            if (swiping)
            {
                OnPointerUp();
            }

            isTouch = false;
        }
    }

    private float DistanceNeededToStartScroll()
    {
        return pxPerInchHorizontal / 20;
    }

    public void OnPointerDown()
    {
        if (swipingDirection == -1)
        {
            if (storeManager.IsNextCarAvailable())
            {
                PrepareEffect();
                StartCoroutine(storeManager.NextPageWithDelay());
                swiping = true;
            }
        }
        else
        {
            if (storeManager.IsPreviousCarAvailable())
            {
                storeManager.ShowPrevious();
                PrepareEffect();
                storeManager.ShowNext();
                swiping = true;
            }
        }

        if (swiping)
        {
            inputBlockImage.SetActive(true);
            cornerHolding = true;
            StartCoroutine(AnimatePageWithMoveCor());
        }

    }

    public void OnPointerUp()
    {
        cornerHolding = false;
    }

    private IEnumerator AnimatePageWithMoveCor()
    {
        IsDuringAnimation = true;
        Vector3 startPosition = Input.mousePosition;
        curtain.material.SetFloat("_ZeroPoint", startZeroPoint);
        curtain.texture = texture;
        curtain.gameObject.SetActive(true);

        bool reverse = false;
        if (swipingDirection == -1)
        {
            reverse = true;
        }

        float phase = 0;

        while (cornerHolding)
        {
            Vector2 delta = MousePositionDeltaFrom(startPosition);
            float normalX = 0;

            if (reverse)
            {
                normalX = 1 - ((sensitivity * -delta.x) / (pxPerInchHorizontal * inchesToScrollFullPage));

            }
            else
            {
                normalX = (sensitivity * delta.x) / (pxPerInchHorizontal * inchesToScrollFullPage);
            }

            phase = Mathf.Lerp(minZeroPoint, startZeroPoint, normalX);
            curtain.material.SetFloat("_ZeroPoint", phase);
            yield return null;
        }

        float currentZeroPoint = phase;
        float tempDuration = duration;
        phase = Mathf.InverseLerp(startZeroPoint, minZeroPoint, currentZeroPoint);
        if (Mathf.InverseLerp(startZeroPoint, minZeroPoint, currentZeroPoint) > offsetNeededToNextPage)
        {
            tempDuration = (1 - phase) * duration;
            if (!Mathf.Approximately(tempDuration, 0.0f))
            {
                yield return ScrollPageFromTo(currentZeroPoint, minZeroPoint, tempDuration);
            }
        }
        else
        {
            tempDuration = phase * duration;
            if (!Mathf.Approximately(tempDuration, 0.0f))
            {
                yield return ScrollPageFromTo(currentZeroPoint, startZeroPoint, tempDuration);
            }

            storeManager.ShowPrevious();
        }

        curtain.gameObject.SetActive(false);
        inputBlockImage.SetActive(false);
        swiping = false;
        IsDuringAnimation = false;

        if (reverse)
        {
            StoreManager.OnNextCarShown();
        }
    }

    private Vector2 MousePositionDeltaFrom(Vector3 otherPosition)
    {
        return Input.mousePosition - otherPosition;
    }

    private IEnumerator ScrollPageFromTo(float from, float to, float duration)
    {
        float phase = 0;
        while (phase < 1)
        {
            phase += Time.deltaTime / duration;
            curtain.material.SetFloat("_ZeroPoint", Mathf.Lerp(from, to, scrolling.Evaluate(phase)));
            yield return null;
        }
    }



}
