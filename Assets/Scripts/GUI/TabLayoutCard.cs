﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TabLayoutCard : MonoBehaviour {

    [SerializeField]
    private Image tabImage;

    public void SetTabColor(Color color)
    {
        tabImage.color = color;
    }

	public void OnTabButtonClick()
	{
        transform.parent.GetComponent<TabLayout>().SelectCard(this);
	}
}
