﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class StoreSaleItem : MonoBehaviour
{
    [SerializeField]
    private Text timerText;
    [SerializeField]
    private Text[] offValueText;
    [SerializeField]
    private GameObject normalPromotionHolder;
    [SerializeField]
    private GameObject christmasPromotionHolder;

    void OnEnable()
    {
        InAppPromoManager.OnRefreshTimer += RefreshTimer;
        InAppPromoManager.OnRefreshProduct += RefreshVisual;
        PrefabReferences.Instance.InAppPromoManager.RefreshDependentProduct(RefreshVisual);
        PrefabReferences.Instance.InAppPromoManager.RefreshDependentTimer(RefreshTimer);
    }

    void OnDisable()
    {
        InAppPromoManager.OnRefreshTimer -= RefreshTimer;
        InAppPromoManager.OnRefreshProduct -= RefreshVisual;
    }


    protected virtual void RefreshVisual(bool isPromo, IAPManager.Products promoProduct, InAppPromoManager.IsProductInPromoDelegate IsProductInPromo)
    {
        bool visible = isPromo && GTMParameters.InAppPromoHalfDiscountInStoreVisible;
        normalPromotionHolder.SetActive(visible && !GTMParameters.IsChristmas);
        christmasPromotionHolder.SetActive(visible && GTMParameters.IsChristmas);

        if (visible)
        {
            InAppInfoItem infoItem = PrefabReferences.Instance.InAppInfoDB.GetInAppInfoItem(promoProduct);
            foreach (var item in offValueText)
            {
                item.text = infoItem.offPercentageValue;
            }
        }
    }

    private void RefreshTimer(System.TimeSpan timeLeft, string timeLeftFormated)
    {
        timerText.text = timeLeftFormated;
    }
}
