﻿using UnityEngine;
using System.Collections;

public class GameModeItem : MonoBehaviour {

    [SerializeField]
    private GameplayMode mode;

    public void SelectThisGameMode()
    {
        GetComponentInParent<GameModeSelectionManager>().SelectGameMode(mode);
    }
}
