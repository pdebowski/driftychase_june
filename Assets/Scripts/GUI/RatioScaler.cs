﻿using UnityEngine;
using System.Collections;

public class RatioScaler : MonoBehaviour {

#if !UNITY_TVOS
    public float scale16_9;
    public float scale4_3;


    void OnEnable()
    {
        Refresh(PrefabReferences.Instance.AdAdapter.BannerActive);
        PrefabReferences.Instance.AdAdapter.OnBannerVisible += Refresh;
    }

    void OnDisable()
    {
        PrefabReferences.Instance.AdAdapter.OnBannerVisible -= Refresh;
    }

    void Refresh(bool visible)
    {
        float newScale = Mathf.Lerp(scale16_9, scale4_3, CommonMethods.IsPortrait() ? CommonMethods.GetRatioFactor(visible) : 0);
        transform.localScale = new Vector3(newScale, newScale, newScale);
    }

#endif

}
