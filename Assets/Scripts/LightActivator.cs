using UnityEngine;
using System.Collections;

public class LightActivator : MonoBehaviour {

    new Light light;
    int triggeredCount = 0;
	
    
    void Awake()
    {
       Destroy(gameObject);
    }
    // Use this for initialization
	void OnEnable () {
        triggeredCount = 0;
        light = GetComponent<Light>();
    //    light.enabled = false;
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player") || other.gameObject.CompareTag("PlayerKiller"))
        {
        //    light.enabled = true;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player") || other.gameObject.CompareTag("PlayerKiller"))
        {
//            light.enabled = false;
        }
    }

}
