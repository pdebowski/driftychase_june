﻿using UnityEngine;
using System.Collections;
using System;
using MenuNavigation;
public class AppleRemoteTouch : MonoBehaviour
{

    public static System.Action<MenuNavigation.NavigationDirectionV2> OnExecuteDirection = delegate { };

    private const float OFFSET_TO_TRIGGER_NAVIGATION = 0.4f; // range is 2
    private const float OFFSET_TO_TRIGGER_NAVIGATION_STORE = 0.55f; // range is 2
    private const float FAST_SPEED = 6f;

    private bool isDuringTouch = false;
    private AppleRemoteController appleRemote;

    private Vector2 lastPositionWhenTriggeredNavigation;

    private float acceleration;
    private Vector2 touchPosition;
    private float lastAngle;




    void Start()
    {
        appleRemote = AppleRemoteController.Instance;
    }

    void Update()
    {

        if (appleRemote.Touch != null)
        {
            touchPosition = appleRemote.Touch.TouchPosition;
            StartTouch();
            AnalyzeTouch(appleRemote.Touch.TouchPositionDelta, true);
        }
        else
        {
            EndTouch();
        }

        if (appleRemote.Touch == null && Time.frameCount % 3 == 0)
        {
            AnalyzeTouch(Vector2.zero, false);
        }
    }

    private void AnalyzeTouch(Vector2 delta, bool touching)
    {
        NavigationDirectionV2 navigationDirection = NavigationDirectionV2.NONE;
        Vector2 lastTriggeredPositionDelta = Vector2.zero;
        lastTriggeredPositionDelta = (Vector2)touchPosition - lastPositionWhenTriggeredNavigation;
        Vector2 lastTriggeredPositionDeltaSigns = new Vector2(Mathf.Sign(lastTriggeredPositionDelta.x), Mathf.Sign(lastTriggeredPositionDelta.y));

        if (CheckIfOffsetIsTraveled(lastTriggeredPositionDelta, delta))
        {
            float swipeAngle;

            if (touching && delta != Vector2.zero)
            {
                swipeAngle = Vector3.Angle(Vector3.right, delta);

                if ((touchPosition.y - lastPositionWhenTriggeredNavigation.y) < 0)
                {
                    swipeAngle = 360 - swipeAngle;
                }

            }
            else
            {
                swipeAngle = lastAngle;
            }

            navigationDirection = ChooseDirection(swipeAngle);

            Vector3 offset = Quaternion.Euler(new Vector3(0, 0, swipeAngle)) * new Vector3(OFFSET_TO_TRIGGER_NAVIGATION, 0, 0);

            lastPositionWhenTriggeredNavigation += (Vector2)offset;
            lastPositionWhenTriggeredNavigation.x = Mathf.Clamp(lastPositionWhenTriggeredNavigation.x, -Mathf.Abs(touchPosition.x), Mathf.Abs(touchPosition.x));
            lastPositionWhenTriggeredNavigation.y = Mathf.Clamp(lastPositionWhenTriggeredNavigation.y, -Mathf.Abs(touchPosition.y), Mathf.Abs(touchPosition.y));
            OnExecuteDirection(navigationDirection);

            lastAngle = swipeAngle;
        }

        acceleration = Mathf.Max(acceleration - Time.deltaTime * 4, 1f);


    }

    private NavigationDirectionV2 ChooseDirection(float angle)
    {
        if (angle < 45)
        {
            return NavigationDirectionV2.ENE;
        }
        else if (angle < 90)
        {
            return NavigationDirectionV2.NEN;
        }
        else if (angle < 135)
        {
            return NavigationDirectionV2.NWN;
        }
        else if (angle < 180)
        {
            return NavigationDirectionV2.WNW;
        }
        else if (angle < 225)
        {
            return NavigationDirectionV2.WSW;
        }
        else if (angle < 270)
        {
            return NavigationDirectionV2.SWS;
        }
        else if (angle < 315)
        {
            return NavigationDirectionV2.SES;
        }
        else
        {
            return NavigationDirectionV2.ESE;
        }
    }

    private NavigationDirectionV2 ConditionTrigger(float value, NavigationDirectionV2 ifPositive, NavigationDirectionV2 ifNegative)
    {
        if (value > 0)
        {
            return ifPositive;
        }
        else
        {
            return ifNegative;
        }
    }

    private bool CheckIfOffsetIsTraveled(Vector2 lastTriggeredPositionDelta, Vector2 lastFramedelta)
    {
        acceleration = Mathf.Min(2.0f, acceleration + Mathf.Abs(lastFramedelta.magnitude) * 20 * Time.deltaTime);
        float speedRatio = 1 / acceleration;

        float neededOffsetToTriggerNavigation = OFFSET_TO_TRIGGER_NAVIGATION * speedRatio;
        return (lastTriggeredPositionDelta.magnitude > neededOffsetToTriggerNavigation);
    }

    private void StartTouch()
    {
        if (!isDuringTouch)
        {
            isDuringTouch = true;
            lastPositionWhenTriggeredNavigation = appleRemote.Touch.TouchStartPosition;
        }
    }

    private void EndTouch()
    {
        isDuringTouch = false;
    }




}
