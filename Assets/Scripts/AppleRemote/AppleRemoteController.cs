using UnityEngine;
using System.Collections;
using System;

public class AppleRemoteController : Singleton<AppleRemoteController>
{

	private bool TURN_ON_SIMUlATOR = false;

	public static System.Action<Buttons> OnButtonClick = delegate { };
	public TouchInfo Touch;

	private ButtonsValue buttonsValue;
	private Vector2 startTouchPosition;
	private Vector2 prevTouchPosition;
	private float startTouchTime;
	private bool startTouch = false;
	private GameObject appleRemoteSimulator;


	bool blockInput = false;


	protected override void Awakened()
	{
		if (TURN_ON_SIMUlATOR)
		{
			appleRemoteSimulator.SetActive(true);
		}
		buttonsValue = new ButtonsValue();

#if !UNITY_EDITOR && UNITY_TVOS
		UnityEngine.Apple.TV.Remote.allowRemoteRotation = false;
		StartCoroutine(RefreshSettings());
		UnityEngine.Apple.TV.Remote.allowExitToHome = true;
#endif
	}

	void OnDestroy()
	{
	}

	private void OnVideoOpen(string id)
	{
		SetBlockInput(true);
	}

	private void OnVideoClose(string id)
	{
		Invoke("BlockInput", 1f);
	}

	void BlockInput()
	{
		SetBlockInput(false);
	}

	public void SetBlockInput(bool blocked)
	{
		blockInput = blocked;
	}


	private void DestroyIfNotTV(bool isTV)
	{
		if (!isTV)
		{
			if (appleRemoteSimulator != null)
			{
				Destroy(appleRemoteSimulator.gameObject);
			}
			Destroy(this.gameObject);
		}
#if !UNITY_EDITOR && !UNITY_TVOS
        Destroy(appleRemoteSimulator.gameObject);
        Destroy(this.gameObject);
#endif
	}

	void Update()
	{
#if UNITY_TVOS
        if (blockInput)
		{
			buttonsValue = new ButtonsValue();
			return;
		}
		ReadButtons();
#endif
    }

	private void ReadButtons()
	{

		if (!TURN_ON_SIMUlATOR)
		{
			buttonsValue = new ButtonsValue();


			buttonsValue.pause_Play = Input.GetButtonDown("Pause");
			buttonsValue.menu = Input.GetButtonDown("Menu");
			buttonsValue.touchButton = Input.GetButtonDown("TouchButton")
#if UNITY_EDITOR
				|| Input.GetMouseButton(0)
#endif
                ;
			InvokeEvents();
		}


#if UNITY_EDITOR && UNITY_TVOS
        if (Input.GetMouseButton(0))
#else
		if(Input.touchCount>0 && Input.GetTouch(0).phase!=TouchPhase.Ended)
#endif
		{

#if UNITY_EDITOR
			float horiz = Mathf.Lerp(-1, 1, Mathf.InverseLerp(0.0f, Screen.width, Input.mousePosition.x));
			float vert = Mathf.Lerp(-1, 1, Mathf.InverseLerp(0.0f, Screen.height, Input.mousePosition.y));
			UnityEngine.Touch currentTouch = new Touch();
#else
            UnityEngine.Touch currentTouch = Input.GetTouch(Input.touchCount -1 );
            float horiz=Input.GetAxis ("Horizontal");
			float vert=Input.GetAxis ("Vertical");
#endif
			if (Mathf.Approximately(horiz, 0) || Mathf.Approximately(vert, 0))
			{
				return;
			}

			if (startTouch)
			{
				Touch = new TouchInfo();
				prevTouchPosition = Touch.TouchStartPosition = startTouchPosition = new Vector2(horiz, vert);
				startTouchTime = Time.time;
				Touch.TouchID = currentTouch.fingerId;
			}

			for (int i = 1; i < Input.touchCount; i++)
			{
				Debug.Log("Touch" + i + " : " + Input.GetTouch(i).position + "  : " + Input.GetTouch(i).phase + " : " + horiz + " : " + vert);
			}

			Touch.TouchPosition = new Vector2(horiz, vert);
			Touch.TouchPositionDelta = Touch.TouchPosition - prevTouchPosition;
			prevTouchPosition = Touch.TouchPosition;
			Touch.TouchTime = Time.time - startTouchTime;
			startTouch = false;
		}
		else
		{
			Touch = null;
			startTouch = true;
		}
	}

	private void InvokeEvents()
	{
		InvokeEvent(Buttons.Menu);
		InvokeEvent(Buttons.Pause_Play);
		InvokeEvent(Buttons.TouchButton);
	}

	private void InvokeEvent(Buttons button)
	{
		if (GetButton(button))
		{
			OnButtonClick(button);
		}
	}

	private IEnumerator RefreshSettings()
	{
		int count = 100;
		while (count-- > 0)
		{
#if !UNITY_EDITOR && UNITY_TVOS
			UnityEngine.Apple.TV.Remote.reportAbsoluteDpadValues = true;
			Input.multiTouchEnabled = true;
#endif
			yield return new WaitForSeconds(1);
		}
	}

	public bool GetButton(Buttons button)
	{
		switch (button)
		{
			case Buttons.Menu:
				return buttonsValue.menu;
			case Buttons.Pause_Play:
				return buttonsValue.pause_Play;
			case Buttons.TouchButton:
				return buttonsValue.touchButton;
			default:
				return false;
		}
	}


	public enum Buttons
	{
		Menu,
		Pause_Play,
		TouchButton
	};

	private struct ButtonsValue
	{
		public bool menu;
		public bool pause_Play;
		public bool touchButton;
	}

	public class TouchInfo
	{
		public Vector2 TouchPosition = Vector2.zero;
		public Vector2 TouchPositionDelta = Vector2.zero;
		public float TouchTime = 0;
		public int TouchID = 0;

		public Vector2 TouchStartPosition
		{
			get;
			set;
		}

		public float TouchSpeed
		{
			get { return TouchPositionDelta.magnitude / TouchTime; }
		}
	}

	//DEbugMethods
	public void SimulateTouchButton()
	{
		OnButtonClick(Buttons.TouchButton);
	}

	public void SimulatePauseButton()
	{
		OnButtonClick(Buttons.Pause_Play);
	}

	public void SimulateMenuButton()
	{
		OnButtonClick(Buttons.Menu);
	}


}


