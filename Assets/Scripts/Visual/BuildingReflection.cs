﻿using UnityEngine;
using System.Collections;

public class BuildingReflection : MonoBehaviour {

    [SerializeField]
    private Transform reflectedObject;

    private const float yHeight = 0.33f;

    private Vector3 startLocalScale;

    void Awake()
    {
        GetComponent<MeshRenderer>().enabled = true;
        startLocalScale = transform.localScale;
    }

	void Update ()
    {

        //position

        transform.position = reflectedObject.position.ChangeY(yHeight);

        Transform camera = PrefabReferences.Instance.Camera.transform;

        float w = Vector3.Distance(transform.position.ChangeY(0), camera.position.ChangeY(0));
        float h = Vector3.Distance(reflectedObject.position, transform.position);
        float hc = Vector3.Distance(camera.position, camera.position.ChangeY(yHeight));

        float resultFactor = ((w * h) / (h + hc)) / w;

        transform.position += (camera.position.ChangeY(yHeight) - transform.position) * resultFactor;

        //scale

        Vector3 projectedCameraLocal = transform.parent.InverseTransformPoint(camera.position);

        if(projectedCameraLocal.z < 0)
        {
            transform.localScale = Vector3.zero;
        }
        else
        {
            projectedCameraLocal = projectedCameraLocal.ChangeZ(0);
            Vector3 projectedCameraGlobal = transform.parent.TransformPoint(projectedCameraLocal);
            projectedCameraGlobal = projectedCameraGlobal.AddY(-(hc + h));

            Vector3 direction = (camera.position - projectedCameraGlobal).normalized;

            float angle = Vector3.Angle(Vector3.up, direction);

            float newScale = Mathf.InverseLerp(0, 90, angle) * 4;
            transform.localScale = startLocalScale.MultiplyY(newScale);
        }


	}
}
