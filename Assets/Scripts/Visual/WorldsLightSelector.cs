using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WorldsLightSelector : MonoBehaviour
{

    [SerializeField]
    private Light light;
    [SerializeField]
    private WorldOrder worldOrder;
    [SerializeField]
    private List<WorldLight> worldsLights;

    [Header("Animation")]
    [SerializeField]
    private float duration;

    private WorldLight previousLight;
    private WorldLight currentLight;

    void Awake()
    {
        previousLight = new WorldLight();
        previousLight.CopyFrom(worldsLights[0]);
    }

    void OnEnable()
    {
        PlayerSpeedManager.OnWorldChange += ChangeLight;
    }

    void OnDisable()
    {
        PlayerSpeedManager.OnWorldChange -= ChangeLight;
    }

    private void ChangeLight()
    {
        WorldType currentWorld = WorldOrder.CurrentWorld;

        previousLight.CopyFrom(currentLight);

        currentLight = worldsLights.Find(x => x.world == currentWorld);
        StartCoroutine(ChangeLightCor());
    }

    private IEnumerator ChangeLightCor()
    {

        float phase = 0;
        while (phase < 1)
        {
            phase += Time.deltaTime / duration;

            light.color = Color.Lerp(previousLight.color, currentLight.color, phase);
            light.intensity = Mathf.Lerp(previousLight.intensity, currentLight.intensity, phase);
            yield return 0;
        }

    }

}

[System.Serializable]
public class WorldLight
{
    public WorldType world;
    public float intensity;
    public Color color;

    public void CopyFrom(WorldLight light)
    {
        if (light == null)
        {
            return;
        }

        intensity = light.intensity;
        color = light.color;
        world = light.world;
    }
}
