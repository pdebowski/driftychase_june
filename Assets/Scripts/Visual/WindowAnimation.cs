﻿using UnityEngine;
using System.Collections;

public class WindowAnimation : MonoBehaviour
{

    private MeshRenderer meshRenderer;

    void Awake()
    {
        meshRenderer = GetComponent<MeshRenderer>();
    }

    void Update()
    {
        if (meshRenderer.isVisible)
        {
            Vector3 cameraPosition = PrefabReferences.Instance.Camera.transform.position;
            float offset = (transform.rotation.eulerAngles.y - PrefabReferences.Instance.Camera.transform.rotation.eulerAngles.y) / 30;
            offset += (cameraPosition.x + cameraPosition.y + cameraPosition.z) / 100;
            meshRenderer.material.SetFloat("_Offset", offset);
        }
    }
}
