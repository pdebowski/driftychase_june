﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class WorldsPopupSelector : MonoBehaviour
{

    [SerializeField]
    private WorldOrder worldOrder;
    [SerializeField]
    private Image image;

    private BaseAnimation animation;

    void Awake()
    {
        if (Tutorial.IsTutorial)
        {
            gameObject.SetActive(false);
        }
        animation = GetComponentInChildren<BaseAnimation>(true);
    }

    void OnEnable()
    {
        if (DailyGiftManager.IsDailyGiftActive || Tutorial.IsTutorial)
        {
            gameObject.SetActive(false);
        }
        else
        {
            Color color = image.color;
            color.a = 0;
            image.color = color;
            StartCoroutine(LoadPopupAndStart());
        }
    }

    private IEnumerator LoadPopupAndStart()
    {
        WorldType currentWorld = WorldOrder.CurrentWorld;
        string resourcePath = PrefabReferences.Instance.WorldPopups.GetWorldPopupResourcePath(currentWorld);
        ResourceRequest request = Resources.LoadAsync<Sprite>(resourcePath);
        while (!request.isDone)
        {
            yield return null;
        }
        image.color = Color.white;
        image.sprite = request.asset as Sprite;
        animation.OnAnimationEnd += ClearAsset;
        animation.deactivateOnAnimationEnd = true;
        animation.StartAnimation();

    }

    private void ClearAsset()
    {
        animation.OnAnimationEnd -= ClearAsset;

        Resources.UnloadAsset(image.sprite);
        image.sprite = null;
    }
}

[System.Serializable]
public class WorldPopup
{
    public WorldType world;
    public string resourceName;
}
