using UnityEngine;
using System.Collections;

public class ContinuePhotoCameraActivator : MonoBehaviour
{

    [SerializeField]
    private Camera photoCamera;

    void OnEnable()
    {
        GameplayManager.OnSceneAfterGameLoaded += ActivateCamera;
        GameplayManager.OnSessionEndedFromPause += ActivateCamera;
        GameplayManager.OnSessionEnded += DeactivateCamera;
        GameplayManager.OnContinueUsed += DeactivateCamera;
    }

    void OnDisable()
    {
        GameplayManager.OnSceneAfterGameLoaded -= ActivateCamera;
        GameplayManager.OnSessionEndedFromPause -= ActivateCamera;
        GameplayManager.OnSessionEnded -= DeactivateCamera;
        GameplayManager.OnContinueUsed -= DeactivateCamera;
    }

    private void ActivateCamera()
    {
        if (!QualityManager.isLowendDevice)
        {
            photoCamera.gameObject.SetActive(true);
        }
    }

    private void DeactivateCamera()
    {
        photoCamera.gameObject.SetActive(false);
    }
}
