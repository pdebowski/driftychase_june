﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BillboardNeonPair : MonoBehaviour {

	void OnEnable()
    {
        List<Transform> childList = new List<Transform>();

        foreach (Transform child in transform)
        {
            childList.Add(child);
            child.gameObject.SetActive(false);
        }


        childList.RandomElement().gameObject.SetActive(true);
    }
}
