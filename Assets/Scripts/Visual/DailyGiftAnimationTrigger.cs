﻿using UnityEngine;
using System.Collections;

public class DailyGiftAnimationTrigger : MonoBehaviour
{


    void OnTriggerEnter(Collider collider)
    {
        if (collider.tag == "Player")
        {
            GetComponentInParent<DailyGiftAnimation>().RunAnimation();
        }
    }


}
