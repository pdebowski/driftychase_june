﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class WorldsPopupSelectorV2 : MonoBehaviour
{
    [SerializeField]
    private WorldOrder worldOrder;
    [SerializeField]
    private Text text;


    void Awake()
    {
        if (Tutorial.IsTutorial)
        {
            TurnOff();
        }
    }

    void OnEnable()
    {
        if (DailyGiftManager.IsDailyGiftActive || Tutorial.IsTutorial)
        {
            TurnOff();
        }
        else
        {
            text.text = WorldOrder.CurrentWorld.ToString();
        }
    }

    void OnDisable()
    {
        TurnOff();
    }

    private void TurnOff()
    {
        gameObject.SetActive(false);
        //Resources.UnloadUnusedAssets();
    }
}