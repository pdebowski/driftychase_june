﻿using UnityEngine;
using System.Collections;

public class Thunder : MonoBehaviour {

    [SerializeField]
    private Vector2 spanBetweenSeries = new Vector3(3, 10);
    [SerializeField]
    private Vector2 spawnBetweenThunders = new Vector3(0.05f, 0.5f);
    [SerializeField]
    private Vector2 seriesCount = new Vector3(1, 4);

    private float seriesTimer = 0;
    private float thunderTimer = 0;
    private float actualSpanSeries = 1;
    private int actualSeriesCount = 1;
    private ThunderAnim thunderAnim;
    private bool isSeries = false;

    void Start()
    {
        thunderAnim = GetComponent<ThunderAnim>();
        Restart();
    }

    void Update()
    {
        if(!isSeries)
        {
            seriesTimer += Time.deltaTime;

            if (seriesTimer > actualSpanSeries)
            {
                StartSeries();
                seriesTimer = 0;
            }
        }


        if(isSeries)
        {
            if (actualSeriesCount > 0)
            {
                thunderTimer -= Time.deltaTime;

                if (thunderTimer <= 0)
                {
                    RunThunder();
                    thunderTimer = Random.Range(spawnBetweenThunders.x, spawnBetweenThunders.y);
                    actualSeriesCount--;
                }
            }


            if (actualSeriesCount <= 0)
            {
                Restart();
            }
        }



    }

    void RunThunder()
    {
        thunderAnim.StartAnimation();
    }

    void StartSeries()
    {
        actualSeriesCount = Random.Range((int)seriesCount.x, (int)seriesCount.y+1);
        isSeries = true;
        thunderTimer = 0;
    }

    void Restart()
    {
        thunderTimer = 0;
        seriesTimer = 0;
        isSeries = false;
        SetNewSeries();
    }

    void SetNewSeries()
    {
        actualSpanSeries = Random.Range(spanBetweenSeries.x, spanBetweenSeries.y);
    }
}
