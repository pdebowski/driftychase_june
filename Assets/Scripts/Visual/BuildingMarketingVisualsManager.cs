using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BuildingMarketingVisualsManager : MonoBehaviour
{

    private StoreWindowsApplier storeWindowsApplier;
    private BillboardApplier[] billboards;
    private NeonApplier[] neons;
    private BillboardSystem billboardSystem;

    private bool initialized = false;

    void Start()
    {
        if (!initialized)
        {
            Initialize();
            Randomize(WorldOrder.CurrentWorld);
        }
    }

    void OnEnable()
    {
        PlayerSpeedManager.OnWorldChange += Refresh;
        if (initialized)
        {
            Randomize(WorldOrder.CurrentWorld);
        }
        else
        {
            Initialize();
        }
    }

    void OnDisable()
    {
        PlayerSpeedManager.OnWorldChange -= Refresh;
    }

    private void Refresh()
    {
        if (GameManager.Instance.CurrentState == GameManager.GameState.InMenus)
        {
            Randomize(WorldOrder.CurrentWorld);
        }
    }

    private void Randomize(WorldType worldType)
    {
        LogoTexturePair logoTexturePair = billboardSystem.GetRandomLogoTexturePair(worldType);
        Color color = billboardSystem.GetRandomColor(worldType);

        if (storeWindowsApplier != null)
        {
            storeWindowsApplier.SetStoreWindows(logoTexturePair.vectorLogo, color);
        }

        foreach (var billboard in billboards)
        {
            billboard.SetBillboard(logoTexturePair.vectorLogo, color);
        }

        foreach (var neon in neons)
        {
            neon.SetNeon(logoTexturePair.GetRandomLogo(), color);
        }
    }

    private void Initialize()
    {
        storeWindowsApplier = GetComponentInChildren<StoreWindowsApplier>(true);
        billboards = GetComponentsInChildren<BillboardApplier>(true);
        neons = GetComponentsInChildren<NeonApplier>(true);
        billboardSystem = PrefabReferences.Instance.BillboardSystem;
        initialized = true;
    }
}
