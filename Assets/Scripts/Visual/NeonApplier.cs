﻿using UnityEngine;
using System.Collections;

public class NeonApplier : MonoBehaviour {

    [SerializeField]
    private MeshRenderer logoRenderer;

    public void SetNeon(Texture logo, Color color)
    {
        if (Random.Range(0f, 1f) < BillboardSystem.chanceToSpawnNeon)
        {
            SetVisible(true);

            logoRenderer.material.SetTexture("_LogoTexture", logo);
            logoRenderer.material.SetColor("_LogoColor", color);
        }
        else
        {
            SetVisible(false);
        }
    }

    private void SetVisible(bool value)
    {
        if(transform.GetChild(0) != null)
        {
            transform.GetChild(0).gameObject.SetActive(value);
        }
        else
        {
            Debug.LogError("Neon without child");
        }        
    }
}
