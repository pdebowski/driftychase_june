﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DailyGiftAnimation : MonoBehaviour
{
    [SerializeField]
    private CoinSpawner coinSpawner;
    [SerializeField]
    private ParticleSystem[] particleSystems;
    [SerializeField]
    private Transform[] explosionPoints;
    [SerializeField]
    private GameObject props;
    [SerializeField]
    private GameObject brick;
    [SerializeField]
    private GameObject notDamagedBank;
    [SerializeField]
    private GameObject damagedBank;
    [SerializeField]
    private GameObject windows;
    [SerializeField]
    private AnimationCurve timeScaleCurve;
    [SerializeField]
    private Transform startAnimationPoint;
    [SerializeField]
    private Transform endAnimationPoint;
    [SerializeField]
    private float boomPhase;
    [SerializeField]
    private float coinsLerpingPhase;
    [SerializeField]
    private AnimationCurve cameraXOffset;
    [SerializeField]
    private AnimationCurve cameraYOffset;
    [SerializeField]
    private AnimationCurve cameraZOffset;
    [SerializeField]
    private Vector3 cameraOffsetRatio;
    [SerializeField]
    private AnimationCurve cameraRotation; // 1 - bankFocus, 0- playerFocus
    [SerializeField]
    private Transform cameraRotationTemplate;


    private List<CoinBag> coinBags;
    private PlayerCamera playerCamera;
    private Transform player;
    private bool giftSpawned = false;
    private bool coinsLerping = false;
    private bool breakAnimation = false;
    private bool duringDailyGift = false;
    private Vector3 startCameraPosition;
    private Quaternion startCameraRotation;
    private List<GameObject> bricks;

    private void Start()
    {
        Restart();
        playerCamera = PrefabReferences.Instance.Camera.GetComponent<PlayerCamera>();
        player = PrefabReferences.Instance.Player;
    }

    private void OnEnable()
    {
        GameplayManager.OnSessionEnded += Restart;
        GameplayManager.OnPlayerCrash += OnPlayerCrash;
        DailyGiftManager.PrepareDailyGiftAnimation += PrepareAnimation;
        DailyGiftManager.HideDailyGiftAnimation += HideAnimation;
    }

    private void OnDisable()
    {
        GameplayManager.OnSessionEnded -= Restart;
        GameplayManager.OnPlayerCrash -= OnPlayerCrash;
        DailyGiftManager.PrepareDailyGiftAnimation -= PrepareAnimation;
        DailyGiftManager.HideDailyGiftAnimation -= HideAnimation;
    }

    private void Restart()
    {
        duringDailyGift = false;
        breakAnimation = false;
        coinsLerping = false;
        giftSpawned = false;
        SetEmitingParticleSystems(false);
        windows.SetActive(true);
        notDamagedBank.SetActive(true);
        damagedBank.SetActive(false);
    }

    private void OnPlayerCrash()
    {
        breakAnimation = true;
        CommonMethods.SetTimeScaleSmoothed(1);
        if (giftSpawned)
        {
            LetCoinsMove();
        }
    }

    public void PrepareAnimation()
    {
		if (PrefabReferences.Instance.PauseButton != null) 
		{
			PrefabReferences.Instance.PauseButton.SetActive(false);
		}
        props.SetActive(true);
        bricks = new List<GameObject>();
        for (int i = 0; i < 50; i++)
        {
            bricks.Add(Instantiate(brick, Vector3.down * 100, Quaternion.identity) as GameObject);
        }
    }

    public void HideAnimation()
    {
		if (PrefabReferences.Instance.PauseButton != null) 
		{
			PrefabReferences.Instance.PauseButton.SetActive(true);
		}
        
        props.SetActive(false);
        if (bricks != null)
        {
            foreach (var item in bricks)
            {
                Destroy(item);
            }
        }
        bricks = null;
    }

    public void RunAnimation()
    {
        if (DailyGiftManager.IsDailyGiftActive)
        {
            StartCoroutine(Animation());
        }
    }

    private IEnumerator Animation()
    {
        duringDailyGift = true;
        player.GetComponent<PlayerController>().BlockInput = true;

        startCameraPosition = playerCamera.transform.position;
        startAnimationPoint.position = player.transform.position;
        startCameraRotation = playerCamera.transform.rotation;
        float phase = 0;

        playerCamera.StopWorking();

        while (endAnimationPoint.position.x > player.position.x && !breakAnimation)
        {
            phase = Mathf.InverseLerp(startAnimationPoint.position.x, endAnimationPoint.position.x, player.transform.position.x);
            RefreshAnimation(phase);
            yield return null;
            
        }
        playerCamera.StartWorking();

        duringDailyGift = false;
		
		if (PrefabReferences.Instance.PauseButton != null) 
		{
			PrefabReferences.Instance.PauseButton.SetActive(true);
		}
        DailyGiftManager.IsDailyGiftActive = false;
        player.GetComponent<PlayerController>().BlockInput = false;

    }

    private void RefreshAnimation(float phase)
    {
        CommonMethods.SetTimeScaleSmoothed(timeScaleCurve.Evaluate(phase));
        Vector3 cameraPositionOffset;
        cameraPositionOffset.x = cameraXOffset.Evaluate(phase) * cameraOffsetRatio.x;
        cameraPositionOffset.y = cameraYOffset.Evaluate(phase) * cameraOffsetRatio.y;
        cameraPositionOffset.z = cameraZOffset.Evaluate(phase) * cameraOffsetRatio.z;
        playerCamera.transform.position = startCameraPosition + cameraPositionOffset + playerCamera.ShakeValue;
        playerCamera.transform.rotation = Quaternion.Lerp(startCameraRotation, Quaternion.LookRotation((cameraRotationTemplate.position - playerCamera.transform.position).normalized), cameraRotation.Evaluate(phase));
        if (phase > boomPhase && !giftSpawned)
        {
            DailyGiftSounds.Instance.Play();
            TurnOnBrics();
            SpawnCoinsAndSetExplosion();
            playerCamera.Shake();
        }
        if (phase > coinsLerpingPhase && !coinsLerping)
        {
            LetCoinsMove();
        }
    }

    public IEnumerator ChangeTimeScaleOverTime(float from, float to, float changeTime)
    {
        float t = 0;
        while (t <= 1f)
        {
            t += Time.unscaledDeltaTime / changeTime;
            float newTimeScale = Mathf.Lerp(from, to, timeScaleCurve.Evaluate(t));
            CommonMethods.SetTimeScaleSmoothed(newTimeScale);
            yield return 0;
        }
    }

    private void SpawnCoinsAndSetExplosion()
    {
        giftSpawned = true;
        coinSpawner.SpawnGift();
        coinBags = coinSpawner.GetCoinBags();
        SetCoinsExplosion();
        SetEmitingParticleSystems(true);
        windows.SetActive(false);
        notDamagedBank.SetActive(false);
        damagedBank.SetActive(true);
    }

    private void SetCoinsExplosion()
    {
        int i = 0;

        foreach (var coinBag in coinBags)
        {
            int index = i / (coinBags.Count / explosionPoints.Length);
            Vector3 newPosition = explosionPoints[index].transform.position;
            Vector3 direction = Quaternion.Euler(0, Random.Range(-60, 60), 0) * explosionPoints[index].forward;

            newPosition += direction;
            coinBag.transform.position = newPosition;
            coinBag.GetComponent<Rigidbody>().AddExplosionForce(40000 * Random.Range(0.5f, 1f), explosionPoints[index].position, 10);
            coinBag.canMove = false;

            i++;
        }
    }

    private void TurnOnBrics()
    {
        for (int i = 0; i < bricks.Count; i++)
        {
            int index = i % 2;
            Vector3 newPosition = explosionPoints[index].transform.position;

            Vector3 direction = Quaternion.Euler(new Vector3(Random.Range(0, 30), Random.Range(-10, 10), 0)) * explosionPoints[index].forward;
            newPosition = explosionPoints[index].transform.position;
            newPosition += direction * 2;
            bricks[i].transform.position = newPosition;
            bricks[i].GetComponent<Rigidbody>().AddExplosionForce(50000 * Random.Range(0.3f, 1f), explosionPoints[index].position, 10);
        }
    }

    private void LetCoinsMove()
    {
        coinsLerping = true;
        foreach (var coinBag in coinBags)
        {
            coinBag.canMove = true;
        }
    }

    private void SetEmitingParticleSystems(bool value)
    {
        foreach (var ps in particleSystems)
        {
            ParticleSystem.EmissionModule emmision = ps.emission;
            emmision.enabled = value;

            if (value == true)
            {
                ps.Play();
            }
            else
            {
                ps.Stop();
                ps.Clear();
            }
        }
    }

    public bool IsDuringAnimation()
    {
        return duringDailyGift;
    }


}
