﻿using UnityEngine;
using System.Collections;

public class GamePopupsSelector : MonoBehaviour
{

    [SerializeField]
    private GameObject worldPopup;
    [SerializeField]
    private GameObject levelGamePopup;
    [SerializeField]
    private MiniLeaderboardsAnimator miniLeaderboardsAnimator;

    private void OnEnable()
    {
        GameplayManager.OnSessionStarted += OnSessionStarted;
        GameplayManager.OnPlayerCrash += OnPlayerCrash;
        PlayerSpeedManager.OnWorldChange += OnWorldChanged;
    }

    private void OnDisable()
    {
        GameplayManager.OnSessionStarted -= OnSessionStarted;
        GameplayManager.OnPlayerCrash -= OnPlayerCrash;
        PlayerSpeedManager.OnWorldChange -= OnWorldChanged;
    }

    private void OnSessionStarted()
    {
        if (GameplayManager.CurrentGameMode == null)
        {
            if(miniLeaderboardsAnimator.CanShowOnBeggining())
            {
                miniLeaderboardsAnimator.ShowOnBeggining();
            }
            else
            {
                worldPopup.gameObject.SetActive(true);
            }           
        }
        else if (GameplayManager.CurrentGameMode != null && !DailyGiftManager.IsDailyGiftActive && !Tutorial.IsTutorial)
        {
            levelGamePopup.gameObject.SetActive(true);
        }
    }

    private void OnPlayerCrash()
    {
        if (miniLeaderboardsAnimator.CanShowOnEnd())
        {
            miniLeaderboardsAnimator.ShowOnEnd();
        }
    }

    private void OnWorldChanged()
    {
        if (GameManager.Instance.CurrentState == GameManager.GameState.Playing)
        {
            worldPopup.gameObject.SetActive(true);
        }
    }
}
