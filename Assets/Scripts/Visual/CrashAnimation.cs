using UnityEngine;
using System.Collections;
using System;

public class CrashAnimation : BaseAnimation
{
    [SerializeField]
    private Transform[] availableTargetPoints;
    [SerializeField]
    private AnimationCurve timeScale;
    [SerializeField]
    private AnimationCurve offsetVector;
    [SerializeField]
    private AnimationCurve cameraLastMoveImpactMoment;
    [SerializeField]
    private AnimationCurve cameraLastMoveImpactTotal;
    [SerializeField]
    private AnimationCurve endRotation;
    [SerializeField]
    private AnimationCurve playerForwardRotation;
    [SerializeField]
    private Transform renderTextureCamera;

    private Vector3 cameraMovingHistoryAvg;
    private Vector3 targetOffset;
    private Vector3 startOffset;
    private Vector3 lastMoveImpactSum;
    private Quaternion startRotation;
    private Quaternion targetRotation;
    private PlayerCamera playerCamera;
    private Transform player;
    private Transform target;
    private float targetYPosition = 0;

    protected override void Init()
    {
        OnAnimationStart += OnStart;
        playerCamera = GetComponent<PlayerCamera>();
        player = PrefabReferences.Instance.Player;
    }

    private void OnStart()
    {
        target = GetTargetPoint();
        targetYPosition = target.position.y;
        renderTextureCamera.position = target.position;
        renderTextureCamera.rotation = target.rotation;

        cameraMovingHistoryAvg = playerCamera.GetHistoryAverage();
        startOffset = transform.position - player.position;
        startRotation = transform.rotation;
        lastMoveImpactSum = Vector3.zero;
        playerCamera.StopWorking();
    }

    private Transform GetTargetPoint()
    {
        Transform result = null;
        RaycastHit hitInfo = new RaycastHit();

        int currentTry = 1;
        int index = UnityEngine.Random.Range(0, availableTargetPoints.Length);

        while (result == null)
        {
            result = availableTargetPoints[index % availableTargetPoints.Length];
            Ray ray = new Ray(result.position.ChangeY(500), Vector3.down);
            if (Physics.Raycast(ray, out hitInfo, 1000, LayerMask.GetMask("Buildings")) && currentTry < availableTargetPoints.Length)
            {
                result = null;
            }
            currentTry++;
            index++;
        }
        return result;
    }

    protected override void Calculate(float phase)
    {
        if (player==null)
        {
            return;
        }
        CommonMethods.SetTimeScaleSmoothed(timeScale.Evaluate(phase));

        float cameraLastMoveImpactFloat = cameraLastMoveImpactMoment.Evaluate(phase);
        Vector3 newPositionBasedOnHistory = transform.position + cameraMovingHistoryAvg;

        lastMoveImpactSum += cameraMovingHistoryAvg * cameraLastMoveImpactFloat;
        Vector3 newPositionBasedOnCurve = player.position + Vector3.Lerp(startOffset, target.position - player.position, offsetVector.Evaluate(phase)) + lastMoveImpactSum * cameraLastMoveImpactTotal.Evaluate(phase);
        newPositionBasedOnCurve.y = Mathf.Clamp(newPositionBasedOnCurve.y, targetYPosition, 100);
        transform.position = Vector3.Lerp(newPositionBasedOnCurve, newPositionBasedOnHistory, cameraLastMoveImpactFloat);

        //Debug.Log("curve position : " + newPositionBasedOnCurve + "    current position :  " + transform.position);
        Quaternion playerRotation = Quaternion.LookRotation(player.position - transform.position);
        Quaternion targetPointRotation = Quaternion.Lerp(transform.rotation, target.rotation, endRotation.Evaluate(phase));

        //Debug.Log("current rotation : " + transform.rotation.eulerAngles + "    ");
        Quaternion destinationRot = Quaternion.Lerp(targetPointRotation, playerRotation, playerForwardRotation.Evaluate(phase));

        transform.rotation = Quaternion.Lerp(transform.rotation, destinationRot, playerForwardRotation.Evaluate(phase));
    }
}
