﻿using UnityEngine;
using System.Collections;

public class BillboardApplier : MonoBehaviour
{

    [SerializeField]
    private BillboardType type;
    [SerializeField]
    private GameObject billboardHolder;
    [SerializeField]
    private MeshRenderer[] textureMeshRenderers;


    public void SetBillboard(Texture logo, Color color)
    {
        if (Random.Range(0f, 1f) < BillboardSystem.chanceToSpawnBillboard)
        {
            SetVisible(true);

            if (type == BillboardType.Horizontal)
            {
                SetTexture(logo, color, true, false);
            }
            if (type == BillboardType.Vertical)
            {
                SetTexture(logo, color, false, true);
            }
            if (type == BillboardType.Square)
            {
                SetTexture(logo, color, false, false);
            }
        }
        else
        {
            SetVisible(false);
        }
    }

    private void SetTexture(Texture logo,Color color, bool xScaling,bool yScaling)
    {
        foreach (var textureMeshRenderer in textureMeshRenderers)
        {
            textureMeshRenderer.material.SetTexture("_LogoTexture", logo);
            textureMeshRenderer.material.SetColor("_BackgroundColor", color);

            if (xScaling)
            {
                float scaleX = textureMeshRenderer.transform.lossyScale.y / textureMeshRenderer.transform.lossyScale.x;
                textureMeshRenderer.material.SetFloat("_ScaleX", scaleX);
            }
            if(yScaling)
            {
                float scaleY = textureMeshRenderer.transform.lossyScale.x / textureMeshRenderer.transform.lossyScale.y;
                textureMeshRenderer.material.SetFloat("_ScaleY", scaleY);
            }
        }
    }

    private void SetVisible(bool value)
    {
        billboardHolder.SetActive(value);
    }
}

public enum BillboardType
{
    Horizontal,
    Vertical,
    Square
}