using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BackgroundBlur : MonoBehaviour
{
    private const float REFERENCED_DPI = 150;

    public static System.Action OnBlurNeedRefresh = delegate { };

    [SerializeField]
    private RawImage rawImage;
    [SerializeField]
    private RawImage multiplyImage;
    [SerializeField]
    private Material multiplyMaterial;
    [SerializeField]
    private GameObject noBackground;
    [SerializeField]
    private RenderTexture renderTexture;
    [SerializeField]
    private Texture2D texture;

    void Awake()
    {
        noBackground.SetActive(false);
    }

    void Start()
    {
        StartCoroutine(MakeBlurWithDelay());
    }

    void OnEnable()
    {
        BackgroundBlur.OnBlurNeedRefresh += MakeBlur;
    }

    void OnDisable()
    {
        BackgroundBlur.OnBlurNeedRefresh -= MakeBlur;

        rawImage.texture = null;
        RenderTexture.ReleaseTemporary(renderTexture);
        DestroyImmediate(texture, true);
        texture = null;
    }

    private IEnumerator MakeBlurWithDelay()
    {
        yield return null;
        MakeBlur();
    }

    public void MakeBlur()
    {
        float textureSizeScaler = GetScaler();
        renderTexture = RenderTexture.GetTemporary((int)(Screen.width / textureSizeScaler), (int)(Screen.height / textureSizeScaler), 24);

        UnityStandardAssets.ImageEffects.BlurOptimized blurScript = PrefabReferences.Instance.Camera.GetComponentInChildren<UnityStandardAssets.ImageEffects.BlurOptimized>(true);
        PrefabReferences.Instance.Camera.targetTexture = renderTexture;
        blurScript.enabled = true;
        PrefabReferences.Instance.Camera.Render();
        texture = CommonMethodsVisual.GetTexture2D(renderTexture);
        blurScript.enabled = false;
        PrefabReferences.Instance.Camera.targetTexture = null;

        SetTexture(texture);
        noBackground.SetActive(true);
    }

    private void SetTexture(Texture2D texture)
    {
        rawImage.texture = texture;
        rawImage.color = new Color(163.0f / 255, 163.0f / 255, 163.0f / 255);
        multiplyImage.gameObject.SetActive(true);
        multiplyImage.texture = texture;
        multiplyMaterial.SetTexture("_MainTex", texture);
    }

    private float GetScaler()
    {
        float additionalDPI = Mathf.Max(Screen.dpi, REFERENCED_DPI) - REFERENCED_DPI;
        return 1 + additionalDPI / REFERENCED_DPI;
    }
}
