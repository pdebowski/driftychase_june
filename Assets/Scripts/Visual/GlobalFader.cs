﻿using UnityEngine;
using System.Collections;

public class GlobalFader : Singleton<GlobalFader>
{
    public bool Fading { get; private set; }
    public bool UseUnscaledDeltaTime { get { return useUnscaledTime; } set { useUnscaledTime = value; } }

    [SerializeField]
    private CanvasGroup fade;
    [SerializeField]
    private AnimationCurve fadeAnimation;
    [SerializeField]
    private float defaultDuration;
    [SerializeField]
    private float defaultStartDelay;
    [SerializeField]
    private float defaultFullFadeDelay;

    private bool useUnscaledTime = false;


    private float GetDeltaTime()
    {
        return (useUnscaledTime == true) ? Time.unscaledDeltaTime : Time.deltaTime;
    }

    public void Fade(CanvasGroup canvasGroup, float from, float to, System.Action onStart, System.Action onEnd)
    {
        Fade(canvasGroup, from, to, onStart, onEnd, defaultDuration, defaultStartDelay);
    }

    public void Fade(CanvasGroup canvasGroup, float from, float to, System.Action onStart, System.Action onEnd, float duration, float startDelay)
    {
        if (!Fading)
        {
            CanvasGroup[] canvasGroups = new CanvasGroup[1];
            canvasGroups[0] = canvasGroup;
            StartCoroutine(FadeAnimationWithCanvasGroup(canvasGroups, onStart, onEnd, from, to, duration, startDelay));
        }
    }

    public void Fade(CanvasGroup[] canvasGroups, float from, float to, System.Action onStart, System.Action onEnd, float duration, float startDelay)
    {
        if (!Fading)
        {
            StartCoroutine(FadeAnimationWithCanvasGroup(canvasGroups, onStart, onEnd, from, to, duration, startDelay));
        }
    }

    private IEnumerator FadeAnimationWithCanvasGroup(CanvasGroup[] canvasGroups, System.Action onStart, System.Action onEnd, float from, float to, float duration, float startDelay)
    {
        Fading = true;

        yield return new WaitForSeconds(startDelay);
        InvokeMethod(onStart);

        float phase = 0;
        while (phase < 1)
        {
            foreach (var item in canvasGroups)
            {
                item.alpha = Mathf.Lerp(from, to, phase);
            }

            phase += GetDeltaTime() / duration;
            phase = Mathf.Clamp01(phase);

            yield return null;
        }

        InvokeMethod(onEnd);
        Fading = false;
    }

    public void Fade(System.Action onStart, System.Action onFullyBlack, System.Action onEnd)
    {
        Fade(onStart, onFullyBlack, onEnd, defaultDuration, defaultStartDelay, defaultFullFadeDelay);
    }

    public void Fade(System.Action onStart, System.Action onFullyBlack, System.Action onEnd, float duration, float startDelay, float fullFadeDelay)
    {
        if (!Fading)
        {
            StartCoroutine(FadeAnimationWithBlackScreen(onStart, onFullyBlack, onEnd, duration, startDelay, fullFadeDelay));
        }
    }



    private IEnumerator FadeAnimationWithBlackScreen(System.Action onStart, System.Action onFullyBlack, System.Action onEnd, float duration, float startDelay, float fullFadeDelay)
    {
        Fading = true;

        yield return new WaitForSeconds(startDelay);
        InvokeMethod(onStart);

        bool wasActionPerformed = false;
        float phase = 0;
        fade.gameObject.SetActive(true);

        while (phase < 1)
        {
            fade.alpha = fadeAnimation.Evaluate(phase);

            if (!wasActionPerformed && phase > 0.5f)
            {
                InvokeMethod(onFullyBlack);
                wasActionPerformed = true;
                yield return new WaitForSeconds(fullFadeDelay);
            }

            phase += GetDeltaTime() / duration;
            yield return null;
        }

        InvokeMethod(onEnd);
        fade.gameObject.SetActive(false);
        Fading = false;

    }

    private void InvokeMethod(System.Action method)
    {
        if (method != null)
        {
            method();
        }
    }


    public void FadeIn(System.Action onStart, System.Action onEnd)
    {
        FadeIn(onStart, onEnd, defaultDuration, defaultStartDelay);
    }

    public void FadeIn(System.Action onStart, System.Action onEnd, float duration, float startDelay)
    {
        if (!Fading)
        {
            StartCoroutine(FadeInAnimationWithBlackScreen(onStart, onEnd, duration, startDelay));
        }
    }

    private IEnumerator FadeInAnimationWithBlackScreen(System.Action onStart, System.Action onEnd, float duration, float startDelay)
    {
        Fading = true;

        yield return new WaitForSeconds(startDelay);
        InvokeMethod(onStart);

        float phase = 0;
        fade.gameObject.SetActive(true);

        while (phase < 0.5f)
        {
            fade.alpha = fadeAnimation.Evaluate(phase);
            phase += GetDeltaTime() / (duration * 2);
            yield return null;
        }

        fade.alpha = 1;
        Fading = false;
        InvokeMethod(onEnd); 
    }

    public void FadeOut(System.Action onStart, System.Action onEnd)
    {
        FadeOut(onStart, onEnd, defaultDuration, defaultStartDelay);
    }

    public void FadeOut(System.Action onStart, System.Action onEnd, float duration, float startDelay)
    {
        if (!Fading)
        {
            StartCoroutine(FadeOutAnimationWithBlackScreen(onStart, onEnd, duration, startDelay));
        }
    }

    private IEnumerator FadeOutAnimationWithBlackScreen(System.Action onStart, System.Action onEnd, float duration, float startDelay)
    {
        Fading = true;

        yield return new WaitForSeconds(startDelay);
        InvokeMethod(onStart);

        float phase = 0.5f;
        fade.gameObject.SetActive(true);

        while (phase < 1)
        {
            fade.alpha = fadeAnimation.Evaluate(phase);
            phase += GetDeltaTime() / (duration * 2);
            yield return null;
        }
        Fading = false;
        InvokeMethod(onEnd);
        fade.gameObject.SetActive(false); 
    }
}
