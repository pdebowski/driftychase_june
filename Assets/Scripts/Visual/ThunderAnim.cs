﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ThunderAnim : BaseAnimation
{
    [SerializeField]
    private float maxAlpha = 1;
    [SerializeField]
    private AnimationCurve alphaCurve;

    private Image image;

    protected override void Init()
    {
        base.Init();

        image = GetComponent<Image>();
    }

    override protected void Calculate(float phase)
    {
        float value = alphaCurve.Evaluate(phase) * maxAlpha;
        Color newColor = new Color();
        newColor.a = 1;
        newColor.r = value;
        newColor.g = value;
        newColor.b = value;
        image.material.SetColor("_TintColor", newColor);
    }
}
