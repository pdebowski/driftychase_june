﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BillboardSystem : MonoBehaviour
{

    public static float chanceToSpawnStoreWindowBillboard = 0.5f;
    public static float chanceToSpawnBillboard = 0.35f;
    public static float chanceToSpawnNeon = 0.5f;
    public static float chanceToEasterEgg = 0.1f;
    public static float chanceToUniversalLogo = 0.1f;

    [SerializeField]
    private List<WorldMarketing> worldMarketings;
    [SerializeField]
    private List<LogoTexturePair> universalLogos;
    [SerializeField]
    private List<LogoTexturePair> eventLogos;
    [SerializeField]
    private bool isEventActive = false;
    [SerializeField]
    private Texture transparentTexture;
    [SerializeField]
    WorldOrder worldOrder;

    private void Awake()
    {
        WorldDependent.CheckForDuplicates(worldMarketings,this.GetType().Name);
    }

    public LogoTexturePair GetRandomLogoTexturePair(WorldType worldType)
    {
        WorldMarketing worldMarketing = GetWorldMarketing(worldType);

        if(isEventActive)
        {
            return eventLogos.RandomElement();
        }

        if (worldMarketing.easterEggs != null && worldMarketing.easterEggs.Count > 0)
        {
            LogoTexturePair easterEgg = worldMarketing.easterEggs.Find(x => x.countryCode == PlayerPrefsAdapter.Country).logoTexturePairs;
            if (easterEgg != null && Random.value < chanceToEasterEgg)
            {
                return easterEgg;
            }
        }

        if (universalLogos.Count > 0 && Random.value < chanceToUniversalLogo)
        {
            return universalLogos.RandomElement();
        }
        else
        {
            return worldMarketing.logoTexturePairs.RandomElement();
        }
    }

    public Color GetRandomColor(WorldType worldType)
    {
        return GetWorldMarketing(worldType).colors.RandomElement().ChangeA(1);
    }

    public Texture GetTransparentTexture()
    {
        return transparentTexture;
    }

    private WorldMarketing GetWorldMarketing(WorldType worldType)
    {
        WorldType selectedWorldType = worldType;
        WorldMarketing result = worldMarketings.Find(x=> x.worldType == selectedWorldType);
        if (result == null)
        {
            DL.LogWarning("World Marketing not found");
            result = worldMarketings.RandomElement();
        }
        return result;
    }

    public int GetWorldDependentAmount()
    {
        return worldMarketings.Count;
    }

}

[System.Serializable]
public class LogoTexturePair
{
    public Texture neonLogo;
    public Texture vectorLogo;
    public Texture[] alternativeNeonLogos;

    public Texture GetRandomLogo()
    {
        if(alternativeNeonLogos.Length == 0)
        {
            return neonLogo;
        }
        else
        {
            List<Texture> logos = new List<Texture>();
            logos.Add(neonLogo);
            logos.AddRange(alternativeNeonLogos);

            return logos.RandomElement();
        }
    }
}

[System.Serializable]
public class EasterEggBillboard
{
    public LogoTexturePair logoTexturePairs;
    public string countryCode;
}

[System.Serializable]
public class WorldMarketing : WorldDependent
{ 
    public List<LogoTexturePair> logoTexturePairs;
    public List<Color> colors;
    public List<EasterEggBillboard> easterEggs;
}
