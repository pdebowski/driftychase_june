using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RoadBlockadeAnimation : MonoBehaviour
{

    private Material quadMaterial;
    
    void Awake()
    {
        quadMaterial = GetComponentInChildren<Renderer>().material;

    }

    void OnEnable()
    {
         quadMaterial.SetColor("_ArrowColor", WorldEffectsManager.Instance.GetArrowColor());
         quadMaterial.SetColor("_BarColor", WorldEffectsManager.Instance.GetBarColor());
    }


    public void SetTiling(float value)
    {
         Vector3 newTiling = quadMaterial.GetTextureScale("_Texture"); 
         newTiling.x *= value;
            quadMaterial.SetTextureScale("_Texture",newTiling);
    }
}
