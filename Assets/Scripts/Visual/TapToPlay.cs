﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TapToPlay : MonoBehaviour
{

    [SerializeField]
    private BaseAnimation alphaAnimation;

    void Awake()
    {
        TextTransformationShaderHelper text = GetComponent<TextTransformationShaderHelper>();
		if (!StartScreenKeysManager.isStartKeysAnimAvailable)
		{
			Color color = text.TintColor;
			color.a = 1;
			text.TintColor = color;
		}
		else
		{
			alphaAnimation.enabled = true;
		}
	}
}
