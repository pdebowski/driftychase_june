using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WorldsRoadColorSelector : MonoBehaviour
{

    [SerializeField]
    private Material roadMaterial;
    [SerializeField]
    private Material stripesMaterial;
    [SerializeField]
    private WorldOrder worldOrder;
    [SerializeField]
    private List<WorldRoads> worldsRoads;

    [Header("Animation")]
    [SerializeField]
    private float duration;

    private WorldRoads previousRoad;
    private WorldRoads currentRoad;

    void Awake()
    {
        previousRoad = new WorldRoads();
        previousRoad.CopyFrom(worldsRoads[0]);        
    }

    void OnEnable()
    {
        PlayerSpeedManager.OnWorldChange += ChangeRoadColor;
    }

    void OnDisable()
    {
        PlayerSpeedManager.OnWorldChange -= ChangeRoadColor;
    }

    private void ChangeRoadColor()
    {
        WorldType currentWorld = WorldOrder.CurrentWorld;

        previousRoad.CopyFrom(currentRoad);
		previousRoad.roadColor = roadMaterial.color;

        currentRoad = worldsRoads.Find(x => x.world == currentWorld);

		if (GameManager.Instance.CurrentState == GameManager.GameState.InMenus) {
			ChangeRoadConfiguration (1);
            if (WorldOrder.CurrentWorld == WorldType.Psycho)
            {
                StartCoroutine(PsychodelicRoadColorize());
            }
        } else {

			StartCoroutine (ChangeRoadColorCor ());
		}
    }

	private void ChangeRoadConfiguration(float normalizedTime){
		
		stripesMaterial.color = Color.Lerp(previousRoad.stripesColor, currentRoad.stripesColor, normalizedTime);

		var newColor= Color.Lerp(previousRoad.roadColor, currentRoad.roadColor, normalizedTime);
		if (normalizedTime < 0.5)
		{
			newColor.a = Mathf.Lerp(previousRoad.roadColor.a, 0, normalizedTime*2);
		}
		else
		{
			if (roadMaterial.mainTexture!= currentRoad.tex2d)
			{
                roadMaterial.mainTextureScale = new Vector2(currentRoad.shaderTransformTexRoad.x, currentRoad.shaderTransformTexRoad.y);
                roadMaterial.mainTextureOffset = new Vector2(currentRoad.shaderTransformTexRoad.z, currentRoad.shaderTransformTexRoad.w);
                roadMaterial.mainTexture = currentRoad.tex2d;
			}

            if (Mathf.Approximately(normalizedTime, 1))
            {
                roadMaterial.mainTextureScale = new Vector2(currentRoad.shaderTransformTexRoad.x, currentRoad.shaderTransformTexRoad.y);
                roadMaterial.mainTextureOffset = new Vector2(currentRoad.shaderTransformTexRoad.z, currentRoad.shaderTransformTexRoad.w);
            }

			newColor.a = Mathf.Lerp(0, currentRoad.roadColor.a, (normalizedTime-0.5f)*2f);
		}

		roadMaterial.color = newColor;
        newColor.a = 1;
        PrefabReferences.Instance.Camera.backgroundColor = newColor;




    }




    IEnumerator ChangeRoadColorCor()
    {
		float t = 0;
		while (t < 1) {
            if (GameManager.Instance.CurrentState == GameManager.GameState.Playing)
            {
                t += Time.deltaTime / duration;
            }
			ChangeRoadConfiguration (t);
			yield return 0;
		}

		if (WorldOrder.CurrentWorld == WorldType.Psycho) 
		{
			StartCoroutine (PsychodelicRoadColorize ());
		}
    }




	IEnumerator PsychodelicRoadColorize()
	{
		PsychodelicConfig.ConfigVar conf = WorldEffectsManager.Instance.GetPsychodelicConfig ();
		Color prevColor, targetColor;
		int colorIt = 0;


		while (WorldOrder.CurrentWorld == WorldType.Psycho) 
		{
			float t = 0;

			prevColor = roadMaterial.color;
			targetColor = conf.colors [colorIt];
			colorIt = (colorIt + 1) % conf.colors.Length;

			while (WorldOrder.CurrentWorld == WorldType.Psycho && t < 1f) 
			{
				if (GameManager.Instance.CurrentState==GameManager.GameState.Playing)
				{
					t += Time.deltaTime / conf.duration;
				}
				roadMaterial.color = Color.Lerp (prevColor,targetColor,t);
				yield return 0;

			}
			yield return 0;
		}
	}

}



[System.Serializable]
public class WorldRoads
{
    public WorldType world;
    public Color stripesColor;
    public Color roadColor;
    public Vector4 shaderTransformTexRoad= new Vector4(1,1,0,0);
    public Texture2D tex2d;


    public void CopyFrom(WorldRoads light)
    {
        if (light == null)
        {
            return;
        }

        tex2d = light.tex2d;
        roadColor = light.roadColor;
        stripesColor = light.stripesColor;
        world = light.world;
        shaderTransformTexRoad = light.shaderTransformTexRoad;
    }
}