﻿using UnityEngine;
using System.Collections;

public class CountingDown : MonoBehaviour
{

    [SerializeField]
    private GameObject[] objects;
    [SerializeField]
    private float timeSpan = 1f;
    [SerializeField]
    private AnimationCurve numberScalingCurve;

    private float timer = 0;
    private bool isEnabled = false;

    void Update()
    {
        if (isEnabled)
        {

            timer += Time.unscaledDeltaTime;
            int index = (int)(timer / timeSpan % objects.Length);
            SetActiveObject(index);
            objects[index].transform.localScale = CommonMethods.Vector3FromFloat(numberScalingCurve.Evaluate((timer % timeSpan) * 1 / timeSpan));

            if (timer > timeSpan * objects.Length)
            {
                Reset();
            }
        }
    }

    public void Run()
    {
        Reset();
        isEnabled = true;
    }

    private void Reset()
    {
        isEnabled = false;
        timer = 0;
        TurnOffAll();
    }

    private void SetActiveObject(int index)
    {
        for (int i = 0; i < objects.Length; i++)
        {
            objects[i].SetActive(i == index);
        }
    }

    private void TurnOffAll()
    {
        SetActiveObject(-1);
    }

    public float GetRunTime()
    {
        return timeSpan * (objects.Length - 1);
    }

    public float GetOneNumberTimeSpan()
    {
        return timeSpan;
    }

}
