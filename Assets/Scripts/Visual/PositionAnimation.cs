﻿using UnityEngine;
using System.Collections;
using System;

public class PositionAnimation : BaseAnimation
{
    public Vector3 DistanceToTravel { get { return distanceToTravel; } private set {; } }

    [SerializeField]
    private Vector3 distanceToTravel;
    [SerializeField]
    private AnimationCurve movement;

    private Vector3 startPosition;

    protected override void Init()
    {
        startPosition = transform.localPosition;
    }

    protected override void Calculate(float phase)
    {
        transform.localPosition = Vector3.Lerp(startPosition, startPosition + distanceToTravel, phase);
    }
}
