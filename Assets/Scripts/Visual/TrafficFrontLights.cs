﻿using UnityEngine;
using System.Collections;

public class TrafficFrontLights : MonoBehaviour
{

    private const float TRAFFIC_FRONT_LIGHTS_Y = 0.2f;

    private Vector3 startLocalPos;
    private MeshRenderer meshRenderer;

    void Awake()
    {
        startLocalPos = transform.localPosition;
        meshRenderer = GetComponentInChildren<MeshRenderer>(true);
    }

    void Update()
    {
        if (meshRenderer.isVisible)
        {
            transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles.ChangeX(90).ChangeZ(0).ChangeY(transform.parent.rotation.eulerAngles.y + 270));
            transform.localPosition = startLocalPos;
            transform.position = transform.position.ChangeY(TRAFFIC_FRONT_LIGHTS_Y);
        }
    }
}
