﻿using UnityEngine;
using System.Collections;

public class StoreWindowsApplier : MonoBehaviour {

    [SerializeField]
    private StoreWindowMeshRenderer[] storeWindowMeshRenderers;
    [SerializeField]
    private MeshRenderer[] lightEmitMeshRenderers;

    private const float emitAlpha = 60 / 255f;


    public void SetStoreWindows(Texture logo, Color color)
    {
        SetLightEmitColor(color.ChangeA(emitAlpha));

        if (Random.Range(0f, 1f) < BillboardSystem.chanceToSpawnStoreWindowBillboard)
        {
            foreach (var storeWindowMeshRenderer in storeWindowMeshRenderers)
            {
                if (storeWindowMeshRenderer.orientation == StoreWindowOrientation.Horizontal)
                {
                    SetWindowLogo(storeWindowMeshRenderer.meshRenderer, logo, color, true,false);
                }
                else if (storeWindowMeshRenderer.orientation == StoreWindowOrientation.Vertical)
                {
                    SetWindowLogo(storeWindowMeshRenderer.meshRenderer, logo, color, false, true);
                }
                else if (storeWindowMeshRenderer.orientation == StoreWindowOrientation.Square)
                {
                    SetWindowLogo(storeWindowMeshRenderer.meshRenderer, logo, color,false, false);
                }
                else if (storeWindowMeshRenderer.orientation == StoreWindowOrientation.NoTexture)
                {
                    SetWindowColor(storeWindowMeshRenderer.meshRenderer, color);
                }
            }
        }
        else
        {
            foreach (var storeWindowMeshRenderer in storeWindowMeshRenderers)
            {
                SetWindowColor(storeWindowMeshRenderer.meshRenderer, color);
            }
        }
    }

    private void SetWindowLogo(MeshRenderer meshRenderer,Texture texture, Color color, bool xScaling, bool yScaling)
    {
        meshRenderer.material.SetTexture("_LogoTexture", texture);
        meshRenderer.material.color = Color.black;
        meshRenderer.material.SetColor("_BackgroundColor", color);
        meshRenderer.material.SetColor("_LogoColor", Color.white);

        if (xScaling)
        {
            float scaleX = meshRenderer.transform.lossyScale.y / meshRenderer.transform.lossyScale.x;
            meshRenderer.material.SetFloat("_ScaleX", scaleX);
        }
        if(yScaling)
        {
            float scaleY = meshRenderer.transform.lossyScale.x / meshRenderer.transform.lossyScale.y;
            meshRenderer.material.SetFloat("_ScaleY", scaleY);
        }
    }

    private void SetWindowColor(MeshRenderer meshRenderer,Color color)
    {
        meshRenderer.material.SetTexture("_LogoTexture", PrefabReferences.Instance.BillboardSystem.GetTransparentTexture());
        meshRenderer.material.color = color;
        meshRenderer.material.SetColor("_BackgroundColor", Color.black);
        meshRenderer.material.SetColor("_LogoColor", Color.black);
    }

    private void SetLightEmitColor(Color color)
    {
        foreach (var lightEmitMeshRenderer in lightEmitMeshRenderers)
        {
            lightEmitMeshRenderer.material.SetColor("_TintColor", color);
        }
    }

}

[System.Serializable]
public class StoreWindowMeshRenderer
{
    public MeshRenderer meshRenderer;
    public StoreWindowOrientation orientation;
}

public enum StoreWindowOrientation
{
    Horizontal,
    Vertical,
    Square,
    NoTexture
}
