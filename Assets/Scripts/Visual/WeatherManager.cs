using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WeatherManager : MonoBehaviour
{
    [SerializeField]
    private List<WorldWeather> worldsWeather;
    [SerializeField]
    private List<WeatherObjectsHolder> weatherObjectsHolders;
    [SerializeField]
    private WorldOrder worldOrder;

    private WeatherObjectsHolder lastWeather;

    void Awake()
    {
        WorldDependent.CheckForDuplicates(worldsWeather, this.GetType().Name);
    }

    void OnEnable()
    {
        PlayerSpeedManager.OnWorldChange += Refresh;
    }

    void OnDisable()
    {
        PlayerSpeedManager.OnWorldChange -= Refresh;
    }

    private void Refresh()
    {
        TurnOffAllWeathers();
        WorldType selectedWorldType = WorldOrder.CurrentWorld;
        List<WeatherChance> currentWorldWeather = worldsWeather.Find(x => x.worldType == selectedWorldType).weathers;
        float randomHit = Random.value;
        if (currentWorldWeather.Count > 0)
        {
            WeatherType choosedWeather = WeatherType.Nothing;
            float currentProbalitySum = 0;
            for (int i = 0; i < currentWorldWeather.Count; i++)
            {
                if (randomHit >= currentProbalitySum && randomHit < (currentWorldWeather[i].chance + currentProbalitySum))
                {
                    choosedWeather = currentWorldWeather[i].type;
                    break;
                }
                else
                {
                    currentProbalitySum += currentWorldWeather[i].chance;
                }
            }

            if (choosedWeather != WeatherType.Nothing)
            {
                TurnOnWeather(weatherObjectsHolders.Find(x => x.type == choosedWeather));
            }

        }
    }

    private void TurnOnWeather(WeatherObjectsHolder weatherObjectHolder)
    {
        foreach (var item in weatherObjectHolder.objects)
        {
            item.SetActive(true);
        }
    }

    private void TurnOffAllWeathers()
    {
        foreach (var weatherObjectHolder in weatherObjectsHolders)
        {
            foreach (var item in weatherObjectHolder.objects)
            {
                item.SetActive(false);
            }
        }
    }

    public int GetWorldDependentAmount()
    {
        return worldsWeather.Count;
    }
}

[System.Serializable]
public class WeatherObjectsHolder
{
    public WeatherType type;
    public GameObject[] objects;
}

[System.Serializable]
public class WorldWeather : WorldDependent
{
    public List<WeatherChance> weathers;
}

[System.Serializable]
public class WeatherChance
{
    public float chance;
    public WeatherType type;
}

public enum WeatherType
{
    Nothing,
    Rain,
    Snow
}
