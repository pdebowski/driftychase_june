using UnityEngine;
using System.Collections;

public class WindowsPulsing : BaseAnimation
{

    [SerializeField]
    private AnimationCurve alphaCurve;

    private MeshRenderer meshRenderer;
    private float startAlpha;
    private float maxOffset;
    private float randomPhaseOffset;
    private float randomAlphaDiff;

    protected override void Init()
    {
        meshRenderer = GetComponent<MeshRenderer>();
        startAlpha = meshRenderer.material.GetColor("_TintColor").a;
        maxOffset = Random.Range(1f, 1.0f);
        randomPhaseOffset = Random.Range(0, 1f);
        duration = 0.15f;
        duration = duration * Random.Range(0.5f, 1.5f);
        randomAlphaDiff = Random.Range(-0.2f, 0.2f);
        randomAlphaDiff = 0;
        repeat = true;

        meshRenderer.material.SetFloat("_Duration", duration);

    }

    protected override void Calculate(float phase) {  }
 /*   protected override void Calculate(float phase)
    {
        if (meshRenderer.isVisible)
        {
            
            float newAlpha = (Mathf.Pow(alphaCurve.Evaluate(phase + randomPhaseOffset), maxOffset) + randomAlphaDiff) * startAlpha;
            CommonMethods.SetAlphaToMaterial(meshRenderer, newAlpha, "_TintColor");
        }
    }

    */


}
