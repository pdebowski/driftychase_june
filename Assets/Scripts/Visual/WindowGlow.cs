﻿using UnityEngine;
using System.Collections;

public class WindowGlow : MonoBehaviour {

    [SerializeField]
    private float maxOffset = 3;

    void Start()
    {
        transform.position += transform.right * Random.Range(-0.7f, 0.7f) * maxOffset;
    }
	
}
