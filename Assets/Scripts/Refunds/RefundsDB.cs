﻿using UnityEngine;
using System.Collections;

public class RefundsDB : IEnumerable
{

    enum RefundProps
    {
        CODE = 0,
        VALUE = 1,
        NUM_OF_CITY_PROPS = 2
    };

    public class RefundData
    {
        public string Code;
        public int Value;
    }

    private RefundData[] refundsData;

    public RefundsDB()
    {
        string remoteData = GoogleTagManagerPlugin.Instance.GetString("Refunds");
        if (remoteData.Length > 0)
        {
            string[] refundsData = remoteData.Split(';');
            this.refundsData = new RefundData[refundsData.Length];

            int refundID = 0;
            foreach (string refundDataItem in refundsData)
            {
                string[] cityValues = refundDataItem.Split(':');
                if (cityValues.Length >= (int)RefundProps.NUM_OF_CITY_PROPS)
                {
                    this.refundsData[refundID] = new RefundData();
                    this.refundsData[refundID].Code = cityValues[(int)RefundProps.CODE];
                    this.refundsData[refundID].Value = int.Parse(cityValues[(int)RefundProps.VALUE]);
                }
                else
                {
                    CrimsonError.LogError("Refund Data DB parse error in refund " + refundID);
                }

                ++refundID;
            }
        }
        else
        {
            CrimsonError.LogError("Refund Data DB parse error, remote string empty");
        }
    }

    public IEnumerator GetEnumerator()
    {
        return refundsData.GetEnumerator();
    }

}
