﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System;
using System.Collections.Generic;

public class RefundsManager : MonoBehaviour
{

    private const int CODE_LENGHT = 7;
    private const string REFUND_CODE = "RefundCode";
    private const string WAS_REFUND = "WAS_REFUND";

    public static bool refundNeedCheck = false;

    [SerializeField]
    private HUDValuesApplier HUD;

    void Awake()
    {
        if (RefundCode == "")
        {
            RefundCode = GenerateCode(CODE_LENGHT);
        }
    }

    void Start()
    {
        //   CheckRefund();
    }

    void Update()
    {
        if (refundNeedCheck)
        {
            refundNeedCheck = false;
            CheckRefund();
        }
    }

    public void CheckRefund()
    {
        if (WasRefund)
        {
            return;
        }
        RefundsDB refundsDB = GTMParameters.RefundsDB;
        if (refundsDB != null)
        {
            foreach (RefundsDB.RefundData refundData in refundsDB)
            {
                if (refundData.Code.ToUpper() == RefundCode.ToUpper())
                {
                    PrefsDataFacade.Instance.AddTotalCash(refundData.Value);
                    HUD.Refresh();
                    WasRefund = true;
                    PrefsDataFacade.Instance.SaveGameStateCloud();
                    ScoreCounter.OnCashOrScoreChanged();
                }
            }
        }
        else
        {
            CrimsonError.LogError("Couldn't obtain RefundsDB");
        }
    }

    private string GenerateCode(int length)
    {
        const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        return new string(Enumerable.Repeat(chars, length).Select(s => s[UnityEngine.Random.Range(0, s.Length - 1)]).ToArray());
    }


    public static string RefundCode
    {
        get
        {
            return PlayerPrefs.GetString(REFUND_CODE, "");
        }
        set
        {
            PlayerPrefs.SetString(REFUND_CODE, value);
        }
    }

    public static bool WasRefund
    {
        get
        {
            return Convert.ToBoolean(PlayerPrefs.GetInt(WAS_REFUND, 0));
        }
        set
        {
            PlayerPrefs.SetInt(WAS_REFUND, Convert.ToInt32(value));
        }
    }
}
