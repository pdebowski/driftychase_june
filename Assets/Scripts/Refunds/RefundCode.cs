﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
[RequireComponent(typeof(Text))]
public class RefundCode : MonoBehaviour
{

    void Start()
    {
        Text text = GetComponent<Text>();
        text.text = RefundsManager.RefundCode;
    }

}
