﻿using UnityEngine;
using System.Collections;

public class LeaderboardFix : MonoBehaviour
{

    private AppleRemoteController appleController;
    private float leaderboardOpenTime = -100;
    private bool leaderboardOpened = false;
    private int focusFalseCounter = 0;

#if UNITY_TVOS
    void Start()
    {
        GameplayManager.OnSessionStarted += ResetStatus;
        appleController = AppleRemoteController.Instance;
    }


    void OnApplicationFocus(bool focus)
    {
        //Max 2.5 sec
        if (focus == false && leaderboardOpenTime + 2.5f > Time.unscaledTime)
        {
            leaderboardOpened = true;
            leaderboardOpenTime = -100;
        }

        if (leaderboardOpened)
        {

            if (focus == false)
            {
                if (focusFalseCounter == 0)
                {
                    appleController.SetBlockInput(true);
                    StartScreenKeysManager.isLeaderboardsButtonEnabled = false;
                }
                focusFalseCounter++;
            }
            else
            {

                focusFalseCounter--;

                if (focusFalseCounter <= 0)
                {
                    ResetStatus();
                }

                appleController.SetBlockInput(false);
            }
        }
    }
#endif

    public void OnLeaderboardsOpen()
    {
        leaderboardOpenTime = Time.unscaledTime;
    }

#if UNITY_TVOS
    void ResetStatus()
    {
        appleController.SetBlockInput(false);
        leaderboardOpened = false;
        focusFalseCounter = 0;
        StartScreenKeysManager.isLeaderboardsButtonEnabled = true;
        leaderboardOpenTime = -100;
    }
#endif

}
