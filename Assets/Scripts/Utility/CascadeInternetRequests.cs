using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class CascadeInternetRequests : MonoBehaviour {

    delegate IEnumerator InitTrackingObject();

    [SerializeField]
    float timeoutForRequest=3;

    readonly List<InitTrackingObject> initOrder = new List<InitTrackingObject>();

    bool cloudLoaded = false;
    bool rankingInitialized = false;
    bool connectedToStore = false;
    bool restoreFinished = false;

	CrimsonPlugins.AdAdapter adAdapter;
	IAPManager iapManager;

    void Awake()
    {
		adAdapter = GameObject.FindObjectOfType<CrimsonPlugins.AdAdapter> ();
		iapManager = GameObject.FindObjectOfType<IAPManager> ();

        if (Application.platform == RuntimePlatform.IPhonePlayer)
        {
         //   initOrder.Add(LoginToRanking);
            initOrder.Add(IAPConnect);
            initOrder.Add(Cloud);
            initOrder.Add(Intersistial);
            initOrder.Add(RewardAd);
        }
        else
        {
        //    initOrder.Add(LoginToRanking);
            initOrder.Add(Intersistial);
            initOrder.Add(IAPConnect);
            initOrder.Add(Cloud);
            initOrder.Add(RewardAd);
        }
    }

    void Start()
    {
        CrimsonPlugins.RankingCloudAdapter.OnAuthenticationChanged += (notMatter) =>
        {
            rankingInitialized = true;
			StartCoroutine (NotificationInitializeAlmostInstant ());
			Debug.Log("NotificationCorStarted");
        };

        CrimsonPlugins.RankingCloudAdapter.OnSavedGameLoadFailed += () =>
        {
            cloudLoaded = true;
        };

        CrimsonPlugins.RankingCloudAdapter.OnSavedGameSuccessfullyLoaded += (notMatter) =>
        {
            cloudLoaded = true;
        };

		iapManager.GetComponent<CrimsonPlugins.ProductManager>().m_productStoreManager.OnConnectedToStoreEvent += () =>
        {
            connectedToStore = true;
        };

		iapManager.GetComponent<CrimsonPlugins.ProductManager>().m_productStoreManager.OnRestoreTransactionsFinishedEvent += () =>
        {
            restoreFinished = true;
        };

        //StartCoroutine(DoHttpCascadeRequests());
        IntersistialInstant();
		RewardAdInstant();
        StartCoroutine(IAPConnect());
		CloudInstant();
    }

    IEnumerator DoHttpCascadeRequests()
    {
        foreach (var method in initOrder)
        {
            yield return StartCoroutine(method());
        }

        initOrder.Clear();

    }

	IEnumerator NotificationInitializeAlmostInstant ()
	{
		const float waitDuration = 0.5f;
		float t = 0;
		while (t < 1f) 
		{
			t += Time.unscaledDeltaTime/waitDuration;
			yield return null;
		}

		CrimsonPlugins.LocalNotificationAdapter.Instance.InitizeNotification ();
	}

	void RewardAdInstant()
	{
		adAdapter.InternalCascadeRequestRewardAd();
	}

	void IntersistialInstant()
	{
		adAdapter.RequestBanner();
		adAdapter.RequestInterstitial();
	}

	void CloudInstant()
	{
		PrefsDataFacade.Instance.LoadGameFromCloud();
	}

	public void LoginToRankingInstant()
	{
		CrimsonPlugins.RankingCloudAdapter.Instance.p_rankingInstance.Login();
	}

	void IAPConnectInstant()
	{
		iapManager.Initialize();
	}	

    

    IEnumerator RewardAd()
    {
		adAdapter.InternalCascadeRequestRewardAd();
		yield return StartCoroutine(TimeOutHelper(() => !adAdapter.IsRewardAdReady(),timeoutForRequest));

        DL.Log("Cascade : RewardAd " + (adAdapter.IsRewardAdReady() ? "initialized" : "timeout"));
    }

    IEnumerator Intersistial()
    {
        adAdapter.RequestBanner();
        yield return new WaitForSeconds(0.5f);
        adAdapter.RequestInterstitial();
        yield return StartCoroutine(TimeOutHelper(() => !adAdapter.InternalCascadeFullAdLoaded(), timeoutForRequest));

        DL.Log("Cascade : FullScreenAd " + (adAdapter.InternalCascadeFullAdLoaded() ? "initialized" : "timeout"));
    }

    IEnumerator Cloud()
    {
        PrefsDataFacade.Instance.LoadGameFromCloud();
        
        yield return StartCoroutine(TimeOutHelper(() => !CrimsonPlugins.RankingCloudAdapter.Instance.p_saveGameInstance.p_IsAuthenticated, 1));

        if (CrimsonPlugins.RankingCloudAdapter.Instance.p_saveGameInstance.p_IsAuthenticated)
        {
            yield return StartCoroutine(TimeOutHelper(() => !cloudLoaded, timeoutForRequest));
        }

        DL.Log("Cascade : FullScreenAd " + (cloudLoaded ? "initialized" : "timeout"));
    }

    IEnumerator LoginToRanking()
    {
        CrimsonPlugins.RankingCloudAdapter.Instance.p_rankingInstance.Login();
        yield return StartCoroutine(TimeOutHelper(() => !rankingInitialized, timeoutForRequest));

        DL.Log("Cascade : Ranking " + (rankingInitialized ? "initialized" : "timeout"));
    }

    IEnumerator IAPConnect()
    {
        yield return 0;
        iapManager.Initialize();

///        yield return StartCoroutine(TimeOutHelper(() => !connectedToStore, timeoutForRequest));

       // if (connectedToStore && Application.platform == RuntimePlatform.Android)
       // {
      //      iapManager.RestorePurchases();
       //     yield return StartCoroutine(TimeOutHelper(() => !restoreFinished, timeoutForRequest));
       // }

        DL.Log("Cascade : IAP " + (connectedToStore ? "connected" : "timeout") + ", "+ (restoreFinished ? "and restored" : "and not restored"));
    }



    delegate bool MainCondition();
    IEnumerator TimeOutHelper(MainCondition breakCondition, float timeout,bool unscaledTime=true)
    {
        float t = 0;

        while (t <= 1 && breakCondition())
        {
            if (unscaledTime)
            {
                t += Time.unscaledDeltaTime / timeoutForRequest;
            }
            else
            {
                t += Time.deltaTime / timeoutForRequest;
            }
            yield return 0;
        }

    }

}
