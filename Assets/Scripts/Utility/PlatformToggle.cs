using UnityEngine;
using System.Collections;

public class PlatformToggle : MonoBehaviour
{

    [SerializeField]
    private GameObject[] android_ios;
    [SerializeField]
    private GameObject[] tv;

    void Awake()
    {
        Destroy(gameObject);
        return;
        if (PlatformRecognition.IsTV)
        {
            tv.SetActive(true);
            android_ios.SetActive(false);
            return;
        }

#if UNITY_TVOS
        tv.SetActive(true);
        android_ios.SetActive(false);
#else
        tv.SetActive(false);
        android_ios.SetActive(true);
#endif
    }
}
