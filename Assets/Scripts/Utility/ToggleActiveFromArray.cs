﻿using UnityEngine;
using System.Collections;

public class ToggleActiveFromArray : MonoBehaviour {

    [SerializeField]
    private GameObject[] objects; 

    private int active = 0;
	
    void Awake()
    {
        SetActive(0);
    }

    public void ToggleNext()
    {
        if(active < objects.Length - 1)
        {
            active++;
        }
        else
        {
            active = 0;
        }

        SetActive(active);
    }

    private void SetActive(int index)
    {
        for (int i = 0; i < objects.Length; i++)
        {
            objects[i].SetActive(i == index);
        }
    }

}
