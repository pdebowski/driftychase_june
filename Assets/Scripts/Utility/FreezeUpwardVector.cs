﻿using UnityEngine;
using System.Collections;

public class FreezeUpwardVector : MonoBehaviour
{

    void FixedUpdate()
    {
        transform.rotation = Quaternion.LookRotation(transform.forward, Vector3.up);
    }
}
