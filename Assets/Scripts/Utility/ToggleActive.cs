﻿using UnityEngine;
using System.Collections;

public class ToggleActive : MonoBehaviour {

	[SerializeField]
	GameObject holder;

	public void Toggle()
	{
		holder.SetActive(!holder.activeSelf);
	}
}
