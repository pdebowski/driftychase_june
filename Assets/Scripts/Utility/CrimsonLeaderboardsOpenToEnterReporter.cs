﻿using UnityEngine;
using System.Collections;

public class CrimsonLeaderboardsOpenToEnterReporter : MonoBehaviour {

    private bool wasOpened = false;
	
	void Start ()
    {
        CrimsonAnalytics.CrimsonLeaderboards.MainScreenShowToEnter.Showed.LogEvent();
	}
	
	public void ReportCardOpened()
    {
        if(!wasOpened)
        {
            wasOpened = true;
            CrimsonAnalytics.CrimsonLeaderboards.MainScreenShowToEnter.Entered.LogEvent();
        }
        else
        {
            CrimsonAnalytics.CrimsonLeaderboards.MainScreenShowToEnter.EnteredAgain.LogEvent();
        }
    }
}
