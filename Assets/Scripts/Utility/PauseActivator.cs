﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(UnityEngine.UI.Button))]
public class PauseActivator : MonoBehaviour
{
    void OnEnable()
    {
        AppleRemoteController.OnButtonClick += OnAppleRemoteClick;

    }

    void OnDisable()
    {
        AppleRemoteController.OnButtonClick -= OnAppleRemoteClick;
    }

    private void OnAppleRemoteClick(AppleRemoteController.Buttons button)
    {
        if (button == AppleRemoteController.Buttons.Pause_Play)
        {
            TryTurnOnPause();
        }
    }

    private void TryTurnOnPause()
    {
        if (GameManager.Instance.CurrentState == GameManager.GameState.Playing)
        {
            GetComponent<UnityEngine.UI.Button>().onClick.Invoke();
        }
    }
}
