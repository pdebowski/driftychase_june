﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(UnityEngine.UI.Button))]
public class PauseDeactivator : MonoBehaviour
{

    void OnEnable()
    {
        AppleRemoteController.OnButtonClick += OnAppleRemoteClick;

    }

    void OnDisable()
    {
        AppleRemoteController.OnButtonClick -= OnAppleRemoteClick;
    }

    private void OnAppleRemoteClick(AppleRemoteController.Buttons button)
    {
        if (button == AppleRemoteController.Buttons.Pause_Play)
        {
            TryTurnOffPause();
        }
    }

    private void TryTurnOffPause()
    {
        if (GameManager.Instance.CurrentState == GameManager.GameState.Pause)
        {
            GetComponent<UnityEngine.UI.Button>().onClick.Invoke();

        }
    }
}
