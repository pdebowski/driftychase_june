using UnityEngine;
using System.Collections;

public class LangAndroidProxy {

#if UNITY_ANDROID
    public string GetDeviceLocale()
    {
        string ret;
        using (AndroidJavaClass cls = new AndroidJavaClass("java.util.Locale"))
        {
            using (AndroidJavaObject locale = cls.CallStatic<AndroidJavaObject>("getDefault"))
            {
                string lang = locale.Call<string>("getLanguage");
                string country = locale.Call<string>("getCountry");

                ret = lang;
            }
        }
        return ret;
    }

    public string GetDeviceCountryCode()
    {
        string ret;
        using (AndroidJavaClass cls = new AndroidJavaClass("java.util.Locale"))
        {
            using (AndroidJavaObject locale = cls.CallStatic<AndroidJavaObject>("getDefault"))
            {
                string lang = locale.Call<string>("getLanguage");
                string country = locale.Call<string>("getCountry");

                ret = country;
            }
        }
        return ret;
    }

#endif

}
