﻿using UnityEngine;
using System.Collections;

public static class RewardAd
{

    private static System.Action<bool> callback = null;

    public static bool IsReady()
    {
        if (GTMParameters.IsInterstitialInsteadOfVideo)
        {
            return PrefabReferences.Instance.AdAdapter.IsRewardInterstitialReady();
        }
        else
        {
            return PrefabReferences.Instance.AdAdapter.IsRewardAdReady();
        }
    }

    public static void Show(string placement, System.Action<bool> onFinishCallback)
    {
        if (GTMParameters.IsInterstitialInsteadOfVideo)
        {
            PrefabReferences.Instance.AdAdapter.OnIntersistialClosed += OnInterstitialClosed;
            callback = onFinishCallback;

            if (PrefabReferences.Instance.AdAdapter.ShowRewardInterstitial())
            {
                ;
            }
            else
            {
                PrefabReferences.Instance.AdAdapter.OnIntersistialClosed -= OnInterstitialClosed;
                SendCallback(false);
            }
        }
        else
        {
            PrefabReferences.Instance.AdAdapter.ShowRewardAds(placement, onFinishCallback);
        }
    }

    private static void OnInterstitialClosed()
    {
        PrefabReferences.Instance.AdAdapter.OnIntersistialClosed -= OnInterstitialClosed;
        SendCallback(true);
    }

    private static void SendCallback(bool success)
    {
        if (callback != null)
        {
            callback(success);
            callback = null;
        }
        else
        {
            Debug.LogError("Callback after ad closed is null ! ");
        }
    }


}
