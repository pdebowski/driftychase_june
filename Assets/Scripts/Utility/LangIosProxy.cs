﻿using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;

public class LangIosProxy {

	[DllImport ("__Internal")]
	private static extern string _GetDeviceCountryCode();

	[DllImport ("__Internal")]
	private static extern string _GetDeviceLocale();


	public string GetDeviceCountryCode()
	{
		return _GetDeviceCountryCode ();
	}

	public string GetDeviceLocale()
	{
		return _GetDeviceLocale ();
	}

}
