﻿using UnityEngine;
using System.Collections;

public class TVDetector : MonoBehaviour {

    [SerializeField]
    private bool simulateTVInEditor = false;

    public static System.Action<bool> OnTVCheck = delegate { };
    public static bool WasChecked { get; private set; }

#if UNITY_EDITOR
    void Awake()
    {
        WasChecked = false;
    }

    void Start()
    {
        if (simulateTVInEditor)
        {
            //GlobalSettings.IsTV = true;
            WasChecked = true;
            OnTVCheck(true);
        }
        else
        {
            //GlobalSettings.IsTV = false;
            WasChecked = true;
            OnTVCheck(false);
        }
    }
#elif UNITY_TVOS && !UNITY_EDITOR
    void Awake()
    {
        WasChecked = false;
    }

    void Start()
    {
        GlobalSettings.IsTV = true;
        WasChecked = true;
        OnTVCheck(true);

    }
#elif UNITY_IOS && !UNITY_EDITOR
    void Awake()
    {
        WasChecked = false;
    }

    void Start()
    {
//        GlobalSettings.IsTV = false;
        WasChecked = true;
        OnTVCheck(false);
    }
#else
    void Start()
    {
        TVAppController.DeviceTypeChecked += OnDeviceTypeChecked;
        WasChecked = false;
        TVAppController.Instance.CheckForATVDevice();
    }

    void OnDeviceTypeChecked()
    {
        TVAppController.DeviceTypeChecked -= OnDeviceTypeChecked;
       // GlobalSettings.IsTV = TVAppController.Instance.IsRuningOnTVDevice;
        WasChecked = true;
        OnTVCheck(TVAppController.Instance.IsRuningOnTVDevice);
        
    }
#endif
}