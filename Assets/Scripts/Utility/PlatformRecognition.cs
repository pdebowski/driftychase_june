using UnityEngine;
using System.Collections;

// working only if portrait is default orientation on mobiles
public class PlatformRecognition
{

    public static bool IsMobile
    {
        get { return Screen.height > Screen.width; }
    }

    public static bool IsTV
    {
        get {

#if UNITY_TVOS
            return true;
#elif UNITY_IOS
            return false;
#else        
            return !IsMobile;
#endif
        }
    }

    public static bool IsAppleDevice
    {
        get
        {
#if UNITY_IOS || UNITY_TVOS
            return true;
#else
            return false;
#endif
        }
    }

    public static bool IsAndroidDevice
    {
        get
        {
#if UNITY_ANDROID
            return true;
#else
            return false;
#endif
        }
    }

    public static bool IsAndroidTV
    {
        get { return IsTV && IsAndroidDevice; }
    }

    public static bool IsAppleTV
    {
        get
        {
#if UNITY_TVOS
            return true;
#else
            return false;
#endif
        }
    }

    public static bool IsAndroidMobile
    {
        get { return IsMobile || IsAndroidDevice; }
    }

    public static bool IsIOSMobile
    {
        get
        {
#if UNITY_IOS
            return true;
#else
            return false;
#endif
        }
    }




}
