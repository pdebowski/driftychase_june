﻿using UnityEngine;
using System.Collections;

public class WorldDependentsCohesion : MonoBehaviour
{
    [SerializeField]
    WorldOrder worldOrder;
    [SerializeField]
    private BillboardSystem billboardSystem;
    [SerializeField]
    private TrafficContainer trafficContainer;
    [SerializeField]
    private WeatherManager weatherManager;

    void Awake()
    {
        int secretWorlds = 1; // christmas city cannot be added to worldsOrded
        int worldsAmount = worldOrder.GetWorldDependentAmount() + secretWorlds;

        if (worldsAmount != billboardSystem.GetWorldDependentAmount())
        {
            Debug.LogError(string.Format("Different amount of worlds({0}) in WorldOrder and BillboardSystem({1}) in trafficContainer",
                worldsAmount,
                billboardSystem.GetWorldDependentAmount()));
        }

        if (worldsAmount != trafficContainer.GetWorldDependentAmount())
        {
            Debug.LogError(string.Format("Different amount of worlds({0}) in WorldOrder and TrafficContainer({1}) in trafficContainer",
                worldsAmount,
                trafficContainer.GetWorldDependentAmount()));
        }

        if (worldsAmount != weatherManager.GetWorldDependentAmount())
        {
            Debug.LogError(string.Format("Different amount of worlds({0}) in WorldOrder and WeatherManager({1}) in weaterManager",
               worldsAmount,
                weatherManager.GetWorldDependentAmount()));
        }


    }
}
