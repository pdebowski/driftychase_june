using UnityEngine;
using System.Collections;

//Language codes from ISO 639-1
//check: https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes


public class LangCodes  {

    public string LocaleLowercase
    {
        get
        {
#if UNITY_IOS && !UNITY_EDITOR
			return new LangIosProxy().GetDeviceLocale().ToLower();
#elif UNITY_ANDROID && !UNITY_EDITOR
            return new LangAndroidProxy().GetDeviceLocale().ToLower();
#else
            return GetLocaleISO6391().ToLower();
#endif
        }
    }

	public string CountryCodeLowercase
    {
        get
        {
#if UNITY_IOS && !UNITY_EDITOR
			return new LangIosProxy().GetDeviceCountryCode().ToLower();
#elif UNITY_ANDROID && !UNITY_EDITOR
            return new LangAndroidProxy().GetDeviceCountryCode().ToLower();
#else
            return GetLocaleISO6391().ToLower();
#endif
        }
    }

    string GetLocaleISO6391()
    {
        switch (Application.systemLanguage)
        {
            case SystemLanguage.Afrikaans:
                return "af";
            case SystemLanguage.Arabic:
                return "ar";
            case SystemLanguage.Basque:
                return "eu";
            case SystemLanguage.Belarusian:
                return "be";
            case SystemLanguage.Bulgarian:
                return "bg";
            case SystemLanguage.Catalan:
                return "ca";
            case SystemLanguage.Chinese:
                return "cn";
            case SystemLanguage.Czech:
                return "cs";
            case SystemLanguage.Danish:
                return "da";
            case SystemLanguage.Dutch:
                return "nl";
            case SystemLanguage.English:
                return "en";
            case SystemLanguage.Estonian:
                return "et";
            case SystemLanguage.Faroese:
                return "fo";
            case SystemLanguage.Finnish:
                return "fi";
            case SystemLanguage.French:
                return "fr";
            case SystemLanguage.German:
                return "de";
            case SystemLanguage.Greek:
                return "el";
            case SystemLanguage.Hebrew:
                return "he";
            case SystemLanguage.Hungarian:
                return "hu";
            case SystemLanguage.Icelandic:
                return "is";
            case SystemLanguage.Indonesian:
                return "id";
            case SystemLanguage.Italian:
                return "it";
            case SystemLanguage.Japanese:
                return "ja";
            case SystemLanguage.Korean:
                return "kr";
            case SystemLanguage.Latvian:
                return "lv";
            case SystemLanguage.Lithuanian:
                return "lt";
            case SystemLanguage.Norwegian:
                return "no";
            case SystemLanguage.Polish:
                return "pl";
            case SystemLanguage.Portuguese:
                return "pt";
            case SystemLanguage.Romanian:
                return "ro";
            case SystemLanguage.Russian:
                return "ru";
            case SystemLanguage.SerboCroatian:
                return "sr";
            case SystemLanguage.Slovak:
                return "sk";
            case SystemLanguage.Slovenian:
                return "sl";
            case SystemLanguage.Spanish:
                return "es";
            case SystemLanguage.Swedish:
                return "sv";
            case SystemLanguage.Thai:
                return "th";
            case SystemLanguage.Turkish:
                return "tr";
            case SystemLanguage.Ukrainian:
                return "uk";
            case SystemLanguage.Vietnamese:
                return "vi";
            default:
                return "en";
        }
    }
}
