﻿using UnityEngine;
using System.Collections;
using System;
using System.IO;
using System.Text;

public class LocalAnalytics : MonoBehaviour
{
    private const int SECONDS_INTERVAL = 1;

    [SerializeField]
    private bool analyticsEnabled = true;
    [SerializeField]
    private string fileName = "DEFAULT_FILE_NAME";
    [SerializeField]
    private PlayerSpeedManager playerSpeedManager;

    private DateTime startAppTime;
    private Coroutine saveAppRunTimeCor;
    private string path;
    private bool initialised = false;
    private int randomID;

    void Awake()
    {
        randomID = UnityEngine.Random.Range(0, 1000000);
    }

    void OnEnable()
    {
        OffersSceneManager.OnEnter += OnEnterToOffers;
    }

    void OnDisable()
    {
        OffersSceneManager.OnEnter -= OnEnterToOffers;
    }

    private void OnEnterToOffers()
    {
        if (analyticsEnabled)
        {
            string scoresPath = Application.persistentDataPath + "/scores.txt";
            bool fileExists = File.Exists(scoresPath);

            using (StreamWriter sw = new StreamWriter(scoresPath, true))
            {
                if (!fileExists)
                {
                    sw.Write("PlayerID:StartScore:Score" + Environment.NewLine);
                }

                sw.Write(randomID + ";" + playerSpeedManager.StartScore + ";" + ScoreCounter.Instance.CurrentScore + Environment.NewLine);
                sw.Close();
            }

            // uncomment if u want to see result in logcat
           /* string result;
            using (StreamReader sr = new StreamReader(scoresPath))
            {
                if (fileExists)
                {
                    result = sr.ReadToEnd();
                    Debug.Log(result);
                }
                sr.Close();
            }*/
        }
    }

    private void Initialise()
    {
        if (!initialised)
        {
            initialised = true;
            path = Application.persistentDataPath + "/" + fileName + ".txt";
        }
    }

    void OnApplicationFocus(bool isFocused)
    {
        Initialise();

        if (isFocused)
        {
            RunAnalytics();
        }
    }

    private void RunAnalytics()
    {
        if (analyticsEnabled)
        {
            SaveLastAppRunTime();
            startAppTime = DateTime.Now;

            if (saveAppRunTimeCor == null)
            {
                saveAppRunTimeCor = StartCoroutine(SaveAppRunTimeCor());
            }
        }
    }

    private IEnumerator SaveAppRunTimeCor()
    {
        while (true)
        {
            SaveCurrentAppRunTime();
            yield return new WaitForSeconds(SECONDS_INTERVAL);
        }
    }

    private void SaveCurrentAppRunTime()
    {
        CurrentAppRunTime = Mathf.RoundToInt((float)(DateTime.Now - startAppTime).TotalSeconds);
    }

    private void SaveLastAppRunTime()
    {
        if (CurrentAppRunTime > 0)
        {
            AddAppRunTime(CurrentAppRunTime);
            AddAppRunTimeToFile(CurrentAppRunTime);
        }
    }

    private void AddAppRunTime(int seconds)
    {
        string appRunTimes = AppRunTimes;
        if (appRunTimes != null && appRunTimes.Length > 0)
        {
            appRunTimes += ";" + seconds.ToString();
        }
        else
        {
            appRunTimes = seconds.ToString();
        }

        AppRunTimes = appRunTimes;
    }

    private void AddAppRunTimeToFile(int seconds)
    {
        bool fileExists = File.Exists(path);

        using (StreamWriter sw = new StreamWriter(path, true))
        {
            if (fileExists)
            {
                sw.Write(";");
            }
            sw.Write(seconds.ToString());
            sw.Close();
        }
    }

    private void PrintAppRunTimes(string dataString)
    {
        if (dataString != null && dataString.Length > 0)
        {
            string[] splitedAppRunTimes = dataString.Split(';');
            float avg = 0;

            if (splitedAppRunTimes.Length > 0)
            {
                for (int i = 0; i < splitedAppRunTimes.Length; i++)
                {
                    avg += int.Parse(splitedAppRunTimes[i]);
                    Debug.Log("LocalAnalytics. " + (i + 1) + ": " + splitedAppRunTimes[i]);
                }

                avg /= splitedAppRunTimes.Length;
                Debug.Log("LocalAnalytics. " + "AVG: " + avg);
            }
        }
    }

    public void ClearPrefsData()
    {
        AppRunTimes = "";
    }

    public void ClearDiskData()
    {
        File.Delete(path);
    }

    public void PrintPrefsData()
    {
        PrintAppRunTimes(AppRunTimes);
    }

    public void PrintDiskData()
    {
        bool fileExists = File.Exists(path);

        string result = "";

        if (!fileExists)
        {
            Debug.Log("LocalAnalytics. NoDiskData");
            return;
        }

        using (StreamReader sr = new StreamReader(path))
        {
            if (fileExists)
            {
                result = sr.ReadToEnd();
            }
            sr.Close();
        }

        PrintAppRunTimes(result);
    }


    //---------------------- PLAYER PREFS 

    private static string LOCALANALYTICS_APP_RUN_TIMES = "LOCALANALYTICS_APP_RUN_TIMES";
    private static string LOCALANALYTICS_CURRENT_APP_RUN_TIME = "LOCALANALYTICS_CURRENT_APP_RUN_TIME";

    public static string AppRunTimes
    {
        get
        {
            return PlayerPrefs.GetString(LOCALANALYTICS_APP_RUN_TIMES, null);
        }
        set
        {
            PlayerPrefs.SetString(LOCALANALYTICS_APP_RUN_TIMES, value.ToString());
        }
    }

    public static int CurrentAppRunTime
    {
        get
        {
            return PlayerPrefs.GetInt(LOCALANALYTICS_CURRENT_APP_RUN_TIME, 0);
        }
        set
        {
            PlayerPrefs.SetInt(LOCALANALYTICS_CURRENT_APP_RUN_TIME, value);
        }
    }
}
