using UnityEngine;
using System.Collections;

public class NetworkChecker : Singleton<NetworkChecker>
{

    public bool p_isConnectedToInternet { get { return m_isConnectedToInternet; } }
    volatile bool m_isConnectedToInternet = false;
    
    volatile float m_cooldownTime = 0;

    private void Update()
    {
        m_cooldownTime -= Time.unscaledDeltaTime;

        if (m_cooldownTime < 0)
        {
            Debug.Log("Internet request, prev status: "+ p_isConnectedToInternet);
            m_cooldownTime = 9999999;
            DetermineNetworkReachability();
        }
    }

    void NetworkReachabilityWorker()
    {
        System.Net.WebClient client = null;
        System.IO.Stream stream = null;
        try
        {
            client = new System.Net.WebClient();
            stream = client.OpenRead("http://www.google.com");
            m_isConnectedToInternet = true;
        }
        catch (System.Net.WebException ex)
        {
           // Debug.LogError(ex.Message);
            m_isConnectedToInternet = false;
        }
        finally
        {
            if (client != null) { client.Dispose(); }
            if (stream != null) { stream.Dispose(); }
            m_cooldownTime = m_isConnectedToInternet ? 120 : 45;
        }
    }
    //----------------------------------------------------
    void DetermineNetworkReachability()
    {
        System.Threading.Thread worker = new System.Threading.Thread(NetworkReachabilityWorker);
        worker.Start();
    }
}
