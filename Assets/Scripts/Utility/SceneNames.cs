﻿using UnityEngine;
using System.Collections;

public class SceneNames
{
    public static string GetRealSceneName_LevelFilter(string scene)
    {
        if (GameplayManager.CurrentGameMode != null)
        {
            scene = "Level" + scene;
        }
        string resultSceneName = string.Format("Scenes/{0}", scene);

        return resultSceneName;
    }
}
