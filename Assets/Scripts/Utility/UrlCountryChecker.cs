﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class UrlCountryChecker : MonoBehaviour
{

    public static System.Action<string> OnCountryCheckFinished = delegate { };

    public static string DEFAULT_COUNTRY = "default";
    public static float timeout = 5;
    private const string URL = "http://freegeoip.net/json/";

    public bool p_loading { get; private set; }

    private WWW www = null;

    //------------------------------------------------------------
    void Awake()
    {
        p_loading = false;
    }

    void Start()
    {
        DoRequest();
    }

    private void OnWWWLoaded(string text)
    {
        try
        {
            var N = SimpleJSON.JSON.Parse(text);
            SimpleJSON.JSONNode variableNode = N["country_code"];
            PlayerPrefsAdapter.Country = variableNode.Value;
        }
        catch (System.Exception)
        {
            ;//ugly fix, but sometimes text have unpredictable text
        }

    }

    private void DoRequest()
    {
        if (p_loading)
            Debug.LogWarning("Already loading the result");
        www = new WWW(URL);
        p_loading = true;
        StartCoroutine(GetWWW());
    }

    private IEnumerator GetWWW()
    {
        System.DateTime startTime = System.DateTime.Now;

        while (www != null)
        {
            if (www != null && !string.IsNullOrEmpty(www.error))
            {
                Debug.Log(www.error);
                p_loading = false;
                www = null;
                break;
            }
            else if (www != null && www.isDone && www.bytesDownloaded <= 0)
            {
                p_loading = false;
                www = null;
                break;
            }
            else if (www != null && www.isDone)
            {
                OnWWWLoaded(www.text);
                p_loading = false;
                www = null;
                break;
            }

            if ((System.DateTime.Now - startTime).TotalSeconds > timeout)
            {
                break;
            }

            yield return null;
        }
        OnCountryCheckFinishedAction();
        Destroy(this.gameObject);
    }

    private void OnCountryCheckFinishedAction()
    {
        if (OnCountryCheckFinished != null)
        {
            OnCountryCheckFinished(PlayerPrefsAdapter.Country);
        }
    }

}