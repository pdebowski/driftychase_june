﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using System.Collections.Generic;

public class TextTransformationShaderHelper : BaseMeshEffect
{
    private const string DEFAULT_IU_MATERIAL = "Default UI Material";

    public Color TintColor { get { return color; } set { color = value; } }
    private static readonly List<UIVertex> s_tempVertices = new List<UIVertex>();

    [SerializeField]
    private float yTransformation;
    [SerializeField]
    private float xTransformation;
    [SerializeField]
    private Color color;

    private float lastYTransformation;
    private float lastXTransformation;
    private Color lastColor;

    void Update()
    {
        if (yTransformation != lastYTransformation || xTransformation != lastXTransformation || lastColor != color)
        {
            Text text = GetComponent<Text>();

            if (text.material.name == DEFAULT_IU_MATERIAL)
            {
                text.material = new Material(Shader.Find("Custom/TextTransformation"));
            }

            text.material.SetFloat("_YTransformationFactor", yTransformation);
            text.material.SetFloat("_XTransformationFactor", xTransformation);
            text.material.SetColor("_Color", color);

            lastYTransformation = yTransformation;
            lastXTransformation = xTransformation;
            lastColor = color;
        }
    }

    public override void ModifyMesh(VertexHelper _vh)
    {


        _vh.GetUIVertexStream(s_tempVertices);

        if (s_tempVertices.Count == 0)
        {
            return;
        }
        Vector3 leftDownVert = s_tempVertices[0].position;
        Vector3 rightUpVert = s_tempVertices[0].position;

        for (var i = 0; i <= s_tempVertices.Count - 3; i += 3)
        {

            //uncoment commented lines, and comment line 62 if you want sent to shader letter UV as IN.tangent variable
            /*Vector2 uvMin = Vector2.Min(s_tempVertices[i + 0].uv0, Vector2.Min(s_tempVertices[i + 1].uv0, s_tempVertices[i + 2].uv0));
            Vector2 uvMax = Vector2.Max(s_tempVertices[i + 0].uv0, Vector2.Max(s_tempVertices[i + 1].uv0, s_tempVertices[i + 2].uv0));*/

            Vector3 triangleLeftDown = Vector3.Min(s_tempVertices[i + 0].position, Vector2.Min(s_tempVertices[i + 1].position, s_tempVertices[i + 2].position));
            Vector3 triangleRightUp = Vector3.Max(s_tempVertices[i + 0].position, Vector2.Max(s_tempVertices[i + 1].position, s_tempVertices[i + 2].position));

            leftDownVert = Vector3.Min(leftDownVert, triangleLeftDown);
            rightUpVert = Vector3.Max(rightUpVert, triangleRightUp);

            //var tangent = new Vector4(uvMin.x, uvMin.y, uvMax.x, uvMax.y);

            for (var v = 0; v < 3; ++v)
            {
                UIVertex vertex = s_tempVertices[i + v];
                // vertex.tangent = tangent;
                vertex.normal.x = triangleRightUp.y - triangleLeftDown.y;
                vertex.normal.y = triangleLeftDown.y;
                s_tempVertices[i + v] = vertex;
            }
        }



        Vector3 realSize = rightUpVert - leftDownVert;
        for (int i = 0; i < s_tempVertices.Count; i++)
        {
            UIVertex vertex = s_tempVertices[i];
            float normX = (vertex.position.x + realSize.x / 2) / realSize.x;
            float normY = (vertex.position.y + Mathf.Abs(vertex.normal.y)) / vertex.normal.x;
            vertex.normal.x = normX;
            vertex.normal.y = normY;
            vertex.normal.z = 0;

            vertex.tangent = new Vector4(realSize.x, realSize.y, 0, 0);

            s_tempVertices[i] = vertex;
        }



        _vh.Clear();
        _vh.AddUIVertexTriangleStream(s_tempVertices);
    }

}
