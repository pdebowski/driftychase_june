using UnityEngine;
using System.Collections;

public class PrefabReferences : Singleton<PrefabReferences>
{

    [Header("Scene References")]
    [SerializeField]
    private Transform player;
    [SerializeField]
    private Camera mainCamera;
    [SerializeField]
    private Transform sun;
    [SerializeField]
    private TrafficContainer trafficContainer;
    [SerializeField]
    private ContinueManager continueManager;
    [SerializeField]
    private GameSceneManager gameSceneManager;
    [SerializeField]
    private DailyGiftManager dailyGiftManager;
    [SerializeField]
    private BillboardSystem billboardSystem;
    [SerializeField]
    private CarStatsDB carStatsDB;
    [SerializeField]
    private GameObject pauseButton;
    [SerializeField]
    private GameObject bankRobberyPopup;
    [SerializeField]
    private CheckpointsManager checkpointsManager;
    [SerializeField]
    private WorldName worldName;
    [SerializeField]
    private WorldOrder worldOrder;
    [SerializeField]
    private AudioListener audioListener;
    [SerializeField]
    private WorldPopups worldPopups;
    [SerializeField]
    private Camera renderTextureCamera;
    [SerializeField]
    private LeaderboardFix leaderboadFix;
    [SerializeField]
    private ActiveCameraHolder activeCameraHolder;
    [SerializeField]
    private OtherGames otherGames;
    [SerializeField]
    private LevelModeController levelModeController;
    [SerializeField]
    private InAppPromoManager inAppPromoManager;
    [SerializeField]
    private InAppInfoDB inAppInfoDB;

    private CrimsonPlugins.AdAdapter adAdapter;
    private IAPManager iAPManager;


    new void Awake()
    {
        base.Awake();
        adAdapter = GameObject.FindObjectOfType<CrimsonPlugins.AdAdapter>();
        iAPManager = GameObject.FindObjectOfType<IAPManager>();
    }

    public Transform Player
    {
        get { return player; }
    }

    public Camera Camera
    {
        get { return mainCamera; }
    }

    public Transform Sun
    {
        get { return sun; }
    }

    public TrafficContainer TrafficContainer
    {
        get { return trafficContainer; }
    }

    public ContinueManager ContinueManager
    {
        get { return continueManager; }
    }

    public GameSceneManager GameSceneManager
    {
        get { return gameSceneManager; }
    }

    public DailyGiftManager DailyGiftManager
    {
        get { return dailyGiftManager; }
    }

    public BillboardSystem BillboardSystem
    {
        get { return billboardSystem; }
    }

    public CarStatsDB CarStatsDB
    {
        get { return carStatsDB; }
    }

    public GameObject PauseButton
    {
        get { return pauseButton; }
    }

    public GameObject BankRobberyPopup
    {
        get { return bankRobberyPopup; }
    }

    public CrimsonPlugins.AdAdapter AdAdapter
    {
        get { return adAdapter; }
    }

    public IAPManager IAPManager
    {
        get { return iAPManager; }
    }

    public CheckpointsManager CheckpointsManager
    {
        get { return checkpointsManager; }
    }

    public WorldName WorldName
    {
        get { return worldName; }
    }

    public WorldOrder WorldOrder
    {
        get { return worldOrder; }
    }

    public AudioListener AudioListener
    {
        get { return audioListener; }
    }

    public WorldPopups WorldPopups
    {
        get { return worldPopups; }
    }

    public Camera RenderTextureCamera
    {
        get { return renderTextureCamera; }
    }

    public LeaderboardFix LeaderboardFix
    {
        get { return leaderboadFix; }
    }

    public ActiveCameraHolder ActiveCameraHolder
    {
        get { return activeCameraHolder; }
    }

    public OtherGames OtherGames
    {
        get { return otherGames; }
    }

    public LevelModeController LevelModeController
    {
        get { return levelModeController; }
    }

    public InAppPromoManager InAppPromoManager
    {
        get { return inAppPromoManager; }
    }

    public InAppInfoDB InAppInfoDB
    {
        get { return inAppInfoDB; }
    }

}


