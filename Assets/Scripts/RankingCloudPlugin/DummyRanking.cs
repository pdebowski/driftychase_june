using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace CrimsonPlugins.Internal.RC
{

    public class DummyRanking : RankingCommandQueue, IRanking
    {

        public bool p_IsAuthenticated { get { return m_isAuthenticated; } }
        public bool p_WasLoggedIn
        {
            get { return PlayerPrefs.HasKey("__SocialLogged"); }
            set
            {
                if (value)
                    PlayerPrefs.SetInt("__SocialLogged", 1);
                else PlayerPrefs.DeleteKey("__SocialLogged");
            }
        }

        public string p_playerId { get { return "playerid1"; } }

        bool m_isAuthenticated = false;
        int m_reportScoreCounter = 0;
        Dictionary<RCLeaderboard, RankingData?> m_leaderboards;

#pragma warning disable 414
        List<LeaderboardCfg> m_leaderboardMapping;
        List<AchievementsCfg> m_achievementMapping;
        List<AchivementReveal> m_hiddenAchievements;
#pragma warning restore 414

        //-----------------------------------------------------------------
        public void Init(LeaderboardCfg[] leaderboardsCfg, AchievementsCfg[] achievementsCfg, AchivementReveal[] hiddenAchievements)
        {
#if !UNITY_WEBPLAYER
            m_leaderboardMapping = leaderboardsCfg.ToList();
            m_achievementMapping = achievementsCfg.ToList();
            m_hiddenAchievements = hiddenAchievements.ToList();

            m_leaderboards = new Dictionary<RCLeaderboard, RankingData?>();
            foreach (var l in leaderboardsCfg)
            {
                m_leaderboards.Add(l.leaderboardEnum, null);
            }

            //p_cubusRanking = p_desertumRanking = p_sphaeraRanking = null;
            //Workaround - facebook sdk need this - otherwise throw null exeption
#endif
        }
        //-------------------------------------------------------

        public void Login()
        {
            if (!p_WasLoggedIn)
                SendMessage("LoginShow", SendMessageOptions.RequireReceiver);
            Debug.Log("sdfsdkfjhsdjkfhskdjfhskdfjsdf");
            QueueMethod(CommandType.Login);
        }
        //----------------------------------------------------------
        public void ReportScore(RCLeaderboard leaderboard, long score)
        {
            if (p_IsAuthenticated)
            {
                m_reportScoreCounter++;
                if (IsInvoking("UpdateLeaderboardsScore"))
                {
                    CancelInvoke("UpdateLeaderboardsScore");
                }

                QueueMethod(CommandType.ReportScore, m_leaderboardMapping.First(v => v.leaderboardEnum == leaderboard).leaderboardId, score.ToString());
            }
        }
        //--------------------------------------------------------
        public void ShowLeaderboards()
        {
            if (p_IsAuthenticated)
            {
                SendMessage("LeaderboardShown", SendMessageOptions.RequireReceiver);
                QueueMethod(CommandType.ShowLeaderboards);
            }
        }
        //--------------------------------------------------------
        public void ShowLeaderboards(RCLeaderboard leaderboard)
        {
            if (p_IsAuthenticated)
            {
                SendMessage("LeaderboardShown", SendMessageOptions.RequireReceiver);
                QueueMethod(CommandType.ShowLeaderboards, m_leaderboardMapping.First(v => v.leaderboardEnum == leaderboard).leaderboardName);
            }
        }
        //---------------------------------------------------------------------
        public void UpdateRankings()
        {
            if (p_IsAuthenticated)
            {
                QueueMethod(CommandType.UpdateRankings);
            }
        }
        //------------------------------------------------------------
        public RankingData? GetRankingData(RCLeaderboard leaderboard)
        {
            return m_leaderboards[leaderboard];
        }
        //------------------------------------------------------------------
        public void ShowAchievements()
        {
            if (p_IsAuthenticated)
            {
                SendMessage("LeaderboardShown", SendMessageOptions.RequireReceiver);
                GooglePlayManager.Instance.ShowAchievementsUI();
            }
        }
        //------------------------------------------------------------------
        public void AchievementIncrement(RCAchievement achievement, float progress)
        {
            if (p_IsAuthenticated)
            {
                if (progress > 0)
                {
                    string achievementId = m_achievementMapping.First(v => v.achievementEnum == achievement).achievementId;

                    GooglePlayManager.Instance.IncrementAchievementById(achievementId, System.Convert.ToInt32(progress*100f));

                }
            }
        }
        //--------------------------------------------------------
        public void AchievementUnlock(RCAchievement achievement)
        {
            if (p_IsAuthenticated)
            {
                string achievementId = m_achievementMapping.First(v => v.achievementEnum == achievement).achievementId;
                GooglePlayManager.Instance.UnlockAchievementById(achievementId);
            }
        }
        //---------------------------------------------------------
        public void AchievementReveal(RCAchievement achievement)
        {
            if (p_IsAuthenticated)
            {
                string achievementId = m_achievementMapping.First(v => v.achievementEnum == achievement).achievementId;
                GooglePlayManager.Instance.RevealAchievementById(achievementId);
            }
        }

        //-------------------------------------------------------------
        public void CheckAchievementNeedReveal()
        {
        }
        //-----------------------------------------------------------------
        protected override void QConnect()
        {
            if (!p_WasLoggedIn)
            {
                p_WasLoggedIn = true;
                m_isAuthenticated = true;
                SendMessage("LoginShow", SendMessageOptions.RequireReceiver);
                SendMessage("SynchronizeScore", true, SendMessageOptions.RequireReceiver);
                SendMessage("SynchronizeAchievement", true, SendMessageOptions.RequireReceiver);
                SendMessage("LoginStateChanged", true, SendMessageOptions.RequireReceiver);
            }
            else
            {
                p_WasLoggedIn = true;
                m_isAuthenticated = true;
                SendMessage("LoginStateChanged", true, SendMessageOptions.RequireReceiver);
                QueueMethod(CommandType.UpdateRankings);
            }

        }
        //------------------------------
        void Bridge_OnApplicationPause(bool paused)
        {
        }
        //-----------------------------------------------------------------
        protected override void QSubmitScore(string leaderboardId, long scores)
        {
            m_reportScoreCounter--;
            StartCoroutine(SubmitScore(leaderboardId, scores));

            if (m_reportScoreCounter == 0)
            {
                Invoke("UpdateLeaderboardsScore", 1f);
            }
        }
        //-----------------------------------------------------------------
        IEnumerator SubmitScore(string leaderboardId, long score)
        {
            yield return new WaitForSeconds(0.7f);

            foreach (var cfg in m_leaderboardMapping)
            {
                string _leaderboard = cfg.leaderboardId;
                if (leaderboardId.Equals(_leaderboard))
                {
                    RCLeaderboard leaderboardEnum = cfg.leaderboardEnum;
                    long oldScore = m_leaderboards[leaderboardEnum].HasValue ? m_leaderboards[leaderboardEnum].Value.m_score : -1;
                    long newScore = score > oldScore ? score : oldScore;
                    int rank = Random.Range(1, 10);
                    m_leaderboards[leaderboardEnum] = new RankingData(newScore, rank);
                    PlayerPrefs.SetInt("rank" + _leaderboard, rank);
                    PlayerPrefs.SetInt("score" + _leaderboard, (int)newScore);
                }
            }

        }
        //-----------------------------------------------------------------
        void UpdateLeaderboardsScore()
        {
            QueueMethod(CommandType.UpdateRankings);
        }
        //-----------------------------------------------------------------
        protected override void QShowLeaderboard(string leaderboardId)
        {
            SendMessage("LoginShow", SendMessageOptions.RequireReceiver);

        }
        //-----------------------------------------------------------------
        protected override void QShowLeaderboardsUI()
        {
            SendMessage("LoginShow", SendMessageOptions.RequireReceiver);

        }
        //-----------------------------------------------------------------
        protected override void QUpdateRankings()
        {
            Invoke("UpdateRank", 1.5f);
        }
        //--------------------------------------------------------------------
        void UpdateRank()
        {
            foreach (var cfg in m_leaderboardMapping)
            {
                string _leaderboard = cfg.leaderboardId;

                if (PlayerPrefs.HasKey("rank" + _leaderboard))
                {
                    RCLeaderboard leaderboardEnum = cfg.leaderboardEnum;
                    m_leaderboards[leaderboardEnum] = new RankingData((long)PlayerPrefs.GetInt("score" + _leaderboard, 0), PlayerPrefs.GetInt("rank" + _leaderboard, 0));
                }
            }

            SendMessage("PlayerScoreUpdated", SendMessageOptions.RequireReceiver);

        }
        //------------------------------------------------------------------
        protected override void QShowAchievements()
        {
            SendMessage("LoginShow", SendMessageOptions.RequireReceiver);

        }
        //----------------------------------------------
        protected override void QUpdateRanking(string leaderboardId)
        {
            if (!IsInvoking("UpdateRank"))
            {
                Invoke("UpdateRank", 1.5f);
            }
        }
        //----------------------------------------------
        protected override bool IsApiConnected()
        {
            return p_IsAuthenticated;
        }
        //----------------------------------------------
        protected override void QAchievementIncrement(string achivementId, int numSteps)
        {

        }
        //----------------------------------------------
        protected override void QAchievementUnlock(string achivementId)
        {

        }
        //----------------------------------------------
        protected override void QAchievementReveal(string achivementId)
        {

        }
        //----------------------------------------------
        protected override void QLoadAchievements()
        {

        }
        //-----------------------------------------------------------------

    }
}
