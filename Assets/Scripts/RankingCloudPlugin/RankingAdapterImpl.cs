using UnityEngine;
using System.Collections;


//!!!!!!!!!!!!!!DO NOT MODIFY THIS FILE!!!!!!!!!!!!!!!

namespace CrimsonPlugins
{
    public partial class RankingCloudAdapter : Singleton<RankingCloudAdapter>
    {
        public static System.Action OnPlayerScoreUpdated;
        public static System.Action OnLoginActivityShow;
        public static System.Action OnGCActivityWillShow;
        public static System.Action<bool> OnAuthenticationChanged;
        public static System.Action<bool> OnScoreNeedSync;
        public static System.Action<bool> OnAchievementNeedSync;

        public CrimsonPlugins.Internal.RC.IRanking p_rankingInstance { get; set; }


        public CrimsonPlugins.Internal.RC.LeaderboardCfg[] m_ANDROIDLeaderboardCfg;
        public CrimsonPlugins.Internal.RC.LeaderboardCfg[] m_IOSLeaderboardCfg;

        public CrimsonPlugins.Internal.RC.AchievementsCfg[] m_ANDROIDAchievementsCfg;
        public CrimsonPlugins.Internal.RC.AchievementsCfg[] m_IOSAchievementsCfg;

        public CrimsonPlugins.Internal.RC.AchivementReveal[] m_hiddenAchievements;


        //--CALLBACK---------------------------------------------------------
        void LoginStateChanged(bool isLoggedIn)
        {
            if (OnAuthenticationChanged != null)
                OnAuthenticationChanged(isLoggedIn);
        }
        //--CALLBACK---------------------------------------------------------
        void LoginShow()
        {
            if (OnLoginActivityShow != null)
                OnLoginActivityShow();
        }
        //--CALLBACK---------------------------------------------------------
        void LeaderboardShown()
        {
            if (OnGCActivityWillShow != null)
                OnGCActivityWillShow();
        }
        //---CALLBACK-------------------------------------------------
        void PlayerScoreUpdated()
        {
            if (OnPlayerScoreUpdated != null)
            {
                OnPlayerScoreUpdated();
            }
        }
        //-----------------------------------------------------
        void OnAchievementsUpdated()
        {
            p_rankingInstance.CheckAchievementNeedReveal();
        }
        //---CALLBACK-------------------------------------------------
        void SynchronizeScore(bool forceUpdate)
        {
            if (OnScoreNeedSync != null)
            {
                OnScoreNeedSync(forceUpdate);
            }
        }
        //---------------------------------------------------------------------
        void SynchronizeAchievement(bool forceUpdate)
        {
            if (OnAchievementNeedSync != null)
            {
                OnAchievementNeedSync(forceUpdate);
            }
        }
        //----------------------------------------------------------------------
    }
}
