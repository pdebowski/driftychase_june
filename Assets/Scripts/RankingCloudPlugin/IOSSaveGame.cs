using UnityEngine;
using System.Collections;

//!!!!!!!!!!!!!!DO NOT MODIFY THIS FILE!!!!!!!!!!!!!!!

namespace CrimsonPlugins.Internal.RC
{

#if UNITY_IOS
public class IOSSaveGame : CloudCommandQueue,ISavedGames {

	public bool m_authenticated = false;
	public bool p_IsAuthenticated { get {return m_authenticated;} }

	private const string c_currentSaveName = "SaveGame";

	// Use this for initialization
	void OnEnable () {
		iCloudManager.OnCloudDataReceivedAction += OnCloudDataReceived;
		iCloudManager.OnCloudInitAction += OnCloudInit;
		iCloudManager.OnStoreDidChangeExternally += OnStoreDidChangeExternally;

	}
	
	void OnDisable () 
	{
		iCloudManager.OnCloudDataReceivedAction -= OnCloudDataReceived;
		iCloudManager.OnCloudInitAction -= OnCloudInit;
		iCloudManager.OnStoreDidChangeExternally -= OnStoreDidChangeExternally;
	}


	void OnCloudDataReceived(iCloudData data)
	{
		Debug.Log("OnCloudDataReceived");

        if (data.key.Equals(c_currentSaveName))
        {
            if (!data.IsEmpty)
            {
                try
                {
                    string decodedValue = null;
                    decodedValue = RankingCloudHelpers.Base64Decode(data.stringValue);
                    Debug.Log("Cloud: Loaded data " + decodedValue);

                    SendMessage("SavedGameSuccessfullyLoaded",decodedValue , SendMessageOptions.RequireReceiver);
                }
                catch (System.Exception e)
                {
                    Debug.LogWarning("Cloud decode Exception" + e.Message);
                    SendMessage("SavedGameLoadFailed", SendMessageOptions.RequireReceiver);
                }
            }
            else
            {
                Debug.LogWarning("Cloud: data is corrupted " + data.IsEmpty);
                if (!data.IsEmpty)
                {
                    Debug.LogWarning("Bad key : " + data.key);
                }

                SendMessage("SavedGameLoadFailed", SendMessageOptions.RequireReceiver);
            }
        }
            UnblockCalls();
	}
	//-------------------------------------------------------
	void OnCloudInit(SA.Common.Models.Result result)
	{
		Debug.Log("OnCloudInit " + result.IsSucceeded);

		m_authenticated = result.IsSucceeded;

		Debug.Log ("Cloud: init " + result.IsSucceeded);
		if (result.IsFailed)
		{
			Debug.LogWarning(result.Error);
		}

		SendMessage("CloudInterfaceInitialized", m_authenticated, SendMessageOptions.RequireReceiver);
	}
	//------------------------------------------------------
	void OnStoreDidChangeExternally(System.Collections.Generic.List<iCloudData> changedData)
	{
		Debug.Log("OnStoreDidChange");
		foreach (iCloudData data in changedData)
		{
			Debug.Log("Cloud data with key:  " + data.key + " was changed, new val: " + data.stringValue);
            if (data.key.Equals(c_currentSaveName))
            {
                try
                {
                    string decodedValue = RankingCloudHelpers.Base64Decode(data.stringValue);
	        		Debug.Log("Cloud data with key:  " + data.key + " was changed, Decoded: " + decodedValue);

                    SendMessage("IOSCloudSaveExternallyUpdated", decodedValue, SendMessageOptions.RequireReceiver);
                }
                catch (System.Exception e)
                {
                    Debug.LogWarning("Parse error " + e.Message);
                }
            }
		}
	}
	//------------------------------------------------------
	//-----------------------------------------------------
		//---------------------------------------------------------
	public void InternalInit()
	{
		iCloudManager.Instance.Init ();	
	}
	//------------------------------------------------
	public void StartLoadingSavedGames()
	{
		QueueMethod(CommandType.StartLoadingSavedGames);
	}
	//----------------------------------------------------------
	public void SaveGame(string data, ConflictResolver conflictResolveDelegate)
	{
		QueueMethod(CommandType.SaveGame, data);
	}
	//----------------------------------------------------------
	//-------------------------------------------------------
	protected override bool IsApiConnected ()
	{
		return p_IsAuthenticated;
	}
	//----------------------------------------------------------
	protected override void QStartLoadingSavedGames()
	{
		BlockCalls();
		iCloudManager.Instance.requestDataForKey (c_currentSaveName);
	}
	//----------------------------------------------------------
	protected override void QSaveGame(string data)
	{
		Debug.Log ("Saved data " + data);
		string encodedString = null;

		try
		{
			encodedString= RankingCloudHelpers.Base64Encode(data);
			iCloudManager.Instance.setString(c_currentSaveName,encodedString);
		} 
		catch (System.Exception e)
		{
			Debug.LogWarning("Cloud encode exception"+ e.Message);
		}

	}
	//---------------------------------------------------------------


}
#endif
}
