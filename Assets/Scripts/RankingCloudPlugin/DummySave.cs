using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;


//!!!!!!!!!!!!!!DO NOT MODIFY THIS FILE!!!!!!!!!!!!!!!


namespace CrimsonPlugins.Internal.RC
{
    public class DummySave : CloudCommandQueue, ISavedGames
    {
        public bool p_IsAuthenticated { get { return false; } }
        public string p_loadedGamestate { get; set; }

        public void InternalInit()
        {
        }

        // Use this for initialization
        public void StartLoadingSavedGames()
        {
            Debug.Log("CLOUD: StartLoading");
            RefundsManager.refundNeedCheck = true;
        }

        public void SaveGame(string saveName, ConflictResolver res)
        {
            Debug.Log("Cloud Save Game");
        }

        protected override bool IsApiConnected()
        {
            return p_IsAuthenticated;
        }

        protected override void QSaveGame(string saveData)
        {
        }

        protected override void QStartLoadingSavedGames()
        {
        }

    }
}
