using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.SocialPlatforms.GameCenter;
using UnityEngine.SocialPlatforms;

//!!!!!!!!!!!!!!DO NOT MODIFY THIS FILE!!!!!!!!!!!!!!!

namespace CrimsonPlugins.Internal.RC
{

    public class IOSGameCenter : MonoBehaviour, IRanking
    {

        public bool p_IsAuthenticated { get { return Social.localUser.authenticated; } }
        public bool p_WasLoggedIn
        {
            get { return PlayerPrefs.HasKey("__SocialLogged"); }
            set
            {
                if (value)
                    PlayerPrefs.SetInt("__SocialLogged", 1);
                else PlayerPrefs.DeleteKey("__SocialLogged");
            }
        }

        public string p_playerId
        {
            get
            {
                return Social.localUser.id;
            }
        }


        IAchievementDescription[] m_achievementsArr = null;
        IAchievement[] m_achievementsArrRevealed = null;

        Dictionary<RCLeaderboard, RankingData?> m_leaderboards;

        List<LeaderboardCfg> m_leaderboardMapping;
        List<AchievementsCfg> m_achievementMapping;
        List<AchivementReveal> m_hiddenAchievements;

        float m_timeLatchLeaderboardUpdate = -100;

        bool m_mustUpdateLeaderboardsAfterLogin = true;

#pragma warning disable 414
        bool m_needReconnect = false;
        Coroutine loadAchievements = null;
#pragma warning restore 414

        //----------------------------------------------------------
        public void Init(LeaderboardCfg[] leaderboardsCfg, AchievementsCfg[] achievementsCfg, AchivementReveal[] hiddenAchievements)
        {
            m_leaderboardMapping = leaderboardsCfg.ToList();
            m_achievementMapping = achievementsCfg.ToList();
            m_hiddenAchievements = hiddenAchievements.ToList();

            m_leaderboards = new Dictionary<RCLeaderboard, RankingData?>();
            foreach (var l in leaderboardsCfg)
            {
                m_leaderboards.Add(l.leaderboardEnum, null);
            }
#if UNITY_IOS || UNITY_TVOS
        GameCenterPlatform.ShowDefaultAchievementCompletionBanner(true);
#endif

        }
        //-------------------------------------------------------------
        public void Login()
        {
            if (!p_WasLoggedIn)
            {
                SendMessage("LoginShow", SendMessageOptions.RequireReceiver);
            }
            Social.localUser.Authenticate(success => GameCenterAuthentication(success));
        }
        //---------------------------------------------------------------
        public RankingData? GetRankingData(RCLeaderboard leaderboard)
        {
            return m_leaderboards[leaderboard];
        }
        //----------------------------------------------------------
        public void ShowAchievements()
        {
            SendMessage("LeaderboardShown", SendMessageOptions.RequireReceiver);
            Social.ShowAchievementsUI();
        }
        //----------------------------------------------------------

        public void ReportScore(RCLeaderboard leaderboard, long score)
        {
            if (p_IsAuthenticated)
            {
                Social.ReportScore(score, m_leaderboardMapping.First(v => v.leaderboardEnum == leaderboard).leaderboardName, (success => OnScoreSubmitted(success)));
            }
        }
        //--------------------------------------------------------
        public void ShowLeaderboards()
        {
            SendMessage("LeaderboardShown", SendMessageOptions.RequireReceiver);
            Social.ShowLeaderboardUI();
        }
        //--------------------------------------------------------
        public void ShowLeaderboards(RCLeaderboard leaderboard)
        {
            //TODO: Clean with send message
            SendMessage("LeaderboardShown", SendMessageOptions.RequireReceiver);
            GameCenterPlatform.ShowLeaderboardUI(m_leaderboardMapping.First(v => v.leaderboardEnum == leaderboard).leaderboardName, UnityEngine.SocialPlatforms.TimeScope.AllTime);
        }
        //--------------------------------------------------------
        public void UpdateRankings()
        {
            UpdateLeaderboardsScore();
        }
        //----------------------------------------------------------
        public void AchievementIncrement(RCAchievement achievement, float progress)
        {
            if (p_IsAuthenticated && !IsAchievementHidden(achievement) && !IsAchievementEarned(achievement))
            {
                if (progress > 0)
                {
                    string achievementId = m_achievementMapping.First(v => v.achievementEnum == achievement).achievementId;
                    //m_achievementsArr;
                    Social.ReportProgress(achievementId, progress * 100, (success) => { 
						Debug.Log("Achievement incremented status: " + success); 
						LoadAchievements();
					});
                }
            }

        }
        //----------------------------------------------------------
        public void AchievementUnlock(RCAchievement achievement)
        {
			Debug.Log ("Unlocking1 " + p_IsAuthenticated + ":" + !IsAchievementEarned (achievement) + ":" + !IsAchievementHidden (achievement));
			if (p_IsAuthenticated && !IsAchievementEarned(achievement) && !IsAchievementHidden(achievement))
            {
                string achievementId = m_achievementMapping.First(v => v.achievementEnum == achievement).achievementId;

				Debug.Log ("Unlockin2" + !IsAchievementEarned (achievement) + ":" + !IsAchievementHidden (achievement));
                Social.ReportProgress(achievementId, 100, (success) =>
                {
                      Debug.Log("Achievement unlocked status: " + success);
						LoadAchievements();
                });
            }
        }
        //----------------------------------------------------------
        public void AchievementReveal(RCAchievement achievement)
        {
			if (p_IsAuthenticated && IsAchievementHidden(achievement))
            {
                string achievementId = m_achievementMapping.First(v => v.achievementEnum == achievement).achievementId;

                        Social.ReportProgress(achievementId, 0, (success) => { 
							Debug.Log("Achievement revealing status: " +achievementId + " :"  + success); 
							LoadAchievements();
						});
            }
        }
        //--------------------------------------------------------
      /*  IAchievementDescription GetAchievement(RCAchievement achievementName)
        {
			string achievementId = m_achievementMapping.First (v => v.achievementEnum == achievementName).achievementId;
            foreach (var achievement in m_achievementsArr ?? new IAchievementDescription[0])
            {
				if (achievement.id.Equals(achievementId)) return achievement;
            }

            Debug.LogWarning("RETURN NULL FOR ACHIEVEMENT :" + achievementName);

            return null;
        }
        */
        //-------------------------------------------------------
        bool IsAchievementEarned(RCAchievement achievement)
        {
            string achievementId = m_achievementMapping.First(v => v.achievementEnum == achievement).achievementId;

			foreach (var ach in m_achievementsArrRevealed ?? new IAchievement[0])
            {
				if (ach.id.Equals (achievementId)) 
				{
					return ach.completed;
				}
            }

            return false;
        }
        //--------------------------------------------------------
        bool IsAchievementHidden(RCAchievement achievement)
        {
            string achievementId = m_achievementMapping.First(v => v.achievementEnum == achievement).achievementId;

			foreach (var ach in m_achievementsArrRevealed ?? new IAchievement[0])
            {
                if (ach.id.Equals(achievementId)) return ach.hidden;
            }
            // if not achievement in array means it hidden
            return true;
        }
        //--------------------------------------------------------
        void LoadAchievements()
        {
            if (loadAchievements != null)
            {
                StopCoroutine(loadAchievements);
            }
            loadAchievements = StartCoroutine(AchievementsLoader());
        }
        //--------------------------------------------------------
        IEnumerator AchievementsLoader()
        {
            yield return new WaitForSeconds(1);
            Debug.Log("LOADING ACHIEVEMENTS ______________________________");

            Social.LoadAchievementDescriptions(descriptions =>
            {
                if (descriptions.Length > 0)
                {
                    Debug.Log("Description loaded + " + descriptions.Length);

					m_achievementsArr = descriptions;
					/*	foreach (var v in m_achievementsArr){
							Debug.Log("Aaaa " +v.id + " : " +v.hidden + " : ");
						}
						*/

                    SendMessage("OnAchievementsUpdated", SendMessageOptions.RequireReceiver);
                }
                else
                {
                    Debug.Log("Failed to load achievement descriptions");
                    m_achievementsArr = null;
                }
            });


            Social.LoadAchievements(achievements =>
            {
                if (achievements.Length > 0)
                {
						m_achievementsArrRevealed = achievements;
                    Debug.Log("Load achievements revealed " + achievements.Length);
						foreach (var v in m_achievementsArrRevealed){
							Debug.Log("Aaaa " +v.id + " : " +v.hidden + " : " + v.completed);
						}
                }
                else
                    Debug.Log("No achievements returned");
            });

            loadAchievements = null;
        }

        //----------------------------------------------------------
        void GameCenterAuthentication(bool status)
        {
            if (status)
            {
                Debug.Log("AUTHENTICATION SUCCESS");

                if (!p_WasLoggedIn)
                {
                    p_WasLoggedIn = true;
                    SendMessage("SynchronizeScore", true, SendMessageOptions.RequireReceiver);
                    SendMessage("SynchronizeAchievement", true, SendMessageOptions.RequireReceiver);
                    m_mustUpdateLeaderboardsAfterLogin = false;
                    m_timeLatchLeaderboardUpdate = -100;
                }

                SendMessage("LoginStateChanged", true, SendMessageOptions.RequireReceiver);
                //refresh leaderboards only after first login
                if (m_mustUpdateLeaderboardsAfterLogin)
                    UpdateLeaderboardsScore();
                m_mustUpdateLeaderboardsAfterLogin = false;

#if !NO_ACHIEVEMENTS
                LoadAchievements();
#endif
            }
            else
            { // NOT AUTHENTICATED
                m_mustUpdateLeaderboardsAfterLogin = true;
                //		m_isAuthenticated = false;
                p_WasLoggedIn = false;
                Debug.Log("AUTHENTICATION FAILED");
                SendMessage("LoginStateChanged", false, SendMessageOptions.RequireReceiver);
            }
        }
        //-------------------------------------------------------------
        void UpdateLeaderboardsScore()
        {
			/*
            if (Time.time > m_timeLatchLeaderboardUpdate + 2)
            {
                m_timeLatchLeaderboardUpdate = Time.time;

                LoadLeaderboard(m_leaderboardMapping[0].leaderboardId);
            }
            */

        }
        //----------------------------------------------------------
        void LoadLeaderboard(string leaderboardId)
        {
            Debug.Log("A11");

            UnityEngine.SocialPlatforms.ILeaderboard bestScoreLeaderboard = Social.CreateLeaderboard();
            bestScoreLeaderboard.id = leaderboardId;
            bestScoreLeaderboard.SetUserFilter(new string[] { Social.localUser.id });
            bestScoreLeaderboard.timeScope = UnityEngine.SocialPlatforms.TimeScope.AllTime;
            bestScoreLeaderboard.userScope = UnityEngine.SocialPlatforms.UserScope.Global;
            bestScoreLeaderboard.LoadScores(success => OnPlayerScoreUpdated(success, bestScoreLeaderboard));
        }
        //----------------------------------------------------------
        void OnPlayerScoreUpdated(bool success, UnityEngine.SocialPlatforms.ILeaderboard leaderboard)
        {

            //bool isfriendScore = leaderboard.localUserScore.rank == -1;//if rank ==-1 try to friend, so friend score
            for (int i = 0; i < m_leaderboardMapping.Count; i++)
            {
                Debug.Log("A22" + leaderboard.id + " ::" + m_leaderboardMapping[i].leaderboardId);

                if (leaderboard.id.Equals(m_leaderboardMapping[i].leaderboardId))
                {
                    if (leaderboard.localUserScore.rank != -1)
                    {
                        m_leaderboards[m_leaderboardMapping[i].leaderboardEnum] = new RankingData(leaderboard.localUserScore.value, leaderboard.localUserScore.rank);
                        if (IsInvoking("ScoreUpdatedNotify"))
                        {
                            CancelInvoke("ScoreUpdatedNotify");
                        }
                        Debug.Log("Updated " + leaderboard.localUserScore.value + " :: " + leaderboard.localUserScore.rank);
                        Invoke("ScoreUpdatedNotify", 0.8f);

                    }

                    if (i + 1 < m_leaderboardMapping.Count)
                    {
                        LoadLeaderboard(m_leaderboardMapping[i + 1].leaderboardId);
                        break;
                    }
                }
            }

        }
        //----------------------------------------------------------
        void OnScoreSubmitted(bool success)
        {
            if (success)
            {
                Invoke("UpdateLeaderboardsScore", 1);
            }
            Debug.Log("OnScoreSubmited:" + success);
        }
        //----------------------------------------------------------
        void ScoreUpdatedNotify()
        {
            Debug.Log("OnScoreNotify");
            //ResolveLeaderboardType();
            SendMessage("PlayerScoreUpdated", SendMessageOptions.RequireReceiver);
        }
        //--------------------------------------------------------------

        /*
    void ResolveLeaderboardType()
	{
		if (p_cubusRanking.HasValue && p_desertumRanking.HasValue && p_sphaeraRanking.HasValue)
		{
			//global score loaded and not obtained ranks - rerequest score but for friends
			if (p_cubusRanking.Value.m_rank == -1 && p_desertumRanking.Value.m_rank == -1 && p_sphaeraRanking.Value.m_rank == -1 && !m_isFriendScore)
			{
				m_isFriendScore = true;
				m_timeLatchLeaderboardUpdate = -100;
				UpdateLeaderboardsScore();
			}//player perhaps havent raported the scores yet
			else if (p_cubusRanking.Value.m_rank == -1 && p_desertumRanking.Value.m_rank == -1 && p_sphaeraRanking.Value.m_rank == -1 && m_isFriendScore)
			{
				m_isFriendScore = false;
			}    
			//
			//user start to getting data from global leaderboard when previous cant do that
			else if (p_cubusRanking.Value.m_rank != -1 && !p_cubusRanking.Value.m_isFriendScore
			         && p_desertumRanking.Value.m_rank != -1 && !p_desertumRanking.Value.m_isFriendScore
			         && p_sphaeraRanking.Value.m_rank != -1 && !p_sphaeraRanking.Value.m_isFriendScore && m_isFriendScore)
			{
				SendMessage("SynchronizeScore",false, SendMessageOptions.RequireReceiver);
				m_isFriendScore = false;
			} 
		} 
        
	} * */
        //--------------------------------------------------------------------
        public void CheckAchievementNeedReveal()
        {

            if (m_achievementsArr == null)
            {
                return;
            }

            foreach (var ach in m_hiddenAchievements)
            {
                if (IsAchievementEarned(ach.achivementTrigger) && IsAchievementHidden(ach.achivementToReveal))
                {
                    AchievementReveal(ach.achivementToReveal);
                }
            }

        }
    }
}
