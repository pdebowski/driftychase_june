using UnityEngine;
using System.Collections.Generic;
using CrimsonPlugins.Internal.RC;

//!!!!!!!!!!!!!!DO NOT MODIFY THIS FILE!!!!!!!!!!!!!!!
//!!!!!!!!!!!!!!DO NOT MODIFY THIS FILE!!!!!!!!!!!!!!!


//Ranking cloud Version
//1.1 - callbacks fix, code more readable, cloud save prevent to call two method in same time, cloud queue

//!!!!!!!!!!!!!!DO NOT MODIFY THIS FILE!!!!!!!!!!!!!!!


/*
           
(1)  <Login> --(firstLogin)(OnGCActivityWillShow) -- (firstLogin)(OnScoreNeedSync) --  
               -- (firstLogin)(OnAchievementNeedSync) -- (OnAuthenticationChanged) -- (!firstLogin - if sucess)(OnPlayerScoreUpdated)   
  
(2)  <ReportScore> -- (OnPlayerScoreUpdated)   [[Invoke once after all ReportScore]]
 
(1)  <UpdateRankings> -- (OnPlayerScoreUpdated)
 
(1)  <ShowLeaderboards> -- (OnGCActivityWillShow)
  
(1)  <ShowAchievement> -- (OnGCActivityWillShow)
 
 */


/*
[Automatic](OnCloudInterfaceInitialized) - when cloud is initialized. On Android it would happen on login to google play callback

<StartLoadingSavedGames> -- (OnSavedGameSuccessfullyLoaded) or (OnSavedGameLoadFailed)

<SaveGame> - (OnSavingGameFinished)

*/


namespace CrimsonPlugins
{

    public partial class RankingCloudAdapter : Singleton<RankingCloudAdapter>
    {

        //-------------------------------------------------
        new void Awake()
        {
            base.Awake();
#if UNITY_EDITOR
            p_rankingInstance = this.gameObject.AddComponent<DummyRanking>() as IRanking;
            p_saveGameInstance = this.gameObject.AddComponent<DummySave>() as ISavedGames;
#elif UNITY_IOS || UNITY_TVOS
		    p_rankingInstance = this.gameObject.AddComponent<IOSGameCenter>() as IRanking;
		    p_saveGameInstance = this.gameObject.AddComponent<IOSSaveGame>() as ISavedGames;
#elif UNITY_ANDROID
#pragma warning disable 414, 0219
            GooglePlayConnection bugFix = GooglePlayConnection.Instance;
#pragma warning restore 414, 0219

            p_rankingInstance = this.gameObject.AddComponent<GooglePlay>() as IRanking;
            p_saveGameInstance = this.gameObject.AddComponent<GoogleSaveGame>() as ISavedGames;
#else
            p_rankingInstance = this.gameObject.AddComponent<DummyRanking>() as IRanking;
#endif
        }

        //----------------------------------------------------
        void Start()
        {
#if UNITY_IOS || UNITY_TVOS
            p_rankingInstance.Init(m_IOSLeaderboardCfg,m_IOSAchievementsCfg,m_hiddenAchievements);
            p_saveGameInstance.InternalInit();
#else
            p_rankingInstance.Init(m_ANDROIDLeaderboardCfg, m_ANDROIDAchievementsCfg, m_hiddenAchievements);
#endif
        }

    }
}
