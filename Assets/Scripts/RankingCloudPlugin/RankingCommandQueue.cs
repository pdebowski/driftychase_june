using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace CrimsonPlugins.Internal.RC
{
    public abstract class RankingCommandQueue : MonoBehaviour
    {
        const int MAX_QUOTES_PER_SEC = 5;
        const float REUSE_QUOTE_AFTER = 1.2f;

        protected enum CommandType { Login, ReportScore, UpdateRankings, ShowLeaderboards, ShowAchievements, AchievementIncrement, AchievementUnlock, AchievementReveal, LoadAchievements, UpdateRanking }

        struct MethodInfo
        {
            public CommandType m_type;
            public List<string> m_params;
        }

        struct MethodInvokedInfo
        {
            public CommandType m_type;
            public float m_invokeTime;
        }


        readonly Dictionary<CommandType, int> m_quotesCost = new Dictionary<CommandType, int>()
    {
        {CommandType.Login, 1},
        {CommandType.ReportScore, 2},
        {CommandType.UpdateRankings, 1},
        {CommandType.ShowLeaderboards, 1},
        {CommandType.ShowAchievements,1},
        {CommandType.AchievementIncrement,2},
        {CommandType.AchievementUnlock,2},
        {CommandType.AchievementReveal,2},
        {CommandType.LoadAchievements,1},
        {CommandType.UpdateRanking,1 }
    };

        Queue<MethodInfo> m_methodQueued;
        Queue<MethodInvokedInfo> m_invokedInfo;

        int m_usingQuotes = 0;

        //-------------------------------------------------
        void Awake()
        {
            m_methodQueued = new Queue<MethodInfo>();
            m_invokedInfo = new Queue<MethodInvokedInfo>();
            StartCoroutine(RankingUpdate());
        }
        //-------------------------------------------------
        IEnumerator RankingUpdate()
        {
            while (true)
            {
                CheckQuotes();

                if (m_methodQueued.Count > 0)
                {
                    CallMethodFromQueue();
                }

                yield return StartCoroutine(WaitForRealSeconds(0.1f));
            }
        }
        //----------------------------------------------
        IEnumerator WaitForRealSeconds(float sec)
        {
            float t = 0;
            while (t < 1f)
            {
                yield return 0;
                t += Time.unscaledDeltaTime / sec;
            }
        }
        //----------------------------------------------
        protected void OnEnable()
        {

        }
        //----------------------------------------------
        protected void OnDisable()
        {

        }
        //----------------------------------------------
        void CheckQuotes()
        {
            if (m_invokedInfo.Count > 0)
            {
                MethodInvokedInfo info = m_invokedInfo.Peek();
                if (Time.unscaledTime > info.m_invokeTime + REUSE_QUOTE_AFTER && (IsApiConnected() || info.m_type==CommandType.Login))
                {
                    m_invokedInfo.Dequeue();
                    m_usingQuotes -= m_quotesCost[info.m_type];
                }
            }
        }
        //----------------------------------------------
        void CallMethodFromQueue()
        {
            MethodInfo methodInfo = m_methodQueued.Peek();
            if (m_usingQuotes + m_quotesCost[methodInfo.m_type] < MAX_QUOTES_PER_SEC)
            {
                m_methodQueued.Dequeue();
                m_usingQuotes += m_quotesCost[methodInfo.m_type];
                m_invokedInfo.Enqueue(new MethodInvokedInfo { m_type = methodInfo.m_type, m_invokeTime = Time.unscaledTime });

                Debug.Log("Invoked " + methodInfo.m_type + " on time " + Time.unscaledTime);

                switch (methodInfo.m_type)
                {
                    case CommandType.Login:
                        QConnect();
                        break;
                    case CommandType.ReportScore:
                        QSubmitScore(methodInfo.m_params[0], long.Parse(methodInfo.m_params[1]));
                        break;
                    case CommandType.ShowLeaderboards:
                        if (methodInfo.m_params[0] == null)
                        {
                            QShowLeaderboardsUI();
                        }
                        else
                        {
                            QShowLeaderboard(methodInfo.m_params[0]);
                        }

                        break;
                    case CommandType.UpdateRankings:
                        QUpdateRankings();
                        break;
                    case CommandType.ShowAchievements:
                        QShowAchievements();
                        break;
                    case CommandType.AchievementIncrement:
                        QAchievementIncrement(methodInfo.m_params[0], int.Parse(methodInfo.m_params[1]));
                        break;
                    case CommandType.AchievementReveal:
                        QAchievementReveal(methodInfo.m_params[0]);
                        break;
                    case CommandType.AchievementUnlock:
                        QAchievementUnlock(methodInfo.m_params[0]);
                        break;
                    case CommandType.LoadAchievements:
                        QLoadAchievements();
                        break;
                    case CommandType.UpdateRanking:
                        QUpdateRanking(methodInfo.m_params[0]);
                        break;
                }
            }
        }
        //----------------------------------------------
        protected abstract void QConnect();
        //----------------------------------------------
        protected abstract void QSubmitScore(string leaderboardId, long scores);
        //----------------------------------------------
        protected abstract void QShowLeaderboard(string leaderboardId);
        //----------------------------------------------
        protected abstract void QShowLeaderboardsUI();
        //----------------------------------------------
        protected abstract void QUpdateRankings();
        //----------------------------------------------
        protected abstract void QUpdateRanking(string leaderboardId);
        //----------------------------------------------
        protected abstract void QShowAchievements();
        //----------------------------------------------
        protected abstract void QAchievementIncrement(string achievementId, int numSteps);
        //----------------------------------------------
        protected abstract void QAchievementUnlock(string achievementId);
        //----------------------------------------------
        protected abstract void QAchievementReveal(string achievementId);
        //----------------------------------------------
        protected abstract void QLoadAchievements();
        //----------------------------------------------
        protected abstract bool IsApiConnected(); 
        //----------------------------------------------


        protected void QueueMethod(CommandType command, string param1 = null, string param2 = null, string param3 = null)
        {
            m_methodQueued.Enqueue(new MethodInfo
            {
                m_type = command,
                m_params = new List<string>() { param1, param2, param3 }
            });
        }
        //----------------------------------------------


    }
}
