using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;

namespace CrimsonPlugins.Internal.RC
{
    public class GooglePlay : RankingCommandQueue, IRanking
    {

        public bool p_IsAuthenticated { get { return m_isAuthenticated && GooglePlayConnection.Instance.IsAPIConnectedCrimsonPine; } }
        public bool p_WasLoggedIn
        {
            get { return PlayerPrefs.HasKey("__SocialLogged"); }
            set
            {
                if (value)
                    PlayerPrefs.SetInt("__SocialLogged", 1);
                else PlayerPrefs.DeleteKey("__SocialLogged");
            }
        }

        public string p_playerId { get { return GooglePlayManager.Instance.player.playerId; } }

        Dictionary<RCLeaderboard, RankingData?> m_leaderboards;

        List<LeaderboardCfg> m_leaderboardMapping;
        List<AchievementsCfg> m_achievementMapping;
        List<AchivementReveal> m_hiddenAchievements;

        bool m_isAuthenticated = false;

        bool m_mustUpdateGooglePlayServicesAfterLogin = true;

        int m_reportScoreCounter = 0;


        //--------------------------------------------------------
        new void OnEnable()
        {
            base.OnEnable();
            GooglePlayConnection.ActionPlayerConnected += PlayAuthenticationSuccess;
            GooglePlayConnection.ActionPlayerDisconnected += AuthenticationFailedEvent;
            GooglePlayConnection.ActionConnectionStateChanged += OnConnectionStateChanged;
            GooglePlayManager.ActionScoreSubmited += OnScoreSubmitted;
            GooglePlayManager.ActionLeaderboardsLoaded += OnPlayerScoreUpdated;
            GooglePlayManager.ActionAchievementsLoaded += OnAchievementLoaded;
            GooglePlayManager.ActionScoresListLoaded += OnPlayerScoreUpdated;
        }
        //--------------------------------------------------------------
        new void OnDisable()
        {
            base.OnDisable();
            GooglePlayConnection.ActionPlayerConnected -= PlayAuthenticationSuccess;
            GooglePlayConnection.ActionPlayerDisconnected -= AuthenticationFailedEvent;
            GooglePlayConnection.ActionConnectionStateChanged -= OnConnectionStateChanged;
            GooglePlayManager.ActionScoreSubmited -= OnScoreSubmitted;
            GooglePlayManager.ActionLeaderboardsLoaded -= OnPlayerScoreUpdated;
            //  GooglePlayManager.ActionAchievementUpdated -= OnAchievementUpdated;
            GooglePlayManager.ActionAchievementsLoaded -= OnAchievementLoaded;
            GooglePlayManager.ActionScoresListLoaded -= OnPlayerScoreUpdated;
        }
        /*
        void Update()
        {
            Debug.Log("KConnect " + GooglePlayConnection.Instance.IsConnectedByKrzysiek);
        }
         */

        //--MEGA CRAP AND SMELLS LIKE SHIT HACK----------------------------
        /*   void Bridge_OnApplicationPause(bool paused)
           {
               GooglePlayConnection.Instance.Bridge_OnApplicationPause(paused);
           }
           */
        //------------------------------------------------------------
        void OnApplicationPause(bool paused)
        {
            if (!paused) Invoke("CheckIfNeedReconnect", 2.5f);
            else CancelInvoke("CheckIfNeedReconnect");
        }
        //----------------------------------------------------------
        public void Init(LeaderboardCfg[] leaderboardsCfg, AchievementsCfg[] achievementsCfg, AchivementReveal[] hiddenAchievements)
        {
#if !UNITY_WEBPLAYER
            m_leaderboardMapping = leaderboardsCfg.ToList();
            m_achievementMapping = achievementsCfg.ToList();
            m_hiddenAchievements = hiddenAchievements.ToList();

            m_leaderboards = new Dictionary<RCLeaderboard, RankingData?>();
            foreach (var l in leaderboardsCfg)
            {
                m_leaderboards.Add(l.leaderboardEnum, null);
            }
#endif
        }
        //------------------------------------------------------------
        void CheckIfNeedReconnect()
        {
            if (!GooglePlayConnection.Instance.IsAPIConnectedCrimsonPine && m_isAuthenticated)
                GooglePlayConnection.Instance.ConnectForce();
        }

        //------------------------------------------------------------
        public RankingData? GetRankingData(RCLeaderboard leaderboard)
        {
            return m_leaderboards[leaderboard];
        }

        //------------------------------------------------------------
        public void Login()
        {
            QueueMethod(CommandType.Login);
        }
        //----------------------------------------------------------
        public void ReportScore(RCLeaderboard leaderboard, long score)
        {
            if (p_IsAuthenticated)
            {
                //yep this must to be here
                m_reportScoreCounter++;
                if (IsInvoking("UpdateLeaderboardsScore"))
                {
                    CancelInvoke("UpdateLeaderboardsScore");
                }

                QueueMethod(CommandType.ReportScore, m_leaderboardMapping.First(v => v.leaderboardEnum == leaderboard).leaderboardId, score.ToString());
            }
        }
        //--------------------------------------------------------
        public void ShowLeaderboards()
        {
            QueueMethod(CommandType.ShowLeaderboards);
        }
        //--------------------------------------------------------
        public void ShowLeaderboards(RCLeaderboard leaderboard)
        {
            QueueMethod(CommandType.ShowLeaderboards, m_leaderboardMapping.First(v => v.leaderboardEnum == leaderboard).leaderboardId);
        }
        //---------------------------------------------------------------------
        public void UpdateRankings()
        {
            if (p_IsAuthenticated)
            {
                UpdateLeaderboardsScore();
            }
        }
        //------------------------------------------------------------------
        public void ShowAchievements()
        {
            QueueMethod(CommandType.ShowAchievements);
        }
        //-----------------------------------------------------------------
        protected override bool IsApiConnected()
        {
            return p_IsAuthenticated;
        }
        //--------------------------------------------------------
        protected override void QConnect()
        {
            if (!p_WasLoggedIn)
            {
                SendMessage("LoginShow", SendMessageOptions.RequireReceiver);
            }
            GooglePlayConnection.Instance.Connect();
        }
        //-----------------------------------------------------------------
        protected override void QSubmitScore(string leaderboardId, long scores)
        {
            GooglePlayManager.Instance.SubmitScoreById(leaderboardId, scores);
        }
        //-----------------------------------------------------------------
        protected override void QShowLeaderboard(string leaderboardId)
        {
            SendMessage("LeaderboardShown", SendMessageOptions.RequireReceiver);
            GooglePlayManager.Instance.ShowLeaderBoardById(leaderboardId);
        }
        //-----------------------------------------------------------------
        protected override void QShowLeaderboardsUI()
        {
            SendMessage("LeaderboardShown", SendMessageOptions.RequireReceiver);
            GooglePlayManager.Instance.ShowLeaderBoardsUI();
        }
        //-----------------------------------------------------------------
        protected override void QUpdateRankings()
        {
            Debug.LogError("Shouldn be called");
            //GooglePlayManager.Instance.();
        }
        //------------------------------------------------------------------
        protected override void QUpdateRanking(string leaderboardId)
        {
            GooglePlayManager.Instance.LoadPlayerCenteredScores(leaderboardId, GPBoardTimeSpan.ALL_TIME, GPCollectionType.GLOBAL, 1);
        }
        //------------------------------------------------------------------
        protected override void QShowAchievements()
        {
            SendMessage("LeaderboardShown", SendMessageOptions.RequireReceiver);
            GooglePlayManager.Instance.ShowAchievementsUI();
        }
        //----------------------------------------------
        protected override void QAchievementIncrement(string achievementId, int numSteps)
        {
            GooglePlayManager.Instance.IncrementAchievementById(achievementId, numSteps);
        }
        //----------------------------------------------
        protected override void QAchievementUnlock(string achievementId)
        {
            GooglePlayManager.Instance.UnlockAchievementById(achievementId);
        }
        //----------------------------------------------
        protected override void QAchievementReveal(string achievementId)
        {
            GooglePlayManager.Instance.RevealAchievementById(achievementId);

        }
        //----------------------------------------------
        protected override void QLoadAchievements()
        {
            GooglePlayManager.Instance.LoadAchievements();
        }
        //----------------------------------------------
        public void AchievementIncrement(RCAchievement achievement, float progress)
        {
            progress *= 100f;

            if (p_IsAuthenticated && !isHidden(achievement) && !isUnlocked(achievement))
            {
                if (System.Convert.ToInt32(progress) > 0)
                {
                    string achievementId = m_achievementMapping.First(v => v.achievementEnum == achievement).achievementId;

                    GPAchievement gpAchievement = GooglePlayManager.Instance.GetAchievement(achievementId);
                    if (gpAchievement != null)
                    {
                        if (gpAchievement.State == GPAchievementState.STATE_REVEALED)
                        {
                            QueueMethod(CommandType.AchievementIncrement, achievementId, System.Convert.ToInt32(progress).ToString());

                        }
                    }
                }
            }
        }
        //--------------------------------------------------------
        public void AchievementUnlock(RCAchievement achievement)
        {
            if (p_IsAuthenticated)
            {
                string achievementId = m_achievementMapping.First(v => v.achievementEnum == achievement).achievementId;
                GPAchievement gpAchievement = GooglePlayManager.Instance.GetAchievement(achievementId);
                if (gpAchievement != null)
                {
                    if (gpAchievement.State == GPAchievementState.STATE_REVEALED)
                    {
                        QueueMethod(CommandType.AchievementUnlock, achievementId);
                    }
                }
            }
        }
        //---------------------------------------------------------
        public void AchievementReveal(RCAchievement achievement)
        {
            if (p_IsAuthenticated)
            {
                string achievementId = m_achievementMapping.First(v => v.achievementEnum == achievement).achievementId;
                GPAchievement gpAchievement = GooglePlayManager.Instance.GetAchievement(achievementId);
                if (gpAchievement != null)
                {
                    if (gpAchievement.State == GPAchievementState.STATE_HIDDEN)
                    {
                        QueueMethod(CommandType.AchievementReveal, achievementId);

                    }
                }
            }
        }

        //-------------------------------------------------------------
        public void CheckAchievementNeedReveal()
        {
            if (GooglePlayManager.Instance == null || GooglePlayManager.Instance.Achievements == null || GooglePlayManager.Instance.Achievements.Count <= 0)
            {
                return;
            }

            foreach (var ach in m_hiddenAchievements)
            {
                if (isUnlocked(ach.achivementTrigger) && isHidden(ach.achivementToReveal))
                {
                    AchievementReveal(ach.achivementToReveal);
                }
            }

            //perhaps doesnt needed here ---
            //    SendMessage("SynchronizeAchievement", true, SendMessageOptions.RequireReceiver);
        }
        //-------------------------------------------------------------
        private bool isUnlocked(RCAchievement achievement)
        {
            string achievementId = m_achievementMapping.First(v => v.achievementEnum == achievement).achievementId;

            if (GooglePlayManager.Instance.GetAchievement(achievementId) == null)
            {
                return false;
            }
            else
            {
                return GooglePlayManager.Instance.GetAchievement(achievementId).State == GPAchievementState.STATE_UNLOCKED;
            }
        }
        //-----------------------------------------------------------------------
        bool isHidden(RCAchievement achievement)
        {
            string achievementId = m_achievementMapping.First(v => v.achievementEnum == achievement).achievementId;


            if (GooglePlayManager.Instance.GetAchievement(achievementId) == null)
            {
                return true;
            }
            else
            {
                return GooglePlayManager.Instance.GetAchievement(achievementId).State == GPAchievementState.STATE_HIDDEN;
            }
        }

        //----------------------------------------------------------
        void OnConnectionStateChanged(GPConnectionState gpGameState)
        {
#if !UNITY_WEBPLAYER
            Debug.Log("connection changed " + gpGameState);
            if (gpGameState == GPConnectionState.STATE_DISCONNECTED)
            {
                m_isAuthenticated = false;
            }
#endif
        }
        //----------------------------------------------------------
        void PlayAuthenticationSuccess()
        {
            m_isAuthenticated = true;
            Debug.Log("AUTHENTICATION SUCCESS");

            if (!p_WasLoggedIn)
            {
                p_WasLoggedIn = true;
                SendMessage("SynchronizeScore", true, SendMessageOptions.RequireReceiver);
                SendMessage("SynchronizeAchievement", true, SendMessageOptions.RequireReceiver);
                //update score after sync
                m_mustUpdateGooglePlayServicesAfterLogin = false;
            }

            SendMessage("LoginStateChanged", true, SendMessageOptions.RequireReceiver);
            //refresh leaderboards only after first login
            if (m_mustUpdateGooglePlayServicesAfterLogin)
            {
                UpdateLeaderboardsScore();
            }
#if !NO_ACHIEVEMENTS
            QueueMethod(CommandType.LoadAchievements);
#endif
            m_mustUpdateGooglePlayServicesAfterLogin = false;
        }
        //-------------------------------------------------------------
        void UpdateLeaderboardsScore()
        {
            /*       foreach (var leaderboard in m_leaderboardMapping)
                   {
                       QueueMethod(CommandType.UpdateRanking, leaderboard.leaderboardId);
                   }
                   */
        }

        //----------------------------------------------------------
        void AuthenticationFailedEvent()
        {
            m_mustUpdateGooglePlayServicesAfterLogin = true;
            m_isAuthenticated = false;
            p_WasLoggedIn = false;
            Debug.Log("AUTHENTICATION FAILED");
            SendMessage("LoginStateChanged", false, SendMessageOptions.RequireReceiver);
        }
        //----------------------------------------------------------
        void OnPlayerScoreUpdated(GooglePlayResult gpResult)
        {

            GPScore playerScore = null;
            foreach (var cfg in m_leaderboardMapping)
            {
                string leaderboardId = cfg.leaderboardId;
                RCLeaderboard leaderboardEnum = cfg.leaderboardEnum;

                if (GooglePlayManager.Instance.GetLeaderBoard(leaderboardId) != null &&
                    (playerScore = GooglePlayManager.Instance.GetLeaderBoard(leaderboardId).GetCurrentPlayerScore(GPBoardTimeSpan.ALL_TIME, GPCollectionType.GLOBAL)) != null)
                {
                    //bool isfriendScore = playerScore.Rank == -1;//if rank ==-1 try to friend, so friend score
                    if (playerScore.Rank != -1)
                    {
                        m_leaderboards[leaderboardEnum] = new RankingData(playerScore.LongScore, playerScore.Rank);
                    }
                    if (!IsInvoking("ScoreUpdatedNotify")) Invoke("ScoreUpdatedNotify", 1.5f);
                }
            }

        }

        //----------------------------------------------------------
        void OnScoreSubmitted(GP_LeaderboardResult gpResult)
        {
            m_reportScoreCounter--;

            if (m_reportScoreCounter == 0)
            {
                Invoke("UpdateLeaderboardsScore", 0.8f);
            }
        }

        //----------------------------------------------------------
        void ScoreUpdatedNotify()
        {
            //   ResolveLeaderboardType();
            SendMessage("PlayerScoreUpdated", SendMessageOptions.RequireReceiver);
        }
        //--------------------------------------------------------------
        void OnAchievementUpdated(GP_AchievementResult gpResult)
        {
            SendMessage("OnAchievementsUpdated", SendMessageOptions.RequireReceiver);
        }
        //----------------------------------------------------------------
        void OnAchievementLoaded(GooglePlayResult googlePlayResult)
        {
            SendMessage("CheckAchievementNeedReveal", SendMessageOptions.RequireReceiver);
        }
        //--------------------------------------------------------------------
    }
}
