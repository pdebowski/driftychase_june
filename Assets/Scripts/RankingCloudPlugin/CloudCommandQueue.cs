using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace CrimsonPlugins.Internal.RC
{
    public abstract class CloudCommandQueue : MonoBehaviour
    {
        protected enum CommandType { StartLoadingSavedGames, SaveGame }

        protected bool IsPendingCall { get; private set; }

        struct MethodInfo
        {
            public CommandType m_type;
            public List<string> m_params;
        }

        Queue<MethodInfo> m_methodQueued;

        //-------------------------------------------------
        void Awake()
        {
            m_methodQueued = new Queue<MethodInfo>();
            StartCoroutine(CloudUpdate());
        }
        //-------------------------------------------
        void OnApplicationPause(bool pauseStatus)
        {
            if (!pauseStatus && IsPendingCall)
            {
                Invoke("UnblockCalls",5);
            }
        }
        //-------------------------------------------------
        IEnumerator CloudUpdate()
        {
            while (true)
            {
                if (!IsPendingCall && m_methodQueued.Count > 0 && IsApiConnected())
                {
                    CallMethodFromQueue();
                }

                yield return StartCoroutine(WaitForRealSeconds(0.1f));
            }
        }
        //----------------------------------------------
        IEnumerator WaitForRealSeconds(float sec)
        {
            float t = 0;
            while (t < 1f)
            {
                yield return 0;
                t += Time.unscaledDeltaTime / sec;
            }
        }
        //--
        //----------------------------------------------
        void CallMethodFromQueue()
        {
                MethodInfo methodInfo = m_methodQueued.Dequeue();

                Debug.Log("Cloud Invoked " + methodInfo.m_type + " on time " + Time.unscaledTime);

                switch (methodInfo.m_type)
                {
                    case CommandType.StartLoadingSavedGames:
                            QStartLoadingSavedGames();
                        break;
                    case CommandType.SaveGame:
                            QSaveGame(methodInfo.m_params[0]);
                        break;
                }
        }
        //----------------------------------------------
        protected abstract void QStartLoadingSavedGames();
        //----------------------------------------------
        protected abstract void QSaveGame(string saveData);
        //----------------------------------------------
        protected abstract bool IsApiConnected();
        //----------------------------------------------
        protected void BlockCalls()
        {
            IsPendingCall = true;
        }
        //----------------------------------------------
        protected void UnblockCalls()
        {
            if (IsInvoking("UnblockCalls"))
            {
                CancelInvoke("UnblockCalls");
            }
            IsPendingCall = false;
        }
        //------------------------------------------------
        protected void QueueMethod(CommandType command, string param1 = null, string param2 = null, string param3 = null)
        {
            m_methodQueued.Enqueue(new MethodInfo
            {
                m_type = command,
                m_params = new List<string>() { param1, param2, param3 }
            });
        }
        //----------------------------------------------
    }
}
