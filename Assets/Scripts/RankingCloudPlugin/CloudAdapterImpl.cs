using UnityEngine;
using System.Collections;

//!!!!!!!!!!!!!!DO NOT MODIFY THIS FILE!!!!!!!!!!!!!!!

namespace CrimsonPlugins
{
    public partial class RankingCloudAdapter : Singleton<RankingCloudAdapter>
    {
        public static System.Action<bool> OnCloudInterfaceInitialized;
        public static System.Action<string> OnSavedGameSuccessfullyLoaded;
        public static System.Action OnSavedGameLoadFailed;
        public static System.Action<bool> OnSavingGameFinished;
        public static System.Action<string> OnIOSCloudSaveExternallyUpdated;
		bool firstLogin=true;

        public CrimsonPlugins.Internal.RC.ISavedGames p_saveGameInstance { get; set; }

        //---------------------------------------------------------------------
        void OnEnable()
        {
            OnAuthenticationChanged += GooglePlayLoggedInInternal;
        }
        //---------------------------------------------------------------------
        void OnDisable()
        {
            OnAuthenticationChanged -= GooglePlayLoggedInInternal;
        }
        //---------------------------------------------------------------------
        void GooglePlayLoggedInInternal(bool isLoggedIn)
        {
#if UNITY_ANDROID
			if (firstLogin && isLoggedIn)
            {
            	p_saveGameInstance.InternalInit();
				firstLogin=false;
			}
#endif
        }
        //---------------------------------------------------------------------
        void SavedGameLoadFailed()
        {
            if (OnSavedGameLoadFailed != null)
            {
                OnSavedGameLoadFailed();
            }
        }
        //---------------------------------------------------------------------
        void SavedGameSuccessfullyLoaded(string gamestate)
        {
            if (OnSavedGameSuccessfullyLoaded != null)
            {
                OnSavedGameSuccessfullyLoaded(gamestate);
            }
        }
        //-------------CALLBACK--------------------------------------------
        void IOSCloudSaveExternallyUpdated(string gamestate)
        {
            if (OnIOSCloudSaveExternallyUpdated != null)
            {
                OnIOSCloudSaveExternallyUpdated(gamestate);
            }
        }
        //-------------CALLBACK--------------------------------------------
        void CloudInterfaceInitialized(bool withSuccess)
        {
            if (OnCloudInterfaceInitialized != null)
            {
                OnCloudInterfaceInitialized(withSuccess);
            }
        }
        //----------------------------------------------------------------------
        void SavingGameFinished(bool isSuccess)
        {
            if (OnSavingGameFinished != null)
            {
                OnSavingGameFinished(isSuccess);
            }
        }
    }
}
