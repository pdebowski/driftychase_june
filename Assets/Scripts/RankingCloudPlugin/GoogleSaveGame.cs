using UnityEngine;
using System.Collections;
using System;

//!!!!!!!!!!!!!!DO NOT MODIFY THIS FILE!!!!!!!!!!!!!!!

namespace CrimsonPlugins.Internal.RC
{
    public class GoogleSaveGame : CloudCommandQueue, ISavedGames
    {


        public bool p_IsAuthenticated { get { return RankingCloudAdapter.Instance.p_rankingInstance.p_IsAuthenticated; } }

        private static string c_currentSaveName = "SaveGame";
        private ConflictResolver m_conflictResolver;
        //----------------------------------------------------------
        void OnEnable()
        {
            GooglePlaySavedGamesManager.ActionAvailableGameSavesLoaded += ActionAvailableGameSavesLoaded;
            GooglePlaySavedGamesManager.ActionGameSaveResult += ActionGameSaveResult;
            GooglePlaySavedGamesManager.ActionGameSaveLoaded += ActionGameSaveLoaded;
            GooglePlaySavedGamesManager.ActionConflict += ActionConflict;
        }
        //----------------------------------------------------------
        void OnDisable()
        {
            GooglePlaySavedGamesManager.ActionAvailableGameSavesLoaded += ActionAvailableGameSavesLoaded;
            GooglePlaySavedGamesManager.ActionGameSaveResult -= ActionGameSaveResult;
            GooglePlaySavedGamesManager.ActionGameSaveLoaded -= ActionGameSaveLoaded;
            GooglePlaySavedGamesManager.ActionConflict -= ActionConflict;
        }
        //---------------------------------------------------------
        public void InternalInit()
        {
            SendMessage("CloudInterfaceInitialized", true);
        }
        //----------------------------------------------------------
        public void StartLoadingSavedGames()
        {
            QueueMethod(CommandType.StartLoadingSavedGames);
        }
        //----------------------------------------------------------
        public void SaveGame(string data, ConflictResolver conflictResolveDelegate)
        {

            QueueMethod(CommandType.SaveGame, data);
            m_conflictResolver = conflictResolveDelegate;
        }
        //----------------------------------------------------------
        protected override bool IsApiConnected()
        {
            return p_IsAuthenticated;
        }
        //----------------------------------------------------------
        protected override void QSaveGame(string data)
        {

            BlockCalls();
            try
            {
                long TotalPlayedTime = 20000;
                string description = "Modified data at: " + System.DateTime.Now.ToString("MM/dd/yyyy H:mm:ss");
                string base64Data = RankingCloudHelpers.Base64Encode(data);

                GooglePlaySavedGamesManager.Instance.CreateNewSnapshot(c_currentSaveName, description, Texture2D.blackTexture,
                                                    base64Data, TotalPlayedTime);
            }
            catch (System.Exception e)
            {
                Debug.Log("Save game failed " + e.Message);
            }
        }
        //----------------------------------------------------------
        protected override void QStartLoadingSavedGames()
        {
            BlockCalls();
            GooglePlaySavedGamesManager.Instance.LoadAvailableSavedGames();
        }
        //----------------------------------------------------------
        //----------------------------------------------------------
        //----------------------------------------------------------
        //----------------------------------------------------------
        //-ACTIONS------------------------------------------------------
        //----------------------------------------------------------
        //----------------------------------------------------------
        private void ActionAvailableGameSavesLoaded(GooglePlayResult res)
        {
            if (GooglePlaySavedGamesManager.Instance.AvailableGameSaves.Count > 0)
            {
                GP_SnapshotMeta snapMeta = GooglePlaySavedGamesManager.Instance.AvailableGameSaves[0];
                GooglePlaySavedGamesManager.Instance.LoadSpanshotByName(snapMeta.Title);
            } else
            {
                UnblockCalls(); // unblock calls because no snapshot
            }
        }
        //----------------------------------------------------------
        private void ActionGameSaveLoaded(GP_SpanshotLoadResult result)
        {
            if (result.IsSucceeded)
            {
                try
                {
                    string gameState= RankingCloudHelpers.Base64Decode(result.Snapshot.stringData);
                    Debug.Log("Game state loaded: " + gameState);
                    SendMessage("SavedGameSuccessfullyLoaded", gameState, SendMessageOptions.RequireReceiver);
                }
                catch (System.Exception e)
                {
                    Debug.LogWarning("load game data corrupted " + e.Message);
                    SendMessage("SavedGameLoadFailed", SendMessageOptions.RequireReceiver);
                }
            }
            else
            {
                Debug.LogWarning("Cannot load gamestate " + result.Message);
                SendMessage("SavedGameLoadFailed", SendMessageOptions.RequireReceiver);
            }

            UnblockCalls();
        }
        //----------------------------------------------------------
        private void ActionGameSaveResult(GP_SpanshotLoadResult result)
        {
            if (result.IsSucceeded)
            {
                Debug.Log("Games Saved: " + result.Snapshot.meta.Title + " DATA :: " + result.Snapshot.stringData);

            }
            else
            {
                Debug.Log("Games Save Failed");
            }

            UnblockCalls();
            SendMessage("SavingGameFinished", result.IsSucceeded, SendMessageOptions.RequireReceiver);
        }
        //----------------------------------------------------------
        private void ActionConflict(GP_SnapshotConflict result)
        {

            Debug.Log("Conflict Detected: ");

            GP_Snapshot snapshot = result.Snapshot;
            GP_Snapshot conflictSnapshot = result.ConflictingSnapshot;

            // Resolve between conflicts by selecting the newest of the conflicting snapshots.
            GP_Snapshot newerSnapshot = snapshot;
            GP_Snapshot olderSnapshot = conflictSnapshot;

            string newerSnapshotDecodedData = null;
            string olderSnapshotDecodedData = null;


            if (snapshot.meta.LastModifiedTimestamp < conflictSnapshot.meta.LastModifiedTimestamp)
            {
                newerSnapshot = conflictSnapshot;
                olderSnapshot = snapshot;
            }


            try
            {
                newerSnapshotDecodedData = RankingCloudHelpers.Base64Decode(newerSnapshot.stringData);
            }
            catch (System.Exception e)
            {
                Debug.LogWarning("Conflict Error, cannot decode newer snapshot data " + e.Message);
            }

            try
            {
                olderSnapshotDecodedData = RankingCloudHelpers.Base64Decode(olderSnapshot.stringData);
            }
            catch (System.Exception e)
            {
                Debug.LogWarning("Conflict Error, cannot decode older snapshot data " + e.Message);
            }

            //cannot parse first argument


            if (m_conflictResolver != null && newerSnapshotDecodedData != null && olderSnapshotDecodedData != null)
            {
                newerSnapshot.stringData = m_conflictResolver(newerSnapshotDecodedData, olderSnapshotDecodedData);
            }
            else if (newerSnapshotDecodedData != null)
            {
                Debug.LogWarning("Resolving problem : conflict resolver == null ? " + (m_conflictResolver == null) + " older snapshot is null ? " + (olderSnapshotDecodedData == null));
                newerSnapshot.stringData = newerSnapshotDecodedData;
            }
            else if (olderSnapshotDecodedData != null)
            {
                Debug.LogWarning("Resolving problem : conflict resolver == null ? " + (m_conflictResolver == null) + " newer snapshot is null ? " + (newerSnapshotDecodedData == null));
                newerSnapshot.stringData = olderSnapshotDecodedData;
            }

            result.Resolve(newerSnapshot);
        }
        //----------------------------------------------------------
        //----------------------------------------------------------
    }
}
