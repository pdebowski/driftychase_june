using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//!!!!!!!!!!!!!!DO NOT MODIFY THIS FILE!!!!!!!!!!!!!!!


namespace CrimsonPlugins
{
    public delegate string ConflictResolver(string snapBaseData, string snapSecondData);
}

namespace CrimsonPlugins.Internal.RC
{
    public struct RankingData
    {
        public long m_score;
        public int m_rank;
        public RankingData(long score, int rank)
        {
            m_score = score;
            m_rank = rank;
        }
    }
    /////////////////////////////////////////////
    [System.Serializable]
    public struct LeaderboardCfg
    {
        public RCLeaderboard leaderboardEnum;
        public string leaderboardName;
        public string leaderboardId;
    }
    /////////////////////////////////////////////
    [System.Serializable]
    public struct AchievementsCfg
    {
        public RCAchievement achievementEnum;
        public string achievementId;
    }
    /////////////////////////////////////////////
    [System.Serializable]
    public struct AchivementReveal
    {
        public RCAchievement achivementTrigger;
        public RCAchievement achivementToReveal;
    }
    /////////////////////////////////////////////
    public interface IRanking
    {
        bool p_IsAuthenticated { get; }
        bool p_WasLoggedIn { get; set; }
        string p_playerId { get; }

        RankingData? GetRankingData(RCLeaderboard leaderboard);
        void Init(LeaderboardCfg[] leaderboardsCfg, AchievementsCfg[] achievementsCfg, AchivementReveal[] hiddenAchievements);
        void Login();
        void ReportScore(RCLeaderboard leaderboard, long score);
        void UpdateRankings();
        void ShowLeaderboards();
        void ShowLeaderboards(RCLeaderboard leaderboard);
        void ShowAchievements();
        void AchievementIncrement(RCAchievement achievement, float progress);
        void AchievementUnlock(RCAchievement achievement);
        void AchievementReveal(RCAchievement achievement);

        void CheckAchievementNeedReveal();

    }
    /////////////////////////////////////////////
    /////////////////////////////////////////////
    public interface ISavedGames
    {
        bool p_IsAuthenticated { get; }

        void InternalInit();
        void StartLoadingSavedGames();
        void SaveGame(string data, ConflictResolver androidConflictResolveDelegate = null);

    }

    /////////////////////////////////////////////
    /////////////////////////////////////////////
}
