﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class OtherGameProperties : MonoBehaviour
{
    public const string OTHER_GAME_NAME_PATH = "OtherGameNames\\";

    [SerializeField]
    protected Image gameNameImage;
    [SerializeField]
    private Text level;

    void OnEnable()
    {
        gameNameImage.gameObject.SetActive(false);
        level.text = (GameplayManager.CurrentLevel + 1).ToString();

        StartCoroutine(LoadGameNameImage());

    }

    protected virtual IEnumerator LoadGameNameImage()
    {
        ResourceRequest req = Resources.LoadAsync<Sprite>(OTHER_GAME_NAME_PATH + GameplayManager.CurrentGameMode.Mode.ToString());
        yield return req;

        gameNameImage.gameObject.SetActive(true);
        gameNameImage.sprite = req.asset as Sprite;
    }
}


