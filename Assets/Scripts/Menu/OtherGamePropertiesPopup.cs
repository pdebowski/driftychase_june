﻿using UnityEngine;
using System.Collections;

public class OtherGamePropertiesPopup : OtherGameProperties
{
    [SerializeField]
    private BaseAnimation popupAnimation;

    protected override IEnumerator LoadGameNameImage()
    {
        yield return base.LoadGameNameImage();

        popupAnimation.deactivateOnAnimationEnd = true;
        popupAnimation.OnAnimationEnd += ClearAsset;
        popupAnimation.StartAnimation();
    }

    private void ClearAsset()
    {
        Resources.UnloadAsset(gameNameImage.sprite);
        gameNameImage.sprite = null;
    }
}
