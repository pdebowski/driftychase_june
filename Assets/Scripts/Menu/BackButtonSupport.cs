﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BackButtonSupport : MonoBehaviour {

    public Button button;

    [Tooltip("If any of this scripits is enabled, this one is off")]
    public BackButtonSupport[] overridingBackButtons;
    [SerializeField]
    private bool canExitGame = false;

    protected virtual void Update()
    {
        foreach (var item in overridingBackButtons)
        {
            if (item!=null && item.isActiveAndEnabled)
            {
                return;
            }
        }

        if (Input.GetKeyDown(KeyCode.Escape) || PadController.Instance.isPressed(ControllerButtons.BUTB))
        {
            if(button != null && button.isActiveAndEnabled)
            {
                DL.Log("BackButtonClick. Scene: " + gameObject.scene.name + "  Object: " + gameObject.name);
                button.onClick.Invoke();
            }           
            else if(canExitGame)
            {
                DL.Log("BackButtonExitGame. Scene: " + gameObject.scene.name + "  Object: " + gameObject.name);
                GameManager.OnGameExit();
            }
        }
    }

}
