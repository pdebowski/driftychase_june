using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class StartGameSceneGUIItemManager : MonoBehaviour
{
    private const string RECORD_TEXT = "Best: # ";

    [SerializeField]
    private Text totalCoins;
    [SerializeField]
    private Text recordValue;

    void OnEnable()
    {
        Refresh();
        ScoreCounter.OnCashOrScoreChanged += Refresh;
        IAPManager.OnPurchased += Refresh;
    }

    void OnDisable()
    {
        ScoreCounter.OnCashOrScoreChanged -= Refresh;
        IAPManager.OnPurchased -= Refresh;
    }

    private void Refresh()
    {
        totalCoins.text = CommonMethods.GetCashStringWithSpacing(PrefsDataFacade.Instance.GetTotalCash());
        recordValue.text = RECORD_TEXT + PrefsDataFacade.Instance.GetRecord().ToString();
    }
}
