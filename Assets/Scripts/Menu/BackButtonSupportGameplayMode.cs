﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BackButtonSupportGameplayMode : BackButtonSupport {

    protected override void Update()
    {
        if(GameManager.Instance.CurrentState == GameManager.GameState.Playing && !Tutorial.IsTutorial && !DailyGiftManager.IsDailyGiftActive)
        {
            base.Update();
        }
    }

}
