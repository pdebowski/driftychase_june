﻿using UnityEngine;
using System.Collections;

public class LeaderboardsResetExplanation : MonoBehaviour
{
    private const string WAS_DISPLAYED = "WAS_DISPLAYER_EXPLANATION_SCREEN";

    public void OnLoaderboardsOpened()
    {
        if (PlayerPrefsAdapter.TotalRunAppTimes > 1 && !PlayerPrefs.HasKey(WAS_DISPLAYED))
        {
            this.gameObject.SetActive(true);
        }

        PlayerPrefs.SetString(WAS_DISPLAYED, "true");
    }

    public void Close()
    {
        this.gameObject.SetActive(false);
    }
}
