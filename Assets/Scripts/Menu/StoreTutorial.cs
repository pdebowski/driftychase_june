﻿using UnityEngine;
using System.Collections;

public class StoreTutorial : MonoBehaviour
{
    [SerializeField]
    private GameObject tutorialHolder;

    void OnEnable()
    {
        if (!PlatformRecognition.IsAndroidTV)
        {

            StoreManager.OnNextCarShown += TurnOff;

            if (!PlayerPrefsAdapter.WasStoreTutorialShown)
            {
                TurnOn();
            }
            else
            {
                TurnOff();
            }

        }
        else
        {
            TurnOff();
        }
    }

    void OnDisable()
    {
        StoreManager.OnNextCarShown -= TurnOff;
    }

    private void TurnOn()
    {
        tutorialHolder.SetActive(true);
    }

    private void TurnOff()
    {
        PlayerPrefsAdapter.WasStoreTutorialShown = true;
        Destroy(this.gameObject);
    }

}
