﻿using UnityEngine;
using System.Collections;

public class SoundManager : MonoBehaviour
{

    public static void PlaySFX(AudioSource source)
    {
        if (PlayerPrefsAdapter.SFX)
        {
            source.Play();
        }
    }

    public static void PlayMusic(AudioSource source)
    {
        if (PlayerPrefsAdapter.MUSIC)
        {
            source.Play();
        }
    }

    public static void UnPauseMusic(AudioSource source)
    {
        if (PlayerPrefsAdapter.MUSIC)
        {
            source.UnPause();
        }
    }

    public static void MuteSound(bool mute)
    {
        //PrefabReferences.Instance.AudioListener.enabled = !mute;
		AudioListener.volume = mute?0f:1f;
    }
}
