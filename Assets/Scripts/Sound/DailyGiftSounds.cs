﻿using UnityEngine;
using System.Collections;

public class DailyGiftSounds : Singleton<DailyGiftSounds>
{

    [SerializeField]
    private AudioClip boomClip;
    [SerializeField]
    private AudioSource source;

    public void Play()
    {
        source.clip = boomClip;
        source.loop = false;
        SoundManager.PlaySFX(source);

    }
}
