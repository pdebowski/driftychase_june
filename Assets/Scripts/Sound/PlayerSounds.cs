﻿using UnityEngine;
using System.Collections;

public class PlayerSounds : MonoBehaviour
{
    public AudioClip EngineStartClip { get; set; }

    [SerializeField]
    private AudioClip keysClip;
    [SerializeField]
    private AudioClip engineClip;
    [SerializeField]
    private AudioClip[] tiresClips;
    [SerializeField]
    private AudioClip[] crashClips;
    [SerializeField]
    private AudioSource engineSource;
    [SerializeField]
    private AudioSource crashSource;
    [SerializeField]
    private AudioSource tiresSource;
    [SerializeField]
    private AudioSource engineStartSource;
    [SerializeField]
    private AudioSource keysSource;

    void OnEnable()
    {
        GameplayManager.OnSessionStarted += Run;
        GameplayManager.OnContinueUsed += Run;
        GameplayManager.OnPlayerCrash += OnCrash;
    }

    void OnDisable()
    {
        GameplayManager.OnSessionStarted -= Run;
        GameplayManager.OnContinueUsed -= Run;
        GameplayManager.OnPlayerCrash -= OnCrash;
    }

    private void Run()
    {
        engineSource.clip = engineClip;
        engineSource.loop = true;
        SoundManager.PlaySFX(engineSource);
    }

    private void OnCrash()
    {
        crashSource.clip = crashClips.RandomElement();
        crashSource.loop = false;
        engineSource.Stop();
        SoundManager.PlaySFX(crashSource);
    }

    public void OnDriftStart()
    {
        tiresSource.clip = tiresClips.RandomElement();
        tiresSource.loop = false;
        SoundManager.PlaySFX(tiresSource);
    }

    public void EngineStart()
    {
        keysSource.clip = keysClip;
        keysSource.loop = false;
        SoundManager.PlaySFX(keysSource);

        engineStartSource.clip = EngineStartClip;
        engineStartSource.loop = false;
        SoundManager.PlaySFX(engineStartSource);

        engineSource.clip = engineClip;
        engineSource.volume = 0f;
        engineSource.loop = true;
        SoundManager.PlaySFX(engineSource);
        StartCoroutine(StartEngineCor());
    }

    private IEnumerator StartEngineCor()
    {
        float phase = 0;
        float duration = 0.5f;
        float maxVolume = 0.15f;

        while (phase < 1)
        {
            phase += Time.deltaTime / duration;
            engineSource.volume = Mathf.Lerp(0, maxVolume, phase);

            yield return null;
        }
    }
}
