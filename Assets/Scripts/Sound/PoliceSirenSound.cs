﻿using UnityEngine;
using System.Collections;

public class PoliceSirenSound : MonoBehaviour
{


    [SerializeField]
    private AudioClip clip;
    [SerializeField]
    private AudioSource source;

    void OnEnable()
    {
        GameplayManager.OnPlayerCrash += PlayWithDelay;
    }

    void OnDisable()
    {
        GameplayManager.OnPlayerCrash += PlayWithDelay;
    }

    private void PlayWithDelay()
    {
        StartCoroutine(Play(1));
    }

    private IEnumerator Play(float delay)
    {
        yield return new WaitForSeconds(delay);
        source.clip = clip;
        SoundManager.PlaySFX(source);
    }
}
