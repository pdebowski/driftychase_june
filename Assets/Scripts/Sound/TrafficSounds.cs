﻿using UnityEngine;
using System.Collections;

public class TrafficSounds : MonoBehaviour
{

    [SerializeField]
    [Range(0.0f, 1.0f)]
    private float honkChance;

    [SerializeField]
    private AudioClip[] honkClips;
    [SerializeField]
    private AudioClip[] crashClips;
    [SerializeField]
    private AudioSource mainSource;

    private bool wasHonk = false;

    void OnEnable()
    {
        wasHonk = false;
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag != "Player" && Random.value < 0.3f && GetComponent<MeshRenderer>().isVisible)
        {
            mainSource.loop = false;
            mainSource.clip = crashClips.RandomElement();
            SoundManager.PlaySFX(mainSource);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player" && !wasHonk)
        {
            if (Random.value < honkChance)
            {
                mainSource.loop = false;
                mainSource.clip = honkClips.RandomElement();
                SoundManager.PlaySFX(mainSource);
                wasHonk = true;
            }
        }
    }


}
