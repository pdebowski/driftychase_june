﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public class AudioSourceActivator : MonoBehaviour
{

    void OnEnable()
    {
        SoundManager.PlaySFX(GetComponent<AudioSource>());
    }
}
