﻿using UnityEngine;
using System.Collections;

public class CashSounds : Singleton<CashSounds>
{

    [SerializeField]
    private AudioClip[] cashClips;
    [SerializeField]
    private AudioSource[] cashSources;

    int currentSoundSource = 0;

    public void Play()
    {
        cashSources[currentSoundSource].clip = cashClips.RandomElement();
        cashSources[currentSoundSource].loop = false;
        SoundManager.PlaySFX(cashSources[currentSoundSource]);

        currentSoundSource++;
        currentSoundSource %= cashSources.Length;
    }
}
