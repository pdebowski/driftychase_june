﻿using UnityEngine;
using System.Collections;

public class TurnSignal : MonoBehaviour
{

    [SerializeField]
    private AudioSource source;

    void Awake()
    {
        SoundManager.PlaySFX(source);
    }
}
