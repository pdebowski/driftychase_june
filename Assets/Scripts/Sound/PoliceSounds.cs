﻿using UnityEngine;
using System.Collections;

public class PoliceSounds : MonoBehaviour
{

    [SerializeField]
    private AudioClip[] crashClip;
    [SerializeField]
    private AudioClip biggerSirenClip;
    [SerializeField]
    private AudioClip sirenClip;
    [SerializeField]
    private AudioClip driftClip;
    [SerializeField]
    private AudioSource crashSource;
    [SerializeField]
    private AudioSource sirenSource;
    [SerializeField]
    private AudioSource biggerSirenSource;
    [SerializeField]
    private AudioSource driftSource;

    public void TurnOnSiren()
    {
        sirenSource.clip = sirenClip;
        sirenSource.loop = true;
        SoundManager.PlaySFX(sirenSource);
    }

    public void TurnOffSiren()
    {
        sirenSource.Pause();
    }

    public void TurnOnBiggerSiren()
    {
        biggerSirenSource.clip = biggerSirenClip;
        biggerSirenSource.loop = false;
        SoundManager.PlaySFX(biggerSirenSource);
    }

    public void OnCrash()
    {
        crashSource.clip = crashClip.RandomElement();
        crashSource.loop = false;
        SoundManager.PlaySFX(crashSource);
    }

    public void OnDrift()
    {
        driftSource.clip = driftClip;
        driftSource.loop = false;
        SoundManager.PlaySFX(driftSource);
    }

}
