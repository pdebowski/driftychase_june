﻿using UnityEngine;
using System.Collections;

public class MenuRandomSound : MonoBehaviour
{

    public bool PreparingToTurnOff { get; set; }

    [SerializeField]
    private AudioClip[] clips;
    [SerializeField]
    private AudioSource source;
    [SerializeField]
    private float minDelay;
    [SerializeField]
    private float maxDaley;

    private Coroutine coroutine;

    void OnEnable()
    {
        PreparingToTurnOff = false;
        coroutine = StartCoroutine(RandomizeSoundCor());
    }

    void OnDisable()
    {
        source.Pause();
        if (coroutine != null)
        {
            StopCoroutine(coroutine);
        }
    }

    private IEnumerator RandomizeSoundCor()
    {
        float delay;
        while (!PreparingToTurnOff)
        {
            delay = Random.Range(minDelay, maxDaley);
            yield return new WaitForSeconds(delay);
            if ((!PreparingToTurnOff))
            {
                source.clip = clips.RandomElement();
                source.loop = false;
                SoundManager.PlaySFX(source);
            }
        }
    }




}
