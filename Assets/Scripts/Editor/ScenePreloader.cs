using UnityEngine;
using System.Collections;
using UnityEditor;
using UnityEditor.SceneManagement;

[InitializeOnLoad]
public class ScenePreloader
{
    private const string OPEN_SCENE_DURING_GAME_STARTUP = "OpenSceneDuringGameStartup";

    static ScenePreloader()
    {
        EditorApplication.playmodeStateChanged += PlaymodeStateChanged;
    }

    private static void PlaymodeStateChanged()
    {
        if (!EditorApplication.isPlaying && EditorApplication.isPlayingOrWillChangePlaymode)
        {
            PlayerPrefs.SetString(OPEN_SCENE_DURING_GAME_STARTUP, EditorSceneManager.GetActiveScene().name);
            EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());
            EditorSceneManager.OpenScene(string.Format("Assets/Scenes/{0}.unity", "GameScene"));
        }
        else if (!EditorApplication.isPlaying && !EditorApplication.isPlayingOrWillChangePlaymode)
        {
            EditorSceneManager.OpenScene(string.Format("Assets/Scenes/{0}.unity", PlayerPrefs.GetString(OPEN_SCENE_DURING_GAME_STARTUP, "GameScene")));
        }
    }

}
