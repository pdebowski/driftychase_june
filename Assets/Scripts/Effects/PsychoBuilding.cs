using UnityEngine;
using System.Collections;

public class PsychoBuilding : MonoBehaviour {

	float x=0,y=0;

    [SerializeField]
    Renderer[] renderers;

	void OnDrawGizmos()
	{
		if (Application.isPlaying /*&&  UnityEditor.EditorApplication.isPaused*/)
		{
		//	UpdateTemp(1/60f);
		}

	}

	void Update()
	{
		UpdateTemp (Time.deltaTime);
	}
	// Update is called once per frame
	void UpdateTemp (float val) {
		Vector2 offsetSpeed = WorldEffectsManager.Instance.GetBuildingOffsetSpeed ();
		x+=val*offsetSpeed.x;
		y+=val*offsetSpeed.y;
        foreach (var r in renderers)
        {
            if (r.gameObject.activeInHierarchy)
            {
                r.material.mainTextureOffset = new Vector2(x, y);
            }
        }
	}
}
