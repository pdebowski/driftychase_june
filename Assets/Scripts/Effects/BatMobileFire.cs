using UnityEngine;
using System.Collections;

public class BatMobileFire : MonoBehaviour {

    [SerializeField]
    private float maxSpeed;
    [SerializeField]
    private Vector3 minScale, maxScale;

    Vector3 lastPos;
	
    void OnEnable ()
    {
        lastPos = transform.position;
	}
	
	void FixedUpdate ()
    {
        Debug.Log((transform.position - lastPos).magnitude);
        transform.localScale = Vector3.Lerp(minScale, maxScale, (transform.position - lastPos).magnitude / maxSpeed) *Random.Range(1f,1.25f);
        lastPos = transform.position;

    }
}
