﻿using UnityEngine;
using System.Collections;

public class ChristmasCarParticles : MonoBehaviour
{

    [SerializeField]
    private ParticleSystem[] particles;

    void OnEnable()
    {
        GameplayManager.OnSessionStarted += TurnOnParticles;
        GameplayManager.OnPlayerCrash += TurnOffParticles;
        GameplayManager.OnContinueUsed += TurnOnParticles;
    }

    void OnDisable()
    {
        GameplayManager.OnSessionStarted -= TurnOnParticles;
        GameplayManager.OnPlayerCrash -= TurnOffParticles;
        GameplayManager.OnContinueUsed -= TurnOnParticles;
    }

    private void TurnOnParticles()
    {
        foreach (var item in particles)
        {
            item.Play();
        }
    }

    private void TurnOffParticles()
    {

        foreach (var item in particles)
        {
            item.Stop();
        }
    }
}
