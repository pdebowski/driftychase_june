using UnityEngine;
using System.Collections;
using System.Linq;

public class ArrowsConfig : MonoBehaviour {

    [System.Serializable]
    public class ConfigStruct
    {
        public WorldType worldType;
        public Color ArrowColor = Color.white;
        public Color BarColor= Color.white;
    }

    [SerializeField]
    ConfigStruct []arrowsConfig;


    public ConfigStruct GetArrowsConfig(WorldType worldType)
    {
        ConfigStruct ret = null;
        ret = arrowsConfig.FirstOrDefault(v => v.worldType == worldType);
        if (ret == null)
        {
            ret = arrowsConfig.First(v => v.worldType == WorldType.NewYork);
        }

        return ret;
    }
}
