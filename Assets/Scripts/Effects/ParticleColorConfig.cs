using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class ParticleColorConfig : MonoBehaviour {

    [System.Serializable]
    public class VarConfig
    {
        public WorldType worldType;
        public Color color;
    }

    public List<VarConfig> config;

    public Color GetColorForParticle(WorldType worldType)
    {
        VarConfig ret = config.FirstOrDefault(v => v.worldType == worldType);
        if (ret == null)
        {
            ret = config.FirstOrDefault(v => v.worldType == WorldType.NewYork);
        }
        return ret.color;
    }

	
}
