using UnityEngine;
using System.Collections;

public class ParticleChanger : MonoBehaviour {

    void OnEnable()
    {
        PlayerSpeedManager.OnWorldChange += OnMoodChanged;
    }

    void OnDisable()
    {
        PlayerSpeedManager.OnWorldChange -= OnMoodChanged;
    }

    void OnMoodChanged()
    {
        Color color = WorldEffectsManager.Instance.GetParticleColor();
		StartCoroutine(SetColor(color));
    }

	IEnumerator SetColor(Color color)
	{
		yield return new WaitForSeconds(1f);

		GetComponent<Renderer>().material.SetColor("_Color", color);
	}
}