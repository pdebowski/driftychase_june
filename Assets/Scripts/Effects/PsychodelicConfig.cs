﻿using UnityEngine;
using System.Collections;

public class PsychodelicConfig : MonoBehaviour {

	public struct ConfigVar
	{
		public float duration;
		public Color[] colors;
	}

	[SerializeField]
	Color[] colors;
	[SerializeField]
	float duration;
	[SerializeField]
	Vector2 buildingOffsetSpeed;



	public ConfigVar GetPsychodelicConfig()
	{
		return new ConfigVar(){colors = this.colors, duration = this.duration};
	}

	public Vector2 GetBuidingOffsetSpeed()
	{
		return buildingOffsetSpeed;
	}

}
