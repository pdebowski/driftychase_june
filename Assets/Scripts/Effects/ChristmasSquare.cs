﻿using UnityEngine;
using System.Collections;

public class ChristmasSquare : MonoBehaviour
{
    [SerializeField]
    private Texture2D[] lightsTextures;
    [SerializeField]
    private Material squareOriginalMaterial;
    [SerializeField]
    private MeshRenderer squareRenderer;
    [SerializeField]
    private Material lightsOnOriginalMaterial;
    [SerializeField]
    private Material lightsOffOriginalMaterial;
    [SerializeField]
    private MeshRenderer lightsRenderer;
    [SerializeField]
    private float blinkTime = 1.0f;
    [SerializeField]
    private int[] lightMaterialsOrder;

    private int currentOffLight = -1;
    private Material lightsOnMaterial;
    private Material lightsOffMaterial;
    private Material squareMaterial;

    void Awake()
    {
        Init();
    }

    void OnEnable()
    {
        StopAllCoroutines();
        StartCoroutine(BlinkCor());
    }

    private void Init()
    {
        lightsOnMaterial = new Material(lightsOnOriginalMaterial);
        lightsOffMaterial = new Material(lightsOffOriginalMaterial);
        squareMaterial = new Material(squareOriginalMaterial);
        squareRenderer.material = squareMaterial;
        TurnOffNextLight();
    }

    private void TurnOffNextLight()
    {
        currentOffLight = (currentOffLight + 1) % lightsRenderer.materials.Length;

        Material[] newMaterials = new Material[4];
        for (int i = 0; i < lightsRenderer.materials.Length; i++)
        {
            if (i == currentOffLight)
            {
                newMaterials[lightMaterialsOrder[i]] = lightsOffMaterial;
                squareMaterial.SetTexture("_LightsMap", lightsTextures[currentOffLight]);
            }
            else
            {
                newMaterials[lightMaterialsOrder[i]] = lightsOnMaterial;
            }
        }



        lightsRenderer.materials = newMaterials;
    }


    private IEnumerator BlinkCor()
    {
        while (true)
        {
            yield return new WaitForSeconds(blinkTime);
            TurnOffNextLight();
        }
    }

}
