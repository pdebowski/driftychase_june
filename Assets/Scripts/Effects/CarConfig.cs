using UnityEngine;
using System.Collections;
using System.Linq;


public class CarConfig : MonoBehaviour {
    [System.Serializable]
    public class CarMaterialMapper
    {
        public GameObject carPrefab;
        public Material material1;
        public Material material2;
        public Material material3;
    }

    [System.Serializable]
    public class ConfigStruct
    {
        public WorldType worldType;
        public CarMaterialMapper[] materialMapper;
    };

    public ConfigStruct[] tafficConfig;

    public CarMaterialMapper GetMaterialsForCar(string prefabName, WorldType worldType)
    {
        ConfigStruct carConfig = tafficConfig.FirstOrDefault(v => v.worldType == worldType);

        if (carConfig != null)
        {
            CarMaterialMapper carRet = null;
            carRet = carConfig.materialMapper.FirstOrDefault(v => prefabName.Contains(v.carPrefab.name));

            if (carRet != null)
            {
                return carRet;
            }
            else Debug.Log("Car Not Found ");
        }

        carConfig = tafficConfig.First(v => v.worldType == WorldType.NewYork);
        CarMaterialMapper defaultRet=carConfig.materialMapper.First(v => prefabName.Contains(v.carPrefab.name));

        return defaultRet;


    }

}
