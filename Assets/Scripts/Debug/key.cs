﻿using UnityEngine;
using System.Collections;

public class key : MonoBehaviour {
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        GetComponent<Animator>().SetBool("Button01", Input.GetKey(KeyCode.Alpha1));
        GetComponent<Animator>().SetBool("Button02", Input.GetKey(KeyCode.Alpha2));
        GetComponent<Animator>().SetBool("Button03", Input.GetKey(KeyCode.Alpha3));
        GetComponent<Animator>().SetBool("Button04", Input.GetKey(KeyCode.Alpha4));
        GetComponent<Animator>().SetBool("StartEngine", Input.GetKey(KeyCode.Alpha5));
        
    }
}
