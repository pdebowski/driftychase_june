using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class GameDebug : MonoBehaviour
{


    void Awake()
    {
        if (!Debug.isDebugBuild)
        {
            Destroy(this.gameObject);
        }
    }

    public void AddMoney()
    {
        PrefsDataFacade.Instance.AddTotalCash(1000000);
    }

    [ContextMenu("Unlock cars")]
    public void UnlockAllCars()
    {
        foreach (CarDefinitions.CarEnum carEnum in System.Enum.GetValues(typeof(CarDefinitions.CarEnum)))
        {
            PrefsDataFacade.Instance.BuyCar(carEnum);
        }
    }

    public void BuyVIP()
    {
        VIP.SetAccountAsVIP();
    }

    public void AddRecord()
    {
		PrefsDataFacade.Instance.SetRecord(PrefsDataFacade.Instance.GetRecord() + 20,true);
    }
}
