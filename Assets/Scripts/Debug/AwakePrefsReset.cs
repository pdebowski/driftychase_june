﻿using UnityEngine;
using System.Collections;
using System.IO;

public class AwakePrefsReset : MonoBehaviour
{

    void Awake()
    {
        PlayerPrefs.DeleteAll();
        File.Delete(Application.persistentDataPath + "dataStuff.myData");
    }
}
