﻿using UnityEngine;
using System.Collections;

public class CrimsonError {

	public static void LogError(object message)
	{
		CrimsonAnalytics.SelfMonitoring.Errors.Error.LogEvent(message.ToString());
		Debug.LogError(message);
	}

	public static void LogWarning(object message)
	{
		Debug.LogWarning(message);
	}

}
