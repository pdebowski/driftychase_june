﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ActualSpeedText : MonoBehaviour {

    [SerializeField]
    private Text text;

	void Update()
    {
        text.text = PrefabReferences.Instance.Player.GetComponent<PlayerSpeedManager>().Speed.ToString();
    }
}
