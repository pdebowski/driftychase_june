using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class TweakSinCity : MonoBehaviour {

    public bool updateAllBr=false;
    public bool updateAllC1 = false;
    public bool updateAllC2 = false;
    public bool updateAllC3 = false;
    public bool updateAllISSephia = false;
    public bool updateAllSephiaFactor = false;



    [System.Serializable]
    public class Vars
    {
        public string shaderNamePart;
        public float Bright=0;
        public float C1 = 1;
        public float C2 = 1;
        public float C3 = 1;
        public bool ISSephia=false;
        public float SephiaFactor = 0;
    }
    public Color roadColor = Color.black;
    public Color roadStripsColor = Color.white;


    public List<Vars> config;

    
    bool paused = false;


#if UNITY_EDITOR
    void OnDrawGizmos()
    {
        if (Application.isPlaying &&  UnityEditor.EditorApplication.isPaused)
        {
            ChangeMatVals();
        }

    }
#endif


    void Update()
    {
        ChangeMatVals();
    }

    void ChangeMatVals() {
        var allRenderers= GameObject.FindObjectsOfType<Renderer>().ToList();
        var allMats = new List<Material>();

        allRenderers.ForEach(v =>  allMats.AddRange(v.materials.ToList()));


        if (config.Any(v => v.shaderNamePart.Equals("ALL")))
        {
            var allConf = config.First(v => v.shaderNamePart.Equals("ALL"));
            config.ForEach(v =>
            {
                v.Bright= updateAllBr? allConf.Bright: v.Bright;
                v.C1 = updateAllC1? allConf.C1: v.C1;
                v.C2 = updateAllC2 ? allConf.C2:v.C2;
                v.C3 = updateAllC3 ? allConf.C3:v.C3;
                v.ISSephia = updateAllISSephia? allConf.ISSephia: v.ISSephia;
                v.SephiaFactor = updateAllSephiaFactor? allConf.SephiaFactor: v.SephiaFactor;
            });
        }

        config.ForEach(conf => {
            var materialsWithShader = (from mat in allMats where mat.shader.name.ToUpper().Contains(conf.shaderNamePart.ToUpper()) select mat).ToList();
            if (materialsWithShader.Count==0 && conf.shaderNamePart.Equals("ALL")==false)
            {
//                Debug.LogError("no mats with name " + conf.shaderNamePart);
            }
            materialsWithShader.ForEach(v =>
            {
                v.SetFloat("_Bright", conf.Bright);
                v.SetFloat("_C1", conf.C1);
                v.SetFloat("_C2", conf.C2);
                v.SetFloat("_C3", conf.C3);
                v.SetFloat("_ISSephia", conf.ISSephia?1:0);
                v.SetFloat("_SephiaFact", conf.SephiaFactor);

            });
        });

        allMats.Where(v => v.name.ToUpper().Contains("ROAD") && !v.name.ToUpper().Contains("STRIPE") 
                    && v.shader.name.Equals("Unlit/Color")).ToList().ForEach(v => v.color = roadColor);

        allMats.Where(v => v.name.ToUpper().Contains("STRIPE")
                    && v.shader.name.Equals("Unlit/Color")).ToList().ForEach(v => v.color = roadStripsColor);


    }
}
