﻿using UnityEngine;
using System.Collections;

public class EULAManager : MonoBehaviour {

    public static string EulaUrl = "http://crimsonpine.com/driftychase/eula";

    [SerializeField]
    private LoadingSceneManager loadingSceneManager;
    [SerializeField]
    private GameObject holder;

    private bool initialized = false;

    void Awake()
    {
        holder.SetActive(false);
        loadingSceneManager.IsEulaShowing = true;
    }

    void Update()
    {
        if(loadingSceneManager.IsCustomAdShowing == false && !initialized)
        {
            Initialize();
        }
    }

    private void Initialize()
    {
        initialized = true;

        if(!PlayerPrefsAdapter.WasEULAAccepted && PlatformRecognition.IsAndroidDevice)
        {
            Show();
        }
        else
        {
            Close();
        }
    }

    private void Show()
    {
        holder.SetActive(true);
    }

	private void Close()
    {
        holder.SetActive(false);        
        loadingSceneManager.IsEulaShowing = false;
    }
    
    public void OnAcceptButtonClicked()
    {
        PlayerPrefsAdapter.WasEULAAccepted = true;
        Close();
    }

    public void OnLinkButtonClicked()
    {
		Application.OpenURL(EulaUrl);
    }
}
