using UnityEngine;
using System.Collections;

public class PauseSceneManager : MonoBehaviour
{

    public static System.Action OnPauseEnter = delegate { };
    public static System.Action OnBackToGameFromPause = delegate { };
    public static System.Action OnExitFromPause = delegate { };

    [SerializeField]
    private GameObject content;

    void Start()
    {
        OnPauseEnter();
    }

    public void OnReturnButtonClick()
    {
        OnBackToGameFromPause();
        content.GetComponent<CanvasGroup>().interactable = false;
        GlobalFader.Instance.UseUnscaledDeltaTime = true;
        GlobalFader.Instance.Fade(content.GetComponent<CanvasGroup>(), 1, 0, null, OnStartFadeEnd);
    }

    private void OnStartFadeEnd()
    {
        GlobalFader.Instance.UseUnscaledDeltaTime = false;
        UnloadPauseScene();
    }

    public void OnExitButtonClick()
    {
        OnExitFromPause();
        GameplayManager.OnSessionEndedFromPause();
        GlobalFader.Instance.UseUnscaledDeltaTime = true;
        GlobalFader.Instance.Fade(null, OnFullyBlackScreenExit, UnloadPauseScene);
    }

    private void OnFullyBlackScreenExit()
    {
        content.SetActive(false);
        PrefabReferences.Instance.Player.GetComponent<PlayerCollision>().CrashAfterExitFromPause();
        GameManager.Instance.CurrentState = GameManager.GameState.InMenus;
        string sceneName = SceneNames.GetRealSceneName_LevelFilter("ResultScene");
        SceneLoader.Instance.LoadSceneAdditive(sceneName);
        PrefabReferences.Instance.GameSceneManager.SetActiveHUD(false);
        GameplayManager.OnSceneAfterGameLoaded();
        GameplayManager.OnContinueResultScreenLoading(ResultSceneType.Result);
    }

    private void UnloadPauseScene()
    {
        GlobalFader.Instance.UseUnscaledDeltaTime = false;
        SceneLoader.Instance.UnloadScene("Scenes/PauseScene");
        //Resources.UnloadUnusedAssets();
    }
}
