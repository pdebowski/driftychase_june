﻿using UnityEngine;
using System.Collections;

public class InstalledAppsReporter : MonoBehaviour {

    public string[] appPackages;

    private string prefsKey = "INSTALLED_APPS_REPORTER_WAS_RUNNED";
    private int ammountToCheck = 999999;
    private bool anyAppInstalled = false;

    void Awake ()
    {
        if (!PlayerPrefs.HasKey(prefsKey))
        {
            ammountToCheck = appPackages.Length;
            AndroidNativeUtility.OnPackageCheckResult += OnPackageCheckResult;
            foreach (var package in appPackages)
            {
                AndroidNativeUtility.Instance.CheckIsPackageInstalled(package);
            }
            PlayerPrefs.SetInt(prefsKey, 1);
        }
    }

    void OnPackageCheckResult(AN_PackageCheckResult res)
    {
        if (res.IsSucceeded)
        {
            Report(res.packageName);   
        }

        ammountToCheck--;
        anyAppInstalled |= res.IsSucceeded;

        if(ammountToCheck == 0 && !anyAppInstalled)
        {
            CrimsonAnalytics.Other.InstalledAppsReporter.None.LogEvent();
        }
    }

    private void Report(string packageName)
    {
        string appName = TrimStart(packageName, "com.crimsonpine.");

        if(appName == "geometryrace")
        {
            CrimsonAnalytics.Other.InstalledAppsReporter.GeometryRace.LogEvent();
        }
        else if (appName == "followtheline2")
        {
            CrimsonAnalytics.Other.InstalledAppsReporter.FTL2.LogEvent();
        }
        else if(appName == "jumpbuddies")
        {
            CrimsonAnalytics.Other.InstalledAppsReporter.JumpBuddies.LogEvent();
        }
        else
        {
            Debug.LogError("InstalledAppsReported - package name not found ! Failed report.");
        }
    }

    private string TrimStart(string value, string toTrim)
    {
        if (value.StartsWith(toTrim))
        {
            int startIndex = toTrim.Length;
            return value.Substring(startIndex);
        }
        return value;
    }
}
