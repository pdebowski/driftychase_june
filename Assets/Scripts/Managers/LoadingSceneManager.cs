using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoadingSceneManager : MonoBehaviour {

    public static bool wasLoadingScreenFired = false;
    private const float MIN_LOADING_TIME = 3f;

    [SerializeField]
    private Image loadingSliderImage;
    [SerializeField]
    private Text hintText;
    [SerializeField]
    private GameObject loadingSceneHolder;
    [SerializeField]
    private HintsDB hintsDB;
    [SerializeField]
    UnityEngine.UI.Image bgImage; 

	public bool IsCustomAdShowing { get; set; }
    public bool IsEulaShowing { get; set; }

    private AsyncOperation loadingGameScene;
    private AsyncOperation loadingStartGameScene;
    private float loadingTime = 0;
    private bool completed = false;
    private int stabilizer = 2;
    private bool loginToRankingInvoked = false;

    void Awake()
    {
        bgImage.sprite = Resources.Load<Sprite>("blurBG");
    }
    void Start()
    {
        wasLoadingScreenFired = true;
        hintText.text = hintsDB.GetHint();
        loadingGameScene = SceneManager.LoadSceneAsync("Scenes/GameScene", LoadSceneMode.Additive);
        loadingStartGameScene = SceneManager.LoadSceneAsync(TVFilter.GetRealName("Scenes/StartGameScene"), LoadSceneMode.Additive);
        loadingGameScene.allowSceneActivation = false;
        loadingStartGameScene.allowSceneActivation = false;   
    }

    void Update()
    {
        if (stabilizer-- > 0)
        {
            return;
        }

        loadingTime += Time.deltaTime;

        loadingSliderImage.fillAmount = Mathf.Min(loadingTime / MIN_LOADING_TIME, (loadingGameScene.progress + loadingStartGameScene.progress)/2f+0.1f);

        if(!loginToRankingInvoked && !IsCustomAdShowing && !IsEulaShowing)
        {
            loginToRankingInvoked = true;
            FindObjectOfType<CascadeInternetRequests>().LoginToRankingInstant();
        }

        if(!completed && loadingGameScene.progress >= 0.9f && loadingStartGameScene.progress >= 0.9f && loadingTime >= MIN_LOADING_TIME && !IsCustomAdShowing && !IsEulaShowing)
        {
            completed = true;           
            StartCoroutine(ActivateLoadedScenes());
        }

    }

    private IEnumerator ActivateLoadedScenes()
    {
        var sourceScene = SceneManager.GetActiveScene();

       /* GameObject objToDestroy = GameObject.FindWithTag("ToRemove");
        if (objToDestroy != null)
        {
            Destroy(objToDestroy);
        }
        */
        loadingGameScene.allowSceneActivation = true;
        loadingStartGameScene.allowSceneActivation = true;

        while (!loadingGameScene.isDone || !loadingStartGameScene.isDone)
        {
            yield return null;
        }

        var destinationScene = SceneManager.GetSceneByName("GameScene");
        SceneManager.MergeScenes(sourceScene, destinationScene);

        //waiting for blur to avoid black background
        yield return null;
        yield return null;

        var bgSprite = bgImage.sprite;
        Resources.UnloadAsset(bgSprite);
        bgImage.sprite = null;
        Destroy(loadingSceneHolder);
    }

}
