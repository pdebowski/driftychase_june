using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ContinueSceneManager : MonoBehaviour
{
    public static System.Action OnEnter = delegate { };

    private ContinueManager continueManager;

    [SerializeField]
    private GameObject content;
    [SerializeField]
    private Text coins;
    [SerializeField]
    private Text score;
    [SerializeField]
    private GameObject bribeWithVIP;
    [SerializeField]
    private GameObject bribeWithoutVIP;

    void Awake()
    {
        if (!VIP.IsVIPAccount)
        {
            CrimsonAnalytics.Offers.Continue.Displayed.LogEvent();
        }

        continueManager = PrefabReferences.Instance.ContinueManager;
        SetBribrButton();
    }

    void Start()
    {
        RefreshStats();
    }

    void OnEnable()
    {
        InitScene();
    }

    protected virtual void InitScene()
    {
        OnEnter();
        SaveCloud();
    }

    private void SaveCloud()
    {
        ScoreCounter.Instance.SetRecordAndTotalDistance(false, ScoreCounter.Instance.ContinueScore);
        PrefsDataFacade.Instance.AddTotalCash(ScoreCounter.Instance.SessionCash);
        PrefsDataFacade.Instance.SaveGameStateCloud();
        PrefsDataFacade.Instance.AddTotalCash(-ScoreCounter.Instance.SessionCash);
    }

    public void OnContinueForVideoButtonClick()
    {
        if (!VIP.IsVIPAccount)
        {
            CrimsonAnalytics.Offers.Continue.Collected.LogEvent();
            PrefabReferences.Instance.GameSceneManager.RewardedAdShow();

#if UNITY_EDITOR
            Reward(true);
#else
        RewardAd.Show(null, Reward);
#endif
        }
        else
        {
            GlobalFader.Instance.Fade(null, OnFullyBlackContinueFade, continueManager.OnFadeEnd);
        }
    }

    private void Reward(bool wasFullyWatched)
    {
        if (wasFullyWatched)
        {
            CrimsonAnalytics.Transaction.RewardedVideo.Displayed.LogEvent();
            GlobalFader.Instance.Fade(null, OnFullyBlackContinueFade, continueManager.OnFadeEnd);
        }
        else
        {
            OnExitButtonClick();
        }
        PrefabReferences.Instance.GameSceneManager.ClosedRewardedAd(wasFullyWatched);
    }

    private void OnFullyBlackContinueFade()
    {
        GameManager.Instance.SetGameState(GameManager.GameState.Playing);
        PrefabReferences.Instance.GameSceneManager.SetActiveHUD(true);
        PrefabReferences.Instance.AdAdapter.HideBanner();
        string sceneName = SceneNames.GetRealSceneName_LevelFilter("ContinueScene");
        SceneLoader.Instance.UnloadScene(sceneName);
        GameplayManager.OnSceneAfterGameLoaded();
        continueManager.OnContinue();
    }

    public void OnExitButtonClick()
    {
        if (!VIP.IsVIPAccount)
        {
            CrimsonAnalytics.Offers.Continue.Skipped.LogEvent();
        }

        GlobalFader.Instance.FadeIn(null, OnFullyBlackExitFadeToOffers, 0.5f, 0);
    }
    private void OnFullyBlackExitFadeToOffers()
    {
        PrefabReferences.Instance.GameSceneManager.ShowOffersScreen();
        UnloadContinueScene();
    }

    private void OnFullyBlackExitFadeToPremium()
    {
        content.SetActive(false);
        PrefabReferences.Instance.GameSceneManager.ShowPremiumScreen();
    }

    private void OnFadeEndToPremium()
    {
        string sceneName = SceneNames.GetRealSceneName_LevelFilter("ContinueScene");
        FindObjectOfType<PremiumShopSceneManager>().BackToScene(sceneName);
        UnloadContinueScene();
    }

    private void LoadScene(string name)
    {
        SceneLoader.OnSceneLoaded += OnSceneLoaded;
        SceneLoader.Instance.LoadSceneAdditive(name);
    }

    private void OnSceneLoaded(string name)
    {
        UnloadContinueScene();
        SceneLoader.OnSceneLoaded -= OnSceneLoaded;
    }

    private void UnloadContinueScene()
    {
        string sceneName = SceneNames.GetRealSceneName_LevelFilter("ContinueScene");
        SceneLoader.Instance.UnloadScene(sceneName);
    }

    public void RefreshStats()
    {
        score.text = "# " + ScoreCounter.Instance.CurrentScore.ToString();
        coins.text = "&   " + ScoreCounter.Instance.SessionCash.ToString();
    }

    private void SetBribrButton()
    {
        if (VIP.IsVIPAccount)
        {
            bribeWithVIP.SetActive(true);
            bribeWithoutVIP.SetActive(false);
        }
        else
        {
            bribeWithVIP.SetActive(false);
            bribeWithoutVIP.SetActive(true);
        }
    }
}
