﻿using UnityEngine;
using System.Collections;

public class CheckpointsManager : MonoBehaviour
{

    private bool isCheckpointActiveForSelectedCar = false;

    //-----MONO-----

    void Start()
    {
        RecalculateIsCheckpointActive();
    }


    //-----PUBLIC----

    public int GetStartWorldIDForSelectedCar()
    {
        if (isCheckpointActiveForSelectedCar && GameplayManager.CurrentGameMode == null)
        {
            return PrefabReferences.Instance.CarStatsDB.GetCarStats(PlayerPrefsAdapter.SelectedCar).startWorld;
        }
        else
        {
            return 0;
        }
    }

    public bool GetIsCheckpointActiveForSelectedCar()
    {
        return isCheckpointActiveForSelectedCar;
    }

    public bool IsCheckpointTimeLeft()
    {
        return GetCheckpointEnabledTimeSpan().TotalMinutes > 0;
    }

    public void SetCheckpointActivatedTimeNow()
    {
        PlayerPrefsAdapter.CheckpointEnabledDateTime = System.DateTime.Now;
    }

    public System.TimeSpan GetCheckpointEnabledTimeSpan()
    {
        System.TimeSpan span = PlayerPrefsAdapter.CheckpointEnabledDateTime - System.DateTime.Now;
        span = span.Add(System.TimeSpan.FromMinutes(GTMParameters.CheckpointDuration));
        return span;
    }

    public bool GetIsCheckpointActivated(CarDefinitions.CarEnum carEnum)
    {
        return System.Convert.ToBoolean(PlayerPrefs.GetInt(GetPrefsName(carEnum)));
    }

    public void SetIsCheckpointActivated(CarDefinitions.CarEnum carEnum, bool value)
    {
        PlayerPrefs.SetInt(GetPrefsName(carEnum), System.Convert.ToInt32(value));
    }

    public void RecalculateIsCheckpointActive()
    {
        isCheckpointActiveForSelectedCar = GetIsCheckpointActivated(PlayerPrefsAdapter.SelectedCar) && (IsCheckpointTimeLeft() || VIP.IsVIPAccount);
    }

    //----PRIVATE------

    private string GetPrefsName(CarDefinitions.CarEnum carEnum)
    {
        return "IS_CHECKPOINT_ACTIVATED-" + carEnum.ToString();
    }
}
