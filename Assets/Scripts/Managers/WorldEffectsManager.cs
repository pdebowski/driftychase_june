using UnityEngine;
using System.Collections;
using System.Linq;

public class WorldEffectsManager : Singleton<WorldEffectsManager> 
{

    CarConfig carConfig;
    ParticleColorConfig particleConfig;
	PsychodelicConfig psychodelicConfig;
    ArrowsConfig arrowConfig;

    protected override void Awakened()
    {
        base.Awakened();
        carConfig=GetComponentInChildren<CarConfig>();
        particleConfig = GetComponentInChildren<ParticleColorConfig>();
		psychodelicConfig = GetComponentInChildren<PsychodelicConfig> ();
        arrowConfig = GetComponentInChildren<ArrowsConfig>();
    }

    public Material[] GetMaterialsForCar(GameObject carObject)
    {
        var retData = carConfig.GetMaterialsForCar(carObject.name, WorldOrder.CurrentWorld);
        return new Material[] { retData.material1, retData.material2, retData.material3 }.Where(v => v != null).ToArray();
    }

    public Color GetParticleColor()
    {
        return particleConfig.GetColorForParticle(WorldOrder.CurrentWorld);
    }

	public PsychodelicConfig.ConfigVar GetPsychodelicConfig()
	{
		return psychodelicConfig.GetPsychodelicConfig ();
	}

	public Vector2 GetBuildingOffsetSpeed()
	{
		return psychodelicConfig.GetBuidingOffsetSpeed ();
	}

    public Color GetArrowColor()
    {
        return arrowConfig.GetArrowsConfig(WorldOrder.CurrentWorld).ArrowColor;
    }

    public Color GetBarColor()
    {
        return arrowConfig.GetArrowsConfig(WorldOrder.CurrentWorld).BarColor;
    }

}
