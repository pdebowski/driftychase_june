﻿using UnityEngine;
using System.Collections;

public class CrimsonLeaderboardsDestroyer : MonoBehaviour
{
    private void Awake()
    {
        if (!GTMParameters.CustomLeaderboardsEnabled && (gameObject != null && !gameObject.Equals(null)))
        {
            Destroy(gameObject);
        }
    }
}
