using UnityEngine;
using System.Collections;
using UnityEngine.iOS;
using System.Linq;
using System.Collections.Generic;

public class QualityManager : MonoBehaviour {
	
	public static bool isLowendDevice { get; private set; }
	// Use this for initialization
	void Awake () {
#if UNITY_IOS && !UNITY_EDITOR
        SetQualityIOS();
#elif UNITY_TVOS
        SetQualityTV();
#endif
    }



#if UNITY_IOS
    void SetQualityIOS()
    {
        Application.targetFrameRate = 60;

        List<DeviceGeneration> lowEndDevices = new List<DeviceGeneration>()
        {
            DeviceGeneration.iPad1Gen,
            DeviceGeneration.iPad2Gen,
            DeviceGeneration.iPadMini1Gen,
            DeviceGeneration.iPhone3G,
            DeviceGeneration.iPhone3GS,
            DeviceGeneration.iPhone4,
            DeviceGeneration.iPhone4S,
        };

        if (lowEndDevices.Any(v => v == Device.generation))
        {
			isLowendDevice = true;
            Debug.Log("QUALITY: Low End");
            int index = -1;
            for (int i = 0; i < QualitySettings.names.Length; i++)
            {
                if (QualitySettings.names[i].Equals("LowEnd"))
                {
                    index = i;
                }
            }

            if (index != -1)
            {
                QualitySettings.SetQualityLevel(index);
            }
            else
            {
                Debug.LogError("QUALITY error Index not found!!!!!!!!!!!!!!!!");
            }
        }
        else
        {
            Debug.Log("QUALITY: HighRes");

            isLowendDevice = false;
            Debug.Log("QUALITY: High End");
            int index = -1;
            for (int i = 0; i < QualitySettings.names.Length; i++)
            {
                if (QualitySettings.names[i].Equals("HighEnd"))
                {
                    index = i;
                }
            }

            if (index != -1)
            {
                QualitySettings.SetQualityLevel(index);
            }
            else
            {
                Debug.LogError("QUALITY error Index HighEnd not found!!!!!!!!!!!!!!!!");
            }

        }

    }
#endif

    void SetQualityTV()
    {
        Application.targetFrameRate = 60;

    }


}
