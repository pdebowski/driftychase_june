using UnityEngine;
using System.Collections;

public class GameplayManager : MonoBehaviour
{

    public static System.Action OnSessionStarted = delegate { };
    public static System.Action OnSessionEnded = delegate { };
    public static System.Action OnSessionPaused = delegate { };
    public static System.Action OnPlayerCrash = delegate { };
    public static System.Action<ResultSceneType> OnContinueResultScreenLoading = delegate { };
    public static System.Action OnContinueUsed = delegate { };
    public static System.Action OnSessionEndedFromPause = delegate { };
    public static System.Action OnCheckpointReached = delegate { };
    public static System.Action OnSafehouseReached = delegate { };
    public static System.Action OnSceneAfterGameLoaded = delegate { };

    public static int CurrentLevel { get; set; }
    public static GameMode CurrentGameMode { get; set; }

    void Awake()
    {
        CurrentGameMode = null;
    }

    void OnEnable()
    {
        OnPlayerCrash += OnPlayerCrashAction;
        OnSessionEnded += OnSessionEndedAction;
        OnContinueUsed += OnContinueUsedAction;
        OnSessionStarted += OnSessionStartedAction;
    }

    void OnDisable()
    {
        OnPlayerCrash -= OnPlayerCrashAction;
        OnSessionEnded -= OnSessionEndedAction;
        OnContinueUsed -= OnContinueUsedAction;
        OnSessionStarted -= OnSessionStartedAction;
    }

    public void Restart()
    {
        OnPlayerCrash();
    }

    private void OnSessionStartedAction()
    {
        PrefabReferences.Instance.AdAdapter.HideBanner();
    }

    private void OnPlayerCrashAction()
    {

    }

    private void OnContinueUsedAction()
    {

    }

    private void OnSessionEndedAction()
    {
    }

    void OnSessionPause()
    {

    }


}

public enum ResultSceneType
{
    Result,
    Continue
}

public enum GameplayMode
{
    NormalLevels
}
