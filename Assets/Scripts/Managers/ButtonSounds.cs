﻿using UnityEngine;
using System.Collections;

public class ButtonSounds : MonoBehaviour
{

    [SerializeField]
    private AudioClip[] audioClips;
    [SerializeField]
    private AudioSource audioSource;

    public void Play()
    {
        audioSource.clip = audioClips.RandomElement();
        SoundManager.PlaySFX(audioSource);
    }
}
