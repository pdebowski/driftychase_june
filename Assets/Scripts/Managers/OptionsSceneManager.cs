using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class OptionsSceneManager : MonoBehaviour
{
    public static System.Action OnEnter = delegate { };
    public static System.Action OnMusicStateChanged = delegate { };

    [SerializeField]
    private Toggle SFXToggle;
    [SerializeField]
    private Toggle MusicToggle;
    [SerializeField]
    private GameObject content;
    [SerializeField]
    private GameObject restorePurchasesButton;
    [SerializeField]
    private GameObject loadingObject;
    [SerializeField]
    private GameObject EULA;

    void Awake()
    {
        SFXToggle.isOn = PlayerPrefsAdapter.SFX;
        MusicToggle.isOn = PlayerPrefsAdapter.MUSIC;

        restorePurchasesButton.SetActive(PlatformRecognition.IsAppleDevice);
        EULA.SetActive(PlatformRecognition.IsAndroidDevice);

        OnEnter();
    }


	public void OnBackButtonClick()
    {
        GlobalFader.Instance.Fade(null, OnFullyBlackExitFade, OnExitFadeEnd);
    }

    private void OnFullyBlackExitFade()
    {
        StartScreenKeysManager.isStartKeysAnimAvailable = false;
        content.SetActive(false);
        if (GameManager.Instance.CurrentState == GameManager.GameState.InMenus)
        {
            SceneLoader.Instance.LoadSceneAdditive("Scenes/StartGameScene");
        }
        else
        {
            SceneLoader.Instance.LoadSceneAdditive("Scenes/PauseScene");
        }
    }

    private void OnExitFadeEnd()
    {
        UnloadOptionsScene();
    }

    private void UnloadOptionsScene()
    {
        SceneLoader.Instance.UnloadScene("Scenes/OptionsScene");
    }

    public void OnSFXToggleClick(bool value)
    {
        PlayerPrefsAdapter.SFX = SFXToggle.isOn;
    }

    public void OnMusicToggleClick(bool value)
    {
        PlayerPrefsAdapter.MUSIC = MusicToggle.isOn;
        OnMusicStateChanged();
    }

    public void OnRestorePurchasesButtonClick()
    {
        PrefabReferences.Instance.IAPManager.SetLoadingObject(loadingObject);
        PrefabReferences.Instance.IAPManager.RestorePurchases();
    }

    public void OnEULAButtonClick()
    {
        Application.OpenURL(EULAManager.EulaUrl);
    }
}
