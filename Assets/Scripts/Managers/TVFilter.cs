﻿using UnityEngine;
using System.Collections;

public class TVFilter
{


    public static string GetRealName(string normalSceneName)
    {
        if (PlatformRecognition.IsTV)
        {
            normalSceneName += "_TV";
        }
        return normalSceneName;
    }
}
