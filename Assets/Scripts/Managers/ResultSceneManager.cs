﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ResultSceneManager : MonoBehaviour
{

    public static System.Action OnEnter = delegate { };

    [SerializeField]
    private Text coins;
    [SerializeField]
    private Text score;
    [SerializeField]
    private GameObject content;

	void Start()
    {
        RefreshStats();
    }

    void OnEnable()
    {
        InitScene();
    }

    protected virtual void InitScene()
    {
        OnEnter();
        SaveCloud();
    }

    private void SaveCloud()
    {
        ScoreCounter.Instance.SetRecordAndTotalDistance(false, ScoreCounter.Instance.CurrentScore - ScoreCounter.Instance.ContinueScore);
        PrefsDataFacade.Instance.AddTotalCash(ScoreCounter.Instance.SessionCash);
        PrefsDataFacade.Instance.SaveGameStateCloud();
        PrefsDataFacade.Instance.AddTotalCash(-ScoreCounter.Instance.SessionCash);
    }

    public void OnNextButtonClick()
    {
        GlobalFader.Instance.FadeIn(null, OnFullyBlackExitFade, 0.5f, 0);
    }

    private void OnFullyBlackExitFade()
    {
        GameManager.Instance.SetGameState(GameManager.GameState.InMenus);
        content.SetActive(false);
        SceneLoader.Instance.LoadSceneAdditive("Scenes/OffersScene");
        UnloadResultScene();
    }

    protected virtual void UnloadResultScene()
    {
        string sceneName = SceneNames.GetRealSceneName_LevelFilter("ResultScene");
        SceneLoader.Instance.UnloadScene(sceneName);
    }

    public void RefreshStats()
    {
        score.text = "# " + ScoreCounter.Instance.CurrentScore.ToString();
        coins.text = "&   " + ScoreCounter.Instance.SessionCash.ToString();
    }
}