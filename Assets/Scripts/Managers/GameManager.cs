using UnityEngine;
using System.Collections;

public class GameManager : Singleton<GameManager>
{


    public static System.Action<GameState> OnGameStateChanged = delegate { };
    public static System.Action OnGameExit = delegate { };

    public byte[] AESKey;
    public float LastTimeScale { get; set; }



    private GameState currentState = GameState.InMenus;

    public GameState CurrentState
    {
        get { return currentState; }
        set
        {
            if (currentState != value)
            {
                currentState = value;
                OnGameStateChanged(currentState);
            }
        }
    }

    protected override void Awakened()
    {
        if (GoogleTagManagerPlugin.Instance.GetBool("DeathPill"))
        {
            Application.Quit();
        }

        OnGameExit += OnGameExitAction;
    }

    void Start()
    {
        CrimsonAnalytics.CA.StartSession();
        CrimsonAnalytics.CA.DispatchHits();
    }

    public void SetTimeScale(float timeScale)
    {
        LastTimeScale = Time.timeScale;
        Time.timeScale = timeScale;
    }

    public void SetGameState(GameManager.GameState gameState)
    {
        CurrentState = gameState;
    }

    private void OnGameExitAction()
    {
        Application.Quit();
    }

    public enum GameState
    {
        Playing,
        InMenus,
        Pause,
    }


}
