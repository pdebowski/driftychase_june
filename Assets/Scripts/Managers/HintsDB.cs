﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class HintsDB : MonoBehaviour
{

    [SerializeField]
    private Hint[] hints;

    [SerializeField]
    private Hint[] iosHints;

    [SerializeField]
    private Hint[] androidHints;

    public string GetHint()
    {
        List<Hint> platformHints = new List<Hint>();
        platformHints.AddRange(hints);

        if (PlatformRecognition.IsAndroidDevice)
        {
            platformHints.AddRange(androidHints);
        }
        else
        {
            platformHints.AddRange(iosHints);
        }

        return platformHints.RandomElement().hintText;
    }
}

[System.Serializable]
public class Hint
{
    [TextArea(1, 4)]
    public string hintText;
}