using UnityEngine;
using System.Collections;

public class DailyGiftManager : MonoBehaviour
{


    public static System.Action PrepareDailyGiftAnimation = delegate { };
    public static System.Action HideDailyGiftAnimation = delegate { };

    public static bool IsDailyGiftActive { get; set; }

    [SerializeField]
    private bool debugOverride;
    [SerializeField]
    private float[] nextGiftTimeInMinutes;

    private System.DateTime lastGiftCollectedTime;

    private int currentTimeIndex = 0;
    private float CurrentTimeToGift { get { return nextGiftTimeInMinutes[currentTimeIndex]; } }



    private void RefreshNextTime()
    {
        currentTimeIndex = Mathf.Clamp(PlayerPrefsAdapter.FreeGiftsCollected, 0, nextGiftTimeInMinutes.Length - 1);
        CrimsonPlugins.LocalNotificationAdapter.Instance.ScheduleReminder(GetTimeToNextInSeconds());
    }

    void OnEnable()
    {
        GameplayManager.OnSessionStarted += TryTurnOnDailyGift;
        GameplayManager.OnSessionEnded += OnSessionEnded;
        UpdateRemoteSettings();

        lastGiftCollectedTime = PlayerPrefsAdapter.DailyGiftCollectedTime;
        RefreshNextTime();
    }

    void OnDisable()
    {
        GameplayManager.OnSessionStarted -= TryTurnOnDailyGift;
        GameplayManager.OnSessionEnded -= OnSessionEnded;
    }

    private void TryTurnOnDailyGift()
    {
        if (IsAvailable())
        {
            GiftCollected();
            PlayerPrefsAdapter.FreeGiftsCollected++;
            RefreshNextTime();
            PrefabReferences.Instance.Player.GetComponent<PlayerController>().BlockInput = true;
            IsDailyGiftActive = true;
            PrepareDailyGiftAnimation();
            CrimsonAnalytics.Offers.FreeGift.Collected.LogEvent();
        }
    }

    private void OnSessionEnded()
    {
        PrefabReferences.Instance.Player.GetComponent<PlayerController>().BlockInput = false;
        HideDailyGiftAnimation();
    }

    public bool IsAvailable()
    {
        return (System.DateTime.UtcNow - lastGiftCollectedTime).TotalMinutes >= CurrentTimeToGift && GameplayManager.CurrentGameMode == null;
    }

    public float GetTimeToNextInSeconds()
    {
        System.TimeSpan span = System.DateTime.UtcNow - lastGiftCollectedTime;
        System.TimeSpan time = System.TimeSpan.FromMinutes(CurrentTimeToGift);
        return (float)(time - span).TotalSeconds;
    }

    public string GetTimeLeft()
    {
        string text;
        System.TimeSpan span = lastGiftCollectedTime - System.DateTime.UtcNow;

        span = span.Add(System.TimeSpan.FromMinutes(CurrentTimeToGift));

        text = (span.Hours) + ":" + string.Format("{0:00}", span.Minutes) + ":" + string.Format("{0:00}", span.Seconds);

        return text;
    }

    private void GiftCollected()
    {
        System.DateTime newTime = System.DateTime.UtcNow;
        newTime = newTime.AddMilliseconds(-newTime.Millisecond);
        PlayerPrefsAdapter.DailyGiftCollectedTime = newTime;

#if UNITY_TVOS
        System.DateTime nextGiftTime = System.DateTime.UtcNow + System.TimeSpan.FromMinutes(CurrentTimeToGift);
        nextGiftTime = nextGiftTime.AddMilliseconds(-nextGiftTime.Millisecond);
        PrefsDataFacade.Instance.SetTVOSNextFreeGiftTime(nextGiftTime.ToString());
#endif

        lastGiftCollectedTime = PlayerPrefsAdapter.DailyGiftCollectedTime;

        int giftValue = PrefabReferences.Instance.CarStatsDB.GetBestOwnedCar().freeGiftValue;

        CrimsonAnalytics.Economy.CurrencySources.FreeGift.LogEvent(giftValue);
        ScoreCounter.Instance.CashFromFreeGift = giftValue;

    }

    void UpdateRemoteSettings()
    {
        if (Debug.isDebugBuild && debugOverride == true)
        {
            return;
        }

        RemoteDailyGiftFrequencies dailyGiftFrequencies = GTMParameters.DailyGiftFrequencies;
        if (dailyGiftFrequencies != null)
        {
            nextGiftTimeInMinutes = dailyGiftFrequencies.GetFrequencies();
        }
        else
        {
            CrimsonError.LogError("Couldn't obtain RemoteDailyGiftFrequencies");
        }
    }
}
