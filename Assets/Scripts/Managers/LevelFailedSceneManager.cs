﻿using UnityEngine;
using System.Collections;

public class LevelFailedSceneManager : ResultSceneManager
{

    protected override void InitScene()
    { 
        int currentLevel = GameplayManager.CurrentLevel;
        int levelLenght = PrefabReferences.Instance.LevelModeController.GetLevelLenght(GameplayManager.CurrentLevel);
        int currentScore = ScoreCounter.Instance.CurrentScore;
        float progress = Mathf.Clamp01((float)currentScore / levelLenght);

        GameplayManager.CurrentGameMode.SetLevelProgress(currentLevel, progress);

        base.InitScene();
    }

    public void OnRetryButtonClick()
    {
        OffersSceneManager.wasChoosedRetry = true;
        base.OnNextButtonClick();
    }
}
