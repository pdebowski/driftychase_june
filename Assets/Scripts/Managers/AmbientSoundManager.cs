﻿using UnityEngine;
using System.Collections;

public class AmbientSoundManager : MonoBehaviour
{

    [SerializeField]
    private MenuRandomSound[] sounds;

    void OnEnable()
    {
        GameplayManager.OnSessionStarted += TurnOff;
        GameplayManager.OnSessionEnded += TurnOn;
    }

    void OnDisable()
    {
        GameplayManager.OnSessionStarted -= TurnOff;
        GameplayManager.OnSessionEnded -= TurnOn;
    }

    private void TurnOn()
    {
        foreach (var item in sounds)
        {
            item.gameObject.SetActive(true);
        }
    }

    private void TurnOff()
    {
        foreach (var item in sounds)
        {
            item.PreparingToTurnOff = true;
        }
        StartCoroutine(TurnOffCor());
    }

    private IEnumerator TurnOffCor()
    {
        yield return new WaitForSeconds(3);
        foreach (var item in sounds)
        {
            item.gameObject.SetActive(false);
        }
    }


}
