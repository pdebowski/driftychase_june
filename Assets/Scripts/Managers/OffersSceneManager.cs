using UnityEngine;
using System.Collections;

public class OffersSceneManager : MonoBehaviour
{


    public static System.Action OnEnter = delegate { };
    public static int OffersDisplayedCount = 1;
    public static bool wasChoosedRetry = false;

    public bool isFakeOffer = false;

    [SerializeField]
    private Vector2 noOffersAmmountAfterShowed = new Vector2(1, 3);
    [SerializeField]
    private OffersManager offersManager;
    [SerializeField]
    private GameObject content;
    [SerializeField]
    private ButtonSounds buttonSounds;

    [SerializeField]
    private Camera[] cameras;

    void Awake()
    {
        OnEnter();
        if (offersManager.IsAnyOfferReady())
        {
            if (isFakeOffer)
            {
                Skip();
            }
            else
            {
                GlobalFader.Instance.FadeOut(ShowOffer, null, 0.3f, 0.0f);
            }
        }
        else
        {
            Skip();
        }
    }

    void OnDisable()
    {
        wasChoosedRetry = false;
        OffersDisplayedCount++;
    }

    private void ShowOffer()
    {
        PlayerPrefsAdapter.RequiredNoOffersAmount = Mathf.RoundToInt(Random.Range(noOffersAmmountAfterShowed.x, noOffersAmmountAfterShowed.y));
        StartCoroutine(ShowOfferInNextFrame());
    }

    private IEnumerator ShowOfferInNextFrame()
    {
        yield return null;
        offersManager.ShowOffer();
        content.SetActive(true);
    }

    public void InitExitOfferButton(Offer offer)
    {
        offer.ExitButton.onClick.AddListener(buttonSounds.Play);
        offer.ExitButton.onClick.AddListener(OnNextButtonClick);
    }

    public void OnNextButtonClick()
    {
        GlobalFader.Instance.FadeIn(null, OnFullyBlackExitFade, 0.25f, 0);
    }

    private void Skip()
    {
        OnFullyBlackExitFade();
    }

    private void OnFullyBlackExitFade()
    {
        if (Time.timeScale < 0.5f) { return; }

        bool showInterstitial = GameplayInterstitialAd.IntestistialPercentProb > GTMParameters.InterstitialAfterStartFrequency ? true : false;


        if (showInterstitial && !VIP.IsVIPAccount && PrefabReferences.Instance.AdAdapter.IsInterstitialReady())
        {
            PrefabReferences.Instance.AdAdapter.OnIntersistialClosed += OnIntersistialClosed;
            if (PrefabReferences.Instance.AdAdapter.ShowInterstitial())
            {
                Time.timeScale = 0;
                return;
            }
            else
            {
                PrefabReferences.Instance.AdAdapter.OnIntersistialClosed -= OnIntersistialClosed;
                FinishSession();
                return;
            }
        }
        else
        {
            FinishSession();
        }
    }

    void OnIntersistialClosed()
    {
        PrefabReferences.Instance.AdAdapter.OnIntersistialClosed -= OnIntersistialClosed;
        Time.timeScale = 1;
        FinishSession();
    }

    private void FinishSession()
    {
        StartScreenKeysManager.isStartKeysAnimAvailable = true;
        content.SetActive(false);

        if (PlayerPrefsAdapter.ForceVIPOffer == false)
        {
            GameplayManager.OnSessionEnded();
        }
        PlayerPrefsAdapter.ForceVIPOffer = false;


        if (!wasChoosedRetry)
        {
            SceneLoader.Instance.LoadSceneAdditive("Scenes/StartGameScene");
            GlobalFader.Instance.FadeOut(null, OnExitFadeEnd, 0.25f, 0);
        }
        else
        {
            PrefabReferences.Instance.GameSceneManager.SetActiveHUD(true);
            GlobalFader.Instance.FadeOut(null, OnExitFadeEnd, 0.5f, 0);

            foreach (var item in cameras)
            {
                item.depth = -1;
            }
        }
    }

    private void OnExitFadeEnd()
    {
        if (wasChoosedRetry)
        {
            GameplayManager.OnSessionStarted();
            GameManager.Instance.CurrentState = GameManager.GameState.Playing;
        }

        UnloadOffersScene();
    }

    private void ReloadToStartScene()
    {
        StartScreenKeysManager.isStartKeysAnimAvailable = true;
        SceneLoader.Instance.LoadSceneAdditive("Scenes/StartGameScene");
        GameplayManager.OnSessionEnded();
        UnloadOffersScene();
    }

    private void UnloadOffersScene()
    {
        SceneLoader.Instance.UnloadScene("Scenes/OffersScene");
    }
}
