using UnityEngine;
using System.Collections;
using CrimsonPlugins;

public abstract class SaveLoadManager<T> : Singleton<T> where T : MonoBehaviour  {

    // Use this for initialization
    protected override void Awakened()
    {
        base.Awakened();
        RankingCloudAdapter.OnCloudInterfaceInitialized += OnCloudInterfaceInitialized;
        RankingCloudAdapter.OnSavedGameSuccessfullyLoaded += OnSavedGameSuccessfullyLoaded;
        RankingCloudAdapter.OnSavedGameLoadFailed += OnSavedGameLoadFailed;
        RankingCloudAdapter.OnSavingGameFinished += OnSavingGameFinished;
        RankingCloudAdapter.OnIOSCloudSaveExternallyUpdated += OnIOSCloudSaveExternallyUpdated;
    }

    protected void SaveGameState(string gameStateToSave)
    {
        if (RankingCloudAdapter.Instance.p_saveGameInstance.p_IsAuthenticated)
        {
            RankingCloudAdapter.Instance.p_saveGameInstance.SaveGame(gameStateToSave, ResolveConflict);
        }
    }

    protected void LoadGameState()
    {
        if (RankingCloudAdapter.Instance.p_saveGameInstance.p_IsAuthenticated)
        {
			PlayerPrefsAdapter.NextCloudLoadTime = System.DateTime.UtcNow.AddSeconds(60 * 15);
            RankingCloudAdapter.Instance.p_saveGameInstance.StartLoadingSavedGames();
		} 
    }

    string ResolveConflict(string newerSnapshot, string olderSnapshot)
    {
        return ResolveCloudConflict(newerSnapshot,olderSnapshot);
    }

    void GameStateLoaded(string gameState)
    {
        if (!string.IsNullOrEmpty(gameState))
        {
            GameStateLoadedFromCloud(gameState);
        }
        else
        {
            DL.LogWarning("CLOUD: Loaded gamestate is empty or null");
        }
    }

    protected abstract void GameStateLoadedFromCloud(string gameState);
    protected abstract string ResolveCloudConflict(string newerSnapshot, string olderSnapshot); 

    //------------CALLBACKS---------------------
    /*void OnCloudInterfaceInitialized(bool isInitialized)
    {
        if (isInitialized)
        {
            RankingCloudAdapter.Instance.p_saveGameInstance.StartLoadingSavedGames();
        }
    }*/
	

	void OnCloudInterfaceInitialized(bool success)
	{
		if (success)
        {
            LoadGameState();
        }
	}

    void OnSavedGameSuccessfullyLoaded(string gameState)
    {
        DL.Log("Cloud: Game Loaded With success");
        GameStateLoaded(gameState);
        RefundsManager.refundNeedCheck = true;
    }

    void OnSavedGameLoadFailed()
    {
        DL.Log("Cloud: Game Load failed");
    }

    void OnSavingGameFinished(bool withSuccess)
    {
        DL.Log("Cloud: Game Saved with sucess status " + withSuccess);
    }

    void OnIOSCloudSaveExternallyUpdated(string gameState)
    {
        DL.Log("Cloud: IOSCloud Externally updated");
        GameStateLoaded(gameState);
    }

}
