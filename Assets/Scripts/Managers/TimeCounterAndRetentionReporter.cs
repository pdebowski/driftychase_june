﻿using UnityEngine;
using System.Collections;
//using UnityEngine.UI;
//using System.Collections.Generic;

public class TimeCounterAndRetentionReporter : MonoBehaviour
{


    private const string TIME_COUNTER_LAST_TIME_ONLINE = "TIME_COUNTER_LAST_TIME_ONLINE";
    private const string TIME_COUNTER_LAST_TIME_REPORTED_RETENTION = "TIME_COUNTER_LAST_TIME_REPORTED_RETENTION";
    private const string SECONDS_SPENT_IN_GAME_IN_DAY = "SECONDS_SPENT_IN_GAME_IN_DAY";
    private const int TIMEOUT_SESSION = 2; // in minutes
    private const int RETENTION_ENGAGEMENT_STOP_MAX_VALUE = 30;

    private System.DateTime LastTimeOnline
    {
        get { return System.DateTime.Parse(PlayerPrefs.GetString(TIME_COUNTER_LAST_TIME_ONLINE, System.DateTime.Now.ToString())); }
        set { PlayerPrefs.SetString(TIME_COUNTER_LAST_TIME_ONLINE, value.ToString()); }
    }

    private System.DateTime LastTimeReportedRetention
    {
        get { return System.DateTime.Parse(PlayerPrefs.GetString(TIME_COUNTER_LAST_TIME_REPORTED_RETENTION, new System.DateTime(2016, 1, 1).ToString())); }
        set { PlayerPrefs.SetString(TIME_COUNTER_LAST_TIME_REPORTED_RETENTION, value.ToString()); }
    }

    //----------------------------------------------------------

    void Start()
    {
        PlayerPrefsAdapter.InstallTime = System.DateTime.Now;
        StartCoroutine(CounterCoroutine());
        ReportRetention();
    }

    void OnApplicationFocus(bool focused)
    {
        if (focused)
        {
            System.TimeSpan timeSpan = System.DateTime.Now - LastTimeOnline;

            if (timeSpan.TotalMinutes >= TIMEOUT_SESSION)
            {
                LastTimeOnline = System.DateTime.Now;
                GAHelper.Instance.ReportOnceASession();
            }
        }

    }

    void ReportRetention()
    {

        System.TimeSpan lastTimeReportedSpan = LastTimeReportedRetention - PlayerPrefsAdapter.InstallTime;
        System.TimeSpan timeSpan = System.DateTime.Now - PlayerPrefsAdapter.InstallTime;

        int[] days = { 0, 1, 2, 3, 5, 7, 10, 14 };

        foreach (var day in days)
        {
            if (lastTimeReportedSpan.Days < day && timeSpan.Days >= day)
            {
                GAHelper.Instance.ReportRetention(day);
            }
        }

        LastTimeReportedRetention = System.DateTime.Now;
    }

    IEnumerator CounterCoroutine()
    {
        while (true)
        {           
            yield return new WaitForSeconds(1);
            LastTimeOnline = System.DateTime.Now;
            int retentionDay = GetRetentionDay();

            PlayerPrefsAdapter.SessionTime += 1;
            PlayerPrefsAdapter.TotalTimeInApp += System.TimeSpan.FromSeconds(1);

            if (GameManager.Instance.CurrentState == GameManager.GameState.Playing)
            {
                PlayerPrefsAdapter.TotalGameplayTime += System.TimeSpan.FromSeconds(1);
                if(retentionDay <= RETENTION_ENGAGEMENT_STOP_MAX_VALUE)
                {
                    AddSecondsSpentInGameInDay(retentionDay, 1);
                }               
            }

            if (retentionDay > PlayerPrefsAdapter.MaxRetentionDay)
            {
                for (int i = PlayerPrefsAdapter.MaxRetentionDay; i < retentionDay; i++)
                {
                    if(i <= RETENTION_ENGAGEMENT_STOP_MAX_VALUE)
                    {
                        ReportSecondsSpentInGameInDay(i);
                    }                  
                }

                PlayerPrefsAdapter.MaxRetentionDay = retentionDay;
            }
        }
    }

    private void ReportSecondsSpentInGameInDay(int day)
    {
        CrimsonAnalytics.PlayerInfo.Engagement.Separate.LogEvent(day, GetSecondsSpentInGameInDay(day));
        CrimsonAnalytics.PlayerInfo.Engagement.Cumulative.LogEvent(day, GetSumSecondsSpentInGameInAllDays(day));
    }

    private int GetSumSecondsSpentInGameInAllDays(int maxDay)
    {
        int sum = 0;

        for (int i = 0; i <= maxDay; i++)
        {
            sum += GetSecondsSpentInGameInDay(i);
        }

        return sum;
    }

    private int GetRetentionDay()
    {
        System.TimeSpan timeSpan = System.DateTime.Now - PlayerPrefsAdapter.InstallTime;
        return timeSpan.Days;
    }

    private void AddSecondsSpentInGameInDay(int day, int seconds)
    {
        PlayerPrefs.SetInt(SECONDS_SPENT_IN_GAME_IN_DAY + "-" + day.ToString(), GetSecondsSpentInGameInDay(day) + seconds);
    }

    private int GetSecondsSpentInGameInDay(int day)
    {
        return PlayerPrefs.GetInt(SECONDS_SPENT_IN_GAME_IN_DAY + "-" + day.ToString(), 0);
    }
}
