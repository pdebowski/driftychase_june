﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class LevelCompleteSceneManager : ResultSceneManager
{

    [SerializeField]
    private GameObject nextLevelButton;

    protected override void InitScene()
    {
        GameplayManager.CurrentGameMode.SetAsCompleted(GameplayManager.CurrentLevel);
        GameplayManager.CurrentGameMode.Unlock(GameplayManager.CurrentLevel + 1);

        base.InitScene();
    }

    public void OnNextLevelButtonClick()
    {
        if (GameplayManager.CurrentLevel >= PrefabReferences.Instance.LevelModeController.GetLevelCount())
        {
            nextLevelButton.SetActive(false);
        }
        else
        {
            GameplayManager.CurrentLevel++;
            OffersSceneManager.wasChoosedRetry = true;
        }

        base.OnNextButtonClick();
    }

    protected override void UnloadResultScene()
    {
        string sceneName = "Scenes/LevelCompleteScene";
        SceneLoader.Instance.UnloadScene(sceneName);
    }
}
