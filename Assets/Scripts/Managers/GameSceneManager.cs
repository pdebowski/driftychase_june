using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameSceneManager : MonoBehaviour
{
    [SerializeField]
    private GameObject HUD;
    [SerializeField]
    private CrashAnimation crashAnimation;
    [SerializeField]
    CountingDown countingDown;
    [SerializeField]
    PlayerController playerController;
    [SerializeField]
    GameplayInterstitialAd gameplayInterstitialAd;

    private bool isContinueAvailable;

    void Awake()
    {
        HUD.SetActive(false);

#if UNITY_EDITOR
        if (!LoadingSceneManager.wasLoadingScreenFired)
        {
            SceneLoader.Instance.LoadSceneAdditive("Scenes/StartGameScene");
        }
#endif
    }

    void OnEnable()
    {
        GameplayManager.OnPlayerCrash += OnPlayerCrash;
        LevelModeController.OnLevelComplete += OnLevelComplete;
        PauseSceneManager.OnBackToGameFromPause += BackToGameAfterPause;
    }

    void OnDisable()
    {
        GameplayManager.OnPlayerCrash -= OnPlayerCrash;
        LevelModeController.OnLevelComplete -= OnLevelComplete;
        PauseSceneManager.OnBackToGameFromPause -= BackToGameAfterPause;
    }

    public void RewardedAdShow()
    {
        PlayerInput.isEnabled = false;
        GameManager.Instance.SetTimeScale(0);
        SoundManager.MuteSound(true);
    }

    public void ClosedRewardedAd(bool fullyWatched)
    {
        PlayerPrefs.SetString("_adsShownTime", System.DateTime.UtcNow.Ticks.ToString());
        PlayerInput.isEnabled = true;
        GameManager.Instance.SetTimeScale(GameManager.Instance.LastTimeScale);
        SoundManager.MuteSound(false);
    }

    private void OnPlayerCrash()
    {
        GameManager.Instance.CurrentState = GameManager.GameState.InMenus;
    }

    private void OnLevelComplete()
    {
        crashAnimation.enabled = true;
        crashAnimation.OnAnimationEnd += OnEndCrashAnimationAfterLevel;
    }

    private void OnEndCrashAnimationAfterLevel()
    {
        crashAnimation.enabled = false;
        crashAnimation.OnAnimationEnd -= OnEndCrashAnimationAfterLevel;
        GameManager.Instance.CurrentState = GameManager.GameState.InMenus;
        GlobalFader.Instance.Fade(null, ShowLevelCompleteScene, null, 1, 0, 0);
    }

    public void OnPauseButtonClick()
    {
        if (GameManager.Instance.CurrentState != GameManager.GameState.Pause)
        {
            PlayerInput.isEnabled = false;
            GameManager.Instance.SetTimeScale(0);
            GameManager.Instance.SetGameState(GameManager.GameState.Pause);
            SceneLoader.Instance.LoadSceneAdditive("Scenes/PauseScene");
        }
    }

    public void ShowContinueScreen()
    {
        if (RewardAd.IsReady())
        {
            isContinueAvailable = true;
            crashAnimation.enabled = true;
            crashAnimation.OnAnimationEnd += OnEndCrashAnimation;
        }
        else
        {
            ShowResultScreen();
        }
    }

    public void ShowResultScreen()
    {
        isContinueAvailable = false;
        crashAnimation.enabled = true;
        crashAnimation.OnAnimationEnd += OnEndCrashAnimation;
    }

    private void OnEndCrashAnimation()
    {
        crashAnimation.OnAnimationEnd -= OnEndCrashAnimation;


        if (isContinueAvailable)
        {
            GlobalFader.Instance.Fade(OnStartingContinueResultFade, LoadContinueScene, null, 0.5f, 1, 0.0f);
        }
        else
        {
            GlobalFader.Instance.Fade(OnStartingContinueResultFade, ShowResultScene, null, 0.5f, 1, 0.0f);
        }
    }

    private void OnStartingContinueResultFade()
    {
        ResultSceneType type = ResultSceneType.Result;
        if (isContinueAvailable)
        {
            type = ResultSceneType.Continue;
        }

        GameplayManager.OnContinueResultScreenLoading(type);
    }

    private void LoadContinueScene()
    {
        SetActiveHUD(false);
        crashAnimation.enabled = false;
        PrefabReferences.Instance.Camera.GetComponent<PlayerCamera>().Restart();
        string sceneName = SceneNames.GetRealSceneName_LevelFilter("ContinueScene");
        SceneLoader.Instance.LoadSceneAdditive(sceneName);
        GameplayManager.OnSceneAfterGameLoaded();
    }

    private void ShowResultScene()
    {
        SetActiveHUD(false);
        crashAnimation.enabled = false;
        PrefabReferences.Instance.Camera.GetComponent<PlayerCamera>().Restart();
        GameManager.Instance.SetGameState(GameManager.GameState.InMenus);
        string sceneName = SceneNames.GetRealSceneName_LevelFilter("ResultScene");
        SceneLoader.Instance.LoadSceneAdditive(sceneName);
        GameplayManager.OnSceneAfterGameLoaded();
    }



    public void ShowOffersScreen()
    {
        SetActiveHUD(false);
        SceneLoader.Instance.LoadSceneAdditive("Scenes/OffersScene");
    }

    public void ShowLevelCompleteScene()
    {
        SetActiveHUD(false);
        GameplayManager.OnSceneAfterGameLoaded();
        SceneLoader.Instance.LoadSceneAdditive("Scenes/LevelCompleteScene");
    }

    public void ShowPremiumScreen()
    {
        SetActiveHUD(false);
        SceneLoader.Instance.LoadSceneAdditive("Scenes/PremiumShopScene");
    }





    public void SetActiveHUD(bool active)
    {
        if (HUD != null)
        {
            HUD.SetActive(active);
        }
    }

    void BackToGameAfterPause()
    {

        StartCoroutine(CountingDown());

    }

    private IEnumerator CountingDown()
    {
        countingDown.Run();

        yield return CommonMethods.WaitForUnscaledSeconds(3 * countingDown.GetOneNumberTimeSpan());

        PlayerInput.isEnabled = true;
        GameManager.Instance.SetGameState(GameManager.GameState.Playing);
        GameManager.Instance.SetTimeScale(1);
        playerController.OnContinueCountingDown();
    }

    void OnApplicationPause(bool pauseStatus)
    {
        if (pauseStatus && GameManager.Instance.CurrentState == GameManager.GameState.Playing && !gameplayInterstitialAd.InterstitialWasDisplayed && !Tutorial.IsTutorial && !DailyGiftManager.IsDailyGiftActive)
        {

            OnPauseButtonClick();
        }

        gameplayInterstitialAd.InterstitialWasDisplayed = false;
    }

}