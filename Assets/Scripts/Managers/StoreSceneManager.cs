﻿using UnityEngine;


public class StoreSceneManager : MonoBehaviour
{
    public static System.Action OnEnter = delegate { };

    [SerializeField]
    private GameObject content;
    [SerializeField]
    private StoreManager storeManager;
    [SerializeField]
    private GameObject premiumShopPopup;
    [SerializeField]
    private GameObject carModel;

    void Awake()
    {
        OnEnter();
    }

    public void OnBackButtonClick()
    {
        GlobalFader.Instance.Fade(null, OnFullyBlackExitFade, OnExitFadeEnd);
    }

    private void OnFullyBlackExitFade()
    {
        StartScreenKeysManager.isStartKeysAnimAvailable = false;
        content.SetActive(false);
        storeManager.SetCar();
        SceneLoader.Instance.LoadSceneAdditive("Scenes/StartGameScene");
        PrefabReferences.Instance.Camera.enabled = true;
    }

    private void OnExitFadeEnd()
    {
        UnloadStoreScene();
    }

    private void UnloadStoreScene()
    {
        SceneLoader.Instance.UnloadScene("Scenes/StoreScene");
    }

    public void OnPremiumShopButtonClick()
    {
        GlobalFader.Instance.Fade(null, OnFullyBlackPremiumShopFade, OnExitFadeEnd);
    }

    private void OnFullyBlackPremiumShopFade()
    {
        content.SetActive(false);
        SceneLoader.Instance.LoadSceneAdditive("Scenes/PremiumShopScene");
    }

    public void SetActivePremiumShopPopup(bool active)
    {
        if (carModel != null)
        {
            carModel.SetActive(!active);
        }
        premiumShopPopup.SetActive(active);
    }

    public void OnNoButtonPremiumShopPopupClick()
    {
        SetActivePremiumShopPopup(false);
    }

    public void OnYesButtonPremiumShopPopupClick()
    {
        OnPremiumShopButtonClick();
    }
}