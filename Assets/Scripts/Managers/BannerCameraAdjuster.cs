﻿using UnityEngine;
using System.Collections;

public class BannerCameraAdjuster : MonoBehaviour {

    [SerializeField]
    private Camera screenCamera;
    [SerializeField]
    private bool bannersAllowed = false;

    void OnEnable()
    {
        PrefabReferences.Instance.AdAdapter.OnBannerVisible += AdjustCameraToBanner;

        if(bannersAllowed && PlatformRecognition.IsMobile)
        {
            PrefabReferences.Instance.AdAdapter.ShowBanner();
        }
        else
        {
            PrefabReferences.Instance.AdAdapter.HideBanner();
        }
    }

    void OnDisable()
    {
        PrefabReferences.Instance.AdAdapter.OnBannerVisible -= AdjustCameraToBanner;
    }

    void AdjustCameraToBanner(bool bannerVisible)
    {
        if(screenCamera!= null)
        {
            if (bannerVisible)
            {
                screenCamera.rect = new Rect(0, 0, 1, GlobalParameters.BANNER_CAMERA_HEIGHT);
            }
            else
            {
                screenCamera.rect = new Rect(0, 0, 1, 1);
            }
        }
    }

}
