﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WorldPopups : MonoBehaviour {

    public const string WORLD_POPUP_RESOURCES_PATH = "WorldPopups\\";

    [SerializeField]
    private List<WorldPopupItem> worldPopupItems;

    public string GetWorldPopupResourcePath(WorldType worldType)
    {
        WorldPopupItem searched = worldPopupItems.Find(x => x.worldType == worldType);

        if (searched == null)
        {
            DL.LogError("WorldName. No World Popup");
            return "ERROR";
        }
        else
        {
            return WORLD_POPUP_RESOURCES_PATH + searched.worldPopupResourceName;
        }
    }
}

[System.Serializable]
public class WorldPopupItem : WorldDependent
{
    public string worldPopupResourceName;
}

