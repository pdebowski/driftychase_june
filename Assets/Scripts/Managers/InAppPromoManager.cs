using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;


public class InAppPromoManager : MonoBehaviour
{
    public delegate bool IsProductInPromoDelegate(IAPManager.Products product);
    public delegate void RefreshTimerDelegate(TimeSpan timeLeft, string timeLeftFormated);
    public delegate void RefreshProductDelegate(bool isPromo, IAPManager.Products promoProduct, IsProductInPromoDelegate IsProductInPromo);

    public static RefreshTimerDelegate OnRefreshTimer = delegate { };
    public static RefreshProductDelegate OnRefreshProduct = delegate { };

    private List<IAPManager.Products> availableProducts = new List<IAPManager.Products>();
    private IAPManager.Products promoProduct = IAPManager.Products.FullWallet;
    private TimeSpan timeLeft = new TimeSpan(0);
    private string timeLeftFormated = "00:00:00";
    private int usedDaysSeed = 0;
    private System.Random randomGenerator;

    private const int RANDOM_SEED = 5745;
    private DateTime REFERENCE_DATE_TIME = new DateTime(2016, 11, 1, 0, 0, 0, 0, DateTimeKind.Utc);
    private const IAPManager.Products REFERENCE_PROMO_PRODUCT = IAPManager.Products.FullTruck;

    //---MONO--------------------------//

    void Awake()
    {
        randomGenerator = new System.Random(RANDOM_SEED);
        ApplyAvailableProductsFromGTM();
        SetPromotion();
        StartCoroutine(TimerCor());
    }

    //---PUBLIC--------------------------//

    public bool IsProductInPromo(IAPManager.Products product)
    {
        return IsPromo() && product == promoProduct;
    }

    public void RefreshDependentTimer(RefreshTimerDelegate refreshTimer)
    {
        RefreshTimerDelegateInvoke(refreshTimer);
    }

    public void RefreshDependentProduct(RefreshProductDelegate refreshProduct)
    {
        RefreshProductDelegateInvoke(refreshProduct);
    }

    //---PRIVATE--------------------------//

    private void ApplyAvailableProductsFromGTM()
    {
        availableProducts = new List<IAPManager.Products>();
        string productsCombinedString = GTMParameters.InAppPromoHalfDiscountProducts;

        if (!string.IsNullOrEmpty(productsCombinedString))
        {
            productsCombinedString = productsCombinedString.TrimEnd(';');
            string[] productsString = productsCombinedString.Split(';');

            foreach (var productString in productsString)
            {
                if (Enum.IsDefined(typeof(IAPManager.Products), productString))
                {
                    IAPManager.Products product = (IAPManager.Products)Enum.Parse(typeof(IAPManager.Products), productString);
                    availableProducts.Add(product);
                }
                else
                {
                    Debug.LogWarning("Product from promotion is not available :" + productString);
                }
            }
        }
    }

    private TimeSpan GetActualPromoTimeLeft()
    {
        if (IsPromo())
        {
            TimeSpan span = (System.DateTime.UtcNow - REFERENCE_DATE_TIME);
            span = span.Subtract(new TimeSpan(span.Days, 0, 0, 0));
            TimeSpan timeLeft = new TimeSpan(1, 0, 0, 0);
            timeLeft = timeLeft.Subtract(span);

            return timeLeft;
        }
        else
        {
            return new TimeSpan(0);
        }
    }

    private void SetPromotion()
    {
        if (IsPromo())
        {
            promoProduct = GeneratePromoProduct();
            DL.Log("InAppPromoManager: Promo - " + promoProduct.ToString());
        }
        else
        {
      //      DL.Log("InAppPromoManager: Promo - NoProductsAvailable");
        }

        RefreshProductDelegateInvoke(OnRefreshProduct);
    }

    private IAPManager.Products GeneratePromoProduct()
    {
        IAPManager.Products result = REFERENCE_PROMO_PRODUCT;
        usedDaysSeed = GetDaysSeed();
        randomGenerator = new System.Random(RANDOM_SEED);

        if (availableProducts.Count > 0)
        {
            for (int i = 0; i <= usedDaysSeed; i += GTMParameters.InAppPromoHalfDiscountDaySpan)
            {
                if (i == 0)
                {
                    result = REFERENCE_PROMO_PRODUCT;
                }
                else
                {
                    if (availableProducts.Count > 1)
                    {
                        result = GetRandomProductExcluding(availableProducts, result, i);
                    }
                    else
                    {
                        return availableProducts[0];
                    }
                }
            }

            return result;
        }
        else
        {
            Debug.LogError("NoProductsToGenerateFrom");
            return IAPManager.Products.FullWallet;
        }
    }

    private IAPManager.Products GetRandomProductExcluding(List<IAPManager.Products> list, IAPManager.Products elementToExclude, int seed)
    {
        List<IAPManager.Products> listCopy = new List<IAPManager.Products>(list);
        listCopy.Remove(elementToExclude);
        int index = randomGenerator.Next(0, listCopy.Count);
        return listCopy[index];
    }

    private int GetDaysSeed()
    {
        return (System.DateTime.UtcNow - REFERENCE_DATE_TIME).Days;
    }

    private IEnumerator TimerCor()
    {
        while (true)
        {
            if (usedDaysSeed != GetDaysSeed())
            {
                SetPromotion();
            }

            timeLeft = PrefabReferences.Instance.InAppPromoManager.GetActualPromoTimeLeft();
            timeLeftFormated = string.Format("{0:00}:{1:00}:{2:00}", timeLeft.Hours, timeLeft.Minutes, timeLeft.Seconds);
            RefreshTimerDelegateInvoke(OnRefreshTimer);

            yield return new WaitForSeconds(1);
        }
    }

    private bool IsPromo()
    {
        return availableProducts.Count > 0 && IsDayToShowPromo(GetDaysSeed());
    }

    private static bool IsDayToShowPromo(int day)
    {
        return day % GTMParameters.InAppPromoHalfDiscountDaySpan == 0;
    }

    private void RefreshTimerDelegateInvoke(RefreshTimerDelegate refreshTimerDelegate)
    {
        refreshTimerDelegate(timeLeft, timeLeftFormated);
    }

    private void RefreshProductDelegateInvoke(RefreshProductDelegate refreshProductDelegate)
    {
        refreshProductDelegate(IsPromo(), promoProduct, IsProductInPromo);
    }
}
