﻿using UnityEngine;
using System.Collections;

public class ContinueResultRenderTextureSelector : MonoBehaviour {

    [SerializeField]
    private GameObject defaultVariation;
    [SerializeField]
    private GameObject lowEndVariation;

    void OnEnable()
    {
        if(!QualityManager.isLowendDevice)
        {
            defaultVariation.SetActive(true);
            ScreenshotShare screenshotShare = defaultVariation.GetComponentInChildren<ScreenshotShare>();
            if(screenshotShare != null)
            {
                screenshotShare.Init();
            }
            Destroy(lowEndVariation);
        }
        else
        {
            lowEndVariation.SetActive(true);
            Destroy(defaultVariation);
        }
    }
	
}
