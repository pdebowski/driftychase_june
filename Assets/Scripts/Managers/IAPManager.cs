using UnityEngine;
using System.Collections;
using CrimsonPlugins;
using CrimsonPlugins.Internal.IAP;
using CrimsonPlugins.Internal.IAP.ProductModel;
using System.Linq;

public class IAPManager : MonoBehaviour, IProductEvents
{
    public static System.Action OnPurchased = delegate
    {

    };

    AdAdapter adAdapter;
    private GameObject loadingObject;

    bool restoring = false;

    public enum Products { Pocket, FullWallet, FullCase, FullTruck, WholeSafe, VipAccount, FullWallet_Promo, FullCase_Promo, FullTruck_Promo, WholeSafe_Promo, Tank, Ego, DragRacer, NightRider };

    ProductManager productManager;

    //---------------------------------------
    void Awake()
    {
        productManager = GetComponent<ProductManager>();
    }
    //-----------------------------------
    public void SetLoadingObject(GameObject loadingObject)
    {
        this.loadingObject = loadingObject;
    }

    //---------------------------------------
    void OnEnable()
    {
        foreach (var it in productManager.m_consumable.Concat(productManager.m_nonConsumable))
        {
            productManager.AddObserver(this, it.name);
        }

        productManager.m_productStoreManager.OnConnectedToStoreEvent += OnConnectedToStore;
        productManager.m_productStoreManager.OnPurchaseCancelledEvent += OnPurchaseCancelled;
        productManager.m_productStoreManager.OnPurchaseSuccessfulEvent += OnPurchaseSuccessful;
        productManager.m_productStoreManager.OnRestoreTransactionsFailedEvent += OnRestoreTransactionsFailed;
        productManager.m_productStoreManager.OnRestoreTransactionsFinishedEvent += OnRestoreTransactionsFinished;
        productManager.m_productStoreManager.OnRestoreTransactionsStartedEvent += OnRestoreTransactionStarted;


    }
    //---------------------------------------
    void OnDisable()
    {
        foreach (var it in productManager.m_consumable.Concat(productManager.m_nonConsumable))
        {
            productManager.RemoveObserver(this, it.name);
        }

        productManager.m_productStoreManager.OnConnectedToStoreEvent -= OnConnectedToStore;
        productManager.m_productStoreManager.OnPurchaseCancelledEvent -= OnPurchaseCancelled;
        productManager.m_productStoreManager.OnPurchaseSuccessfulEvent -= OnPurchaseSuccessful;
        productManager.m_productStoreManager.OnRestoreTransactionsFailedEvent -= OnRestoreTransactionsFailed;
        productManager.m_productStoreManager.OnRestoreTransactionsFinishedEvent -= OnRestoreTransactionsFinished;
        productManager.m_productStoreManager.OnRestoreTransactionsStartedEvent -= OnRestoreTransactionStarted;
    }
    //---------------------------------------
    public void Initialize()
    {
        productManager.m_productStoreManager.ConnectToStore();
    }
    //----------------------------------------------
    public void BuyProduct(Products product)
    {
        if (loadingObject != null)
        {
            loadingObject.SetActive(true);
        }

        switch (product)
        {
            case Products.Pocket:
                productManager.RequestPurchase("Pocket");
                break;
            case Products.FullWallet:
                productManager.RequestPurchase("Full Wallet");
                break;
            case Products.FullCase:
                productManager.RequestPurchase("Full Case");
                break;
            case Products.FullTruck:
                productManager.RequestPurchase("Full Truck");
                break;
            case Products.WholeSafe:
                productManager.RequestPurchase("Whole Safe");
                break;
            case Products.FullWallet_Promo:
                productManager.RequestPurchase("Full Wallet Promo");
                break;
            case Products.FullCase_Promo:
                productManager.RequestPurchase("Full Case Promo");
                break;
            case Products.FullTruck_Promo:
                productManager.RequestPurchase("Full Truck Promo");
                break;
            case Products.WholeSafe_Promo:
                productManager.RequestPurchase("Whole Safe Promo");
                break;
            case Products.VipAccount:
                productManager.RequestPurchase("Vip Account");
                break;
            case Products.Tank:
                productManager.RequestPurchase("Tank");
                break;
            case Products.Ego:
                productManager.RequestPurchase("Ego");
                break;
            case Products.DragRacer:
                productManager.RequestPurchase("DragRacer");
                break;
            case Products.NightRider:
                productManager.RequestPurchase("NightRider");
                break;
            default:
                Debug.LogError("Bad produck name for product: " + product);
                break;
        }

    }
    //---------------------------------------
    public void RestorePurchases()
    {
        if (loadingObject != null)
        {
            loadingObject.SetActive(true);
        }

        productManager.RequestRestore();
    }

    //---------------------------------------
    void OnConnectedToStore()
    {
        Debug.Log("OnConnectedToStore");
    }
    //---------------------------------------
    //-------------------------------------------------
    void OnPurchaseCancelled(string reason)
    {
        if (loadingObject)
        {
            loadingObject.SetActive(false);
        }

        //   IAPManager.DebugLog("OnPurchaseCancelled: " + reason);
        //   premiumShop.ShopPopup(PremiumShop.PremiumShopPopups.Cancel);
    }
    //---------------------------------------
    public void OnPurchaseSuccessful(StoreTransactionInfo storeTransactionInfo)
    {
        Debug.Log("OnPurchaseSuccessful productIdentifier:" + storeTransactionInfo.productIdentifier);
        Debug.Log("OnPurchaseSuccessful transactionIdentifier:" + storeTransactionInfo.transactionIdentifier);

        int consumablePrice = 0;

        switch (storeTransactionInfo.productName)
        {
            case "Pocket":
                PrefsDataFacade.Instance.AddTotalCash(10000);
                PrefsDataFacade.Instance.ForceSaveAfterPause();
                consumablePrice = 1;
                break;
            case "Full Wallet":
                PrefsDataFacade.Instance.AddTotalCash(30000);
                PrefsDataFacade.Instance.ForceSaveAfterPause();
                consumablePrice = 2;
                break;
            case "Full Case":
                PrefsDataFacade.Instance.AddTotalCash(100000);
                PrefsDataFacade.Instance.ForceSaveAfterPause();
                consumablePrice = 5;
                break;
            case "Full Truck":
                PrefsDataFacade.Instance.AddTotalCash(300000);
                PrefsDataFacade.Instance.ForceSaveAfterPause();
                consumablePrice = 10;
                break;
            case "Whole Safe":
                PrefsDataFacade.Instance.AddTotalCash(1000000);
                PrefsDataFacade.Instance.ForceSaveAfterPause();
                consumablePrice = 15;
                break;
            case "Full Wallet Promo":
                PrefsDataFacade.Instance.AddTotalCash(30000);
                PrefsDataFacade.Instance.ForceSaveAfterPause();
                consumablePrice = 1;
                break;
            case "Full Case Promo":
                PrefsDataFacade.Instance.AddTotalCash(100000);
                PrefsDataFacade.Instance.ForceSaveAfterPause();
                consumablePrice = 2;
                break;
            case "Full Truck Promo":
                PrefsDataFacade.Instance.AddTotalCash(300000);
                PrefsDataFacade.Instance.ForceSaveAfterPause();
                consumablePrice = 5;
                break;
            case "Whole Safe Promo":
                PrefsDataFacade.Instance.AddTotalCash(1000000);
                PrefsDataFacade.Instance.ForceSaveAfterPause();
                consumablePrice = 7;
                break;
            case "Vip Account":
                VIP.SetAccountAsVIP();
                consumablePrice = 0;
                break;
            case "Tank":
                BuyOrRestoreCar(CarDefinitions.CarEnum.Car13);
                consumablePrice = 0;
                break;
            case "Ego":
                BuyOrRestoreCar(CarDefinitions.CarEnum.Car11);
                consumablePrice = 0;
                break;
            case "DragRacer":
                BuyOrRestoreCar(CarDefinitions.CarEnum.Car15);
                consumablePrice = 0;
                break;
            case "NightRider":
                BuyOrRestoreCar(CarDefinitions.CarEnum.Car16);
                consumablePrice = 0;
                break;
            default:
                Debug.LogError("OnPurchaseSuccess: bad product Name " + storeTransactionInfo.productName);
                break;
        }

        if (loadingObject != null)
        {
            loadingObject.SetActive(false);

        }

        CrimsonAnalytics.Transaction.InApp.Purchased.LogEvent(storeTransactionInfo.productName + " || " + storeTransactionInfo.productIdentifier);
        GAHelper.Instance.ReportDollarsSpent(consumablePrice);
        OnPurchased();
    }

    private void BuyOrRestoreCar(CarDefinitions.CarEnum car)
    {
        if (restoring)
        {
            PrefsDataFacade.Instance.BuyCar(car);
        }
        else
        {
            StoreManager.Instance.CarBoughtFromIAP(car);
        }
    }
    //---------------------------------------
    public void OnRestoreTransactionsFailed(string reason)
    {
        restoring = false;
        if (loadingObject)
        {
            loadingObject.SetActive(false);
        }
    }
    //---------------------------------------
    public void OnRestoreTransactionsFinished()
    {
        restoring = false;
        if (loadingObject)
        {
            loadingObject.SetActive(false);
        }
    }
    //---------------------------------------
    public void OnRestoreTransactionStarted()
    {
        restoring = true;
    }
    //---------------------------------------
    public void OnLeveledMaxLevelAchieved()
    {
    }
    //---------------------------------------------
    public void OnPurchaseFailed(ProductInfo prodInfo, E_FailReason reason)
    {
        if (loadingObject)
        {
            loadingObject.SetActive(false);
        }
    }
    //-------------------------------------------------
    public void OnProductUpdated(ProductInfo prodInfo, E_UpdateReason reason)
    {
    }
}


