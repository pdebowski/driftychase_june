﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class InAppInfoDB : MonoBehaviour {

    [SerializeField]
    private InAppInfoItem defaultItem;
    [SerializeField]
    private List<InAppInfoItem> items; 

    public InAppInfoItem GetInAppInfoItem(IAPManager.Products productType)
    {
        InAppInfoItem searched = items.Find(x => x.productType == productType);

        if(searched== null)
        {
            searched = defaultItem;
        }

        return searched;
    }

}

[System.Serializable]
public class InAppInfoItem
{
    public IAPManager.Products productType;
    public Sprite sprite;
    public string name;
    public string normalPrice;
    public string promoPrice;
    public string offPercentageValue;
    public string visualAmmount;
}
