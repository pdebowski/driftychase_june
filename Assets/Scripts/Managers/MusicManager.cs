﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MusicManager : MonoBehaviour
{
    [SerializeField]
    private bool continuous = false;
    [SerializeField]
    private bool crossfadeNoise = true;
    [SerializeField]
    private AudioLowPassFilter lowPassFilter;
    [SerializeField]
    private List<WorldMusic> worldMusics;
    [SerializeField]
    private AudioSource firstAudioSource;
    [SerializeField]
    private AudioSource secondAudioSource;
    [SerializeField]
    private AudioSource noiseAudioSource;
    [SerializeField]
    private float maxMusicVolume;
    [SerializeField]
    private float maxNoiseVolume;
    [SerializeField]
    private float crossfadeDelay;
    [SerializeField]
    private float crossfadeDuration;
    [SerializeField]
    private AnimationCurve crossfadeCurve;
    [SerializeField]
    private AnimationCurve noiseVolumeCurve;



    private AudioSource currentMainSource;
    private bool isMusicPlaying;

    void Awake()
    {
        currentMainSource = firstAudioSource;
        isMusicPlaying = false;
    }

    void Start()
    {
        PlayDefaultMusic();
    }

    void OnEnable()
    {
        GameplayManager.OnSessionStarted += OnSessionStarted;
        GameplayManager.OnPlayerCrash += OnPlayerCrash;
        GameplayManager.OnContinueUsed += OnContinueUsed;
        PlayerSpeedManager.OnWorldChange += OnWorldChange;
        GameplayManager.OnSessionEnded += OnSessionEnded;
        OptionsSceneManager.OnEnter += OnEnterToOptions;
        StoreSceneManager.OnEnter += OnEnterToStore;
        StartGameSceneManager.OnEnter += OnEnterToMainScene;
        OptionsSceneManager.OnMusicStateChanged += OnMusicStateChanged;
    }

    void OnDisable()
    {
        GameplayManager.OnSessionStarted -= OnSessionStarted;
        GameplayManager.OnPlayerCrash -= OnPlayerCrash;
        GameplayManager.OnContinueUsed -= OnContinueUsed;
        PlayerSpeedManager.OnWorldChange -= OnWorldChange;
        GameplayManager.OnSessionEnded -= OnSessionEnded;
        OptionsSceneManager.OnEnter -= OnEnterToOptions;
        StoreSceneManager.OnEnter -= OnEnterToStore;
        StartGameSceneManager.OnEnter -= OnEnterToMainScene;
        OptionsSceneManager.OnMusicStateChanged -= OnMusicStateChanged;
    }

    private void OnMusicStateChanged()
    {
        if (firstAudioSource.isPlaying)
        {
            firstAudioSource.Pause();
            SoundManager.UnPauseMusic(firstAudioSource);
        }
        else
        {
            SoundManager.PlayMusic(firstAudioSource);
        }
    }

    private void OnEnterToOptions()
    {
        lowPassFilter.enabled = true;
    }

    private void OnEnterToStore()
    {
        lowPassFilter.enabled = true;
    }

    private void OnEnterToMainScene()
    {
        lowPassFilter.enabled = false;
    }

    private void OnSessionStarted()
    {
        CrossfadeMusic();
    }

    private void OnSessionEnded()
    {
        PlayDefaultMusic();
    }

    private void OnPlayerCrash()
    {
        firstAudioSource.Pause();
        secondAudioSource.Pause();
        isMusicPlaying = false;
    }

    private void OnContinueUsed()
    {
        SoundManager.UnPauseMusic(currentMainSource);
        isMusicPlaying = true;
    }

    private void OnWorldChange()
    {
        if (GameManager.Instance.CurrentState == GameManager.GameState.Playing)
        {
            CrossfadeMusic();
        }
    }

    private void CrossfadeMusic()
    {
        if (isMusicPlaying)
        {
            AudioSource from = currentMainSource;
            AudioSource to = currentMainSource == firstAudioSource ? secondAudioSource : firstAudioSource;
            currentMainSource = to;
            StartCoroutine(CrossfadeMusics(from, to));
        }
    }

    private void PlayDefaultMusic()
    {
        firstAudioSource.clip = GetWorldMusic(WorldType.NewYork);
        firstAudioSource.volume = maxMusicVolume;
        SoundManager.PlayMusic(firstAudioSource);
        firstAudioSource.time = 0;
        isMusicPlaying = true;
        currentMainSource = firstAudioSource;
    }


    private IEnumerator CrossfadeMusics(AudioSource from, AudioSource to)
    {
        to.clip = GetWorldMusic(WorldOrder.CurrentWorld);
        to.volume = 0;
        SoundManager.PlayMusic(to);
        if (continuous)
        {
            to.time = from.time;
        }



        if (crossfadeNoise)
        {
            SoundManager.PlayMusic(noiseAudioSource);
            noiseAudioSource.volume = 0;
        }



        yield return new WaitForSeconds(crossfadeDelay);
        float phase = 0;
        while (phase < 1)
        {
            phase += Time.deltaTime / crossfadeDuration;
            from.volume = crossfadeCurve.Evaluate(1 - phase) * maxMusicVolume;
            to.volume = crossfadeCurve.Evaluate(phase) * maxMusicVolume;
            noiseAudioSource.volume = noiseVolumeCurve.Evaluate(phase);
            yield return null;
        }
        from.Stop();
        noiseAudioSource.Stop();
    }

    private AudioClip GetWorldMusic(WorldType world)
    {
        WorldMusic worldMusic = worldMusics.Find(x => x.type == world);
        AudioClip clip = null;

        if (worldMusic != null)
        {
            clip = worldMusic.clip;
        }

        return clip;
    }

    public void PauseMusic()
    {
        firstAudioSource.Pause();
    }

    public void UnPauseMusic()
    {
        SoundManager.UnPauseMusic(firstAudioSource);
    }

}

[System.Serializable]
public class WorldMusic
{
    public WorldType type;
    public AudioClip clip;
}
