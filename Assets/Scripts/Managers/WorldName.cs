﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WorldName : MonoBehaviour {

    [SerializeField]
    private List<WorldNameItem> worldNameItems;

    public string GetWorldName(WorldType worldType)
    {
        WorldNameItem searched = worldNameItems.Find(x => x.worldType == worldType);

        if(searched == null)
        {
            DL.LogError("WorldName. No World Name");
            return "NO NAME";
        }
        else
        {
            return searched.worldName;
        }
    }
}

[System.Serializable]
public class WorldNameItem : WorldDependent
{
    [Tooltip("Starting from 1")]
    public string worldName;
}
