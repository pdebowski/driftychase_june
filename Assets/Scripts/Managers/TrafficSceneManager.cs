﻿using UnityEngine;
using System.Collections;

public class TrafficSceneManager : MonoBehaviour {

	private void UnloadTrafficScene()
	{
		SceneLoader.Instance.UnloadScene("Scenes/TrafficScene");
		//Resources.UnloadUnusedAssets();
	}

	public void OnBackButtonClick()
	{
		SceneLoader.OnSceneLoaded += OnSceneLoaded;
		SceneLoader.Instance.LoadSceneAdditive("Scenes/StartGameScene");
	}

	private void OnSceneLoaded(string name)
	{
		UnloadTrafficScene();
		SceneLoader.OnSceneLoaded -= OnSceneLoaded;
	}
}
