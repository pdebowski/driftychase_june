﻿using UnityEngine;
using System.Collections;

public class CashPerMinuteAnalyticsReporter : Singleton<CashPerMinuteAnalyticsReporter>
{

    private const string SECONDS_SPENT_IN_GAME_IN_CAR = "SECONDS_SPENT_IN_GAME_IN_CAR";
    private const string CASH_EARNED_IN_GAME_IN_CAR = "CASH_EARNED_IN_GAME_IN_CAR";

    void Start()
    {
        StartCoroutine(CounterCoroutine());
    }

    void OnEnable()
    {
        StoreManager.OnCarBought += ReportWhenCarBought;
        GameplayManager.OnSessionStarted += ResetOneGameplayTime;
    }

    void OnDisable()
    {
        StoreManager.OnCarBought -= ReportWhenCarBought;
        GameplayManager.OnSessionStarted -= ResetOneGameplayTime;
    }

    IEnumerator CounterCoroutine()
    {
        while (true)
        {
            yield return new WaitForSeconds(1);

            if (GameManager.Instance.CurrentState == GameManager.GameState.Playing)
            {
                PlayerPrefsAdapter.OneGameplayTime += System.TimeSpan.FromSeconds(1);
            }
        }
    }

    private void ReportWhenCarBought(CarDefinitions.CarEnum boughtCarEnum)
    {
        int boughtCarIndex = PrefabReferences.Instance.CarStatsDB.GetCarIndex(boughtCarEnum);
        int bestOwnedCarIndex = PrefabReferences.Instance.CarStatsDB.GetBestOwnedCarIndex();
        bool newBestCarBought = boughtCarIndex == bestOwnedCarIndex;

        if(newBestCarBought)
        {
            int previousBestCarIndex = boughtCarIndex - 1;
            int secondsSpentInGameInCar = GetSecondsSpentInGameInCar(previousBestCarIndex);
            int cashEarnedInGameInCar = GetCashEarnedInGameInCar(previousBestCarIndex);

            if(secondsSpentInGameInCar > 0)
            {
                int cashPerMinute = (cashEarnedInGameInCar * 60) / secondsSpentInGameInCar;
                CrimsonAnalytics.Economy.CashPerMinuteInGame.LogAll(previousBestCarIndex, cashPerMinute, GetBucketLength(previousBestCarIndex));
            }
            else
            {
                CrimsonAnalytics.Economy.CashPerMinuteInGame.CarNotPlayed.LogEvent(previousBestCarIndex);
            }
        }
    }

    public void AddCashPerMinuteData()
    {
        int bestOwnedCarIndex = PrefabReferences.Instance.CarStatsDB.GetBestOwnedCarIndex();
        int selectedCarIndex = PrefabReferences.Instance.CarStatsDB.GetCarIndex(PlayerPrefsAdapter.SelectedCar);

        if(selectedCarIndex == bestOwnedCarIndex)
        {
            int cash = ScoreCounter.Instance.SessionCash - (ScoreCounter.Instance.CashFromFreeGift + ScoreCounter.Instance.CashFromInterstitial);
            AddSecondsSpentInGameInCar(selectedCarIndex, Mathf.RoundToInt((float)PlayerPrefsAdapter.OneGameplayTime.TotalSeconds));
            AddCashEarnedInGameInCar(selectedCarIndex, cash);

            DL.Log("AnalyticsDebug. CashPerMinuteAnalyticsReporter. Car-" + CommonMethods.GetCrimsonAnalyticsOrder(selectedCarIndex + 1));
            DL.Log("AnalyticsDebug. CashPerMinuteAnalyticsReporter. Cash: " + cash);
            DL.Log("AnalyticsDebug. CashPerMinuteAnalyticsReporter. Time: " + PlayerPrefsAdapter.OneGameplayTime.TotalMinutes);
        }
    }

    private void ResetOneGameplayTime()
    {
        PlayerPrefsAdapter.OneGameplayTime = System.TimeSpan.FromHours(0);
    }

    private int GetBucketLength(int carIndex)
    {
        int[] buckets = { 10, 30, 50, 40, 50, 75, 100, 100, 150, 150, 150 };

        if(carIndex >= 0 && carIndex < buckets.Length)
        {
            return buckets[carIndex];
        }
        else
        {
            return buckets[buckets.Length - 1];
        }
    }

    //--------------------------

    private void AddSecondsSpentInGameInCar(int carIndex, int seconds)
    {
        PlayerPrefs.SetInt(SECONDS_SPENT_IN_GAME_IN_CAR + "-" + carIndex.ToString(), GetSecondsSpentInGameInCar(carIndex) + seconds);
    }

    private int GetSecondsSpentInGameInCar(int carIndex)
    {
        return PlayerPrefs.GetInt(SECONDS_SPENT_IN_GAME_IN_CAR + "-" + carIndex.ToString(), 0);
    }

    private void AddCashEarnedInGameInCar(int carIndex, int seconds)
    {
        PlayerPrefs.SetInt(CASH_EARNED_IN_GAME_IN_CAR + "-" + carIndex.ToString(), GetCashEarnedInGameInCar(carIndex) + seconds);
    }

    private int GetCashEarnedInGameInCar(int carIndex)
    {
        return PlayerPrefs.GetInt(CASH_EARNED_IN_GAME_IN_CAR + "-" + carIndex.ToString(), 0);
    }
}
