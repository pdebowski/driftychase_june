using UnityEngine;
using System.Collections;



public class StartGameSceneManager : MonoBehaviour
{
    public static System.Action OnStartButtonClick = delegate { };
    public static System.Action OnEnter = delegate { };

    [SerializeField]
    private CanvasGroup[] contents;
    [SerializeField]
    private GameObject tapToPlay;
    [SerializeField]
    private KeyChooser keyChooser;
    [SerializeField]
    private Camera bannerCamera;
    [SerializeField]
    private MenuNavigation.ForceSelection gameCenterButton;

    void Awake()
    {
        OnEnter();
    }

    void OnEnable()
    {
        IAPManager.OnPurchased += RefreshKeys;
    }

    void OnDisable()
    {
        IAPManager.OnPurchased -= RefreshKeys;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            StartButtonClick();
        }
    }

    public void StartButtonClick()
    {
        if (bannerCamera != null)
        {
            bannerCamera.gameObject.SetActive(false);
        }

        foreach (var item in contents)
        {
            item.interactable = false;
        }

        GlobalFader.Instance.Fade(contents, 1, 0, OnStartGameFadeStart, OnStartGameFadeEnd, 0.6f, 0.5f);
    }

    private void OnStartGameFadeStart()
    {
        PrefabReferences.Instance.GameSceneManager.SetActiveHUD(true);
        HideTapToPlay();
        OnStartButtonClick();
    }

    private void OnStartGameFadeEnd()
    {
        GameplayManager.OnSessionStarted();
        SceneLoader.Instance.UnloadScene("Scenes/StartGameScene");
    }

    public void OnOptionsButtonClick()
    {
        GlobalFader.Instance.Fade(null, OnFullyBlackOptionFade, null, 0.5f, 0.1f, 0);
    }

    public void OnStoreButtonClick()
    {
        GlobalFader.Instance.Fade(null, OnFullyBlackStoreFade, null, 0.5f, 0.1f, 0);
    }

    public void OnVIPButtonClick()
    {
        PlayerPrefsAdapter.ForceVIPOffer = true;
        GlobalFader.Instance.FadeIn(null, OnFullyBlackExitFadeToOffers, 0.5f, 0);
        HideTapToPlay();
    }

    private void OnFullyBlackExitFadeToOffers()
    {
        PrefabReferences.Instance.GameSceneManager.ShowOffersScreen();
        UnloadContinueScene();
    }

    private void UnloadContinueScene()
    {
        SceneLoader.Instance.UnloadScene("Scenes/StartGameScene");
    }

    private void RefreshKeys()
    {
        keyChooser.Refresh();
        if (!PlatformRecognition.IsTV)
        {
            tapToPlay.SetActive(true);
        }
    }

    public void HideTapToPlay()
    {
        if (!PlatformRecognition.IsTV)
        {
            tapToPlay.SetActive(false);
        }
    }

    public void RateMe()
    {
        PlayerPrefsAdapter.WasRateMeClicked = true;
#if UNITY_IOS
		Application.OpenURL("itms-apps://itunes.apple.com/app/id1145500015");
#else
        Application.OpenURL("com.apple.TVAppStore://itunes.apple.com/us/app/drifty-chase/id1145500015?mt=8");
#endif
    }

    public void OnLeaderboardsButtonClick()
    {

#if UNITY_TVOS
        {
            gameCenterButton.ForceSelect();
            PrefabReferences.Instance.LeaderboardFix.OnLeaderboardsOpen();
        }
#endif

        if (CrimsonPlugins.RankingCloudAdapter.Instance.p_rankingInstance.p_IsAuthenticated)
        {
            CrimsonPlugins.RankingCloudAdapter.Instance.p_rankingInstance.ShowLeaderboards();
        }
        else
        {
            CrimsonPlugins.RankingCloudAdapter.Instance.p_rankingInstance.Login();
        }

    }

    public void OnAchievementsButtonClick()
    {
        if (CrimsonPlugins.RankingCloudAdapter.Instance.p_rankingInstance.p_IsAuthenticated)
        {
            CrimsonPlugins.RankingCloudAdapter.Instance.p_rankingInstance.ShowAchievements();
        }
        else
        {
            CrimsonPlugins.RankingCloudAdapter.Instance.p_rankingInstance.Login();
        }
    }

    private void OnFullyBlackOptionFade()
    {
        SceneLoader.Instance.UnloadScene("Scenes/StartGameScene");
        SceneLoader.Instance.LoadSceneAdditive("Scenes/OptionsScene");
    }

    private void OnFullyBlackStoreFade()
    {
        PlayerPrefsAdapter.CarToSelectOnStoreEnter = PlayerPrefsAdapter.SelectedCar;
        SceneLoader.Instance.UnloadScene("Scenes/StartGameScene");
        SceneLoader.Instance.LoadSceneAdditive("Scenes/StoreScene");
    }
}
