﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PremiumShopSceneManager : MonoBehaviour
{

    [SerializeField]
    private GameObject content;
    [SerializeField]
    private Text cashText;


	private string navigateTo = "";

    void Start()
    {
        RefreshCashText();
    }

	void OnEnable()
	{
		IAPManager.OnPurchased += RefreshCashText;
	}

    void OnDisable()
    {
        IAPManager.OnPurchased -= RefreshCashText;
    }

	public void BackToScene(string sceneName)
    {
        navigateTo = sceneName;
    }

    public void OnBackButtonClick()
    {
        GlobalFader.Instance.Fade(null, OnFullyBlackExitFade, OnExitFadeEnd);
    }

    private void OnFullyBlackExitFade()
    {
        content.SetActive(false);
        if (navigateTo == "")
        {
            navigateTo = "Scenes/StoreScene";
        }
        SceneLoader.Instance.LoadSceneAdditive(navigateTo);
        navigateTo = "";
    }

    private void OnExitFadeEnd()
    {
        UnloadPremiumStorecene();
    }

    private void UnloadPremiumStorecene()
    {
        SceneLoader.Instance.UnloadScene("Scenes/PremiumShopScene");
        //Resources.UnloadUnusedAssets();
    }

    private void RefreshCashText()
    {
        cashText.text = PrefsDataFacade.Instance.GetTotalCash().ToString();
    }

    public void OnBuyCashButtonClick()
    {
        //TBD
        RefreshCashText();
    }

    public void OnBuyVipButtonClick()
    {
        //TBD
    }
}
