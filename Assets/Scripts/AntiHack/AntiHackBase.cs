﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;


public abstract class AntiHackBase<T>{
    public delegate T ParserDelegate(string val);
    private const string DELIMITER = "|::AAA::|";

    string m_key=null;
    

    public abstract T p_value{get;set;}
    //---------------------------------------------------------

    public AntiHackBase()
    {
        p_value = default(T);
    }
    //------------------------------------------------------------
    public AntiHackBase(string key): this()
    {
        m_key = key;
    }
    //------------------------------------------------------------
    public abstract void SaveToPrefs();
    //-----------------------------------------------------------
    public abstract void LoadFromPrefs();
    //---------------------------------------------------------
    protected void SaveToDisc()
    {
        if (m_key == null)
        {
            throw new System.NullReferenceException("Trying to Save prefs with non defined key");
        }

        SimpleAES crypter = new SimpleAES();

        string encryptedString = crypter.Encrypt(p_value.ToString() + DELIMITER + p_value.ToString());
        PlayerPrefs.SetString(m_key, encryptedString);
    }
    //---------------------------------------------------------
    protected void LoadFromDisc(ParserDelegate ParserFunc)
    {
        if (m_key == null)
        {
            throw new System.NullReferenceException("Trying to load from prefs with non defined key");
        }

        SimpleAES crypter = new SimpleAES();
        string encryptedString = PlayerPrefs.GetString(m_key, DELIMITER + DELIMITER);
        
        //not INITIALIZED
        if (encryptedString.Equals(DELIMITER + DELIMITER))
        {
            p_value = default(T);
        }
        else
        {
            string decriptedString = crypter.Decrypt(encryptedString);

            string[] splitted = decriptedString.Split(new string[] { DELIMITER }, StringSplitOptions.RemoveEmptyEntries);
            
            if (splitted.Length < 2 || !splitted[0].Equals(splitted[1]))
            {
                p_value = default(T);

                if (splitted.Length < 2)
                {
                    Debug.LogError("CHEATER ");
                }
                else
                {
                    Debug.LogError("CHEATER " + splitted[0] + "::" + splitted[1]);
                }       
            }
            else
            {
                p_value = ParserFunc(splitted[0]);
            }
        }

    }
}
