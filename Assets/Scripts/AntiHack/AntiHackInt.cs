﻿using UnityEngine;
using System.Collections;



public class AntiHackInt : AntiHackBase<int>
{
    private const int XOR_KEY = 0x5b5b5b5b;
    private const string UNIQUE_DELIMITER = "384W907";

    private int dynamicXor = new System.Random().Next();

    string m_strValue;
    public override int p_value
    {
        get
        {
            if (m_strValue == null)
            {
                Debug.LogWarning("returned 0,1");
                return 0;
            }

            string[] splittedEntries = m_strValue.Split(new string[] { UNIQUE_DELIMITER }, System.StringSplitOptions.RemoveEmptyEntries);

            if (splittedEntries.Length != 2)
            {
                Debug.LogWarning("returned 0,31");
                return 0;
            }

            int val1 = int.Parse(splittedEntries[0]);
            int val2 = int.Parse(splittedEntries[1]);
            val1 ^= XOR_KEY;
            val2 ^= dynamicXor;


            if (val1 == val2) return val1;
            else
            {
                Debug.LogWarning("returned 0,2");
                return 0;
            }
        }
        set
        {
            int xored1 = value ^ XOR_KEY;
            int xored2 = value ^ dynamicXor;

            m_strValue = xored1 + UNIQUE_DELIMITER + xored2;
        }
    }

    //-------------------------------------------------------
    public AntiHackInt() : base() { }
    //-------------------------------------------------------
    public AntiHackInt(string key) : base(key) { }
    //-------------------------------------------------------
    public override void SaveToPrefs()
    {
        SaveToDisc();
    }
    //-------------------------------------------------------
    public override void LoadFromPrefs()
    {
        LoadFromDisc(int.Parse);
    }
    //-------------------------------------------------------


}
