﻿using UnityEngine;
using System.Collections;
using System;



public class AntiHackString : AntiHackBase<string>
{

    private const int XOR_KEY = 0x5b5b5b5b;
    private int dynamicXor = new System.Random().Next();
    private const string UNIQUE_DELIMITER = "384W907";

    private string strValue;

    public override string p_value
    {
        get
        {
            if (strValue == null)
            {
                Debug.LogWarning("returned null ,1");
                return null;
            }

            string[] splittedEntries = strValue.Split(new string[] { UNIQUE_DELIMITER }, System.StringSplitOptions.RemoveEmptyEntries);

            if (splittedEntries.Length != 2)
            {
                Debug.LogWarning("returned null ,2");
                return null;
            }

            string val1 = CalcXor(splittedEntries[0], XOR_KEY);
            string val2 = CalcXor(splittedEntries[1], dynamicXor);

            if (val1.Equals(val2))
            {
                return val1;
            }
            else
            {
                Debug.LogWarning("returned null ,3");
                return null;
            }
        }
        set
        {
			if (value == null) {
				strValue = null;
			}
			else
			{
				string xored1 = CalcXor (value, XOR_KEY);
				string xored2 = CalcXor (value, dynamicXor);

				strValue = xored1 + UNIQUE_DELIMITER + xored2;
			}
        }
    }

    //-------------------------------------------------------
    public AntiHackString() : base() { }
    //-------------------------------------------------------
    public AntiHackString(string key) : base(key) { }
    //-------------------------------------------------------


    public override void SaveToPrefs()
    {
        SaveToDisc();
    }

    public override void LoadFromPrefs()
    {
        LoadFromDisc(ReturnString);
    }

    public string ReturnString(string str)
    {
        return str;
    }

    public string CalcXor(string str, int xorVal)
    {
        char[] charAArray = str.ToCharArray();
        char[] result = new char[str.Length];
        byte[] xorArray = BitConverter.GetBytes(xorVal);

        for (int i = 0; i < charAArray.Length; i++)
        {
            result[i] = (char)(charAArray[i] ^ xorArray[i % xorArray.Length]);
        }

        return new string(result);
    }

}
