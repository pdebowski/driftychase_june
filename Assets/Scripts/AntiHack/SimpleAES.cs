﻿//#define IGNORE_USA_COMPILANCE
using System;
using System.Security.Cryptography;
using System.IO;


public class SimpleAES
{
    private const string NEW_ENCRYPTION_PREFIX = "CRIMSON";
   // private static byte[] key = { 222, 0, 155, 13, 88, 55, 99, 111, 142, 5, 64, 178, 61, 34, 134, 200, 64, 13, 77, 37, 46, 59, 30, 70, 22, 77, 17, 254, 146, 153, 208, 244 };
    private const int USA_KEY_LENGTH = 40 / 8;

    bool serverEnc = false;
    private byte[] key;
    private byte[] vector;
    private byte[] vectorUsa;
    private byte[] keyUsa;


    private ICryptoTransform encryptor, decryptor;
    private System.Text.UTF8Encoding encoder;

    RijndaelManaged rm;
    RC2CryptoServiceProvider rc2crypto;


    public SimpleAES()
    {
        key = GameManager.Instance.AESKey;

        keyUsa = new byte[USA_KEY_LENGTH];

        for (int i = 0; i < USA_KEY_LENGTH; i++)
        {
            keyUsa[i] = GameManager.Instance.AESKey[i];
        }


        vector = new byte[] { 77, 18, 121, 177, 23, 201, 209, 32, 99, 191, 89, 164, 159, 0, 225, 7 };
        vectorUsa = new byte[] { 77, 18, 121, 177, 23, 201, 209, 32 };

        rm = new RijndaelManaged();
        rc2crypto = new RC2CryptoServiceProvider();
        rc2crypto.Mode = CipherMode.CBC;

        encoder = new System.Text.UTF8Encoding();
    }

    public SimpleAES(PaddingMode paddingMode, bool serverEnc) : this()
    {
        rc2crypto.Padding = paddingMode;
        rm.Padding = paddingMode;
        this.serverEnc = serverEnc;
    }

    public string Encrypt(string unencrypted)
    {
        string encrypted = Convert.ToBase64String(Encrypt(encoder.GetBytes(unencrypted)));

#if !IGNORE_USA_COMPILANCE
        if (!serverEnc)
        {
            encrypted = NEW_ENCRYPTION_PREFIX + encrypted;
        }
#endif

        return encrypted;
    }

    public string Decrypt(string encrypted)
    {
        string ret = null;

#if !IGNORE_USA_COMPILANCE
        if (encrypted.StartsWith(NEW_ENCRYPTION_PREFIX))
        {
            encrypted = encrypted.Remove(0, NEW_ENCRYPTION_PREFIX.Length);
            try
            {
                ret = encoder.GetString(DecryptRC2(Convert.FromBase64String(encrypted)));
            }
            catch (System.Exception e)
            {
                UnityEngine.Debug.LogWarning("Decryption error in RC2 " + e.Message + " trying AES");

                try
                {
                    ret = encoder.GetString(DecryptAES(Convert.FromBase64String(encrypted)));
                }
                catch (System.Exception e2)
                {
                    ret = null;
                    UnityEngine.Debug.LogWarning("Decryption error in AES after RC2 " + e2.Message);
                }

            }
        }
        else
        {
            try
            {
                ret = encoder.GetString(DecryptAES(Convert.FromBase64String(encrypted)));
            }
            catch (System.Exception e)
            {
                UnityEngine.Debug.LogWarning("Decryption Error in AES " + e.Message + " trying RC2");
                try
                {
                    ret = encoder.GetString(DecryptRC2(Convert.FromBase64String(encrypted)));
                }
                catch (System.Exception e2)
                {
                    ret = null;
                    UnityEngine.Debug.LogWarning("Decryption Error in RC2 afetr AES " + e2.Message);
                }
            }
        }
#else
		ret = encoder.GetString(DecryptAES (Convert.FromBase64String (encrypted)));
#endif
        return ret;
    }

#if !IGNORE_USA_COMPILANCE
    public byte[] Encrypt(byte[] buffer)
    {
        encryptor = rc2crypto.CreateEncryptor(keyUsa, vectorUsa);
        byte[] ret = null;
        try
        {
            ret = Transform(buffer, encryptor);
        }
        catch (System.Exception e)
        {
            UnityEngine.Debug.LogWarning("Encryptor Fatal Error !!!! : " + e.Message);
        }

        return ret;
    }
#else
	public byte[] Encrypt(byte[] buffer)
	{
		encryptor = rm.CreateEncryptor(key, vector);
		byte[] ret = null; 
		try{
			ret=Transform(buffer, encryptor);
		}catch (System.Exception e){
			UnityEngine.Debug.LogWarning ("Encryptor Fatal Error AES !!!! : " + e.Message); 
		}

		return ret;
	}
#endif

    byte[] DecryptRC2(byte[] buffer)
    {
        byte[] ret = null;

        decryptor = rc2crypto.CreateDecryptor(keyUsa, vectorUsa);
        ret = Transform(buffer, decryptor);
        return ret;
    }

    byte[] DecryptAES(byte[] buffer)
    {
        decryptor = rm.CreateDecryptor(key, vector);
        return Transform(buffer, decryptor);
    }


    protected byte[] Transform(byte[] buffer, ICryptoTransform transform)
    {
        MemoryStream stream = new MemoryStream();
        using (CryptoStream cs = new CryptoStream(stream, transform, CryptoStreamMode.Write))
        {
            cs.Write(buffer, 0, buffer.Length);
            cs.FlushFinalBlock();
        }
        return stream.ToArray();
    }
}
