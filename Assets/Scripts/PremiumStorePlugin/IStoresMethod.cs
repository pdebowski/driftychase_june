using UnityEngine;
using System.Collections;

public interface IStoresMethod {
	bool p_IsConnected {get;}

	void ConsumeProduct (string productIdentifier);
	void RestorePurchases();
	void ConnectToStore();
	void PurchaseProduct(string productIdentifier);
}
