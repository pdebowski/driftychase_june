using UnityEngine;
using System.Collections;
using System.Linq;

namespace CrimsonPlugins.Internal.IAP
{
    public class IAPDummyAdapter : MonoBehaviour, IStoresMethod
    {
        public bool p_IsConnected { get { return connected; } }
        bool connected;
        public void RestorePurchases()
        {
            StartCoroutine(RestorePurchasedCor());
            Debug.Log("IAPDummyAdapter Dummy Restore purchase");
        }
        public void ConnectToStore() {
            Debug.Log("IAPDummyAdapter Dummy Connect");
            connected = true;
        }

        public void PurchaseProduct(string storeId) {
            StartCoroutine(PruchasedDelay(storeId));
        }

		public void ConsumeProduct(string storeId)	
		{
			Debug.Log("consuming product " + storeId);
		}

        IEnumerator RestorePurchasedCor()
        {
            yield return new WaitForSeconds(3);
            GetComponent<ProductStoreManager>().RestoreTransactionsFinished();
        }

        IEnumerator PruchasedDelay(string storeId)
        {
            yield return new WaitForSeconds(3);
            Debug.Log("asdasdsd" + storeId);
            var storeInfo = new StoreTransactionInfo();
            var storeManager = GetComponent<ProductStoreManager>();

            storeInfo.productIdentifier = storeId;

            storeManager.PurchaseSuccessful(storeInfo);

        }
    }
}