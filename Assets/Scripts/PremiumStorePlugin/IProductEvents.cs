using UnityEngine;
using System.Collections;
using CrimsonPlugins.Internal.IAP;

namespace CrimsonPlugins
{
    public interface IProductEvents
    {

        void OnProductUpdated(ProductInfo prodInfo, E_UpdateReason reason);

        void OnPurchaseFailed(ProductInfo prodInfo, E_FailReason reason);

        void OnLeveledMaxLevelAchieved();

    }


}