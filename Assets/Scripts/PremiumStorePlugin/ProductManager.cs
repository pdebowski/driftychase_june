using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using CrimsonPlugins.Internal.IAP.ProductModel;
using CrimsonPlugins.Internal.IAP;
using System.Runtime.CompilerServices;

namespace CrimsonPlugins
{
    public class ProductManager : MonoBehaviour
    {

        public Item[] m_consumable;
        public Item[] m_nonConsumable;
        public ItemLeveled[] m_leveled;

        [HideInInspector]
        public ProductStoreManager m_productStoreManager;


        public int Coins
        {
            get
            {
                return PlayerPrefs.GetInt("__Coins", 0);
            }
            set
            {
                int previousValue = PlayerPrefs.GetInt("__Coins", 0);
                PlayerPrefs.SetInt("__Coins", value);
                PlayerPrefs.Save();
                if (_coinsUpdated != null)
                    _coinsUpdated(value, previousValue);
            }
        }

        private ItemBase[] m_allProductsBase;


        //
        private ProductStatusObserver m_observerAdapter;


        private event System.Action<int, int> _coinsUpdated = null;
        public event System.Action<int, int> CoinsUpdated
        {
            add
            {
                _coinsUpdated += value;
                _coinsUpdated(Coins, Coins);
            }
            remove
            {
                _coinsUpdated -= value;
            }
        }


        //public event Action restoreTransactionsFinishedEvent;

        //---------------------------------------------------------------------------------------------------------
        [ContextMenu("Delete Prefs")]
        void DeletePrefs()
        {
            PlayerPrefs.DeleteAll();
        }

        //---------------------------------------------------------------------------------------------------------
        void Awake()
        {            
            m_productStoreManager = GetComponent<ProductStoreManager>();
            m_observerAdapter = new ProductStatusObserver();

            m_allProductsBase = new ItemBase[m_consumable.Length + m_nonConsumable.Length + m_leveled.Length];

            ItemBase[] it = m_consumable.OfType<ItemBase>().ToArray();
            it.CopyTo(m_allProductsBase, 0);

            it = m_nonConsumable.OfType<ItemBase>().ToArray();
            it.CopyTo(m_allProductsBase, m_consumable.Length);

            it = m_leveled.OfType<ItemBase>().ToArray();
            it.CopyTo(m_allProductsBase, m_consumable.Length + m_nonConsumable.Length);


        }
        //---------------------------------------------------------------------------------------------------------
        public void RequestRestore()
        {
            m_productStoreManager.RestorePurchases();
        }

        //---------------------------------------------------------------------------------------------------------
        //amountbycoins is ignored when currency type is Money
        public void RequestPurchase(string name, E_Currency currency = E_Currency.Money, int amountByCoins = 0)
        {

            //perhaps coins
            if (!m_allProductsBase.Any(v => v.name.Equals(name)) && currency == E_Currency.Money)
            {
                m_productStoreManager.PurchaseProduct(name);
                return;
            }

            if (GetProductType(name) == E_ProductType.Leveled && currency == E_Currency.Money)
            {
                Debug.LogError("Cannot buy Leveled type for real money");
                return;
            }


            ProductInfo p = GetProductInfo(name);

            //check if have enough money if coins is the currency
            if (currency == E_Currency.Coins)
            {
                if (Coins < p.cost)
                {
                    m_observerAdapter.NotifyPurchaseFailed(GetProductInfo(name), E_FailReason.NoMoney);
                    return;
                }
            }
            //CHECK IF transaction is possible
            switch (p.type)
            {
                case E_ProductType.Consumable:
                    {
                        break;
                    }
                case E_ProductType.NonConsumable:
                    {
                        Item nonConsumable = m_nonConsumable.First(item => item.name.Equals(name));
                        if (nonConsumable.Amount > 0)
                        {
                            m_observerAdapter.NotifyPurchaseFailed(GetProductInfo(name), E_FailReason.NonConsumableAlreadyPurchased);
                            return;
                        }
                        break;
                    }
                case E_ProductType.Leveled:
                    {
                        ItemLeveled leveled = m_leveled.First(item => item.name.Equals(name));

                        if (leveled.numberOfLevels == 0) Debug.LogError("Error, no costs for levels for item with id: " + leveled.id);

                        if (leveled.Level + 1 >= leveled.numberOfLevels)
                        {
                            m_observerAdapter.NotifyPurchaseFailed(GetProductInfo(name), E_FailReason.LeveledMax);
                            return;
                        }
                        break;
                    }
            }

            if (currency == E_Currency.Coins)
            {
                ProductPurchased(p, currency, amountByCoins);
            }
            else if (currency == E_Currency.Money)
            {
                m_productStoreManager.PurchaseProduct(name);
            }

        }
        //---------------------------------------------------------
        public void PurchasedFromStore(string name, int amount)
        {
            ProductInfo p = GetProductInfo(name);
            ProductPurchased(p, E_Currency.Money, amount);
        }
        //---------------------------------------------------------------------------------------------------------
        public void RequestUpdate(string name)
        {
            ProductInfo p = GetProductInfo(name);
            m_observerAdapter.NotifyProductUpdated(p, E_UpdateReason.Update);
            if (p.type == E_ProductType.Leveled && p.numberOfLevels <= p.level + 1)
                m_observerAdapter.NotifyLeveledMaxLevelAchieved(p);
        }

        /// <summar>
        //---------------------------------------------------------------------------------------------------------


        //---------------------------------------------------------------------------------------------------------
        public ProductInfo GetProductInfo(string name)
        {
            return GetProductInfo(GetIdFromName(name));
        }

        //---------------------------------------------------------------------------------------------------------
        public void AddObserver(IProductEvents observer, int id)
        {
            m_observerAdapter.AddObserver(observer, id);
        }

        //---------------------------------------------------------------------------------------------------------
        public void RemoveObserver(IProductEvents observer, int id)
        {
            m_observerAdapter.RemoveObserver(observer, id);
        }


        //---------------------------------------------------------------------------------------------------------
        public int[] GetLeveledCosts(string name)
        {
            if (GetProductType(name) != E_ProductType.Leveled)
            {
                Debug.LogError("Only leveled type allowed");
            }
            return m_leveled.First(item => item.name.Equals(name)).costs;
        }

        //---------------------------------------------------------------------------------------------------------
        public void AddObserver(IProductEvents observer, string name)
        {
            AddObserver(observer, GetIdFromName(name));
        }

        //---------------------------------------------------------------------------------------------------------
        public void RemoveObserver(IProductEvents observer, string name)
        {
            RemoveObserver(observer, GetIdFromName(name));
        }


        //---------------------------------------------------------------------------------------------------------
        public int GetLeveledLevelsAvailable(string name)
        {
            return GetLeveledCosts(name).Length;
        }


        //---------------------------------------------------------------------------------------------------------
        public E_ProductType GetProductType(string name)
        {
            if (m_consumable.Any(item => item.name.Equals(name))) return E_ProductType.Consumable;
            else if (m_nonConsumable.Any(item => item.name.Equals(name))) return E_ProductType.NonConsumable;
            else if (m_leveled.Any(item => item.name.Equals(name))) return E_ProductType.Leveled;

            Debug.LogError("Bad name " + name);
            return E_ProductType.NonConsumable;

        }


        //---------------------------------------------------------------------------------------------------------
        //----------PRIVATE------------------------------------------------------------------------------------
        //---------------------------------------------------------------------------------------------------------
        private E_ProductType GetProductType(int id)
        {

            if (id < 0 && id >= m_consumable.Length + m_nonConsumable.Length + m_leveled.Length)
                Debug.LogError("Bad id " + id);

            if (id < m_consumable.Length)
                return E_ProductType.Consumable;
            else if (id < m_consumable.Length + m_nonConsumable.Length && m_nonConsumable.Length > 0)
                return E_ProductType.NonConsumable;
            else if (id < m_consumable.Length + m_nonConsumable.Length + m_leveled.Length && m_leveled.Length > 0)
                return E_ProductType.Leveled;

            Debug.LogError("Bad id " + id);
            return E_ProductType.Leveled;
        }

        //---------------------------------------------------------------------------------------------------------
        //---------------------------------------------------------------------------------------------------------
        private int GetIdFromName(string name)
        {
            if (!m_allProductsBase.Any(cl => name.Equals(cl.name)))
            {
                Debug.LogError("There is no product with name " + name);
                return 0;
            }
            return m_allProductsBase.First(cl => name.Equals(cl.name)).id;
        }

        //---------------------------------------------------------------------------------------------------------
        private ProductInfo GetProductInfo(int id)
        {
            ProductInfo ret;
            ItemBase it = null; ;

            ret = new ProductInfo();
            ret.type = GetProductType(id);

            switch (ret.type)
            {
                case E_ProductType.Consumable:
                    {
                        it = m_consumable.First(item => item.id == id);
                        ret.amount = ((Item)it).Amount;
                        ret.cost = ((Item)it).cost;
                        break;
                    }
                case E_ProductType.NonConsumable:
                    {
                        it = m_nonConsumable.First(item => item.id == id);
                        ret.cost = ((Item)it).cost;
                        ret.amount = ((Item)it).Amount;
                        break;
                    }
                case E_ProductType.Leveled:
                    {
                        it = m_leveled.First(item => item.id == id);
                        ItemLeveled itLeveled = ((ItemLeveled)it);
                        ret.level = itLeveled.Level;
                        if (itLeveled.numberOfLevels == 0) Debug.LogError("Error, no costs for levels for item with id: " + it.id);

                        if (itLeveled.numberOfLevels <= itLeveled.Level + 1) ret.cost = -10;
                        else ret.cost = itLeveled.costs[itLeveled.Level + 1];
                        ret.modifier = itLeveled.costs[itLeveled.Level];
                        ret.numberOfLevels = itLeveled.numberOfLevels;

                        break;
                    }
            }

            ret.id = it.id;
            ret.name = it.name;
            ret.description = it.description;
            return ret;
        }

        //---------------------------------------------------------------------------------------------------------
        private void ProductPurchased(ProductInfo p, E_Currency currency, int amount)
        {

            Debug.Log("Product purchased: " + p.name + " amount:" + amount + " currency " + currency.ToString() + " type " + p.type.ToString());

            if (currency == E_Currency.Coins)
            {
                Coins -= p.cost;
            }

            if (p.type == E_ProductType.Leveled)
            {
                ItemLeveled leveled = m_leveled.First(item => item.id.Equals(p.id));
                leveled.Level += amount;
            }
            else
            {
                Item it = m_consumable.Concat(m_nonConsumable).First(item => item.id.Equals(p.id));
                it.Amount += amount;
            }

            //refresh product status
            p = GetProductInfo(p.id);
            if (currency == E_Currency.Coins)
            {
                m_observerAdapter.NotifyProductUpdated(p, E_UpdateReason.BoughtCoins);
            }
            else
            {
                m_observerAdapter.NotifyProductUpdated(p, E_UpdateReason.BoughtMoney);
            }


            if (p.type == E_ProductType.Leveled)
            {
                ItemLeveled leveled = m_leveled.First(item => item.id.Equals(p.id));

                if (leveled.numberOfLevels <= leveled.Level + 1)
                {
                    m_observerAdapter.NotifyLeveledMaxLevelAchieved(GetProductInfo(p.id));
                }
            }

        }

        //---------------------------------------------------------------------------------------------------------
    }

}
