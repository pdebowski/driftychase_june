using UnityEngine;
using System.Collections;

namespace CrimsonPlugins.Internal.IAP
{
    public enum E_ProductType
    {
        Consumable,
        NonConsumable,
        Leveled
    }

    public enum E_Currency
    {
        Coins,
        Money
    }

    public enum E_UpdateReason
    {
        //bought for coins
        BoughtCoins,
        //bought for real money
        BoughtMoney,
        Update,
        Used
    }

    public enum E_FailReason
    {

        //NONCONSUMABLE :: already purchased
        NonConsumableAlreadyPurchased,

        //LEVELED :: maximum level already achieved
        LeveledMax,

        //COMMON
        //not enought coins
        NoMoney

    }
    #region STORE
    public class StoreTransactionInfo
    {
        //	public string name;
        public string productName;
        public string productIdentifier;
        public string transactionIdentifier;
        public string base64EncodedTransactionReceipt;
    }

    public class StoreProductInfo
    {

        public string productIdentifier;
        public string title;
        public string description;
        public string price;
        public string currencySymbol;
        public string currencyCode;

    }

    #endregion

    public class ProductInfo : ProductModel.ItemBase
    {

        //product id from base
        //public int id;

        //product name from base
        //public string name;

        //product description from base
        //public string description;

        //product type
        public E_ProductType type;

        //cost of the next level when product type is leveled (-10 when actual level is maximum) or cost of product otherwise
        public int cost;


        //Leveled::actual level in case of Leveled iap
        public int level;
        //Leveled::modifier for current level - for game logic
        public float modifier;
        //Leveled::modifier for current level - for game logic
        public int numberOfLevels;

        //Consumables:amount in case of Consumables
        public int amount;
    }


    namespace ProductModel
    {

        public class ItemBase
        {
            public int id = 0;
            public string name;
            public string description;

        }

        [System.Serializable]
        public class Item : ItemBase
        {

            [HideInInspector]
            public bool isConsumable = false;
            public int cost = 0;

            public int Amount
            {
                get
                {
                    AntiHackInt amount = new AntiHackInt("_amount" + name);
                    amount.LoadFromPrefs();
                    return amount.p_value;
                }
                set
                {
                    AntiHackInt amount = new AntiHackInt("_amount" + name);
                    amount.p_value = value;
                    amount.SaveToPrefs();
                }
            }
        }

        [System.Serializable]
        public class ItemLeveled : ItemBase
        {
            public int numberOfLevels = 1;
            public int[] costs;
            public float[] modifiers;
            [HideInInspector]
            public int Level
            {
                get
                {
                    AntiHackInt level = new AntiHackInt("_level" + name);
                    level.LoadFromPrefs();
                    return level.p_value;
                }
                set
                {
                    AntiHackInt level = new AntiHackInt("_level" + name);
                    level.p_value = value;
                    level.SaveToPrefs();
                }
            }
        }
    }
}
