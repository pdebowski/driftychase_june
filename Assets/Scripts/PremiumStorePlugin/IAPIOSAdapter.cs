using UnityEngine;
using System.Collections;
#if UNITY_IOS

using SA.IOSNative.StoreKit;

namespace CrimsonPlugins.Internal.IAP
{
	public class IAPIOSAdapter : MonoBehaviour, IStoresMethod
    {

        float m_lastConnectTime;
        ProductStoreManager m_storeManager;
        public bool p_IsConnected
        {
            get
            {
                //          IAPManager.DebugLog("is connected to store " + AndroidInAppPurchaseManager.instance.IsConnectd);
				return PaymentManager.Instance.IsStoreLoaded;
            }
        }
        //---------------------------------------------------------------------------
        void Awake()
        {
            m_lastConnectTime = Time.time;
            m_storeManager = GetComponent<ProductStoreManager>();

			foreach (var it in m_storeManager.m_storeItems)
            {
				PaymentManager.Instance.AddProductId(it.storeId);
            }

        }
        //--------------------------------------------------------------------------
        void OnEnable()
        {
			PaymentManager.OnTransactionComplete += ProductPurchased;
			PaymentManager.OnStoreKitInitComplete += OnStoreConnected;
			PaymentManager.OnRestoreComplete += OnRetrieveProductsFinished;

			PaymentManager.OnTransactionStarted += OnTransactionStarted;
			PaymentManager.OnRestoreStarted += OnRestoreStarted;
    //        IOSInAppPurchaseManager.OnVerificationComplete += OnVerifyResponse;
        }
        //--------------------------------------------------------------------------
        void OnDisable()
        {
			PaymentManager.OnTransactionComplete -= ProductPurchased;
			PaymentManager.OnStoreKitInitComplete -= OnStoreConnected;
			PaymentManager.OnRestoreComplete -= OnRetrieveProductsFinished;

			PaymentManager.OnTransactionStarted -= OnTransactionStarted;
			PaymentManager.OnRestoreStarted -= OnRestoreStarted;
     //       IOSInAppPurchaseManager.OnVerificationComplete -= OnVerifyResponse;
        }
		//-------------------------------------------------------------------------
		//-----------------------------------------------------------
		public void PurchaseProduct(string productIdentifier)
		{
			PaymentManager.Instance.BuyProduct(productIdentifier);
		}
		//-------------------------------------------------------------------------
		void ProductPurchased(PurchaseResult billResult)
        {
			if (billResult.State == PurchaseState.Purchased || billResult.State == PurchaseState.Restored)
            {
                //Add Verify
                StoreTransactionInfo transactionInfo = new StoreTransactionInfo();
                transactionInfo.base64EncodedTransactionReceipt = billResult.Receipt;
                transactionInfo.productIdentifier = billResult.ProductIdentifier;
                transactionInfo.transactionIdentifier = billResult.TransactionIdentifier;

				Debug.Log(transactionInfo.base64EncodedTransactionReceipt + " : " + transactionInfo.productIdentifier + " : " + transactionInfo.transactionIdentifier);

                m_storeManager.PurchaseSuccessful(transactionInfo);

             //   IOSInAppPurchaseManager.Instance.VerifyLastPurchase(IOSInAppPurchaseManager.APPLE_VERIFICATION_SERVER);

            }
            else
            {
                m_storeManager.PurchaseCancelled(billResult.TransactionErrorCode.ToString());
                Debug.Log("Purchase fail" + billResult.TransactionErrorCode + " : " + billResult.State);
            }
            //m_storeManager.OnStopBlockingAds();
        }
        //------------------------------------------------------------
        public void ConnectToStore()
        {
			PaymentManager.Instance.LoadStore();
        }
        //------------------------------------------------------------
        public void RestorePurchases()
        {
			PaymentManager.Instance.RestorePurchases();
        }

		//-------------------------------------------------------------
		public void ConsumeProduct(string storeId)
		{
			//void, non consuming product on ios
		}
        //-------------------------------------------------------------
        void OnTransactionStarted(string msg)
        {
            Debug.Log("OnTransactionStarted " + msg);
            //m_storeManager.OnStartBlockingAds();
        }
        //-------------------------------------------------------------
        void OnRestoreStarted()
        {
            Debug.Log("OnRestoreStarted");
            m_storeManager.RestoreTransactionsStarted();
            //m_storeManager.OnStartBlockingAds();
        }
        //-------------------------------------------------------------

		void OnStoreConnected(SA.Common.Models.Result result)
        {
            if (result.IsSucceeded)
            {
                //Store connection is Successful. Next we loading product and customer purchasing details
                m_storeManager.ConnectedToStore();
            }
            Debug.Log("Connection Responce: " + result.Error + " " + result.IsSucceeded);
        }
        //------------------------------------------------------------
		void OnRetrieveProductsFinished(RestoreResult result)
        {
            if (result.TransactionErrorCode == TransactionErrorCode.SKErrorNone)
            {
                Debug.Log("Restore transaction finished");
                m_storeManager.RestoreTransactionsFinished();
                Debug.Log("Connection Responce" + ":" + result.TransactionErrorCode);
            }
            else
            {
                m_storeManager.RestoreTransactionsFailed(result.Error.ToString());//, SendMessageOptions.RequireReceiver);
                Debug.Log("Connection Responce333333" + result.TransactionErrorCode);
            }
            //m_storeManager.OnStopBlockingAds();
        }
        //-------------------------------------------------------------
    }
}
#endif
