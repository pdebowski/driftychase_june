using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace CrimsonPlugins.Internal.IAP
{

    [RequireComponent(typeof(ProductManager))]

    [System.Serializable]
    public class StoreItem
    {

        public string name;
        public string storeId;
        public int amount;
    }


    public class ProductStoreManager : MonoBehaviour
    {

        //optional
        public event System.Action OnRestoreTransactionsFinishedEvent;

        public event System.Action OnRestoreTransactionsStartedEvent;

        public event System.Action<string> OnRestoreTransactionsFailedEvent;

        public event System.Action<string> OnPurchaseCancelledEvent;

        public event System.Action<StoreTransactionInfo> OnPurchaseSuccessfulEvent;

        public event System.Action OnConnectedToStoreEvent;

        //	public event System.Action<List<StoreProductInfo>> productListReceived; //due useless


        public StoreItem[] m_storeItems;
        public StoreItem[] m_storeCoins;

        IStoresMethod m_storeMethods;
        float m_lastConnectionTime;
        ProductManager productManager;


        //------------------------------------------------------
        void Awake()
        {
            productManager = GetComponent<ProductManager>();

            ProductModel.Item[] purchasableItems = productManager.m_consumable.Concat(productManager.m_nonConsumable).ToArray();

            foreach (StoreItem item in m_storeItems)
            {
                if (!purchasableItems.Any(v => v.name.Equals(item.name)))
                {
                    Debug.LogError("The product name " + item.name + " does not exists in consumable or nonconsumable list in ProductManager.cs ");
                }
            }

#if UNITY_EDITOR
            gameObject.AddComponent<IAPDummyAdapter>();
#elif UNITY_IOS
			gameObject.AddComponent<IAPIOSAdapter>();
#elif UNITY_ANDROID
            gameObject.AddComponent<IAPAndroidPluginAdapter>();
#endif

        }
        //------------------------------------------------------
        void Start()
        {
            m_storeMethods = GetComponent(typeof(IStoresMethod)) as IStoresMethod;
        }
        //-----------------------------------------------------
        public ProductModel.Item[] GetNonConsumables()
        {
            return productManager.m_nonConsumable;
        }
        //------------------------------------------------------
        public void PurchaseProduct(string _name)
        {
            if (m_storeMethods.p_IsConnected == false)
            {
              	ConnectToStore();
				Invoke ("PurchaseNoConnection", 0.2f);
            }
            else
            {
                StoreItem coin = m_storeCoins.FirstOrDefault(v => v.name.Equals(_name));
                StoreItem item = m_storeItems.FirstOrDefault(v => v.name.Equals(_name));
                if (coin != null)
                {
                    m_storeMethods.PurchaseProduct(coin.storeId);
                }
                else if (item != null)
                {
                    m_storeMethods.PurchaseProduct(item.storeId);
                }
                else
                    Debug.LogError("no listener registered for 'purchase' or no item named " + _name + " in ProductStoreManager");
            }
        }
		//------------------------------------------------
		void PurchaseNoConnection(){
			PurchaseCancelled ("IAP: On connection");
		}
		//------------------------------------------------
		void RestoreTransactionsNoConnection(){
			RestoreTransactionsFailed ("IAP: On connection");
		}
		//-------------------------------------------------
        public void ConnectToStore()
        {
            m_lastConnectionTime = Time.time;
            m_storeMethods.ConnectToStore();
        }
        //------------------------------------------------------
        public void RestorePurchases()
        {
            if (m_storeMethods.p_IsConnected == false)
            {
                ConnectToStore();
				Invoke ("RestoreTransactionsNoConnection", 0.2f);
            }
            else
            {
                m_storeMethods.RestorePurchases();
            }
        }
        //------------------------------------------------------
        public void RestoreTransactionsFailed(string error)
        {
            if (OnRestoreTransactionsFailedEvent != null)
                OnRestoreTransactionsFailedEvent(error);
        }

        //------------------------------------------------------
        public void RestoreTransactionsFinished()
        {
            if (OnRestoreTransactionsFinishedEvent != null)
                OnRestoreTransactionsFinishedEvent();
        }
        //------------------------------------------------------
        public void RestoreTransactionsStarted()
        {
            if (OnRestoreTransactionsStartedEvent != null)
                OnRestoreTransactionsStartedEvent();
        }
        //------------------------------------------------------
        public void PurchaseCancelled(string message)
        {
            if (OnPurchaseCancelledEvent != null)
                OnPurchaseCancelledEvent(message);
        }
        //------------------------------------------------------
        public void ConnectedToStore()
        {
            if (OnConnectedToStoreEvent != null)
                OnConnectedToStoreEvent();
            //if (p_isPurchaseRestored==false) RestorePurchases(); 		
        }
        //------------------------------------------------------
        public void PurchaseSuccessful(StoreTransactionInfo transactionInfo)
        {

            StoreItem coin = m_storeCoins.FirstOrDefault(v => v.storeId.Equals(transactionInfo.productIdentifier));
            if (coin != null)
            {
                transactionInfo.productName = coin.name;
                productManager.Coins += coin.amount;
            }

            StoreItem item = m_storeItems.FirstOrDefault(v => v.storeId.Equals(transactionInfo.productIdentifier));
            if (item != null)
            {
                transactionInfo.productName = item.name;
                productManager.PurchasedFromStore(item.name, item.amount);
				
			if (productManager.m_consumable.Any(v=>v.name.Equals(item.name)))
				{
					m_storeMethods.ConsumeProduct (item.storeId);
				}
            }


            if (OnPurchaseSuccessfulEvent != null)
                OnPurchaseSuccessfulEvent(transactionInfo);

        }
        //------------------------------------------------------
    }

}
