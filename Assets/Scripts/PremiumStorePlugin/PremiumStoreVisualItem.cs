﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PremiumStoreVisualItem : InAppPromoDependent
{

    [SerializeField]
    private IAPManager.Products productType;
    [SerializeField]
    private GameObject[] promoObjects;


    public void BuyProduct()
    { 
        PremiumStore.BuyProduct(productType);
    }

    protected override void RefreshVisual(bool isPromo, IAPManager.Products promoProduct, InAppPromoManager.IsProductInPromoDelegate IsProductInPromo)
    {
        InAppInfoItem infoItem = PrefabReferences.Instance.InAppInfoDB.GetInAppInfoItem(productType);
        bool isInPromo = IsProductInPromo(productType);
        RefreshVisualBase(isInPromo, infoItem);
        promoObjects.SetActive(isInPromo);

    }

}
