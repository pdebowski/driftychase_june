using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace CrimsonPlugins.Internal.IAP
{
    public class ProductStatusObserver
    {

        //-----------------------------------------------------------------------------
        //-----------------------------------------------------------------------------
        class ObserverInfo
        {
            public IProductEvents Observer { get; set; }
            public int Id { get; set; }

            public ObserverInfo(IProductEvents observer, int id)
            {
                Observer = observer;
                Id = id;
            }
        }

        //-----------------------------------------------------------------------------
        private List<ObserverInfo> observers = new List<ObserverInfo>();

        //-----------------------------------------------------------------------------
        public void AddObserver(IProductEvents observer, int id)
        {
            observers.Add(new ObserverInfo(observer, id));
        }

        //-----------------------------------------------------------------------------
        public void RemoveObserver(IProductEvents observer, int id)
        {
            ObserverInfo o = observers.Find((obj) => observer == obj.Observer && id == obj.Id);
            if (o == null)
            {
                Debug.LogError("Observer doesnt exist on the list");
            }
            observers.Remove(o);
        }

        //-----------------------------------------------------------------------------
        public void NotifyProductUpdated(ProductInfo prodInfo, E_UpdateReason reason)
        {
            foreach (ObserverInfo o in observers)
            {
                if (o.Id == prodInfo.id)
                {
                    o.Observer.OnProductUpdated(prodInfo, reason);
                }
            }
        }

        //-----------------------------------------------------------------------------
        public void NotifyPurchaseFailed(ProductInfo prodInfo, E_FailReason reason)
        {
            foreach (ObserverInfo o in observers)
            {
                if (o.Id == prodInfo.id)
                {
                    o.Observer.OnPurchaseFailed(prodInfo, reason);
                }
            }
        }

        //-----------------------------------------------------------------------------
        public void NotifyLeveledMaxLevelAchieved(ProductInfo prodInfo)
        {
            foreach (ObserverInfo o in observers)
            {
                if (o.Id == prodInfo.id)
                {
                    o.Observer.OnLeveledMaxLevelAchieved();
                }
            }
        }
        //-----------------------------------------------------------------------------

        //-----------------------------------------------------------------------------

    }
}

