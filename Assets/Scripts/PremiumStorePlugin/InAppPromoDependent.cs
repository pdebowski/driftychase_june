﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class InAppPromoDependent : MonoBehaviour
{


    [SerializeField]
    protected Image productImage;
    [SerializeField]
    protected Text nameText;
    [SerializeField]
    protected Text priceText;
    [SerializeField]
    protected Text ammountText;
    [SerializeField]
    private Text timerText;
    [SerializeField]
    private Image priceTag;
    [SerializeField]
    private Text[] offValueText;
    [SerializeField]
    private Text oldPriceText;
    [SerializeField]
    private GameObject[] christmasObjectsEnabled;
    [SerializeField]
    private GameObject[] christmasObjectsDisabled;

    private Color normalPriceTagColor = new Color(47 / 255f, 46 / 255f, 45 / 255f);
    private Color promoPriceTagColor = new Color(227 / 255f, 42 / 255f, 55 / 255f);
    private Color normalPriceTextColor = new Color(199 / 255f, 191 / 255f, 184 / 255f);
    private Color promoPriceTextColor = new Color(1, 1, 1);

    void OnEnable()
    {
        InAppPromoManager.OnRefreshTimer += RefreshTimer;
        InAppPromoManager.OnRefreshProduct += RefreshVisual;
        PrefabReferences.Instance.InAppPromoManager.RefreshDependentProduct(RefreshVisual);
        PrefabReferences.Instance.InAppPromoManager.RefreshDependentTimer(RefreshTimer);
    }

    void OnDisable()
    {
        InAppPromoManager.OnRefreshTimer -= RefreshTimer;
        InAppPromoManager.OnRefreshProduct -= RefreshVisual;
    }

    protected virtual void RefreshVisual(bool isPromo, IAPManager.Products promoProduct, InAppPromoManager.IsProductInPromoDelegate IsProductInPromo)
    {

    }

    protected void RefreshVisualBase(bool isInPromo, InAppInfoItem infoItem)
    {
        productImage.sprite = infoItem.sprite;
        nameText.text = infoItem.name;
        ammountText.text = infoItem.visualAmmount;
        priceText.text = "$" + (isInPromo ? infoItem.promoPrice : infoItem.normalPrice);
        priceText.color = isInPromo ? promoPriceTextColor : normalPriceTextColor;
        priceTag.color = isInPromo ? promoPriceTagColor : normalPriceTagColor;
        foreach (var item in offValueText)
        {
            item.text = infoItem.offPercentageValue;
        }
        oldPriceText.text = "$" + infoItem.normalPrice;
        christmasObjectsEnabled.SetActive(isInPromo && GTMParameters.IsChristmas);
        christmasObjectsDisabled.SetActive(isInPromo && !GTMParameters.IsChristmas);
    }

    private void RefreshTimer(System.TimeSpan timeLeft, string timeLeftFormated)
    {
        timerText.text = timeLeftFormated;
    }

}
