using UnityEngine;
using System.Collections;
//using UnionAssets.FLE;
using System.Linq;

namespace CrimsonPlugins.Internal.IAP
{

    public class IAPAndroidPluginAdapter : MonoBehaviour, IStoresMethod
    {

        float m_lastConnectTime;
        ProductStoreManager m_storeManager;
		bool restored=false;

        public bool p_IsConnected
        {
            get
            {
                return AndroidInAppPurchaseManager.Client.IsConnected;
            }
        }

        void Awake()
        {
            m_lastConnectTime = Time.time;
            m_storeManager = GetComponent<ProductStoreManager>();

			foreach (var item in m_storeManager.m_storeItems) 
			{
				AndroidInAppPurchaseManager.Client.AddProduct (item.storeId);
			}

            AndroidInAppPurchaseManager.ActionRetrieveProducsFinished += OnRetrieveProductsFinished;
            AndroidInAppPurchaseManager.ActionProductPurchased += OnProductPurchased;
            AndroidInAppPurchaseManager.ActionBillingSetupFinished += OnBillingConnected;
            AndroidInAppPurchaseManager.ActionProductConsumed += OnProductConsumed;
        }

        //-----------------------------------------------------------
		public void PurchaseProduct(string productIdentifier)
        {
			AndroidInAppPurchaseManager.Client.Purchase(productIdentifier);
        }
		//----------------------------------------------------------------
		//-------------------------------------------------------------
        //---------------------------------------------------------------
		public void ConsumeProduct(string productIdentifier)
        {
			AndroidInAppPurchaseManager.Client.Consume(productIdentifier);
			Debug.Log("consuming: " + productIdentifier);
        }
        //------------------------------------------------------------
        public void ConnectToStore()
        {
            AndroidInAppPurchaseManager.Client.Connect();
        }
        //------------------------------------------------------------
        public void RestorePurchases()
        {
			Debug.LogError ("Shouldnt be here");
        }
        //-------------------------------------------------------------
        void OnBillingConnected(BillingResult result)
        {
            AndroidInAppPurchaseManager.ActionBillingSetupFinished -= OnBillingConnected;

            if (result.IsSuccess)
            {
                m_storeManager.ConnectedToStore();

				if (!restored) 
				{
                    m_storeManager.RestoreTransactionsStarted();
                    AndroidInAppPurchaseManager.Client.RetrieveProducDetails();
				}
            }

            Debug.Log("Connection Responce: " + result.Response.ToString() + " " + result.Message);
        }
        //--------------------------------------------------------------------------
        void OnProductPurchased(BillingResult billResult)
        {

            if (billResult.Response == BillingResponseCodes.BILLING_RESPONSE_RESULT_OK)
            {
                StoreTransactionInfo transactionInfo = new StoreTransactionInfo();
                transactionInfo.base64EncodedTransactionReceipt = billResult.Purchase.Signature;
                transactionInfo.productIdentifier = billResult.Purchase.SKU;
                transactionInfo.transactionIdentifier = billResult.Purchase.Token;

                Debug.Log(transactionInfo.base64EncodedTransactionReceipt + " : " + transactionInfo.productIdentifier + " : " + transactionInfo.transactionIdentifier);

                m_storeManager.PurchaseSuccessful(transactionInfo);
            }
            else
            {
                m_storeManager.PurchaseCancelled(billResult.Message);
                Debug.Log("Purchase fail" + billResult.Response);
            }

        }
        //-------------------------------------------------------------
        int tries = 0;
        void OnProductConsumed(BillingResult result)
        {
            if (result.IsSuccess)
            {
                tries = 0;
                Debug.Log("Consumed With Sucesss " + result.Purchase.SKU);
            }
            else
            {
                if (tries < 3)
                {
                    Debug.LogWarning("Consume Failed " + result.Purchase.SKU);
                    ConsumeProduct(result.Purchase.SKU);
                }
                tries++;
            }
        }
        
        //------------------------------------------------------------
        void OnRetrieveProductsFinished(BillingResult e)
        {
            BillingResult result = e;
            AndroidInAppPurchaseManager.ActionRetrieveProducsFinished -= OnRetrieveProductsFinished;


            if (result.IsSuccess)
            {
                DoRestorePurchases();
                Debug.Log("Connection Responce" + ":" + result.Message);
            }
            else
            {
                Debug.Log("Connection Responce333333 " + result.Message.ToString() + " : " +result.Response);
            }
        }
        //-------------------------------------------------------------
        void DoRestorePurchases()
        {
            AndroidInventory inventory = AndroidInAppPurchaseManager.Client.Inventory;
            foreach (GooglePurchaseTemplate restoredPurchases in inventory.Purchases)
            {
                Debug.Log("AAAA" + restoredPurchases.SKU);
                GooglePurchaseTemplate restoredPurchase = inventory.GetPurchaseDetails(restoredPurchases.SKU);
                Debug.Log("BBBBB" + restoredPurchase.SKU + " : " + restoredPurchase.Signature + " : " + restoredPurchase.Token + " : ");
                Debug.Log("CCCCC" + restoredPurchase.OriginalJson);

                StoreTransactionInfo transactionInfo = new StoreTransactionInfo();
                transactionInfo.transactionIdentifier = restoredPurchase.Signature;
                transactionInfo.productIdentifier = restoredPurchase.SKU;

                

                m_storeManager.PurchaseSuccessful(transactionInfo);

                //            GoogleProductTemplate 
                //          inventory.purchases[restoredProduct.SKU];
            }
            m_storeManager.RestoreTransactionsFinished();
        }
    }
}
