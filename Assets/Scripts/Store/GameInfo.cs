using System.Collections.Generic;
using System.Runtime.Serialization;
using System;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

//c# lack of friend workaround
namespace FriendClassesOnlyForPrefsDataFacade
{
    //CLOUD RESTRICTION:
    // -- enum values must be in ascending order, and continuous starting from zero
    // -- removing enum wont cause error if the list wont be reversed


    [Serializable]
    public class CarStoreInfo : ISerializable
    {
        public CarDefinitions.CarEnum carEnum
        {
            get { return (CarDefinitions.CarEnum)carEnumAntiHack.p_value; }
        }

        public bool isBought
        {
            get
            {
                return boughtAntiHack.p_value == boughtAntiHackVal;
            }
        }

        int boughtAntiHackVal = new System.Random().Next(2, 9999999);
        AntiHackInt carEnumAntiHack;
        AntiHackInt boughtAntiHack;


        public CarStoreInfo(CarDefinitions.CarEnum carEnum, bool isBought = false)
        {
            carEnumAntiHack = new AntiHackInt();
            boughtAntiHack = new AntiHackInt();

            carEnumAntiHack.p_value = (int)carEnum;
            if (isBought)
            {
                BuyThisCar();
            }
        }


        public CarStoreInfo(SerializationInfo info, StreamingContext context)
        {
            carEnumAntiHack = new AntiHackInt();
            boughtAntiHack = new AntiHackInt();

            //Add variable here if 

            int variablesFound = 0;
            foreach (SerializationEntry entry in info)
            {
                variablesFound++;
                switch (entry.Name)
                {
                    case "carInt":
                        int carEnumInt = (int)entry.Value;
                        var lastEnum = Enum.GetValues(typeof(CarDefinitions.CarEnum)).Cast<CarDefinitions.CarEnum>().Last();

                        if (carEnumInt <= (int)lastEnum)
                        {
                            carEnumAntiHack.p_value = carEnumInt;
                        }
                        else
                        {
                            UnityEngine.Debug.LogWarning("Error, Bad enum when parsing");
                        }
                        break;
                    case "isBought":
                        if (System.Convert.ToBoolean(entry.Value))
                        {
                            BuyThisCar();
                        }
                        break;
                    default:
                        DL.LogWarning(this.GetType().Name + " Variable doesnt exists " + entry.Name + " , " + entry.Value);
                        break;
                }
            }
            DL.Log(this.GetType().Name + " variables found " + variablesFound + " carEnum " + carEnum + " , isBought " + isBought);
        }

        public void BuyThisCar()
        {
            boughtAntiHack.p_value = boughtAntiHackVal;
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("carInt", carEnumAntiHack.p_value, typeof(int));
            info.AddValue("isBought", isBought, typeof(bool));
        }
    }

    [Serializable]
    public class GameVariable : ISerializable
    {

        AntiHackInt totalCashAntiHack;
        AntiHackInt recordAntiHack;
        AntiHackInt totalDistanceAntiHack;
        AntiHackString nextTVOSFreeGiftTime;

        public GameVariable()
        {
            InitVarWithDefault();
        }

        public GameVariable(SerializationInfo info, StreamingContext context)
        {
            InitVarWithDefault();
            int variablesFound = 0;

            foreach (SerializationEntry entry in info)
            {
                variablesFound++;
                switch (entry.Name)
                {
                    case "totalCash":
                        totalCashAntiHack.p_value = System.Convert.ToInt32(entry.Value);
                        break;
                    case "record":
                        recordAntiHack.p_value = System.Convert.ToInt32(entry.Value);
                        break;
                    case "totalDistance":
                        totalDistanceAntiHack.p_value = System.Convert.ToInt32(entry.Value);
                        break;
                    case "nextTVOSGiftTime":
                        System.DateTime dateTime = System.DateTime.UtcNow;
                        if (System.DateTime.TryParse(entry.Value.ToString(), out dateTime))
                        {
                            nextTVOSFreeGiftTime.p_value = dateTime.ToString();
                        }
                        else
                        {
                            nextTVOSFreeGiftTime.p_value = "NO_VALUE";
                        }
                        break;
                    default:
                        DL.LogWarning(this.GetType().Name + " Variable doesnt exists " + entry.Name + " , " + entry.Value);
                        break;
                }
            }
            DL.Log(this.GetType().Name + " variables found " + variablesFound + " totalCash " + TotalCash);
        }

        void InitVarWithDefault()
        {
            totalCashAntiHack = new AntiHackInt();
            totalCashAntiHack.p_value = 0;
            recordAntiHack = new AntiHackInt();
            recordAntiHack.p_value = 0;
            totalDistanceAntiHack = new AntiHackInt();
            totalDistanceAntiHack.p_value = 0;
            nextTVOSFreeGiftTime = new AntiHackString();
            nextTVOSFreeGiftTime.p_value = "NO_VALUE";
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("totalCash", TotalCash, typeof(int));
            info.AddValue("record", Record, typeof(int));
            info.AddValue("totalDistance", TotalDistance, typeof(int));
            info.AddValue("nextTVOSGiftTime", NextTVOSFreeGiftTime, typeof(string));
        }

        public int TotalCash
        {
            get { return totalCashAntiHack.p_value; }
            set { totalCashAntiHack.p_value = value; }
        }

        public int Record
        {
            get { return recordAntiHack.p_value; }
            set { recordAntiHack.p_value = value; }
        }
        public int TotalDistance
        {
            get { return totalDistanceAntiHack.p_value; }
            set { totalDistanceAntiHack.p_value = value; }
        }

        public string NextTVOSFreeGiftTime
        {
            get { return nextTVOSFreeGiftTime.p_value; }
            set { nextTVOSFreeGiftTime.p_value = value; }
        }
    }

   /* [Serializable]
    public class OtherGameInfo : ISerializable
    {
        public const int LOCKED = 0;
        public const int UNLOCKED = 1;
        public const int COMPLETED = 2;

        public GameplayMode mode;
        public List<int> levelsInfo;
        public List<float> levelsProgress;

        public OtherGameInfo(GameplayMode gameplayMode)
        {
            mode = gameplayMode;
            InitVarWithDefault();
        }

        public OtherGameInfo(SerializationInfo info, StreamingContext context)
        {
            InitVarWithDefault();

            foreach (SerializationEntry entry in info)
            {
                switch (entry.Name)
                {
                    case "modeInt":
                        int modeInt = (int)entry.Value;
                        var lastEnum = Enum.GetValues(typeof(GameplayMode)).Cast<GameplayMode>().Last();

                        if (modeInt <= (int)lastEnum)
                        {
                            mode = (GameplayMode)modeInt;
                        }
                        else
                        {
                            UnityEngine.Debug.LogWarning("Error, Bad enum when parsing other game");
                        }
                        break;
                    case "levelsInfo":
                        levelsInfo = (entry.Value as int[]).ToList(); ;
                        break;
                    case "levelsProgress":
                        levelsProgress = (entry.Value as float[]).ToList(); ;
                        break;
                    default:
                        UnityEngine.Debug.LogWarning(this.GetType().Name + " Variable doesnt exists " + entry.Name + " , " + entry.Value);
                        break;
                }
            }
        }

        void InitVarWithDefault()
        {
            levelsInfo = new List<int>();
            levelsProgress = new List<float>();
            levelsInfo.Add(UNLOCKED);
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("modeInt", (int)mode, typeof(int));
            info.AddValue("levelsInfo", levelsInfo.ToArray(), typeof(int[]));
            info.AddValue("levelsProgress", levelsProgress.ToArray(), typeof(float[]));
        }
    }*/

    [Serializable]
    public class GameInfo : ISerializable
    {
        public List<CarStoreInfo> carStoreInfoList { get; private set; }
        public GameVariable gameVariable { get; private set; }
        /*private byte[] otherGamesInfoData;

        [NonSerialized]
        public List<OtherGameInfo> otherGamesInfo;*/


        public GameInfo()
        {
            InitVarWithDefault();
        }

        public GameInfo(SerializationInfo info, StreamingContext context)
        {
            InitVarWithDefault();

            int variablesFound = 0;
            foreach (SerializationEntry entry in info)
            {
                variablesFound++;
                switch (entry.Name)
                {
                    case "carStoreInfoArray":
                        carStoreInfoList = (entry.Value as CarStoreInfo[]).ToList(); ;
                        break;
                    case "gameVariable":
                        gameVariable = entry.Value as GameVariable;
                        break;
                    /*case "otherGamesInfo":
                        try
                        {
                            otherGamesInfoData = entry.Value as byte[];
                            otherGamesInfo = DeserializeOtherGamesData(otherGamesInfoData);
                        }
                        catch (Exception)
                        {
                            DL.Log("Error in OtherGamesDeserialization");
                        }
                        break;*/

                    default:
                        UnityEngine.Debug.LogWarning(this.GetType().Name + " Variable doesnt exists " + entry.Name + " , " + entry.Value);
                        break;
                }
            }
            DL.Log(this.GetType().Name + " variables found " + variablesFound + " carStoreInfoList " + carStoreInfoList + " , gameVariable " + gameVariable);
        }

        void InitVarWithDefault()
        {
            gameVariable = new GameVariable();

            carStoreInfoList = new List<CarStoreInfo>();
            foreach (CarDefinitions.CarEnum carEnum in System.Enum.GetValues(typeof(CarDefinitions.CarEnum)))
            {
                //add all cars, and unlock first from default
                carStoreInfoList.Add(new CarStoreInfo(carEnum, carEnum == CarDefinitions.CarEnum.Car05));
            }

            /*otherGamesInfo = new List<OtherGameInfo>();
            foreach (GameplayMode gameplayMode in System.Enum.GetValues(typeof(GameplayMode)))
            {
                otherGamesInfo.Add(new OtherGameInfo(gameplayMode));
            }*/
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("carStoreInfoArray", carStoreInfoList.ToArray(), typeof(CarStoreInfo[]));
            info.AddValue("gameVariable", gameVariable, typeof(GameVariable));

           /* otherGamesInfoData = CommonMethods.Serialize(otherGamesInfo);
            info.AddValue("otherGamesInfo", otherGamesInfoData.ToArray(), typeof(byte[]));*/
        }

        public void PreserveCompability()
        {
            CarStoreInfo defaultCar = carStoreInfoList.FirstOrDefault(v => v.carEnum == CarDefinitions.CarEnum.Car05);

            if (defaultCar == null)
            {
                carStoreInfoList.Insert(0, new CarStoreInfo(CarDefinitions.CarEnum.Car05, true));
            }
            else if (!defaultCar.isBought)
            {
                defaultCar.BuyThisCar();
            }

            List<CarDefinitions.CarEnum> missingEnums =
            (
                from carsEnum in Enum.GetValues(typeof(CarDefinitions.CarEnum)).Cast<CarDefinitions.CarEnum>()
                where !carStoreInfoList.Any(v => v.carEnum == carsEnum)
                select carsEnum
            ).ToList();

            missingEnums.ForEach(
                missedCarEnum =>
                {
                    DL.LogWarning("Missed car added " + missedCarEnum);
                    carStoreInfoList.Add(new CarStoreInfo(missedCarEnum));
                }
            );


            /*List<GameplayMode> missingGameplayEnums =
            (
                from gameplayMode in Enum.GetValues(typeof(GameplayMode)).Cast<GameplayMode>()
                where !otherGamesInfo.Any(v => v.mode == gameplayMode)
                select gameplayMode
            ).ToList();

            missingGameplayEnums.ForEach(
                missedGameplayEnum =>
                {
                    DL.LogWarning("Missed gameplay added " + missedGameplayEnum);
                    otherGamesInfo.Add(new OtherGameInfo(missedGameplayEnum));
                }
            );*/
        }

        /*private List<OtherGameInfo> DeserializeOtherGamesData(byte[] stream)
        {
            BinaryFormatter formatter = new BinaryFormatter();

            using (MemoryStream memoryStream = new MemoryStream(stream))
            {
                return formatter.Deserialize(memoryStream) as List<OtherGameInfo>;
            }
        }*/

    }
}
