﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class InAppPromoMiniOffer : InAppPromoDependent {

    
    [SerializeField]
    private GameObject holder;
    [SerializeField]
    private GameObject loadingObject;
    [SerializeField]
    private StartScreenKeysManager startScreenKeysManager;
    [SerializeField]
    private StartGameSceneManager startGameSceneManager;

    private IAPManager.Products productType;

    public void OnPointerDown()
    {
        startScreenKeysManager.Block();
    }

    public void OnPointerUp()
    {
        startScreenKeysManager.Unblock();
    }

    public void OnPromoButtonClick()
    {
        startGameSceneManager.HideTapToPlay();
        PrefabReferences.Instance.IAPManager.SetLoadingObject(loadingObject);
        PremiumStore.BuyProduct(productType);
    }

    protected override void RefreshVisual(bool isPromo, IAPManager.Products promoProduct, InAppPromoManager.IsProductInPromoDelegate IsProductInPromo)
    {
        bool visible = isPromo && GTMParameters.InAppPromoHalfDiscountInStartScreenVisible;

        holder.SetActive(visible);

        if(visible)
        {
            productType = promoProduct;
            InAppInfoItem infoItem = PrefabReferences.Instance.InAppInfoDB.GetInAppInfoItem(productType);

            RefreshVisualBase(isPromo, infoItem);
        }  
    }

   
}
