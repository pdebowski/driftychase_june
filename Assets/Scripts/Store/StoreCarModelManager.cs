using UnityEngine;
using System.Collections;
using System.Linq;

public class StoreCarModelManager : MonoBehaviour
{

    [SerializeField]
    float rotationSpeed;
    [SerializeField]
    Canvas canvas;
    [SerializeField]
    Camera camera;


    void Update()
    {
        transform.Rotate(new Vector3(0, Time.deltaTime * rotationSpeed, 0));
    }

    public void SetCarModel(CarDefinitions.CarEnum selectedCar)
    {
        CarStats carStats = PrefabReferences.Instance.CarStatsDB.GetCarStats(selectedCar);
        PlayerSpeedManager playerSpeedManager = PrefabReferences.Instance.Player.GetComponent<PlayerSpeedManager>();
        bool isUnlocked = PrefsDataFacade.Instance.GetRecord() >= playerSpeedManager.GetScoreInWorld(carStats.startWorld);

        DestroyOld();

        if (PrefsDataFacade.Instance.IsCarBought(selectedCar) || isUnlocked)
        {
            AdjustModel(selectedCar);
        }
    }

    private void AdjustModel(CarDefinitions.CarEnum selectedCar)
    {
        GameObject carModel = GameObject.Instantiate(Resources.Load(selectedCar.ToString()) as GameObject);
        carModel.transform.SetParent(transform);
        carModel.transform.localScale = new Vector3(1, 1, 1);
        carModel.transform.localPosition = new Vector3(0, 0, 0);
        carModel.transform.localRotation = Quaternion.identity;
        canvas.renderMode = RenderMode.ScreenSpaceCamera;
        canvas.worldCamera = camera;

        GameObject carLights = carModel.GetComponent<PlayerCarModel>().CarLights;
        if (carLights != null)
        {
            carLights.SetActive(false);
        }
        else
        {
            Debug.LogError("No lights in PlayerCarModel");
        }
    }

    void DestroyOld()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            Destroy(transform.GetChild(i).gameObject);
        }
    }
}
