﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;


public class StoreItemInteraction : MonoBehaviour
{

    public Text nameText;
    public Image iconImage;
    public Text checkpointText;
    public Text luckText;
    public Text priceText;
    public StoreItem storeItem;
    public GameObject loadingObject;

    [HideInInspector]
    public CarDefinitions.CarEnum carEnum;

    private StoreSceneManager storeSceneManager;

    public void OnBuyButtonClick()
    {
        if (storeItem.IsPremiumCar())
        {
            PrefabReferences.Instance.IAPManager.SetLoadingObject(loadingObject);
            PrefabReferences.Instance.IAPManager.BuyProduct(storeItem.GetIAPProduct());
        }
        else
        {
             StoreManager.Instance.BuyCar(carEnum);
        } 
    }

    public void OnCarImageButtonClicked()
    {
        if (PrefsDataFacade.Instance.IsCarBought(carEnum))
        {
            storeSceneManager.OnBackButtonClick();
        }
    }

    public void SetStoreSceneManager(StoreSceneManager storeSceneManager)
    {
        this.storeSceneManager = storeSceneManager;
    }


}
