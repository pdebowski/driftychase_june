﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class CarStatsDB : MonoBehaviour
{

    [SerializeField]
    private bool debugOverride;

    [SerializeField]
    private List<CarStats> carStatsList;
    [SerializeField]
    private List<CarStats> specialCarsStats;

    [SerializeField]
    private List<CarDefinitions.CarEnum> carsOrder;

    void OnEnable()
    {
        if (Debug.isDebugBuild && debugOverride == true)
        {
            return;
        }


        RemoteCarStatsDBv2 remoteCarsDB = GTMParameters.CarStatsDB;

        if (remoteCarsDB != null)
        {
            int carId = 0;
            foreach (CarStats carStats in carStatsList)
            {
                RemoteCarStatsDB.CarStats remoteData = remoteCarsDB.GetStats(carsOrder[carId]);
                if (remoteData != null)
                {
                    carStats.ReadData(remoteData);
                }
                ++carId;
            }
        }
        else
        {
            CrimsonError.LogError("Couldn't obtain RemoteCarsDB");
        }

        SnowMobileFilter();
        SpecialCarsFilter();

    }

    public CarStats GetCarStats(CarDefinitions.CarEnum carEnum)
    {
        return carStatsList[carsOrder.IndexOf(carEnum)];
    }

    public List<CarDefinitions.CarEnum> GetCarsOrder()
    {
        return carsOrder;
    }

    public CarStats GetBestOwnedCar()
    {
        CarStats result = carStatsList[0];
        foreach (var item in carsOrder)
        {
            if (PrefsDataFacade.Instance.IsCarBought(item))
            {
                result = GetCarStats(item);
            }
        }
        return result;
    }

    public int GetBestOwnedCarIndex()
    {
        int indexOfBestOwned = 0;

        for (int i = 0; i < carsOrder.Count; i++)
        {
            if (PrefsDataFacade.Instance.IsCarBought(carsOrder[i]))
            {
                indexOfBestOwned = i;
            }
        }

        return indexOfBestOwned;
    }

    public int GetCarIndex(CarDefinitions.CarEnum carEnum)
    {
        return carsOrder.IndexOf(carEnum);
    }

    public int GetNextCarToBuyCost()
    {
        if (IsNextCarToBuyAvailable())
        {
            return carStatsList[GetNextCarToBuyIndex()].GetPrice();
        }
        else
        {
            return 0;
        }
    }

    public bool IsNextCarToBuyAvailable()
    {
        return GetNextCarToBuyIndex() < carsOrder.Count;
    }

    private void SnowMobileFilter()
    {
        if (!PrefsDataFacade.Instance.IsCarBought(CarDefinitions.CarEnum.Car12))
        {
            int index = carsOrder.IndexOf(CarDefinitions.CarEnum.Car12);
            carStatsList.RemoveAt(index);
            carsOrder.Remove(CarDefinitions.CarEnum.Car12);
        }
    }

    private void SpecialCarsFilter()
    {
        AddCarIfBought(CarDefinitions.CarEnum.Car19, specialCarsStats[0]);
        AddCarIfBought(CarDefinitions.CarEnum.Car20, specialCarsStats[1]);
    }

    private void AddCarIfBought(CarDefinitions.CarEnum car, CarStats stats)
    {
        if (PrefsDataFacade.Instance.IsCarBought(car))
        {
            carsOrder.Add(car);
            carStatsList.Add(stats);
        }
    }

    private int GetNextCarToBuyIndex()
    {
        int indexOfBestBought = 0;

        for (int i = 0; i < carsOrder.Count; i++)
        {
            if (PrefsDataFacade.Instance.IsCarBought(carsOrder[i]))
            {
                indexOfBestBought = i;
            }
        }

        return indexOfBestBought + 1;
    }
}

[System.Serializable]
public class CarStats
{
    public int startWorld = 0;
    public float cashMultiplier = 1f;
    [SerializeField]
    private int price = 1000;
    public int freeGiftValue = 1000;
    public int videoGiftMinValue = 100;
    public int videoGiftMaxValue = 200;
    public int scoreRequired;

    public void ReadData(RemoteCarStatsDB.CarStats remoteStats)
    {
        price = remoteStats.Price;
        cashMultiplier = remoteStats.CashMultiplier;
        freeGiftValue = remoteStats.FreeGiftValue;
        videoGiftMinValue = remoteStats.GiftMinValue;
        videoGiftMaxValue = remoteStats.GiftMaxValue;
        scoreRequired = remoteStats.ScoreRequired;
    }

    public int GetPrice()
    {
        return VIP.IsVIPAccount ? price / 2 : price;
    }
}
