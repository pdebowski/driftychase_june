using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class StoreBuilder : MonoBehaviour
{
    [SerializeField]
    private GameObject storeItem;
    [SerializeField]
    private StoreDB storeDB;
    [SerializeField]
    private StoreSceneManager storeSceneManager;
    [SerializeField]
    private StorePageAnimation storePageAnimation;
    [SerializeField]
    private Sprite lockedCarImage;
    [SerializeField]
    private GameObject loadingObject;

    public void Initialize()
    {
        storePageAnimation.Init();
    }

    public void SetItem(CarDefinitions.CarEnum carEnum)
    {
        StoreItem storeDBItem = storeDB.GetItem(carEnum);
        CarStats carStats = PrefabReferences.Instance.CarStatsDB.GetCarStats(carEnum);

        string worldName = PrefabReferences.Instance.WorldName.GetWorldName(PrefabReferences.Instance.WorldOrder.GetWorldType(carStats.startWorld));
        bool isRequiredScore = PrefsDataFacade.Instance.GetRecord() < carStats.scoreRequired;
        bool isCarLocked = !PrefsDataFacade.Instance.IsCarBought(carEnum) && isRequiredScore;
        bool isCheckpointAvailable = carStats.startWorld > 0;

        StoreItemInteraction storeItemInteraction = this.storeItem.GetComponent<StoreItemInteraction>();
        storeItemInteraction.storeItem = storeDBItem;
        storeItemInteraction.loadingObject = loadingObject;
        SetCarPrice(storeItemInteraction, isCarLocked, carStats, storeDBItem);
        storeItemInteraction.nameText.text = storeDBItem.GetName();
        storeItemInteraction.iconImage.sprite = isCarLocked ? lockedCarImage : storeDBItem.GetIcon();
        storeItemInteraction.checkpointText.text = isCarLocked ? "Unknown" : worldName;
        storeItemInteraction.luckText.text = "Cash +" + (isCarLocked ? "??" : Mathf.RoundToInt((carStats.cashMultiplier - 1) * 100).ToString()) + "%";
        storeItemInteraction.carEnum = carEnum;
        storeItemInteraction.SetStoreSceneManager(storeSceneManager);


        this.storeItem.GetComponent<StoreItemVisual>().Set(carEnum, PrefsDataFacade.Instance.IsCarBought(carEnum), isCarLocked, isCheckpointAvailable, carStats.scoreRequired);
    }

    private void SetCarPrice(StoreItemInteraction storeItemInteraction, bool isCarLocked, CarStats carStats, StoreItem storeItem)
    {
        if (isCarLocked)
        {
            storeItemInteraction.priceText.text = "??? ???";
        }
        else
        {
            if (storeItem.IsPremiumCar())
            {
                storeItemInteraction.priceText.text = "$" + storeItem.GetPremiumCarPrice();
            }
            else
            {
                storeItemInteraction.priceText.text = CommonMethods.GetCashStringWithSpacing(carStats.GetPrice()) + "   &";
            }
        }
    }
}