﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class StoreItem : MonoBehaviour, IStoreItem
{
    [SerializeField]
    private CarDefinitions.CarEnum carEnum;
    [SerializeField]
    private string Name;
    [SerializeField]
    private Sprite Icon;
    [Header("IAP Setting")]
    [SerializeField]
    private IAPStoreItem IAPProductInfo;

    public CarDefinitions.CarEnum GetCarEnum()
    {
        return carEnum;
    }

    public Sprite GetIcon()
    {
        return Icon;
    }

    public string GetName()
    {
        return Name;
    }

    public bool IsPremiumCar()
    {
        return IAPProductInfo.isPremiumCar;
    }

    public float GetPremiumCarPrice()
    {
        return IAPProductInfo.premiumCarPrice;
    }

    public IAPManager.Products GetIAPProduct()
    {
        return IAPProductInfo.iapProduct;
    }

    [System.Serializable]
    struct IAPStoreItem
    {       
        public bool isPremiumCar;
        public float premiumCarPrice;
        public IAPManager.Products iapProduct;
    }
}
