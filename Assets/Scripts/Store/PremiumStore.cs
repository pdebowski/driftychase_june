﻿using UnityEngine;
using System.Collections;

public class PremiumStore : MonoBehaviour
{
    [SerializeField]
    private GameObject loadingObject;

    void Awake()
    {
        PrefabReferences.Instance.IAPManager.SetLoadingObject(loadingObject);
    }

    public static void BuyProduct(IAPManager.Products product)
    {
        if (product == IAPManager.Products.VipAccount && VIP.IsVIPAccount)
        {
            return;
        }

        InAppPromoManager promoManager = PrefabReferences.Instance.InAppPromoManager;
        bool isProductInPromo = promoManager.IsProductInPromo(product);

        if (!isProductInPromo)
        {
            PrefabReferences.Instance.IAPManager.BuyProduct(product);
        }
        else
        {
            string promoProductEnumString = product.ToString() + "_Promo";

            if (System.Enum.IsDefined(typeof(IAPManager.Products), promoProductEnumString))
            {
                IAPManager.Products discountedProduct = (IAPManager.Products)System.Enum.Parse(typeof(IAPManager.Products), promoProductEnumString);
                PrefabReferences.Instance.IAPManager.BuyProduct(discountedProduct);
            }
            else
            {
                PrefabReferences.Instance.IAPManager.BuyProduct(product);
            }
        }
    }

}
