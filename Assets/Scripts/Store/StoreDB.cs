﻿using UnityEngine;
using System.Collections.Generic;
using System;
using System.Linq;


public class StoreDB : MonoBehaviour, IStoreDB {

	public StoreItem GetItem(CarDefinitions.CarEnum carEnum)
	{
		return transform.GetComponentsInChildren<StoreItem>().ToList().Find(x => x.GetCarEnum() == carEnum);
	}

	public int GetItemCount()
	{
		return transform.childCount;
	}

	public List<StoreItem> GetItems()
	{
		return transform.GetComponentsInChildren<StoreItem>().ToList();
	}
}
