﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class StoreItemVisual : MonoBehaviour
{
    [SerializeField]
    private Text requiredScoreText;
    [SerializeField]
    private GameObject boughtStamp;
    [SerializeField]
    private CheckpointPanelAdjuster checkpointPanelAdjuster;
    [SerializeField]
    private GameObject driveButton;
    [SerializeField]
    private GameObject priceText;

    private StoreItemInteraction interaction;
    private System.DateTime lastTimeUsedAd;

    public void Set(CarDefinitions.CarEnum carEnum, bool isBought, bool isLocked, bool isCheckpointAvailable, int requiredScore)
    {
        RefreshVisual(carEnum, isBought, isLocked);

        requiredScoreText.text = requiredScore.ToString() + " Required";
        checkpointPanelAdjuster.Set(!isLocked, isBought, carEnum, isCheckpointAvailable);
    }

    public void Set(CarDefinitions.CarEnum carEnum, bool isBought, bool isLocked)
    {
        RefreshVisual(carEnum, isBought, isLocked);
        checkpointPanelAdjuster.Set(!isLocked, isBought, carEnum);
    }

    private void RefreshVisual(CarDefinitions.CarEnum carEnum, bool isBought, bool isLocked)
    {
        SetActiveStateIfExist(boughtStamp, isBought, false);
        SetActiveStateIfExist(driveButton, false, isBought);
        SetActiveStateIfExist(priceText, true, !isBought);
    }

    private void SetActiveStateIfExist(GameObject element, bool onTV, bool notOnTV)
    {
        if (element != null)
        {
            element.SetActive(PlatformRecognition.IsTV ? onTV : notOnTV);
        }
    }
}
