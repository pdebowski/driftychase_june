﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Collections;

public class StoreManager : Singleton<StoreManager>
{
    public static System.Action<CarDefinitions.CarEnum> OnCarBought = delegate { };
    public static System.Action OnNextCarShown = delegate { };

    [SerializeField]
    private StoreDB storeDB;
    [SerializeField]
    private StoreBuilder storeBuilder;
    [SerializeField]
    private StoreSceneManager storeSceneManager;
    [SerializeField]
    private Text cashValue;
    [SerializeField]
    private StorePageAnimation pageAnimation;
    [SerializeField]
    private GameObject storeItem;
    [SerializeField]
    private StoreCarModelManager storeCarModelManager;
    [SerializeField]
    private MenuNavigation.StoreItemScope checkpointButtonsScope;

    private CarDefinitions.CarEnum selectedCar;

    void Start()
    {
        selectedCar = PlayerPrefsAdapter.CarToSelectOnStoreEnter;
        storeBuilder.Initialize();
        storeBuilder.SetItem(selectedCar);
        ShowCarModel(selectedCar);
        RefreshCoins();
        SetNavigationButtonsStates();
    }

    void OnDisable()
    {
        PlayerPrefsAdapter.CarToSelectOnStoreEnter = selectedCar;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            ShowPreviousCar();
        }
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            ShowNextCar();
        }
    }

    public void ShowNextCar()
    {
        if (IsNextCarAvailable() && !pageAnimation.IsDuringAnimation)
        {
            pageAnimation.PrepareEffect();
            pageAnimation.NextPage();

            StartCoroutine(NextPageWithDelay());
        }
    }

    public void ShowPreviousCar()
    {
        if (IsPreviousCarAvailable() && !pageAnimation.IsDuringAnimation)
        {
            ShowPrevious();
            pageAnimation.PrepareEffect();
            ShowNext();
            pageAnimation.PreviousPage();

            StartCoroutine(PreviousPageWithDelay());
        }
    }

    public IEnumerator NextPageWithDelay()
    {
        yield return null;
        ShowNext();
    }

    private IEnumerator PreviousPageWithDelay()
    {
        yield return new WaitForSeconds(pageAnimation.Duration);
        ShowPrevious();
    }

    public void ShowNext()
    {
        List<CarDefinitions.CarEnum> carsOrder = PrefabReferences.Instance.CarStatsDB.GetCarsOrder();
        int selectedCarID = carsOrder.IndexOf(selectedCar);
        selectedCar = carsOrder[Mathf.Clamp(++selectedCarID, 0, carsOrder.Count - 1)];
        storeBuilder.SetItem(selectedCar);
        ShowCarModel(selectedCar);

        if (PlatformRecognition.IsTV && MenuNavigation.NavigationController.Instance.ActiveScope == checkpointButtonsScope)
        {
            MenuNavigation.NavigationController.Instance.Activate(checkpointButtonsScope);
        }
    }

    public void ShowPrevious()
    {
        List<CarDefinitions.CarEnum> carsOrder = PrefabReferences.Instance.CarStatsDB.GetCarsOrder();
        int selectedCarID = carsOrder.IndexOf(selectedCar);
        selectedCar = carsOrder[Mathf.Clamp(--selectedCarID, 0, carsOrder.Count - 1)];
        storeBuilder.SetItem(selectedCar);
        ShowCarModel(selectedCar);

        if (PlatformRecognition.IsTV && MenuNavigation.NavigationController.Instance.ActiveScope == checkpointButtonsScope)
        {
            MenuNavigation.NavigationController.Instance.Activate(checkpointButtonsScope);
        }
    }

    private void ShowCarModel(CarDefinitions.CarEnum enumCarModel)
    {
        if (PlatformRecognition.IsTV)
        {
            storeCarModelManager.SetCarModel(enumCarModel);
        }
    }

    public bool IsNextCarAvailable()
    {

        List<CarDefinitions.CarEnum> carsOrder = PrefabReferences.Instance.CarStatsDB.GetCarsOrder();
        int selectedCarID = carsOrder.IndexOf(selectedCar);
        return selectedCarID < carsOrder.Count - 1;
    }

    public bool IsPreviousCarAvailable()
    {
        List<CarDefinitions.CarEnum> carsOrder = PrefabReferences.Instance.CarStatsDB.GetCarsOrder();
        int selectedCarID = carsOrder.IndexOf(selectedCar);
        return selectedCarID > 0;
    }

    public int GetCarOrderID(CarDefinitions.CarEnum carEnum)
    {
        List<CarDefinitions.CarEnum> carsOrder = PrefabReferences.Instance.CarStatsDB.GetCarsOrder();
        return carsOrder.IndexOf(carEnum);
    }


    private void SetNavigationButtonsStates()
    {
        List<CarDefinitions.CarEnum> carsOrder = PrefabReferences.Instance.CarStatsDB.GetCarsOrder();
        bool isFirstCar = (selectedCar == carsOrder[0]);
        bool isLastCar = (selectedCar == carsOrder[carsOrder.Count - 1]);


        storeItem.GetComponent<PageAnimationParts>().CornerButton.Deactivate(isLastCar);
    }

    public void SetCar()
    {
        if (PrefsDataFacade.Instance.IsCarBought(selectedCar))
        {
            PlayerPrefsAdapter.SelectedCar = selectedCar;
            PrefabReferences.Instance.Player.GetComponent<PlayerCarModelAdapter>().SetPlayerCarFromPrefs();
        }
    }

    public void BuyCar(CarDefinitions.CarEnum carEnum)
    {
        CarStatsDB carStatsDB = PrefabReferences.Instance.CarStatsDB;

        if (!PrefsDataFacade.Instance.IsCarBought(carEnum) &&
            PrefsDataFacade.Instance.GetTotalCash() >= carStatsDB.GetCarStats(carEnum).GetPrice())
        {
            PrefsDataFacade.Instance.AddTotalCash(-carStatsDB.GetCarStats(carEnum).GetPrice());
            BoughtCarAction(carEnum);
        }
        else if (PrefsDataFacade.Instance.GetTotalCash() < carStatsDB.GetCarStats(carEnum).GetPrice())
        {
            storeSceneManager.SetActivePremiumShopPopup(true);
        }
    }

    public void CarBoughtFromIAP(CarDefinitions.CarEnum carEnum)
    {
        BoughtCarAction(carEnum);
    }

    private void BoughtCarAction(CarDefinitions.CarEnum carEnum)
    {
        PrefsDataFacade.Instance.BuyCar(carEnum);
        GetComponentInChildren<StoreItemVisual>().Set(carEnum, true, false);
        RefreshCoins();
        OnCarBought(carEnum);
        PrefsDataFacade.Instance.SaveGameStateCloud();
    }

    private void RefreshCoins()
    {
        cashValue.text = CommonMethods.GetCashStringWithSpacing(PrefsDataFacade.Instance.GetTotalCash());
    }
}