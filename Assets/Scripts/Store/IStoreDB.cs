﻿using UnityEngine;
using System.Collections.Generic;

public interface IStoreDB {

	StoreItem GetItem(CarDefinitions.CarEnum carEnum);
	int GetItemCount();
	List<StoreItem> GetItems();
}
