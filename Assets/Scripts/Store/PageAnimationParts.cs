﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PageAnimationParts : MonoBehaviour
{
    public Transform Page { get { return page; } }
    public RawImage Curtain { get { return curtain; } }
    public CornerButton CornerButton { get { return cornerButton; } }

    [SerializeField]
    private Transform page;
    [SerializeField]
    private RawImage curtain;
    [SerializeField]
    private CornerButton cornerButton;
}
