﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public interface IStoreItem
{
	CarDefinitions.CarEnum GetCarEnum();
	Sprite GetIcon();
	string GetName();
}