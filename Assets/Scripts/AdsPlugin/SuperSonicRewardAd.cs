
//TODO: Analytics to layered


/*

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace CrimsonPlugins.Internal.Ads
{
    public class SuperSonicRewardAd : MonoBehaviour, IRewardAd
    {
        string adId;

        bool adClosedNotInvoked = false;
        System.Action<bool> adResultCallback = null;
        bool isRewarded = false;
        Coroutine performLoadAdCor = null;


        volatile bool perform_onAdLoaded = false;
        volatile bool perform_onAdFailedToLoad = false;
        volatile bool perform_onAdOpening = false;
        volatile bool perform_onAdStarted = false;
        volatile bool perform_onAdClosed = false;
        volatile bool perform_onAdRewarded = false;
        volatile bool perform_onAdLeavingApplication = false;
        volatile bool perform_onInitialized = false;
        volatile bool perform_failInitialize = false;

        public bool IsReady { get { return Supersonic.Agent.isRewardedVideoAvailable(); } }

        //------------------------------------------------
        public void InitializeAndRequest(string adId)
        {
            this.adId = adId;
            Supersonic.Agent.initRewardedVideo(adId, SystemInfo.deviceUniqueIdentifier);



        }
        //----------------------------------------------------------------
        void OnEnable()
        {
            SupersonicEvents.onRewardedVideoAdOpenedEvent += OnRewardedVideoAdOpenedEvent;
            SupersonicEvents.onRewardedVideoAdRewardedEvent += OnRewardedVideoAdRewardedEvent;
            SupersonicEvents.onRewardedVideoAdClosedEvent += OnRewardedVideoAdClosedEvent;
            SupersonicEvents.onRewardedVideoShowFailEvent += RewardedVideoShowFailEvent;
            SupersonicEvents.onVideoAvailabilityChangedEvent += VideoAvailabilityChangedEvent;
            SupersonicEvents.onRewardedVideoInitSuccessEvent += RewardedVideoInitSuccessEvent;
            SupersonicEvents.onRewardedVideoInitFailEvent += RewardedVideoInitFailEvent;

        }
        //----------------------------------------------------------------
        void OnDisable()
        {
            SupersonicEvents.onRewardedVideoAdOpenedEvent -= OnRewardedVideoAdOpenedEvent;
            SupersonicEvents.onRewardedVideoAdRewardedEvent -= OnRewardedVideoAdRewardedEvent;
            SupersonicEvents.onRewardedVideoAdClosedEvent -= OnRewardedVideoAdClosedEvent;
            SupersonicEvents.onRewardedVideoShowFailEvent -= RewardedVideoShowFailEvent;
            SupersonicEvents.onVideoAvailabilityChangedEvent -= VideoAvailabilityChangedEvent;
            SupersonicEvents.onRewardedVideoInitSuccessEvent -= RewardedVideoInitSuccessEvent;
            SupersonicEvents.onRewardedVideoInitFailEvent -= RewardedVideoInitFailEvent;

        }

        void Update()   //BUGFIX -  Thread 
        {
            if (perform_onAdLoaded)
            {
                perform_onAdLoaded = false;
                PerformOnAdLoaded();
            }
            else if (perform_onAdFailedToLoad)
            {
                perform_onAdFailedToLoad = false;
                PerformOnAdFailedToLoad();
            }
            else if (perform_onAdOpening)
            {
                perform_onAdOpening = false;
                PerformOnAdOpening();
            }
            else if (perform_onAdStarted)
            {
                perform_onAdStarted = false;
                PerformOnAdStarted();
            }
            else if (perform_onAdClosed)
            {
                adClosedNotInvoked = false;
                perform_onAdClosed = false;
                PerformOnAdClosed();
            }
            else if (perform_onAdRewarded)
            {
                perform_onAdRewarded = false;
                PerformOnAdRewarded();
            }
            else if (perform_onAdLeavingApplication)
            {
                perform_onAdLeavingApplication = false;
                PerformOnAdLeavingApplication();
            }
            else if (perform_onInitialized)
            {
                perform_onInitialized = false;
                PerformInitialized();
            }
            else if (perform_failInitialize)
            {
                perform_failInitialize = false;
                PerformFailInitialize();
            }
        }

        void NotifyResult() //BUGFIX - stupid sdk implementation (OnReward, OnAdClosed order undef)
        {
            if (adResultCallback != null)
            {
                adResultCallback(isRewarded);
                adResultCallback = null;
            }
        }

        void InvokeCloseIfNeeded() //BUGFIX - AdColony not called OnAdClosed when go to BG
        {
            if (adClosedNotInvoked)
            {
                adClosedNotInvoked = false;
                PerformOnAdClosed();
            }
        }
        //------------------------------------------------------------------
        void RewardedVideoInitSuccessEvent()
        {
            perform_onInitialized = true;
        }
        //----------------------------------------------------------------------
        void RewardedVideoInitFailEvent(SupersonicError error)
        {
            perform_failInitialize=true;
        }
        //----------------------------------------------------------------------
        void VideoAvailabilityChangedEvent(bool canShowAd)
        {
            if (canShowAd)
            {
                perform_onAdLoaded=true;
            }
            else
            {
                perform_onAdFailedToLoad= true;
            }
        }
        //----------------------------------------------------------------------
        void RewardedVideoShowFailEvent(SupersonicError error)
        {
            perform_onAdClosed = true;
        }
        //-----------------------------------------------------------------
        void OnRewardedVideoAdOpenedEvent()
        {
            perform_onAdOpening = true;
            isRewarded = false;
        }
        //-----------------------------------------------------------------
        void OnRewardedVideoAdRewardedEvent(SupersonicPlacement placement)
        {
            perform_onAdRewarded = true;
            isRewarded = true;
        }
        //-----------------------------------------------------------------
        void OnRewardedVideoAdClosedEvent()
        {
            perform_onAdClosed = true;
        }
        //-----------------------------------------------------------------
        void LoadAd()
        {
            Supersonic.Agent.loadInterstitial();
        }
        //-----------------------------------------------------------------
        public void ShowRewardAd(string placement, System.Action<bool> onFinishCallback)
        {
            if (IsReady)
            {
                this.adResultCallback = onFinishCallback;
                adClosedNotInvoked = true;
                isRewarded = false;
                Supersonic.Agent.showRewardedVideo();
            }

        }
        //------------------------------------------------------
#if !UNITY_IOS
        void OnApplicationPause(bool paused)
        {
            if (paused == false && adClosedNotInvoked)
            {
                StartCoroutine(RealTimeInvoke(InvokeCloseIfNeeded, 1.5f));
            }
        }
#endif
        //------------------------------------------------------
        IEnumerator RealTimeInvoke(System.Action func, float duration)
        {
            float t = 0;

            while (t < 1f)
            {
                t += Time.unscaledDeltaTime / duration;
                yield return 0;
            }
            func();
        }
        //-----------------------------------------------------------------
        #region perform callback
        void PerformFailInitialize()
        {
            Supersonic.Agent.initRewardedVideo(this.adId, SystemInfo.deviceUniqueIdentifier);
        }

        void PerformInitialized()
        {
            LoadAd();
        }

        void PerformOnAdLoaded()
        {
            Debug.Log("RewardSonic: OnAdLoaded");
        }

        void PerformOnAdFailedToLoad()
        {
            StartCoroutine(RealTimeInvoke(LoadAd, 60f));
            Debug.Log("RewardSonic: OnAdFailedToLoad");
        }

        void PerformOnAdOpening()
        {
            Debug.Log("RewardSonic: OnAdOpening");
        }

        void PerformOnAdStarted()
        {
            Debug.Log("RewardSonic: OnAdStarted");
        }

        void PerformOnAdClosed()
        {
            if (isRewarded && adResultCallback != null)
            {
                adResultCallback(true);
                adResultCallback = null;
            }
            else
            {
                StartCoroutine(RealTimeInvoke(NotifyResult, 0.8f));
            }

            Debug.Log("RewardSonic: OnAdClosed");
            if (performLoadAdCor == null)
            {
                performLoadAdCor = StartCoroutine(RealTimeInvoke(LoadAd, 2));
            }
        }

        void PerformOnAdRewarded()
        {
            if (adResultCallback != null)
            {
                adResultCallback(true);
                adResultCallback = null;
            }

            Debug.Log("RewardSonic: OnAdRewarded");
        }

        void PerformOnAdLeavingApplication()
        {
            Debug.Log("RewardSonic: OnAdLeavingApplication");
        }
        #endregion


    }
}
*/
