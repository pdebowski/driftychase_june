using UnityEngine;
using System.Collections;
using CrimsonAnalytics;

/*
namespace CrimsonPlugins.Internal.Ads
{
    public class AdMobRewardAd : MonoBehaviour, IRewardAd
    {
        public bool IsReady { get { return RewardBasedVideoAd.Instance.IsLoaded(); } }
        //-----------------------------------------------------------------
        System.Action<bool> adResultCallback = null;
        bool isRewarded = false;


        volatile bool perform_onAdLoaded = false;
        volatile bool perform_onAdFailedToLoad = false;
        volatile bool perform_onAdClosed = false;
        volatile bool perform_onAdRewarded = false;
        volatile bool perform_onAdLeavingApplication = false;


        bool adClosedNotInvoked = false;
        string id;
		Coroutine performLoadAdCor=null;

        bool lastNoInternet = true;
        int fetchFailedCounter = 0;


        void Awake()
        {
            RewardBasedVideoAd.Instance.OnAdLoaded += OnAdLoaded;
            RewardBasedVideoAd.Instance.OnAdFailedToLoad += OnAdFailedToLoad;
            RewardBasedVideoAd.Instance.OnAdClosed += OnAdClosed;
            RewardBasedVideoAd.Instance.OnAdRewarded += OnAdRewarded;
            RewardBasedVideoAd.Instance.OnAdLeavingApplication += OnAdLeavingApplication;
        }

        public void InitializeAndRequest(string id)
        {
            this.id = id;
            LoadAd();
        }


		#if !UNITY_IOS
        void OnApplicationPause(bool paused)
        {
            if (paused == false && adClosedNotInvoked)
            {
				StartCoroutine(RealTimeInvoke(InvokeCloseIfNeeded, 2.5f));
            }
        }
		#endif

        void Update()   //BUGFIX -  Thread 
        {
            if (perform_onAdLoaded)
            {
                perform_onAdLoaded = false;
                PerformOnAdLoaded();
            }
            else if (perform_onAdFailedToLoad)
            {
                perform_onAdFailedToLoad = false;
                PerformOnAdFailedToLoad();
            }
            else if (perform_onAdClosed)
            {
                adClosedNotInvoked = false;
                perform_onAdClosed = false;
                PerformOnAdClosed();
            }
            else if (perform_onAdRewarded)
            {
                perform_onAdRewarded = false;
                PerformOnAdRewarded();
            }
            else if (perform_onAdLeavingApplication)
            {
                perform_onAdLeavingApplication = false;
                PerformOnAdLeavingApplication();
            }
        }

        public void LoadAd()
        {
            RewardBasedVideoAd.Instance.LoadAd(new AdRequest.Builder().Build(), id);
			performLoadAdCor = null;
        }

        public void ShowRewardAd(string placement, System.Action<bool> onFinishCallback)
        {
            if (IsReady)
            {
                MediationTest.AdmobRewarded.ShowVideo.LogEvent(NetworkChecker.Instance.p_isConnectedToInternet ? 1 : 0);
                this.adResultCallback = onFinishCallback;
                adClosedNotInvoked = true;
                isRewarded = false;
                RewardBasedVideoAd.Instance.Show();
            }
        }

        void NotifyResult() //BUGFIX - stupid sdk implementation (OnReward, OnAdClosed order undef)
        {
            if (adResultCallback != null)
            {
                MediationTest.AdmobRewarded.Closed.LogEvent(isRewarded ? 1 : 0);
                adResultCallback(isRewarded);
                adResultCallback = null;
            }
        }

        void InvokeCloseIfNeeded() //BUGFIX - AdColony not called OnAdClosed when go to BG
        {
            if (adClosedNotInvoked)
            {
                adClosedNotInvoked = false;
                PerformOnAdClosed();
            }
        }

		IEnumerator RealTimeInvoke(System.Action func, float duration)
		{
			float t = 0;

			while (t < 1f) 
			{
				t += Time.unscaledDeltaTime / duration;
				yield return 0;
			}
			func ();
		}

        #region perform callback
        void PerformOnAdLoaded()
        {
            MediationTest.AdmobRewarded.Loaded.LogEvent(NetworkChecker.Instance.p_isConnectedToInternet ? 1 : 0);
            Debug.Log("Reward: OnAdLoaded");
        }

        void PerformOnAdFailedToLoad()
        {
            if (fetchFailedCounter < 20)
            {
                fetchFailedCounter++;
            }

            if (NetworkChecker.Instance.p_isConnectedToInternet)
            {
                if (lastNoInternet)
                {
                    fetchFailedCounter = 1;
                }
                lastNoInternet = false;
                MediationTest.AdmobRewarded.FetchFailed.LogEvent(fetchFailedCounter);
            }
            else
            {
                if (!lastNoInternet)
                {
                    fetchFailedCounter = 1;
                }
                lastNoInternet = true;
                MediationTest.AdmobRewarded.FetchFailedNoInternet.LogEvent(fetchFailedCounter);
            }

            if (fetchFailedCounter < 5 && NetworkChecker.Instance.p_isConnectedToInternet)
            {
                StartCoroutine(RealTimeInvoke(LoadAd, 10));
            }
            else
            {
                StartCoroutine(RealTimeInvoke(LoadAd, 60));
            }
            Debug.Log("Reward: OnAdFailedToLoad");
        }

        void PerformOnAdClosed()
        {
            if (isRewarded && adResultCallback != null)
            {
                MediationTest.AdmobRewarded.Closed.LogEvent(isRewarded ? 1 : 0);
                adResultCallback(true);
                adResultCallback = null;
            }
            else
            {
				StartCoroutine(RealTimeInvoke(NotifyResult, 0.8f));
            }

            Debug.Log("Reward: OnAdClosed");

			if (performLoadAdCor == null) 
			{
				performLoadAdCor = StartCoroutine (RealTimeInvoke (LoadAd, 2));
			}
        }

        void PerformOnAdRewarded()
        {
            isRewarded = true;
            Debug.Log("Reward: OnAdRewarded");
        }

        void PerformOnAdLeavingApplication()
        {
            MediationTest.AdmobRewarded.Clicked.LogEvent();
            Debug.Log("Reward: OnAdLeavingApplication");
        }
        #endregion


        #region Callback subscribers
        void OnAdLoaded(object sender, System.EventArgs args)
        {
            perform_onAdLoaded = true;
        }

        void OnAdFailedToLoad(object sender, AdFailedToLoadEventArgs args)
        {
            perform_onAdFailedToLoad = true;
        }

        void OnAdClosed(object sender, System.EventArgs args)
        {
            perform_onAdClosed = true;
        }

        void OnAdRewarded(object sender, Reward reward)
        {
            perform_onAdRewarded = true;
        }

        void OnAdLeavingApplication(object sender, System.EventArgs args)
        {
            perform_onAdLeavingApplication = true;
        }
        #endregion
    }
}
*/
 