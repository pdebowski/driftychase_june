﻿using UnityEngine;
using System.Collections;
using Tapdaq;
using CrimsonAnalytics;

namespace CrimsonPlugins.Internal.Ads
{
    public class TapdaqObject : MonoBehaviour, IAdEvents
    {
        bool requestIntersistialSheduled = false;
        bool requestBannerSheduled = false;

        bool intersfaceInitialized = false;

        bool performShowFailed = false;
        bool lastNoInternet = true;
        int fetchFailedCounter = 0;
        bool reportedBannerLoad = false;
		bool isBannerLoaded=false;

        void OnEnable()
        {

            TDCallbacks.TapdaqConfigLoaded += OnTapdaqConfigLoaded;
            TDCallbacks.AdAvailable += OnAdAvailable;
            TDCallbacks.AdNotAvailable += OnAdNotAvailable;
            TDCallbacks.AdClosed += OnAdClosed;
            TDCallbacks.AdClicked += OnAdClicked;
            TDCallbacks.AdError += OnAdError;
        }
        //------------------------------------------------------------------
        void OnDisable()
        {
            TDCallbacks.TapdaqConfigLoaded -= OnTapdaqConfigLoaded;
            TDCallbacks.AdAvailable -= OnAdAvailable;
            TDCallbacks.AdNotAvailable -= OnAdNotAvailable;
            TDCallbacks.AdClosed -= OnAdClosed;
            TDCallbacks.AdClicked -= OnAdClicked;
            TDCallbacks.AdError -= OnAdError;
        }

        public void InitSdk(string intersistialId, string bannerId)
        {
            AdManager.Init();
        }

        void TestMediation()
        {
            AdManager.LaunchMediationDebugger();
        }


        void OnAdError(TDAdEvent e)
        {

            if (e.adType == "VIDEO")
            {
                Debug.Log("TAPDAQ-STATIC: OnAdError " + e.adType + " : " + e.message);
                MediationTest.Tapdaq_Interstitial.ShowAdFailed.LogEvent();
                MediationTest.Tapdaq_Interstitial.Closed.LogEvent();

                Invoke("RequestInterstitial", 10);
                SendMessage("AdsClosedCallback", SendMessageOptions.RequireReceiver);

            }
        }

        void OnAdClicked(TDAdEvent e)
        {

            if (e.adType == "VIDEO")
            {
                Debug.Log("TAPDAQ-STATIC: OnAdClicked " + e.adType);
                MediationTest.Tapdaq_Interstitial.Clicked.LogEvent();
            }
            else if (e.adType == "BANNER")
            {
                Debug.Log("TAPDAQ-BANNER: OnAdClicked " + e.adType);
                MediationTest.Tapdaq_Banner.Clicked.LogEvent();
            }
        }

        void OnAdClosed(TDAdEvent e)
        {
            if (e.adType == "VIDEO")
            {
                Debug.Log("TAPDAQ-STATIC: OnAdClosed " + e.adType);

                MediationTest.Tapdaq_Interstitial.Closed.LogEvent();
                SendMessage("AdsClosedCallback", SendMessageOptions.RequireReceiver);
            }
        }

        void OnAdAvailable(TDAdEvent e)
        {
            if (e.adType == "VIDEO")
            {
                Debug.Log("TAPDAQ-STATIC: OnAdAvailable " + e.adType);
                MediationTest.Tapdaq_Interstitial.Fetched.LogEvent();
            }
            else if (e.adType == "BANNER")
            {
                Debug.Log("TAPDAQ-BANNER: OnAdAvailable " + e.adType);
				isBannerLoaded = true;
				if (!reportedBannerLoad) {
					reportedBannerLoad = true;
			//		SendMessage("BannerLoaded", SendMessageOptions.RequireReceiver); //Due Ugly bug

					MediationTest.Tapdaq_Banner.AvailableInSession.LogEvent ();
				}

            }
        }

        void OnAdNotAvailable(TDAdEvent e)
        {

            if (e.adType == "VIDEO")
            {
                Debug.Log("TAPDAQ-STATIC: OnAdNotAvailable " + e.adType);

                if (fetchFailedCounter < 20)
                {
                    fetchFailedCounter++;
                }

                if (NetworkChecker.Instance.p_isConnectedToInternet)
                {
                    if (lastNoInternet)
                    {
                        fetchFailedCounter = 1;
                    }
                    lastNoInternet = false;
                    MediationTest.Tapdaq_Interstitial.FetchFailed.LogEvent(fetchFailedCounter);
                }
                else
                {
                    if (!lastNoInternet)
                    {
                        fetchFailedCounter = 1;
                    }
                    lastNoInternet = true;
                    MediationTest.Tapdaq_Interstitial.FetchFailedNoInternet.LogEvent(fetchFailedCounter);
                }

                Invoke("RequestInterstitial", (NetworkChecker.Instance.p_isConnectedToInternet && fetchFailedCounter<5) ? 10 : 60);
            }
			else if (e.adType == "BANNER")
			{
				Debug.Log("TAPDAQ-BANNER: OnAdNotAvailable " + e.adType);
				isBannerLoaded = false;
				Invoke ("RequestBanner", 40f);
			}
        }

        void OnTapdaqConfigLoaded()
        {
            Debug.Log("TAPDAQ-STATIC: OnTapdaqConfigLoaded");

            intersfaceInitialized = true;

            if (requestIntersistialSheduled)
            {
                requestIntersistialSheduled = false; // just in case
                RequestInterstitial();
            }

            if (requestBannerSheduled)
            {
                requestBannerSheduled = false;   // just in case
                RequestBanner();
            }

            if (Debug.isDebugBuild)
            {
                Invoke("TestMediation", 2f);
            }
        }

        public bool IsFullScreenAdLoaded()
        {
            return AdManager.IsVideoReady();
        }


        public void ShowFullscreenAd()
        {
            MediationTest.Tapdaq_Interstitial.ShowAd.LogEvent();
            AdManager.ShowVideo();
        }

		public void RequestInterstitial()
        {
            if (intersfaceInitialized)
            {
                AdManager.LoadVideo();
            }
            else
            {
                requestIntersistialSheduled = true;
            }
        }

        #region BANNER

        public bool IsBannerLoaded()
        {
			return isBannerLoaded;
        }

        public void RequestBanner()
        {
            if (intersfaceInitialized)
            {
				AdManager.RequestBanner(TDMBannerSize.TDMBannerStandard);
            }
            else
            {
                requestBannerSheduled = true;
            }
        }

        public void ShowBannerAd()
        {
            AdManager.ShowBanner(TDBannerPosition.Top);
        }

        public void HideBannerAd()
        {
            AdManager.HideBanner();
        }

        public void ApplicationQuitRequest()
        {
            Application.Quit();
        }
        #endregion
    }
}
