using UnityEngine;
using System.Collections;

namespace CrimsonPlugins.Internal.Ads
{
    public interface IAdEvents
    {
        void InitSdk(string intersistialId,string bannerId);

        bool IsFullScreenAdLoaded();
        bool IsBannerLoaded();

        void ShowFullscreenAd();
        void ShowBannerAd();

        void HideBannerAd();

        void RequestInterstitial();
        void RequestBanner();

        void ApplicationQuitRequest();
    }
}
