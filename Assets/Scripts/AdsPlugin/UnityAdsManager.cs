using UnityEngine;
using System.Collections;
//using UnityEngine.Advertisements;
/*
namespace CrimsonPlugins.Internal.Ads
{
    public class UnityAdsManager : MonoBehaviour, IRewardAd
    {
        //-----------------------------------------------------------------
        public bool IsReady { get { return Advertisement.IsReady(); } }
        //-----------------------------------------------------------------
        //----------------------------------------------------------------
        volatile bool isRewardShow=false;
        volatile System.Action<bool> finishedCallbackMainThread=null;
        volatile bool watchResult = false;

        public void InitializeAndRequest(string id)
        {
            if (Advertisement.isSupported)
            {
                Advertisement.Initialize(id, Debug.isDebugBuild);
            }
            else
            {
                Debug.Log("Platform not supported");
            }
        }
        //-----------------------------------------------------------------
        void LateUpdate()
        {
            if (finishedCallbackMainThread != null)
            {
                finishedCallbackMainThread(watchResult);
                finishedCallbackMainThread = null;
                watchResult = false;
            }
        }
        //-----------------------------------------------------------------
        public void ShowRewardAd(string placement, System.Action<bool> onFinishCallback)
        {
            isRewardShow = true;
            Advertisement.Show(placement, new ShowOptions
            {
                resultCallback = result =>
                {
                    isRewardShow = false;
                    finishedCallbackMainThread = onFinishCallback;
                    watchResult = result == ShowResult.Finished;
                }
            });
        }

        //-----------------------------------------------------------------
        //-----------------------------------------------------------------
    }

}
*/
