using UnityEngine;
using System.Collections;
using CrimsonAnalytics;

/*
namespace CrimsonPlugins.Internal.Ads
{
    public class AdMobObject : MonoBehaviour
    {


        volatile bool m_bannerLoaded = false;

        private InterstitialAd m_interstitial = null;
        private BannerView m_bannerView = null;


        volatile bool m_performCloseIntersistial = false;
        volatile bool m_performFailedLoadInter = false;
        volatile bool m_performLoadedInter = false;
        volatile bool m_performClickedInter = false;



        volatile bool m_performFailedLoadBanner = false;
        volatile bool m_performBannerLoaded = false;
        volatile bool m_performBannerClicked = false;


        volatile bool m_performQuit = false;

        string bannerId, intersistialId;
        int fetchFailedCounter = 0;
        bool lastNoInternet = true;

        //------------------------------------------------------------
        void Update()
        {
            if (m_performCloseIntersistial)
            {
                MediationTest.AdmobIntersistial.Closed.LogEvent();
                //just in case, destroy object - dont delete
                if (m_interstitial != null)
                {
                    m_interstitial.OnAdLoaded -= HandleInterstitialLoaded;
                    m_interstitial.OnAdFailedToLoad -= HandleInterstitialFailedToLoad;
                    m_interstitial.OnAdClosed -= HandleInterstitialClosed;
                    m_interstitial.OnAdLeavingApplication -= HandleInterstitialLeftApplication;
                    m_interstitial.Destroy();
                    m_interstitial = null;
                }
                m_performCloseIntersistial = false;
                SendMessage("AdsClosedCallback", SendMessageOptions.RequireReceiver);
            }
            else if (m_performLoadedInter)
            {
                m_performLoadedInter = false;
                MediationTest.AdmobIntersistial.Fetched.LogEvent();
            }
            else if (m_performFailedLoadInter)
            {
                m_performFailedLoadInter = false;

                if (fetchFailedCounter < 20)
                {
                    fetchFailedCounter++;
                }

                if (NetworkChecker.Instance.p_isConnectedToInternet)
                {
                    if (lastNoInternet)
                    {
                        fetchFailedCounter = 1;
                    }
                    lastNoInternet = false;
                    MediationTest.AdmobIntersistial.FetchFailed.LogEvent(fetchFailedCounter);
                }
                else
                {
                    if (!lastNoInternet)
                    {
                        fetchFailedCounter = 1;
                    }
                    lastNoInternet = true;
                    MediationTest.AdmobIntersistial.FetchFailedNoInternet.LogEvent(fetchFailedCounter);
                }

                if (fetchFailedCounter < 5 && NetworkChecker.Instance.p_isConnectedToInternet)
                {
                    Invoke("RequestInterstitial", 10);
                }
                else
                {
                    Invoke("RequestInterstitial", 60);
                }
            }
            else if (m_performClickedInter)
            {
                m_performClickedInter = false;
                MediationTest.AdmobIntersistial.Clicked.LogEvent();
            }


            else if (m_performBannerLoaded)
            {
                m_performBannerLoaded = false;
                SendMessage("BannerLoaded", SendMessageOptions.RequireReceiver);
            }
            else if (m_performFailedLoadBanner)
            {
                m_performFailedLoadBanner = false;
                Invoke("RequestBanner", 60);
            }
            else if (m_performBannerClicked)
            {
                m_performBannerClicked = false;
            }
            else if (m_performQuit)
            {
                m_performQuit = false;
                Invoke("Quit", 0.3f);
            }
        }
        //-----------------------------------------------------------
        public void InitSdk(string intersistialId, string bannerId)
        {
            this.bannerId = bannerId;
            this.intersistialId = intersistialId;
        }
        //------------------------------------------------------
        public bool IsFullScreenAdLoaded()
        {
            return m_interstitial != null && m_interstitial.IsLoaded();
        }
        //------------------------------------------------------
        public bool IsBannerLoaded()
        {
            return m_bannerLoaded;
        }
        //------------------------------------------------------
        public void ShowBannerAd()
        {
            if (m_bannerView != null)
            {
                m_bannerView.Show();
            }
        }
        //------------------------------------------------------
        public void HideBannerAd()
        {
            if (m_bannerView != null)
            {
                m_bannerView.Hide();
            }
        }
        //------------------------------------------------------
        public void RequestBanner()
        {
            InitBannerObject();
            m_bannerView.LoadAd(CreateAdRequest());
        }
        //------------------------------------------------------
        public void RequestInterstitial()
        {
            InitInterstitialObject();
            m_interstitial.LoadAd(CreateAdRequest());
        }
        //------------------------------------------------------

        //return if intersistial is showm
        public void ShowFullscreenAd()
        {
            MediationTest.AdmobIntersistial.ShowAd.LogEvent();
            m_interstitial.Show();
        }
        //------------------------------------------------------

        //------------------------------------------------------------

        void InitInterstitialObject()
        {

            if (m_interstitial != null)
            {
                m_interstitial.OnAdLoaded -= HandleInterstitialLoaded;
                m_interstitial.OnAdFailedToLoad -= HandleInterstitialFailedToLoad;
                m_interstitial.OnAdClosed -= HandleInterstitialClosed;
                m_interstitial.OnAdLeavingApplication -= HandleInterstitialLeftApplication;
                m_interstitial.Destroy();
            }

            m_interstitial = new InterstitialAd(intersistialId);
            m_interstitial.OnAdLoaded += HandleInterstitialLoaded;
            m_interstitial.OnAdFailedToLoad += HandleInterstitialFailedToLoad;
            m_interstitial.OnAdClosed += HandleInterstitialClosed;
            m_interstitial.OnAdLeavingApplication += HandleInterstitialLeftApplication;
        }
        //------------------------------------------------------
        void InitBannerObject()
        {
            if (m_bannerView != null)
            {
                // Register for ad events.
                m_bannerView.OnAdLoaded -= HandleAdLoaded;
                m_bannerView.OnAdFailedToLoad -= HandleAdFailedToLoad;
          //      m_bannerView.OnAdOpening -= HandleAdOpened;
                m_bannerView.OnAdClosed -= HandleAdClosed;
                m_bannerView.OnAdLeavingApplication -= HandleAdLeftApplication;

                m_bannerView.Destroy();
            }

            m_bannerView = new BannerView(bannerId, AdSize.SmartBanner, AdPosition.Top);
            //    }
            // Register for ad events.
            m_bannerView.OnAdLoaded += HandleAdLoaded;
            m_bannerView.OnAdFailedToLoad += HandleAdFailedToLoad;
           // m_bannerView.OnAdOpening += HandleAdOpened;
            m_bannerView.OnAdClosed += HandleAdClosed;
            m_bannerView.OnAdLeavingApplication += HandleAdLeftApplication;
       
            m_bannerLoaded = false;
        }

        //------------------------------------------------------
        //------------------------------------------------------
        private AdRequest CreateAdRequest()
        {
            return new AdRequest.Builder()
                    .Build();

        }
        //------------------------------------------------------
        void OnDestroy()
        {
            if (m_bannerView != null)
            {
                m_bannerView.Destroy();
            }
            if (m_interstitial != null)
            {
                m_interstitial.Destroy();
            }

            m_bannerView = null;
            m_interstitial = null;
        }

        //------------------------------------------------------

        #region Interstitial callback handlers

        public void HandleInterstitialLoaded(object sender, System.EventArgs args)
        {
            m_performLoadedInter = true;
            print("HandleInterstitialLoaded event received.");
        }
        //------------------------------------------------------	
        public void HandleInterstitialFailedToLoad(object sender, AdFailedToLoadEventArgs args)
        {
            m_performFailedLoadInter = true;
            print("HandleInterstitialFailedToLoad event received with message: " + args.Message);
        }
        //------------------------------------------------------
        public void HandleInterstitialClosed(object sender, System.EventArgs args)
        {
            m_performCloseIntersistial = true;
            print("HandleInterstitialClosed event received");
        }
        //------------------------------------------------------
        public void HandleInterstitialLeftApplication(object sender, System.EventArgs args)
        {
            m_performClickedInter = true;
            print("HandleInterstitialLeftApplication event received");
        }
        //------------------------------------------------------
        #endregion

        #region Banner callback handlers

        public void HandleAdLoaded(object sender, System.EventArgs args)
        {
            print("HandleAdLoaded event received.");
            m_bannerLoaded = true;
            m_performBannerLoaded = true;
        }
        //------------------------------------------------------	
        public void HandleAdFailedToLoad(object sender, AdFailedToLoadEventArgs args)
        {
            print("HandleFailedToReceiveAd event received with message: " + args.Message);
            m_bannerLoaded = false;
            m_performFailedLoadBanner = true;
        }
        //------------------------------------------------------
        public void HandleAdClosed(object sender, System.EventArgs args)
        {
            print("HandleAdClosed event received");
        }
        //------------------------------------------------------
        public void HandleAdLeftApplication(object sender, System.EventArgs args)
        {
            m_performBannerClicked = true;
            print("HandleAdLeftApplication event received");
        }
        //------------------------------------------------------
        #endregion

    }
}
*/
 