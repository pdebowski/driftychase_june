using UnityEngine;
using System.Collections;

namespace CrimsonPlugins.Internal.Ads
{

    public interface IRewardAd 
    {

        bool IsReady { get; }
        void InitializeAndRequest(string id);
        void ShowRewardAd(string placement,System.Action<bool> onFinishCallback);
    }
}
