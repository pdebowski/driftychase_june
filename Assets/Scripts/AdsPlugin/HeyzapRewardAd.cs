using UnityEngine;
using System.Collections;
//TODO: Analytics to layered
/*
using Heyzap;

namespace CrimsonPlugins.Internal.Ads
{
	public class HeyzapRewardAd : MonoBehaviour,IRewardAd {

		public bool IsReady { get { return HZIncentivizedAd.IsAvailable(); } }

		volatile bool perform_onAdLoaded = false;
		volatile bool perform_onAdFailedToLoad = false;
		volatile bool perform_onAdOpening = false;
		volatile bool perform_onAdStarted = false;
		volatile bool perform_onAdClosed = false;
		volatile bool perform_onAdRewarded = false;
		volatile bool perform_onAdLeavingApplication = false;

		bool adClosedNotInvoked = false;
		System.Action<bool> adResultCallback = null;
		bool isRewarded = false;

		public void InitializeAndRequest(string id){
			if (id != null) 
			{
				Debug.LogError ("id should be null,because initialized from intersistial sdk");
			}

			HZIncentivizedAd.SetDisplayListener(HeyzapListener);
			HZIncentivizedAd.Fetch ();			
		}
		///-----------------------------------------------
		void HeyzapListener(string adState, string adTag)
		{
			Debug.Log ("HZIncentivizedAd " + adState + " : adTag " + adTag);

			if (adState.Equals("show"))
			{
				perform_onAdOpening = true;
			}
			if (adState.Equals("hide"))
			{
				perform_onAdClosed = true;
			}
			if (adState.Equals("click"))
			{
				perform_onAdLeavingApplication = true;
			}
			if (adState.Equals("failed"))
			{
				perform_onAdClosed = true;
			}
			if (adState.Equals("available"))
			{
				perform_onAdLoaded = true;
			}
			if (adState.Equals("fetch_failed"))
			{
				perform_onAdFailedToLoad = true;
			}


			if (adState.Equals("audio_starting"))
			{
			}
			if (adState.Equals("audio_finished"))
			{
			}


			if ( adState.Equals("incentivized_result_complete") ) {
				isRewarded = true;
				perform_onAdRewarded = true;
			}
			if ( adState.Equals("incentivized_result_incomplete") ) {
				isRewarded = false;
			}
		}


		public void ShowRewardAd(string placement, System.Action<bool> onFinishCallback)
		{
			if (IsReady)
			{
				this.adResultCallback = onFinishCallback;
				adClosedNotInvoked = true;
				isRewarded = false;
				HZIncentivizedAd.Show();
			}
		}

		#if !UNITY_IOS
		void OnApplicationPause(bool paused)
		{
			if (paused == false && adClosedNotInvoked)
			{
				StartCoroutine(RealTimeInvoke(InvokeCloseIfNeeded, 1.5f));
			}
		}
		#endif

		IEnumerator RealTimeInvoke(System.Action func, float duration)
		{
			float t = 0;

			while (t < 1f) 
			{
				t += Time.unscaledDeltaTime / duration;
				yield return 0;
			}
			func ();
		}

		void Update()   //BUGFIX -  Thread 
		{
			if (perform_onAdLoaded)
			{
				perform_onAdLoaded = false;
				PerformOnAdLoaded();
			}
			else if (perform_onAdFailedToLoad)
			{
				perform_onAdFailedToLoad = false;
				PerformOnAdFailedToLoad();
			}
			else if (perform_onAdOpening)
			{
				perform_onAdOpening = false;
				PerformOnAdOpening();
			}
			else if (perform_onAdStarted)
			{
				perform_onAdStarted = false;
				PerformOnAdStarted();
			}
			else if (perform_onAdClosed)
			{
				adClosedNotInvoked = false;
				perform_onAdClosed = false;
				PerformOnAdClosed();
			}
			else if (perform_onAdRewarded)
			{
				perform_onAdRewarded = false;
				PerformOnAdRewarded();
			}
			else if (perform_onAdLeavingApplication)
			{
				perform_onAdLeavingApplication = false;
				PerformOnAdLeavingApplication();
			}
		}

		void NotifyResult() //BUGFIX - stupid sdk implementation (OnReward, OnAdClosed order undef)
		{
			if (adResultCallback != null)
			{
				adResultCallback(isRewarded);
				adResultCallback = null;
			}
		}

		void InvokeCloseIfNeeded() //BUGFIX - AdColony not called OnAdClosed when go to BG
		{
			if (adClosedNotInvoked)
			{
				adClosedNotInvoked = false;
				PerformOnAdClosed();
			}
		}

		void LoadAd()
		{
            Debug.Log("HZIncentivizedAd :Loada");
			HZIncentivizedAd.Fetch ();
        }

		#region perform callback
		void PerformOnAdLoaded()
		{
			Debug.Log("RewardHeyzap: OnAdLoaded");
		}

		void PerformOnAdFailedToLoad()
		{
			Invoke ("LoadAd", 60f);
			Debug.Log("RewardHeyzap: OnAdFailedToLoad");
		}

		void PerformOnAdOpening()
		{
			Debug.Log("RewardHeyzap: OnAdOpening");
		}

		void PerformOnAdStarted()
		{
			Debug.Log("RewardHeyzap: OnAdStarted");
		}

		void PerformOnAdClosed()
		{
			if (isRewarded && adResultCallback != null)
			{
				adResultCallback(true);
				adResultCallback = null;
			}
			else
			{
				StartCoroutine (RealTimeInvoke (NotifyResult, 0.8f));
			}

			Debug.Log("RewardHeyzap: OnAdClosed");
			if (!IsInvoking("LoadAd")) 
			{
				 Invoke ("LoadAd", 2);
			}
		}

		void PerformOnAdRewarded()
		{
			if (adResultCallback != null)
			{
				adResultCallback(true);
				adResultCallback = null;
			}

			Debug.Log("RewardHeyzap: OnAdRewarded");
		}

		void PerformOnAdLeavingApplication()
		{
			Debug.Log("RewardHeyzap: OnAdLeavingApplication");
		}
		#endregion
	}
}
*/
