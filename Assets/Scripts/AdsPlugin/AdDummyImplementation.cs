using UnityEngine;
using System.Collections;

namespace CrimsonPlugins.Internal.Ads
{
    public class AdDummyImplementation : MonoBehaviour, IAdEvents,IRewardAd
    {

        public bool IsReady { get { return false; } }

        public void InitializeAndRequest(string id) { }

        public void ShowRewardAd(string placement, System.Action<bool> onFinishCallback) { }

        public void InitSdk(string intersistialId,string bannerId)
        {
        }

        public bool IsFullScreenAdLoaded()
        {
            return false;
        }

        public bool IsBannerLoaded()
        {
            return false;
        }

        public void ShowFullscreenAd() { }

        public void ShowBannerAd() { }

        public void HideBannerAd() { }

        public void RequestInterstitial() { }

        public void RequestBanner() { }

        public void ApplicationQuitRequest()
        {
            Application.Quit();
        }
    }
}