using UnityEngine;
using System.Collections;
//TODO: Analytics to layered
/*
#if !UNITY_TVOS
using Heyzap;

namespace CrimsonPlugins.Internal.Ads
{
    public class HeyzapObject : MonoBehaviour, IAdEvents
    {
        volatile bool performClose = false;
        volatile bool performFetchFailed = false;
        volatile bool performClick = false;
        volatile bool performShow = false;

		HeyzapRewardAd rewardedObject;

        HeyzapBanner heyzapBanner;

       public void InitSdk(string intersistialId, string bannerId)
        {
            HeyzapAds.Start(intersistialId, HeyzapAds.FLAG_DISABLE_AUTOMATIC_FETCHING);

            HZInterstitialAd.SetDisplayListener(HeyzapEventListener);

            heyzapBanner = gameObject.AddComponent<HeyzapBanner>();
            heyzapBanner.InitSdk();
            
			UnityEngine.SceneManagement.SceneManager.sceneLoaded += (scene,mode) =>
              {
                  if (scene.name.ToUpper().Equals("SCENES/GAMESCENE"))
                  {
                      if (Debug.isDebugBuild)
                      {
                          HeyzapAds.ShowMediationTestSuite();
                      }
                  }
              };


        }
        
        //-----------------------------------------------------------------
        void Update()
        {
            if (performClose)
            {
                performClose = false;
                SendMessage("AdsClosedCallback", SendMessageOptions.RequireReceiver);
            }
            else if (performFetchFailed)
            {
                Invoke("RequestInterstitial", 60);
                performFetchFailed = false;
            }
            else if(performClick)
            {
                CrimsonAnalytics.Transaction.Interstitial.Clicked.LogEvent();
                performClick = false;
            }
            else if(performShow)
            {
                CrimsonAnalytics.Transaction.Interstitial.Displayed.LogEvent();
                performShow = false;
            }
        }
        //-----------------------------------------------------------------

        void HeyzapEventListener(string adState, string adTag)
        {
            Debug.Log("HAYZAP: adstate " + adState + " ;Tag " + adTag);
            if (adState.Equals("show"))
            {
                performShow = true;
            //    AdsCallback.StaticOpened();
            }
            if (adState.Equals("hide"))
            {
                performClose = true;
              //  AdsCallback.StaticClosed();
            }
            if (adState.Equals("click"))
            {
                performClick = true;
                //AdsCallback.StaticClicked();
            }
            if (adState.Equals("failed"))
            {
                performClose = true;

                //AdsCallback.StaticOpeningFailed();
            }
            if (adState.Equals("available"))
            {
               // AdsCallback.StaticFetchSuccess();
            }
            if (adState.Equals("fetch_failed"))
            {
                performFetchFailed = true;
                //AdsCallback.StaticFetchFailed();
            }


            if (adState.Equals("audio_starting"))
            {
            }
            if (adState.Equals("audio_finished"))
            {
            }
        }


        //*********************************
        public void ApplicationQuitRequest()
        {
             Application.Quit();
        }
        //-----------------------------------------------------------------
        public bool IsFullScreenAdLoaded()
        {
            return HZInterstitialAd.IsAvailable();
        }

        public void ShowFullscreenAd()
        {
            HZInterstitialAd.Show();

        }
        public void RequestInterstitial()
        {
            if (!IsFullScreenAdLoaded())
            {
                HZInterstitialAd.Fetch();
            }
        }

#region banner 
        public bool IsBannerLoaded()
        {
            return heyzapBanner.IsBannerLoaded();
        }
        public void ShowBannerAd()
        {
            heyzapBanner.ShowBannerAd();
        }
        public void HideBannerAd()
        {
            heyzapBanner.HideBannerAd();
        }
        public void RequestBanner()
        {
            heyzapBanner.RequestBanner();
        }
#endregion
    }
}
#endif
*/
