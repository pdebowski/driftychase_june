
//TODO: Analytics to layered

/*

using UnityEngine;
using System.Collections;
#if !UNITY_TVOS
using Heyzap;

namespace CrimsonPlugins.Internal.Ads
{
    public class SuperSonicObject : MonoBehaviour, IAdEvents
    {
        volatile bool performClose = false;
        volatile bool performFetchFailed = false;
        volatile bool performClick = false;
        volatile bool performShow = false;

        AdMobObject admobObject;

       public void InitSdk(string intersistialId, string bannerId)
        {
            Supersonic.Agent.initInterstitial(intersistialId, SystemInfo.deviceUniqueIdentifier);

            admobObject = gameObject.AddComponent<AdMobObject>();
            admobObject.InitSdk(null, bannerId);
            
        }
        ///-----------------------------------------------
        void Awake()
        {
            // init
            //SupersonicEvents.onInterstitialInitSuccessEvent += 
            //SupersonicEvents.onInterstitialInitFailedEvent += InterstitialInitFailEvent;

            // load
           // SupersonicEvents.onInterstitialReadyEvent += InterstitialReadyEvent;
            SupersonicEvents.onInterstitialLoadFailedEvent += (nl)=> { performFetchFailed = true; };

            // show
            SupersonicEvents.onInterstitialShowSuccessEvent += ()=> { performShow = true; };
            SupersonicEvents.onInterstitialShowFailedEvent += (nl)=> { performClose = true; };

            // ad interaction
            SupersonicEvents.onInterstitialClickEvent += ()=> { performClick = true; };
       //     SupersonicEvents.onInterstitialOpenEvent += InterstitialAdOpenedEvent;
            SupersonicEvents.onInterstitialCloseEvent += ()=> { performClose = true; }; 
        }
        

        //-----------------------------------------------------------------
        void Update()
        {
            if (performClose)
            {
                performClose = false;
                SendMessage("AdsClosedCallback", SendMessageOptions.RequireReceiver);
            }
            else if (performFetchFailed)
            {
                Invoke("RequestInterstitial", 60);
                performFetchFailed = false;
            }
            else if(performClick)
            {
                CrimsonAnalytics.Transaction.Interstitial.Clicked.LogEvent();
                performClick = false;
            }
            else if(performShow)
            {
                CrimsonAnalytics.Transaction.Interstitial.Displayed.LogEvent();
                performShow = false;
            }
        }
        //-----------------------------------------------------------------



        //*********************************
        public void ApplicationQuitRequest()
        {
             Application.Quit();
        }
        //-----------------------------------------------------------------
        public bool IsFullScreenAdLoaded()
        {
            return Supersonic.Agent.isInterstitialReady();
        }

        public void ShowFullscreenAd()
        {
            Supersonic.Agent.showInterstitial();

        }
        public void RequestInterstitial()
        {
            if (!IsFullScreenAdLoaded())
            {
                Supersonic.Agent.loadInterstitial();
            }
        }

#region banner 
        public bool IsBannerLoaded()
        {
            return admobObject.IsBannerLoaded(); ;
        }
        public void ShowBannerAd()
        {
            admobObject.ShowBannerAd();
        }
        public void HideBannerAd()
        {
            admobObject.HideBannerAd();
        }
        public void RequestBanner()
        {
            admobObject.RequestBanner();
        }
#endregion
    }
}
#endif
*/
