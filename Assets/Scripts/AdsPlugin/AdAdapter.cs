using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using CrimsonPlugins.Internal.Ads;

namespace CrimsonPlugins
{
    public class AdAdapter : MonoBehaviour
    {

        public System.Action<bool> OnBannerVisible = delegate { };
        public System.Action OnIntersistialClosed = delegate { };

        /*
        [SerializeField]
        string admobIntersistialId_Android;
        [SerializeField]
        string admobBannerId_Android;
        [SerializeField]
        string admobRewardId_Android;
        [SerializeField]
        string unityAdsId_Android;
        [SerializeField]
        string rewardMediationId_Android;
        [SerializeField]
        string heyzapId_Android;
        [SerializeField]
        string superSonic_Android;

        [SerializeField]
        string admobIntersistialId_Ios;
        [SerializeField]
        string admobBannerId_Ios;
        [SerializeField]
        string admobRewardId_Ios;
        [SerializeField]
        string unityAdsId_Ios;
        [SerializeField]
        string rewardMediationId_Ios;
        [SerializeField]
        string heyzapId_Ios;
        [SerializeField]
        string superSonic_Ios;
        */
        [SerializeField]
        string DEBUG_admobIntersistialId_Android;
        [SerializeField]
        string DEBUG_admobRewardId_Android;
        [SerializeField]
        string DEBUG_admobIntersistialId_Ios;
        [SerializeField]
        string DEBUG_admobRewardId_Ios;


        bool m_showAlways = true; // Debug.isDebugBuild must be true also

        public bool SupportFullScreen { get { return true; } }
        public bool SupportBanner { get { return !VIP.IsVIPAccount && GTMParameters.EnableBannerAds; } }
        public bool BannerActive = false;

        bool m_isBannerShallShown = false;
        string rewardAdsId;

        IAdEvents m_adsInterface;
        IRewardAd m_rewardAds;

        //---------------------------------------------------------------
        void Awake()
        {
            m_showAlways = m_showAlways && Debug.isDebugBuild;

            OnBannerVisible += OnBannerVisibleAction;

            if (!PlayerPrefs.HasKey("_adsFreeTime"))
            {
                PlayerPrefs.SetString("_adsFreeTime", System.DateTime.UtcNow.Ticks.ToString());
            }

            string intersistialId, bannerId;

#if PREMIUM
		gameObject.AddComponent<AdDummyImplementation> ();
#elif UNITY_STANDALONE_WIN
        gameObject.AddComponent<AdDummyImplementation>();
#elif UNITY_IOS
			intersistialId = ""; //not used in Tadaq
			bannerId = ""; //not used in Tadaq
			rewardAdsId = ""; //not used in Tadaq		

			gameObject.AddComponent<TapdaqObject>();
			gameObject.AddComponent<TapdaqRewardAd>();
#elif UNITY_ANDROID
      
            gameObject.AddComponent<TapdaqObject>();
            gameObject.AddComponent<TapdaqRewardAd>();
            intersistialId = bannerId = null;
#else
            Debug.LogError("Bad Platform in AdAdapter");
#endif

            m_adsInterface = GetComponent(typeof(IAdEvents)) as IAdEvents;
            m_adsInterface.InitSdk(intersistialId, bannerId);

            m_rewardAds = GetComponent(typeof(IRewardAd)) as IRewardAd;
        }
        //---------------------------------------------------------------

        //---------------------------------------------------------------
        public void ShowBanner()
        {
            if (SupportBanner)
            {
                m_isBannerShallShown = true;
                if (m_adsInterface.IsBannerLoaded())
                {
                    if (m_adsInterface.IsBannerLoaded() && OnBannerVisible != null)
                    {
                        OnBannerVisible(true);
                    }
                    m_adsInterface.ShowBannerAd();
                }
                else
                {
                    print("Banner is not ready yet.");
                }
            }
        }
        //---------------------------------------------------------------
        public bool IsRewardAdReady()
        {
#if UNITY_EDITOR
            return Random.Range(0f, 1f) < 0.75f;
#else
            return m_rewardAds != null && m_rewardAds.IsReady;
#endif
        }
        //---------------------------------------------------------------
        public void HideBanner()
        {
            if (m_adsInterface != null)
            {
                m_isBannerShallShown = false;
                if (m_adsInterface.IsBannerLoaded())
                {
                    m_adsInterface.HideBannerAd();
                }
                OnBannerVisible(false);
            }
        }
        //--------------------------------------------------------------
        //----------------------------------------------------------------
        public void ShowRewardAds(string placement, System.Action<bool> onFinishCallback)
        {
#if UNITY_EDITOR
            onFinishCallback(true);
#else
			m_rewardAds.ShowRewardAd(placement, onFinishCallback);
#endif
        }

        //---------------------------------------------------------------
        //---------------------------------------------------------------
        public void RequestInterstitial()
        {
            if (!m_adsInterface.IsFullScreenAdLoaded())
            {
                m_adsInterface.RequestInterstitial();
            }
        }
        //---------------------------------------------------------------
        public void RequestBanner()
        {
            if (SupportBanner && !m_adsInterface.IsBannerLoaded())
            {
                m_adsInterface.RequestBanner();
            }
        }
        //---------------------------------------------------------------

        bool IsTimeAllowShowFullAds()
        {
            long timeFirstRun = long.Parse(PlayerPrefs.GetString("_adsFreeTime", "0"));
            long adsShownTime = long.Parse(PlayerPrefs.GetString("_adsShownTime", "0"));


            System.TimeSpan firstRunDiff = new System.TimeSpan(System.DateTime.UtcNow.Ticks - timeFirstRun);
            System.TimeSpan adsShownDiff = new System.TimeSpan(System.DateTime.UtcNow.Ticks - adsShownTime);

            bool firstRunDiffAllow = firstRunDiff.TotalSeconds > GTMParameters.AdTimeAfterGameInstalled;
            bool adsShowDiffAllow = adsShownDiff.TotalSeconds > GTMParameters.AdTimeBetweenAds;

            return (SupportFullScreen && firstRunDiffAllow && adsShowDiffAllow) || m_showAlways;
        }
        //---------------------------------------------------------------
        public bool IsInterstitialReady()
        {
#if UNITY_EDITOR
            return false;
#endif
            return IsTimeAllowShowFullAds() && m_adsInterface != null && m_adsInterface.IsFullScreenAdLoaded();
        }
        //---------------------------------------------------------------
        public bool IsRewardInterstitialReady()
        {
            return m_adsInterface != null && m_adsInterface.IsFullScreenAdLoaded();
        }
        //---------------------------------------------------------------
        public void InternalCascadeRequestRewardAd()
        {
            m_rewardAds.InitializeAndRequest(rewardAdsId);
        }
        //---------------------------------------------------------------
        public bool InternalCascadeFullAdLoaded()
        {
            return m_adsInterface.IsFullScreenAdLoaded();
        }
        //---------------------------------------------------------------
        public bool ShowInterstitial()
        {

            if (IsInterstitialReady())
            {
                PlayerPrefs.SetString("_adsShownTime", System.DateTime.UtcNow.Ticks.ToString());
                m_adsInterface.ShowFullscreenAd();
                return true;
            }
            else
            {
                print("Interstitial is not ready yet.");
                return false;
            }
        }
        //---------------------------------------------------------------
        public bool ShowRewardInterstitial()
        {
            if (IsRewardInterstitialReady())
            {
                PlayerPrefs.SetString("_adsShownTime", System.DateTime.UtcNow.Ticks.ToString());
                m_adsInterface.ShowFullscreenAd();
                return true;
            }
            else
            {
                print("Interstitial is not ready yet.");
                return false;
            }
        }
        //---------------------------------------------------------------
        //worakround - bug callback from admob manager
        public void BannerLoaded()
        {
            if (!m_isBannerShallShown)
            {
                HideBanner();
            }
            Debug.Log("xaz BannerLoaded(): " + m_isBannerShallShown);
            OnBannerVisible(m_isBannerShallShown);
        }
        //----------------------------------------------------
        void AdsClosedCallback()
        { //.CALLBACK
            OnIntersistialClosed();
            RequestInterstitial();
        }
        //----------------------------------------------------------------
        private void OnBannerVisibleAction(bool visible)
        {
            BannerActive = visible;
        }
    }
}