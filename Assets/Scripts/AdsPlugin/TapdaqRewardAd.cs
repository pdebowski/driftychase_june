﻿using UnityEngine;
using System.Collections;
using Tapdaq;
using CrimsonAnalytics;


namespace CrimsonPlugins.Internal.Ads
{
    public class TapdaqRewardAd : MonoBehaviour, IRewardAd
    {
        bool lastNoInternet = true;
        int fetchFailedCounter = 0;

        System.Action<bool> onFinishCallback=null;
        bool isRewarded = false;
        

        public bool IsReady { get { return AdManager.IsRewardedVideoReady(); } }

        private void OnEnable()
        {
            TDCallbacks.TapdaqConfigLoaded += OnTapdaqConfigLoaded;
            TDCallbacks.AdAvailable += OnAdAvailable;
            TDCallbacks.AdNotAvailable += OnAdNotAvailable;
            TDCallbacks.AdClosed += OnAdClosed;
            TDCallbacks.AdClicked += OnAdClicked;
            TDCallbacks.AdError += OnAdError;
            TDCallbacks.RewardVideoValidated += OnRewardVideoValidated;
        }

        private void OnDisable()
        {
            TDCallbacks.TapdaqConfigLoaded -= OnTapdaqConfigLoaded;
            TDCallbacks.AdAvailable -= OnAdAvailable;
            TDCallbacks.AdNotAvailable -= OnAdNotAvailable;
            TDCallbacks.AdClosed -= OnAdClosed;
            TDCallbacks.AdClicked -= OnAdClicked;
            TDCallbacks.AdError -= OnAdError;
            TDCallbacks.RewardVideoValidated -= OnRewardVideoValidated;
        }



        public void ShowRewardAd(string placement, System.Action<bool> onFinishCallback)
        {
            this.onFinishCallback = onFinishCallback;
            MediationTest.Tapdaq_Rewarded.ShowVideo.LogEvent(NetworkChecker.Instance.p_isConnectedToInternet ? 1 : 0);
            AdManager.ShowRewardVideo();
        }

        public void InitializeAndRequest(string id)
        {

        }

        void RequestReward()
        {
            AdManager.LoadRewardedVideo();
        }

        #region ADSREQUESTS
        void OnRewardVideoValidated(TDVideoReward videoReward)
        {

            isRewarded = videoReward.RewardValid;
            Debug.Log("TAPDAQ-REWARD: I got " + videoReward.RewardAmount + " of " + videoReward.RewardName
                    + "   tag=" + videoReward.Tag + " IsRewardValid " + videoReward.RewardValid + " CustomJson: " + videoReward.RewardJson);
        }

        void OnAdError(TDAdEvent e)
        {
            if (e.adType == "REWARD_AD")
            {
                Debug.Log("TAPDAQ-REWARD: OnAdError " + e.adType + " : " + e.message);

                MediationTest.Tapdaq_Rewarded.ShowFailed.LogEvent(NetworkChecker.Instance.p_isConnectedToInternet ? 1 : 0);
                MediationTest.Tapdaq_Rewarded.Closed.LogEvent(0);

                if (onFinishCallback!=null)
                {
                    onFinishCallback(false);
                    onFinishCallback = null;
                }

                Invoke("RequestReward", 10);
            }
        }

        void OnAdClicked(TDAdEvent e)
        {
            if (e.adType == "REWARD_AD")
            {
                Debug.Log("TAPDAQ-REWARD: OnAdClicked " + e.adType);
                MediationTest.Tapdaq_Rewarded.Clicked.LogEvent();
            }
        }

        void OnAdClosed(TDAdEvent e)
        {
            if (e.adType == "REWARD_AD")
            {
                Debug.Log("TAPDAQ-REWARD: OnAdClosed " + e.adType);

                MediationTest.Tapdaq_Rewarded.Closed.LogEvent(NetworkChecker.Instance.p_isConnectedToInternet ? 1 : 0);
                
                if (onFinishCallback!=null)
                {
                    onFinishCallback(isRewarded);
                    onFinishCallback = null;
                }
                Invoke("RequestReward", 10);
            }
        }

        void OnAdAvailable(TDAdEvent e)
        {
            if (e.adType == "REWARD_AD")
            {
                Debug.Log("TAPDAQ-REWARD: OnAdAvailable " + e.adType);

                MediationTest.Tapdaq_Rewarded.Loaded.LogEvent(NetworkChecker.Instance.p_isConnectedToInternet ? 1 : 0);
            }
        }

        void OnAdNotAvailable(TDAdEvent e)
        {
            if (e.adType == "REWARD_AD")
            {
                Debug.Log("TAPDAQ-REWARD: OnAdNotAvailable " + e.adType);

                if (fetchFailedCounter < 20)
                {
                    fetchFailedCounter++;
                }

                if (NetworkChecker.Instance.p_isConnectedToInternet)
                {
                    if (lastNoInternet)
                    {
                        fetchFailedCounter = 1;
                    }
                    lastNoInternet = false;
                    MediationTest.Tapdaq_Rewarded.FetchFailed.LogEvent(fetchFailedCounter);
                }
                else
                {
                    if (!lastNoInternet)
                    {
                        fetchFailedCounter = 1;
                    }
                    lastNoInternet = true;
                    MediationTest.Tapdaq_Rewarded.FetchFailedNoInternet.LogEvent(fetchFailedCounter);
                }

                Invoke("RequestReward", (NetworkChecker.Instance.p_isConnectedToInternet && fetchFailedCounter < 5) ? 10 : 60);
            }
        }

        void OnTapdaqConfigLoaded()
        {
            Debug.Log("TAPDAQ-REWARD: OnTapdaqConfigLoaded");
            AdManager.LoadRewardedVideo();
        }
#endregion
    }
}
