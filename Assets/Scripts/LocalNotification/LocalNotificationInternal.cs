using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using SimpleJSON;

namespace CrimsonPlugins.Internal.Notification
{
    public class ExternalDataParser
    {
        public class NotificationInfo
        {
            public int time;
            public string title;
            public string message;
            public bool useSound;
        }

        public class ScheduledNotification
        {
            public string uniqueKey;
            public int timestamp;

            public string appId;
            public string title;
            public string message;
        }

        public class ReminderNotification
        {
            public string title;
            public string message;
            public bool useSound;
        }


        const int SECOND = 1;
        const int MINUTE = 60 * SECOND;
        const int HOUR = 60 * MINUTE;
        const int DAY = 24 * HOUR;

        List<NotificationInfo> notificationInfos;
        ScheduledNotification scheduledNotification;
        ReminderNotification reminderNotification;
        ILocalNotificationPerGame gameConcreteInterface;

        public ExternalDataParser(string countryCode, string jsonStrFromPlayfab, string dataFromGtm, ILocalNotificationPerGame gameInterface)
        {
            notificationInfos = new List<NotificationInfo>();
            gameConcreteInterface = gameInterface;

            //Set Default whne parse error
            if (string.IsNullOrEmpty(jsonStrFromPlayfab) || string.IsNullOrEmpty(dataFromGtm) || string.IsNullOrEmpty(countryCode))
            {
				Debug.Log("Parse problem1 (not configured) : " + (string.IsNullOrEmpty(jsonStrFromPlayfab)) + " : " +(string.IsNullOrEmpty(dataFromGtm))
                    +(string.IsNullOrEmpty(countryCode)));
                return;
            }

            JSONNode jsonPlayfab = JSON.Parse(jsonStrFromPlayfab);
            string[] notifPairs = dataFromGtm.Split(';');

            foreach (var item in notifPairs)
            {
                string[] gtmPair = item.Split('-');

                if (gtmPair.Length != 2)
                {
                    Debug.LogWarning("Parse problem2");
                }
                else
                {
                    NotificationInfo info = CreateInfoForParams(countryCode, gtmPair[0], gtmPair[1], jsonPlayfab);
                    if (info != null)
                    {
                        notificationInfos.Add(info);
                    }
                }
            }

            scheduledNotification = CreateScheduledNotification(countryCode, jsonPlayfab);
            reminderNotification = CreateReminderNotification(countryCode, jsonPlayfab);
        }

        public List<NotificationInfo> GetNotificationList()
        {
            return notificationInfos;
        }

        public ScheduledNotification GetScheduledNotification()
        {
            return scheduledNotification;
        }

        public ReminderNotification GetReminderNotification()
        {
            return reminderNotification;
        }

        ReminderNotification CreateReminderNotification(string countryCode, JSONNode jsonNode)
        {

			bool recordExists;
			int it = 1;

			var reminderList= new System.Collections.Generic.List<ReminderNotification> ();

			do {
                JSONNode reminderNode = null;

                if (jsonNode ["reminder" + it + "_" + countryCode.ToUpper ()] != null) {
					reminderNode = jsonNode ["reminder"+it + "_" + countryCode.ToUpper ()];
				} else if (jsonNode ["reminder"+it] != null) {
					reminderNode = jsonNode ["reminder"+it];
				}


				if (reminderNode != null && reminderNode ["title"] != null && reminderNode ["message"] != null) {
					bool useSound = jsonNode ["sound_off"] == null || jsonNode ["sound_off"].AsBool == false;
					reminderList.Add(new ReminderNotification () {
						title = reminderNode ["title"].Value,
						message = reminderNode ["message"],
						useSound = useSound
					});
					recordExists=true;
				}
				else 
				{
					recordExists=false;
				}
				it++;
			} while (recordExists);

			if (reminderList.Count > 0) 
			{
				return reminderList [Random.Range (0, reminderList.Count)];
			}
            else
            {
                return null;
            }
        }

        ScheduledNotification CreateScheduledNotification(string countryCode, JSONNode jsonNode)
        {
            string platform = "";
#if UNITY_IOS
            platform = "ios";
#elif UNITY_ANDROID
            platform = "android";
#endif

            JSONNode scheduledNode = null;

            if (jsonNode["scheduled" + "_" + countryCode.ToUpper()] != null)
            {
                scheduledNode = jsonNode["scheduled" + "_" + countryCode.ToUpper()];
            }
            else if (jsonNode["scheduled"] != null)
            {
                scheduledNode = jsonNode["scheduled"];
            }

            if (scheduledNode != null && scheduledNode["unique_key"] != null
                && scheduledNode["timestamp"] != null && scheduledNode["appid" + "_" + platform] != null
                && scheduledNode["title" + "_" + platform] != null && scheduledNode["message" + "_" + platform] != null)
            {
                return new ScheduledNotification()
                {
                    uniqueKey = scheduledNode["unique_key"].Value,
                    timestamp = scheduledNode["timestamp"].AsInt,
                    appId = scheduledNode["appid" + "_" + platform].Value,
                    title = scheduledNode["title" + "_" + platform].Value,
                    message = scheduledNode["message" + "_" + platform].Value
                };
            }
            else
            {
                return null;
            }
        }

        NotificationInfo CreateInfoForParams(string countryCode, string notificationId, string timeModifierString, JSONNode cachedPlayfabJson)
        {
            string id=null;

            if (cachedPlayfabJson[notificationId + "_" + countryCode.ToUpper()] != null)
            {
                id = notificationId + "_" + countryCode.ToUpper();
            }
            else if (cachedPlayfabJson[notificationId] != null)
            {
                id = notificationId;
            }
            else
            {
                Debug.LogWarning("Pair Not Found for " + notificationId + " result " + cachedPlayfabJson[notificationId + "_" + countryCode.ToUpper()]
                    + " : " + cachedPlayfabJson["n1"] + " pure json " + cachedPlayfabJson.ToString());
            }


            if (id != null)
            {
                NotificationInfo info = jsonToInfo(cachedPlayfabJson[id], cachedPlayfabJson[notificationId + "_conditions"], timeModifierString);
                if (info != null)
                {
                    return info;
                }
            }

            return null;
        }

        NotificationInfo jsonToInfo(JSONNode jsonNode, JSONNode jsonConditions, string timeModifierString)
        {
            NotificationInfo info = new NotificationInfo();

            float parsedDays;
            if (!float.TryParse(timeModifierString, out parsedDays))
            {
                Debug.LogWarning("Cannot Parse float");
                return null;
            }

            info.time = (int)(parsedDays * (float)DAY);
            info.useSound = jsonNode["sound_off"] == null || jsonNode["sound_off"].AsBool == false;

            if (gameConcreteInterface.IsNotificationAllowed(jsonConditions))
            {
                info.message = jsonNode["message"] != null ? gameConcreteInterface.GetRealMessage(jsonNode["message"].Value) : "";
                info.title = jsonNode["title"] != null ? jsonNode["title"].Value : "";
            }
            else
            {
                if (jsonNode["message_else"] != null)
                {
                    if (jsonNode["title_else"] != null)
                    {
                        info.title = jsonNode["title_else"].Value;
                    }
                    else
                    {
                        info.title = jsonNode["title"] != null ? jsonNode["title"].Value : "";
                    }

                    info.message = gameConcreteInterface.GetRealMessage(jsonNode["message_else"].Value);
                }
                else
                {
                    return null; // null if not "else" message
                }
            }
            
            return info;
        }
    }
}
