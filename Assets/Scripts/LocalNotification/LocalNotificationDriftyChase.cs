using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using SimpleJSON;
using System.Globalization;

namespace CrimsonPlugins.Internal.Notification
{

    public class LocalNotificationDriftyChase : MonoBehaviour, ILocalNotificationPerGame
    {

        public bool IsNotificationAllowed(JSONNode conditionsNode)
        {

            if (conditionsNode == null)
            {
                return true;
            }

            return true;
        }

        public string GetRealMessage(string msg)
        {
            return msg;
        }

        public string GetCountryCode()
        {
            if (PlayerPrefs.HasKey("Language_CountryCode"))
            {
                return PlayerPrefs.GetString("Language_CountryCode");
            }
            else
            {
                switch (Application.systemLanguage)
                {
                    case SystemLanguage.English:
                        return "EN";
                    case SystemLanguage.Chinese:
                        return "CN";
                    case SystemLanguage.French:
                        return "FR";
                    case SystemLanguage.German:
                        return "DE";
                    case SystemLanguage.Korean:
                        return "KR";
                    case SystemLanguage.Polish:
                        return "PL";
                    case SystemLanguage.Spanish:
                        return "ES";
                    default:
                        return "EN";
                }
            }
        }

        public string GetJsonStringFromPlayfab()
        {
            //      var debugmsg = "{\"n1_conditions\":{\"all_unlocked\": \"false\"},\"n1\":{\"title\":\"Follow The Line 2\",\"message\":\"Keep going, only [points_to_unlock] mm more to unlock new level\",\"title_else\":\"Follow The Line 2\",\"message_else\":\"Others are beating your score - show them you're better!\"},\"n1_PL\":{\"title\":\"Follow The Line 2\",\"message\":\"Keep going, only [points_to_unlock] mm more to unlock new levelAAA\",\"title_else\":\"Follow The Line 2\",\"message_else\":\"Others are beating your score - show them you're betterAAAA!\"},\"n2\": {\"title\":\"Follow The Line 2\",\"message\": \"Others are beating your score - show them you're better!\"},\"n3_conditions\": {\"all_unlocked\": \"false\" },\"n3\": {\"title\": \"Follow The Line 2\",\"message\": \"[unlocked_levels] levels unlocked [to_unlock] more to go\",\"title_else\": \"Follow The Line 2\",\"message_else\": \"Others are beating your score - show them you're better!\"},\"n4\": {\"title\": \"Follow The Line 2\",\"message\": \"Come back, new levels are waiting for you!\"},\"n5\": {\"title\": \"Follow The Line 2\",\"message\": \"Come back, new levels are waiting for you!\",\"sound_off\": \"true\"},\"scheduled\": {\"unique_key\": \"32ewrd3435\",\"timestamp\": \"1469466580\",\"appid_android\": \"com.crimsonpine.game\",\"title_android\": \"Download GR\",\"message_android\": \"GR Ready to download\",\"appid_ios\": \"1289374\",\"title_ios\": \"Download GR\",\"message_ios\": \"GR Ready to download\"},\"scheduled_PL\": {\"unique_key\": \"32ewrd3435\",\"timestamp\": \"1469466580\",\"appid_android\": \"com.crimsonpine.game\",\"title_android\": \"Download GR\",\"message_android\": \"GR Readyssss to download\",\"appid_ios\": \"1289374\",\"title_ios\": \"Download GR\",\"message_ios\": \"GR Ready to downlssoad\"}}";
            //    return debugmsg;
                 PlayFabManager playFabManager = FindObjectOfType<PlayFabManager>();
            		return (playFabManager == null || GTMParameters.EnableNotifications==false) ? null : playFabManager.notificationJSONString;
            //return null;
        }

        public string GetDataFromGTM()
        {
            //   var debugmsg = "n1-0.00034;n2-0.0007;n3-0.001;n4-0.0014;n5-0.0019";
            // return debugmsg;
            //return new ParameterCustomizer().p_localNotificationConfig;
            return GTMParameters.LocalNotificationConfig;
        }



    }

}
    