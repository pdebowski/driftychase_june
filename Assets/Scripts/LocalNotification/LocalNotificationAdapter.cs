using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using SimpleJSON;
using CrimsonPlugins.Internal.Notification;


namespace CrimsonPlugins
{

    [SerializeField]
    public interface ILocalNotificationPerGame
    {
        bool IsNotificationAllowed(JSONNode conditionsNode);
        string GetRealMessage(string msg);
        string GetCountryCode();
        string GetJsonStringFromPlayfab();
        string GetDataFromGTM();
    }


    public class LocalNotificationAdapter : Singleton<LocalNotificationAdapter>
    {
        const string NOTIF_APPID_PREFSKEY = "NOTIF_LAUNCH_APPID";
        const string NOTIF_NOTIFICATIONID_PREFSKEY = "NOTIF_LAUNCH_NOTIFICATIONID";
        const string NOTIF_UNIQUEID_PREFSKEY = "NOTIF_LAUNCH_UNIQUEID";
        const string NOTIF_LAST_FIRED_UNIQUEID_PREFSKEY = "NOTIF_LAUNCH_FIREDUNIQUEID";

        const string REMINDER_NOTIF_TIME_KEY = "REMINDER_NOTIF_TIME";

        ILocalNotificationPerGame gameInterface;
        bool initialized = false;

#if UNITY_ANDROID
        AndroidNotificationManager notificationManager;
#else
		ISN_LocalNotificationsController notificationManager;
#endif


        // Use this for initialization
        new void Awake()
        {
            base.Awake();
#if UNITY_ANDROID
            notificationManager = AndroidNotificationManager.Instance; // instace gaily creation bugfix for onapplicationquit usuing

            AndroidNotificationManager.Instance.OnNotificationIdLoaded += (notificationId) =>
            {
                TryToLaunchStore(notificationId);
            };

            AndroidNotificationManager.Instance.LocadAppLaunchNotificationId();
            AndroidNotificationManager.Instance.CancelAllLocalNotifications();
            initialized = true;
#else
			notificationManager = ISN_LocalNotificationsController.Instance; // instace gaily creation bugfix for onapplicationquit usuing
#endif


            /*        var debugmsg = "{\"n1_conditions\": {\"all_unlocked\": \"false\"},\"n1\": {\"title\": \"Follow The Line 2\",\"message\": \"Keep going, only [points_to_unlock] mm more to unlock new level\",\"title_else\": \"Follow The Line 2\",\"message_else\": \"Others are beating your score - show them you're better!\"},  \"n2\": {\"title\":\"Follow The Line 2\",\"message\": \"Others are beating your score - show them you're better!\"},\"n3_conditions\": {\"all_unlocked\": \"false\" },\"n3\": {\"title\": \"Follow The Line 2\",\"message\": \"[unlocked_levels] levels unlocked [to_unlock] more to go\",\"title_else\": \"Follow The Line 2\",\"message_else\": \"Others are beating your score - show them you're better!\"},\"n4\": {\"title\": \"Follow The Line 2\",\"message\": \"Come back, new levels are waiting for you!\"},\"n5\": {\"title\": \"Follow The Line 2\",\"message\": \"Come back, new levels are waiting for you!\",\"sound_off\": \"true\"},\"scheduled\": {\"unique_key\": \"32ewrd3435\",\"timestamp\": \"123412323\",\"appid_android\": \"com.crimsonpine.game\",\"title_android\": \"Download GR\",\"message_android\": \"GR Ready to download\",\"appid_ios\": \"1289374\",\"title_ios\": \"Download GR\",\"message_ios\": \"GR Ready to download\"}}";

                    var j = JSON.Parse(debugmsg);
                    Debug.Log(j["n1"].ToString());*/

            gameInterface = GetComponent(typeof(ILocalNotificationPerGame)) as ILocalNotificationPerGame;
            if (gameInterface == null)
            {
                for (int i = 0; i < 10; i++)
                {
                    Debug.LogError("ILocalNotificationPerGame Not Found");
                }
            }


        }

        private void Start()
        {
#if UNITY_ANDROID
            AndroidNotificationManager bugfix = AndroidNotificationManager.Instance;
#else

#endif
        }

        //------------------------------------------------------
        public void InitizeNotification()
        {


#if UNITY_IOS

			ISN_LocalNotificationsController.Instance.RequestNotificationPermissions();
			ISN_LocalNotificationsController.Instance.CancelAllLocalNotifications();
			ISN_LocalNotificationsController.Instance.ApplicationIconBadgeNumber(0);

			initialized=true;
#endif
        }
        //------------------------------------------------------
#if UNITY_IOS
        void InGameLocalNotificationReceived(ISN_LocalNotification notification)
        {
            TryToLaunchStore(notification.Badges);
        }
#endif
        //------------------------------------------------------
        void TryToLaunchStore(int notificationId)
        {
            Debug.Log("App Launched by notification with id : " + notificationId);
            if (PlayerPrefs.HasKey(NOTIF_APPID_PREFSKEY) && PlayerPrefs.HasKey(NOTIF_NOTIFICATIONID_PREFSKEY)
                && PlayerPrefs.HasKey(NOTIF_UNIQUEID_PREFSKEY) && notificationId == PlayerPrefs.GetInt(NOTIF_NOTIFICATIONID_PREFSKEY, -123))
            {
                PlayerPrefs.SetString(NOTIF_LAST_FIRED_UNIQUEID_PREFSKEY, PlayerPrefs.GetString(NOTIF_UNIQUEID_PREFSKEY));
#if UNITY_ANDROID
                Application.OpenURL("market://details?id=" + PlayerPrefs.GetString(NOTIF_APPID_PREFSKEY));
#else
                Application.OpenURL("itms-apps://itunes.apple.com/app/id" + PlayerPrefs.GetString(NOTIF_APPID_PREFSKEY));
#endif
            }
        }
        //------------------------------------------------------
        public void ScheduleReminder(float inSeconds)
        {
            System.DateTime dateToShedule = System.DateTime.UtcNow.AddSeconds(inSeconds);
            PlayerPrefs.SetString(REMINDER_NOTIF_TIME_KEY, dateToShedule.ToString());
        }
        //------------------------------------------------------
        public void OnApplicationQuit()
        {
            UpdateNotification();
        }
        //------------------------------------------------------

        public void OnApplicationPause(bool pausing)
        {
            if (pausing)
            {
                UpdateNotification();
            }
            else
            {
#if UNITY_ANDROID
                AndroidNotificationManager.Instance.CancelAllLocalNotifications();
#else
            ISN_LocalNotificationsController.Instance.CancelAllLocalNotifications();
            ISN_LocalNotificationsController.Instance.ApplicationIconBadgeNumber(0);
#endif
            }
        }
        //----------------------------------------------------------------------------------
        void UpdateNotification()
        {
            if (!initialized)
            {
                return;
            }

#if UNITY_ANDROID
            notificationManager.CancelAllLocalNotifications();
#else
            notificationManager.CancelAllLocalNotifications();
            notificationManager.ApplicationIconBadgeNumber(0);
#endif

            ExternalDataParser dataParser = new ExternalDataParser(gameInterface.GetCountryCode(),
                                gameInterface.GetJsonStringFromPlayfab(), gameInterface.GetDataFromGTM(), gameInterface);

            List<ExternalDataParser.NotificationInfo> notificationInfos = dataParser.GetNotificationList();
            int idIterator = 1;
            notificationInfos.ForEach(notification =>
            {
                ScheduleNotification(idIterator, notification.title, notification.message, notification.time, notification.useSound);
                idIterator++;
            });

            //SCHEDULED NOTIFICATION
            ExternalDataParser.ScheduledNotification scheduledNotification = dataParser.GetScheduledNotification();

            if (scheduledNotification != null && PlayerPrefs.GetString(NOTIF_LAST_FIRED_UNIQUEID_PREFSKEY).Equals(scheduledNotification.uniqueKey) == false)
            {
                int unixTimestamp = (int)(System.DateTime.UtcNow.Subtract(new System.DateTime(1970, 1, 1))).TotalSeconds;

                int secondToFire = scheduledNotification.timestamp - unixTimestamp;
                if (secondToFire < 0 && secondToFire > -(60 * 60 * 24 * 100))
                {
                    secondToFire = 60;
                }

                PlayerPrefs.SetString(NOTIF_APPID_PREFSKEY, scheduledNotification.appId);
                PlayerPrefs.SetString(NOTIF_UNIQUEID_PREFSKEY, scheduledNotification.uniqueKey);
                PlayerPrefs.SetInt(NOTIF_NOTIFICATIONID_PREFSKEY, idIterator);
                ScheduleNotification(idIterator, scheduledNotification.title, scheduledNotification.message, secondToFire, true);
                idIterator++;
            }

            //REMINDER NOTIFICATION
            ExternalDataParser.ReminderNotification reminderNotif = dataParser.GetReminderNotification();
            if (reminderNotif != null)
            {
                string text = PlayerPrefs.GetString(REMINDER_NOTIF_TIME_KEY, null);
                System.DateTime reminderDate;
                if (text != null && text.Length > 0)
                {
                    reminderDate = System.DateTime.Parse(text);
                }
                else
                {
                    reminderDate = new System.DateTime(1970, 1, 1, 0, 0, 0, 0);
                }

                long reminderStamp = System.Convert.ToInt64(
                    new System.TimeSpan(reminderDate.Ticks).TotalSeconds
                );
                long nowStamp = System.Convert.ToInt64(
                    new System.TimeSpan(System.DateTime.UtcNow.Ticks).TotalSeconds
                );

                long secondToFire = reminderStamp - nowStamp;

                if (secondToFire > 10)
                {
                    ScheduleNotification(idIterator, reminderNotif.title, reminderNotif.message, System.Convert.ToInt32(secondToFire), true);
                    idIterator++;
                }
            }

        }
        //----------------------------------------------------------------------------------
        //----------------------------------------------------------------------------------
        void ScheduleNotification(int index, string title, string message, int time, bool useSound)
        {
            if (Debug.isDebugBuild)
            {
                Debug.LogWarning("ScheduledNotification " + index + " : " + message + " : " + time + " : " + useSound);
                time = 60 * index;
            }
#if UNITY_ANDROID
            AndroidNotificationBuilder notifBuilder;
            notifBuilder = new AndroidNotificationBuilder(index, title, message, time);
            notifBuilder.ShowIfAppIsForeground(true);
            notifBuilder.SetIconName("notif_small");
            notifBuilder.SetLargeIcon("notif_large");

            notificationManager.ScheduleLocalNotification(notifBuilder);
#else
        ISN_LocalNotification notification = new ISN_LocalNotification(System.DateTime.Now.AddSeconds(time), message, useSound);
		notification.SetBadgesNumber(index);
        notification.Schedule();
#endif
        }
    }
}

