using UnityEngine;
using System.Collections;

public class GAHelper : SA.Common.Pattern.Singleton<GAHelper>
{

    private const string LAST_TIME_ONCE_DAY_REPORTED_KEY = "GA_HELPER_LAST_TIME_ONCE_DAY_REPORTED";

    public bool WasEverReported { get { return PlayerPrefs.HasKey(LAST_TIME_ONCE_DAY_REPORTED_KEY); } }
    public bool IsNextDay { get; set; }

    private int dollarsSpentReported = 0;
    private int dollarsInQueue = 0;
    private Coroutine dollarsQueueCor;

    private System.DateTime lastTimeOnceDayReported
    {
        get
        {
            if (!PlayerPrefs.HasKey(LAST_TIME_ONCE_DAY_REPORTED_KEY))
            {
                lastTimeOnceDayReported = System.DateTime.Now;
                return lastTimeOnceDayReported;
            }
            return System.DateTime.Parse(PlayerPrefs.GetString(LAST_TIME_ONCE_DAY_REPORTED_KEY));
        }
        set
        {
            PlayerPrefs.SetString(LAST_TIME_ONCE_DAY_REPORTED_KEY, value.ToString());
        }
    }

    //------------------------------

    void Awake()
    {
        CheckIfNextDay();
    }

    void Start()
    {
        if(PlatformRecognition.IsTV)
        {
            CrimsonAnalytics.Other.Platform.TV.LogEvent();
        }
        else
        {
            CrimsonAnalytics.Other.Platform.Mobile.LogEvent();
        }
        

        TryReportOnceADay();
    }

    void OnEnable()
    {
        StoreManager.OnCarBought += ReportCarBought;
        GameplayManager.OnSessionStarted += ReportCheckpointUsage;
    }

    void OnDisable()
    {
        StoreManager.OnCarBought -= ReportCarBought;
        GameplayManager.OnSessionStarted -= ReportCheckpointUsage;
    }

    public void ReportRetention(int days)
    {
        DL.Log(CrimsonAnalytics.CA.DEBUG_STRING + "GAHelper ReportRetention");
        string daysString = CommonMethods.GetCrimsonAnalyticsOrder(days);
        CrimsonAnalytics.PlayerInfo.Retention.Days.LogEvent(daysString);
    }

    public void ReportOnceASession()
    {
        DL.Log(CrimsonAnalytics.CA.DEBUG_STRING + "GAHelper ReportOnceASession");
        CrimsonAnalytics.PlayerInfo.Gameplay.SessionTime.LogEvent(PlayerPrefsAdapter.SessionTime);
        PlayerPrefsAdapter.SessionTime = 0;
    }

    public void ReportDollarsSpent(int ammount)
    {
        int left = ammount;

        while(dollarsSpentReported < 10 && left>0)
        {
            ReportOneDollarSpent();
            left--;
        }

        if(left > 0)
        {
            dollarsInQueue += left;

            if(dollarsQueueCor == null)
            {
                dollarsQueueCor = StartCoroutine(ReportDollarsQueue());
            }
        }
    }

    private void ReportOneDollarSpent()
    {
        CrimsonAnalytics.Transaction.InApp.OneDollarSpent.LogEvent();
        dollarsSpentReported++;
    }

    private IEnumerator ReportDollarsQueue()
    {
        while(true)
        {
            yield return new WaitForSeconds(1.5f);

            if (dollarsInQueue > 0)
            {
                ReportOneDollarSpent();
                dollarsInQueue--;
            }
            else
            {
                dollarsQueueCor = null;
                yield break;
            }
        }
    }

    private void CheckIfNextDay()
    {
        System.TimeSpan timeSpan = System.DateTime.Now - lastTimeOnceDayReported;

        if (timeSpan.TotalDays >= 1)
        {
            IsNextDay = true;
            lastTimeOnceDayReported = System.DateTime.Now;
        }
    }

    private void TryReportOnceADay()
    {
        if (!WasEverReported)
        {
            lastTimeOnceDayReported = System.DateTime.Now;
        }

        if (IsNextDay)
        {
            ReportOnceADay();
        }
    }

    private void ReportOnceADay()
    {
        DL.Log(CrimsonAnalytics.CA.DEBUG_STRING + "GAHelper ReportOnceADay");
        CrimsonAnalytics.PlayerPerformance.Record.LogAll(PrefsDataFacade.Instance.GetRecord());
        CrimsonAnalytics.PlayerPerformance.TotalDistance.LogAll(PrefsDataFacade.Instance.GetTotalDistance());
        CrimsonAnalytics.PlayerPerformance.BestSeenCity.Distribution.LogEvent(PlayerPrefsAdapter.BestCitySawIndex + 1);
        CrimsonAnalytics.PlayerInfo.TimeRatio.GameplayTimeToTotalTime.LogEvent(Mathf.RoundToInt((float)(100 * PlayerPrefsAdapter.TotalGameplayTime.TotalMinutes / PlayerPrefsAdapter.TotalTimeInApp.TotalMinutes)));
    }

    private void ReportCarBought(CarDefinitions.CarEnum carEnum)
    {
        int carIndex = PrefabReferences.Instance.CarStatsDB.GetCarsOrder().FindIndex(x => x == carEnum);
        CrimsonAnalytics.Economy.CarUnlockTimeWithMenus.CarBought.LogEvent(carIndex, (int)PlayerPrefsAdapter.TotalTimeInApp.TotalMinutes);
        CrimsonAnalytics.Economy.CarUnlockTimeInGame.CarBought.LogEvent(carIndex, (int)PlayerPrefsAdapter.TotalGameplayTime.TotalMinutes);
    }

    private void ReportCheckpointUsage()
    {
        if(!VIP.IsVIPAccount)
        {
            int carID = PrefabReferences.Instance.CarStatsDB.GetCarIndex(PlayerPrefsAdapter.SelectedCar);

            if (PrefabReferences.Instance.CheckpointsManager.IsCheckpointTimeLeft())
            {
                CrimsonAnalytics.PlayerInfo.CheckpointUsage.RunWithCheckpoint.LogEvent(carID);
            }
            else
            {
                CrimsonAnalytics.PlayerInfo.CheckpointUsage.RunWithoutCheckpoint.LogEvent(carID);
            }
        }


    }




}
