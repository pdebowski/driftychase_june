using UnityEngine;
using System.Collections;
using CrimsonAnalyticsFeatures;

namespace CrimsonAnalytics
{
    public class CA : MonoBehaviour
    {
        public static string DEBUG_STRING = "AnalyticsDebug. ";
        private static ECommerceTracker eCommerceTracker;
#if !UNITY_TVOS
        private static GoogleAnalyticsV4 googleAnalyticsV4;
#endif

        void Awake()
        {
#if !UNITY_TVOS
            googleAnalyticsV4 = GetComponent<GoogleAnalyticsV4>();
#endif
            eCommerceTracker = GetComponent<ECommerceTracker>();
        }

        public static void StartSession()
        {
#if !UNITY_TVOS
            googleAnalyticsV4.StartSession();
#endif
        }

        public static void DispatchHits()
        {
#if !UNITY_TVOS
            googleAnalyticsV4.DispatchHits();
#endif
        }


        //------------------------------------------------------------------

        public class AnalyticsBuilder
        {
            private static string experimentKey;
            private static string containerVersion;

#if !UNITY_TVOS
            public static EventHitBuilder buildEvent()
            {
                GetGTMParameters();

                return new EventHitBuilder()
                    .SetCustomDimension(1, experimentKey)
                    .SetCustomMetric(1, containerVersion)
                    ;
            }
#endif

            private static void GetGTMParameters()
            {
                if (experimentKey == null || containerVersion == null)
                {
                    experimentKey = GTMParameters.ExperimentKey;
                    containerVersion = GTMParameters.ContainerVersion.ToString();
                }
            }
        }

        public class AnalyticsCategory<Category> : AnalyticsBuilder
        {
            public static string CategoryName { get { return typeof(Category).Name; } }
        }

        public class AnalyticsAction<Action> : AnalyticsBuilder
        {
            public static string ActionName { get { return typeof(Action).Name; } }
        }

        public class AnalyticsTransaction<Transaction>
        {
            public static string TransactionName { get { return typeof(Transaction).Name; } }

            public static void LogTransaction(CPMAdTypes type, string categoryName)
            {
                CrimsonAnalyticsFeatures.CountryCPM countryCPMPair = eCommerceTracker.GetCMPCountryPair(type);
                string transactionID = Random.value.ToString();

#if !UNITY_TVOS
                CrimsonAnalytics.MicroAnalytics.MicroAnalytics.LogEvent(transactionID, TransactionName, "ad", new System.Collections.Generic.Dictionary<string, object>() { { "value", countryCPMPair.CPM / 1000 } });
                googleAnalyticsV4.LogItem(transactionID, TransactionName, "ad", categoryName, countryCPMPair.CPM / 1000, 1);
                googleAnalyticsV4.LogTransaction(transactionID, TransactionName, countryCPMPair.CPM / 1000, 0.0f, 0.0f);
#endif
            }
        }

        public class AnalyticsAffiliation<Affiliation>
        {
            public static string AffiliationName { get { return typeof(Affiliation).Name; } }

            public static void LogTransaction(CPMAdTypes type, string categoryName, string transactionName)
            {
                CrimsonAnalyticsFeatures.CountryCPM countryCPMPair = eCommerceTracker.GetCMPCountryPair(type);
                string transactionID = Random.value.ToString();
#if !UNITY_TVOS
                CrimsonAnalytics.MicroAnalytics.MicroAnalytics.LogEvent(transactionID, transactionName, "ad", new System.Collections.Generic.Dictionary<string, object>() { { "value", countryCPMPair.CPM / 1000 } });
                googleAnalyticsV4.LogItem(transactionID, transactionName, "ad", categoryName, countryCPMPair.CPM / 1000, 1);
                googleAnalyticsV4.LogTransaction(transactionID, AffiliationName, countryCPMPair.CPM / 1000, 0.0f, 0.0f);
#endif
            }
        }

        public class AnalyticsLabelIntervals : AnalyticsLabel<AnalyticsLabelIntervals>
        {
            public static void LogEvent(string categoryName, string actionName, int[] buckets, int value, string additionalLabelText = "")
            {
                int previousBucket = 0;
                string bucketName = "Intervals." + CommonMethods.GetCrimsonAnalyticsOrder(buckets.Length) + ".   " + buckets[buckets.Length - 1].ToString() + "-inf";

                for (int i = 0; i < buckets.Length; ++i)
                {
                    if (value < buckets[i])
                    {
                        int min = previousBucket;
                        int max = buckets[i] - 1;

                        string bucketString = min != max ? min.ToString() + " - " + max.ToString() : min.ToString();

                        bucketName = "Intervals." + CommonMethods.GetCrimsonAnalyticsOrder(i + 1) + ".   " + bucketString;
                        break;
                    }
                    previousBucket = buckets[i];
                }

                if (value < buckets[0])
                {
                    bucketName = "Intervals.00.   -inf - " + (buckets[0] - 1).ToString();
                }
#if !UNITY_TVOS
                CrimsonAnalytics.MicroAnalytics.MicroAnalytics.LogEvent(categoryName, actionName, additionalLabelText + bucketName, value);
                googleAnalyticsV4.LogEvent(buildEvent()
                                    .SetEventCategory(categoryName)
                                    .SetEventAction(actionName)
                                    .SetEventLabel(additionalLabelText + bucketName)
                                    .SetEventValue(value)
                                    );
#endif
                Debug.Log(DEBUG_STRING + categoryName + " : " + actionName + ":" + additionalLabelText + bucketName + " : " + value);
            }


        }


        public class AnalyticsLabelDistribution : AnalyticsLabel<AnalyticsLabelDistribution>
        {
            public static void LogEvent(string categoryName, string actionName, int[] buckets, int value, string additionalLabelText = "")
            {
                string bucketName = "";

                for (int i = 0; i < buckets.Length; ++i)
                {
                    if (value >= buckets[i])
                    {
                        if (i == buckets.Length - 1)
                        {
                            bucketName = "Distribution." + CommonMethods.GetCrimsonAnalyticsOrder(i + 1) + ".   " + buckets[i].ToString() + "-inf";
                        }
                        else
                        {
                            int min = buckets[i];
                            int max = buckets[i + 1] - 1;

                            string bucketString = min != max ? min.ToString() + " - " + max.ToString() : min.ToString();

                            bucketName = "Distribution." + CommonMethods.GetCrimsonAnalyticsOrder(i + 1) + ".   " + bucketString;
                        }
#if !UNITY_TVOS
                        CrimsonAnalytics.MicroAnalytics.MicroAnalytics.LogEvent(categoryName, actionName, additionalLabelText + bucketName, value);
                        googleAnalyticsV4.LogEvent(buildEvent()
                                            .SetEventCategory(categoryName)
                                            .SetEventAction(actionName)
                                            .SetEventLabel(additionalLabelText + bucketName)
                                            .SetEventValue(value)
                                            );
#endif
                        Debug.Log(DEBUG_STRING + categoryName + " : " + actionName + ":" + additionalLabelText + bucketName + " : " + value);
                    }
                }
            }


        }

        public class AnalyticsLabelHistogram : AnalyticsLabel<AnalyticsLabelHistogram>
        {
            public static void LogEvent(string categoryName, string actionName, int bucketLength, int value, string additionalLabelText = "")
            {
                if (value < 0)
                {
                    Debug.LogError("AnalyticsLabelHistogram negative value");
                    return;
                }

                int min = (value / bucketLength) * bucketLength;
                int max = ((value / bucketLength) + 1) * bucketLength - 1;

                string bucketString = min != max ? min.ToString() + " - " + max.ToString() : min.ToString();

                string bucketName = "Histogram." + CommonMethods.GetCrimsonAnalyticsOrder(value / bucketLength) + ".   " + bucketString;
#if !UNITY_TVOS
                CrimsonAnalytics.MicroAnalytics.MicroAnalytics.LogEvent(categoryName, actionName, additionalLabelText + bucketName, value);
                googleAnalyticsV4.LogEvent(buildEvent()
                    .SetEventCategory(categoryName)
                    .SetEventAction(actionName)
                    .SetEventLabel(additionalLabelText + bucketName)
                    .SetEventValue(value)
                    );
#endif
                Debug.Log(DEBUG_STRING + categoryName + " : " + actionName + ":" + additionalLabelText + bucketName + " : " + value);
            }

        }

        public class AnalyticsLabel<Label> : AnalyticsBuilder
        {
            public static string LabelName { get { return typeof(Label).Name; } }

            public static void LogEvent(string categoryName, string actionName, int value)
            {
                string labelName = LabelName;
                Debug.Log(DEBUG_STRING + categoryName + " : " + actionName + ":" + labelName + " : " + value);
#if !UNITY_TVOS
                CrimsonAnalytics.MicroAnalytics.MicroAnalytics.LogEvent(categoryName, actionName, labelName, value);
                googleAnalyticsV4.LogEvent(buildEvent()
                    .SetEventCategory(categoryName)
                    .SetEventAction(actionName)
                    .SetEventLabel(labelName)
                    .SetEventValue(value)
                    );
#endif
            }

            public static void LogEvent(string categoryName, string actionName)
            {
                LogEvent(categoryName, actionName, 0);
            }
        }


        public class AnalyticsLabelCustom : AnalyticsBuilder
        {

            public static void LogEvent(string categoryName, string actionName, string labelName, int value)
            {
                Debug.Log(DEBUG_STRING + categoryName + " : " + actionName + ":" + labelName + " : " + value);
#if !UNITY_TVOS
                CrimsonAnalytics.MicroAnalytics.MicroAnalytics.LogEvent(categoryName, actionName, labelName, value);
                googleAnalyticsV4.LogEvent(buildEvent()
                    .SetEventCategory(categoryName)
                    .SetEventAction(actionName)
                    .SetEventLabel(labelName)
                    .SetEventValue(value)
                    );
#endif
            }

            public static void LogEvent(string categoryName, string actionName, string labelName)
            {
                LogEvent(categoryName, actionName, labelName, 0);
            }
        }

        public class AnalyticsLabelCustomWithTransaction : AnalyticsLabelCustom
        {
            public static void LogTransaction(string transactionID, string IAPname, string SKU, string categoryName, string affilationName, float price, string currencyCode)
            {
#if !UNITY_TVOS
                googleAnalyticsV4.LogItem(transactionID, IAPname, SKU, categoryName, price, 1, currencyCode);
                googleAnalyticsV4.LogTransaction(transactionID, affilationName, price, 0.0f, 0.0f, currencyCode);
#endif
            }
        }
    }
}