﻿using UnityEngine;
using System.Collections;

namespace CrimsonAnalytics
{
    public class PlayerInfo : CA.AnalyticsCategory<PlayerInfo>
    {
        public class Retention : CA.AnalyticsAction<Retention>
        {
            public class Days : CA.AnalyticsLabelCustom
            {
                public static void LogEvent(string daysCount)
                {
                    LogEvent(CategoryName, ActionName, daysCount);
                }
            }
        }

        public class Engagement : CA.AnalyticsAction<Engagement>
        {
            public class Cumulative : CA.AnalyticsLabelCustom
            {
                public static void LogEvent(int day, int secondsSpentInDay)
                {
                    LogEvent(CategoryName, ActionName, "Cumulative. Day-" + CommonMethods.GetCrimsonAnalyticsOrder(day), secondsSpentInDay);
                }
            }

            public class Separate : CA.AnalyticsLabelCustom
            {
                public static void LogEvent(int day, int secondsSpentInDay)
                {
                    LogEvent(CategoryName, ActionName, "Separate. Day-" + CommonMethods.GetCrimsonAnalyticsOrder(day), secondsSpentInDay);
                }
            }
        }

        public class Gameplay : CA.AnalyticsAction<Gameplay>
        {
            public class SessionTime : CA.AnalyticsLabel<SessionTime>
            {
                public static void LogEvent(int value)
                {
                    LogEvent(CategoryName, ActionName, value);
                }
            }
        }

        public class CheckpointUsage : CA.AnalyticsAction<CheckpointUsage>
        {
            public class RunWithCheckpoint : CA.AnalyticsLabelCustom
            {
                public static void LogEvent(int carID)
                {
                    LogEvent(CategoryName, ActionName, "Car-" + CommonMethods.GetCrimsonAnalyticsOrder(carID + 1) + ": RunWithCheckpoint");
                }
            }

            public class RunWithoutCheckpoint : CA.AnalyticsLabelCustom
            {
                public static void LogEvent(int carID)
                {
                    LogEvent(CategoryName, ActionName, "Car-" + CommonMethods.GetCrimsonAnalyticsOrder(carID + 1) + ": RunWithoutCheckpoint");
                }
            }
        }

        public class TimeRatio : CA.AnalyticsAction<TimeRatio>
        {
            public class GameplayTimeToTotalTime : CA.AnalyticsLabel<GameplayTimeToTotalTime>
            {
                public static void LogEvent(int value)
                {
                    LogEvent(CategoryName, ActionName, value);
                }
            }
        }

    }
}
