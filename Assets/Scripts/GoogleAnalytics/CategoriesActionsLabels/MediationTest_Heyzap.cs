using UnityEngine;
using System.Collections;

namespace CrimsonAnalytics
{
    public partial class MediationTest : CA.AnalyticsCategory<MediationTest>
    {
        public class HeyZapIntersistial : CA.AnalyticsAction<HeyZapIntersistial>
        {
            public class Closed : CA.AnalyticsLabel<Closed>
            {
                public static void LogEvent()
                {
                    LogEvent(CategoryName, ActionName);
                }
            }

            public class Clicked : CA.AnalyticsLabel<Clicked>
            {
                public static void LogEvent()
                {
                    LogEvent(CategoryName, ActionName);
                }
            }

            public class ShowAd : CA.AnalyticsLabel<ShowAd>
            {
                public static void LogEvent()
                {
                    LogEvent(CategoryName, ActionName);
                }
            }

            public class ShowAdFailed : CA.AnalyticsLabel<ShowAdFailed>
            {
                public static void LogEvent()
                {
                    LogEvent(CategoryName, ActionName);
                }
            }

            public class Fetched : CA.AnalyticsLabel<Fetched>
            {
                public static void LogEvent()
                {
                    LogEvent(CategoryName, ActionName);
                }
            }

            public class FetchFailed : CA.AnalyticsLabelCustom
            {
                public static void LogEvent(int fetchFailedCounter)
                {
                    LogEvent(CategoryName, ActionName, "FetchFailed" + fetchFailedCounter);
                }
            }

            public class FetchFailedNoInternet : CA.AnalyticsLabelCustom
            {
                public static void LogEvent(int fetchFailedCounter)
                {
                    LogEvent(CategoryName, ActionName, "FetchFailedNoInternet" + fetchFailedCounter);
                }
            }
        }

        public class HeyZapRewarded : CA.AnalyticsAction<HeyZapRewarded>
        {
            public class ShowVideo : CA.AnalyticsLabel<ShowVideo>
            {
                public static void LogEvent(float value)
                {
                    LogEvent(CategoryName, ActionName, System.Convert.ToInt32(value));
                }
            }

            public class Closed : CA.AnalyticsLabel<Closed>
            {
                public static void LogEvent(float value)
                {
                    LogEvent(CategoryName, ActionName, System.Convert.ToInt32(value));
                }
            }

            public class Loaded : CA.AnalyticsLabel<Loaded>
            {
                public static void LogEvent(float value)
                {
                    LogEvent(CategoryName, ActionName, System.Convert.ToInt32(value));
                }
            }

            public class FetchFailed : CA.AnalyticsLabelCustom
            {
                public static void LogEvent(int fetchFailedCounter)
                {
                    LogEvent(CategoryName, ActionName, "FetchFailed" + fetchFailedCounter);
                }
            }

            public class FetchFailedNoInternet : CA.AnalyticsLabelCustom
            {
                public static void LogEvent(int fetchFailedCounter)
                {
                    LogEvent(CategoryName, ActionName, "FetchFailedNoInternet" + fetchFailedCounter);
                }
            }

            public class Clicked : CA.AnalyticsLabel<Clicked>
            {
                public static void LogEvent()
                {
                    LogEvent(CategoryName, ActionName);
                }
            }

        }

        public class HeyZapBanner : CA.AnalyticsAction<HeyZapBanner>
        {
            public class Clicked : CA.AnalyticsLabel<Clicked>
            {
                public static void LogEvent()
                {
                    LogEvent(CategoryName, ActionName);
                }
            }
        }
    }
}
