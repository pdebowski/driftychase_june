using UnityEngine;
using System.Collections;

namespace CrimsonAnalytics
{
	public class Other : CA.AnalyticsCategory<Other>
	{
        public class Platform : CA.AnalyticsAction<Platform>
        {
            public class Mobile : CA.AnalyticsLabel<Mobile>
            {
                public static void LogEvent()
                {
                    LogEvent(CategoryName, ActionName);
                }
            }

            public class TV : CA.AnalyticsLabel<TV>
            {
                public static void LogEvent()
                {
                    LogEvent(CategoryName, ActionName);
                }
            }
        }

		public class InstalledAppsReporter : CA.AnalyticsAction<InstalledAppsReporter>
		{
			public class FTL2 : CA.AnalyticsLabel<FTL2>
			{
				public static void LogEvent() {
					LogEvent (CategoryName, ActionName);
				}
			}
			
			public class GeometryRace : CA.AnalyticsLabel<GeometryRace>
			{
				public static void LogEvent() {
					LogEvent (CategoryName, ActionName);
				}
			}

            public class JumpBuddies : CA.AnalyticsLabel<JumpBuddies>
            {
                public static void LogEvent()
                {
                    LogEvent(CategoryName, ActionName);
                }
            }

            public class None : CA.AnalyticsLabel<None>
			{
				public static void LogEvent() {
					LogEvent (CategoryName, ActionName);
				}
			}
		}
	
		public class CustomMetrics : CA.AnalyticsAction<CustomMetrics>
		{
			public class ContainerVersion : CA.AnalyticsLabel<ContainerVersion>
			{
				public static void LogEvent() {
					LogEvent (CategoryName, ActionName);
				}
			}
		}
			
		public class CustomDimensions : CA.AnalyticsAction<CustomDimensions>
		{
			public class ExperimentKey : CA.AnalyticsLabel<ExperimentKey>
			{
				public static void LogEvent() {
					LogEvent (CategoryName, ActionName);
				}
			}
		}
	}
}
