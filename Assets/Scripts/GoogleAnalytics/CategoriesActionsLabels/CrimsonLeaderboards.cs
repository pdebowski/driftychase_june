﻿using UnityEngine;
using System.Collections;

namespace CrimsonAnalytics
{

    public class CrimsonLeaderboards : CA.AnalyticsCategory<CrimsonLeaderboards>
    {
        public class MainScreenShowToEnter : CA.AnalyticsAction<MainScreenShowToEnter>
        {
            private const float probability = 0.01f;

            public class Showed : CA.AnalyticsLabel<Showed>
            {
                public static void LogEvent()
                {
                    if (Random.value < probability)
                    {
                        LogEvent(CategoryName, ActionName);
                    }
                }
            }

            public class Entered : CA.AnalyticsLabel<Entered>
            {
                public static void LogEvent()
                {
                    if (Random.value < probability)
                    {
                        LogEvent(CategoryName, ActionName);
                    }
                }
            }

            public class EnteredAgain : CA.AnalyticsLabel<EnteredAgain>
            {
                public static void LogEvent()
                {
                    if (Random.value < probability)
                    {
                        LogEvent(CategoryName, ActionName);
                    }
                }
            }
        }

    }

}
