using UnityEngine;
using System.Collections;

namespace CrimsonAnalytics
{
    public class PlayerPerformance : CA.AnalyticsCategory<PlayerPerformance>
    {
        public class Score : CA.AnalyticsAction<Score>
        {
            private const float probability = 0.01f;

            public static void LogAll(int value)
            {
                if (Random.value < probability)
                {
                    Average.LogEvent(value);
                    Distribution.LogEvent(value);
                    Histogram.LogEvent(value);
                }
            }

            private class Average : CA.AnalyticsLabel<Average>
            {
                public static void LogEvent(int value)
                {
                    LogEvent(CategoryName, ActionName, value);
                }
            }

            private class Distribution : CA.AnalyticsLabelDistribution
            {
                public static int[] Buckets = new int[] { 0, 5, 10, 15, 20, 25, 30, 40, 50, 60, 70, 80, 90, 100, 140, 200 };

                public static void LogEvent(int value)
                {
                    LogEvent(CategoryName, ActionName, Buckets, value);
                }
            }

            private class Histogram : CA.AnalyticsLabelHistogram
            {
                public static void LogEvent(int value)
                {
                    LogEvent(CategoryName, ActionName, 10, value);
                }
            }
        }

        public class Record : CA.AnalyticsAction<Record>
        {
            public static void LogAll(int value)
            {
                Distribution.LogEvent(value);
                Histogram.LogEvent(value);
            }

            private class Distribution : CA.AnalyticsLabelDistribution
            {
                public static int[] Buckets = new int[] { 0, 5, 10, 15, 20, 25, 30, 40, 50, 60, 70, 80, 90, 100, 140, 200 };

                public static void LogEvent(int value)
                {
                    LogEvent(CategoryName, ActionName, Buckets, value);
                }
            }

            private class Histogram : CA.AnalyticsLabelHistogram
            {
                public static void LogEvent(int value)
                {
                    LogEvent(CategoryName, ActionName, 10, value);
                }
            }
        }

        public class TotalDistance : CA.AnalyticsAction<TotalDistance>
        {
            public static void LogAll(int value)
            {
                Intervals.LogEvent(value);
                Distribution.LogEvent(value);
            }

            private class Intervals : CA.AnalyticsLabelIntervals
            {
                public static int[] Buckets = new int[] { 0, 30, 100, 300, 500, 1000, 2000, 5000, 1000, 20000, 50000, 100000 };

                public static void LogEvent(int value)
                {
                    LogEvent(CategoryName, ActionName, Buckets, value);
                }
            }

            private class Distribution : CA.AnalyticsLabelDistribution
            {
                public static int[] Buckets = new int[] { 0, 30, 100, 300, 500, 1000, 2000, 5000, 1000, 20000, 50000, 100000 };

                public static void LogEvent(int value)
                {
                    LogEvent(CategoryName, ActionName, Buckets, value);
                }
            }
        }

        public class CitySeenTimeSpent : CA.AnalyticsAction<CitySeenTimeSpent>
        {
            public class TimeSpent : CA.AnalyticsLabelCustom
            {
                public static void LogEvent(int cityIndex, int value)
                {
                    LogEvent(CategoryName, ActionName, "City-" + CommonMethods.GetCrimsonAnalyticsOrder(cityIndex), value);
                }
            }
        }

        public class BestSeenCity : CA.AnalyticsAction<BestSeenCity>
        {
            public class Distribution : CA.AnalyticsLabelDistribution
            {
                public static int[] Buckets = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 };

                public static void LogEvent(int value)
                {
                    LogEvent(CategoryName, ActionName, Buckets, value);
                }
            }
        }
    }
}
