﻿using UnityEngine;
using System.Collections;
using CrimsonAnalyticsFeatures;

namespace CrimsonAnalytics
{
    public class Transaction : CA.AnalyticsCategory<Transaction>
    {

        public class Interstitial : CA.AnalyticsAction<Interstitial>
        {
            public class Clicked : CA.AnalyticsLabel<Clicked>
            {
                public static void LogEvent()
                {
                    LogEvent(CategoryName, ActionName);
                }
            }

            public class Displayed : CA.AnalyticsLabel<Displayed>
            {
                public static void LogEvent()
                {
                    LogEvent(CategoryName, ActionName);
                }
            }
        }

        public class Banner : CA.AnalyticsAction<Banner>
        {
            public class Clicked : CA.AnalyticsLabel<Clicked>
            {
                public static void LogEvent()
                {
                    LogEvent(CategoryName, ActionName);
                }
            }

            public class Displayed : CA.AnalyticsLabel<Displayed>
            {
                public static void LogEvent()
                {
                    LogEvent(CategoryName, ActionName);
                }
            }
        }

        public class RewardedVideo : CA.AnalyticsAction<RewardedVideo>
        {
            public class Displayed : CA.AnalyticsLabel<Displayed>
            {
                public static void LogEvent()
                {
                    LogEvent(CategoryName, ActionName);
                }
            }
        }

        public class InApp : CA.AnalyticsAction<InApp>
        {
            public class Purchased : CA.AnalyticsLabelCustom
            {
                public static void LogEvent(string productName)
                {
                    LogEvent(CategoryName, ActionName, productName);
                }
            }

            public class OneDollarSpent : CA.AnalyticsLabel<OneDollarSpent>
            {
                public static void LogEvent()
                {
                    LogEvent(CategoryName, ActionName);
                }
            }
        }

    }
}
