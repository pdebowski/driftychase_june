﻿using UnityEngine;
using System.Collections;

namespace CrimsonAnalytics
{

    public class Economy : CA.AnalyticsCategory<Economy>
    {
        public class CurrencySources : CA.AnalyticsAction<CurrencySources>
        {

            public class VideoGift : CA.AnalyticsLabel<VideoGift>
            {
                public static void LogEvent(int value)
                {
                    LogEvent(CategoryName, ActionName, value);
                }
            }

            public class FreeGift : CA.AnalyticsLabel<FreeGift>
            {
                public static void LogEvent(int value)
                {
                    LogEvent(CategoryName, ActionName, value);
                }
            }

            public class Interstitial : CA.AnalyticsLabel<Interstitial>
            {
                public static void LogEvent(int value)
                {
                    LogEvent(CategoryName, ActionName, value);
                }
            }

            public class Gameplay : CA.AnalyticsLabel<Gameplay>
            {
                public static void LogEvent(int value)
                {
                    LogEvent(CategoryName, ActionName, value);
                }
            }
        }

        public class CarUnlockTimeWithMenus : CA.AnalyticsAction<CarUnlockTimeWithMenus>
        {
            public class CarBought : CA.AnalyticsLabelCustom
            {
                public static void LogEvent(int boughtCarIndex, int timeInMinutes)
                {
                    LogEvent(CategoryName, ActionName, "Car-" + CommonMethods.GetCrimsonAnalyticsOrder(boughtCarIndex), timeInMinutes);
                }
            }
        }

        public class CarUnlockTimeInGame : CA.AnalyticsAction<CarUnlockTimeInGame>
        {
            public class CarBought : CA.AnalyticsLabelCustom
            {
                public static void LogEvent(int boughtCarIndex, int timeInMinutes)
                {
                    LogEvent(CategoryName, ActionName, "Car-" + CommonMethods.GetCrimsonAnalyticsOrder(boughtCarIndex), timeInMinutes);
                }
            }
        }

        public class CashPerMinuteInGame : CA.AnalyticsAction<CashPerMinuteInGame>
        {
            public static void LogAll(int carIndex, int value, int bucketLength)
            {
                carIndex += 1;
                Average.LogEvent(carIndex, value);
                Histogram.LogEvent(carIndex, value, bucketLength);
            }

            private class Average : CA.AnalyticsLabelCustom
            {
                public static void LogEvent(int carIndex, int value)
                {
                    LogEvent(CategoryName, ActionName, "Car-" + CommonMethods.GetCrimsonAnalyticsOrder(carIndex) + " Average", value);
                }
            }

            private class Histogram : CA.AnalyticsLabelHistogram
            {
                public static void LogEvent(int carIndex, int value, int bucketLength)
                {
                    LogEvent(CategoryName, ActionName, bucketLength, value, "Car-" + CommonMethods.GetCrimsonAnalyticsOrder(carIndex) + " ");
                }
            }

            public class CarNotPlayed : CA.AnalyticsLabelCustom
            {
                public static void LogEvent(int carIndex)
                {
                    carIndex += 1;
                    LogEvent(CategoryName, ActionName, "Car-" + CommonMethods.GetCrimsonAnalyticsOrder(carIndex) + " NotPlayed", 0);
                }
            }
        }

    }

}
