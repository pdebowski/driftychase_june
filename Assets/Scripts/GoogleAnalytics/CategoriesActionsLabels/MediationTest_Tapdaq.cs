﻿using UnityEngine;
using System.Collections;
using CrimsonAnalytics;


namespace CrimsonAnalytics
{
    public partial class MediationTest : CA.AnalyticsCategory<MediationTest>
    {

        public class Tapdaq_Interstitial : CA.AnalyticsAction<Tapdaq_Interstitial>
        {
            public class Closed : CA.AnalyticsLabel<Closed>
            {
                public static void LogEvent()
                {
                    Debug.Log(CategoryName + " " + ActionName);
                    LogEvent(CategoryName, ActionName);
                }
                /*
				public static void LogEvent(string mediator,string network)
                {
					LogEvent(CategoryName, ActionName);
					MicroAnalytics.MicroAnalytics.LogEvent (CategoryName, ActionName, LabelName);

				  	LogEvent(CategoryName, ActionName, mediator + "_" + network + "_" + LabelName);


					Dictionary<string, object> parameters = new Dictionary<string, object>()
					{
						{"mediator", mediator },
						{ "network", network },
					};

					MicroAnalytics.MicroAnalytics.LogEvent(CategoryName, ActionName,LabelName, parameters);
				}
									*/
            }

            public class Clicked : CA.AnalyticsLabel<Clicked>
            {
                public static void LogEvent()
                {
                    LogEvent(CategoryName, ActionName);
                }
            }

            public class ShowAd : CA.AnalyticsLabel<ShowAd>
            {
                public static void LogEvent()
                {
                    LogEvent(CategoryName, ActionName);
                }
            }

            public class ShowAdFailed : CA.AnalyticsLabel<ShowAdFailed>
            {
                public static void LogEvent()
                {
                    LogEvent(CategoryName, ActionName);
                }
            }

            public class Fetched : CA.AnalyticsLabel<Fetched>
            {
                public static void LogEvent()
                {
                    LogEvent(CategoryName, ActionName);
                }
            }

            public class FetchFailed : CA.AnalyticsLabelCustom
            {
                public static void LogEvent(int fetchFailedCounter)
                {
                    LogEvent(CategoryName, ActionName, "FetchFailed" + fetchFailedCounter);
                }
            }

            public class FetchFailedNoInternet : CA.AnalyticsLabelCustom
            {
                public static void LogEvent(int fetchFailedCounter)
                {
                    LogEvent(CategoryName, ActionName, "FetchFailedNoInternet" + fetchFailedCounter);
                }
            }
        }

        public class Tapdaq_Rewarded : CA.AnalyticsAction<Tapdaq_Rewarded>
        {
            public class ShowVideo : CA.AnalyticsLabel<ShowVideo>
            {
                public static void LogEvent(float value)
                {
                    LogEvent(CategoryName, ActionName, System.Convert.ToInt32(value));
                }
            }

            public class ShowFailed : CA.AnalyticsLabel<ShowFailed>
            {
                public static void LogEvent(float value)
                {
                    LogEvent(CategoryName, ActionName, System.Convert.ToInt32(value));
                }
            }

            public class Closed : CA.AnalyticsLabel<Closed>
            {
                public static void LogEvent(float value)
                {
                    LogEvent(CategoryName, ActionName, System.Convert.ToInt32(value));
                }
            }

            public class Loaded : CA.AnalyticsLabel<Loaded>
            {
                public static void LogEvent(float value)
                {
                    LogEvent(CategoryName, ActionName, System.Convert.ToInt32(value));
                }
            }

            public class FetchFailed : CA.AnalyticsLabelCustom
            {
                public static void LogEvent(int fetchFailedCounter)
                {
                    LogEvent(CategoryName, ActionName, "FetchFailed" + fetchFailedCounter);
                }
            }

            public class FetchFailedNoInternet : CA.AnalyticsLabelCustom
            {
                public static void LogEvent(int fetchFailedCounter)
                {
                    LogEvent(CategoryName, ActionName, "FetchFailedNoInternet" + fetchFailedCounter);
                }
            }

            public class Clicked : CA.AnalyticsLabel<Clicked>
            {
                public static void LogEvent()
                {
                    LogEvent(CategoryName, ActionName);
                }
            }

        }

        public class Tapdaq_Banner : CA.AnalyticsAction<Tapdaq_Banner>
        {
            public class AvailableInSession : CA.AnalyticsLabel<AvailableInSession>
            {
                public static void LogEvent()
                {
                    LogEvent(CategoryName, ActionName);
                }
            }

            public class Clicked : CA.AnalyticsLabel<Clicked>
            {
                public static void LogEvent()
                {
                    LogEvent(CategoryName, ActionName);
                }
            }

        }
    }
}

