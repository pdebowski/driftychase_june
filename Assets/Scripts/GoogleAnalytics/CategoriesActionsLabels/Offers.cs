using UnityEngine;
using System.Collections;

namespace CrimsonAnalytics
{
	public class Offers : CA.AnalyticsCategory<Offers>
	{
		public class VideoGift : CA.AnalyticsAction<VideoGift>
		{
			public class Displayed : CA.AnalyticsLabel<Displayed>
			{
				public static void LogEvent() {
					LogEvent (CategoryName, ActionName);
				}
			}
			
			public class Skipped : CA.AnalyticsLabel<Skipped>
			{
				public static void LogEvent() {
					LogEvent (CategoryName, ActionName);
				}
			}
			
			public class Collected : CA.AnalyticsLabel<Collected>
			{
				public static void LogEvent() {
					LogEvent (CategoryName, ActionName);
				}
			}
		}

        public class FreeGift : CA.AnalyticsAction<FreeGift>
        {
            public class Collected : CA.AnalyticsLabel<Collected>
            {
                public static void LogEvent()
                {
                    LogEvent(CategoryName, ActionName);
                }
            }
        }        

        public class Continue : CA.AnalyticsAction<Continue>
		{
			public class Displayed : CA.AnalyticsLabel<Displayed>
			{
				public static void LogEvent() {
					LogEvent (CategoryName, ActionName);
				}
			}
			
			public class Skipped : CA.AnalyticsLabel<Skipped>
			{
				public static void LogEvent() {
					LogEvent (CategoryName, ActionName);
				}
			}		
			
			public class Collected : CA.AnalyticsLabel<Collected>
			{
				public static void LogEvent() {
					LogEvent (CategoryName, ActionName);
				}
			}
		}

        public class ActivateCheckpointInStore : CA.AnalyticsAction<ActivateCheckpointInStore>
        {
            public class Collected : CA.AnalyticsLabel<Collected>
            {
                public static void LogEvent()
                {
                    LogEvent(CategoryName, ActionName);
                }
            }
        }

        public class ActivateCheckpointInOffers : CA.AnalyticsAction<ActivateCheckpointInOffers>
        {
            public class Displayed : CA.AnalyticsLabel<Displayed>
            {
                public static void LogEvent()
                {
                    LogEvent(CategoryName, ActionName);
                }
            }

            public class Skipped : CA.AnalyticsLabel<Skipped>
            {
                public static void LogEvent()
                {
                    LogEvent(CategoryName, ActionName);
                }
            }

            public class Collected : CA.AnalyticsLabel<Collected>
            {
                public static void LogEvent()
                {
                    LogEvent(CategoryName, ActionName);
                }
            }
        }

    }
}
