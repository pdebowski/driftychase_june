﻿using UnityEngine;
using System.Collections;

namespace CrimsonAnalytics
{

	public class SelfMonitoring : CA.AnalyticsCategory<SelfMonitoring>
	{
		public class Errors : CA.AnalyticsAction<Errors>
		{
			public class Error : CA.AnalyticsLabelCustom
			{
				public static void LogEvent(string value)
				{
					LogEvent(CategoryName, ActionName, value);
				}
			}
		}
	}

}
