﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System.Collections.Generic;

public class CityArchitect : MonoBehaviour
{
    private const int FIXED_CITIES = 10;
    private const int CITY_ROADS_COUNT = 10;

    public Vector3 StartCrossroadPosition
    {
        get { return startCityPosition + Vector3.forward * GetRightRoad(StartID).width / 2; }
        private set {; }
    }
    public Vector3 StartID { get; private set; }
    private int MaxCrossroadID { get { return CITY_ROADS_COUNT * FIXED_CITIES / 2; } }

    [SerializeField]
    private CityRoadsSet[] roadSets;
    [SerializeField]
    private CityRoadsSet startSet;
    [SerializeField]
    private Vector2 startID;
    [SerializeField]
    private List<FixedRoad> fixedRoads;
    [SerializeField]
    private RoadsContainer roadsContainer;
    [SerializeField]
    private CityCreator cityCreator;

    private bool isTutorialRun = false;
    private bool isStartSet = true;
    private Vector3 startCityPosition;


    void Awake()
    {
        SetFixedRoads();
    }

    public void Init()
    {
        StartID = new Vector3(startID.x, 0, startID.y);
        startCityPosition = cityCreator.transform.position;
    }

    void OnEnable()
    {
        GameplayManager.OnSessionEnded += SetFixedRoads;
    }

    void OnDisable()
    {
        GameplayManager.OnSessionEnded -= SetFixedRoads;
    }

    public void SetFixedRoads()
    {
        SetFixedRoads(Random.Range(1, 999999999));
    }

    public void SetFixedRoads(int seed)
    {
        fixedRoads.Clear();
        Vector2 nextCityID = startID;
        fixedRoads.Add(new FixedRoad(false, 0, 2));

        nextCityID = AddtoFixedRoads(startID, startSet.roadsIDs, null);

        System.Random rand = new System.Random(seed);

        for (int i = 0; i < FIXED_CITIES - 1; i++)
        {
            nextCityID = AddtoFixedRoads(nextCityID, roadSets[rand.Next(0, roadSets.Length)].roadsIDs, rand);
        }

        fixedRoads.Add(new FixedRoad(true, MaxCrossroadID - 1, 2));
    }

    private Vector2 AddtoFixedRoads(Vector2 startCrossroadID, List<int> roads, System.Random rand)
    {
        bool isForwardRoad = true;
        bool isRandom = rand != null;
        List<int> tempRoads = new List<int>();

        roads.CopyTo(tempRoads);

        for (int i = 0; i < roads.Count; i += 2)
        {
            int random = isRandom ? rand.Next(0, tempRoads.Count) : i;
            fixedRoads.Add(new FixedRoad(isForwardRoad, (int)startCrossroadID.x, tempRoads[random]));
            isForwardRoad = !isForwardRoad;
            if (isRandom)
            {
                tempRoads.RemoveAt(random);
            }

            startCrossroadID += new Vector2(1, 1);

            random = isRandom ? rand.Next(0, tempRoads.Count) : (i + 1);
            fixedRoads.Add(new FixedRoad(isForwardRoad, (int)startCrossroadID.y, tempRoads[random]));
            isForwardRoad = !isForwardRoad;
            if (isRandom)
            {
                tempRoads.RemoveAt(random);
            }
        }

        return startCrossroadID;
    }

    public RoadInfo GetRoad(Vector3 direction, Vector3 crossroadID)
    {

        if (direction == Vector3.forward)
        {
            return GetForwardRoad(crossroadID);
        }
        else if (direction == Vector3.back)
        {
            crossroadID -= Vector3.forward;
            return GetForwardRoad(crossroadID);
        }
        else if (direction == Vector3.right)
        {
            return GetRightRoad(crossroadID);
        }
        else
        {
            crossroadID -= Vector3.right;
            return GetRightRoad(crossroadID);
        }

    }

    public RoadInfo GetStartRoad()
    {
        return GetRoad(transform.forward, StartID);
    }

    public RoadInfo GetForwardRoad(Vector3 crossroadID)
    {
        FixedRoad fixedRoad = fixedRoads.FirstOrDefault(x => x.isForwardRoad == true && x.roadID == (crossroadID.x % MaxCrossroadID));
        if (fixedRoad != null)
        {
            return roadsContainer.AvailableRoads[fixedRoad.roadArrayID];
        }
        return GetForwardRoad(crossroadID.x);
    }

    public RoadInfo GetRightRoad(Vector3 crossroadID)
    {
        FixedRoad fixedRoad = fixedRoads.FirstOrDefault(x => x.isForwardRoad == false && x.roadID == (crossroadID.z % MaxCrossroadID));
        if (fixedRoad != null)
        {
            return roadsContainer.AvailableRoads[fixedRoad.roadArrayID];
        }
        return GetRightRoad(crossroadID.z);
    }

    private RoadInfo GetRightRoad(float id)
    {
        Debug.Log("xle");
        return GetNextRoad();
    }

    private RoadInfo GetForwardRoad(float id)
    {
        Debug.Log("xle");
        if (id == startID.x)
        {
            return roadsContainer.AvailableRoads[1];
        }
        else
        {
            return GetNextRoad();
        }
    }

    private RoadInfo GetNextRoad()
    {
        return roadsContainer.AvailableRoads.RandomElement();
    }

    public Vector3 GetRoadPosition(Vector3 crossroadID, Vector3 direction)
    {
        Vector3 crossroadPosition = GetCrossroadPosition(crossroadID);
        RoadInfo rightRoad = GetRightRoad(crossroadID);
        RoadInfo forwardRoad = GetForwardRoad(crossroadID);

        if (direction == Vector3.forward || direction == Vector3.back)
        {
            return crossroadPosition + direction * (rightRoad.width / 2 + forwardRoad.length / 2);
        }
        else
        {
            return crossroadPosition + direction * (forwardRoad.width / 2 + rightRoad.length / 2);
        }

    }

    public Vector3 GetCrossroadPosition(Vector3 crossroadID)
    {
        float Zpos = GetOffset(StartID, crossroadID, Vector3.forward);
        float Xpos = GetOffset(StartID, crossroadID, Vector3.right);
        return StartCrossroadPosition + new Vector3(Xpos, 0, Zpos);
    }



    private float GetOffset(Vector3 from, Vector3 to, Vector3 directionStep)
    {

        int intFrom = directionStep == Vector3.forward ? (int)from.z : (int)from.x;
        int intTo = directionStep == Vector3.forward ? (int)to.z : (int)to.x;

        if (intFrom == intTo)
        {
            return 0;
        }

        int yOffsetSign = (int)Mathf.Sign(intTo - intFrom);
        Vector3 sideDirectionToStep = directionStep == Vector3.forward ? Vector3.right : Vector3.forward;
        float result = 0;
        float toAdd = 0;

        result += (yOffsetSign * GetRoad(directionStep, from).length);
        for (int i = intFrom; i != intTo + yOffsetSign; i += yOffsetSign)
        {
            if (i != intFrom && i != intTo)
            {
                toAdd = (yOffsetSign * GetRoad(sideDirectionToStep, from).width);
                toAdd += (yOffsetSign * GetRoad(directionStep, from).length);
            }
            else
            {
                toAdd = (yOffsetSign * GetRoad(sideDirectionToStep, from).width / 2);
            }
            from += yOffsetSign * directionStep;
            result += toAdd;
        }

        return result;
    }

    public Vector3 GetCrossroadSize(Vector3 crossroadID)
    {
        float height = GetRightRoad(crossroadID).width;
        float width = GetForwardRoad(crossroadID).width;
        return new Vector3(width, height, 1);
    }

    public bool CouldSpawnTrafficAt(Vector3 crossroadID)
    {
        if (crossroadID == StartID || crossroadID == StartID + Vector3.forward || crossroadID == StartID + Vector3.back)
        {
            return false;
        }
        return true;
    }
}

[System.Serializable]
public class FixedRoad
{
    public bool isForwardRoad;
    public int roadID;
    public int roadArrayID;

    public FixedRoad(bool isForwardRoad, int roadID, int roadArrayID)
    {
        this.isForwardRoad = isForwardRoad;
        this.roadID = roadID;
        this.roadArrayID = roadArrayID;
    }
}

[System.Serializable]
public class CityRoadsSet
{
    public List<int> roadsIDs;
}