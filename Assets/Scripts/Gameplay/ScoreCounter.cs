using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using CrimsonPlugins;
using System.Collections.Generic;

public class ScoreCounter : Singleton<ScoreCounter>
{
    public static System.Action OnCashOrScoreChanged = delegate { };

    private AntiHackInt currentScoreAntiHack;
    private AntiHackInt sessionCashAntiHack;
    private AntiHackInt startScoreAntiHack;
    private AntiHackInt continueScoreAntiHack;

    public int CurrentScore
    {
        get
        {
            InitializeIfNeeded(currentScoreAntiHack);
            return currentScoreAntiHack.p_value;
        }
        set
        {
            InitializeIfNeeded(currentScoreAntiHack);
            currentScoreAntiHack.p_value = value;
        }
    }
    public int SessionCash
    {
        get
        {
            InitializeIfNeeded(sessionCashAntiHack);
            return sessionCashAntiHack.p_value;

        }
        set
        {
            InitializeIfNeeded(sessionCashAntiHack);
            sessionCashAntiHack.p_value = value;

        }
    }

    public int StartScore
    {
        get
        {
            InitializeIfNeeded(startScoreAntiHack);
            return startScoreAntiHack.p_value;
        }
        set
        {
            InitializeIfNeeded(startScoreAntiHack);
            startScoreAntiHack.p_value = value;
        }
    }

    public int ContinueScore
    {
        get
        {
            InitializeIfNeeded(continueScoreAntiHack);
            return continueScoreAntiHack.p_value;
        }
        set
        {
            InitializeIfNeeded(continueScoreAntiHack);
            continueScoreAntiHack.p_value = value;
        }
    }

    public int CashFromFreeGift { get; set; }
    public int CashFromInterstitial { get; set; }

    private bool firstRun = true;
    bool firstReport = true;

    override protected void Awakened()
    {
        currentScoreAntiHack = new AntiHackInt();
        CurrentScore = 0;
        sessionCashAntiHack = new AntiHackInt();
        SessionCash = 0;
        startScoreAntiHack = new AntiHackInt();
        StartScore = 0;
        continueScoreAntiHack = new AntiHackInt();
        ContinueScore = 0;

        Restart();
        GameplayManager.OnSessionEnded += Restart;
        firstRun = false;
    }

    void OnDestroy()
    {
        GameplayManager.OnSessionEnded -= Restart;
    }

    void OnEnable()
    {
        ContinueSceneManager.OnEnter += SaveContinueScore;
    }

    void OnDisalbe()
    {
        ContinueSceneManager.OnEnter -= SaveContinueScore;
    }

    private void SaveContinueScore()
    {
        ContinueScore = CurrentScore;
    }

    public void AddScore()
    {
        CurrentScore += 1;
        CheckHighScore();
        RefreshVisuals();
    }

    public void SetStartScore(int score)
    {
        CurrentScore = score;
        StartScore = score;
        RefreshVisuals();
    }

    public void AddCash(int value)
    {
        SessionCash += value;
        RefreshVisuals();
    }

    public void Restart()
    {
        if (!firstRun)
        {
            CrimsonAnalytics.Economy.CurrencySources.Gameplay.LogEvent(SessionCash - (CashFromFreeGift + CashFromInterstitial));
            CrimsonAnalytics.PlayerPerformance.Score.LogAll(CurrentScore);
            CheckTotalDistance();
            TransferCollectedCoins();
            firstReport = false;
        }

        CurrentScore = StartScore;
        SessionCash = 0;
        ContinueScore = 0;
        CashFromInterstitial = 0;
        CashFromFreeGift = 0;
        RefreshVisuals();
    }

    private void TransferCollectedCoins()
    {
        if (SessionCash != 0)
        {
            PrefsDataFacade.Instance.AddTotalCash(SessionCash);
        }
        SessionCash = 0;
        RefreshVisuals();
    }

    public int GetScore()
    {
        return CurrentScore;
    }

    public void SetRecordAndTotalDistance(bool reportForce, int totalDistanceToAdd)
    {
        PrefsDataFacade.Instance.SetRecord(CurrentScore, reportForce);
        PrefsDataFacade.Instance.SetTotalDistance(PrefsDataFacade.Instance.GetTotalDistance() + totalDistanceToAdd);
    }

    public void RefreshVisuals()
    {
        OnCashOrScoreChanged();
    }


    void CheckHighScore()
    {

        Dictionary<RCAchievement, int> highscoreAchDict = new Dictionary<RCAchievement, int> {
            {RCAchievement.HIGHSCORE15,15},
            {RCAchievement.HIGHSCORE30,30},
            {RCAchievement.HIGHSCORE50,60},
            {RCAchievement.HIGHSCORE60,90},
            {RCAchievement.HIGHSCORE70,120},
            {RCAchievement.HIGHSCORE80,160},
            {RCAchievement.HIGHSCORE90,200},
            {RCAchievement.HIGHSCORE100,300}
        };

        foreach (var pair in highscoreAchDict)
        {
            if (CurrentScore >= pair.Value && !PlayerPrefs.HasKey(pair.Key.ToString() + "Earned"))
            {
                RankingCloudAdapter.Instance.p_rankingInstance.AchievementUnlock(pair.Key);
                PlayerPrefs.SetInt(pair.Key.ToString() + "Earned", 1);
            }
        }


    }

    void CheckTotalDistance()
    {
        float totalDistance = System.Convert.ToSingle(PrefsDataFacade.Instance.GetTotalDistance());

        Dictionary<RCAchievement, float> distanceAchievements = new Dictionary<RCAchievement, float> {
            {RCAchievement.TOTALDISTANCE100,100f},
            {RCAchievement.TOTALDISTANCE500,500f},
            {RCAchievement.TOTALDISTANCE2000,2000f},
            {RCAchievement.TOTALDISTANCE5000,5000f},
            {RCAchievement.TOTALDISTANCE20000,20000f},
            {RCAchievement.TOTALDISTANCE100000,100000f}
        };


        foreach (var ach in distanceAchievements)
        {
            if (!PlayerPrefs.HasKey(ach.ToString() + "Earned"))
            {
                float progress = totalDistance / ach.Value;
                RankingCloudAdapter.Instance.p_rankingInstance.AchievementIncrement(ach.Key, progress);
                if (progress > 1)
                {
                    PlayerPrefs.SetInt(ach.Key.ToString() + "Earned", 1);
                }
            }
        }
    }

    private void InitializeIfNeeded(AntiHackInt antihackInt)
    {
        if (antihackInt == null)
        {
            antihackInt = new AntiHackInt();
            antihackInt.p_value = 0;
        }
    }
}
