using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameplayInterstitialAd : MonoBehaviour
{

    [SerializeField]
    private PlayerInputStartDelay startDelay;
    [SerializeField]
    private int coinsCount = 5;
    [SerializeField]
    private BaseAnimation startAnim;
    [SerializeField]
    private BaseAnimation endAnim;
    [SerializeField]
    private AnimationCurve coinsMove;
    [SerializeField]
    private float coinsSpawningDuration = 0.7f;
    [SerializeField]
    private MusicManager musicManager;

    List<CoinBag> coins;
    private CoinsPool coinsPool;


    static float intestistialPercentProb = 0;
    static float probRefreshTime = -100;
    public static float IntestistialPercentProb
    {
        get
        {
            if (Time.time > probRefreshTime + 10)
            {
                probRefreshTime = Time.time;
                intestistialPercentProb = Random.value;
            }
            return intestistialPercentProb;
        }
    }

    public bool InterstitialWasDisplayed { get; set; }

    void Start()
    {
        coinsPool = CoinsSpawningSettings.Instance.gameObject.GetComponent<CoinsPool>();
    }

    void OnEnable()
    {
        GameplayManager.OnSessionStarted += OnSessionStarted;
        GameplayManager.OnSessionEnded += ReturnCoins;
        startAnim.OnAnimationEnd += ShowAd;
        endAnim.OnAnimationEnd += StartGame;
    }

    void OnDisable()
    {
        GameplayManager.OnSessionEnded -= OnSessionStarted;
        GameplayManager.OnSessionEnded -= ReturnCoins;
        startAnim.OnAnimationEnd -= ShowAd;
        endAnim.OnAnimationEnd -= StartGame;

        startAnim.transform.parent.gameObject.SetActive(false);
        endAnim.transform.parent.gameObject.SetActive(false);
    }

    private void ReturnCoins()
    {
        if (coins != null && coins.Count > 0)
        {
            foreach (var item in coins)
            {
                item.SetAdSpawner(null);
                coinsPool.Return(item);
            }
            coins.Clear();
        }

    }

    public void ReturnCoin(CoinBag coin)
    {
        if (coin != null)
        {
            coin.SetAdSpawner(null);
            coinsPool.Return(coin);
            coins.Remove(coin);
        }
    }


    private void OnSessionStarted()
    {
        if (Time.timeScale < 0.5f) { return; }

        bool showInterstitial = GameplayInterstitialAd.IntestistialPercentProb < GTMParameters.InterstitialAfterStartFrequency ? true : false;

        if (!Tutorial.IsTutorial && !DailyGiftManager.IsDailyGiftActive && showInterstitial && !VIP.IsVIPAccount && PrefabReferences.Instance.AdAdapter.IsInterstitialReady())
        {
#if !UNITY_EDITOR
                 if (PrefabReferences.Instance.AdAdapter.IsInterstitialReady() )
                 {
                     musicManager.PauseMusic();
                     CommonMethods.SetTimeScaleSmoothed(0);
                     PlayerInput.isEnabled = false;
                     startAnim.transform.parent.gameObject.SetActive(true);
                     return;
                 }
#endif
        }

        startDelay.StartDelay();
        GameManager.Instance.CurrentState = GameManager.GameState.Playing;
    }

    private void ShowAd()
    {
        if (Time.timeScale < 0.5f) { Time.timeScale = 0f; return; }
        PrefabReferences.Instance.AdAdapter.OnIntersistialClosed += OnInterstitialClosed;
        PrefabReferences.Instance.AdAdapter.ShowInterstitial();
    }

    private void OnInterstitialClosed()
    {
        startAnim.transform.parent.gameObject.SetActive(false);
        Time.timeScale = 1f;
        PrefabReferences.Instance.AdAdapter.OnIntersistialClosed -= OnInterstitialClosed;
        InterstitialWasDisplayed = true;

        endAnim.transform.parent.gameObject.SetActive(true);
    }

    private void StartGame()
    {
        endAnim.transform.parent.gameObject.SetActive(false);
        StartCoroutine(ManageCoins());
    }

    private IEnumerator ManageCoins()
    {
        yield return null;
        Transform player = PrefabReferences.Instance.Player;
        coins = new List<CoinBag>();
        List<Vector3> coinsDestination = new List<Vector3>();

        Vector3 spawnPosition = player.transform.position + player.forward * 17.5f;
        for (int i = 0; i < coinsCount; i++)
        {
            coins.Add(coinsPool.Get());
            coins[i].SetCoinValue(CoinsSpawningSettings.Instance.coinValue);
            coins[i].canMove = false;
            coins[i].SetAdSpawner(this);
            coins[i].transform.position = spawnPosition;
            coinsDestination.Add(player.transform.position + player.forward * Random.Range(5, 30) + player.right * Random.Range(-8, 8) + player.up * 1);
        }


        float phase = 0;

        while (phase < 1)
        {
            phase += Time.unscaledDeltaTime / coinsSpawningDuration;
            for (int i = 0; i < coins.Count; i++)
            {
                coins[i].transform.position = Vector3.Lerp(spawnPosition, coinsDestination[i], coinsMove.Evaluate(phase));
            }
            yield return null;
        }

        CommonMethods.SetTimeScaleSmoothed(1);
        musicManager.UnPauseMusic();
        GameManager.Instance.CurrentState = GameManager.GameState.Playing;
        foreach (var item in coins)
        {
            item.canMove = true;
        }
        CrimsonAnalytics.Economy.CurrencySources.Interstitial.LogEvent(CoinsSpawningSettings.Instance.coinValue * coinsCount);
        ScoreCounter.Instance.CashFromInterstitial = CoinsSpawningSettings.Instance.coinValue * coinsCount;
        PlayerInput.isEnabled = true;
    }
}
