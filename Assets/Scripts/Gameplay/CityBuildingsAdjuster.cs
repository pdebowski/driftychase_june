using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CityBuildingsAdjuster : MonoBehaviour
{

    [SerializeField]
    private Transform roadsSpawner;
    [SerializeField]
    private CityArchitect cityArchitect;
    [SerializeField]
    private Vector3 bankSquarePartID;
    [SerializeField]
    private Vector3[] startBuildingPositions;
    [SerializeField]
    private CityBuildingsContainer cityBuildingsContainer;
    [SerializeField]
    private CityBuildingsContainer cityBanksContainer;

    private List<Vector3> activeBuildingsCoordinate;
    private List<Transform> buildings;
    List<Vector3> rotations;

    void Awake()
    {
        activeBuildingsCoordinate = new List<Vector3>();
        buildings = new List<Transform>();

        rotations = new List<Vector3>();
        rotations.Add(Vector3.left);
        rotations.Add(Vector3.right);
        rotations.Add(Vector3.forward);
        rotations.Add(Vector3.back);
    }

    public void Restart()
    {
        buildings.Clear();
        activeBuildingsCoordinate.Clear();
        cityBuildingsContainer.Restart();
        cityBanksContainer.Restart();
    }

    public void RemoveNotNeededBuildings(Vector3 lastCrossroadID, Vector3 nextCrossroadID)
    {
#if !NO_OPTIM
        StartCoroutine(RemoveBuildings(lastCrossroadID, nextCrossroadID));
#else
        DoRemoveBuildings(lastCrossroadID, nextCrossroadID);
#endif
    }

    IEnumerator RemoveBuildings(Vector3 lastCrossroadID, Vector3 nextCrossroadID)
    {
        yield return 0;
        DoRemoveBuildings(lastCrossroadID, nextCrossroadID);
    }
    void DoRemoveBuildings(Vector3 lastCrossroadID, Vector3 nextCrossroadID)
    {
        for (int i = 0; i < activeBuildingsCoordinate.Count; i++)
        {
            if (!IsBuildingIDAtCrossroad(lastCrossroadID, activeBuildingsCoordinate[i])
                && !IsBuildingIDAtCrossroad(nextCrossroadID, activeBuildingsCoordinate[i]))
            {
                activeBuildingsCoordinate.RemoveAt(i);
                cityBuildingsContainer.EndUse(buildings[i]);
                buildings.RemoveAt(i);
                i--;
            }
        }
    }

    public bool IsBuildingIDAtCrossroad(Vector3 crossroadID, Vector3 buildingID)
    {
        float x = Mathf.Abs(crossroadID.x - buildingID.x);
        float z = Mathf.Abs(crossroadID.z - buildingID.z);
        return x == 0.5f && z == 0.5f;
    }

    public void SpawnStartBuildings()
    {
        foreach (var item in startBuildingPositions)
        {
            Transform newBuilding = cityBuildingsContainer.GetNextPart();
            SetCityPart(newBuilding, item, Vector3.zero, true, false);
        }
    }

    public void SpawnBuildings(Vector3 crossroadID,bool onTurn=false)
    {

#if NO_OPTIM
        onTurn=false;
#endif
        if (onTurn)
        {
            StartCoroutine(SpawnBuildingsCor(crossroadID));
        }
        else
        {
            RoadInfo rightRoad = cityArchitect.GetRoad(-roadsSpawner.transform.right, crossroadID);
            Vector3 newPosition = roadsSpawner.position;
            newPosition += -roadsSpawner.right * (rightRoad.length / 2);

            RoadInfo forwardRoad = cityArchitect.GetRoad(roadsSpawner.transform.forward, crossroadID);
            Vector3 offset = roadsSpawner.forward * (rightRoad.width / 2 + forwardRoad.length / 2);
            SpawnPart(newPosition + (-roadsSpawner.right * forwardRoad.width), offset, crossroadID, 1);
            forwardRoad = cityArchitect.GetRoad(-roadsSpawner.transform.forward, crossroadID);
            offset = roadsSpawner.forward * (rightRoad.width / 2 + forwardRoad.length / 2);
            SpawnPart(newPosition + (-roadsSpawner.right * forwardRoad.width), offset, crossroadID, -1);
        }
    }

    IEnumerator SpawnBuildingsCor(Vector3 crossroadID)
    {
        RoadInfo rightRoad = cityArchitect.GetRoad(-roadsSpawner.transform.right, crossroadID);
        Vector3 newPosition = roadsSpawner.position;
        newPosition += -roadsSpawner.right * (rightRoad.length / 2);

        RoadInfo forwardRoad = cityArchitect.GetRoad(roadsSpawner.transform.forward, crossroadID);
        Vector3 offset = roadsSpawner.forward * (rightRoad.width / 2 + forwardRoad.length / 2);
        yield return 0;

        SpawnPart(newPosition + (-roadsSpawner.right * forwardRoad.width), offset, crossroadID, 1);
        forwardRoad = cityArchitect.GetRoad(-roadsSpawner.transform.forward, crossroadID);
        offset = roadsSpawner.forward * (rightRoad.width / 2 + forwardRoad.length / 2);
        yield return 0;

        SpawnPart(newPosition + (-roadsSpawner.right * forwardRoad.width), offset, crossroadID, -1);
    }

    private void SpawnPart(Vector3 basePosition, Vector3 offset, Vector3 crossroadID, int sign)
    {
        Transform newBuilding;
        Vector3 partID = CreateID(crossroadID, (sign * roadsSpawner.forward - roadsSpawner.right) * 0.5f);
        if (!activeBuildingsCoordinate.Contains(partID))
        {
            bool addToList;
            bool randomRotation;

            if (partID.Approximately(bankSquarePartID))
            {
                newBuilding = cityBanksContainer.GetNextPart();
                randomRotation = false;
                addToList = false;
            }
            else
            {
                newBuilding = cityBuildingsContainer.GetNextPart();
                randomRotation = true;
                addToList = true;
            }

            SetCityPart(newBuilding, basePosition + sign * offset, partID, randomRotation, addToList);
        }
    }



    private Vector3 CreateID(Vector3 crossroadID, Vector3 IDoffset)
    {
        Vector3 result = Vector3.zero;
        result.x = Mathf.Round((crossroadID.x + IDoffset.x) * 10) / 10;
        result.y = Mathf.Round((crossroadID.y + IDoffset.y) * 10) / 10;
        result.z = Mathf.Round((crossroadID.z + IDoffset.z) * 10) / 10;
        return result;
    }

    private void SetCityPart(Transform cityPart, Vector3 position, Vector3 id, bool randomRotation, bool addToList)
    {
        cityPart.position = position;
        if (randomRotation)
        {
            cityPart.transform.rotation = Quaternion.LookRotation(GetRandomRotation());
        }
        cityPart.gameObject.SetActive(true);
        if (addToList)
        {
            buildings.Add(cityPart);
            activeBuildingsCoordinate.Add(id);
            RestartPart(cityPart);
        }

    }

    private Vector3 GetRandomRotation()
    {
        return rotations.RandomElement<Vector3>();
    }

    private void RestartPart(Transform cityPart)
    {
        foreach (var humanSpawnPoint in cityPart.GetComponentsInChildren<HumanSpawnPoint>())
        {
            humanSpawnPoint.Restart();
        }
    }
}
