using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerCarModelAdapter : MonoBehaviour
{
    public PlayerCarModel CurrentPlayerCarModel { get; private set; }

    [SerializeField]
    private PoliceManager policeManager;
    [SerializeField]
    private CityCreator cityCreator;

    private PlayerController playerController;
    private PlayerSpeedManager playerSpeed;
    private PlayerCollision playerCollision;
    private PlayerParticles playerParticles;
    private PlayerSounds playerSounds;
    private BoxCollider boxCollider;



    void Start()
    {
        playerController = GetComponent<PlayerController>();
        playerSpeed = GetComponent<PlayerSpeedManager>();
        playerCollision = GetComponent<PlayerCollision>();
        playerParticles = GetComponent<PlayerParticles>();
        playerSounds = GetComponent<PlayerSounds>();
        boxCollider = GetComponent<BoxCollider>();

        SetPlayerCarFromPrefs();
    }

    public void SetPlayerCarFromPrefs()
    {
        SetPlayerCar(PlayerPrefsAdapter.SelectedCar);
    }

    public void SetPlayerCar(CarDefinitions.CarEnum carEnum)
    {
        GameObject prefab = Resources.Load<GameObject>(carEnum.ToString());
        SetPlayerCar(prefab);
    }

    private void SetPlayerCar(GameObject prefab)
    {
        PlayerCarModel previousPlayerCar = CurrentPlayerCarModel;

        GameObject playerCar = Instantiate(prefab) as GameObject;
        playerCar.transform.SetParent(this.transform);
        playerCar.transform.localPosition = Vector3.zero;
        playerCar.transform.localRotation = Quaternion.identity;

        CurrentPlayerCarModel = playerCar.GetComponent<PlayerCarModel>();

        playerParticles.Init(CurrentPlayerCarModel.DriftParticles, CurrentPlayerCarModel.Explosions);
        playerController.Init(playerParticles.DriftParticles, CurrentPlayerCarModel.BoxCollider);
        playerCollision.Init(playerParticles.Explosions);
        CarStats carStats = PrefabReferences.Instance.CarStatsDB.GetCarStats(PlayerPrefsAdapter.SelectedCar);
        PrefabReferences.Instance.CheckpointsManager.RecalculateIsCheckpointActive();
        playerSpeed.Init(PrefabReferences.Instance.CheckpointsManager.GetStartWorldIDForSelectedCar());
        playerSounds.EngineStartClip = CurrentPlayerCarModel.EngineStartClip;
        policeManager.SwitchPoliceTo(CurrentPlayerCarModel.PolicePrefab);
        CoinSpawner.playerCarCashMultiplier = carStats.cashMultiplier;
        boxCollider.CopyFrom(CurrentPlayerCarModel.BoxCollider);



        CurrentPlayerCarModel.BoxCollider.enabled = false;

        if (previousPlayerCar != null)
        {
            Destroy(previousPlayerCar.gameObject);
            cityCreator.Restart(false);
            //Resources.UnloadUnusedAssets();
        }

    }
}
