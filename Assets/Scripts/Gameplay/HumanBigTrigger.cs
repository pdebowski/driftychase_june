﻿using UnityEngine;
using System.Collections;

public class HumanBigTrigger : MonoBehaviour {

    [SerializeField]
    private float distanceToRun = 15f;

    private HumanController human;

    void Awake()
    {
        human = GetComponentInParent<HumanController>();
    }

    void Update()
    {
        if (!human.isDead)
        {
            if(Vector3.Distance(transform.position,PrefabReferences.Instance.Player.position) < distanceToRun)
            {
                human.TryRunAway(PrefabReferences.Instance.Player);
            }
        }

    }

}
