﻿using UnityEngine;
using System.Collections;

public class TransporterSpawner : MonoBehaviour
{
    [SerializeField]
    private GameObject[] variations;

    void OnEnable()
    {
        variations.SetActive(false);
        variations.RandomElement().SetActive(true);
    }

}
