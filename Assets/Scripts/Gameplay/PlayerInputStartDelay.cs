﻿using UnityEngine;
using System.Collections;

public class PlayerInputStartDelay : MonoBehaviour
{

    [SerializeField]
    private float delay;

    public void StartDelay()
    {
        if (!Tutorial.IsTutorial && !DailyGiftManager.IsDailyGiftActive)
        {
            PlayerInput.isEnabled = false;
            StartCoroutine(ActivateAfter(delay));
        }
    }

    public void StartDelay(float delay)
    {
        PlayerInput.isEnabled = false;
        StartCoroutine(ActivateAfter(delay));
    }

    private IEnumerator ActivateAfter(float delay)
    {
        yield return CommonMethods.WaitForUnscaledSeconds(delay);
        PlayerInput.isEnabled = true;
    }


}
