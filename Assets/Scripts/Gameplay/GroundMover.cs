using UnityEngine;
using System.Collections;

public class GroundMover : MonoBehaviour
{

    [SerializeField]
    private Transform player;

    private Rigidbody rb;
    bool immedietelly = false;

    void Awake()
    {
        rb = GetComponent<Rigidbody>();
        StartCoroutine(UpdatePos());
        GameplayManager.OnSessionEnded += () =>
        {
            immedietelly = true;
        };
    }


    IEnumerator UpdatePos()
    {
        while (true)
        {
            float t = 0;
            while (t < 1f && !immedietelly)
            {
                t += Time.unscaledDeltaTime;
                yield return 0;
            }
            immedietelly = false;
            yield return new WaitForFixedUpdate();
            Vector3 newPosition = player.position;
            newPosition.y = 0;
            rb.position = newPosition;
        }
    }
}
