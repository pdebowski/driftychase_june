﻿using UnityEngine;
using System.Collections;
using System;

public class BehindTrafficSpawner : OppositeTrafficSpawner
{

    protected override CarValues SpawnTraffic(GameObject[] availableTraffic, PlayerController player)
    {
        canceled = false;

        GameObject prefab = GetRandomTrafficFrom(availableTraffic);
        if (prefab == null)
        {
            return null;
        }
        prefab = WideTrafficFilter(prefab);


        TrafficInfo trafficInfo = prefab.GetComponent<TrafficInfo>();
        CarValues trafficCar = GetTrafficValuesFromPrefab(prefab);
        if (trafficInfo.Type == TrafficTypes.Static)
        {
            return null;
        }
        CheckTrafficType(trafficInfo);

        float offsetSign = GetRandomHit();
        offsetSign = SignFilter(offsetSign, trafficInfo, trafficCar);

        float playerCarS = GetPlayerCarS(trafficInfo, trafficCar, offsetSign);
        playerCar.SetValues(player, playerCarS, offsetSign);

        bool spawnfirstWithDifferentOffsetSign = false;

        if (trafficCars.Count > 0)
        {
            spawnfirstWithDifferentOffsetSign = !SetValuesToNext(offsetSign, trafficCar, trafficInfo);
        }

        if (trafficCars.Count == 0 || spawnfirstWithDifferentOffsetSign)
        {
            SetValuesToFirst(offsetSign, trafficCar, trafficInfo);
        }

        SetSlowingDownFlag(offsetSign, trafficCar);


        trafficCar.SetDestPos(trafficDestPosInLocal);
        Vector3 trafficSpawnPositionInLocal = new Vector3(trafficPositionInLocal.x, trafficPositionInLocal.y, trafficDestPosInLocal);
        if (CouldSpawn(prefab) && !canceled)
        {
            prefab = CreateTrafficCar(prefab, trafficSpawnPositionInLocal, trafficCar.V, trafficCar);
            trafficCar.SetCarObject(prefab);
        }
        else
        {
            trafficCar = null;
        }

        return trafficCar;
    }

    private void CheckTrafficType(TrafficInfo info)
    {
        if (info.Type == TrafficTypes.WithHole)
        {
            manager.BlockNextSpecialTraffic();
        }
    }

    private void SetSlowingDownFlag(float sign, CarValues trafficCar)
    {
        if (sign == 1 && FloatEqual(trafficPositionInLocal.x, turnPointInLocal.x))
        {
            trafficCar.CouldSlowingDown = false;
        }
        else
        {
            trafficCar.CouldSlowingDown = true;
        }
    }

    private void SetValuesToFirst(float sign, CarValues trafficCar, TrafficInfo trafficInfo)
    {
        float trafficCarV = GetSpeedFor(trafficInfo);
        trafficCarV = TrafficVFilter(sign, trafficInfo, trafficCarV);

        float trafficCarS = playerCar.T * trafficCarV;
        trafficCar.SetValues(trafficCarV, trafficCarS);
        trafficCar.SetOffsetSign(sign);

        trafficDestPosInLocal = turnPointInLocal.z
            + GetSumWithSign(-reversedSpawnPointSign * sign, trafficCar.Size.z / 2, playerCar.Size.x / 2)
            + GetSafeDistance(-reversedSpawnPointSign * sign)
            + GetSafeDistanceOffset(-reversedSpawnPointSign * sign);

        trafficDestPosInLocal = DestPosOtherTrafficFilter(trafficCar, trafficDestPosInLocal);
        trafficDestPosInLocal = DestPosFilter(trafficInfo, trafficCar, trafficDestPosInLocal);

    }

    //Return true if traffic with this sign is available to spawn
    private bool SetValuesToNext(float sign, CarValues trafficCar, TrafficInfo trafficInfo)
    {
        CarValues firstCarInTraffic = trafficCars.Find(x => x.OffsetSign == sign);
        CarValues previousCarInTraffic = trafficCars.FindLast(x => x.OffsetSign == sign);
        if (firstCarInTraffic == null)
        {
            return false;
        }
        else
        {
            float trafficCarV = firstCarInTraffic.V + GetTrafficColumnSpeedOffset();
            if (sign == 1)
            {
                trafficCarV = RandomBetween(firstCarInTraffic.V, firstCarInTraffic.V + 10);
            }
            else
            {
                trafficCarV = RandomBetween(firstCarInTraffic.V - manager.MaxTrafficColumnSpeedOffset, firstCarInTraffic.V);
            }
            float trafficCarS = previousCarInTraffic.S + GetOffsetBeetwenTraffic(-sign) + GetTrafficSizeOffset(-sign, trafficCar, previousCarInTraffic);

            trafficCar.SetValues(trafficCarV, trafficCarS);
            trafficCar.SetOffsetSign(sign);
            trafficDestPosInLocal = previousCarInTraffic.DestinationPosition;
            trafficDestPosInLocal = DestPosOtherTrafficFilter(trafficCar, trafficDestPosInLocal);
            return true;
        }
    }

    private float TrafficVFilter(float sign, TrafficInfo info, float currentV)
    {
        if (sign == -1)
        {
            currentV = RandomBetween(playerCar.V - 5, playerCar.V);
        }
        else
        {
            currentV = RandomBetween(playerCar.V, playerCar.V + 5);
        }

        if (info.Type == TrafficTypes.WithHole)
        {
            currentV = playerCar.V;
        }
        else if (info.Type == TrafficTypes.Static)
        {
            currentV = 0;
        }
        else if (info.Type == TrafficTypes.Slow || info.Type == TrafficTypes.Fast)
        {
            currentV = GetSpeedFor(info);
        }
        else if (info.Type == TrafficTypes.Wide)
        {
            return currentV;
        }
        return currentV;
    }

    private float DestPosFilter(TrafficInfo info, CarValues trafficCar, float currentDestPos)
    {
        if (info.Type == TrafficTypes.Static)
        {
            currentDestPos = trafficPositionInLocal.z + manager.RoadLenght / 2 - trafficCar.Size.z / 2 - 2;
        }
        else if (info.Type == TrafficTypes.WithHole)
        {
            currentDestPos = turnPointInLocal.z;
        }
        return currentDestPos;
    }

    private float SignFilter(float offsetSign, TrafficInfo trafficInfo, CarValues trafficCar)
    {
        if (trafficInfo.Type == TrafficTypes.Fast)
        {
            offsetSign = 1;
        }
        else if (trafficInfo.Type == TrafficTypes.Slow)
        {
            offsetSign = -1;
        }
        else if ((trafficInfo.Type == TrafficTypes.Wide || trafficInfo.Type == TrafficTypes.WithHole) && !CheckIfCouldSpawnOnTwoSides(trafficCar, trafficInfo.Type))
        {
            offsetSign = -1;
        }
        return offsetSign;
    }

    private bool CouldSpawn(GameObject prefab)
    {
        if (prefab == null || prefab.transform == null)
        {
            return false;
        }

        if (Mathf.Abs(turnPointInLocal.z - trafficDestPosInLocal) > 20)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    protected override bool CheckIfTrafficCollide(CarValues trafficCar, float currentDestPos, float precision)
    {
        foreach (var other in manager.TrafficToAvoid)
        {
            Vector3 otherInLocal = transform.InverseTransformPoint(other.CarObject.transform.position);
            Vector3 contactPosition = new Vector3(trafficPositionInLocal.x, trafficPositionInLocal.y, otherInLocal.z);
            float mySizeOffset = (other.Size.x / 2 + trafficCar.Size.z / 2) * precision;
            float myOffset = (contactPosition.z - currentDestPos) + trafficCar.S;
            float t1 = (myOffset - mySizeOffset) / trafficCar.V;
            float t2 = (myOffset + mySizeOffset) / trafficCar.V;

            float otherSizeOffset = (other.Size.z / 2 + trafficCar.Size.x / 2) * precision;
            float otherOffset = Mathf.Abs(contactPosition.x - otherInLocal.x);
            float t3 = (otherOffset - otherSizeOffset) / other.V;
            float t4 = (otherOffset + otherSizeOffset) / other.V;

            if ((t1 > t3 && t1 < t4) || (t2 < t4 && t2 > t3)
             || (t3 > t1 && t3 < t2) || (t4 < t2 && t4 > t1))
            {
                return true;
            }
        }
        return false;
    }

}
