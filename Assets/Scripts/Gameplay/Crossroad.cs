﻿using UnityEngine;
using System.Collections;

public class Crossroad : MonoBehaviour {

    [SerializeField]
    private MeshRenderer mesh;

    private ObjectsContainer container;
    private bool returnIfIsNotVisible = false;

    void OnDisable()
    {
        returnIfIsNotVisible = false;
    }

    public void ReturnIfIsNotVisible(ObjectsContainer container)
    {
        if (!mesh.isVisible)
        {
            container.EndUsing(this.transform);
        }
        else
        {
            this.transform.parent = container.transform;
            this.container = container;
            returnIfIsNotVisible = true;
        }
    }

    void Update()
    {
        if (returnIfIsNotVisible)
        {
            if (!mesh.isVisible)
            {
                container.EndUsing(this.transform);
            }
        }
    }

}
