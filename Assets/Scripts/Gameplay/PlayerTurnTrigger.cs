﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerTurnTrigger : MonoBehaviour
{
    [SerializeField]
    private Type type;
    [SerializeField]
    private DailyGiftAnimation dailyGiftAnimation;
    [SerializeField]
    private Transform turnPoint;


    private PlayerController player;

    void Start()
    {
        player = PrefabReferences.Instance.Player.GetComponent<PlayerController>();
    }

    void OnEnable()
    {
        GameplayManager.OnSessionStarted += OnSessionStarted;
    }

    void OnDisable()
    {
        GameplayManager.OnSessionStarted -= OnSessionStarted;
    }

    private void OnSessionStarted()
    {
        PrefabReferences.Instance.BankRobberyPopup.SetActive(false);
        SetTriggerPosition();
    }

    private void SetTriggerPosition()
    {
        switch (type)
        {
            case Type.Start:
                float newZposition = turnPoint.transform.position.z - GetPlayerTurningOffset();
                transform.position = transform.position.ChangeZ(newZposition);
                break;
            case Type.End:
                float newXposition = turnPoint.transform.position.x - GetPlayerTurningOffset();
                transform.position = transform.position.ChangeX(newXposition);
                break;
            default:
                break;
        }
    }


    private float GetPlayerTurningOffset()
    {
        return player.GetComponent<PlayerSpeedManager>().Speed * PlayerController.PLAYER_TURNING_OFFSET_PER_ONE_SPEED - (GetComponent<BoxCollider>().size.z / 2 + player.GetComponent<BoxCollider>().size.z / 2);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player" && DailyGiftManager.IsDailyGiftActive)
        {
            player.OnTurnButtonClicked(true);
            switch (type)
            {
                case Type.Start:
                    BankRobberyPopup bankPopup = PrefabReferences.Instance.BankRobberyPopup.GetComponentInChildren<BankRobberyPopup>(true);
                    if (Tutorial.IsTutorial)
                    {
                        bankPopup.SetTutorialMode(Continue);
                        CommonMethods.SetTimeScaleSmoothed(0);
                    }
                    else
                    {
                        bankPopup.SetNormalMode();
                    }

                    break;
                case Type.End:

                    break;
                default:
                    break;
            }
        }
    }



    public void Continue()
    {
        PrefabReferences.Instance.BankRobberyPopup.GetComponentInChildren<Button>(true).onClick.RemoveListener(Continue);
        CommonMethods.SetTimeScaleSmoothed(1);
        PrefabReferences.Instance.BankRobberyPopup.SetActive(false);
    }

    enum Type
    {
        Start,
        End
    }
}
