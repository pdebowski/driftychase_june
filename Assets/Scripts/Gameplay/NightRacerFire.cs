﻿using UnityEngine;
using System.Collections;

public class NightRacerFire : MonoBehaviour
{

    private Vector3 lastPosition;

    void Update()
    {
        Vector3 movedOffset = transform.position - lastPosition;
        movedOffset = transform.parent.InverseTransformVector(movedOffset);
        movedOffset.x = Mathf.Abs(movedOffset.x);
        this.transform.localScale = this.transform.localScale.ChangeZ(Mathf.Lerp(this.transform.localScale.z, Mathf.Lerp(0.5f, 1, movedOffset.x), 2 * Time.deltaTime));
        lastPosition = transform.position;
    }
}
