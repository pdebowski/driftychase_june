﻿using UnityEngine;
using System.Collections;

public class DifficultyLevel : MonoBehaviour
{
    [SerializeField]
    private CrossroadTrafficManager trafficManager;
    [SerializeField]
    private float maxScoreSafeDistance;
    [Header("SafeDistance")]
    [SerializeField]
    private AnimationCurve safeDistanceCurve;
    [SerializeField]
    private float minSafeDistance;
    [SerializeField]
    private float maxSafeDistance;
    [Header("TrafficCount")]
    [SerializeField]
    private AnimationCurve trafficCountCurve;
    [SerializeField]
    private float minMaxTrafficCount;
    [SerializeField]
    private float maxMaxTrafficCount;
    [SerializeField]
    private float minMinTrafficCount;
    [SerializeField]
    private float maxMinTrafficCount;

    void OnEnable()
    {
        PlayerController.OnTurn += Manage;
        GameplayManager.OnSessionStarted += Restart;
    }

    void OnDisable()
    {
        PlayerController.OnTurn -= Manage;
        GameplayManager.OnSessionStarted -= Restart;
    }

    private void Restart()
    {
        EvaluateDifficulty(0);
    }

    private void Manage(TurnDirection turnDirection)
    {
        EvaluateDifficulty(ScoreCounter.Instance.GetScore() / maxScoreSafeDistance);
    }

    private void EvaluateDifficulty(float value)
    {
        float safeDistance = Mathf.Abs(safeDistanceCurve.Evaluate(value) - 1) * (maxSafeDistance - minSafeDistance) + minSafeDistance;
        float trafficCountCurveValue = Mathf.Abs(trafficCountCurve.Evaluate(value));
        int maxTrafficCount = (int)Mathf.Round(trafficCountCurveValue * (maxMaxTrafficCount - minMaxTrafficCount) + minMaxTrafficCount);
        int minTrafficCount = (int)Mathf.Round(trafficCountCurveValue * (maxMinTrafficCount - minMinTrafficCount) + minMinTrafficCount);

        trafficManager.SetDifficultyLevel(safeDistance, maxTrafficCount, minTrafficCount);
    }
}
