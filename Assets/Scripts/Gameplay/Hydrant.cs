﻿using UnityEngine;
using System.Collections;

public class Hydrant : MonoBehaviour
{
    [SerializeField]
    private GameObject hydrantHolder;
    [SerializeField]
    private GameObject damagedObject;

    private Quaternion startLocalRotation;
    private Vector3 startLocalPosition;
    private bool damaged = false;

    void Awake()
    {
        if(Random.Range(0f,1f) > 0.3f)
        {
            Destroy(hydrantHolder);
        }

        startLocalPosition = transform.localPosition;
        startLocalRotation = transform.localRotation;
    }

    void OnDisable()
    {
        Restart();
    }

    void Restart()
    {
        damagedObject.SetActive(false);
        GetComponent<Rigidbody>().useGravity = false;

        transform.localRotation = startLocalRotation;
        transform.localPosition = startLocalPosition;
        damaged = false;
    }

    void OnCollisionEnter(Collision collision)
    {
        if (!damaged)
        {
            GetComponent<Rigidbody>().useGravity = true;
            damagedObject.SetActive(true);
            damaged = true;
        }

    }


}
