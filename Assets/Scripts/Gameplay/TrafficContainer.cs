﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class TrafficContainer : MonoBehaviour
{
    [SerializeField]
    private List<AvailableTraffic> availableTrafficInWorlds;
    [SerializeField]
    private int prefabInstancesNumber = 5;
    [Header("Debug")]
    [SerializeField]
    private bool isTestingMode = false;
    [SerializeField]
    private List<GameObject> testingTraffic;
    [SerializeField]
    private WorldOrder worldOrder;
    [SerializeField]
    private GameObject truckPrefab;

    public List<GameObject> AvailableNormalTraffic { get; set; }
    public List<GameObject> AvailableSpecialTraffic { get; set; }

    public List<List<Transform>> instantiatedTraffic;
    public List<List<Transform>> trafficInUse;

    void Awake()
    {
        WorldDependent.CheckForDuplicates(availableTrafficInWorlds, this.GetType().Name);
        Fill();
    }

    void OnEnable()
    {
        PlayerSpeedManager.OnWorldChange += RefreshTrafficList;
    }

    void OnDisable()
    {
        PlayerSpeedManager.OnWorldChange -= RefreshTrafficList;
    }

    public void RefreshTrafficList()
    {
        if (!isTestingMode)
        {
            WorldType levelWorldType = WorldOrder.CurrentWorld;
            AvailableTraffic trafficToThisStage = availableTrafficInWorlds.Find(x => x.worldType == levelWorldType);
            AvailableNormalTraffic = trafficToThisStage.availableNormalTraffic;
            AvailableSpecialTraffic = trafficToThisStage.availableSpecialTraffic;
        }
        else
        {
            AvailableNormalTraffic = testingTraffic;
            AvailableSpecialTraffic = testingTraffic;
        }

    }

    private void Fill()
    {
        instantiatedTraffic = new List<List<Transform>>();
        trafficInUse = new List<List<Transform>>();

        foreach (var item in availableTrafficInWorlds)
        {
            Fill(item.availableNormalTraffic);
            Fill(item.availableSpecialTraffic);
        }

        if (isTestingMode)
        {
            Fill(testingTraffic);
        }

    }

    private void Fill(List<GameObject> prefabList)
    {
        for (int i = 0; i < prefabList.Count; i++)
        {
            if (GetInstantiatedListWith(prefabList[i].transform) != null)
            {
                continue;
            }

            instantiatedTraffic.Add(new List<Transform>());
            trafficInUse.Add(new List<Transform>());
            for (int j = 0; j < prefabInstancesNumber; j++)
            {
                AddNewTraffic(instantiatedTraffic[instantiatedTraffic.Count - 1], prefabList[i]);
            }
        }
    }

    private List<Transform> GetInstantiatedListWith(Transform prefab)
    {
        return instantiatedTraffic.Find(x => x.Find(y => y.name.Contains(prefab.name)));
    }

    private Transform AddNewTraffic(List<Transform> listToAddNewObject, GameObject prefab)
    {
        Transform newTraffic = ((GameObject)Instantiate(prefab)).GetComponent<Transform>();
        listToAddNewObject.Add(newTraffic);
        newTraffic.SetParent(this.transform);
        newTraffic.gameObject.SetActive(false);
        return newTraffic;
    }

    public void Restart()
    {
        for (int i = 0; i < trafficInUse.Count; i++)
        {
            for (int j = 0; j < trafficInUse[i].Count; j++)
            {
                instantiatedTraffic[i].Add(trafficInUse[i][j]);
                DeativateTraffic(trafficInUse[i][j]);
            }
            trafficInUse[i].Clear();
        }
    }

    public GameObject GetTraffic(GameObject prefab)
    {
        List<Transform> listWithTraffic = GetInstantiatedListWith(prefab.transform);
        if (listWithTraffic.Count == 1)
        {
            AddNewTraffic(listWithTraffic, prefab);
        }

        Transform traffic = listWithTraffic[0];
        listWithTraffic.Remove(traffic);

        trafficInUse[instantiatedTraffic.IndexOf(listWithTraffic)].Add(traffic);

        return traffic.gameObject;
    }

    public void Free(Transform trafficCar)
    {
        for (int i = 0; i < trafficInUse.Count; i++)
        {
            if (trafficInUse[i].Contains(trafficCar))
            {
                trafficInUse[i].Remove(trafficCar);
                instantiatedTraffic[i].Add(trafficCar);
                DeativateTraffic(trafficCar);
            }
        }
    }

    private void DeativateTraffic(Transform traffic)
    {
        traffic.SetParent(this.transform);
        traffic.gameObject.SetActive(false);
    }

    public int GetWorldDependentAmount()
    {
        return availableTrafficInWorlds.Count;
    }

    public void SetTruckAsSpecial()
    {
        AvailableSpecialTraffic = new List<GameObject>();
        AvailableSpecialTraffic.Add(truckPrefab);

    }

    [System.Serializable]
    private class AvailableTraffic : WorldDependent
    {
        public List<GameObject> availableNormalTraffic;
        public List<GameObject> availableSpecialTraffic;
    }
}
