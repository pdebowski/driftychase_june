using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public abstract class RoadTrafficSpawner : MonoBehaviour, IComparable<RoadTrafficSpawner>
{
    public bool Blocked { get; set; }

    protected List<CarValues> trafficCars;
    protected RoadTrafficSpawner[] roads;
    protected CrossroadTrafficManager manager;
    protected Transform player;
    protected Vector3 trafficPositionInLocal;
    protected Vector3 playerPositionInLocal;
    protected Vector3 turnPointInLocal;
    protected Vector3 turnPointInGlobal;
    protected CarValues playerCar;
    protected bool initialized;
    protected bool isTutorialCar;
    protected float reversedSpawnPointSign;
    protected float additionalOffset;

    protected abstract CarValues SpawnTraffic(GameObject[] availableTraffic, PlayerController player);

    public List<CarValues> Spawn(PlayerController player, bool specialTraffic)
    {
        if (!initialized)
        {
            Debug.LogWarning(gameObject.name + " is not initialized! Use Init()");
            return null;
        }
        if (Blocked)
        {
            return null;
        }
        playerCar.SetCarSize(player.OuterCollider.size);

        List<GameObject> carToSpawn = specialTraffic == true ? manager.SpecialCars : manager.NormalCars;
        int trafficCountToSpawn = specialTraffic == true ? 1 : GetNormalTrafficCount();
        for (int i = 0; i < trafficCountToSpawn; i++)
        {
            CarValues traffic = SpawnTraffic(carToSpawn.ToArray(), player);
            if (traffic != null)
            {
                AddTrafficToList(traffic);
            }
        }
        return trafficCars;
    }



    public void Init(Vector3 playerPosition, Vector3 turnPoint, Transform crossroad, RoadTrafficSpawner[] roads, PlayerController player, CrossroadTrafficManager manager)
    {
        trafficCars = new List<CarValues>();
        playerPositionInLocal = transform.parent.InverseTransformPoint(playerPosition);
        turnPointInLocal = transform.parent.InverseTransformPoint(turnPoint);
        turnPointInGlobal = turnPoint;
        trafficPositionInLocal = this.transform.localPosition;
        this.player = player.transform;
        playerCar = new CarValues(player);
        this.reversedSpawnPointSign = Mathf.Approximately(transform.localRotation.eulerAngles.y, 180) ? 1 : -1;
        this.manager = manager;
        this.roads = roads;

        additionalOffset = 0;

        initialized = true;
    }

    protected GameObject CreateTrafficCar(GameObject prefab, Vector3 localPosition, float speed, CarValues carValues)
    {
        Vector3 globalPosition = transform.parent.TransformPoint(localPosition);
        globalPosition += -transform.forward * (carValues.S + additionalOffset);
        globalPosition.y = 0;
        prefab = manager.GetObjectFromPrefab(prefab);
        prefab.transform.position = globalPosition;
        float rotation = transform.rotation.eulerAngles.y;
        if (WorldOrder.CurrentWorld == WorldType.Surreal)
        {
            rotation += 180;
        }
        prefab.transform.rotation = Quaternion.Euler(new Vector3(0, rotation, 0));
        prefab.SetActive(true);
        prefab.transform.SetParent(this.transform);
        AddProperiesToTraffic(prefab, carValues);
        Rigidbody trafficRB = prefab.GetComponentInChildren<Rigidbody>(true);
        trafficRB.velocity = transform.forward * speed;

        if (!isTutorialCar)
        {
            var materialsToReplacing = WorldEffectsManager.Instance.GetMaterialsForCar(prefab);
            var renderer = prefab.GetComponent<Renderer>();
            if (prefab.GetComponent<TrafficInfo>().Type == TrafficTypes.Static)
            {

                foreach (var item in prefab.GetComponentsInChildren<Renderer>(true))
                {
                    if (item.gameObject.layer == LayerMask.NameToLayer("DynamicLighting"))
                    {
                        item.materials = materialsToReplacing;
                    }
                }
            }
            else
            {
                renderer.materials = materialsToReplacing;
            }

        }

        return prefab;
    }

    protected bool FloatEqual(float value1, float value2)
    {
        return Mathf.Abs(value1 - value2) < 0.001f;
    }

    protected void AddTrafficToList(CarValues traffic)
    {
        if (traffic != null)
        {
            trafficCars.Add(traffic);
        }
    }

    protected virtual GameObject GetRandomTrafficFrom(GameObject[] availableTraffic)
    {
        if (availableTraffic.Length == 0)
        {
            return null;
        }

        GameObject result = availableTraffic[RandomBetween(0, availableTraffic.Length)];
        return result;
    }

    protected GameObject GetSpecialTrafficOtherThen(TrafficTypes type)
    {
        GameObject prefab = null;
        TrafficInfo info = new TrafficInfo();
        int maxAttempts = 5;
        do
        {
            if (maxAttempts < 0)
            {
                return GetRandomTrafficFrom(manager.NormalCars.ToArray());
            }
            prefab = GetRandomTrafficFrom(manager.SpecialCars.ToArray());

            maxAttempts--;
            if (prefab == null)
            {
                continue;
            }

            info = prefab.GetComponent<TrafficInfo>();
        } while (info.Type == type);
        return prefab;
    }

    private void AddProperiesToTraffic(GameObject traffic, CarValues carValues)
    {
        TrafficController trafficController = traffic.GetComponentInChildren<TrafficController>(true);
        if (trafficController != null)
        {
            trafficController.CarValues = carValues;
        }
    }

    private bool CompareEqualVectorsWithIntValues(Vector3 a, Vector3 b)
    {
        return a.WithIntValues() == b.WithIntValues();
    }

    protected bool CouldSpawnWideCar()
    {
        if (roads.Length <= 1)
        {
            return false;
        }

        int trafficIndex = System.Array.IndexOf(roads, this);
        int secondLaneIndex = 0;
        bool result = true;
        if (trafficIndex == 0)
        {
            secondLaneIndex = 1;
            result &= CompareEqualVectorsWithIntValues(this.transform.forward, roads[secondLaneIndex].transform.forward);
        }
        else if (trafficIndex == roads.Length - 1)
        {
            secondLaneIndex = trafficIndex - 1;
            result &= CompareEqualVectorsWithIntValues(this.transform.forward, roads[secondLaneIndex].transform.forward);
        }
        else
        {
            secondLaneIndex = trafficIndex + GetRandomHit();
            result &= CompareEqualVectorsWithIntValues(this.transform.forward, roads[secondLaneIndex].transform.forward);
        }

        return result;
    }

    protected void AdjustSpawnPointToWideCar()
    {

        int trafficIndex = System.Array.IndexOf(roads, this);
        int secondLaneIndex = trafficIndex;
        if (trafficIndex == 0)
        {
            secondLaneIndex = 1;
        }
        else if (trafficIndex == roads.Length - 1)
        {
            secondLaneIndex = trafficIndex - 1;
        }
        else
        {
            int randomHit = GetRandomHit();
            secondLaneIndex = trafficIndex + randomHit;
            if (!CompareEqualVectorsWithIntValues(this.transform.forward, roads[secondLaneIndex].transform.forward))
            {
                randomHit = -randomHit;
                secondLaneIndex = trafficIndex + randomHit;
            }
        }
        trafficPositionInLocal = (this.transform.localPosition + roads[secondLaneIndex].transform.localPosition) / 2;
        roads[secondLaneIndex].Blocked = true;
    }

    protected GameObject WideTrafficFilter(GameObject prefab)
    {
        TrafficInfo trafficInfo = prefab.GetComponent<TrafficInfo>();
        if (trafficInfo.Type == TrafficTypes.Wide)
        {
            if (CouldSpawnWideCar())
            {
                AdjustSpawnPointToWideCar();
            }
            else
            {
                prefab = GetSpecialTrafficOtherThen(TrafficTypes.Wide);
                trafficInfo = prefab.GetComponent<TrafficInfo>();
            }
        }
        return prefab;
    }

    protected float GetSpeedFor(TrafficInfo info)
    {
        return RandomBetween(info.MinSpeed, info.MaxSpeed);
    }

    protected CarValues GetTrafficValuesFromPrefab(GameObject trafficCarPrefab)
    {
        CarValues trafficCar = new CarValues();
        trafficCar.SetCarSize(CommonMethods.Vector3Multiply(trafficCarPrefab.GetComponent<BoxCollider>().size, trafficCarPrefab.transform.localScale));
        return trafficCar;
    }

    protected float GetOffsetBeetwenTraffic(float sign)
    {
        return sign * RandomBetween(manager.MinDistanceBetweenTraffic, manager.MaxDistanceBetweenTraffic);
    }

    protected float GetTrafficColumnSpeedOffset()
    {
        return RandomBetween(-manager.MaxTrafficColumnSpeedOffset, manager.MaxTrafficColumnSpeedOffset);
    }

    protected int GetNormalTrafficCount()
    {
        return RandomBetween(manager.MinNormalTrafficCount, manager.MaxNormalTrafficCount);
    }

    protected int GetRandomHit()
    {
        float hit = RandomBetween(0.0f, 1.0f);
        return hit > 0.5f ? 1 : -1;
    }

    protected float GetSafeDistance(float sign)
    {
        if (isTutorialCar)
        {
            return sign * 0;
        }
        return sign * manager.SafeDistanceFromPlayer;
    }

    protected float GetSafeDistanceOffset(float sign)
    {
        return sign * RandomBetween(0, manager.MaxSafeDistanceOffset);
    }

    protected float GetTrafficSizeOffset(float sign, CarValues car1, CarValues car2 = null)
    {
        float secondCarZ = 0;
        if (car2 != null)
        {
            secondCarZ = car2.Size.z / 2;
        }

        return GetSumWithSign(sign, car1.Size.z / 2, secondCarZ);
    }

    protected float GetSumWithSign(float sign, float value1, float value2)
    {
        return sign * (value1 + value2);
    }


    public int CompareTo(RoadTrafficSpawner other)
    {
        return (int)(this.transform.localPosition.x - other.transform.localPosition.x);
    }

    protected float RandomBetween(float val1, float val2)
    {
        return GameplayRandom.RandomBetween(val1, val2);
    }

    protected int RandomBetween(int val1, int val2)
    {
        return GameplayRandom.RandomBetween(val1, val2);
    }

    [System.Serializable]
    public class CarValues
    {
        public float V { get; private set; }
        public float S { get; private set; }
        public float T { get; private set; }
        public bool CouldSlowingDown { get; set; }
        public Vector3 Size { get; private set; }
        public float OffsetSign { get; private set; }
        public float DestinationPosition { get; private set; }
        public GameObject CarObject { get; private set; }

        public CarValues(PlayerController car)
        {
            SetCarSize(car.OuterCollider.size);
            CouldSlowingDown = false;
        }

        public CarValues(float speed, float s)
        {
            SetValues(speed, s);
            CouldSlowingDown = false;
        }

        public CarValues()
        {
            CouldSlowingDown = false;
        }

        public void SetValues(float speed, float s)
        {
            V = speed;
            S = s;
            T = S / V;
        }

        public void SetValues(PlayerController car, float s, float offsetSign)
        {
            PlayerSpeedManager playerSpeed = car.GetComponent<PlayerSpeedManager>();
            SetValues(playerSpeed.GetSpeedAtScore(ScoreCounter.Instance.CurrentScore + (offsetSign == -1 ? 1 : 2)), s);
        }

        public void SetCarSize(Vector3 size)
        {
            Size = size;
        }

        public void SetOffsetSign(float offsetSign)
        {
            OffsetSign = offsetSign;
        }

        public void SetDestPos(float destPos)
        {
            DestinationPosition = destPos;
        }

        public void SetCarObject(GameObject car)
        {
            CarObject = car;
        }
    }
}
