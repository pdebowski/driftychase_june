﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TurnPointManager : MonoBehaviour
{

    public Vector3 LastTurnPoint { get; private set; }

    [SerializeField]
    private GameObject turnPointIndicator;

    private PlayerController player;
    private List<GameObject> turnPoints;
    private Transform playerTransformOnTurn;

    public void Init()
    {
        player = PrefabReferences.Instance.Player.GetComponent<PlayerController>();
        playerTransformOnTurn = (new GameObject()).transform;
        playerTransformOnTurn.name = "PlayerTransformOnTurn";
        turnPoints = new List<GameObject>();
    }

    public void Restart()
    {
        turnPoints.Clear();
    }

    public void SetNewTurnPoint(BehindTrafficSpawner[] roadsWithBehindTraffic,
                                    OppositeTrafficSpawner[] roadsWithOppositeTraffic,
                                    bool isFirstCrossroad,
                                    Transform crossroadHolder)
    {
        Transform newTurnPointLineObject = GetNewTurnPointLine(roadsWithBehindTraffic, roadsWithOppositeTraffic);
        Vector3 newTurnPointLine = newTurnPointLineObject.position;
        float playerWidth = player.GetComponent<BoxCollider>().size.x;
        Transform crossroadObject = crossroadHolder.GetComponentInChildren<Crossroad>(true).transform;
        Vector3 newTurnPointPosition = Vector3.zero;
        Vector3 rotation = Vector3.zero;

        if (transform.forward == Vector3.forward || transform.forward == Vector3.back)
        {
            float offset = crossroadObject.lossyScale.y / 2 - playerWidth / 2;
            newTurnPointPosition.x = newTurnPointLine.x;
            newTurnPointPosition.z = playerTransformOnTurn.transform.position.z;
            if (!isFirstCrossroad)
            {
                newTurnPointPosition.z += PlayerDriftOffset(player) * playerTransformOnTurn.transform.forward.z;
                newTurnPointPosition.z = Mathf.Clamp(newTurnPointPosition.z, crossroadObject.position.z - offset, crossroadObject.position.z + offset);
            }
        }
        else
        {
            float offset = crossroadObject.lossyScale.x / 2 - playerWidth / 2;
            newTurnPointPosition.x = playerTransformOnTurn.transform.position.x;
            newTurnPointPosition.z = newTurnPointLine.z;
            if (!isFirstCrossroad)
            {
                newTurnPointPosition.x += PlayerDriftOffset(player) * playerTransformOnTurn.transform.forward.x;
                newTurnPointPosition.x = Mathf.Clamp(newTurnPointPosition.x, crossroadObject.position.x - offset, crossroadObject.position.x + offset);

            }
            rotation = new Vector3(0, -90, 0);
        }

        GameObject newTurnPointIndicator = Instantiate(turnPointIndicator, newTurnPointPosition, Quaternion.Euler(rotation)) as GameObject;
        newTurnPointIndicator.transform.SetParent(crossroadHolder);

        turnPoints.Add(newTurnPointIndicator);
        LastTurnPoint = newTurnPointIndicator.transform.position;
    }

    public void SetPlayerPositionOnTurn(Transform player)
    {
        playerTransformOnTurn.position = player.position;
        playerTransformOnTurn.rotation = player.rotation;
    }

    private float PlayerDriftOffset(PlayerController player)
    {
        return PlayerController.PLAYER_TURNING_OFFSET_PER_ONE_SPEED * player.Speed;
    }

    private Transform GetNewTurnPointLine(BehindTrafficSpawner[] roadsWithBehindTraffic,
                                         OppositeTrafficSpawner[] roadsWithOppositeTraffic)
    {
        int randomIndex = Random.Range(0, roadsWithBehindTraffic.Length + roadsWithOppositeTraffic.Length);
        if (randomIndex >= roadsWithBehindTraffic.Length)
        {
            return roadsWithOppositeTraffic[randomIndex - roadsWithBehindTraffic.Length].transform;
        }
        else
        {
            return roadsWithBehindTraffic[randomIndex].transform;
        }

    }
}