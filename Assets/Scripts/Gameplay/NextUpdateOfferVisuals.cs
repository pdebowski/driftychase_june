﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class NextUpdateOfferVisuals : MonoBehaviour {

    [SerializeField]
    private Text headlineText;
    [SerializeField]
    private Text descriptionText;

    void Start()
    {
        headlineText.text = "UPDATE " + GTMParameters.NextUpdateVersionNumber;
        descriptionText.text = GTMParameters.NextUpdateDescription;
    }

    
}
