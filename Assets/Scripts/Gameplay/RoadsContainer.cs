﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RoadsContainer : MonoBehaviour
{
    public List<RoadInfo> AvailableRoads { get; set; }
    public List<RoadInfo> AvailableConnectors { get; set; }

    [SerializeField]
    private int prefabInstancesNumber = 5;
    [SerializeField]
    private List<RoadInfo> availableRoads;
    [SerializeField]
    private List<RoadInfo> availableConnectors;

    public List<List<Transform>> instantiatedRoads;
    public List<List<Transform>> roadsInUse;

    void Awake()
    {
        AvailableRoads = availableRoads;
        AvailableConnectors = availableConnectors;
        Fill();
    }

    void OnEnable()
    {
        EnglishMode.OnEnglishModeChange += OnEnglisModeChanged;
    }

    void OnDisable()
    {
        EnglishMode.OnEnglishModeChange -= OnEnglisModeChanged;
    }

    private void Fill()
    {
        instantiatedRoads = new List<List<Transform>>();
        roadsInUse = new List<List<Transform>>();

        for (int i = 0; i < AvailableRoads.Count; i++)
        {
            instantiatedRoads.Add(new List<Transform>());
            roadsInUse.Add(new List<Transform>());
            for (int j = 0; j < prefabInstancesNumber; j++)
            {
                AddNewRoad(instantiatedRoads[i], AvailableRoads[i].prefab);
            }
        }

        for (int i = 0; i < availableConnectors.Count; i++)
        {
            instantiatedRoads.Add(new List<Transform>());
            roadsInUse.Add(new List<Transform>());
            AddNewRoad(instantiatedRoads[AvailableRoads.Count + i], availableConnectors[i].prefab);
        }

    }

    private Transform AddNewRoad(List<Transform> listToAddNewObject, GameObject prefab)
    {
        Transform newRoad = Instantiate(prefab).GetComponent<Transform>();
        listToAddNewObject.Add(newRoad);
        newRoad.SetParent(this.transform);
        newRoad.gameObject.SetActive(false);

        if (PlayerPrefsAdapter.EnglishMode)
        {
            newRoad.transform.localScale = new Vector3(-1, 1, 1);
        }

        return newRoad;
    }

    public void Restart()
    {
        for (int i = 0; i < roadsInUse.Count; i++)
        {
            for (int j = 0; j < roadsInUse[i].Count; j++)
            {
                instantiatedRoads[i].Add(roadsInUse[i][j]);
                DeativateRoad(roadsInUse[i][j]);
            }
            roadsInUse[i].Clear();
        }
    }

    public GameObject GetRoadObject(RoadInfo prefab)
    {
        List<RoadInfo> roadsList = AvailableRoads;
        int index = AvailableRoads.IndexOf(prefab);

        if (index == -1)
        {
            index = AvailableRoads.Count + AvailableConnectors.IndexOf(prefab);
            roadsList = AvailableConnectors;
        }

        if (instantiatedRoads[index].Count == 0)
        {
            AddNewRoad(instantiatedRoads[index], roadsList[index % availableRoads.Count].prefab);
        }
        Transform road = instantiatedRoads[index][0];
        instantiatedRoads[index].Remove(road);
        roadsInUse[index].Add(road);

        int xScale = PlayerPrefsAdapter.EnglishMode == false ? 1 : -1;
        road.transform.localScale = new Vector3(xScale, 1, 1);

        return road.gameObject;
    }

    public void Return(Transform road)
    {
        for (int i = 0; i < roadsInUse.Count; i++)
        {
            if (roadsInUse[i].Contains(road))
            {
                roadsInUse[i].Remove(road);
                instantiatedRoads[i].Add(road);
                DeativateRoad(road);
            }
        }
    }

    private void DeativateRoad(Transform road)
    {
        road.GetComponentInChildren<CoinSpawner>().CleanCoins();
        road.SetParent(this.transform);
        road.gameObject.SetActive(false);
    }

    private void OnEnglisModeChanged(bool englishMode)
    {
        int xScale = englishMode == false ? 1 : -1;

        foreach (var roadType in instantiatedRoads)
        {
            foreach (var item in roadType)
            {
                item.transform.localScale = new Vector3(xScale, 1, 1);
            }
        }
    }
}
