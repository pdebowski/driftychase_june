﻿using UnityEngine;
using System.Collections;

public class Arrow : MonoBehaviour
{

    public Transform Target
    {
        get { return target; }
        set { target = value; }
    }

    [SerializeField]
    private GameObject arrowObject;
    [SerializeField]
    private Vector3 XZRotation;
    [SerializeField]
    private Transform target;
    [SerializeField]
    private Transform from;

    void Update()
    {
        if (target != null && target.gameObject.activeInHierarchy)
        {
            arrowObject.SetActive(true);
            float yRotation = Quaternion.LookRotation(target.position - from.position).eulerAngles.y;
            transform.rotation = Quaternion.Euler(new Vector3(XZRotation.x, yRotation, XZRotation.z));
        }
        else
        {
            arrowObject.SetActive(false);
        }
    }

}
