﻿using UnityEngine;
using System.Collections;

public class TransformRestartOnEnable : MonoBehaviour
{

    private SavedData savedData;

    void Awake()
    {
        savedData = new SavedData();
        savedData.GetDataFromTransform(this.transform);
    }

    void OnEnable()
    {
        transform.localPosition = savedData.localPosition;
        transform.localRotation = savedData.localRotation;
        transform.localScale = savedData.localScale;
    }

    private struct SavedData
    {
        public Vector3 localPosition;
        public Quaternion localRotation;
        public Vector3 localScale;

        public void GetDataFromTransform(Transform transform)
        {
            localPosition = transform.localPosition;
            localRotation = transform.localRotation;
            localScale = transform.localScale;
        }
    }
}
