﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerSpeedManager : MonoBehaviour
{

    public static int CurrentWorldLevel { get; private set; }
    public static int CurrentSpeedLevel { get; private set; }

    public int StartScore { get; private set; }
    public float Speed { get; private set; }

    public static System.Action OnWorldChange = delegate { };
    public static System.Action<int> OnSpeedIncrease = delegate { };

    [SerializeField]
    private bool debugOverride;
    [SerializeField]
    private WorldOrder worldsOrder;
    [SerializeField]
    private float minPlayerSpeed = 25f;
    [SerializeField]
    private float maxPlayerSpeed = 40f;
    [SerializeField]
    private int speedUpsAmount = 15;
    [SerializeField]
    private List<int> worldsLenght;
    [SerializeField]
    private List<int> worldsSpeeds;
    [SerializeField]
    private bool separateSpeedFromWorld = false;
    [SerializeField]
    private float continueSpeedDecreaseRatio = 0;
    [SerializeField]
    private int maxSpeedDistance = 100;
    [SerializeField]
    private int sameSpeedDistanceInterval = 10;
    [SerializeField]
    private bool fistCityPopupAvailable = false;



    private int lastCityStartScore;
    private int maxSpeedScore;
    private int startScore;
    private int continueLevel;
    private int speedIntervalsCount;
    private float carModelMinPlayerSpeed;
    private float originalMinSpeed;
    private float originalMaxSpeed;
    private float continueScoreDecrease = 0;

    void Awake()
    {
        originalMinSpeed = minPlayerSpeed;
        originalMaxSpeed = maxPlayerSpeed;
    }

    public void Init(int startLevel)
    {
        SetMaxSpeedScore();
        Restart(startLevel);
        CheckpointInstantOffer.WasCheckpointActiveInLastRun = PrefabReferences.Instance.CheckpointsManager.IsCheckpointTimeLeft();
    }

    private void SetMaxSpeedScore()
    {
        if (separateSpeedFromWorld)
        {
            maxSpeedScore = maxSpeedDistance;
            speedIntervalsCount = maxSpeedScore / sameSpeedDistanceInterval;
        }
        else
        {
            maxSpeedScore = 0;
            for (int i = 0; i < speedUpsAmount; i++)
            {
                maxSpeedScore += worldsLenght[Mathf.Clamp(i, 0, worldsLenght.Count - 1)];
            }
        }
    }

    void OnEnable()
    {
        GameplayManager.OnContinueUsed += OnContinueUsed;
        GameplayManager.OnSessionStarted += OnSessionStarted;
        UpdateRemoteSettings();
    }

    void OnDisable()
    {
        GameplayManager.OnContinueUsed += OnContinueUsed;
        GameplayManager.OnSessionStarted -= OnSessionStarted;
    }

    private void OnContinueUsed()
    {
        if (separateSpeedFromWorld)
        {
            continueLevel = CurrentSpeedLevel;
            continueScoreDecrease = (Speed - minPlayerSpeed) * continueSpeedDecreaseRatio;
            Speed = GetSpeedAtScore(ScoreCounter.Instance.CurrentScore);
        }
        else
        {
            continueLevel = CurrentWorldLevel;
            Speed = carModelMinPlayerSpeed;
        }


    }

    private void OnSessionStarted()
    {


        PlayerPrefsAdapter.NewWorldSeenLastSession = false;
    }

    public void Restart(int startLevel)
    {
        continueScoreDecrease = 0;

        StartScore = GetScoreInWorld(startLevel);
        ScoreCounter.Instance.SetStartScore(StartScore);

        CurrentWorldLevel = startLevel;
        lastCityStartScore = StartScore;

        if (separateSpeedFromWorld)
        {
            CurrentSpeedLevel = StartScore / sameSpeedDistanceInterval;
            continueLevel = 0;
        }
        else
        {
            continueLevel = CurrentWorldLevel;
        }

        carModelMinPlayerSpeed = GetSpeedAtScore(lastCityStartScore);
        Speed = carModelMinPlayerSpeed;

        CheckIfItIsUnseenWorld();
        worldsOrder.SetWorld(CurrentWorldLevel, true);
        OnWorldChange();

        if (separateSpeedFromWorld)
        {
            OnSpeedIncrease(CurrentSpeedLevel);
        }
        else
        {
            OnSpeedIncrease(CurrentWorldLevel);
        }

    }

    public void OnPlayerTurnEnd()
    {
        int currentScore = ScoreCounter.Instance.GetScore();

        if (IsSpeedIncreaseAvailable())
        {
            CurrentSpeedLevel++;
            Speed = GetSpeedAtScore(currentScore);
            if (separateSpeedFromWorld)
            {
                OnSpeedIncrease(CurrentSpeedLevel);
            }
            else
            {
                OnSpeedIncrease(CurrentWorldLevel + 1);
            }

        }

        if (IsNextCityAvailable())
        {
            lastCityStartScore = currentScore;
            CurrentWorldLevel = GetWorldIndexAtScore(lastCityStartScore);
            CheckIfItIsUnseenWorld();
            worldsOrder.SetWorld(CurrentWorldLevel, false);

            OnWorldChange();

        }
    }

    private bool IsNextCityAvailable()
    {
        int currentScore = ScoreCounter.Instance.GetScore();
        int nextCityScore = lastCityStartScore + worldsLenght[Mathf.Clamp(CurrentWorldLevel, 0, worldsLenght.Count - 1)];

        return currentScore >= nextCityScore;
    }

    private bool IsSpeedIncreaseAvailable()
    {
        int currentScore = ScoreCounter.Instance.GetScore();
        if (separateSpeedFromWorld)
        {
            return currentScore >= (CurrentSpeedLevel + 1) * sameSpeedDistanceInterval;
        }
        else
        {
            return IsNextCityAvailable();
        }
    }

    private void CheckIfItIsUnseenWorld()
    {
        if (CurrentWorldLevel > PlayerPrefsAdapter.BestCitySawIndex)
        {
            PlayerPrefsAdapter.BestCitySawIndex = CurrentWorldLevel;
            CrimsonAnalytics.PlayerPerformance.CitySeenTimeSpent.TimeSpent.LogEvent(CurrentWorldLevel, Mathf.RoundToInt((float)PlayerPrefsAdapter.TotalGameplayTime.TotalMinutes));
            PlayerPrefsAdapter.NewWorldSeenLastSession = true;
        }
    }

    public int GetWorldIndexAtScore(int score)
    {
        int index = -1;
        int sum = 0;
        while (sum <= score)
        {
            index++;
            sum += worldsLenght[Mathf.Clamp(index, 0, worldsLenght.Count - 1)];
        }
        return index;
    }

    public int GetScoreInWorld(int worldLevel)
    {
        int sum = 0;
        for (int i = 0; i < worldLevel; i++)
        {
            sum += worldsLenght[Mathf.Clamp(i, 0, worldsLenght.Count - 1)];
        }
        return sum;
    }

    public bool NextWillBeDifficultyStep()
    {
        if (separateSpeedFromWorld)
        {
            int currentScore = ScoreCounter.Instance.GetScore();
            return currentScore % sameSpeedDistanceInterval == sameSpeedDistanceInterval - 1;
        }
        else
        {
            int currentScore = ScoreCounter.Instance.GetScore();
            return (currentScore - lastCityStartScore) == (lastCityStartScore + worldsLenght[Mathf.Clamp(CurrentWorldLevel, 0, worldsLenght.Count - 1)] - 1);
        }

    }

    public float GetSpeedAtScore(int score)
    {
        int startCarlevel = PrefabReferences.Instance.CheckpointsManager.GetStartWorldIDForSelectedCar();

        if (separateSpeedFromWorld)
        {
            int startCarScore = GetScoreInWorld(startCarlevel);
            int startSpeedLevel = startCarScore / sameSpeedDistanceInterval;
            int speedLevelForScore = score / sameSpeedDistanceInterval;

            int currentLevel = speedLevelForScore;

            float speed = Mathf.Lerp(minPlayerSpeed, maxPlayerSpeed, ((float)currentLevel / speedIntervalsCount));
            speed -= continueScoreDecrease;
            speed = Mathf.Clamp(speed, minPlayerSpeed, maxPlayerSpeed);

            return speed;
        }
        else
        {
            int level = GetWorldIndexAtScore(score);
            level -= (continueLevel - startCarlevel);
            level = Mathf.Clamp(level, 0, worldsSpeeds.Count - 1);

            return worldsSpeeds[level];
        }
    }

    public int GetWorldsLenght()
    {
        int sum = 0;

        foreach (var item in worldsLenght)
        {
            sum += item;
        }

        return sum;
    }

    void UpdateRemoteSettings()
    {
        if (Debug.isDebugBuild && debugOverride == true)
        {
            return;
        }

        RemoteCityStatsDB remoteCityDB = GTMParameters.CityStatsDB;
        worldsLenght = new List<int>();
        worldsSpeeds = new List<int>();

        if (remoteCityDB != null)
        {
            int cityId = 0;
            foreach (RemoteCityStatsDB.CityStats cityStats in remoteCityDB)
            {
                if (cityId < remoteCityDB.Length)
                {
                    worldsLenght.Add(cityStats.Length);
                }
                if (cityId < remoteCityDB.Length)
                {
                    worldsSpeeds.Add(cityStats.Speed);
                }
                ++cityId;
            }
        }
        else
        {
            CrimsonError.LogError("Couldn't obtain RemoteCityDB");
        }
    }

    public void InitNormalMode()
    {
        UpdateRemoteSettings();

        minPlayerSpeed = originalMinSpeed;
        maxPlayerSpeed = originalMaxSpeed;

        Init(PrefabReferences.Instance.CheckpointsManager.GetStartWorldIDForSelectedCar());

    }

    public void InitLevel(float levelSpeed, int[] levelWorldsLenght)
    {
        worldsLenght = new List<int>();

        if (levelWorldsLenght != null && levelWorldsLenght.Length > 0)
        {
            levelWorldsLenght.CopyTo(worldsLenght);
        }
        else
        {
            Debug.LogError("levelWorldsLenght not exist or levelWorldsLenght is empty");
        }

        minPlayerSpeed = levelSpeed;
        maxPlayerSpeed = levelSpeed;

        Init(0);

    }
}
