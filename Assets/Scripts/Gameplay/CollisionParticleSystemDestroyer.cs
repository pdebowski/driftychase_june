﻿using UnityEngine;
using System.Collections;

public class CollisionParticleSystemDestroyer : MonoBehaviour {

    private const float MAX_LIFE_TIME = 3f;
    private float lifeTime = 0;
    
    private void Update()
    {
        lifeTime += Time.deltaTime;
        
        if(lifeTime >= MAX_LIFE_TIME)
        {
            Destroy(gameObject);
        }   
    }
}
