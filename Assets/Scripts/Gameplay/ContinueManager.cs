﻿using UnityEngine;
using System.Collections;

public class ContinueManager : MonoBehaviour
{
    [SerializeField]
    private int minScoreToContinue;
    [SerializeField]
    private GameSceneManager gameSceneManager;
    [SerializeField]
    private int continueCount;
    [SerializeField]
    private OtherGames otherGames;
    [Header("Debug")]
    [SerializeField]
    private bool alwaysContinue;

    [Header("Objects To Timeline")]
    [SerializeField]
    private PlayerController playerController;
    [SerializeField]
    private PlayerCamera playerCamera;
    [SerializeField]
    private CityCreator cityCreator;
    [SerializeField]
    private CountingDown countingDown;

    private int triesRemaining;

    private bool wasContinueShown;

    void Start()
    {
        wasContinueShown = false;
    }

    void OnEnable()
    {
        GameplayManager.OnPlayerCrash += OnPlayerCrashAction;
        GameplayManager.OnSessionStarted += Restart;
    }

    void OnDisable()
    {
        GameplayManager.OnPlayerCrash -= OnPlayerCrashAction;
        GameplayManager.OnSessionStarted -= Restart;
    }

    private void Restart()
    {
        triesRemaining = continueCount;
        wasContinueShown = false;
    }

    private void OnPlayerCrashAction()
    {
        if (GameManager.Instance.CurrentState != GameManager.GameState.Playing)
        {
            return;
        }

        triesRemaining--;


        if ((!wasContinueShown || alwaysContinue) && playerCamera.Working && IsLevelModeContinueAvailable())
        {
            gameSceneManager.ShowContinueScreen();
            wasContinueShown = true;
#if !UNITY_TVOS
            CashPerMinuteAnalyticsReporter.Instance.AddCashPerMinuteData();
#endif
        }
        else
        {
            gameSceneManager.ShowResultScreen();
        }
    }

    private bool IsLevelModeContinueAvailable()
    {
        if (GameplayManager.CurrentGameMode == null)
        {
            return true;
        }
        else
        {
            return GameplayManager.CurrentGameMode.IsContinueAvailable();
        }
    }

    public void OnContinue()
    {
        Vector3 playerPositionToContinue = cityCreator.GetPlayerPositionToContinue();
        Quaternion playerRotationToContinue = cityCreator.GetPlayerRotationToContinue();
        playerController.SetNextTargetRotation(playerRotationToContinue);
        GameplayManager.OnContinueUsed();
        playerController.transform.position = playerPositionToContinue;
        playerCamera.FocusTarget();
    }

    public void OnFadeEnd()
    {
        StartCoroutine(CountingDown());
    }

    private IEnumerator CountingDown()
    {
        countingDown.Run();

        yield return new WaitForSeconds(3 * countingDown.GetOneNumberTimeSpan());

        playerController.OnContinueCountingDown();
    }


}
