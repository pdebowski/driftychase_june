﻿using UnityEngine;
using System.Collections;

public class DangerMarker : MonoBehaviour
{


    private Color white = Color.white;
    private Color red = new Color(255 / 255f, 100 / 255f, 100 / 255f);

    private float yOffset = 1f;
    private MeshRenderer arrowMeshRenderer;

    void Awake()
    {
        arrowMeshRenderer = GetComponentInChildren<MeshRenderer>(true);

        //temporary disabled for test
        this.enabled = false;
    }

    void Update()
    {
        if (!arrowMeshRenderer.isVisible)
        {
            return;
        }
        RaycastHit hit;
        Vector3 rayCastDirection = -transform.right;
        if (Physics.Raycast(transform.position + Vector3.up * yOffset, rayCastDirection, out hit, Mathf.Infinity, LayerMask.GetMask("Traffic")))
        {
            Vector3 nextCarDirection = PrefabReferences.Instance.Player.GetComponent<PlayerController>().GetNextTargetDirection();

            if (Vector3.Angle(hit.collider.transform.forward, -rayCastDirection) < 5f && Vector3.Angle(nextCarDirection, -rayCastDirection) > 5f)
            {
                SetVisible(true);
            }
            else
            {
                SetVisible(false);
            }
        }
        else
        {
            SetVisible(false);
        }
    }

    void SetVisible(bool value)
    {
        arrowMeshRenderer.material.color = value ? red : white;
    }
}
