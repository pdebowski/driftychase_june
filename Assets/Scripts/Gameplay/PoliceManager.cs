using UnityEngine;
using System.Collections;

public class PoliceManager : MonoBehaviour
{
    [SerializeField]
    private PoliceCameraLights policeCameraLights;
    [SerializeField]
    private Vector3 startPolicePosition;

    private GameObject currentPoliceObject;

    public void SwitchPoliceTo(GameObject prefab)
    {
        if (currentPoliceObject != null)
        {
            Destroy(currentPoliceObject);
            currentPoliceObject = null;
        }

        if (prefab != null)
        {
            currentPoliceObject = Instantiate(prefab, startPolicePosition, Quaternion.identity) as GameObject;
            currentPoliceObject.transform.SetParent(this.transform);

            PoliceController policeController = currentPoliceObject.GetComponent<PoliceController>();
            policeController.Init(policeCameraLights);

            currentPoliceObject.SetActive(true);

        }


    }
}
