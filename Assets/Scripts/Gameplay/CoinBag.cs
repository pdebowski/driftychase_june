﻿using UnityEngine;
using System.Collections;

public class CoinBag : MonoBehaviour
{
    [HideInInspector]
    public bool canMove = false;

    private Transform player;
    private Rigidbody rigidBody;
    private CoinSpawner coinSpawner;
    private int coinValue = 0;
    private GameplayInterstitialAd adSpawner;

    void Start()
    {
        player = PrefabReferences.Instance.Player;
        rigidBody = GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
        if (canMove)
        {
            float distance = Vector3.Distance(player.position, transform.position);
            float speed = Mathf.InverseLerp(50, 0, distance);
            speed *= speed;

            Vector3 localPos = player.InverseTransformPoint(transform.position);

            if (localPos.z < 0)
            {
                speed = 1;
            }

            Vector3 direction = (player.position.ChangeY(transform.position.y) - transform.position).normalized;
            rigidBody.MovePosition(transform.position + direction * speed * 50 * Time.deltaTime);
        }

        rigidBody.MoveRotation(Quaternion.Euler(transform.rotation.eulerAngles + new Vector3(0, 90 * Time.deltaTime, 0)));
    }

    void OnTriggerEnter(Collider collider)
    {
        if (collider.tag == "Player")
        {
            ScoreCounter.Instance.AddCash(coinValue);
            CashSounds.Instance.Play();
            TryReportGiftCollected();
            if (adSpawner != null)
            {
                adSpawner.ReturnCoin(this);
            }
            else
            {
                GetComponentInParent<CoinSpawner>().ReturnCoin(this);
            }

        }
    }

    public void SetAdSpawner(GameplayInterstitialAd spawner)
    {
        adSpawner = spawner;
    }

    public void SetCoinSpawner(CoinSpawner coinSpawner)
    {
        this.coinSpawner = coinSpawner;
    }

    private void TryReportGiftCollected()
    {
        if (coinSpawner != null)
        {
            coinSpawner.TryReportGiftCollected();
        }
    }

    public void SetCoinValue(int value)
    {
        coinValue = value;
    }
}
