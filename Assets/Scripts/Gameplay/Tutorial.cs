﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Tutorial : MonoBehaviour
{
    private const float TIME_SCALING_DURATION = 0.3f;
    private const float MIN_TUTORIAL_VISIBLE_TIME = 0.5f;

    public static bool IsTutorial { get; private set; }


    [SerializeField]
    private GameObject[] tutorialElements;
    [SerializeField]
    private PlayerInputStartDelay playerInputStartDelay;
    [SerializeField]
    private PlayerController playerController;
    [SerializeField]
    private NotYetText notYetText;

    [Header("Tutorial Elements")]
    [SerializeField]
    private GameObject tapToTurnLeft;
    [SerializeField]
    private GameObject tapToTurnRight;
    [SerializeField]
    private GameObject goodLuck;
    [SerializeField]
    private AnimationCurve timeScaleCurve;
    [SerializeField]
    private GameObject pauseButton;
    [SerializeField]
    private GameObject turningTutorial;

    private TutorialPart lastPart = TutorialPart.RightTurn;
    private bool stopGame = false;
    private bool resumeGame = false;
    private float originalTimeScale;
    private float lastTime;
    private List<GameObject> objectsToTurnOffAfterTap = null;
    private bool turnOffPlayerInputAfterTap = false;
    private float timeElapsed = 0;

    void Awake()
    {
        PlayerPrefsAdapter.TotalRunAppTimes++;
    }

    void Start()
    {
        if (PlayerPrefsAdapter.TotalRunAppTimes > 1 || PrefabReferences.Instance.CarStatsDB.GetBestOwnedCar().startWorld != 0 || GameplayManager.CurrentGameMode != null)
        {
            EndTutorial();
        }
        else
        {
            TurnOnTurningTutorial();
        }
    }

    private void TurnOnTurningTutorial()
    {
        turningTutorial.SetActive(true);
        PlayerInput.isEnabled = false;
        IsTutorial = true;
        pauseButton.SetActive(false);
    }

    void OnEnable()
    {
        PlayerInput.OnTurnButtonClickedDisabled += OnTurnButtonClickedDisabled;
        GameplayManager.OnSessionStarted += OnSessionStarted;
    }

    void OnDisable()
    {
        PlayerInput.OnTurnButtonClickedDisabled -= OnTurnButtonClickedDisabled;
        GameplayManager.OnSessionStarted -= OnSessionStarted;
    }

    private void OnTurnButtonClickedDisabled()
    {
        if (timeElapsed >= MIN_TUTORIAL_VISIBLE_TIME)
        {
            if (objectsToTurnOffAfterTap != null)
            {
                playerController.OnTurnButtonClicked(true);
                lastTime = Time.realtimeSinceStartup;
                resumeGame = true;
                foreach (var item in objectsToTurnOffAfterTap)
                {
                    item.SetActive(false);
                }
                objectsToTurnOffAfterTap = null;
            }

            if (turnOffPlayerInputAfterTap)
            {
                PlayerInput.isEnabled = false;
            }
        }
    }

    private void OnSessionStarted()
    {
        if (lastPart == TutorialPart.FirstPartEnding || PrefabReferences.Instance.CarStatsDB.GetBestOwnedCar().startWorld != 0 || GameplayManager.CurrentGameMode != null)
        {
            EndTutorial();
        }
    }

    public void TutorialTrigger(TutorialPart trigger)
    {
        notYetText.HideText();

        switch (trigger)
        {
            case global::TutorialPart.RightTurn:
                FirstTutorial();
                stopGame = true;
                break;
            case global::TutorialPart.LeftTurn:
                SecondTutorial();
                stopGame = true;
                break;
            case global::TutorialPart.GoodLuck:
                GoodLuck();
                stopGame = true;
                break;
            case global::TutorialPart.FirstPartEnding:
                EndTutorial();
                break;
            default:
                break;
        }

        originalTimeScale = Time.timeScale;
        lastTime = Time.realtimeSinceStartup;
        lastPart = trigger;
        timeElapsed = 0;
    }

    void Update()
    {
        timeElapsed += Time.unscaledDeltaTime;

        if (stopGame)
        {
            float phase = GetPhase();
            Time.timeScale = timeScaleCurve.Evaluate(phase) * originalTimeScale;
            if (phase == 1)
            {
                stopGame = false;
            }
        }

        if (resumeGame)
        {
            float phase = GetPhase();
            Time.timeScale = timeScaleCurve.Evaluate(1 - phase) * originalTimeScale;
            if (phase == 1)
            {
                resumeGame = false;
            }
        }
    }

    private float GetPhase()
    {
        float myDeltaTime = Time.realtimeSinceStartup - lastTime;
        return Mathf.Clamp01(myDeltaTime / TIME_SCALING_DURATION);
    }


    private void FirstTutorial()
    {
        tapToTurnRight.SetActive(true);
        objectsToTurnOffAfterTap = new List<GameObject>();
        objectsToTurnOffAfterTap.Add(tapToTurnRight);
        turnOffPlayerInputAfterTap = true;
    }

    private void SecondTutorial()
    {
        tapToTurnLeft.SetActive(true);
        objectsToTurnOffAfterTap = new List<GameObject>();
        objectsToTurnOffAfterTap.Add(tapToTurnLeft);
        turnOffPlayerInputAfterTap = true;
    }

    private void GoodLuck()
    {
        IsTutorial = false;
        goodLuck.SetActive(true);
        objectsToTurnOffAfterTap = new List<GameObject>();
        objectsToTurnOffAfterTap.Add(goodLuck);
    }

    private void EndTutorial()
    {
        IsTutorial = false;
        pauseButton.SetActive(true);
        PlayerInput.isEnabled = true;
        Destroy(turningTutorial);
        foreach (var item in tutorialElements)
        {
            Destroy(item);
        }
        Destroy(this.gameObject);
    }
}

public enum TutorialPart
{
    RightTurn,
    LeftTurn,
    GoodLuck,
    FirstPartEnding
}
