using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CityBuildingsContainer : MonoBehaviour
{
    [SerializeField]
    private Transform defaultPrefab;
    [SerializeField]
    private List<WorldBuilding> customPrefabs;
    [SerializeField]
    private int containerSize;
    [SerializeField]
    private WorldOrder worldOrder;

    private List<Transform> instantiatedParts;
    private List<Transform> partsInUse;

    private Transform instantiatedBank;

    void Awake()
    {
        instantiatedParts = new List<Transform>();
        partsInUse = new List<Transform>();
        Fill();
    }

    private void Fill()
    {
        instantiatedParts = new List<Transform>();
        partsInUse = new List<Transform>();

        for (int i = 0; i < customPrefabs.Count; i++)
        {
            for (int j = 0; j < containerSize; j++)
            {
                AddNewPart(customPrefabs[i].prefab.transform);
            }
        }

    }

    private void AddNewPart()
    {
        Transform prefabToSpawn = GetPrefab();
        AddNewPart(prefabToSpawn);
    }

    private void AddNewPart(Transform prefabToSpawn)
    {
        Transform newPart = GameObject.Instantiate(prefabToSpawn);

        instantiatedParts.Add(newPart);
        newPart.gameObject.SetActive(false);
        newPart.SetParent(this.transform);
    }


    private Transform GetPrefab()
    {
        WorldBuilding currentWorldBuilding = customPrefabs.Find(x => x.world == WorldOrder.CurrentWorld);
        if (currentWorldBuilding != null)
        {
            return currentWorldBuilding.prefab.GetComponent<Transform>();
        }
        else
        {
            return defaultPrefab;
        }
    }

    public void Restart()
    {
        if (partsInUse.Count > 0)
        {
            while (partsInUse.Count > 0)
            {
                EndUse(partsInUse[0]);
            }
        }
        partsInUse.Clear();
    }

    public Transform GetNextPart()
    {
        if (instantiatedParts.Count == 0)
        {
            AddNewPart();
        }

        Transform cityPart = GetWorldPart();
        StartUse(cityPart);

        return cityPart;
    }

    private Transform GetWorldPart()
    {
        WorldBuilding currentWorldBuilding = customPrefabs.Find(x => x.world == WorldOrder.CurrentWorld);
        Transform instantiatedPrefab = null;
        if (currentWorldBuilding != null)
        {
            instantiatedPrefab = instantiatedParts.Find(x => x.name.Contains(currentWorldBuilding.prefab.name));
        }
        else
        {
            instantiatedPrefab = instantiatedParts.Find(x => x.name.Contains(defaultPrefab.name));
        }

        if (instantiatedPrefab == null)
        {
            AddNewPart();
            return GetWorldPart();
        }

        return instantiatedPrefab;
    }

    private void StartUse(Transform part)
    {
        if (part != null)
        {
            instantiatedParts.Remove(part);
            partsInUse.Add(part);
        }
    }

    public void EndUse(Transform part)
    {
        if (part != null)
        {
            partsInUse.Remove(part);
            instantiatedParts.Add(part);
            part.gameObject.SetActive(false);
        }
    }
}

[System.Serializable]
public class WorldBuilding
{
    public WorldType world;
    public GameObject prefab;
}
