﻿using UnityEngine;
using System.Collections;

public class BasculeBridgeTrigger : MonoBehaviour
{
    public BasculeBridgeEnum State { get; private set; }

    [SerializeField]
    private BasculeBridgeEnum state;

    void Awake()
    {
        State = state;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<PlayerController>() != null)
        {
            BasculeBridge parent = GetComponentInParent<BasculeBridge>();
            if (parent != null)
            {
                parent.OnPlayerEnter(state);
            }
        }
    }

}

public enum BasculeBridgeEnum
{
    FirstPartEntry,
    FirstPartExit,
    SecondPartEntry,
    SecondPartExit
}