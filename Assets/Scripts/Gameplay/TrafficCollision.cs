using UnityEngine;
using System.Collections;

public class TrafficCollision : MonoBehaviour
{

    const float COLLISION_DISTANCE = 30f;

    private Rigidbody rigidBody;
    private PlayerCollision playerCollision;
    private AudioSource audioSource;

    void Awake()
    {
        audioSource = GetComponentInChildren<AudioSource>();
        rigidBody = GetComponentInChildren<Rigidbody>(true);
        playerCollision = PrefabReferences.Instance.Player.GetComponent<PlayerCollision>();
    }


    void OnEnable()
    {
        GameplayManager.OnContinueUsed += OnContinueUsedAction;
    }

    void OnDisable()
    {
        GameplayManager.OnContinueUsed -= OnContinueUsedAction;
        Restart();
    }

    void Update()
    {
        bool shouldCollide = Vector3.Distance(playerCollision.transform.position, transform.position) < COLLISION_DISTANCE;
        string layer = playerCollision.IsDead() && shouldCollide ? "TrafficWithCollision" : "Traffic";
        gameObject.layer = (LayerMask.NameToLayer(layer));
    }

    private void OnContinueUsedAction()
    {
        PrefabReferences.Instance.TrafficContainer.Free(transform);
    }


    void OnCollisionEnter(Collision collision)
    {
        if (rigidBody != null)
        {
            rigidBody.drag = GlobalParameters.carDragAfterCrash;
            rigidBody.angularDrag = GlobalParameters.carDragAfterCrash;
            rigidBody.useGravity = true;
        }
    }

    private void Restart()
    {
        if (rigidBody != null)
        {
            rigidBody.drag = 0;
            rigidBody.angularDrag = 0;
            rigidBody.useGravity = false;
            rigidBody.angularVelocity = Vector3.zero;
            rigidBody.velocity = Vector3.zero;
        }
    }
}
