﻿using UnityEngine;
using System.Collections;
using System;

public class TrafficSpawnerPoint : MonoBehaviour, IComparable<TrafficSpawnerPoint>
{

    public int CompareTo(TrafficSpawnerPoint other)
    {
        return (int)(this.transform.localPosition.x - other.transform.localPosition.x);
    }
}
