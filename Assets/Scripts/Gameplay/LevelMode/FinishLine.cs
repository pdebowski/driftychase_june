﻿using UnityEngine;
using System.Collections;

public class FinishLine : MonoBehaviour
{
    [SerializeField]
    private ParticleSystem[] confetti;

    void OnEnable()
    {
        foreach (var item in confetti)
        {
            item.Clear();
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            LevelModeController.OnLevelComplete();
            foreach (var item in confetti)
            {
                item.Play();
            }
        }
    }


}
