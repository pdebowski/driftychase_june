﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

[System.Serializable]
public class LevelModeData
{
    public int Speed { get { return speed; } }
    public int MinCoinsCount { get { return minCoinsCount; } }
    public int MaxCoinsCount { get { return maxCoinsCount; } }
    public WorldType[] Worlds { get { return worlds; } }
    public int[] WorldsLenght { get { return worldsLenght; } }

    [SerializeField]
    private int speed;
    [SerializeField]
    private int minCoinsCount;
    [SerializeField]
    private int maxCoinsCount;
    [SerializeField]
    private WorldType[] worlds;
    [SerializeField]
    private int[] worldsLenght;

    public Vector2 GetFinishCrossroadID()
    {
        int sum = worldsLenght.Sum();
        Vector2 result = new Vector2(sum / 2, sum / 2 + sum % 2);
        return result;
    }
}
