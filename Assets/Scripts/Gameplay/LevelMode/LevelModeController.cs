﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class LevelModeController : LevelGame
{

    public static System.Action OnLevelComplete;

    [SerializeField]
    private List<LevelModeData> levels;
    [SerializeField]
    private CityCreator cityCreator;
    [SerializeField]
    private CityArchitect cityArchitect;
    [SerializeField]
    private PlayerCarModelAdapter playerCarModelAdapter;

    private PlayerSpeedManager playerSpeedManager;
    private WorldOrder worldOrder;

    void Awake()
    {
        playerSpeedManager = PrefabReferences.Instance.Player.GetComponent<PlayerSpeedManager>();
        worldOrder = PrefabReferences.Instance.WorldOrder;
    }

    public void InitNormalMode()
    {
        worldOrder.InitNormalMode();
        playerSpeedManager.InitNormalMode();
        playerCarModelAdapter.SetPlayerCarFromPrefs();

        cityCreator.TurnOnNormalMode();
        cityCreator.Restart(false);
    }

    private void StartLevel(int index)
    {
        LevelModeData levelData = levels[index];
        worldOrder.InitLevel(levelData.Worlds);
        playerSpeedManager.InitLevel(levelData.Speed, levelData.WorldsLenght);

        cityArchitect.SetFixedRoads(index);

        cityCreator.TurnOnLevelMode(levelData.GetFinishCrossroadID());
        cityCreator.Restart(false);



        GameplayRandom.SetNewGenerator(index);
    }

    public override int GetLevelCount()
    {
        return levels.Count;
    }

    public override void InitLevel(int index)
    {

        if (levels != null)
        {
            if (index >= 0 && index < levels.Count)
            {
                StartLevel(index);
            }
            else
            {
                Debug.LogError("Level with index = " + index + " not exist.");
            }
        }
        else
        {
            Debug.LogError("Levels list not exist");
        }
    }

    public override bool IsContinueAvailable()
    {
        return ScoreCounter.Instance.CurrentScore < GetLevelLenght(GameplayManager.CurrentLevel) - 3;
    }

    public override void BackToNormalMode()
    {
        InitNormalMode();
    }

    public int GetMinCoinsAmountInLevel(int index)
    {
        if (levels != null)
        {
            if (index >= 0 && index < levels.Count)
            {
                return levels[index].MinCoinsCount;
            }
            else
            {
                Debug.LogError("Level with index = " + index + " not exist.");
            }
        }
        else
        {
            Debug.LogError("Levels list not exist");
        }
        return 0;
    }


    public int GetMaxCoinsAmountInLevel(int index)
    {
        if (levels != null)
        {
            if (index >= 0 && index < levels.Count)
            {
                return levels[index].MaxCoinsCount;
            }
            else
            {
                Debug.LogError("Level with index = " + index + " not exist.");
            }
        }
        else
        {
            Debug.LogError("Levels list not exist");
        }
        return 0;
    }

    public int GetLevelLenght(int index)
    {
        if (levels != null)
        {
            if (index >= 0 && index < levels.Count)
            {
                int sum = 0;
                foreach (var item in levels[index].WorldsLenght)
                {
                    sum += item;
                }
                return sum;
            }
            else
            {
                Debug.LogError("Level with index = " + index + " not exist.");
            }
        }
        else
        {
            Debug.LogError("Levels list not exist");
        }

        return 0;
    }



}
