﻿using UnityEngine;
using System.Collections;

public class OnTurnTrigger : MonoBehaviour
{

    [SerializeField]
    private TurnDirection turnDirection;

    void OnTriggerEnter(Collider collider)
    {
        if (collider.tag == "Player")
        {
            PlayerController.OnTurn(turnDirection);
        }
    }
}
