using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class WorldOrder : MonoBehaviour
{
    public static WorldType CurrentWorld { get; set; }

    [SerializeField]
    private int christmasCityOffsetInCities = 2;
    [SerializeField]
    private List<WorldOrderItem> worldOrderItems;

    private List<WorldOrderItem> originalWorldOrderItems;
    private bool wasChristmasCity = false;
    private int christmasCityFor = 0;

    private void Awake()
    {
        WorldDependent.CheckForDuplicates(worldOrderItems, this.GetType().Name);
        originalWorldOrderItems = new List<WorldOrderItem>();
        worldOrderItems.CopyTo(originalWorldOrderItems);
        SafeCheckWorldOrder();
    }

    public void SetWorld(int level, bool isWorldWithBank)
    {
        bool activateChristmasCity = GTMParameters.IsChristmas && !isWorldWithBank && !wasChristmasCity && christmasCityFor == 0;
        if (activateChristmasCity)
        {
            CurrentWorld = WorldType.ChristmasCity;
            wasChristmasCity = true;
        }
        else
        {
            wasChristmasCity = false;
            CurrentWorld = GetWorldType(level);
        }

        if (!isWorldWithBank)
        {
            christmasCityFor = (christmasCityFor + 1) % christmasCityOffsetInCities;
        }
    }

    private void SafeCheckWorldOrder()
    {
        for (int i = 0; i < worldOrderItems.Count; i++)
        {
            if (!worldOrderItems.Exists(x => x.worldOrder == i + 1))
            {
                DL.LogError(this.GetType().Name + ". Missing world order: " + (i + 1));
            }
        }
    }

    public WorldType GetWorldType(int worldID)
    {
        return worldOrderItems[worldID % worldOrderItems.Count].worldType;
    }

    public int GetWorldDependentAmount()
    {
        return worldOrderItems.Count;
    }

    public void InitNormalMode()
    {
        worldOrderItems.Clear();
        originalWorldOrderItems.CopyTo(worldOrderItems);
        SetWorld(0, true);
    }

    public void InitLevel(WorldType[] worlds)
    {
        worldOrderItems = new List<WorldOrderItem>();
        if (worlds != null && worlds.Length > 0)
        {
            foreach (var world in worlds)
            {
                WorldOrderItem worldItem = originalWorldOrderItems.Find(x => x.worldType == world);
                worldOrderItems.Add(worldItem);
            }
        }
        else
        {
            Debug.LogError("levelWorlds not exist or levelWorlds is empty");
        }

        SetWorld(0, true);
    }
}


[System.Serializable]
public class WorldOrderItem : WorldDependent
{
    [Tooltip("Starting from 1")]
    public int worldOrder;
}

public abstract class WorldDependent
{
    public string debugName; // only for inspector to identify world
    public WorldType worldType;

    public static void CheckForDuplicates<T>(List<T> worldDependents, string className) where T : WorldDependent
    {
        List<WorldType> usedWorldTypes = new List<WorldType>();

        foreach (var world in worldDependents)
        {
            if (usedWorldTypes.Contains(world.worldType))
            {
                DL.LogError(className + ". Duplicated WorldType: " + world.worldType + " in " + world.debugName);
            }

            usedWorldTypes.Add(world.worldType);
        }
    }
}


public enum WorldType
{
    // Add only on the end of list
    // Do not add enum if World is not used in WorldOrder 

    Miami,
    China,
    IceCity,
    NewYork,
    SinCity,
    Surreal,
    Modern,
    Cyber,
    Psycho,
    London,
    ChristmasCity
}

