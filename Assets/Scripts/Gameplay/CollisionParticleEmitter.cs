﻿using UnityEngine;
using System.Collections;

public class CollisionParticleEmitter : MonoBehaviour {

	void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "PlayerKiller")
        {
            SpawnParticleSystem(collision.contacts[0].point);
        }      
    }

    private void SpawnParticleSystem(Vector3 position)
    {
        //TBD LATER - waiting for prefab
        //Instantiate(PrefabReferences.Instance.CollisionParticleSystemPrefab, position, Quaternion.LookRotation(Vector3.up));
    }
}
