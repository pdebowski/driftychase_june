﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UpdateNowOfferVisuals : MonoBehaviour {

    [SerializeField]
    private Text headlineText;
    [SerializeField]
    private Text descriptionText;

    void Start()
    {
        headlineText.text = "To Version " + GTMParameters.LiveVersionNumber;
        descriptionText.text = GTMParameters.LiveUpdateDescription;
    }
}
