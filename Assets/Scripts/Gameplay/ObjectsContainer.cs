﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ObjectsContainer : MonoBehaviour
{
    [SerializeField]
    private List<GameObject> prefabs;
    [SerializeField]
    private Transform objectsHolder;
    [SerializeField]
    private int containerSize;

    private List<Transform> availableObjects;
    private List<Transform> objectsInUse;

    void Awake()
    {
        availableObjects = new List<Transform>();
        objectsInUse = new List<Transform>();
        Fill();
    }

    private void Fill()
    {
        availableObjects = new List<Transform>();
        objectsInUse = new List<Transform>();

        for (int i = 0; i < containerSize; i++)
        {
            AddNewObject();
        }
    }

    private void AddNewObject()
    {
        Transform newObject = GameObject.Instantiate(prefabs.RandomElement()).GetComponent<Transform>();
        availableObjects.Add(newObject);
        newObject.gameObject.SetActive(false);
        newObject.SetParent(objectsHolder);
    }

    public void Restart()
    {
        if (objectsInUse.Count > 0)
        {
            while (objectsInUse.Count > 0)
            {
                EndUsing(objectsInUse[0]);
            }
        }
        objectsInUse.Clear();
    }



    public Transform GetNextObject()
    {
        if (availableObjects.Count == 0)
        {
            AddNewObject();
        }

        Transform objectToReturn = availableObjects[0];
        StartUsing(objectToReturn);

        return objectToReturn;
    }

    private void StartUsing(Transform instandiatedObject)
    {
        if (instandiatedObject != null)
        {
            availableObjects.Remove(instandiatedObject);
            objectsInUse.Add(instandiatedObject);
        }
    }

    public void EndUsing(Transform objectInUse)
    {
        if (objectInUse != null)
        {
            objectsInUse.Remove(objectInUse);
            availableObjects.Add(objectInUse);
            objectInUse.gameObject.SetActive(false);
            objectInUse.SetParent(this.transform);
        }
    }



}
