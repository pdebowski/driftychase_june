﻿using UnityEngine;
using System.Collections;

public class HelicopterController : MonoBehaviour {

    [SerializeField]
    private Transform target;
    [SerializeField]
    private Vector3 maxOffset;
    [SerializeField]
    private Vector3 minOffset;

    private Rigidbody rigidBody;
    private Vector3 offset;
    public Vector3 destOffset;

    //DEBUG SCRIPT

    void Awake()
    {
        rigidBody = GetComponent<Rigidbody>();
    }

    void OnEnable()
    {
        GameplayManager.OnSessionEnded += OnSessionEndedAction;
        PlayerController.OnTurn += SetNewOffset;
    }

    void OnDisable()
    {
        GameplayManager.OnSessionEnded -= OnSessionEndedAction;
        PlayerController.OnTurn -= SetNewOffset;
    }

    void FixedUpdate()
    {
        offset = Vector3.Lerp(offset, destOffset, Time.deltaTime);
        float lerpSpeed = 3;

        Vector3 localOffset = Quaternion.Euler(new Vector3(0, transform.rotation.eulerAngles.y, 0)) * offset;
        float distance = Vector3.Distance(target.position.AddY(20f) + localOffset, transform.position);
        Vector3 direction = (target.position.AddY(20f) + localOffset - transform.position).normalized;
        float speed = 50 * Time.deltaTime * Mathf.InverseLerp(0, 20, distance);
        Vector3 newPosition = transform.position + direction * speed;

        //Vector3 newPosition = Vector3.Lerp(transform.position, target.position.AddY(20f) + Quaternion.LookRotation(transform.forward) * offset, Time.deltaTime * lerpSpeed);
        rigidBody.MovePosition(newPosition);

        Vector3 targetRotationEuler = target.rotation.eulerAngles;
        targetRotationEuler = targetRotationEuler.ChangeX(30);
        Quaternion newRotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(targetRotationEuler), Time.deltaTime * lerpSpeed);
        rigidBody.MoveRotation(newRotation);
    }

    private void OnSessionEndedAction()
    {
        SetNewOffset();
        Snap();
    }

    private void SetNewOffset(TurnDirection turnDirection = TurnDirection.Right)
    {
        destOffset = new Vector3(Random.Range(minOffset.x, maxOffset.x), Random.Range(minOffset.y, maxOffset.y), Random.Range(minOffset.z, maxOffset.z));
    }

    private void Snap()
    {
        Vector3 targetRotationEuler = target.rotation.eulerAngles;
        targetRotationEuler = targetRotationEuler.ChangeX(30);
        Quaternion newRotation = Quaternion.Euler(targetRotationEuler);
        transform.rotation = newRotation;

        Vector3 localOffset = Quaternion.Euler(new Vector3(0, transform.rotation.eulerAngles.y, 0)) * offset;
        transform.position = target.position.AddY(20f) + localOffset;
    }

	
}

