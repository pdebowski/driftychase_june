﻿using UnityEngine;
using System.Collections;

public class PoliceCameraLights : MonoBehaviour
{
    [SerializeField]
    private bool disable = false;
    [SerializeField]
    private float lightSwichingDelay;
    [SerializeField]
    private GameObject[] lights;

    private bool working = false;
    private bool waiting = false;
    private int counter = 0;

    public void StartWorking()
    {
        if (!disable)
        {
            working = true;
            waiting = false;
        }
    }

    public void EndWorking()
    {
        working = false;
        foreach (var item in lights)
        {
            item.SetActive(false);
        }
    }

    void Update()
    {
        if (working && !waiting)
        {
            lights[counter].SetActive(false);
            counter++;
            if (counter >= lights.Length)
            {
                counter = 0;
            }
            StartCoroutine(ActivateWithDelay(lights[counter], lightSwichingDelay));
        }
    }

    private IEnumerator ActivateWithDelay(GameObject light, float delay)
    {
        waiting = true;
        yield return new WaitForSeconds(delay);
        if (working)
        {
            light.SetActive(true);
        }
        waiting = false;
    }

}
