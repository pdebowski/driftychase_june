﻿using UnityEngine;
using System.Collections;

public class AnchoredPositionAnimation : BaseAnimation
{
    [SerializeField]
    private AnimationCurve yPosition;
    [SerializeField]
    private float yFactorMinValue = 1;
    [SerializeField]
    private float yFactorMaxValue = 5;
    [SerializeField]
    private float durationMin = 3;
    [SerializeField]
    private float durationMax = 10;
    [SerializeField]
    private Transform anchoredObject;

    private Vector3 anchoredOffset;
    private float yFactor = 0;

    protected override void Init()
    {
        base.Init();

        anchoredOffset = transform.position - anchoredObject.position;
        yFactor = Random.Range(yFactorMinValue, yFactorMaxValue);
        duration = Random.Range(durationMin, durationMax); 
    }

    protected override void Calculate(float phase)
    {
        Vector3 newPosition = anchoredObject.position + anchoredOffset;

        newPosition.y += yPosition.Evaluate(phase) * yFactor;

        transform.position = newPosition;
    }
}
