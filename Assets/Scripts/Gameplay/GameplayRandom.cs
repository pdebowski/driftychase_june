﻿using UnityEngine;
using System.Collections;

public class GameplayRandom : MonoBehaviour
{

    private static System.Random randomInstance;
    private static int currentSeed;

    public static void SetNewGenerator()
    {
        if (randomInstance == null)
        {
            currentSeed = Random.Range(0, 1000000);
        }
        else
        {
            currentSeed = RandomBetween(0, 1000000);
        }

        randomInstance = new System.Random(currentSeed);
    }

    public static void SetNewGenerator(int seed)
    {
        currentSeed = seed;
        randomInstance = new System.Random(currentSeed);
    }

    public static int GetCurrentSeed()
    {
        return currentSeed;
    }

    public static float Hit()
    {
        return (float)randomInstance.NextDouble();
    }

    public static float RandomBetween(float val1, float val2)
    {
        if (val2 < val1)
        {
            return val1;
        }
        return val1 + (float)randomInstance.NextDouble() * (val2 - val1);
    }

    public static int RandomBetween(int val1, int val2)
    {
        if (val2 < val1)
        {
            int temp = val1;
            val1 = val2;
            val2 = temp;
        }

        return randomInstance.Next(val1, val2);
    }




}
