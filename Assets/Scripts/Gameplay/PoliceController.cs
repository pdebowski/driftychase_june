using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PoliceController : MonoBehaviour
{
    [SerializeField]
    private float strength = 25;

    [SerializeField]
    private float speed = 25;
    [Header("Turning")]
    [SerializeField]
    private float visualDriftMaxAngle = 35f;
    [SerializeField]
    private AnimationCurve driftVectorCurve;
    [SerializeField]
    private AnimationCurve driftRotationCurve;
    [SerializeField]
    private float driftTime = 0.5f;
    [SerializeField]
    private AnimationCurve speedAtOffset;
    [SerializeField]
    private float minOffset = 10;
    [SerializeField]
    private float maxOffset = 30;   
    [SerializeField]
    private MeshRenderer overridedMeshRenderer;

    private Vector3 targetRotation;
    private Vector3 nextTargetRotation;

    private PoliceCameraLights policeCameraLights;
    private Transform player;
    private float orgMinOffset;
    private int rotateSign = 1;
    private float driftPhase = 0;
    private Rigidbody rigidBody;
    private Vector3 precalculatedPosition;
    private bool working = false;
    private bool snapToPlayer = false;
    private float notVisibleTimer = 0;
    private bool shouldSpawnBehindPlayer = false;
    private float untouchableTime = 0;
    private bool isDrifting = false;
    private bool initialised = false;
    private PlayerSpeedManager playerSpeedManager;
    private PoliceSounds policeSounds;

    private Vector3 startPosition;
    private Vector3 startRotation;

    private Transform turnPointHelper;
    private Vector3 playerCrashPosition;
    private int currentFakeCrashesAmount;
    private int maxFakeCrashesAmount;


    public void Init(PoliceCameraLights policeCameraLights)
    {
        if (initialised)
        {
            return;
        }

        policeSounds = GetComponentInChildren<PoliceSounds>(true);
        this.policeCameraLights = policeCameraLights;
        player = PrefabReferences.Instance.Player;

        startPosition = transform.position;
        rigidBody = GetComponent<Rigidbody>();
        playerSpeedManager = player.GetComponent<PlayerSpeedManager>();
        turnPointHelper = new GameObject().transform;
        turnPointHelper.transform.SetParent(transform.parent);
        turnPointHelper.name = "PoliceTurnPointHelper";
        orgMinOffset = minOffset;

        Initialise();
        Restart();
        SetMaxCrashesAmount();
        StartCoroutine(RandomizeOffsetFromPlayer());
    }


    void OnEnable()
    {
        GameplayManager.OnSessionEnded += Restart;
        PlayerController.OnTurn += OnPlayerTurn;
        GameplayManager.OnSessionStarted += StartMoving;
        GameplayManager.OnPlayerCrash += OnPlayerCrash;
        GameplayManager.OnContinueUsed += OnContinueUsedAction;
        GameplayManager.OnContinueResultScreenLoading += OnGameFade;
    }

    void OnDisable()
    {
        GameplayManager.OnSessionEnded -= Restart;
        PlayerController.OnTurn -= OnPlayerTurn;
        GameplayManager.OnSessionStarted -= StartMoving;
        GameplayManager.OnPlayerCrash -= OnPlayerCrash;
        GameplayManager.OnContinueUsed -= OnContinueUsedAction;
        GameplayManager.OnContinueResultScreenLoading -= OnGameFade;
    }



    void OnDestroy()
    {
        if (turnPointHelper != null)
        {
            Destroy(turnPointHelper.gameObject);
        }
    }

    private void OnPlayerCrash()
    {
        if (working)
        {
            snapToPlayer = true;
            playerCrashPosition = player.transform.position;
        }
        else
        {
            Respawn();
            working = true;
            snapToPlayer = true;
        }
    }

    private void OnGameFade(ResultSceneType type)
    {
        policeSounds.TurnOffSiren();
        policeSounds.TurnOnBiggerSiren();
    }

    private void StartMoving()
    {
        if (Tutorial.IsTutorial)
        {
            return;
        }
        policeSounds.TurnOnSiren();
        working = true;

    }

    private IEnumerator RandomizeOffsetFromPlayer()
    {
        float timer = 5;
        while (true)
        {
            yield return new WaitForSeconds(timer);
            minOffset = player.GetComponent<BoxCollider>().size.z / 2 + GetComponent<BoxCollider>().size.z / 2 + orgMinOffset + Random.Range(-2, 3);
            timer = 3;
            Keyframe key = speedAtOffset.keys[1];
            key.time = minOffset / maxOffset;
            speedAtOffset.MoveKey(1, key);
        }
    }

    private void OnContinueUsedAction()
    {
        policeSounds.TurnOnSiren();
        Vector3 newPosition = player.position - player.forward * 200f;
        newPosition.y = startPosition.y;
        transform.position = newPosition;
        working = false;
        snapToPlayer = false;

    }

    private void OnPlayerTurn(TurnDirection direction)
    {
        if (!initialised || Tutorial.IsTutorial)
        {
            return;
        }


        if (shouldSpawnBehindPlayer)
        {
            Respawn();
        }

        turnPointHelper.position = player.position;
        turnPointHelper.rotation = player.rotation;


        if (direction != TurnDirection.Forward)
        {
            StartCoroutine(RotateWithDelay(0.55f, direction));
        }
    }

    private void Respawn()
    {
        Vector3 spawnPosition = GetSpawnPosition();
        if (CouldBeSpawnedAt(spawnPosition))
        {
            policeCameraLights.StartWorking();
            SpawnBehindPlayer();
            shouldSpawnBehindPlayer = false;
            untouchableTime = 0;
            rigidBody.drag = 0;
            StartCoroutine(RandomizeOffsetFromPlayer());
        }
    }

    private bool CouldBeSpawnedAt(Vector3 position)
    {
        Vector3 extents = GetComponent<BoxCollider>().size / 2;
        Vector3 origin = position;
        Ray ray;

        origin += Vector3.up * 10;
        ray = new Ray(origin, -player.transform.up);

        if (Physics.Raycast(ray, 10, LayerMask.GetMask("Traffic")))
        {
            return false;
        }

        origin = position;
        origin -= player.transform.right * extents.x;
        origin -= player.transform.forward * extents.z;
        origin.y = 1;
        ray = new Ray(origin, player.transform.forward);

        if (Physics.Raycast(ray, extents.z * 2, LayerMask.GetMask("Traffic")))
        {
            return false;
        }

        origin += player.transform.right * extents.x * 2;
        ray = new Ray(origin, player.transform.forward);

        if (Physics.Raycast(ray, extents.z * 2, LayerMask.GetMask("Traffic")))
        {
            return false;
        }

        return true;
    }

    private void SpawnBehindPlayer()
    {
        StopAllCoroutines();
        SpawnRestart();
        transform.position = GetSpawnPosition();
        working = true;
    }

    private Vector3 GetSpawnPosition()
    {
        Vector3 newPosition = player.position - player.forward * 33f;
        newPosition.y = startPosition.y;
        return newPosition;
    }

    private IEnumerator RotateWithDelay(float delay, TurnDirection direction)
    {
        while (true)
        {
            Vector3 localPos = turnPointHelper.InverseTransformPoint(transform.position);

            if (localPos.z >= 0)
            {
                RotateCar(direction);
                break;
            }

            yield return null;
        }
    }

    void Initialise()
    {
        targetRotation = transform.rotation.eulerAngles;
        startRotation = transform.rotation.eulerAngles;
        startPosition = transform.position;

        initialised = true;
    }

    void RotateCar(TurnDirection direction)
    {
        if (direction == TurnDirection.Left)
        {
            rotateSign = -1;
        }
        else
        {
            rotateSign = 1;
        }

        driftPhase = 0;
        nextTargetRotation = targetRotation + new Vector3(0, rotateSign * 90, 0);
        isDrifting = true;
        policeSounds.OnDrift();
    }

    void Update()
    {
        if (!initialised)
        {
            return;
        }


        if (!snapToPlayer)
        {
            MeshRenderer renderer = overridedMeshRenderer != null ? overridedMeshRenderer : GetComponent<MeshRenderer>();

            if (!renderer.isVisible)
            {
                notVisibleTimer += Time.deltaTime;

                if (notVisibleTimer > 0.5f)
                {
                    shouldSpawnBehindPlayer = true;
                }
            }
            else
            {
                notVisibleTimer = 0;
                shouldSpawnBehindPlayer = false;
            }
        }
        if (untouchableTime > 0)
        {
            untouchableTime -= Time.deltaTime;
            if (untouchableTime <= 0)
            {
                this.gameObject.layer = LayerMask.NameToLayer("DynamicLighting");
                policeCameraLights.EndWorking();
            }
        }
    }

    void FixedUpdate()
    {
        if (!initialised)
        {
            return;
        }

        if (working)
        {
            if (!snapToPlayer)
            {


                float distance = Vector3.Distance(player.transform.position, transform.position);
                distance = Mathf.InverseLerp(0, maxOffset, distance);

                float boostFactor = speedAtOffset.Evaluate(distance) + 1;

                if (isDrifting)
                {
                    driftPhase += Time.deltaTime / driftTime;
                    speed = (playerSpeedManager.Speed) * boostFactor;

                    if (driftPhase > 1)
                    {
                        isDrifting = false;
                        driftPhase = 0;
                        targetRotation = nextTargetRotation;
                    }
                }
                else
                {
                    speed = (playerSpeedManager.Speed) * boostFactor;
                }

                AdjustPosition();
                AdjustRotation();
            }
        }
        if (snapToPlayer)
        {
            Vector3 force = player.transform.position - this.transform.position;
            force.y = 0;
            if (Vector3.Distance(player.transform.position, playerCrashPosition) > 5 && force.magnitude < 10)
            {
                force /= 100;
            }
            rigidBody.AddForce(force * rigidBody.mass * 3);

            Quaternion destRotation = Quaternion.LookRotation(force.normalized);
            rigidBody.MoveRotation(Quaternion.Lerp(transform.rotation, destRotation, Time.deltaTime * 4));
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        currentFakeCrashesAmount++;

        if ((collision.relativeVelocity.magnitude > strength || currentFakeCrashesAmount >= maxFakeCrashesAmount) && working)
        {
            currentFakeCrashesAmount = 0;
            SetMaxCrashesAmount();
            rigidBody.useGravity = true;
            rigidBody.drag = 10;
            working = false;
            policeSounds.OnCrash();
        }
    }

    private void SetMaxCrashesAmount()
    {
        maxFakeCrashesAmount = Random.Range(4, 6);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<TrafficWithHole>() != null && snapToPlayer == false)
        {
            rigidBody.useGravity = false;
            snapToPlayer = false;
            working = false;
            rigidBody.velocity = Vector3.zero;

        }
    }

    private void AdjustPosition()
    {
        float directionFactor = driftVectorCurve.Evaluate(driftPhase);

        Vector3 direction = Vector3.Lerp(Quaternion.Euler(targetRotation) * Vector3.forward, Quaternion.Euler(nextTargetRotation) * Vector3.forward, directionFactor);
        Vector3 newRigidBodyPosition = precalculatedPosition + direction * speed * Time.deltaTime;

        Vector3 directionForward = direction;
        Vector3 directionRight = Quaternion.Euler(0, 90, 0) * direction;

        Vector3 forwardVelocity = Vector3.Project(rigidBody.velocity, directionForward);
        Vector3 forwardVelocityDelta = directionForward * speed - forwardVelocity;

        Vector3 rightVelocity = Vector3.Project(rigidBody.velocity, directionRight);
        Vector3 rightVelocityDelta = directionRight * 0 - rightVelocity;

        rigidBody.AddForce(forwardVelocityDelta * rigidBody.mass * 10);
        rigidBody.AddForce(rightVelocityDelta * rigidBody.mass * 10);

        float yDiff = newRigidBodyPosition.y - transform.position.y;

        if (yDiff > 0)
        {
            yDiff = 0;
        }

        rigidBody.AddForce(Vector3.up * rigidBody.mass * yDiff / Time.deltaTime);
        precalculatedPosition += direction * speed * Time.deltaTime;
    }

    private void AdjustRotation()
    {
        Quaternion temporary = Quaternion.Lerp(Quaternion.Euler(targetRotation), Quaternion.Euler(nextTargetRotation), driftPhase);
        Quaternion endRotation = Quaternion.Euler(0, rotateSign * driftRotationCurve.Evaluate(driftPhase) * visualDriftMaxAngle, 0) * temporary;

        Vector3 newRotation = transform.rotation.eulerAngles.ChangeY(endRotation.eulerAngles.y);
        rigidBody.MoveRotation(Quaternion.Euler(newRotation));

        rigidBody.angularVelocity = rigidBody.angularVelocity.ChangeY(0);
    }

    private void SpawnRestart()
    {
        if (!initialised)
        {
            return;
        }

        driftPhase = 0;
        rigidBody.drag = 0;
        rigidBody.angularDrag = 0;
        rigidBody.useGravity = false;
        rigidBody.angularVelocity = Vector3.zero;
        rigidBody.velocity = Vector3.zero;
        shouldSpawnBehindPlayer = false;
        targetRotation = player.GetComponent<PlayerController>().GetTargetRotation();
        nextTargetRotation = targetRotation;
        isDrifting = false;
        snapToPlayer = false;
    }

    public void Restart()
    {
        SpawnRestart();

        transform.position = startPosition;
        transform.rotation = Quaternion.Euler(startRotation);
        precalculatedPosition = transform.position;
        snapToPlayer = false;
        working = false;
        targetRotation = startRotation;
        nextTargetRotation = targetRotation;
        policeSounds.TurnOffSiren();
    }
}
