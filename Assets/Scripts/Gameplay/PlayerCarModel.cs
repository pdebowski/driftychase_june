﻿using UnityEngine;
using System.Collections;

public class PlayerCarModel : MonoBehaviour
{

    public BoxCollider BoxCollider { get { return boxCollider; } }
    public Transform[] DriftParticles { get { return driftParticles; } }
    public Transform[] Explosions { get { return explosions; } }
    public GameObject PolicePrefab { get { return policePrefab; } }
    public AudioClip EngineStartClip { get { return engineStartClip; } }
	public GameObject CarLights { get { return lights; } }
    public Animator Animator { get { return animator; } }

    [SerializeField]
    private BoxCollider boxCollider;
    [SerializeField]
    private Transform[] driftParticles;
    [SerializeField]
    private Transform[] explosions;
    [SerializeField]
    private GameObject policePrefab;
    [SerializeField]
    private AudioClip engineStartClip;
	[SerializeField]
	private GameObject lights;
    [SerializeField]
    private Animator animator;
}
