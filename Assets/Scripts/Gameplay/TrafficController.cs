﻿using UnityEngine;
using System.Collections;

public class TrafficController : MonoBehaviour
{

    private const string TRAFFIC_LAYER = "Traffic";

    public RoadTrafficSpawner.CarValues CarValues { get; set; }

    private Rigidbody rigidBody;
    private MeshRenderer meshRenderer;
    private bool removeAfterBecameNotVisible = false;

    void Awake()
    {
        meshRenderer = transform.parent.GetComponent<MeshRenderer>();
        rigidBody = transform.parent.GetComponent<Rigidbody>();
    }

    void OnDisable()
    {
        removeAfterBecameNotVisible = false;
    }


    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer(TRAFFIC_LAYER) && other.GetComponent<TrafficController>() != null)
        {
            TryCopyVelocity(other);
        }
    }

    void OnTriggerStay(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer(TRAFFIC_LAYER) && other.GetComponent<TrafficController>() != null)
        {
            TryCopyVelocity(other);
        }
    }

    private void TryCopyVelocity(Collider other)
    {
        Rigidbody otherRB = other.GetComponent<Rigidbody>();
        if (otherRB == null)
        {
            return;
        }
        Vector3 otherVelocity = otherRB.velocity;
        TrafficController otherController = other.GetComponentInChildren<TrafficController>();
        if (otherVelocity != rigidBody.velocity && other.transform.forward == this.transform.forward && otherController.CarValues.OffsetSign == CarValues.OffsetSign)
        {
            rigidBody.velocity = otherVelocity;
        }
    }

    void Update()
    {
        if (removeAfterBecameNotVisible && !meshRenderer.isVisible)
        {
            PrefabReferences.Instance.TrafficContainer.Free(transform.parent);
        }
    }

    public void DeactivateSelfAfterVisibleLost()
    {
        removeAfterBecameNotVisible = true;
    }
}
