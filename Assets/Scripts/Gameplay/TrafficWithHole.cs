﻿using UnityEngine;
using System.Collections;

public class TrafficWithHole : MonoBehaviour
{

    void OnEnable()
    {
        PlayerSpeedManager.OnSpeedIncrease += ChangeSpeed;
    }

    void OnDisable()
    {
        PlayerSpeedManager.OnSpeedIncrease -= ChangeSpeed;
    }

    private void ChangeSpeed(int level)
    {
        GetComponent<Rigidbody>().velocity = GetComponent<Rigidbody>().velocity.normalized * PrefabReferences.Instance.Player.GetComponent<PlayerController>().Speed;
    }
}
