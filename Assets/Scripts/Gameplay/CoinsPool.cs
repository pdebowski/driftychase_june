﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CoinsPool : MonoBehaviour {

    [SerializeField]
    
    private int preSpawnAmmount = 0;
    [SerializeField]
    private GameObject coinPrefab;

    List<CoinBag> coinsAvailable = new List<CoinBag>();
    List<CoinBag> coinsUsed = new List<CoinBag>();

    public void SetValues(int preSpawnAmmount, GameObject coinPrefab)
    {
        this.preSpawnAmmount = preSpawnAmmount;
        this.coinPrefab = coinPrefab;
    }

    void Start()
    {
        for (int i = 0; i < preSpawnAmmount; i++)
        {
            CoinBag coin = Spawn();
            coinsAvailable.Add(coin);
        }
    }

    public CoinBag Get()
    {
        CoinBag coin;

        if (coinsAvailable.Count>0)
        {
            coin = coinsAvailable[0];
            coinsAvailable.Remove(coin);
            coinsUsed.Add(coin);
        }
        else
        {
            coin = Spawn();
            coinsUsed.Add(coin);
        }

        coin.gameObject.SetActive(true);

        return coin;
    }

    public void Return(CoinBag coin)
    {
        coin.transform.parent = transform;
        coin.gameObject.SetActive(false);
        coinsUsed.Remove(coin);
        coinsAvailable.Add(coin);
    }

    private CoinBag Spawn()
    {
        CoinBag coin = (Instantiate(coinPrefab, Vector3.zero, Quaternion.identity) as GameObject).GetComponent<CoinBag>();
        coin.gameObject.SetActive(false);

        return coin;
    }
}
