﻿using UnityEngine;
using System.Collections;

public class HumanController : MonoBehaviour
{
    [HideInInspector]
    public bool isDead = false;

    private Animator animator;
   

    private float speed { get { return animator.GetFloat("Speed_f"); } set { animator.SetFloat("Speed_f", value); } }

    private bool triggerLock = false;
    private const float WALK_SPEED = 0.15f;
    private const float RUN_SPEED = 1f;

    void Awake()
    {
        animator = GetComponentInParent<Animator>();
        Restart();
    }

    void FixedUpdate()
    {
        triggerLock = false;
    }

    public void TryDie(Collider collider)
    {
        speed = 0;
        animator.enabled = false;
        isDead = true;
        //GetComponent<Rigidbody>().drag = 100000;
        //GetComponent<Rigidbody>().velocity = Vector3.zero;

        //if(collider.gameObject.name == "store_corner")
        //{
        //    Debug.Log(2);
        //}

        //if (Vector3.Distance(transform.position, collider.transform.position) < 5)
        //{
        //    Vector3 closestOther = collider.ClosestPointOnBounds(transform.position);
        //    Vector3 closestThis = transform.position;

        //    float distanceToKill = 0.7f;

        //    if (Vector3.Distance(closestOther, closestThis) < distanceToKill)
        //    {
        //        speed = 0;
        //        animator.enabled = false;
        //        isDead = true;
        //        GetComponent<Rigidbody>().drag = 100000;
        //        GetComponent<Rigidbody>().velocity = Vector3.zero;
        //    }
        //}
    }

    public void TryRunAway(Transform player)
    {
        if (!triggerLock)
        {
            triggerLock = true;
            speed = RUN_SPEED;


            Vector3 referencePosition = player.InverseTransformPoint(transform.position);


            float normalFactor = (15 - Mathf.Abs(referencePosition.x)) / 15f * Mathf.Sign(referencePosition.x);

            Quaternion targetRotation = Quaternion.Euler(transform.rotation.eulerAngles.x, Quaternion.LookRotation(player.forward).eulerAngles.y, transform.rotation.eulerAngles.z);

            //float additionalRotation = Mathf.Lerp(20, 90, Mathf.Abs(normalFactor)) * Mathf.Sign(normalFactor);

            targetRotation *= Quaternion.Euler(0, 90 * normalFactor, 0);

            transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, 3 * Time.deltaTime);
        }
    }

    public void Restart()
    {
        animator.enabled = true;
        triggerLock = false;
        isDead = false;
        speed = WALK_SPEED;
    }

}
