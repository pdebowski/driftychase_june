﻿using UnityEngine;
using System.Collections;

public class HumanSmallTrigger : MonoBehaviour {

    private HumanController human;

    void Awake()
    {
        human = GetComponentInParent<HumanController>();
    }


    void OnTriggerEnter(Collider collider)
    {
        if (!human.isDead)
        {
            if (collider.gameObject.tag == "PlayerKiller" || collider.gameObject.tag == "Player")
            {
                human.TryDie(collider);
            }
        }
    }
}
