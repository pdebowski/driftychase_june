﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class OppositeTrafficSpawner : RoadTrafficSpawner
{
    protected float trafficDestPosInLocal = 0;
    protected bool canceled = false;

    private float destPosNextCarOffset = 0;

    protected override CarValues SpawnTraffic(GameObject[] availableTraffic, PlayerController player)
    {
        isTutorialCar = false;
        canceled = false;

        GameObject prefab = GetRandomTrafficFrom(availableTraffic);
        if (prefab == null)
        {
            return null;
        }
        prefab = WideTrafficFilter(prefab);

        TrafficInfo trafficInfo = prefab.GetComponent<TrafficInfo>();
        CarValues trafficCar = GetTrafficValuesFromPrefab(prefab);

        bool couldSpawnOnTwoSides = CheckIfCouldSpawnOnTwoSides(trafficCar, trafficInfo.Type);

        float offsetSign = GetRandomHit();
        offsetSign = SignFilter(offsetSign, trafficInfo, couldSpawnOnTwoSides);

        float playerCarS = GetPlayerCarS(trafficInfo, trafficCar, offsetSign);
        playerCar.SetValues(player, playerCarS, offsetSign);
        bool spawnfirstWithDifferentOffsetSign = false;

        if (trafficCars.Count > 0)
        {
            spawnfirstWithDifferentOffsetSign = !SetValuesToNext(offsetSign, trafficCar, trafficInfo);
        }

        if (trafficCars.Count == 0 || spawnfirstWithDifferentOffsetSign)
        {
            SetValuesToFirst(offsetSign, trafficCar, trafficInfo);
        }

        trafficCar.SetDestPos(trafficDestPosInLocal);
        trafficCar.CouldSlowingDown = false;
        Vector3 trafficSpawnPositionInLocal = new Vector3(trafficPositionInLocal.x, trafficPositionInLocal.y, trafficDestPosInLocal);

        if (CouldSpawn(trafficInfo, couldSpawnOnTwoSides, prefab) && !canceled)
        {
            prefab = CreateTrafficCar(prefab, trafficSpawnPositionInLocal, trafficCar.V, trafficCar);
            trafficCar.SetCarObject(prefab);
        }
        else
        {
            trafficCar = null;
        }

        return trafficCar;
    }

    private float SignFilter(float offsetSign, TrafficInfo trafficInfo, bool couldSpawnOnTwoSides)
    {
        if (isTutorialCar)
        {
            return -1;
        }
        else if (trafficInfo.Type == TrafficTypes.Static)
        {
            return -1;
        }
        else if (couldSpawnOnTwoSides)
        {
            return offsetSign;
        }
        else
        {
            if (trafficInfo.Type == TrafficTypes.WithHole)
            {
                return -1;
            }
            else
            {
                return 1;
            }
        }
    }

    private void SetValuesToFirst(float sign, CarValues trafficCar, TrafficInfo trafficInfo)
    {
        float trafficCarV = GetSpeedFor(trafficInfo);
        trafficCarV = TrafficVFilter(sign, trafficInfo, trafficCarV);
        float trafficCarS = playerCar.T * trafficCarV;
        trafficCar.SetValues(trafficCarV, trafficCarS);
        trafficCar.SetOffsetSign(sign);

        destPosNextCarOffset = 0;
        trafficDestPosInLocal = turnPointInLocal.z
            + GetSumWithSign(-reversedSpawnPointSign * sign, trafficCar.Size.z / 2, playerCar.Size.x / 2)
            + GetSafeDistance(-reversedSpawnPointSign * sign)
            + GetSafeDistanceOffset(-reversedSpawnPointSign * sign);



        trafficDestPosInLocal = DestPosOtherTrafficFilter(trafficCar, trafficDestPosInLocal);
        trafficDestPosInLocal = DestPosFilter(trafficInfo, trafficCar, trafficDestPosInLocal);
    }

    //Return true if traffic with this sign is available to spawn
    private bool SetValuesToNext(float sign, CarValues trafficCar, TrafficInfo trafficInfo)
    {
        CarValues firstCarInTraffic = trafficCars.Find(x => x.OffsetSign == sign);
        CarValues previousCarInTraffic = trafficCars.FindLast(x => x.OffsetSign == sign);
        if (firstCarInTraffic == null)
        {
            return false;
        }
        else
        {
            float trafficCarV = firstCarInTraffic.V + GetTrafficColumnSpeedOffset();
            if (sign == 1)
            {
                trafficCarV = RandomBetween(firstCarInTraffic.V, firstCarInTraffic.V + 5);
            }
            else
            {
                trafficCarV = RandomBetween(trafficInfo.MinSpeed, firstCarInTraffic.V + manager.MaxTrafficColumnSpeedOffset);
            }
            destPosNextCarOffset = GetOffsetBeetwenTraffic(-sign) + GetTrafficSizeOffset(-sign, trafficCar, previousCarInTraffic);
            float trafficCarS = previousCarInTraffic.S + destPosNextCarOffset;

            destPosNextCarOffset = trafficCarS - firstCarInTraffic.S;

            trafficCar.SetValues(trafficCarV, trafficCarS);
            trafficCar.SetOffsetSign(sign);
            trafficDestPosInLocal = previousCarInTraffic.DestinationPosition;
            trafficDestPosInLocal = DestPosOtherTrafficFilter(trafficCar, trafficDestPosInLocal);
            return true;
        }
    }

    private float TrafficVFilter(float sign, TrafficInfo info, float currentV)
    {
        if (info.Type == TrafficTypes.WithHole)
        {
            currentV = playerCar.V;
        }
        else if (info.Type == TrafficTypes.Static)
        {
            currentV = 0;
        }
        else if (info.Type == TrafficTypes.Slow || info.Type == TrafficTypes.Fast || info.Type == TrafficTypes.Wide)
        {
            return currentV;
        }
        return currentV;
    }

    protected float DestPosOtherTrafficFilter(CarValues trafficCar, float currentDestPos)
    {
        if (isTutorialCar)
        {
            return currentDestPos;
        }

        int attempts = 5;
        float precision = 1f;

        bool result = false;

        if (manager.TrafficToAvoid == null)
        {
            return currentDestPos;
        }

        for (int i = 0; i < attempts; i++)
        {
            if (CheckIfTrafficCollide(trafficCar, currentDestPos, precision))
            {
                currentDestPos += (-reversedSpawnPointSign * trafficCar.OffsetSign) * 5;
                result = true;
            }
            else
            {
                result = false;
                break;
            }
        }

        canceled = result;

        return currentDestPos;
    }

    protected virtual bool CheckIfTrafficCollide(CarValues trafficCar, float currentDestPos, float precision)
    {
        foreach (var other in manager.TrafficToAvoid)
        {
            Vector3 otherInLocal = transform.InverseTransformPoint(other.CarObject.transform.position);
            Vector3 contactPosition = new Vector3(trafficPositionInLocal.x, trafficPositionInLocal.y, -otherInLocal.z);
            float mySizeOffset = other.Size.x / 2 + trafficCar.Size.z / 2;


            float myOffset = (currentDestPos - contactPosition.z) + trafficCar.S;
            float t1 = (myOffset - mySizeOffset) / trafficCar.V;
            float t2 = (myOffset + mySizeOffset) / trafficCar.V;

            float otherSizeOffset = other.Size.z / 2 + trafficCar.Size.x / 2;
            float otherOffset = Mathf.Abs(otherInLocal.x);
            float t3 = (otherOffset - otherSizeOffset) / other.V;
            float t4 = (otherOffset + otherSizeOffset) / other.V;

            if ((t1 > t3 && t1 < t4) || (t2 < t4 && t2 > t3)
                || (t3 > t1 && t3 < t2) || (t4 < t2 && t4 > t1))
            {
                return true;
            }

        }
        return false;
    }

    protected bool CheckIfCouldSpawnOnTwoSides(CarValues trafficCar, TrafficTypes trafficType)
    {
        float carLenght = playerCar.Size.z / 2;
        float minPos = turnPointInLocal.x - carLenght;
        float maxpos = turnPointInLocal.x + carLenght;
        if (trafficType == TrafficTypes.Fast
            || trafficType == TrafficTypes.WithHole
            || !(trafficPositionInLocal.x + trafficCar.Size.x / 2 < minPos || trafficPositionInLocal.x - trafficCar.Size.x / 2 > maxpos))
        {
            return false;
        }
        return true;
    }

    private bool CouldSpawn(TrafficInfo trafficInfo, bool couldSpawnOnTwoSides, GameObject prefab)
    {
        if (prefab == null || prefab.transform == null)
        {
            return false;
        }

        if (trafficInfo.Type == TrafficTypes.Static && !couldSpawnOnTwoSides)
        {
            return false;
        }
        else if (trafficInfo.Type == TrafficTypes.WithHole && FloatEqual(trafficPositionInLocal.x, turnPointInLocal.x))
        {
            return false;
        }
        else if ((Mathf.Abs(turnPointInLocal.z - trafficDestPosInLocal) + Mathf.Abs(destPosNextCarOffset)) > 35)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    private float DestPosFilter(TrafficInfo info, CarValues trafficCar, float currentDestPos)
    {
        if (info.Type == TrafficTypes.Static)
        {
            currentDestPos = trafficPositionInLocal.z - manager.RoadLenght / 2 + trafficCar.Size.z / 2 + 2;
        }
        else if (info.Type == TrafficTypes.Slow)
        {
            if (trafficCar.OffsetSign == -1)
            {
                currentDestPos += reversedSpawnPointSign * 7;
            }
        }
        else if (info.Type == TrafficTypes.WithHole)
        {
            currentDestPos = turnPointInLocal.z;
            if (trafficCar.OffsetSign == 1)
            {
                currentDestPos -= reversedSpawnPointSign * 5;
            }
        }
        else
        {
            if (trafficCar.OffsetSign == -1)
            {
                currentDestPos += reversedSpawnPointSign * 3;
            }
            else
            {
                currentDestPos -= reversedSpawnPointSign * 20;
            }
        }
        return currentDestPos;
    }

    protected float GetPlayerCarS(TrafficInfo info, CarValues trafficCar, float offsetSign)
    {
        float S = Mathf.Abs(playerPositionInLocal.x - trafficPositionInLocal.x) + Mathf.Abs(playerPositionInLocal.z - turnPointInLocal.z);

        if (info.Type == TrafficTypes.WithHole)
        {
            return S;
        }
        else
        {
            float carsCollidersOffset = GetSumWithSign(-offsetSign, playerCar.Size.z / 2, trafficCar.Size.x / 2);
            return S + carsCollidersOffset;
        }
    }

    protected override GameObject GetRandomTrafficFrom(GameObject[] availableTraffic)
    {
        List<GameObject> staticTraffics = GetStaticTraffics(availableTraffic);

        if (staticTraffics.Count > 0 && RandomBetween(0.0f, 1.0f) < manager.StaticTrafficChance)
        {
            return staticTraffics.RandomElement();
        }
        else
        {
            return base.GetRandomTrafficFrom(availableTraffic);
        }


    }

    private List<GameObject> GetStaticTraffics(GameObject[] availableTraffics)
    {
        List<GameObject> result = new List<GameObject>();

        foreach (var item in availableTraffics)
        {
            TrafficInfo trafficInfo = item.GetComponent<TrafficInfo>();
            if (trafficInfo.Type == TrafficTypes.Static)
            {
                result.Add(item);
            }
        }

        return result;
    }









}
