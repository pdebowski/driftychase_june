﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class OtherGames : MonoBehaviour, IEnumerable
{

    [SerializeField]
    private List<GameMode> availableOtherGames;

    private GameMode currentGame;

    public IEnumerator GetEnumerator()
    {
        return availableOtherGames.GetEnumerator();
    }

    public GameMode GetGameController(GameplayMode mode)
    {
        return availableOtherGames.Find(x => x.Mode == mode);
    }



}

[System.Serializable]
public class GameMode
{
    public GameplayMode Mode { get { return mode; } }

    [SerializeField]
    private GameplayMode mode;
    [SerializeField]
    private LevelGame levelController;

    public int GetLevelCount()
    {
        return levelController.GetLevelCount();
    }

    public void InitLevel(int index)
    {
        levelController.InitLevel(index);
    }

    public bool IsContinueAvailable()
    {
        return levelController.IsContinueAvailable();
    }

    public void BackToNormalMode()
    {
        levelController.BackToNormalMode();
    }



    // public interface to player game data

    public int GetHighestAvailableLevel()
    {
        return PrefsDataFacade.Instance.GetHighestAvailableLevel(mode);
    }

    public bool IsCompleted(int level)
    {
        return PrefsDataFacade.Instance.IsCompleted(mode, level);
    }

    public bool IsAvailable(int level)
    {
        return PrefsDataFacade.Instance.IsAvailable(mode, level);
    }

    public void Unlock(int level)
    {
        PrefsDataFacade.Instance.Unlock(mode, level);
        SetLevelProgress(level, 0);
    }

    public void SetAsCompleted(int level)
    {
        PrefsDataFacade.Instance.SetAsCompleted(mode, level);
        SetLevelProgress(level, 1);
    }

    public void SetLevelProgress(int level, float progress)
    {
        PrefsDataFacade.Instance.SetLevelProgress(mode, level, progress);
    }

    public float GetLevelProgress(int level)
    {
        return PrefsDataFacade.Instance.GetLevelProgress(mode, level);
    }
}
