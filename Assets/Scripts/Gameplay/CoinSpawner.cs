﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CoinSpawner : MonoBehaviour
{
    public static float playerCarCashMultiplier;

    private const int maxCoinsDistance = 300;
    private const int dailyGiftCoinCount = 20;

    [SerializeField]
    private Transform vertex1;
    [SerializeField]
    private Transform vertex2;

    private const float Y_HEIGHT = 1f;

    private float minX;
    private float maxX;
    private float minZ;
    private float maxZ;

    private List<CoinBagHolder> coinsBagHolders = new List<CoinBagHolder>();
    private bool giftCollectedReported = false;
    private bool isDailyGift = false;

    void Update()
    {
        if (!isDailyGift)
        {
            bool isOnRoad = IsOnRoad(PrefabReferences.Instance.Player.position);

            foreach (var coinBagHolder in coinsBagHolders)
            {
                coinBagHolder.coinBag.canMove = isOnRoad;
            }
        }
    }

    public void CleanCoins()
    {
        foreach (var coinBagHolder in coinsBagHolders)
        {
            coinBagHolder.pool.Return(coinBagHolder.coinBag);
        }

        coinsBagHolders.Clear();
    }

    public void SpawnNormalCoins()
    {
        giftCollectedReported = true;
        RespawnCoins(false);
    }

    private void RespawnCoins(bool isDailyGift)
    {
        CleanCoins();

        minX = Mathf.Min(vertex1.transform.position.x, vertex2.transform.position.x);
        maxX = Mathf.Max(vertex1.transform.position.x, vertex2.transform.position.x);
        minZ = Mathf.Min(vertex1.transform.position.z, vertex2.transform.position.z);
        maxZ = Mathf.Max(vertex1.transform.position.z, vertex2.transform.position.z);

        foreach (var coinSpawnInfo in CoinsSpawningSettings.Instance.coinSpawnInfos)
        {
            RespawnCoinsFromPool(coinSpawnInfo, isDailyGift);
        }
    }

    private void RespawnCoinsFromPool(CoinSpawnInfo coinSpawnInfo, bool isDailyGift)
    {
        if (!isDailyGift)
        {
            float quantityLeft = GetCoinsCount(coinSpawnInfo);

            while (quantityLeft > 1)
            {
                SpawnSingleCoin(coinSpawnInfo.pool, minX, maxX, minZ, maxZ, CoinsSpawningSettings.Instance.coinValue);
                quantityLeft--;
            }

            if (Random.Range(0f, 1f) < quantityLeft)
            {
                SpawnSingleCoin(coinSpawnInfo.pool, minX, maxX, minZ, maxZ, CoinsSpawningSettings.Instance.coinValue);
            }
        }
        else
        {
            int giftValue = PrefabReferences.Instance.CarStatsDB.GetBestOwnedCar().freeGiftValue / dailyGiftCoinCount;

            for (int i = 0; i < dailyGiftCoinCount; i++)
            {
                SpawnSingleCoin(coinSpawnInfo.pool, minX, maxX, minZ, maxZ, giftValue);
            }
        }
    }

    private float GetCoinsCount(CoinSpawnInfo coinSpawnInfo)
    {
        float quantityLeft = 0;

        if (GameplayManager.CurrentGameMode == null)
        {
            float lerpFactor = ScoreCounter.Instance.GetScore() / (float)maxCoinsDistance;
            quantityLeft = coinSpawnInfo.spawnCurve.Evaluate(lerpFactor)
                * coinSpawnInfo.spawnMaxAmmount
                * CoinsSpawningSettings.Instance.spawnFactor
                * playerCarCashMultiplier;
        }
        else
        {
            if (!GameplayManager.CurrentGameMode.IsCompleted(GameplayManager.CurrentLevel))
            {
                PlayerSpeedManager playerSpeed = PrefabReferences.Instance.Player.GetComponent<PlayerSpeedManager>();
                float lerpFactor = (ScoreCounter.Instance.GetScore() + 1) / ((float)playerSpeed.GetWorldsLenght());


                if (GameplayManager.CurrentGameMode.Mode == GameplayMode.NormalLevels && lerpFactor >= GameplayManager.CurrentGameMode.GetLevelProgress(GameplayManager.CurrentLevel))
                {
                    LevelModeController levelModeController = PrefabReferences.Instance.LevelModeController;
                    int minCoins = levelModeController.GetMinCoinsAmountInLevel(GameplayManager.CurrentLevel);
                    int maxCoins = levelModeController.GetMaxCoinsAmountInLevel(GameplayManager.CurrentLevel);
                    quantityLeft = coinSpawnInfo.spawnCurve.Evaluate(lerpFactor)
                                    * Mathf.Lerp(minCoins, maxCoins, lerpFactor)
                                    * CoinsSpawningSettings.Instance.spawnFactor
                                    * playerCarCashMultiplier;
                }
            }
        }


        return quantityLeft;
    }

    private void SpawnSingleCoin(CoinsPool pool, float minX, float maxX, float minZ, float maxZ, int coinValue)
    {
        CoinBagHolder coinBagHolder = new CoinBagHolder();
        coinBagHolder.pool = pool;
        coinBagHolder.coinBag = pool.Get();
        coinBagHolder.coinBag.transform.position = new Vector3(Random.Range(minX, maxX), Y_HEIGHT, Random.Range(minZ, maxZ));
        coinBagHolder.coinBag.transform.parent = transform;
        coinBagHolder.coinBag.SetCoinSpawner(this);
        coinBagHolder.coinBag.SetCoinValue(coinValue);
        coinsBagHolders.Add(coinBagHolder);
    }

    public void ReturnCoin(CoinBag coin)
    {
        CoinBagHolder coinBagHolder = coinsBagHolders.Find(x => x.coinBag == coin);

        if (coinBagHolder != null)
        {
            coinBagHolder.pool.Return(coin);
        }
        else
        {
            Debug.LogError("Returned coin cannot be found");
        }

        coinsBagHolders.Remove(coinBagHolder);
    }

    private bool IsOnRoad(Vector3 position)
    {
        return position.x > minX && position.x < maxX && position.z > minZ && position.z < maxZ;
    }

    public void SpawnGift()
    {
        isDailyGift = true;
        giftCollectedReported = false;
        RespawnCoins(true);
    }

    public List<CoinBag> GetCoinBags()
    {
        List<CoinBag> coinBags = new List<CoinBag>();
        foreach (var coinBagHolder in coinsBagHolders)
        {
            coinBags.Add(coinBagHolder.coinBag);
        }
        return coinBags;
    }

    public void TryReportGiftCollected()
    {
        if (!giftCollectedReported)
        {
            giftCollectedReported = true;
            //PrefabReferences.Instance.DailyGiftManager.GiftCollected();
        }
    }

}

class CoinBagHolder
{
    public CoinBag coinBag;
    public CoinsPool pool;
}