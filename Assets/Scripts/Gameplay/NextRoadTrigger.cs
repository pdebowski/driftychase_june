﻿using UnityEngine;
using System.Collections;

public class NextRoadTrigger : MonoBehaviour
{

    public static System.Action OnNextRoadEnter = delegate { };

    public void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            OnNextRoadEnter();
        }
    }
}
