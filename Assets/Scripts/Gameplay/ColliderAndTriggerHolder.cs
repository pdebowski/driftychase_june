﻿using UnityEngine;
using System.Collections;

public class ColliderAndTriggerHolder : MonoBehaviour {

    void OnEnable()
    {
        SetChildrenActive(false);
    }

	void OnTriggerEnter(Collider collider)
    {
	    if(collider.tag == "Player")
        {
            SetChildrenActive(true);
        }
	}

    void OnTriggerExit(Collider collider)
    {
        if (collider.tag == "Player")
        {
            SetChildrenActive(false);
        }
    }

    private void SetChildrenActive(bool value)
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            transform.GetChild(i).gameObject.SetActive(value);
        }
    }
}
