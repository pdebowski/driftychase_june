﻿using UnityEngine;
using System.Collections;

public class HumanSpawnPoint : MonoBehaviour {

    [SerializeField]
    private GameObject prefab;

    private HumanController human;

    void Awake()
    {
        if(GlobalParameters.peopleSpawningEnabled)
        {
            human = Instantiate(prefab).GetComponent<HumanController>();
        }       
    }

	void OnEnable()
    {
        Restart();
    }

    public void Restart()
    {
        if(human != null)
        {
            Vector3 spawnPos = transform.position;
            spawnPos.y = prefab.transform.position.y;

            human.transform.position = spawnPos;
            human.transform.rotation = transform.rotation;

            human.Restart();
        }
    }
}
