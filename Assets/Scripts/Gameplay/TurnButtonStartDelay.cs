﻿using UnityEngine;
using System.Collections;

public class TurnButtonStartDelay : MonoBehaviour
{

    [SerializeField]
    private float delay;
    [SerializeField]
    private GameObject turnButton;

    void OnEnable()
    {
        GameplayManager.OnSessionStarted += StartDelay;
    }

    void OnDisable()
    {
        GameplayManager.OnSessionStarted -= StartDelay;
    }

    public void StartDelay()
    {
        if (!Tutorial.IsTutorial && !DailyGiftManager.IsDailyGiftActive)
        {
            turnButton.SetActive(false);
            StartCoroutine(ActivateAfter(delay));
        }
    }

    public void StartDelay(float delay)
    {
        turnButton.SetActive(false);
        StartCoroutine(ActivateAfter(delay));
    }

    private IEnumerator ActivateAfter(float delay)
    {
        yield return CommonMethods.WaitForUnscaledSeconds(delay);
        turnButton.SetActive(true);
    }


}
