﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CoinsSpawningSettings : Singleton<CoinsSpawningSettings> {

    public float spawnFactor = 1;
    public int coinValue = 10;

    public List<CoinSpawnInfo> coinSpawnInfos;

    override protected void Awakened()
    {
        foreach (var coinSpawnInfo in coinSpawnInfos)
        {
            coinSpawnInfo.pool = gameObject.AddComponent<CoinsPool>();
            coinSpawnInfo.pool.SetValues(coinSpawnInfo.preSpawnAmmount, coinSpawnInfo.prefab);
        }
    }
}


[System.Serializable]
public class CoinSpawnInfo
{
    public GameObject prefab;
    [HideInInspector]
    public CoinsPool pool;
    public float spawnMaxAmmount = 2f;
    public AnimationCurve spawnCurve = new AnimationCurve(new Keyframe(0, 0), new Keyframe(1, 1));
    [Tooltip("Ammount to be spawned in Start, in order to avoid performance peaks during game")]
    public int preSpawnAmmount = 0;
}
