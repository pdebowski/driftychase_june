﻿using UnityEngine;
using System.Collections;

public class TrafficInfo : MonoBehaviour
{

    public TrafficTypes Type { get { return type; } private set { type = value; } }
    public float MinSpeed { get { return minSpeed; } private set { minSpeed = value; } }
    public float MaxSpeed { get { return maxSpeed; } private set { maxSpeed = value; } }

    [SerializeField]
    private TrafficTypes type;
    [SerializeField]
    private float minSpeed;
    [SerializeField]
    private float maxSpeed;

}
