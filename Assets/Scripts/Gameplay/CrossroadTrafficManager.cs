﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class CrossroadTrafficManager : MonoBehaviour
{
    public List<GameObject> NormalCars { get { return trafficContainer.AvailableNormalTraffic; } private set { trafficContainer.AvailableNormalTraffic = value; } }
    public List<GameObject> SpecialCars { get { return trafficContainer.AvailableSpecialTraffic; } private set { trafficContainer.AvailableSpecialTraffic = value; } }
    public float MaxSafeDistanceOffset { get { return maxSafeDistanceOffset; } private set { maxSafeDistanceOffset = value; } }
    public float MinDistanceBetweenTraffic { get { return minDistanceBetweenTraffic; } private set { minDistanceBetweenTraffic = value; } }
    public float MaxDistanceBetweenTraffic { get { return maxDistanceBetweenTraffic; } private set { maxDistanceBetweenTraffic = value; } }
    public float MaxTrafficColumnSpeedOffset { get { return maxTrafficColumnSpeedOffset; } private set { maxTrafficColumnSpeedOffset = value; } }
    public float StaticTrafficChance { get { return staticTrafficChance; } private set { staticTrafficChance = value; } }
    public List<RoadTrafficSpawner.CarValues> TrafficToAvoid { get; private set; }


    public float SafeDistanceFromPlayer { get; private set; }
    public int MaxNormalTrafficCount { get; private set; }
    public int MinNormalTrafficCount { get; private set; }
    public float RoadLenght { get; private set; }
    public bool IsLefTurnCrossroad { get; private set; }

    [SerializeField]
    private PlayerController player;
    [SerializeField]
    private TrafficContainer trafficContainer;

    [Header("TrafficSettings")]
    [SerializeField]
    private float specialTrafficSpawnChance;
    [SerializeField]
    private float maxSafeDistanceOffset;
    [SerializeField]
    private float minDistanceBetweenTraffic;
    [SerializeField]
    private float maxDistanceBetweenTraffic;
    [SerializeField]
    private float maxTrafficColumnSpeedOffset;
    [SerializeField]
    private float staticTrafficChance;


    private BehindTrafficSpawner[] roadsWithBehindTraffic;
    private OppositeTrafficSpawner[] roadsWithOppositeTraffic;
    private FromTheSideTrafficSpawner[] roadsWithFromTheSideTraffic;
    private bool isFirstCrossroad = true;
    private Transform currentCrossroad;
    private TurnPointManager turnPointManager;
    private bool blockSpecialSpawn = false;

    public void Init(float roadLenght)
    {
        TrafficToAvoid = new List<RoadTrafficSpawner.CarValues>();
        turnPointManager = GetComponent<TurnPointManager>();
        turnPointManager.Init();
        RoadLenght = roadLenght;
    }

    public void SetDifficultyLevel(float saveDistanceFromPlayer, int maxTrafficCount, int minTrafficCount)
    {
        SafeDistanceFromPlayer = saveDistanceFromPlayer;
        MaxNormalTrafficCount = maxTrafficCount;
        MinNormalTrafficCount = minTrafficCount;
    }

    private void Manage()
    {
        bool isSpecialCrossroad = isFirstCrossroad;
        turnPointManager.SetNewTurnPoint(roadsWithBehindTraffic, roadsWithOppositeTraffic, isSpecialCrossroad, currentCrossroad);

        RoadTrafficSpawner spawnerToSpawnSpecial = GetRandomRoadToSpawnSpecialTraffic();

        if (!PlayerPrefsAdapter.WasTutorialTruckSpawned && roadsWithBehindTraffic.Length > 0)
        {
            spawnerToSpawnSpecial = roadsWithBehindTraffic[0];
            trafficContainer.SetTruckAsSpecial();
        }

        SpawnAtRoad<OppositeTrafficSpawner>(roadsWithOppositeTraffic, spawnerToSpawnSpecial);
        TrafficToAvoid = SpawnAtRoad<BehindTrafficSpawner>(roadsWithBehindTraffic, spawnerToSpawnSpecial);

        if (!PlayerPrefsAdapter.WasTutorialTruckSpawned && roadsWithBehindTraffic.Length > 0)
        {
            trafficContainer.RefreshTrafficList();
            PlayerPrefsAdapter.WasTutorialTruckSpawned = true;
        }
    }


    private RoadTrafficSpawner GetRandomRoadToSpawnSpecialTraffic()
    {
        int index = GameplayRandom.RandomBetween(0, roadsWithBehindTraffic.Length + roadsWithFromTheSideTraffic.Length + roadsWithOppositeTraffic.Length);
        RoadTrafficSpawner result = null;

        if (GameplayRandom.Hit() < specialTrafficSpawnChance && !blockSpecialSpawn)
        {
            if (index < roadsWithBehindTraffic.Length)
            {
                result = roadsWithBehindTraffic[index];
            }
            else
            {
                index -= roadsWithBehindTraffic.Length;
                if (index < roadsWithFromTheSideTraffic.Length)
                {
                    result = roadsWithFromTheSideTraffic[index];
                }
                else
                {
                    index -= roadsWithFromTheSideTraffic.Length;
                    if (index < roadsWithOppositeTraffic.Length)
                    {

                        result = roadsWithOppositeTraffic[index];
                    }
                }
            }
        }
        blockSpecialSpawn = false;


        return result;
    }

    private List<RoadTrafficSpawner.CarValues> SpawnAtRoad<T>(RoadTrafficSpawner[] roadSpawners, RoadTrafficSpawner specialSpawnHere)
    {
        List<RoadTrafficSpawner.CarValues> spawnedCars = new List<RoadTrafficSpawner.CarValues>();
        List<RoadTrafficSpawner.CarValues> spawnedCarsAtSpawner; ;
        System.Array.Sort(roadSpawners);
        bool specialIsFromHere = false;
        foreach (var item in roadSpawners)
        {
            if (item.GetType() == typeof(T))
            {
                item.Init(player.transform.position, turnPointManager.LastTurnPoint, currentCrossroad, roadSpawners, player, this);
                if (item == specialSpawnHere)
                {
                    specialIsFromHere = true;
                }
            }
        }
        if (specialIsFromHere)
        {
            spawnedCarsAtSpawner = specialSpawnHere.Spawn(player, true);
            if (spawnedCarsAtSpawner != null)
            {
                spawnedCars.AddRange(spawnedCarsAtSpawner);
            }
        }
        foreach (var item in roadSpawners)
        {
            if (item.GetType() == typeof(T))
            {
                if (item != specialSpawnHere)
                {
                    spawnedCarsAtSpawner = item.Spawn(player, false);
                    if (spawnedCarsAtSpawner != null)
                    {
                        spawnedCars.AddRange(spawnedCarsAtSpawner);
                    }
                }
            }

        }
        return spawnedCars;
    }

    public void SetPlayerDataToTurn(Transform player)
    {
        turnPointManager.SetPlayerPositionOnTurn(player);
    }

    public void Restart()
    {
        turnPointManager.Restart();
        trafficContainer.Restart();
    }

    public void SetAsCurrentCrossroad(GameObject crossroadHolder, bool isFirstCrossroad, bool isLeftTurnCrossroad)
    {
        currentCrossroad = crossroadHolder.transform;
        roadsWithBehindTraffic = currentCrossroad.GetComponentsInChildren<BehindTrafficSpawner>(true);
        roadsWithOppositeTraffic = currentCrossroad.GetComponentsInChildren<OppositeTrafficSpawner>(true);
        roadsWithFromTheSideTraffic = currentCrossroad.GetComponentsInChildren<FromTheSideTrafficSpawner>(true);
        this.isFirstCrossroad = isFirstCrossroad;
        this.IsLefTurnCrossroad = isLeftTurnCrossroad;
        Manage();
    }

    public GameObject GetObjectFromPrefab(GameObject prefab)
    {
        return trafficContainer.GetTraffic(prefab);
    }

    public void ClearTrafficAt(Transform crossroad)
    {
        TrafficInfo[] traffics = crossroad.GetComponentsInChildren<TrafficInfo>(true);
        foreach (var item in traffics)
        {
            if (item.GetComponent<TrafficInfo>().Type == TrafficTypes.Static || !item.GetComponent<MeshRenderer>().isVisible)
            {
                EndUseTrafficCar(item.gameObject);
            }
            else
            {
                item.GetComponentInChildren<TrafficController>().DeactivateSelfAfterVisibleLost();
                item.transform.parent = null;
            }

        }
    }

    public void EndUseTrafficCar(GameObject trafficCar)
    {
        trafficContainer.Free(trafficCar.transform);
    }

    public void BlockNextSpecialTraffic()
    {
        blockSpecialSpawn = true;
    }

}

[System.Serializable]
public class AvailableTraffic
{
    public TrafficTypes type;
    public GameObject[] prefabs;
}

public enum TrafficTypes
{
    Normal,
    Fast,
    Slow,
    Wide,
    Long,
    Static,
    WithHole
}
