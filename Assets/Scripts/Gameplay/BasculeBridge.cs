﻿using UnityEngine;
using System.Collections;

public class BasculeBridge : MonoBehaviour
{
    public bool IsLifting { get; set; }

    [SerializeField]
    private Transform firstPart;
    [SerializeField]
    private float firstPartLiftingSpeed;
    [SerializeField]
    private Transform secondPart;
    [SerializeField]
    private float secondPartLiftingSpeed;


    private Quaternion firstPartRotation;
    private Quaternion secondPartRotation;

    void Awake()
    {
        firstPartRotation = firstPart.rotation;
        secondPartRotation = secondPart.rotation;
    }

    void OnEnable()
    {
        GameplayManager.OnSessionStarted += Restart;
    }

    void OnDisable()
    {
        GameplayManager.OnSessionStarted -= Restart;
    }

    private void Restart()
    {
        firstPart.rotation = firstPartRotation;
        secondPart.rotation = secondPartRotation;
        IsLifting = false;
    }

    public void OnPlayerEnter(BasculeBridgeEnum state)
    {
        switch (state)
        {
            case BasculeBridgeEnum.FirstPartEntry:
                IsLifting = true;
                break;
            case BasculeBridgeEnum.FirstPartExit:
                break;
            case BasculeBridgeEnum.SecondPartEntry:
                break;
            case BasculeBridgeEnum.SecondPartExit:
                IsLifting = false;
                break;
            default:
                break;
        }
    }

    void Update()
    {
        if (IsLifting)
        {
            Vector3 rotationOffset = new Vector3(firstPartLiftingSpeed * Time.deltaTime, 0, 0);
            firstPart.rotation = Quaternion.Euler(firstPart.rotation.eulerAngles - rotationOffset);
            rotationOffset = new Vector3(secondPartLiftingSpeed * Time.deltaTime, 0, 0);
            secondPart.rotation = Quaternion.Euler(secondPart.rotation.eulerAngles - rotationOffset);
        }
    }


}
