﻿using UnityEngine;
using System.Collections;

public class Road : MonoBehaviour
{
    [SerializeField]
    private MeshRenderer mesh;

    private RoadsContainer roadsContainer;
    private bool returnIfIsNotVisible = false;

    void OnDisable()
    {
        returnIfIsNotVisible = false;
    }

    public void ReturnIfIsNotVisible(RoadsContainer roadsContainer)
    {
        if (!mesh.isVisible)
        {
            roadsContainer.Return(this.transform);
        }
        else
        {
            this.transform.parent = null;
            this.roadsContainer = roadsContainer;
            returnIfIsNotVisible = true;
        }
    }

    void Update()
    {
        if (returnIfIsNotVisible)
        {
            if (!mesh.isVisible)
            {
                roadsContainer.Return(this.transform);
            }
        }
    }

}
