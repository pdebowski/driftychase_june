﻿using UnityEngine;
using System.Collections;

public class BlockadeManager : MonoBehaviour
{

    private const float BLOCKADE_ADDITIONAL_OFFSET = 5.0f;
    public const float SIDEWALK_WIDTH = 4f;

    [SerializeField]
    private float blockadeChance;
    [SerializeField]
    private GameObject blockadePrefab;
    [SerializeField]
    private CityArchitect cityArchitect;

    public void Init()
    {
    }

    public GameObject SetBlockadeAt(Vector3 crossroadID, Vector3 direction)
    {
        GameObject blockade;
        blockade = CreateObject();
        SetPosition(blockade, crossroadID, direction);
        SetScale(blockade, crossroadID, direction);
        SetRotation(blockade, direction);

        return blockade;
    }

    private GameObject CreateObject()
    {
        return GameObject.Instantiate(blockadePrefab) as GameObject;
    }

    private void SetPosition(GameObject newBlockade, Vector3 crossroadID, Vector3 direction)
    {
        Vector3 crossroadPosition = cityArchitect.GetCrossroadPosition(crossroadID);
        float offset = 0;
        if (direction == Vector3.forward || direction == Vector3.back)
        {
            offset = cityArchitect.GetRightRoad(crossroadID).width / 2;
        }
        else
        {
            offset = cityArchitect.GetForwardRoad(crossroadID).width / 2;
        }

        Vector3 blockadePosition = crossroadPosition + direction * (offset + BLOCKADE_ADDITIONAL_OFFSET);
        blockadePosition.y = 2;
        newBlockade.transform.position = blockadePosition;
    }

    private void SetScale(GameObject newBlockade, Vector3 crossroadID, Vector3 direction)
    {
        float roadWidth;
        if (direction == Vector3.forward || direction == Vector3.back)
        {
            roadWidth = cityArchitect.GetForwardRoad(crossroadID).width;
        }
        else
        {
            roadWidth = cityArchitect.GetRightRoad(crossroadID).width;
        }
        Vector3 oldScale = newBlockade.transform.localScale;
        newBlockade.transform.localScale = new Vector3(roadWidth + SIDEWALK_WIDTH, oldScale.y, oldScale.z);
        newBlockade.GetComponent<RoadBlockadeAnimation>().SetTiling(newBlockade.transform.localScale.x / oldScale.x);
    }


    private void SetRotation(GameObject newBlockade, Vector3 direction)
    {
        Quaternion newRotataion;

        if (direction == Vector3.left || direction == Vector3.right)
        {
            newRotataion = Quaternion.LookRotation(Vector3.right);
        }
        else
        {
            newRotataion = Quaternion.LookRotation(Vector3.back);
        }
        newBlockade.transform.rotation = Quaternion.Euler(0,180,0) * newRotataion;
    }

}
