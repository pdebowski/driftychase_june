﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Collider))]
public class TutorialTrigger : MonoBehaviour
{

    [SerializeField]
    private Tutorial tutorialManager;
    [SerializeField]
    private TutorialPart tutorialPart;


    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            if (tutorialManager != null)
            {
                tutorialManager.TutorialTrigger(tutorialPart);
            }
            else
            {
                Debug.LogWarning(tutorialPart + " Not working couse TutorialManager reference is null");
            }
        }

    }


}
