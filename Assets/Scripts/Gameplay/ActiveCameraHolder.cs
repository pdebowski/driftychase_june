﻿using UnityEngine;
using System.Collections;

public class ActiveCameraHolder : MonoBehaviour {

    private Camera activeCamera;

    void Start()
    {
        activeCamera = PrefabReferences.Instance.Camera;
    }
    
    void Update()
    {
        Camera renderTextureCamera = PrefabReferences.Instance.RenderTextureCamera;
        Camera gameCamera = PrefabReferences.Instance.Camera;
        activeCamera = renderTextureCamera.isActiveAndEnabled ? renderTextureCamera : gameCamera;
    }

	public Transform GetActiveCamera()
    {
        return activeCamera.transform;
    }
}
