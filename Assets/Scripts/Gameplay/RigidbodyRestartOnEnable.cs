﻿using UnityEngine;
using System.Collections;

public class RigidbodyRestartOnEnable : MonoBehaviour
{

    void OnEnable()
    {
        Rigidbody rb = GetComponent<Rigidbody>();
        rb.drag = 0;
        rb.velocity = Vector3.zero;
        rb.angularDrag = 0;
        rb.angularVelocity = Vector3.zero;
    }
}
