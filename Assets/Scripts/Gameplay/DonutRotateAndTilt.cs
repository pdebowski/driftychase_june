﻿using UnityEngine;
using System.Collections;

public class DonutRotateAndTilt : MonoBehaviour
{
    [SerializeField]
    private float R = 5.5f;
    [SerializeField]
    private Transform holder;
    [SerializeField]
    private bool tilt = true;

    private Rigidbody rigBody;
    private float rotationX = 0;
    private Vector3 lastPosition;

    void Awake()
    {
        rigBody = GetComponentInParent<Rigidbody>();
    }

    void FixedUpdate()
    {
        Vector3 movedOffset = transform.position - lastPosition;
        movedOffset = transform.parent.InverseTransformVector(movedOffset);


        rotationX += movedOffset.z / (2 * Mathf.PI * R) * 360;
        rotationX = rotationX % 360;

        transform.localRotation = Quaternion.Euler(new Vector3(rotationX, 0, 0));
        if (tilt)
        {
            holder.localRotation = Quaternion.Euler(new Vector3(0, 0, movedOffset.x * 2 / Time.deltaTime));
        }

        lastPosition = transform.position;
    }
}
