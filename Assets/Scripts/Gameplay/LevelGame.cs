﻿using UnityEngine;
using System.Collections;

public abstract class LevelGame : MonoBehaviour
{

    public abstract int GetLevelCount();
    public abstract void InitLevel(int index);
    public abstract void BackToNormalMode();
    public abstract bool IsContinueAvailable();
}
