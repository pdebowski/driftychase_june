﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnglishMode : MonoBehaviour
{
    [SerializeField]
    private WorldOrder worldOrder;

    public static System.Action<bool> OnEnglishModeChange = delegate { };


    void OnEnable()
    {
        PlayerSpeedManager.OnWorldChange += CheckWorld;
    }

    void OnDisable()
    {
        PlayerSpeedManager.OnWorldChange -= CheckWorld;
    }

    private void CheckWorld()
    {
        WorldType currentWorld = WorldOrder.CurrentWorld;
        if (currentWorld == WorldType.London)
        {
            TurnOn();
        }
        else
        {
            TurnOff();
        }
    }


    public void TurnOn()
    {
        PlayerPrefsAdapter.EnglishMode = true;
        OnEnglishModeChange(PlayerPrefsAdapter.EnglishMode);
    }

    public void TurnOff()
    {
        PlayerPrefsAdapter.EnglishMode = false;
        OnEnglishModeChange(PlayerPrefsAdapter.EnglishMode);
    }
}
