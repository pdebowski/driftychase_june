using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CityCreator : MonoBehaviour
{
    public const float LANE_WIDTH = 7f;
    public const float ROAD_LENGHT = 35f;
    public const float FORWARD_TRIGGER_OFFSET = 10;

    [SerializeField]
    private int maxCrossroadsCount;
    [SerializeField]
    private ObjectsContainer crossroadsContainer;
    [SerializeField]
    private Transform cityPartsHolder;
    [SerializeField]
    private Transform triggersHolder;
    [SerializeField]
    private CityBuildingsAdjuster cityBuildingsAdjuster;
    [SerializeField]
    private RoadsContainer roadsContainer;
    [SerializeField]
    private CityArchitect cityArchitect;
    [SerializeField]
    private BlockadeManager blockadeManager;
    [SerializeField]
    private PlayerSpeedManager playerSpeedManager;
    [SerializeField]
    private DailyGiftManager dailyGiftManager;
    [SerializeField]
    private Transform nextRoadTrigger;
    [SerializeField]
    private GameObject finishLineObject;

    private RoadInfo LastCrossroadRoadSet { get { return roads[roads.Count - 1]; } set {; } }
    private RoadInfo LeftRoad { get { return cityArchitect.GetRoad(transform.forward, currentCrossroadID); } set {; } }
    private RoadInfo RightRoad { get { return cityArchitect.GetRoad(-transform.forward, currentCrossroadID); } set {; } }
    private RoadInfo ForwardRoad { get { return cityArchitect.GetRoad(-transform.right, currentCrossroadID); } set {; } }
    private RoadInfo BackwardRoad { get { return cityArchitect.GetRoad(transform.right, currentCrossroadID); } set {; } }

    private RoadInfo newRoadSet;
    private bool isFirstCrossroad = true;
    private bool isLeftTurnCrossroad = false;
    private bool spawnTraffic = false;
    private bool isPlayerAtCurrentRoad = true;
    private bool isLevelModeON = false;
    private TurnDirection lastTurnDirection;
    private Vector3 currentCrossroadID;
    private Vector3 lastCrossroadID;
    private Vector3 nextRoadTriggerPosition;
    private Vector3 finishCrossroadID;
    private int currentCrossroadNumber = 0;
    private Quaternion nextRoadTriggerRotation;

    private List<CrossroadInfo> crossroads;
    private List<RoadInfo> roads;

    private Vector3 startPosition;
    private Quaternion startRotation;


    private CrossroadTrafficManager crossroadTrafficManager;

    void Awake()
    {
        roads = new List<RoadInfo>();
        crossroads = new List<CrossroadInfo>();
        startPosition = transform.position;
        startRotation = transform.rotation;
        crossroadTrafficManager = GetComponent<CrossroadTrafficManager>();
        crossroadTrafficManager.Init(ROAD_LENGHT);
    }

    void Start()
    {
        cityArchitect.Init();
        blockadeManager.Init();
        Restart(true);
    }


    void OnEnable()
    {
        GameplayManager.OnSessionEnded += Restart;
        PlayerController.OnTurn += OnTurn;
        NextRoadTrigger.OnNextRoadEnter += OnPlayerNextRoadEnter;
        GameplayManager.OnContinueUsed += OnContinueUsed;
    }

    void OnDisable()
    {
        GameplayManager.OnSessionEnded -= Restart;
        PlayerController.OnTurn -= OnTurn;
        NextRoadTrigger.OnNextRoadEnter -= OnPlayerNextRoadEnter;
        GameplayManager.OnContinueUsed -= OnContinueUsed;
    }

    private void OnPlayerNextRoadEnter()
    {
        if (playerSpeedManager.GetComponent<PlayerController>().Working)
        {
            isPlayerAtCurrentRoad = true;
            SetNextRoadTrigger();
        }
    }

    private void Restart()
    {
        Restart(true);
    }

    public void Restart(bool resetPlayerSpeedManager)
    {
        GameplayRandom.SetNewGenerator();

        if (resetPlayerSpeedManager)
        {
            PrefabReferences.Instance.CheckpointsManager.RecalculateIsCheckpointActive();
            playerSpeedManager.Restart(PrefabReferences.Instance.CheckpointsManager.GetStartWorldIDForSelectedCar());
            CheckpointInstantOffer.WasCheckpointActiveInLastRun = PrefabReferences.Instance.CheckpointsManager.IsCheckpointTimeLeft();
        }

        crossroadTrafficManager.Restart();
        ClearLastGameData();
        finishLineObject.SetActive(false);

        currentCrossroadID = cityArchitect.StartID;
        lastTurnDirection = TurnDirection.Left;
        isFirstCrossroad = true;
        isLeftTurnCrossroad = false;
        isPlayerAtCurrentRoad = true;
        currentCrossroadNumber = 0;

        transform.position = startPosition;
        transform.rotation = startRotation;



        roads.Clear();
        SpawnStart();
        RefreshNextRoadTriggerNextValues();
        ResetNextRoadTrigger();

        cityBuildingsAdjuster.SpawnStartBuildings();
    }

    private void OnContinueUsed()
    {
        isPlayerAtCurrentRoad = true;
        SetNextRoadTrigger();
    }

    private void ResetNextRoadTrigger()
    {
        SetNextRoadTrigger();
    }

    private void ClearLastGameData()
    {
        foreach (var item in crossroads)
        {
            CleanCrossroad(item, false);
        }
        crossroads.Clear();

        cityBuildingsAdjuster.Restart();
        roadsContainer.Restart();

    }

    public void OnTurn(TurnDirection turnDirection)
    {
        if (isPlayerAtCurrentRoad)
        {
            if (isFirstCrossroad)
            {
                lastTurnDirection = turnDirection;
            }
            else
            {
                lastTurnDirection = (lastTurnDirection == TurnDirection.Left ? TurnDirection.Right : TurnDirection.Left);
            }


            TryClearOldCrossroads(turnDirection);
#if !NO_OPTIM
            crossroadTrafficManager.SetPlayerDataToTurn(PrefabReferences.Instance.Player);
            StartCoroutine(OnTurnCor());
#else
        MoveSpawnerToNextCrossroad(lastTurnDirection);

        CreateCrossroad(false);
        SpawnCrossRoadForward(true);
        isPlayerAtCurrentRoad = false;
#endif 
        }
    }

    IEnumerator OnTurnCor()
    {
        yield return 0;
        yield return 0;


        MoveSpawnerToNextCrossroad(lastTurnDirection);



        yield return 0;

        CreateCrossroad(false, true);
        RefreshNextRoadTriggerNextValues();
        yield return 0;
        yield return 0;
        yield return 0;



        StartCoroutine(SpawnCrossRoadForwardCor(true, true));
        isPlayerAtCurrentRoad = false;
    }

    private void RefreshNextRoadTriggerNextValues()
    {
        nextRoadTriggerPosition = CalculateNextRoadTriggerNextPosition();
        nextRoadTriggerRotation = CalculateNextRoadTriggerNextRotation();
    }


    private void SpawnStart()
    {
        roads.Add(cityArchitect.GetStartRoad());
        CreateCrossroad();
        MoveSpawnerToNextCrossroad(TurnDirection.Forward);
        CreateCrossroad();

        SpawnCrossRoadForward(true);


    }

    private void SpawnCrossRoadForward(bool fakeCrossroad = false, bool onTurn = false)
    {
        MoveSpawnerToNextCrossroad(TurnDirection.Forward);
        CreateCrossroad(fakeCrossroad, onTurn);

        MoveSpawnerToNextCrossroad(TurnDirection.Back);
    }

    private IEnumerator SpawnCrossRoadForwardCor(bool fakeCrossroad = false, bool onTurn = false)
    {
        MoveSpawnerToNextCrossroad(TurnDirection.Forward);

        CreateCrossroad(fakeCrossroad, onTurn);
        yield return 0;
        yield return 0;
        yield return 0;
        yield return 0;
        yield return 0;
        MoveSpawnerToNextCrossroad(TurnDirection.Back);

    }

    public void ChangeCrossroadID(TurnDirection turnDirection)
    {
        lastCrossroadID = currentCrossroadID;
        currentCrossroadID = GetNextCrossroadID(turnDirection);
    }

    private Vector3 GetNextCrossroadID(TurnDirection turnDirection)
    {
        Vector3 result = currentCrossroadID;
        if (turnDirection == TurnDirection.Back)
        {
            result += transform.right;
        }
        else if (turnDirection == TurnDirection.Forward)
        {
            result += -transform.right;
        }
        else if (turnDirection == TurnDirection.Right)
        {
            result += -transform.forward;
        }
        else if (turnDirection == TurnDirection.Left)
        {
            result += transform.forward;
        }
        result.x = Mathf.Round(result.x);
        result.z = Mathf.Round(result.z);
        return result;
    }



    private void CreateCrossroad(bool fakeCrossroad = false, bool onTurn = false)
    {
        if (CouldSpawnCrossroadHere())
        {
            if (!fakeCrossroad)
            {
                currentCrossroadNumber++;
                if (lastTurnDirection != TurnDirection.Forward)
                {
                    isLeftTurnCrossroad = IsLeftTurnCrossroadNow();
                }
            }

            newRoadSet = LeftRoad;

            spawnTraffic = true;
            spawnTraffic &= !playerSpeedManager.NextWillBeDifficultyStep();
            spawnTraffic &= cityArchitect.CouldSpawnTrafficAt(currentCrossroadID);
            spawnTraffic &= !Tutorial.IsTutorial;
            spawnTraffic &= !fakeCrossroad;
            spawnTraffic &= !DailyGiftManager.IsDailyGiftActive;
            SpawnRoads(fakeCrossroad, onTurn);
            cityBuildingsAdjuster.SpawnBuildings(currentCrossroadID, onTurn);

            roads.Add(newRoadSet);

            isFirstCrossroad = false;
        }
    }


    private bool CouldSpawnCrossroadHere()
    {
        return crossroads.Find(x => x.direction == -transform.right && x.id == currentCrossroadID) == null;
    }

    private bool IsLeftTurnCrossroadNow()
    {
        isLeftTurnCrossroad = !isLeftTurnCrossroad;
        return isLeftTurnCrossroad;
    }

    private Vector3 CalculateNextRoadTriggerNextPosition()
    {
        Vector3 triggerPosition = transform.position;
        triggerPosition += (isLeftTurnCrossroad == true ? transform.forward : -transform.forward) * (ForwardRoad.width / 2 + BlockadeManager.SIDEWALK_WIDTH);
        triggerPosition += -transform.right * LeftRoad.width / 2;
        return triggerPosition;
    }

    private Quaternion CalculateNextRoadTriggerNextRotation()
    {
        return Quaternion.LookRotation(transform.forward);
    }


    private void SetNextRoadTrigger()
    {
        nextRoadTrigger.position = nextRoadTriggerPosition;
        nextRoadTrigger.rotation = nextRoadTriggerRotation;
    }


    private void TryClearOldCrossroads(TurnDirection turnDirection)
    {
        Vector3 nextCrossroadID = GetNextCrossroadID(turnDirection);
        cityBuildingsAdjuster.RemoveNotNeededBuildings(currentCrossroadID, nextCrossroadID);
        RemoveNotNeededCrossroads(currentCrossroadID, nextCrossroadID);
    }

    private void RemoveNotNeededCrossroads(Vector3 lastCrossroadID, Vector3 nextCrossroadID)
    {
        for (int i = 0; i < crossroads.Count; i++)
        {
            if (!CheckIfIsNeeded(crossroads[i], lastCrossroadID)
                && !CheckIfIsNeeded(crossroads[i], nextCrossroadID))
            {
                CleanCrossroad(crossroads[i], true);
                i--;
            }
        }
    }
    private bool CheckIfIsNeeded(CrossroadInfo crossroad, Vector3 crossroadID)
    {
        if (crossroadID == crossroad.id)
        {
            return true;
        }

        if (AreCrossroadsColliding(crossroad, crossroadID))
        {
            foreach (var item in crossroads)
            {
                if (item.id == crossroadID && AreCrossroadsColliding(item, crossroad.id))
                {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    private bool AreCrossroadsColliding(CrossroadInfo first, Vector3 crossroadID)
    {
        return first.id + first.direction == crossroadID
            || first.id + Quaternion.Euler(0, -90, 0) * first.direction == crossroadID
            || first.id + Quaternion.Euler(0, 90, 0) * first.direction == crossroadID;
    }

    private void DestroyNewestCrossroad()
    {
        CleanCrossroad(crossroads[crossroads.Count - 1], true);
    }

    private void CleanCrossroad(CrossroadInfo crossroad, bool removeFromList)
    {
        crossroadTrafficManager.ClearTrafficAt(crossroad.gameObject.transform);
        ClearRoadsAtCrossroad(crossroad.gameObject.transform);
        ClearCrossroadObject(crossroad.gameObject.transform);

        Destroy(crossroad.gameObject);
        if (removeFromList)
        {
            crossroads.Remove(crossroad);
        }
    }

    private void ClearRoadsAtCrossroad(Transform crossroad)
    {
        Road[] roadsAtCrossroad = crossroad.GetComponentsInChildren<Road>(true);
        foreach (var item in roadsAtCrossroad)
        {

            RoadTrafficSpawner[] spawners = item.GetComponentsInChildren<RoadTrafficSpawner>(true);
            foreach (var spawner in spawners)
            {
                spawner.Blocked = true;
                Destroy(spawner);
            }
            item.ReturnIfIsNotVisible(roadsContainer);
        }
    }

    private void ClearCrossroadObject(Transform crossroad)
    {
        Crossroad crossroadObject = crossroad.GetComponentInChildren<Crossroad>(true);
        if (crossroadObject != null)
        {
            crossroadObject.ReturnIfIsNotVisible(crossroadsContainer);
        }
    }



    private void SpawnRoads(bool fakeCrossroad = false, bool onTurn = false)
    {
#if NO_OPTIM
        onTurn = false;
#endif
        if (onTurn)
        {
            StartCoroutine(SpawnRoadsCor(fakeCrossroad));
        }
        else
        {
            GameObject crossroadHolder = CreateNewCrossroadHolder();

            SpawnForwardRoad(crossroadHolder.transform);
            if (!isFirstCrossroad)
            {
                SpawnRight_LeftRoads(crossroadHolder.transform, fakeCrossroad);
            }
            SpawnCrossroad(crossroadHolder);

            if (!fakeCrossroad)
            {
                SpawnBlockades(crossroadHolder.transform);
            }


            if (spawnTraffic)
            {
                crossroadTrafficManager.SetAsCurrentCrossroad(crossroadHolder, isFirstCrossroad, isLeftTurnCrossroad);
            }
        }
        //   return crossroadHolder;
    }

    IEnumerator SpawnRoadsCor(bool fakeCrossroad = false)
    {
        GameObject crossroadHolder = CreateNewCrossroadHolder();

        SpawnForwardRoad(crossroadHolder.transform);
        yield return 0;
        if (!isFirstCrossroad)
        {
            SpawnRight_LeftRoads(crossroadHolder.transform, fakeCrossroad);
        }
        yield return 0;
        SpawnCrossroad(crossroadHolder);

        if (!fakeCrossroad)
        {
            yield return 0;
            SpawnBlockades(crossroadHolder.transform);
        }


        if (spawnTraffic)
        {
            yield return 0;
            crossroadTrafficManager.SetAsCurrentCrossroad(crossroadHolder, isFirstCrossroad, isLeftTurnCrossroad);
        }
    }



    private GameObject CreateNewCrossroadHolder()
    {
        GameObject crossroadHolder = new GameObject("Crossroad");
        crossroadHolder.transform.position = this.transform.position;
        crossroadHolder.transform.rotation = Quaternion.identity;
        crossroadHolder.transform.SetParent(cityPartsHolder);

        CrossroadInfo newCrossroad = new CrossroadInfo();
        newCrossroad.direction = -transform.right;
        newCrossroad.gameObject = crossroadHolder;
        newCrossroad.id = currentCrossroadID;
        crossroads.Add(newCrossroad);

        return crossroadHolder;
    }

    private void SpawnForwardRoad(Transform crossroadHolder)
    {
        RoadInfo road = ForwardRoad;
        GameObject newRoad = roadsContainer.GetRoadObject(road) as GameObject;
        newRoad.transform.position = transform.position;
        newRoad.transform.position -= transform.right.WithIntValues() * (LeftRoad.width + road.length / 2);
        newRoad.SetActive(true);
        newRoad.transform.rotation = GetForwardRoadRotation();
        newRoad.transform.parent = crossroadHolder;
    }

    private void SpawnRight_LeftRoads(Transform crossroadHolder, bool fakeCrossroad)
    {
        GameObject newRoadLeft = null;
        GameObject newRoadRight = null;

        newRoadLeft = SpawnRight_LeftRoad(LeftRoad, crossroadHolder, true, fakeCrossroad);

        newRoadRight = SpawnRight_LeftRoad(RightRoad, crossroadHolder, false, fakeCrossroad);



        if (spawnTraffic)
        {
            AddTrafficSpawnersToLeft_RightRoads(newRoadLeft, newRoadRight, newRoadSet);
        }
    }

    private GameObject SpawnRight_LeftRoad(RoadInfo newRoadSet, Transform crossroadHolder, bool isLeft, bool fakeCrossroad)
    {
        GameObject newRoad = roadsContainer.GetRoadObject(newRoadSet) as GameObject;
        newRoad.transform.position = transform.position;
        newRoad.transform.rotation = Quaternion.identity;
        newRoad.transform.position += (isLeft ? 1 : -1) * transform.forward.WithIntValues() * (ForwardRoad.width / 2 + newRoadSet.length / 2);
        newRoad.transform.position -= transform.right.WithIntValues() * (newRoadSet.width / 2);
        newRoad.SetActive(true);
        newRoad.transform.rotation = GetLeft_RightRoadRotation();

        newRoad.transform.parent = crossroadHolder;

        if (!fakeCrossroad && !isFirstCrossroad && ((isLeftTurnCrossroad && isLeft) || (!isLeftTurnCrossroad && !isLeft)))
        {
            newRoad.GetComponentInChildren<CoinSpawner>().SpawnNormalCoins();
            if (isLevelModeON && IsFinishCrossroad())
            {
                finishLineObject.transform.position = newRoad.transform.position;
                finishLineObject.transform.rotation = newRoad.transform.rotation;
                finishLineObject.transform.position -= finishLineObject.transform.forward * 10;
                finishLineObject.SetActive(true);
            }
        }



        return newRoad;
    }

    private bool IsFinishCrossroad()
    {
        return currentCrossroadID.x == finishCrossroadID.x && currentCrossroadID.z == finishCrossroadID.z;
    }

    private void SpawnCrossroad(GameObject crossroadHolder)
    {
        GameObject crossroad = crossroadsContainer.GetNextObject().gameObject;
        crossroad.SetActive(true);
        crossroad.transform.localScale = cityArchitect.GetCrossroadSize(currentCrossroadID);
        crossroad.transform.position = transform.position + (-transform.right.WithIntValues()) * LeftRoad.width / 2.0f + new Vector3(0, 0.01f, 0);
        crossroad.transform.SetParent(crossroadHolder.transform);
    }

    private void SpawnBlockades(Transform crossroadHolder)
    {
        if (isFirstCrossroad)
        {
            return;
        }

        GameObject blockade = blockadeManager.SetBlockadeAt(currentCrossroadID, -transform.right);
        blockade.transform.SetParent(crossroadHolder);

        float directionSign = isLeftTurnCrossroad ? -1 : 1;

        blockade = blockadeManager.SetBlockadeAt(currentCrossroadID, directionSign * transform.forward);
        blockade.transform.SetParent(crossroadHolder);
    }

    private void AddTrafficSpawnersToLeft_RightRoads(GameObject leftRoad, GameObject rightRoad, RoadInfo newRoadSet)
    {
        bool isNormalRotation = IsNormalRotation();

        if (isLeftTurnCrossroad)
        {
            if (isNormalRotation)
            {
                AddTrafficSpawners<OppositeTrafficSpawner>(leftRoad, newRoadSet, true);
                AddTrafficSpawners<BehindTrafficSpawner>(rightRoad, newRoadSet, false);
            }
            else
            {
                AddTrafficSpawners<BehindTrafficSpawner>(rightRoad, newRoadSet, true);
                AddTrafficSpawners<OppositeTrafficSpawner>(leftRoad, newRoadSet, false);
            }

        }
        else
        {
            if (isNormalRotation)
            {
                AddTrafficSpawners<OppositeTrafficSpawner>(rightRoad, newRoadSet, true);
                AddTrafficSpawners<BehindTrafficSpawner>(leftRoad, newRoadSet, false);
            }
            else
            {
                AddTrafficSpawners<BehindTrafficSpawner>(leftRoad, newRoadSet, false);
                AddTrafficSpawners<OppositeTrafficSpawner>(rightRoad, newRoadSet, true);
            }
        }
    }

    private Quaternion GetForwardRoadRotation()
    {
        if (transform.forward == Vector3.left || transform.forward == Vector3.right)
        {
            return Quaternion.LookRotation(Vector3.forward);
        }
        else
        {
            return Quaternion.LookRotation(Vector3.right);
        }
    }

    private Quaternion GetLeft_RightRoadRotation()
    {
        if (transform.forward == Vector3.left || transform.forward == Vector3.right)
        {
            return Quaternion.LookRotation(Vector3.right);
        }
        else
        {
            return Quaternion.LookRotation(Vector3.forward);
        }
    }


    private float GetRotationSign()
    {
        return (transform.forward == Vector3.forward || transform.forward == Vector3.left) ? -1 : 1;
    }

    private bool IsNormalRotation()
    {
        return GetRotationSign() == -1 ? true : false;
    }

    public Vector3 GetPlayerPositionToContinue()
    {
        return transform.position + transform.right * BackwardRoad.length;
    }

    public Quaternion GetPlayerRotationToContinue()
    {
        return Quaternion.LookRotation(-transform.right);
    }


    private void MoveSpawnerToNextCrossroad(TurnDirection direction)
    {
        ChangeCrossroadID(direction);

        int sign = 1;
        float forwardOffset = 0;
        float rightOffset = 0;
        float angle = 0;

        switch (direction)
        {
            case TurnDirection.Left:
            case TurnDirection.Right:
                bool wasRightTurn = direction == TurnDirection.Right;
                sign = wasRightTurn == true ? -1 : 1;
                rightOffset = -(cityArchitect.GetRoad(transform.forward, lastCrossroadID).width / 2);
                forwardOffset = sign * (cityArchitect.GetRoad(transform.right, lastCrossroadID).width / 2 + cityArchitect.GetRoad(sign * transform.forward, lastCrossroadID).length);
                angle = wasRightTurn ? 90 : -90;
                break;
            case TurnDirection.Forward:
            case TurnDirection.Back:
                sign = direction == TurnDirection.Forward ? -1 : 1;
                float horizontalRoadLenght = cityArchitect.GetRoad(direction == TurnDirection.Forward ? transform.right : -transform.right, currentCrossroadID).length;
                float verticalRoadWidth = cityArchitect.GetRoad(transform.forward, direction == TurnDirection.Forward ? lastCrossroadID : currentCrossroadID).width;
                rightOffset = sign * (horizontalRoadLenght + verticalRoadWidth);
                forwardOffset = 0;
                angle = 0;
                break;
            default:
                break;
        }
        transform.position += transform.forward.WithIntValues() * forwardOffset;
        transform.position += transform.right.WithIntValues() * rightOffset;
        transform.Rotate(transform.up.WithIntValues(), angle);

        if (triggersHolder != null)
        {
            triggersHolder.position = transform.position + (-transform.right) * (cityArchitect.GetRoad(transform.forward, currentCrossroadID).width + ROAD_LENGHT / 2 + FORWARD_TRIGGER_OFFSET);
        }

    }

    private void AddTrafficSpawners<T>(GameObject road, RoadInfo roadInfo, bool atOppositeLanes) where T : MonoBehaviour
    {
        if ((road == null) || (typeof(T) == typeof(OppositeTrafficSpawner) && ScoreCounter.Instance.CurrentScore < 6))
        {
            return;
        }

        TrafficSpawnerPoint[] points = GetSortedSpawnPoints(road);
        int from = 0;
        int to = 0;
        if (atOppositeLanes)
        {
            from = 0;
            to = roadInfo.oppositeLanes;
        }
        else
        {
            from = roadInfo.oppositeLanes;
            to = from + roadInfo.myLanes;
        }

        AddComponentToArray<T>(points, from, to);
    }

    private void AddComponentToArray<T>(TrafficSpawnerPoint[] array, int from, int to) where T : MonoBehaviour
    {
        for (int i = from; i < to; i++)
        {
            array[i].gameObject.AddComponent<T>();
        }
    }

    private TrafficSpawnerPoint[] GetSortedSpawnPoints(GameObject road)
    {
        TrafficSpawnerPoint[] points = road.GetComponentsInChildren<TrafficSpawnerPoint>(true);
        System.Array.Sort(points);
        return points;
    }

    public void TurnOnLevelMode(Vector2 levelFinishCrossroadID)
    {
        isLevelModeON = true;
        this.finishCrossroadID = new Vector3(levelFinishCrossroadID.x, 0, levelFinishCrossroadID.y);
    }

    public void TurnOnNormalMode()
    {
        finishLineObject.SetActive(false);
        isLevelModeON = false;
    }
}

[System.Serializable]
public class CrossroadInfo
{
    public Vector3 direction;
    public Vector3 id;
    public GameObject gameObject;
}



[System.Serializable]
public struct RoadInfo
{
    public int myLanes;
    public int oppositeLanes;
    public float width;
    public float length;
    public GameObject prefab;
}