using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace CrimsonAnalyticsFeatures
{
    public class CountryCPM
    {
        public string Country { get; set; }
        public float CPM { get; set; }
    }

    public enum CPMAdTypes
    {
        Interstitial,
        RewardedVideo,
        Banner
    }


    public class ECommerceTracker : MonoBehaviour
    {
        public static float CPM_DEFAULT_VALUE = 0.00f;

    // /   [SerializeField]
      // /  private CrimsonAds.AdFacade adFacade;

        string staticProvider = null;
        string videoProvider = null;
        string bannerProvider = null;
        private List<CountryCPM> countriesCPMVideo;
        private List<CountryCPM> countriesCPMFullscreen;
        private List<CountryCPM> countriesCPMBanner;

        void Start()
        {
          //  CPMParser cmpParser = new CPMParser();
           /*
            // adFacade.OnRewardedAdLoaded += (provider) =>
           {
                string provider = "UnityAds";
               countriesCMPVideo = cmpParser.ParseCountryCPMPairs(GTMParameters.GetCountriesCPM(provider, CPMAdTypes.RewardedVideo));
           };
            //adFacade.OnStaticAdLoaded += (provider) =>
            {
                string provider = "Heyzap";
                countriesCMPFullscreen = cmpParser.ParseCountryCPMPairs(GTMParameters.GetCountriesCPM(provider, CPMAdTypes.Interstitial));
            };
            */
            //not in use in this project
            /*adFacade.OnBannerAdLoaded += (provider) =>
            {
                countriesCMPBanner = cmpParser.ParseCountryCPMPairs(GTMParameters.GetCountriesCPM(provider, CPMAdTypes.Banner));
            };*/
            
        }

        public CountryCPM GetCMPCountryPair(CPMAdTypes type)
        {
            return default(CountryCPM);
            /*
            switch (type)
            {
                case CPMAdTypes.Interstitial:
                    string newProvider = adFacade.GetStaticAdNetworkName() ?? "";
                    if (countriesCPMFullscreen==null || !staticProvider.Equals(newProvider))
                    {
                        staticProvider = newProvider;
         // /               countriesCPMFullscreen = new CPMParser().ParseCountryCPMPairs(GTMParameters.GetCountriesCPM(newProvider, CPMAdTypes.Interstitial)).Cast<CountryCPM>().ToList();
                    }
                    return GetCMPCountryPair(countriesCPMFullscreen);
                case CPMAdTypes.RewardedVideo:
                    string newProviderVideo = adFacade.GetVideoAdNetworkName() ?? "";
                    if (countriesCPMVideo==null || !videoProvider.Equals(newProviderVideo))
                    {
                        videoProvider = newProviderVideo;
        // /                countriesCPMVideo = new CPMParser().ParseCountryCPMPairs(GTMParameters.GetCountriesCPM(newProviderVideo, CPMAdTypes.RewardedVideo)).Cast<CountryCPM>().ToList();
                    }
                    return GetCMPCountryPair(countriesCPMVideo);
                case CPMAdTypes.Banner:
                    Debug.LogError("Banner not implemented");
                    return GetCMPCountryPair(countriesCPMBanner);
                default:
                    return null;
            }
            */
        }

        private CountryCPM GetCMPCountryPair(List<CountryCPM> CMPs)
        {
            string country = PlayerPrefsAdapter.Country.ToUpper();
            if (CMPs.Any(x => x.Country.ToUpper() == country) == false)
            {
                if (CMPs.Any(x => x.Country.ToUpper() == UrlCountryChecker.DEFAULT_COUNTRY.ToUpper()) == true)
                {
                    country = UrlCountryChecker.DEFAULT_COUNTRY.ToUpper();
                }
                else
                {
                    CountryCPM defaultCPM = new CountryCPM
                    {
                        Country = UrlCountryChecker.DEFAULT_COUNTRY,
                        CPM = CPM_DEFAULT_VALUE
                    };
                    return defaultCPM;
                }
            }

            CountryCPM countryCPMPair = CMPs.First(x => x.Country.ToUpper() == country);
            return countryCPMPair;
        }

        //Debug methods

        public void OnButtonClick()
        {
            //CrimsonAnalytics.Ad.Interstitial.LogTransaction();
        }
        public void OnButtonClick2()
        {
            //CrimsonAnalytics.Ad.RewardedVideo.Continue.LogTransaction();
        }
        public void OnButtonClick3()
        {
            //CrimsonAnalytics.Ad.RewardedVideo.VideoGift.LogTransaction();
        }
        public void OnButtonClick4()
        {
            //CrimsonAnalytics.Ad.RewardedVideo.VideoPrizeMachine.LogTransaction();
        }
        public void OnButtonClick5()
        {
            //CrimsonAnalytics.Ad.Banner.LogTransaction();
        }
        public void OnButtonClick6()
        {
 /*/ /*           StoreTransactionInfo s = new StoreTransactionInfo();
            s.productIdentifier = "iap.buddies.avatar04";
            s.transactionIdentifier = "test.test.test";
#if UNITY_ANDROID
            GoogleProductTemplate productTemplate = AndroidInAppPurchaseManager.Instance.Inventory.GetProductDetails(s.productIdentifier);
            string currencyCode = productTemplate.PriceCurrencyCode;
#else
            IOSProductTemplate productTemplate =  IOSInAppPurchaseManager.Instance.GetProductById(s.productIdentifier);
            string currencyCode = productTemplate.CurrencyCode;
#endif

            float price = productTemplate.Price;

            Debug.Log(currencyCode + " " + price);

            CrimsonAnalytics.IAP.Android.Avatar.LogTransaction(s, price, currencyCode);*/
        }
    }
}
