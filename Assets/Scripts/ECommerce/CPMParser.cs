﻿using UnityEngine;
using System.Collections;


namespace CrimsonAnalyticsFeatures
{
    public class CPMParser
    {
        private static int COUNTRY_CODE_LENGHT = 2;

        public ArrayList ParseCountryCPMPairs(string stringToParse)
        {

            if (string.IsNullOrEmpty(stringToParse))
            {
                return Default();
            }

            ArrayList countryCPMs = new ArrayList();

            try
            {
                string[] countryCPMPairs = null;
                countryCPMPairs = stringToParse.Split(';');

                foreach (var item in countryCPMPairs)
                {
                    string[] countryCPMPair = item.Split('-');
                    CountryCPM currentCPM = new CountryCPM();

                    currentCPM.Country = countryCPMPair[0].ToUpper();

                    float value = 0;
                    if (countryCPMPair.Length > 1 && float.TryParse(countryCPMPair[1], out value))
                    {
                        currentCPM.CPM = value;

                        if (currentCPM.Country.Length == COUNTRY_CODE_LENGHT || currentCPM.Country.ToUpper() == UrlCountryChecker.DEFAULT_COUNTRY.ToUpper())
                        {
                            countryCPMs.Add(currentCPM);
                        }

                    }

                }
            }
            catch (System.Exception e)
            {
                return Default();
            };

            return countryCPMs;
        }

        private ArrayList Default()
        {
            ArrayList countryCPMs = new ArrayList();

            CountryCPM countryCPM = new CountryCPM();
            countryCPM = new CountryCPM();
            countryCPM.Country = UrlCountryChecker.DEFAULT_COUNTRY.ToUpper();
            countryCPM.CPM = ECommerceTracker.CPM_DEFAULT_VALUE;

            countryCPMs.Add(countryCPM);

            return countryCPMs;
        }
    }
}
