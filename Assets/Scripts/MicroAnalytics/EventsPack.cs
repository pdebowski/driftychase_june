﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SimpleJSON;
using UnityEngine;

namespace CrimsonAnalytics.MicroAnalytics
{
    class EventsPack
    {
        private static string USER_ID_KEYWORD = "userId";
        private static string OS_KEYWORD = "operatingSystem";
        private static string GAME_ID_KEYWORD = "gameId";
        private static string APP_VERSION_KEYWORD = "appVersion";
        private static string SESSION_ID = "sessionId";
        private static string GUID_KEYWORD = "eventPackId";
        private static string EVENTS_PACK_KEYWORD = "eventPack";
        private static string EVENTS_KEYWORD = "events";
        private static string VARIANTS_KEYWORD = "variants";
        //--Custom------//


        public string UserID { get { return rootNode[USER_ID_KEYWORD]; } }
        public string GameId { get { return rootNode[GAME_ID_KEYWORD]; } }
        public string AppVersion { get { return rootNode[APP_VERSION_KEYWORD]; } }
        public string SessionId { get { return rootNode[SESSION_ID]; } }

        private JSONNode rootNode = new JSONClass();

        public EventsPack(string jsonData)
        {
            JSONNode node = JSONNode.Parse(jsonData);
            if (node != null)
            {
                rootNode = node;
            }
        }

        public EventsPack(string userId, string gameId, string appVersion, string sessionId)
        {
            rootNode[USER_ID_KEYWORD] = userId;
            rootNode[GAME_ID_KEYWORD] = gameId;
            rootNode[APP_VERSION_KEYWORD] = appVersion;
            rootNode[SESSION_ID] = sessionId;
            rootNode[GUID_KEYWORD] = Guid.NewGuid().ToString();

#if UNITY_ANDROID
            rootNode[EVENTS_PACK_KEYWORD][OS_KEYWORD] = "Android";
#elif UNITY_IOS
            if (PlatformRecognition.IsTV)
            {
                rootNode[EVENTS_PACK_KEYWORD][OS_KEYWORD] = "AppleTV";
            }
            else
            {
                rootNode[EVENTS_PACK_KEYWORD][OS_KEYWORD] = "iOS";
            }
#endif
            //--Custom---//

        }

        public string GetJSON()
        {
            return rootNode.ToString();
        }

        public JSONNode GetRoot()
        {
            return rootNode;
        }

        public int Length { get { return rootNode[EVENTS_PACK_KEYWORD][EVENTS_KEYWORD].AsArray.Count; } }

        public Event CreateEvent()
        {
            Event e = new Event();
            rootNode[EVENTS_PACK_KEYWORD][EVENTS_KEYWORD].AsArray.Add(e);
            return e;
        }
    }
}
