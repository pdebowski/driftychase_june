﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SimpleJSON;

namespace CrimsonAnalytics.MicroAnalytics
{
    class Event : JSONClass
    {
        public Event()
        {
            this["eventTime"] = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ss.fffffffZ");
        }

        public Event Add(string key, object value)
        {
            if(value.GetType() == typeof(bool))
            {
                this[key] = (bool)value ? "true" : "false";
            }
            else
            {
                this[key] = value.ToString();
            }

            return this;
        }
    }
}
