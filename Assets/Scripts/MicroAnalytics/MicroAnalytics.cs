﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SimpleJSON;
using UnityEngine;

namespace CrimsonAnalytics.MicroAnalytics
{
    class MicroAnalytics : Singleton<MicroAnalytics>
    {
        [SerializeField]
        private bool isDebugMode = false;

        private static string FIRST_PACK_ID = "MICROANALYTICS_FIRST_PACK_ID";
        private static string CURRENT_PACK_ID = "MICROANALYTICS_CURRENT_PACK_ID";
        private static string PACK_CONTENTS_PREFIX = "MICROANALYTICS_PACK_";
        private static string USER_ID_KEY = "MICROANALYTICS_USER_ID";

        private int firstPackId = 1;
        private int currentPackId = 0;

        private EventsPack currentEventsPack;
        private int eventsBeforeSavingToDisk = 3;
        private DateTime lastTimeEventsSent;

        private string userID;
        private string gameBundleID;
        private string appVersion;
        private string sessionID;

        override protected void Awakened()
        {
            firstPackId = PlayerPrefs.GetInt(FIRST_PACK_ID, 1);
            currentPackId = PlayerPrefs.GetInt(CURRENT_PACK_ID, 0);
            gameBundleID = Application.identifier;
            appVersion = Application.version;
            sessionID = Guid.NewGuid().ToString();

            lastTimeEventsSent = DateTime.UtcNow;

#if UNITY_EDITOR
            userID = isDebugMode ? "EDITOR_TEST_USER" : PlayerPrefs.GetString(USER_ID_KEY, "");
#elif UNITY_ANDROID || UNITY_IOS
            userID = isDebugMode ? "ANDROID_TEST_USER" : PlayerPrefs.GetString(USER_ID_KEY, "");
#endif
            if (userID.Equals("") || userID == null)
            {
                userID = CommonMethods.GetUserID();
                PlayerPrefs.SetString(USER_ID_KEY, userID);
            }

            CreateNewEventsPack();
        }

        private void Update()
        {
            if (IsTimeToPushEvents() || currentEventsPack.SessionId != sessionID)
            {
                PushAllEvents();
            }
        }

        private bool IsTimeToPushEvents()
        {
            return (DateTime.UtcNow - lastTimeEventsSent).TotalSeconds >= GTMParameters.MicroAnalyticsSendTimeSpan;
        }

        private void CreateNewEventsPack()
        {
            lastTimeEventsSent = DateTime.UtcNow;
            currentEventsPack = new EventsPack(userID, gameBundleID, appVersion, sessionID);
            ++currentPackId;
            PlayerPrefs.SetInt(CURRENT_PACK_ID, currentPackId);
            Save();
        }

        private static Event CreateEvent(string category, string action, string label)
        {
            Event e = Instance.CreateEventImpl();
            e.Add("category", category);
            e.Add("action", action);
            e.Add("label", label);
            return e;
        }

        private static Event CreateEvent(string category, string action, string label, Dictionary<string, object> dictionary)
        {
            Event e = CreateEvent(category, action, label);
            if(dictionary != null)
            {
                foreach (var obj in dictionary)
                {
                    e.Add(obj.Key, obj.Value);
                }
            }

            return e; 
        }

        private static Event CreateEvent(string category, string action, string label, float value, Dictionary<string, object> dictionary)
        {
            Event e = CreateEvent(category, action, label, dictionary);
            e.Add("value", value);
            return e;
        }

        public static void LogEvent(string category, string action, string label, Dictionary<string, object> dictionary = null)
        {
            CreateEvent(category, action, label, dictionary);
            Instance.Save();
        }

        public static void LogEvent(string category, string action, string label, float value, Dictionary<string, object> dictionary = null)
        {
            CreateEvent(category, action, label, value, dictionary);
            Instance.Save();
        }

        public static void LogEvent(string category, string action, Dictionary<string, object> dictionary = null)
        {
            CreateEvent(category, action, dictionary);
            Instance.Save();
        }

        private static Event CreateEvent(string category, string action, Dictionary<string, object> dictionary)
        {
            Event e = CreateEvent(category, action);
            if (dictionary != null)
            {
                foreach (var obj in dictionary)
                {
                    e.Add(obj.Key, obj.Value);
                }
            }
            return e;
        }

        private static Event CreateEvent(string category, string action)
        {
            Event e = Instance.CreateEventImpl();
            e.Add("category", category);
            e.Add("action", action);
            return e;
        }

        public static void PushAllEvents()
        {
            if (Instance.currentEventsPack != null && Instance.currentEventsPack.Length > 0)
            {
                Instance.CreateNewEventsPack();
            }
        }

        private Event CreateEventImpl()
        {
            return currentEventsPack.CreateEvent();
        }

        public void Save()
        {
            PlayerPrefs.SetString(PACK_CONTENTS_PREFIX + currentPackId, currentEventsPack.GetJSON());
        }

        public EventsPack GetPackToUpload()
        {
            if (firstPackId < currentPackId)
            {
                string jsonData = PlayerPrefs.GetString(PACK_CONTENTS_PREFIX + firstPackId, "");
                EventsPack packToUpload = new EventsPack(jsonData);
                if(packToUpload.Length > 0)
                {
                    return packToUpload;
                }
                else
                {   
                    ++firstPackId;
                    PlayerPrefs.SetInt(FIRST_PACK_ID, firstPackId);
                    PlayerPrefs.DeleteKey(PACK_CONTENTS_PREFIX + (firstPackId - 1));
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        public int GetNumberOfWaitingPacks()
        {
            return currentPackId - firstPackId;
        }

        public void ConfirmPackUploaded()
        {
            if (firstPackId < currentPackId)
            {
                string jsonData = PlayerPrefs.GetString(PACK_CONTENTS_PREFIX + firstPackId, "");
                Debug.Log(jsonData);
                ++firstPackId;
                PlayerPrefs.SetInt(FIRST_PACK_ID, firstPackId);
                PlayerPrefs.DeleteKey(PACK_CONTENTS_PREFIX + (firstPackId - 1));
            }
            else
            {
                // WTF
            }
        }
    }
}
