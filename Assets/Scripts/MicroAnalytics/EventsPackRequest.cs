﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SimpleJSON;

namespace CrimsonAnalytics.MicroAnalytics
{
    class EventsPackRequest
    {
        private JSONNode rootNode = new JSONClass();

        private static string SENT_TIME_KEYWORD = "sentTime";
        private static string EVENTS_PACK_KEYWORD = "eventPack";

        public EventsPackRequest(EventsPack eventsPack)
        {
            rootNode = eventsPack.GetRoot();
            rootNode[SENT_TIME_KEYWORD] = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ss.fffffffZ");
        }

        public string GetJSON()
        {
            return rootNode.ToString();
        }
    }
}
