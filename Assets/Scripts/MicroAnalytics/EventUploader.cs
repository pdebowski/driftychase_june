﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;

namespace CrimsonAnalytics.MicroAnalytics
{
    class EventUploader : MonoBehaviour
    {
        private MicroAnalytics microAnalytics;
        private EventsPack packCurrentlyUploaded = null;
        private UnityWebRequest requestInProgress;
        private float uploadStartTime;
        private float TIMEOUT = 60;

        private static long RESPONSE_OK = 200;
        private static long RESPONSE_BAD_REQUEST = 400;

        private string analyticsUrl = "https://microanalytics.cfapps.io/events";
        private bool isEnabled = true;

        void Awake()
        {
            isEnabled = GTMParameters.IsMicroAnalyticsEnabled;
            analyticsUrl = GTMParameters.MicroAnalyticsURL;
#if !UNITY_TVOS
            microAnalytics = transform.GetComponent<MicroAnalytics>();
            StartCoroutine(UploadMicroAnalyticsPack());
#endif
        }

        IEnumerator UploadMicroAnalyticsPack()
        {
            int serverRetryWait = 10;

            while (true)
            {
                packCurrentlyUploaded = microAnalytics.GetPackToUpload();
                if (packCurrentlyUploaded != null)
                {
                    if (!Application.isEditor && isEnabled && microAnalytics.GetNumberOfWaitingPacks() < GTMParameters.MicroAnalyticsPacksLimit)
                    {
                        EventsPackRequest request = new EventsPackRequest(packCurrentlyUploaded);
                        string postData = request.GetJSON();
                        using (UnityWebRequest www = UnityWebRequest.Put(analyticsUrl, postData))
                        {
                            www.SetRequestHeader("Content-Type", "application/json");

                            requestInProgress = www;
                            uploadStartTime = Time.time;
                            yield return www.Send();
                            requestInProgress = null;

                            bool isError = www.isError || www.responseCode != (long)HttpStatusCode.OK;

                            if (isError)
                            {
                                if (www.responseCode == (long)HttpStatusCode.BadRequest)
                                {
                                    Debug.LogWarning("MicroAnalytics packet rejected");
                                    isError = false;
                                }
                                else if (www.responseCode == (long)HttpStatusCode.Conflict)
                                {
                                    Debug.LogWarning("MicroAnalytics packet duplicated");
                                    isError = false;
                                }
                            }

                            if (isError)
                            {
                                Debug.Log(www.error);
                                yield return new WaitForSeconds(serverRetryWait);
                                serverRetryWait *= 2;
                            }
                            else
                            {
                                microAnalytics.ConfirmPackUploaded();
                                serverRetryWait = 10;
                            }
                        }
                    }
                    else
                    {
                        microAnalytics.ConfirmPackUploaded();
                    }
                }

                yield return new WaitForSeconds(1);
            }
        }

        void FixedUpdate()
        {
            if (requestInProgress != null && Time.time > uploadStartTime + TIMEOUT)
            {
                requestInProgress.Abort();
            }
        }
    }
}
