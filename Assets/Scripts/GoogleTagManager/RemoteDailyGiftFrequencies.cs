﻿using UnityEngine;
using System.Collections;

public class RemoteDailyGiftFrequencies {

	private float[] frequencies;

	public RemoteDailyGiftFrequencies()
	{
		string remoteData = GoogleTagManagerPlugin.Instance.GetString("DailyGiftFrequencies");
		if (remoteData.Length > 0)
		{
			string[] frequenciesData = remoteData.Split(';');
			frequencies = new float[frequenciesData.Length];

			int frequencyId = 0;
			foreach(string frequency in frequenciesData)
			{
				frequencies[frequencyId] = float.Parse(frequency);
				++frequencyId;
			}
		}
		else
		{
			CrimsonError.LogError("City Stats DB parse error, remote string empty");
		}
	}

	public float[] GetFrequencies()
	{
		return frequencies;
	}
}
