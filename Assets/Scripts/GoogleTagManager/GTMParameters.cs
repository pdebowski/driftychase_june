using UnityEngine;
using System.Collections;
using System;
using System.Linq;




public class GTMParameters
{
    public static string CrossPromoLink { get { return GoogleTagManagerPlugin.Instance.GetString("CrossPromoLink"); } }

    ///----------------SYSTEM-------------------------/// 
    /// 

    public static string ExperimentKey { get { return GoogleTagManagerPlugin.Instance.GetString("expKey"); } }
    public static int ContainerVersion { get { return GoogleTagManagerPlugin.Instance.GetInt("Container Version"); } }

    ///---------------------VARIABLES-----------------////////
    public static bool IsMicroAnalyticsEnabled { get { return GoogleTagManagerPlugin.Instance.GetBool("MA_Enable"); } }
    public static int MicroAnalyticsSendTimeSpan { get { return GoogleTagManagerPlugin.Instance.GetInt("MA_SendTimeSpan"); } }
    public static int MicroAnalyticsPacksLimit { get { return GoogleTagManagerPlugin.Instance.GetInt("MA_PacksLimit"); } }
    public static string MicroAnalyticsURL { get { return GoogleTagManagerPlugin.Instance.GetString("MA_URL"); } }

    /// 
    /// 
    /// 
    public static int AdTimeAfterGameInstalled { get { return GoogleTagManagerPlugin.Instance.GetInt("AdTimeAfterGameInstalled"); } }
    public static int AdTimeBetweenAds { get { return GoogleTagManagerPlugin.Instance.GetInt("AdTimeBetweenAds"); } }
    public static string LocalNotificationConfig { get { return GoogleTagManagerPlugin.Instance.GetString("LocalNotificationConfig"); } }
    public static float VipRateOnKeyProp { get { return GoogleTagManagerPlugin.Instance.GetFloat("VipRateOnKeyProp"); } }
    public static bool EnableBannerAds { get { return GoogleTagManagerPlugin.Instance.GetBool("EnableBannerAds"); } }
    public static float InterstitialAfterStartFrequency { get { return GoogleTagManagerPlugin.Instance.GetFloat("InterstitialAfterStartFrequency"); } } // 1.0f - always after start, 0.0f - always after result screen
    public static bool EnableHouseAds { get { return GoogleTagManagerPlugin.Instance.GetBool("EnableHouseAds"); } }
    public static bool EnableNotifications { get { return GoogleTagManagerPlugin.Instance.GetBool("EnableNotifications"); } }
    public static int CheckpointDuration { get { return GoogleTagManagerPlugin.Instance.GetInt("CheckpointDuration"); } }
    public static float AdmobHeyzapProb { get { return GoogleTagManagerPlugin.Instance.GetFloat("AdmobHeyzapProb"); } }
    public static bool PreReleaseMode { get { return GoogleTagManagerPlugin.Instance.GetBool("PreReleaseMode"); } }
    public static string InAppPromoHalfDiscountProducts { get { return GoogleTagManagerPlugin.Instance.GetString("InAppPromoHalfDiscountProducts"); } }
    public static bool InAppPromoHalfDiscountInStartScreenVisible { get { return GoogleTagManagerPlugin.Instance.GetBool("InAppPromoHalfDiscountInStartScreenVisible"); } }
    public static bool InAppPromoHalfDiscountInStoreVisible { get { return GoogleTagManagerPlugin.Instance.GetBool("InAppPromoHalfDiscountInStoreVisible"); } }
    private static string NextUpdateOfferInfo { get { return GoogleTagManagerPlugin.Instance.GetString("NextUpdateOfferInfo"); } }
    public static bool IsChristmas { get { return GoogleTagManagerPlugin.Instance.GetBool("IsChristmas"); } }
    public static bool IsInterstitialInsteadOfVideo { get { return GoogleTagManagerPlugin.Instance.GetBool("IsInterstitialInsteadOfVideo"); } }
    public static float CustomLeaderboardsForceRefreshTime { get { return GoogleTagManagerPlugin.Instance.GetFloat("CustomLeaderboardsForceRefreshTime"); } }
    public static bool CustomLeaderboardsEnabled { get { return GoogleTagManagerPlugin.Instance.GetBool("CustomLeaderboardsEnabled"); } }
    public static string CustomLeaderboardsURL { get { return GoogleTagManagerPlugin.Instance.GetString("CustomLeaderboardsURL"); } }

    public static int InAppPromoHalfDiscountDaySpan
    {
        get
        {
            int span = GoogleTagManagerPlugin.Instance.GetInt("InAppPromoHalfDiscountDaySpan");
            if (span < 1)
            {
                return 1;
            }
            else
            {
                return span;
            }
        }
    }


    public static RemoteCarStatsDBv2 CarStatsDB
    {
        get
        {
            try
            {
                return new RemoteCarStatsDBv2();
            }
            catch
            {
                return null;
            }
        }
    }

    public static RemoteCityStatsDB CityStatsDB
    {
        get
        {
            try
            {
                return new RemoteCityStatsDB();
            }
            catch
            {
                return null;
            }
        }
    }

    public static RefundsDB RefundsDB
    {
        get
        {
            try
            {
                return new RefundsDB();
            }
            catch
            {
                return null;
            }
        }
    }

    public static RemoteDailyGiftFrequencies DailyGiftFrequencies
    {
        get
        {
            try
            {
                return new RemoteDailyGiftFrequencies();
            }
            catch
            {
                return null;
            }
        }
    }

    public static bool NextUpdateReady
    {
        get
        {
            try
            {
                string valueToParse = GetNextUpdateOfferInfo(0);
                if (valueToParse != null && valueToParse.Length > 0)
                {
                    return bool.Parse(valueToParse);
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }
        }
    }

    public static string LiveVersionNumber
    {
        get
        {
            return GetNextUpdateOfferInfo(1);
        }
    }

    public static string NextUpdateVersionNumber
    {
        get
        {
            return GetNextUpdateOfferInfo(2);
        }
    }

    public static string LiveUpdateDescription
    {
        get
        {
            return GetNextUpdateOfferInfo(3).Replace("\\n", System.Environment.NewLine);
        }
    }

    public static string NextUpdateDescription
    {
        get
        {
            return GetNextUpdateOfferInfo(4).Replace("\\n", System.Environment.NewLine);
        }
    }



    private static string GetNextUpdateOfferInfo(int i)
    {
        try
        {
            string[] strings = NextUpdateOfferInfo.Split(';');
            if (strings != null && strings.Length > i)
            {
                return strings[i];
            }
            else
            {
                return null;
            }
        }
        catch
        {
            return null;
        }
    }
}

