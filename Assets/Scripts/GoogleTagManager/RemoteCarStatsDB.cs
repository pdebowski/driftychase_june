﻿using UnityEngine;
using System.Collections;

public class RemoteCarStatsDB {

	public class CarStats {
		public float CashMultiplier;
		public int Price;
		public int FreeGiftValue;
		public int GiftMinValue;
		public int GiftMaxValue;
		public int ScoreRequired;
	}

	protected CarStats[] carStats;

	protected enum CarProps
	{
		CASH_MULTIPLIER = 0,
		PRICE = 1,
		FREE_GIFT_VALUE = 2,
		GIFT_MIN_VALUE = 3,
		GIFT_MAX_VALUE = 4,
		SCORE_REQUIRED = 5,
		NUM_OF_CAR_PROPS = 6
	};

	public RemoteCarStatsDB()
	{
		string remoteData = GoogleTagManagerPlugin.Instance.GetString("CarStatsDB");
		if (remoteData.Length > 0)
		{
			string[] carsData = remoteData.Split(';');
			carStats = new CarStats[carsData.Length];

			int carId = 0;
			foreach(string carData in carsData)
			{
				string[] carValues = carData.Split(',');
				if (carValues.Length >= (int)CarProps.NUM_OF_CAR_PROPS)
				{
					carStats[carId] = new CarStats();
					carStats[carId].CashMultiplier = float.Parse(carValues[(int)CarProps.CASH_MULTIPLIER]);
					carStats[carId].Price = int.Parse(carValues[(int)CarProps.PRICE]);
					carStats[carId].FreeGiftValue = int.Parse(carValues[(int)CarProps.FREE_GIFT_VALUE]);
					carStats[carId].GiftMinValue = int.Parse(carValues[(int)CarProps.GIFT_MIN_VALUE]);
					carStats[carId].GiftMaxValue = int.Parse(carValues[(int)CarProps.GIFT_MAX_VALUE]);
					carStats[carId].ScoreRequired = int.Parse(carValues[(int)CarProps.SCORE_REQUIRED]);
				}
				else
				{
					CrimsonError.LogError("Car Stats DB parse error in car " + carId);
				}

				++carId;
			}
		}
		else
		{
			CrimsonError.LogError("Car Stats DB parse error, remote string empty");
		}
	}

	public CarStats GetStats(int carId)
	{
		if (carId < carStats.Length)
		{
			return carStats[carId];
		}
		else
		{
			CrimsonError.LogWarning("Cannot find car in remote cars db");
			return null;
		}
	}


}
