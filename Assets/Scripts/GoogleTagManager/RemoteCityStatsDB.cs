﻿using UnityEngine;
using System.Collections;

public class RemoteCityStatsDB : IEnumerable
{

    public class CityStats
    {
        public int Length;
        public int Speed;
    }

    private CityStats[] cityStats;

    enum CityProps
    {
        LENGTH = 0,
        SPEED = 1,
        NUM_OF_CITY_PROPS = 2
    };

    public int Length { get { return cityStats.Length; } }

    public RemoteCityStatsDB()
    {
        string remoteData = GoogleTagManagerPlugin.Instance.GetString("CityStatsDB");
        if (remoteData.Length > 0)
        {
            string[] citiesData = remoteData.Split(';');
            cityStats = new CityStats[citiesData.Length];

            int cityId = 0;
            foreach (string cityData in citiesData)
            {
                string[] cityValues = cityData.Split(',');
                if (cityValues.Length >= (int)CityProps.NUM_OF_CITY_PROPS)
                {
                    cityStats[cityId] = new CityStats();
                    cityStats[cityId].Length = int.Parse(cityValues[(int)CityProps.LENGTH]);
                    cityStats[cityId].Speed = int.Parse(cityValues[(int)CityProps.SPEED]);
                }
                else
                {
                    CrimsonError.LogError("City Stats DB parse error in city " + cityId);
                }

                ++cityId;
            }
        }
        else
        {
            CrimsonError.LogError("City Stats DB parse error, remote string empty");
        }
    }

    public CityStats GetStats(int cityId)
    {
        if (cityId < cityStats.Length)
        {
            return cityStats[cityId];
        }
        else
        {
            CrimsonError.LogWarning("Cannot find city in remote cities db");
            return null;
        }
    }



    public IEnumerator GetEnumerator()
    {
        return cityStats.GetEnumerator();
    }
}
