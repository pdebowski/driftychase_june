﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;

public class RemoteCarStatsDBv2 : RemoteCarStatsDB
{

    private List<CarDefinitions.CarEnum> carEnums;

    public RemoteCarStatsDBv2()
    {

        string remoteData = GoogleTagManagerPlugin.Instance.GetString("CarStatsDBv3");
        if (remoteData.Length > 0)
        {
            string[] carsData = remoteData.Split(';');
            List<CarStats> remoteCarStats = new List<CarStats>();
            carEnums = new List<CarDefinitions.CarEnum>();

            int carId = 0;
            foreach (string carData in carsData)
            {
                string[] carInfo = carData.Split('-');
                if (IsCarAvailable(carInfo[0].Trim()))
                {
                    string[] carValues = carInfo[1].Split(',');
                    if (carValues.Length >= (int)CarProps.NUM_OF_CAR_PROPS)
                    {
                        CarStats carStats = new CarStats();
                        carStats.CashMultiplier = float.Parse(carValues[(int)CarProps.CASH_MULTIPLIER]);
                        carStats.Price = int.Parse(carValues[(int)CarProps.PRICE]);
                        carStats.FreeGiftValue = int.Parse(carValues[(int)CarProps.FREE_GIFT_VALUE]);
                        carStats.GiftMinValue = int.Parse(carValues[(int)CarProps.GIFT_MIN_VALUE]);
                        carStats.GiftMaxValue = int.Parse(carValues[(int)CarProps.GIFT_MAX_VALUE]);
                        carStats.ScoreRequired = int.Parse(carValues[(int)CarProps.SCORE_REQUIRED]);

                        remoteCarStats.Add(carStats);
                        carEnums.Add((CarDefinitions.CarEnum)Enum.Parse(typeof(CarDefinitions.CarEnum), carInfo[0].Trim(), true));
                    }
                    else
                    {
                        CrimsonError.LogError("Car Stats DB parse error in car " + carId);
                    }


                    ++carId;
                }
                else
                {
                    CrimsonError.LogError("Car is not available " + carInfo[0]);
                }

            }

            this.carStats = remoteCarStats.ToArray();
        }
        else
        {
            CrimsonError.LogError("Car Stats DB parse error, remote string empty");
        }
    }

    private bool IsCarAvailable(string carEnum)
    {
        List<CarDefinitions.CarEnum> carEnums = Enum.GetValues(typeof(CarDefinitions.CarEnum)).Cast<CarDefinitions.CarEnum>().ToList();
        foreach (var item in carEnums)
        {
            if (item.ToString() == carEnum)
            {
                return true;
            }
        }

        return false;
    }

    public CarStats GetStats(CarDefinitions.CarEnum carEnum)
    {
        if (carEnums.Contains(carEnum))
        {
            return carStats[carEnums.IndexOf(carEnum)];
        }
        else
        {
            CrimsonError.LogWarning("Cannot find car in remote cars db");
            return null;
        }
    }
}
