﻿using UnityEngine;
using System.Collections;

namespace MenuNavigation
{
    [RequireComponent(typeof(UnityEngine.UI.Slider))]
    public class Slider : Scope
    {
        [Header("Slider")]
        [SerializeField]
        private float sliderSpeed = 0.1f;

        private UnityEngine.UI.Slider slider;
        private Orientation orientation;

        protected override void Awake()
        {
            base.Awake();
            slider = GetComponent<UnityEngine.UI.Slider>();
            CheckOrientation();
        }

        private void CheckOrientation()
        {
            switch (slider.direction)
            {
                case UnityEngine.UI.Slider.Direction.LeftToRight:
                case UnityEngine.UI.Slider.Direction.RightToLeft:
                    orientation = Orientation.Horizontal;
                    break;
                case UnityEngine.UI.Slider.Direction.BottomToTop:
                case UnityEngine.UI.Slider.Direction.TopToBottom:
                    orientation = Orientation.Vertical;
                    break;
                default:
                    break;
            }
        }

        public override void Activate()
        {
            base.Activate();
            NavigationController.Instance.ActiveElement = null;
        }

        public override void PerformNavigation(NavigationInfo navigationInfo)
        {
            NavigationElement destinationElement = null;
            switch (navigationInfo.Direction)
            {
                case NavigationDirection.Left:
                    destinationElement = navigation.Left;
                    break;
                case NavigationDirection.Up:
                    destinationElement = navigation.Up;
                    break;
                case NavigationDirection.Right:
                    destinationElement = navigation.Right;
                    break;
                case NavigationDirection.Down:
                    destinationElement = navigation.Down;
                    break;
                default:
                    break;
            }
            AnalyzeNavigation(navigationInfo, destinationElement);
        }

        public override void PerformNavigation(NavigationInfoV2 navigationInfoV2)
        {
            NavigationElement destinationElement = null;
            if (Utilities.IsMatch(NavigationDirectionV2.WW, navigationInfoV2.Direction))
            {
                destinationElement = navigation.Left;
            }
            else if (Utilities.IsMatch(NavigationDirectionV2.NN, navigationInfoV2.Direction))
            {
                destinationElement = navigation.Up;
            }
            else if (Utilities.IsMatch(NavigationDirectionV2.SS, navigationInfoV2.Direction))
            {
                destinationElement = navigation.Down;
            }
            else if (Utilities.IsMatch(NavigationDirectionV2.EE, navigationInfoV2.Direction))
            {
                destinationElement = navigation.Right;
            }
            AnalyzeNavigation(navigationInfoV2, destinationElement);
        }

        private void AnalyzeNavigation(NavigationInfo navigationInfo, NavigationElement element)
        {
            switch (navigationInfo.Direction)
            {
                case NavigationDirection.Left:
                case NavigationDirection.Right:
                    if (orientation == Orientation.Horizontal)
                    {
                        MoveSlider(navigationInfo.Direction);
                    }
                    else
                    {
                        NavigateTo(element, navigationInfo);
                    }
                    break;
                case NavigationDirection.Up:
                case NavigationDirection.Down:
                    if (orientation == Orientation.Vertical)
                    {
                        MoveSlider(navigationInfo.Direction);
                    }
                    else
                    {                     
                        NavigateTo(element, navigationInfo);
                    }
                    break;
                default:
                    break;
            }
        }

        private void AnalyzeNavigation(NavigationInfoV2 navigationInfoV2, NavigationElement element)
        {

            if (Utilities.IsMatch(NavigationDirectionV2.WW, navigationInfoV2.Direction) || Utilities.IsMatch(NavigationDirectionV2.EE, navigationInfoV2.Direction))
            {
                if (orientation == Orientation.Horizontal)
                {
                    MoveSlider(navigationInfoV2.Direction);
                }
                else
                {
                    NavigateTo(element, navigationInfoV2);
                }
            }
            else if (Utilities.IsMatch(NavigationDirectionV2.SS, navigationInfoV2.Direction) || Utilities.IsMatch(NavigationDirectionV2.NN, navigationInfoV2.Direction))
            {
                if (orientation == Orientation.Vertical)
                {
                    MoveSlider(navigationInfoV2.Direction);
                }
                else
                {
                    NavigateTo(element, navigationInfoV2);
                }
            }         
        }

        private void MoveSlider(NavigationDirection direction)
        {
            int signFromSliderDirection = 1;
            switch (slider.direction)
            {
                case UnityEngine.UI.Slider.Direction.LeftToRight:
                case UnityEngine.UI.Slider.Direction.BottomToTop:
                    signFromSliderDirection = 1;
                    break;
                case UnityEngine.UI.Slider.Direction.RightToLeft:
                case UnityEngine.UI.Slider.Direction.TopToBottom:
                    signFromSliderDirection = -1;
                    break;
                default:
                    break;
            }
            switch (direction)
            {  
                case NavigationDirection.Right:
                case NavigationDirection.Up:
                    signFromSliderDirection *= 1;
                    break;
                case NavigationDirection.Left:
                case NavigationDirection.Down:
                    signFromSliderDirection *= -1;
                    break;
                default:
                    break;
            }
            slider.normalizedValue += sliderSpeed * signFromSliderDirection;
        }

        private void MoveSlider(NavigationDirectionV2 directionV2)
        {
            int signFromSliderDirection = 1;
            switch (slider.direction)
            {
                case UnityEngine.UI.Slider.Direction.LeftToRight:
                case UnityEngine.UI.Slider.Direction.BottomToTop:
                    signFromSliderDirection = 1;
                    break;
                case UnityEngine.UI.Slider.Direction.RightToLeft:
                case UnityEngine.UI.Slider.Direction.TopToBottom:
                    signFromSliderDirection = -1;
                    break;
                default:
                    break;
            }
            if (Utilities.IsMatch(NavigationDirectionV2.EE, directionV2) || Utilities.IsMatch(NavigationDirectionV2.NN, directionV2))
            {
                signFromSliderDirection *= 1;
            }
            else if (Utilities.IsMatch(NavigationDirectionV2.WW, directionV2) || Utilities.IsMatch(NavigationDirectionV2.SS, directionV2))
            {
                signFromSliderDirection *= -1;
            }
            slider.normalizedValue += sliderSpeed * signFromSliderDirection;
        }

    }
}