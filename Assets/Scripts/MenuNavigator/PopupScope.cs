﻿using UnityEngine;
using System.Collections;

namespace MenuNavigation
{
    public class PopupScope : Scope
    {
        [Header("Popup Scope")]
        [SerializeField]
        private Scope scopeToGetFocusBackOnDisable;

        protected override void Awake()
        {
            base.Awake();
            activateAlwaysDefaultElement = true;
            blockScopesHierarchy = true;
            isDefault = true;
        }

        protected override void OnDisable()
        {
            if (scopeToGetFocusBackOnDisable != null)
            {
                scopeToGetFocusBackOnDisable.GetFocusBack();
            }
        }

        protected override void NavigateTo(NavigationElement element, NavigationInfo navigationInfo)
        {
        }
    }
}