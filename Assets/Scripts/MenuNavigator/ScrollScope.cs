﻿using UnityEngine;
using System.Collections;

namespace MenuNavigation
{
    [RequireComponent(typeof(UnityEngine.UI.ScrollRect))]
    public class ScrollScope : Scope
    {
        [Header("ScrollScope")]

        [SerializeField]
        private Orientation orientation;

        private ScrollAdapter scrollAdapter;
        private UnityEngine.UI.ScrollRect scrollRect;
        private ScrollItemScope currentItem;

        protected override void Awake()
        {
            base.Awake();
            scrollRect = GetComponent<UnityEngine.UI.ScrollRect>();
            scrollAdapter = GetComponent<ScrollAdapter>();
        }

        public override void Activate()
        {
            base.Activate();
            currentItem = scrollAdapter.SetDefault();
            currentItem.Activate(false);
        }

        private void ActivateCurrentElement(bool fromOtherScrollElement)
        {
            currentItem = scrollAdapter.ScrollToCurrent();
            currentItem.Activate(fromOtherScrollElement);
        }

        public override void PerformNavigation(NavigationInfo navigationInfo)
        {
            switch (navigationInfo.Direction)
            {
                case NavigationDirection.Left:
                    AnalyzeNavigation(navigationInfo, navigation.Left);
                    break;
                case NavigationDirection.Up:
                    AnalyzeNavigation(navigationInfo, navigation.Up);
                    break;
                case NavigationDirection.Right:
                    AnalyzeNavigation(navigationInfo, navigation.Right);
                    break;
                case NavigationDirection.Down:
                    AnalyzeNavigation(navigationInfo, navigation.Down);
                    break;
                default:
                    break;
            }
        }

        public override void PerformNavigation(NavigationInfoV2 navigationInfoV2)
        {
            if (Utilities.IsMatch(NavigationDirectionV2.WW, navigationInfoV2.Direction))
            {
                AnalyzeNavigation(navigationInfoV2, navigation.Left);
            }
            else if (Utilities.IsMatch(NavigationDirectionV2.NN, navigationInfoV2.Direction))
            {
                AnalyzeNavigation(navigationInfoV2, navigation.Up);
            }
            else if (Utilities.IsMatch(NavigationDirectionV2.EE, navigationInfoV2.Direction))
            {
                AnalyzeNavigation(navigationInfoV2, navigation.Right);
            }
            else if (Utilities.IsMatch(NavigationDirectionV2.SS, navigationInfoV2.Direction))
            {
                AnalyzeNavigation(navigationInfoV2, navigation.Down);
            }
        }

        private void AnalyzeNavigation(NavigationInfo navigationInfo, NavigationElement element)
        {
            if (orientation == Orientation.Vertical)
            {
                switch (navigationInfo.Direction)
                {
                    case NavigationDirection.Up:
                        currentItem = scrollAdapter.ScrollToPreviousItem();
                        ActivateCurrentElement(true);
                        break;
                    case NavigationDirection.Down:
                        currentItem = scrollAdapter.ScrollToNextItem();
                        ActivateCurrentElement(true);
                        break;
                    case NavigationDirection.Left:
                    case NavigationDirection.Right:
                        NavigateTo(element, navigationInfo);
                        break;
                    default:
                        break;
                }
            }
            else
            {
                switch (navigationInfo.Direction)
                {
                    case NavigationDirection.Left:
                        currentItem = scrollAdapter.ScrollToPreviousItem();
                        ActivateCurrentElement(true);
                        break;
                    case NavigationDirection.Right:
                        currentItem = scrollAdapter.ScrollToNextItem();
                        ActivateCurrentElement(true);
                        break;
                    case NavigationDirection.Down:
                    case NavigationDirection.Up:
                        NavigateTo(element, navigationInfo);
                        break;
                    default:
                        break;
                }
            }
        }

        private void AnalyzeNavigation(NavigationInfoV2 navigationInfoV2, NavigationElement element)
        {
            if (orientation == Orientation.Vertical)
            {
                if (Utilities.IsMatch(NavigationDirectionV2.NN, navigationInfoV2.Direction))
                {
                    currentItem = scrollAdapter.ScrollToPreviousItem();
                    ActivateCurrentElement(true);
                }
                else if (Utilities.IsMatch(NavigationDirectionV2.SS, navigationInfoV2.Direction))
                {
                    currentItem = scrollAdapter.ScrollToNextItem();
                    ActivateCurrentElement(true);
                }
                else if (Utilities.IsMatch(NavigationDirectionV2.WW, navigationInfoV2.Direction) || Utilities.IsMatch(NavigationDirectionV2.EE, navigationInfoV2.Direction))
                {
                    base.PerformNavigation(navigationInfoV2);
                }
            }
            else
            {
                if (Utilities.IsMatch(NavigationDirectionV2.WW, navigationInfoV2.Direction))
                {
                    currentItem = scrollAdapter.ScrollToPreviousItem();
                    ActivateCurrentElement(true);
                }
                else if (Utilities.IsMatch(NavigationDirectionV2.EE, navigationInfoV2.Direction))
                {
                    currentItem = scrollAdapter.ScrollToNextItem();
                    ActivateCurrentElement(true);
                }
                else if (Utilities.IsMatch(NavigationDirectionV2.SS, navigationInfoV2.Direction) || Utilities.IsMatch(NavigationDirectionV2.NN, navigationInfoV2.Direction))
                {
                    base.PerformNavigation(navigationInfoV2);
                }
            }
        }
    }
}
