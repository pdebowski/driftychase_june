﻿using UnityEngine;
using System.Collections;

namespace MenuNavigation
{

    public interface ScrollAdapter
    {
        ScrollItemScope SetDefault();
        ScrollItemScope ScrollToCurrent();
        ScrollItemScope ScrollToNextItem();
        ScrollItemScope ScrollToPreviousItem();
    }
}
