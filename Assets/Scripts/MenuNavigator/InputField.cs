﻿using UnityEngine;
using System.Collections;
namespace MenuNavigation
{
    [RequireComponent(typeof(UnityEngine.UI.InputField))]
    public class InputField : ClickableElement
    {
        protected UnityEngine.UI.InputField inputField;

        protected override void Awake()
        {
            base.Awake();
            inputField = GetComponent<UnityEngine.UI.InputField>();
        }

        public override void PerformActionClick()
        {
            if (this.gameObject.activeInHierarchy == true)
            {
                base.PerformActionClick();

                inputField.ActivateInputField();
            }
        }

    }
}
