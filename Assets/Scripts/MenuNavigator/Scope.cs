﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace MenuNavigation
{
    public abstract class Scope : NavigationElement
    {
        [Header("Scope")]
        [SerializeField]
        protected NavigationElement activateIfZeroChilds;
        [SerializeField]
        protected List<NavigationElement> elementsInScope;

        public NavigationElement CurrentActiveElement { get; set; }

        protected bool activateLastSelectedElement = false;
        protected bool activateAlwaysDefaultElement = false;
        protected bool blockScopesHierarchy = false;
        protected bool isFirstActivation = false;

        private bool areElementsSetFromInspector = false;

        protected override void Awake()
        {
            base.Awake();
            isFirstActivation = true;
            if (elementsInScope != null && elementsInScope.Count > 0)
            {
                areElementsSetFromInspector = true;
            }
            GetChilds();
        }

        private void GetChilds()
        {
            if (areElementsSetFromInspector == false)
            {
                NavigationElement[] elementsInScopeArray = GetComponentsInChildren<NavigationElement>();
                elementsInScope = new List<NavigationElement>();
                foreach (var item in elementsInScopeArray)
                {
                    if (item != this && item.GetScope() == this)
                    {
                        elementsInScope.Add(item);
                    }
                }
            }
        }

        protected override void OnEnable()
        {
            base.OnEnable();

            if ((GetComponentInChildren<TutorialScope>() != null && GetComponentInChildren<TutorialScope>().GetScope() == this)
                || (blockScopesHierarchy == false && GetScope() != null))
            {
                return;
            }
            if (isDefault == true)
            {
                if (NavigationController.Instance != null)
                {
                    NavigationController.Instance.Activate(this);
                }
            }
        }

        protected virtual void OnDisable()
        {
            if (!areElementsSetFromInspector)
            {
                elementsInScope = null;
            }
        }


        public override void Activate()
        {
            base.Activate();

            GetChilds();
            if (isFirstActivation || activateAlwaysDefaultElement)
            {
                bool activatedButton = false;

                foreach (var item in elementsInScope)
                {
                    if (item.isDefault && CouldBeActivate(item))
                    {
                        activatedButton = true;
                        ActivateElement(item);
                        CurrentActiveElement = item;
                        break;
                    }
                }

                isFirstActivation = false;

                if (!activatedButton)
                {
                    ActivateIfZeroChilds();
                }
            }
            else
            {
                if (CurrentActiveElement != null && CouldBeActivate(CurrentActiveElement))
                {
                    ActivateElement(CurrentActiveElement);
                }
                else
                {
                    ActivateIfZeroChilds();
                }
            }


        }

        public void ActivateIfZeroChilds()
        {

            if (NavigationController.Instance != null && activateIfZeroChilds != null)
            {
                NavigationController.Instance.ActivateOnlyElement(activateIfZeroChilds);
                return;
            }

            foreach (var item in elementsInScope)
            {
                if (CouldBeActivate(item))
                {
                    ActivateElement(item);
                    CurrentActiveElement = item;
                    return;
                }
            }
        }

        private bool CouldBeActivate(NavigationElement element)
        {
            return !(element.focusOnlyIfIsActive == true && element.gameObject.activeInHierarchy == false);
        }

        private void ActivateElement(NavigationElement element)
        {
            if (NavigationController.Instance != null)
            {
                NavigationController.Instance.Activate(element);
            }
        }


        public override void Deactivate()
        {
            base.Deactivate();
            if (CurrentActiveElement != null)
            {
                CurrentActiveElement.Deactivate();
            }
        }

        protected override void NavigateTo(NavigationElement element, NavigationInfo navigationInfo)
        {
            if (blockScopesHierarchy == false)
            {
                if (element != null)
                {
                    if (NavigationController.Instance != null)
                    {
                        NavigationController.Instance.Activate(element);
                    }
                    if (element.GetScope() == this)
                    {
                        CurrentActiveElement = element;
                    }
                }
                else
                {
                    base.NavigateTo(element, navigationInfo);
                }

            }
        }

        public void GetFocusBack()
        {
            if (CurrentActiveElement != null)
            {
                ActivateElement(CurrentActiveElement);
            }
            else
            {
                Activate();
            }
        }
    }
}
