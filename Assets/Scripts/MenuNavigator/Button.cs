﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

namespace MenuNavigation
{
    [RequireComponent(typeof(UnityEngine.UI.Button))]
    public class Button : ClickableElement
    {
        [Header("Button")]
        [SerializeField]
        private bool isAvailableWhenNotEnable = true;
        [SerializeField]
        protected NavigationElement alternativeElement = null;


        protected UnityEngine.UI.Button button;
        private bool wasClick = false;

        protected override void Awake()
        {
            base.Awake();
            button = GetComponent<UnityEngine.UI.Button>();
        }

        protected override void OnEnable()
        {
            button.onClick.AddListener(EndTutorialIfAvailable);
        }

        void OnDisable()
        {
            if (button != null)
            {
                button.onClick.RemoveListener(EndTutorialIfAvailable);
            }


            if (wasClick == true && alternativeElement != null)
            {
                if (NavigationController.Instance != null && NavigationController.Instance.ActiveElement == this)
                {
                    NavigationController.Instance.Activate(alternativeElement);
                }
                wasClick = false;
            }
        }

        public override void Activate()
        {
            base.Activate();
        }

        public override bool IsElementAvailable()
        {
            bool isAvailable = isAvailableWhenNotEnable;
            if (button != null)
            {
                isAvailable |= button.isActiveAndEnabled == true;
                if (button.targetGraphic != null)
                {
                    isAvailable |= button.targetGraphic.isActiveAndEnabled == true;
                }
            }


            return base.IsElementAvailable() && isAvailable == true;
        }

        public override void PerformActionClick()
        {
            if (this.gameObject.activeInHierarchy == true && button.interactable)
            {
                base.PerformActionClick();

                wasClick = true;

                button.onClick.Invoke();
            }
        }
    }
}

