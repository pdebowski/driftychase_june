﻿using UnityEngine;
using System.Collections;

namespace MenuNavigation
{
    public class NavigationController : Singleton<NavigationController>
    {

        public Scope ActiveScope { get; set; }
        public ClickableElement ActiveElement { get; set; }

        public void Activate(Scope scope)
        {
            if (ActiveScope != null)
            {
                ActiveScope.Deactivate();
            }
            ActiveScope = scope;
            if (scope != null)
            {
                scope.Activate();
                Scope parentScope = scope.GetScope();
                if (parentScope != null)
                {
                    parentScope.CurrentActiveElement = scope;
                }
            }

        }

        public void Activate(ClickableElement element)
        {
            if (ActiveElement != null && ActiveElement != element)
            {
                ActiveElement.Deactivate();
            }
            ActiveElement = element;
            if (element != null)
            {
                element.Activate();
                Scope elementScope = element.GetScope();
                if (elementScope != null)
                {
                    elementScope.CurrentActiveElement = element;
                }
                if (ActiveScope != elementScope)
                {
                    Activate(elementScope);
                }
            }
            else
            {
                ActiveElement = null;
            }

        }

        public void Activate(NavigationElement element)
        {
            if (element is ClickableElement)
            {
                Activate(element as ClickableElement);
            }
            if (element is Scope)
            {
                Activate(element as Scope);
            }
        }

        public void ActivateOnlyElement(NavigationElement element)
        {
            if (element != null)
            {
                element.ActivateOnlySelf();
                if (element is ClickableElement)
                {
                    ActiveElement = element as ClickableElement;
                }
                ActiveScope = element.GetScope();
            }
        }

        public void PerformNavigation(NavigationInfo navigationInfo)
        {
            if (ActiveElement != null)
            {
                ActiveElement.PerformNavigation(navigationInfo);
            }
            else
            {
                if (ActiveScope != null)
                {
                    ActiveScope.PerformNavigation(navigationInfo);
                }
            }
        }
        public void PerformNavigation(NavigationInfoV2 navigationInfoV2)
        {
            if (ActiveElement != null)
            {
                ActiveElement.PerformNavigation(navigationInfoV2);
            }
            else
            {
                if (ActiveScope != null)
                {
                    ActiveScope.PerformNavigation(navigationInfoV2);
                }
            }
        }

        public void PerformActionClick()
        {
            if (ActiveElement != null)
            {
                ActiveElement.PerformActionClick();
            }
        }
    }
}
