﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
namespace MenuNavigation
{
    [RequireComponent(typeof(UnityEngine.UI.Toggle))]
    public class Toggle : ClickableElement
    {

        private UnityEngine.UI.Toggle toggle;

        protected override void Awake()
        {
            toggle = GetComponent<UnityEngine.UI.Toggle>();
        }

        public override void PerformActionClick()
        {
            if (this.gameObject.activeInHierarchy == true)
            {
                base.PerformActionClick();
                toggle.isOn = !toggle.isOn;
                toggle.onValueChanged.Invoke(toggle.isOn);
            }
        }

    }
}
