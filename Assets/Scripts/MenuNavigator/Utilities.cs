﻿using UnityEngine;
using System.Collections;

namespace MenuNavigation
{

    class Utilities
    {
        public static bool IsMatch(NavigationDirectionV2 direction, NavigationDirectionV2 toCompare)
        {
            if (direction == toCompare || direction == NavigationDirectionV2.ALL)
            {
                return true;
            }
            switch (direction)
            {
                case NavigationDirectionV2.N:
                    return toCompare.ToString().Contains("N");
                case NavigationDirectionV2.E:
                    return toCompare.ToString().Contains("E");
                case NavigationDirectionV2.W:
                    return toCompare.ToString().Contains("W");
                case NavigationDirectionV2.S:
                    return toCompare.ToString().Contains("S");
                case NavigationDirectionV2.NN:
                    return toCompare.ToString().Length > 2 && toCompare.ToString()[0] == 'N';
                case NavigationDirectionV2.EE:
                    return toCompare.ToString().Length > 2 && toCompare.ToString()[0] == 'E';
                case NavigationDirectionV2.WW:
                    return toCompare.ToString().Length > 2 && toCompare.ToString()[0] == 'W';
                case NavigationDirectionV2.SS:
                    return toCompare.ToString().Length > 2 && toCompare.ToString()[0] == 'S';
                case NavigationDirectionV2.SE:
                    return toCompare.ToString().Length > 2 && toCompare.ToString().Contains("S") && toCompare.ToString().Contains("E");
                case NavigationDirectionV2.SW:
                    return toCompare.ToString().Length > 2 && toCompare.ToString().Contains("S") && toCompare.ToString().Contains("W");
                case NavigationDirectionV2.NE:
                    return toCompare.ToString().Length > 2 && toCompare.ToString().Contains("N") && toCompare.ToString().Contains("E");
                case NavigationDirectionV2.NW:
                    return toCompare.ToString().Length > 2 && toCompare.ToString().Contains("N") && toCompare.ToString().Contains("W");
                default:
                    break;
            }
            return false;
        }
    }

    public enum NavigationDirection
    {
        Left,
        Up,
        Right,
        Down
    }

    public enum NavigationDirectionV2
    {
        N,
        E,
        W,
        S,
        NN,
        EE,
        WW,
        SS,
        NE,
        NW,
        SE,
        SW,
        NWN,
        NEN,
        ENE,
        ESE,
        SES,
        SWS,
        WSW,
        WNW,
        ALL,
        NONE
    }

    [System.Serializable]
    public class NavigationLabel
    {
        public NavigationDirectionV2 direction;
        public NavigationElement element;
    }


    public enum Orientation
    {
        Horizontal,
        Vertical
    }

    public class NavigationInfo
    {
        public NavigationDirection Direction;
    }

    public class NavigationInfoV2
    {
        public NavigationDirectionV2 Direction;
    }


    [System.Serializable]
    public class Navigation
    {
        public NavigationElement Left;
        public NavigationElement Up;
        public NavigationElement Right;
        public NavigationElement Down;
    }




}
