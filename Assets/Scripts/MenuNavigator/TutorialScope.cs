﻿using UnityEngine;
using System.Collections;

namespace MenuNavigation
{
    public class TutorialScope : PopupScope
    {
        [Header("TutorialScope")]
        [SerializeField]
        private ClickableElement elementToTeach;

        private Scope elementOriginalScope;
        private Navigation elementOriginalNavigation;

        protected override void Awake()
        {
            base.Awake();
            activateAlwaysDefaultElement = false;
            blockScopesHierarchy = true;
            isDefault = true;

        }
        protected override void OnEnable()
        {
            elementToTeach.TutorialScreen = this;

            elementOriginalScope = elementToTeach.GetScope();
            elementToTeach.SetScope(this);

            elementOriginalNavigation = elementToTeach.GetNavigation();
            elementToTeach.SetNavigation(new Navigation());

            CurrentActiveElement = elementToTeach;

            if (isDefault == true)
            {
                isFirstActivation = true;
                if (NavigationController.Instance != null)
                {
                    NavigationController.Instance.Activate(this);
                }
            }
        }

        public override void Activate()
        {
            base.Activate();
            if (NavigationController.Instance != null)
            {
                NavigationController.Instance.Activate(CurrentActiveElement);
            }
        }

        public void EndTutorial()
        {
            elementToTeach.SetScope(elementOriginalScope);
            elementToTeach.SetNavigation(elementOriginalNavigation);
            elementOriginalScope.CurrentActiveElement = elementToTeach;
        }



    }
}
