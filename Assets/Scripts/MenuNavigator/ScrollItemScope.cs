﻿using UnityEngine;
using System.Collections;

namespace MenuNavigation
{
    public class ScrollItemScope : Scope
    {

        protected override void Awake()
        {
            base.Awake();
            activateLastSelectedElement = true;
        }

        public override void Activate()
        {
            isFirstActivation = true;
            base.Activate();
        }

        public void Activate(bool fromOtherScrollElement)
        {
            if (fromOtherScrollElement == true)
            {
                CurrentActiveElement = null;
            }
            Activate();
        }
    }
}
