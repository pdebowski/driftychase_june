﻿using UnityEngine;
using System.Collections;

namespace MenuNavigation
{
    public abstract class NavigationElement : MonoBehaviour
    {
        [Header("NavigationElement")]
        [SerializeField]
        public bool isDefault = false;
        [SerializeField]
        protected Scope scope;
        [SerializeField]
        private GameObject[] focusObjects;
        [SerializeField]
        public bool focusOnlyIfIsActive = false;

        [Header("Digital Navigation")]
        [SerializeField]
        protected Navigation navigation;
        [Header("Analog Navigation")]
        [SerializeField]
        protected NavigationLabel[] navigationV2;

        protected bool isScopeSetFromInspector = false;
        private bool isInitialized = false;
        private bool isActive = false;


        protected virtual void Awake()
        {
            if (scope != null)
            {
                isScopeSetFromInspector = true;
            }

            TVTools();
            SetScope();
            isInitialized = true;
        }

        private void TVTools()
        {
            if (!TVDetector.WasChecked)
            {
                TVDetector.OnTVCheck += OnTVCheck;
            }
            else
            {
                //if (!GlobalSettings.IsTV)
                //{
                //    DestroyFocusObjects();
                //}
            }
        }

        private void OnTVCheck(bool isTV)
        {
            TVDetector.OnTVCheck -= OnTVCheck;
            if (!isTV)
            {
                DestroyFocusObjects();
            }
            else
            {
                if (isActive)
                {
                    SetFocusObjectsState(true);
                }
            }
        }

        private void DestroyFocusObjects()
        {
            if (focusObjects != null && focusObjects.Length > 0)
            {
                foreach (var item in focusObjects)
                {
                    Destroy(item.gameObject);
                }
            }
        }

        protected virtual void OnEnable()
        {
            if (isInitialized == false)
            {
                Awake();
            }
        }

        public Navigation GetNavigation()
        {
            return navigation;
        }

        public void SetNavigation(Navigation navigation)
        {
            this.navigation = navigation;
        }

        public void SetScope()
        {
            if (transform.parent != null && scope == null)
            {
                scope = transform.parent.GetComponentInParent<Scope>();
            }
        }
        public void SetScope(Scope scope)
        {
            this.scope = scope;
        }

        public Scope GetScope()
        {
            if (scope == null)
            {
                SetScope();
            }
            return scope;
        }

        public virtual void PerformNavigation(NavigationInfo navigationInfo)
        {
            NavigationElement element = GetElementAt(navigationInfo.Direction);
            NavigateTo(element, navigationInfo);
        }

        public void PerformNavigation(NavigationInfo navigationInfo, NavigationInfoV2 navigationInfoV2)
        {

            NavigationElement element = GetElementAt(navigationInfo.Direction);
            NavigateTo(element, navigationInfoV2);
        }

        public virtual void PerformNavigation(NavigationInfoV2 navigationInfoV2)
        {
            if (navigationV2.Length == 0)
            {
                NavigationInfo digitalNavigationInfo = ConvertToDigitalDirection(navigationInfoV2.Direction);
                PerformNavigation(digitalNavigationInfo);
                return;
            }
            NavigationElement destination = null;
            foreach (var item in navigationV2)
            {
                if (Utilities.IsMatch(item.direction, navigationInfoV2.Direction))
                {
                    destination = item.element;
                }
            }
            NavigateTo(destination, navigationInfoV2);
        }

        private NavigationElement GetElementAt(NavigationDirection direction)
        {
            switch (direction)
            {
                case NavigationDirection.Left:
                    return navigation.Left;
                case NavigationDirection.Up:
                    return navigation.Up;
                case NavigationDirection.Right:
                    return navigation.Right;
                case NavigationDirection.Down:
                    return navigation.Down;
                default:
                    return null;
            }
        }

        private NavigationInfo ConvertToDigitalDirection(NavigationDirectionV2 direction)
        {
            NavigationDirection newDirection = NavigationDirection.Down;
            if (Utilities.IsMatch(NavigationDirectionV2.WW, direction))
            {
                newDirection = NavigationDirection.Left;
            }
            if (Utilities.IsMatch(NavigationDirectionV2.NN, direction))
            {
                newDirection = NavigationDirection.Up;
            }
            if (Utilities.IsMatch(NavigationDirectionV2.EE, direction))
            {
                newDirection = NavigationDirection.Right;
            }
            if (Utilities.IsMatch(NavigationDirectionV2.SS, direction))
            {
                newDirection = NavigationDirection.Down;
            }
            NavigationInfo newNavigationInfo = new NavigationInfo();
            newNavigationInfo.Direction = newDirection;
            return newNavigationInfo;
        }

        protected virtual void NavigateTo(NavigationElement element, NavigationInfo navigationInfo)
        {
            if (element != null)
            {
                if (element.IsElementAvailable())
                {
                    if (NavigationController.Instance != null)
                    {
                        NavigationController.Instance.Activate(element);
                    }
                }
                else
                {
                    element.PerformNavigation(navigationInfo);
                }
            }
            else
            {
                if (GetScope() != null)
                {
                    scope.PerformNavigation(navigationInfo);
                }
            }
        }

        protected virtual void NavigateTo(NavigationElement element, NavigationInfoV2 navigationInfoV2)
        {
            if (element != null)
            {
                if (element.IsElementAvailable())
                {
                    if (NavigationController.Instance != null)
                    {
                        NavigationController.Instance.Activate(element);
                    }
                }
                else
                {
                    element.PerformNavigation(navigationInfoV2);
                }
            }
            else
            {
                if (GetScope() != null)
                {
                    scope.PerformNavigation(navigationInfoV2);
                }
            }
        }

        public virtual bool IsElementAvailable()
        {
            return this.gameObject.activeInHierarchy == true;
        }

        public virtual void Activate()
        {
            if (isInitialized == false)
            {
                Awake();
            }
            SetFocusObjectsState(true);
            if (scope == null)
            {
                SetScope();
            }
        }

        public virtual void ActivateOnlySelf()
        {
            if (isInitialized == false)
            {
                Awake();
            }
            SetFocusObjectsState(true);
        }

        public virtual void Deactivate()
        {
            isActive = false;
            SetFocusObjectsState(false);
        }

        private void SetFocusObjectsState(bool state)
        {
            isActive = true;
            if (focusObjects != null && focusObjects.Length > 0 && PlatformRecognition.IsTV)
            {
                foreach (var item in focusObjects)
                {
                    item.SetActive(state);
                }

            }
        }
    }
}
