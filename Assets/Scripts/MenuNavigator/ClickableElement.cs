﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

namespace MenuNavigation
{
    public class ClickableElement : NavigationElement
    {

        public TutorialScope TutorialScreen { get; set; }

        protected override void Awake()
        {
            base.Awake();
        }

        protected override void OnEnable()
        {
            base.OnEnable();
        }

        public virtual void PerformActionClick()
        {
            
        }

        protected void EndTutorialIfAvailable()
        {
            if (TutorialScreen != null)
            {
                TutorialScreen.EndTutorial();
            }
        }

        public override void Activate()
        {
            if (EventSystem.current != null)
            {
                EventSystem.current.SetSelectedGameObject(null);
            }
            base.Activate();
        }


    }
}
