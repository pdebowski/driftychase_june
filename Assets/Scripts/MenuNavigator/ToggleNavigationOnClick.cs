﻿using UnityEngine;
using System.Collections;

namespace MenuNavigation
{


    public class ToggleNavigationOnClick : MonoBehaviour
    {
        [SerializeField]
        private NavigationElement element;
        [SerializeField]
        private Navigation navigationAfterClick;

        private Navigation originalNavigation;
        private bool isOriginal = true;

        public void Toggle()
        {
            if (isOriginal)
            {
                originalNavigation = element.GetNavigation();
                element.SetNavigation(navigationAfterClick);
            }
            else
            {
                element.SetNavigation(originalNavigation);
            }

            isOriginal = !isOriginal;
        }

    }
}