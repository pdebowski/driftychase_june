﻿using UnityEngine;
using System.Collections;

namespace MenuNavigation
{
    public class ScreenScope : Scope
    {

        protected override void Awake()
        {
            base.Awake();
            activateAlwaysDefaultElement = true;
            blockScopesHierarchy = true;
        }

    }
}
