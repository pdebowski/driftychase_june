﻿using UnityEngine;
using System.Collections;
using System;

namespace MenuNavigation
{
    public class InputManager : MonoBehaviour
    {
        private const float DEATH_AREA = 0.5f;

        [SerializeField]
        private bool turnOffKeyboard_PadInput = false;
        
        [SerializeField]
        private float[] clickFrequency;


        private int frequencyCurrentIndex = -1;
        private float pressTime = 0;
        private NavigationDirectionV2 lastDirection;
        private bool isInputReaded = false;
        private PadController padController;

        #region Game_Depend_Code

        void Awake()
        {
            TVDetector.OnTVCheck += DestroyIfNotTV;

            AppleRemoteTouch.OnExecuteDirection += ForceTriggerNavigation;
            AppleRemoteController.OnButtonClick += ForceTriggerAction;
            padController = PadController.Instance;
    }

        private void DestroyIfNotTV(bool isTV)
        {
            if (!isTV)
            {
                TVDetector.OnTVCheck -= DestroyIfNotTV;
                Destroy(this.gameObject);
            }
        }

        public void ForceTriggerNavigation(MenuNavigation.NavigationDirectionV2 direction)
        {
            lastDirection = direction;
            FirePerformDirection();
        }

        public void ForceTriggerAction(AppleRemoteController.Buttons button)
        {
            if (NavigationController.Instance != null && button == AppleRemoteController.Buttons.TouchButton)
            {
                NavigationController.Instance.PerformActionClick();
            }
        }
        #endregion

#if !UNITY_TVOS
        void Update()
        {
            if (PlatformRecognition.IsTV)
            {
                if (turnOffKeyboard_PadInput)
                {
                    return;
                }

                AnalyzeNavigationInput();

                PrepareAndSendNavigation();

                if (IsActionPerformClick())
                {
                    if (NavigationController.Instance != null)
                    {
                        NavigationController.Instance.PerformActionClick();
                    }
                }
            }
        }
#endif

        private void AnalyzeNavigationInput()
        {
            isInputReaded = false;
            DigitalInput();
            AnalogInput();
        }

        private void DigitalInput()
        {
            IsLeftPressed();
            IsRightPressed();
            IsUpPressed();
            IsDownPressed();
        }

        private void AnalogInput()
        {
            if (isInputReaded)
            {
                return;
            }

            NavigationDirectionV2 direction = GetAnalogDirection();
            if (direction != NavigationDirectionV2.NONE)
            {
                ManageDirectionInput(true, direction);
            }
            else
            {
                ManageDirectionInput(false, direction);
            }
        }

        private void PrepareAndSendNavigation()
        {
            if (pressTime > clickFrequency[frequencyCurrentIndex + 1])
            {
                frequencyCurrentIndex++;
                frequencyCurrentIndex = Mathf.Clamp(frequencyCurrentIndex, -1, clickFrequency.Length - 2);
                FirePerformDirection();
                pressTime = 0;
            }
        }

        private void FirePerformDirection()
        {
            if (NavigationController.Instance != null)
            {
                if (CheckIfIsDigital(lastDirection))
                {
                    NavigationDirection digitalDirection = ConvertToDigitalDirection(lastDirection);
                    NavigationController.Instance.PerformNavigation(CreateNavigationInfo(digitalDirection));
                }
                else
                {
                    NavigationController.Instance.PerformNavigation(CreateNavigationInfo(lastDirection));
                }
            }
        }

        private NavigationDirection ConvertToDigitalDirection(NavigationDirectionV2 direction)
        {
            switch (direction)
            {
                case NavigationDirectionV2.NN:
                    return NavigationDirection.Up;
                case NavigationDirectionV2.EE:
                    return NavigationDirection.Right;
                case NavigationDirectionV2.WW:
                    return NavigationDirection.Left;
                case NavigationDirectionV2.SS:
                    return NavigationDirection.Down;
                default:
                    return NavigationDirection.Up;
            }
        }

        private bool CheckIfIsDigital(NavigationDirectionV2 direction)
        {
            switch (direction)
            {

                case NavigationDirectionV2.NN:
                    return true;
                case NavigationDirectionV2.EE:
                    return true;
                case NavigationDirectionV2.WW:
                    return true;
                case NavigationDirectionV2.SS:
                    return true;
                default:
                    return false;
            }
        }

        private NavigationInfo CreateNavigationInfo(NavigationDirection direction)
        {
            NavigationInfo navigationInfo = new NavigationInfo();
            navigationInfo.Direction = direction;
            return navigationInfo;
        }

        private NavigationInfoV2 CreateNavigationInfo(NavigationDirectionV2 direction)
        {
            NavigationInfoV2 navigationInfo = new NavigationInfoV2();
            navigationInfo.Direction = direction;
            return navigationInfo;
        }

        private bool IsLeftPressed()
        {
            if (isInputReaded)
            {
                return false;
            }

            bool result = IsPressedDigital(KeyCode.LeftArrow,
                ControllerButtons.LEFT);

            ManageDirectionInput(result, NavigationDirectionV2.WW);

            return result;
        }
        private bool IsUpPressed()
        {
            if (isInputReaded)
            {
                return false;
            }

            bool result = IsPressedDigital(KeyCode.UpArrow,
                ControllerButtons.UP);

            ManageDirectionInput(result, NavigationDirectionV2.NN);

            return result;
        }
        private bool IsRightPressed()
        {
            if (isInputReaded)
            {
                return false;
            }

            bool result = IsPressedDigital(KeyCode.RightArrow,
                ControllerButtons.RIGHT);

            ManageDirectionInput(result, NavigationDirectionV2.EE);

            return result;
        }
        private bool IsDownPressed()
        {
            if (isInputReaded)
            {
                return false;
            }

            bool result = IsPressedDigital(KeyCode.DownArrow,
                ControllerButtons.DOWN);

            ManageDirectionInput(result, NavigationDirectionV2.SS);

            return result;
        }


        private NavigationDirectionV2 ManageDirectionInput(bool isPressed, NavigationDirectionV2 direction)
        {
            if (isPressed)
            {
                isInputReaded = true;
                if (direction == lastDirection)
                {
                    pressTime += Time.unscaledDeltaTime;
                }
                else
                {
                    pressTime = 0;
                    frequencyCurrentIndex = -1;
                }
                lastDirection = direction;
            }
            else
            {
                if (direction == lastDirection)
                {
                    pressTime = 0;
                    frequencyCurrentIndex = -1;
                }
            }
            return lastDirection;
        }

        private bool IsPressedDigital(KeyCode key, ControllerButtons button)
        {
            return (Input.GetKey(key)
                || padController.isDown(button));
        }

        private NavigationDirectionV2 GetAnalogDirection()
        {
            float leftXValue = padController.getAnalog(ControllerAnalogs.LEFTX);
            float leftYValue = padController.getAnalog(ControllerAnalogs.LEFTY);
            float rightXValue = padController.getAnalog(ControllerAnalogs.RIGHTX);
            float rightYValue = padController.getAnalog(ControllerAnalogs.RIGHTY);
            NavigationDirectionV2 result = NavigationDirectionV2.NONE;
            result = CheckNextQuarter(result, leftXValue, leftYValue, 1, -1, NavigationDirectionV2.NEN, NavigationDirectionV2.ENE);
            result = CheckNextQuarter(result, leftXValue, leftYValue, 1, 1, NavigationDirectionV2.SES, NavigationDirectionV2.ESE);
            result = CheckNextQuarter(result, leftXValue, leftYValue, -1, -1, NavigationDirectionV2.NWN, NavigationDirectionV2.WNW);
            result = CheckNextQuarter(result, leftXValue, leftYValue, -1, 1, NavigationDirectionV2.SWS, NavigationDirectionV2.WSW);
            result = CheckNextQuarter(result, rightXValue, rightYValue, 1, -1, NavigationDirectionV2.NEN, NavigationDirectionV2.ENE);
            result = CheckNextQuarter(result, rightXValue, rightYValue, 1, 1, NavigationDirectionV2.SES, NavigationDirectionV2.ESE);
            result = CheckNextQuarter(result, rightXValue, rightYValue, -1, -1, NavigationDirectionV2.NWN, NavigationDirectionV2.WNW);
            result = CheckNextQuarter(result, rightXValue, rightYValue, -1, 1, NavigationDirectionV2.SWS, NavigationDirectionV2.WSW);
            return result;
        }

        private NavigationDirectionV2 CheckNextQuarter(NavigationDirectionV2 lastValue, float xValue, float yValue, int xSign, int ySign, NavigationDirectionV2 upper, NavigationDirectionV2 down)
        {
            NavigationDirectionV2 result = GetOneQuarter(xValue, yValue, xSign, ySign, upper, down);
            if (result != NavigationDirectionV2.NONE)
            {
                return result;
            }
            else
            {
                return lastValue;
            }
        }

        private NavigationDirectionV2 GetOneQuarter(float xValue, float yValue, int xSign, int ySign, NavigationDirectionV2 upper, NavigationDirectionV2 down)
        {
            xValue *= xSign;
            yValue *= ySign;
            if (xValue >= 0 && yValue >= 0 && (xValue >= DEATH_AREA || yValue >= DEATH_AREA))
            {
                if (xValue > yValue)
                {
                    return down;
                }
                else
                {
                    return upper;
                }
            }

            return NavigationDirectionV2.NONE;
        }




        private bool IsActionPerformClick()
        {
            return Input.GetKeyDown(KeyCode.Return)
                || padController.isPressed(ControllerButtons.BUTA);
        }

        // DEBUG METHONDS 
        public void N()
        {
            if (NavigationController.Instance != null)
            {
                NavigationController.Instance.PerformNavigation(CreateNavigationInfo(NavigationDirectionV2.N));
            }
        }

        public void S()
        {
            if (NavigationController.Instance != null)
            {
                NavigationController.Instance.PerformNavigation(CreateNavigationInfo(NavigationDirectionV2.S));
            }
        }
        public void W()
        {
            if (NavigationController.Instance != null)
            {
                NavigationController.Instance.PerformNavigation(CreateNavigationInfo(NavigationDirectionV2.W));
            }
        }
        public void E()
        {
            if (NavigationController.Instance != null)
            {
                NavigationController.Instance.PerformNavigation(CreateNavigationInfo(NavigationDirectionV2.E));
            }
        }

        public void NN()
        {
            lastDirection = NavigationDirectionV2.NN;
            FirePerformDirection();
        }
        public void SS()
        {
            lastDirection = NavigationDirectionV2.SS;
            FirePerformDirection();
        }
        public void WW()
        {
            lastDirection = NavigationDirectionV2.WW;
            FirePerformDirection();
        }
        public void EE()
        {
            lastDirection = NavigationDirectionV2.EE;
            FirePerformDirection();
        }

        public void NWN()
        {
            if (NavigationController.Instance != null)
            {
                NavigationController.Instance.PerformNavigation(CreateNavigationInfo(NavigationDirectionV2.NWN));
            }
        }

        public void NEN()
        {
            if (NavigationController.Instance != null)
            {
                NavigationController.Instance.PerformNavigation(CreateNavigationInfo(NavigationDirectionV2.NEN));
            }
        }
        public void ENE()
        {
            if (NavigationController.Instance != null)
            {
                NavigationController.Instance.PerformNavigation(CreateNavigationInfo(NavigationDirectionV2.ENE));
            }
        }
        public void ESE()
        {
            if (NavigationController.Instance != null)
            {
                NavigationController.Instance.PerformNavigation(CreateNavigationInfo(NavigationDirectionV2.ESE));
            }
        }

        public void WNW()
        {
            if (NavigationController.Instance != null)
            {
                NavigationController.Instance.PerformNavigation(CreateNavigationInfo(NavigationDirectionV2.WNW));
            }
        }
        public void WSW()
        {
            if (NavigationController.Instance != null)
            {
                NavigationController.Instance.PerformNavigation(CreateNavigationInfo(NavigationDirectionV2.WSW));
            }
        }
        public void SWS()
        {
            if (NavigationController.Instance != null)
            {
                NavigationController.Instance.PerformNavigation(CreateNavigationInfo(NavigationDirectionV2.SWS));
            }
        }
        public void SES()
        {
            if (NavigationController.Instance != null)
            {
                NavigationController.Instance.PerformNavigation(CreateNavigationInfo(NavigationDirectionV2.SES));
            }
        }
        public void SE()
        {
            if (NavigationController.Instance != null)
            {
                NavigationController.Instance.PerformNavigation(CreateNavigationInfo(NavigationDirectionV2.SE));
            }
        }
        public void SW()
        {
            if (NavigationController.Instance != null)
            {
                NavigationController.Instance.PerformNavigation(CreateNavigationInfo(NavigationDirectionV2.SW));
            }

        }
        public void NE()
        {
            if (NavigationController.Instance != null)
            {
                NavigationController.Instance.PerformNavigation(CreateNavigationInfo(NavigationDirectionV2.NE));
            }
        }
        public void NW()
        {
            if (NavigationController.Instance != null)
            {
                NavigationController.Instance.PerformNavigation(CreateNavigationInfo(NavigationDirectionV2.NW));
            }
        }
    }


}