﻿using UnityEngine;
using System.Collections;

namespace MenuNavigation
{
    public class ForceSelection : MonoBehaviour
    {
        [SerializeField]
        private NavigationElement elementToSelect;


        public void ForceSelect()
        {
            NavigationController.Instance.Activate(elementToSelect);
        }



    }
}
