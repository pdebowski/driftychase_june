﻿using UnityEngine;
using System.Collections;
namespace MenuNavigation
{
    public class DriveButton : Button
    {
        public override void PerformActionClick()
        {

            if (PlayerPrefsAdapter.WasStoreTutorialShown)
            {
                base.PerformActionClick();
            }
        }

        public override void PerformNavigation(NavigationInfoV2 navigationInfoV2)
        {
            if (PlayerPrefsAdapter.WasStoreTutorialShown || (navigationInfoV2.Direction == NavigationDirectionV2.ENE || navigationInfoV2.Direction == NavigationDirectionV2.ESE))
            {
                base.PerformNavigation(navigationInfoV2);
            }
        }


    }
}
