﻿using UnityEngine;
using System.Collections;
namespace MenuNavigation
{
    public class StoreButton : Button
    {

        private bool wasNavigationPerformed = false;
        private bool wasActionPerformed = false;

        public override void PerformActionClick()
        {
            if (PlayerPrefsAdapter.WasStoreTutorialShown)
            {
                wasActionPerformed = true;
                base.PerformActionClick();
            }
        }

        public override void PerformNavigation(NavigationInfoV2 navigationInfoV2)
        {
            if (Utilities.IsMatch(NavigationDirectionV2.WW, navigationInfoV2.Direction) || Utilities.IsMatch(NavigationDirectionV2.NN, navigationInfoV2.Direction) || Utilities.IsMatch(NavigationDirectionV2.EE, navigationInfoV2.Direction))
            {
                wasNavigationPerformed = true;
                GetScope().PerformNavigation(navigationInfoV2);
            }

        }

        void OnDisable()
        {

            if (button != null)
            {
                button.onClick.RemoveListener(EndTutorialIfAvailable);
            }

            if (alternativeElement != null && wasActionPerformed)
            {
                if (NavigationController.Instance != null && NavigationController.Instance.ActiveElement == this)
                {
                    NavigationController.Instance.Activate(alternativeElement);
                }
                wasActionPerformed = false;
            }
        }

    }
}
