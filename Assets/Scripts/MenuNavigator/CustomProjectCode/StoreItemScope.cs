﻿using UnityEngine;
using System.Collections;

namespace MenuNavigation
{


    public class StoreItemScope : ContainerScope
    {
        private StoreManager storeManager;

        private void SetStoreManager()
        {
            if (storeManager == null)
            {
                storeManager = GetComponentInParent<StoreManager>();
            }
        }

        public override void PerformNavigation(NavigationInfoV2 navigationInfo)
        {
            SetStoreManager();

            if (Utilities.IsMatch(NavigationDirectionV2.EE, navigationInfo.Direction))
            {
                storeManager.ShowNextCar();
                return;
            }

            if (Utilities.IsMatch(NavigationDirectionV2.WW, navigationInfo.Direction))
            {
                storeManager.ShowPreviousCar();
                return;
            }

            if (PlayerPrefsAdapter.WasStoreTutorialShown)
            {
                base.PerformNavigation(navigationInfo);
            }
        }

        public override void PerformNavigation(NavigationInfo navigationInfo)
        {
            SetStoreManager();

            if (navigationInfo.Direction == NavigationDirection.Right)
            {
                storeManager.ShowNextCar();
                return;
            }

            if (navigationInfo.Direction == NavigationDirection.Left)
            {
                storeManager.ShowPreviousCar();
                return;
            }

            if (PlayerPrefsAdapter.WasStoreTutorialShown)
            {
                base.PerformNavigation(navigationInfo);
            }
        }

        public override void Activate()
        {
            base.Activate();
        }
    }
}
