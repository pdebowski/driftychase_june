﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

namespace MenuNavigation
{
    [RequireComponent(typeof(UnityEngine.UI.Button))]
    public class ButtonWithEvents : ClickableElement
    {
        [Header("Button")]
        [SerializeField]
        private NavigationElement alternativeElement = null;

        protected UnityEngine.UI.Button button;
        private bool wasClick = false;

        protected override void Awake()
        {
            base.Awake();
            button = GetComponent<UnityEngine.UI.Button>();
        }


        void OnDisable()
        {

            if (wasClick == true && alternativeElement != null)
            {
                if (NavigationController.Instance != null)
                {
                    NavigationController.Instance.Activate(alternativeElement);
                }
                wasClick = false;
            }
        }

        public override void Activate()
        {
            base.Activate();
        }

        public override void PerformActionClick()
        {
            base.PerformActionClick();

            wasClick = true;

            var pointer = new PointerEventData(EventSystem.current);
            ExecuteEvents.Execute(button.gameObject, pointer, ExecuteEvents.pointerDownHandler);
            ExecuteEvents.Execute(button.gameObject, pointer, ExecuteEvents.pointerUpHandler);
            EndTutorialIfAvailable();
        }
    }
}

