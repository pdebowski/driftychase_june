﻿using UnityEngine;
using System.Collections;

namespace MenuNavigation
{
    public class ContainerScope : Scope
    {
        [SerializeField]
        private bool ActivateAlwaysDefaultElement = false;

        protected override void Awake()
        {
            base.Awake();
            activateAlwaysDefaultElement = ActivateAlwaysDefaultElement;
            blockScopesHierarchy = false;
        }
    }
}