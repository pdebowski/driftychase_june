﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class OffersManager : MonoBehaviour
{
    public const string OFFER_RESOURCES_PATH = "Offers\\{0}Offer";

    [SerializeField]
    private List<IsReadyAble> offersList;
    [SerializeField]
    private List<IsReadyAble> prioritizedOffersList;
    [SerializeField]
    private IsReadyAble appleTVBankOffer;

    private IsReadyAble choosedOffer;

    public bool IsAnyOfferReady()
    {
		if(PlayerPrefsAdapter.ForceVIPOffer)
		{
			IsReadyAble result = offersList.Find(x => x.name == "VIP");
			result.ChooseOffer();
			choosedOffer = result;
            return true;
        }

        foreach (IsReadyAble item in prioritizedOffersList)
        {
            if (item.IsReady())
            {
                choosedOffer = ChooseOffer(true);
                return true;
            }
        }

        foreach (IsReadyAble item in offersList)
        {
            if (item.IsReady())
            {
                choosedOffer = ChooseOffer(false);
                return true;
            }
        }

        return false;
    }

    private IsReadyAble ChooseOffer(bool prioritized)
    {
        List<IsReadyAble> offersWithWeightsList = new List<IsReadyAble>();
        List<IsReadyAble> offerListToSearch = prioritized ? prioritizedOffersList : offersList;

        foreach (IsReadyAble item in offerListToSearch)
        {
            if (item.IsReady())
            {
                for (int i = 0; i < item.weight; i++)
                {
                    offersWithWeightsList.Add(item);
                }
            }
        }

        IsReadyAble result;
        if (OffersSceneManager.OffersDisplayedCount == 3 && GTMParameters.PreReleaseMode && !PlatformRecognition.IsTV && PlatformRecognition.IsIOSMobile)
        {
            result = appleTVBankOffer;
        }
        else
        {
            result = offersWithWeightsList.RandomElement();
        }

        result.ChooseOffer();
        return result;
    }

    public void ShowOffer()
    {
        choosedOffer.SetActiveOffer();
    }
}
