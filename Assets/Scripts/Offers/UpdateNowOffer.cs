﻿using UnityEngine;
using System.Collections;

public class UpdateNowOffer : IsReadyAble
{

    [SerializeField]
    private OffersSceneManager offerSceneManager;

    private GameObject holder;

    public override bool IsReady()
    {
        return PlayerPrefsAdapter.TotalGameplayTime.TotalMinutes > 1 && CommonMethods.IsFirstVersionHigherThanSecond(GTMParameters.LiveVersionNumber,Application.version);
    }

    public override void SetActiveOffer()
    {
        holder = GameObject.Instantiate(Resources.Load(string.Format(OffersManager.OFFER_RESOURCES_PATH, TVFilter.GetRealName(gameObject.name)))) as GameObject;
        holder.transform.SetParent(this.transform, false);
        holder.SetActive(!true);
        holder.SetActive(true);

        Offer offer = holder.GetComponent<Offer>();
        offerSceneManager.InitExitOfferButton(offer);
        offer.OKButton.onClick.AddListener(OnUpdateNowOfferClicked);
    }

    public void OnUpdateNowOfferClicked()
    {
#if UNITY_IOS
		Application.OpenURL("itms-apps://itunes.apple.com/app/id1145500015");
#else
        Application.OpenURL("market://details?id=com.crimsonpine.driftychase");
#endif
        offerSceneManager.OnNextButtonClick();
    }
}
