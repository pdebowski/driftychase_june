﻿using UnityEngine;
using System.Collections;

public abstract class IsReadyAble : MonoBehaviour
{

    public abstract bool IsReady();
    public abstract void SetActiveOffer();
    [SerializeField]
    public int weight;

    private bool choosed;

    public virtual void ChooseOffer()
    {
        choosed = true;
    }
}
