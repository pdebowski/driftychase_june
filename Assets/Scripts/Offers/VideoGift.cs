﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using System.Collections.Generic;

public class VideoGift : IsReadyAble
{
    [SerializeField]
    private OffersSceneManager offerSceneManager;

    private GameObject holder;

    public override bool IsReady()
    {
        return true;
    }

    public override void ChooseOffer()
    {
        base.ChooseOffer();

        if (!RewardAd.IsReady())
        {
            offerSceneManager.isFakeOffer = true;
        }
    }

    public override void SetActiveOffer()
    {
        holder = GameObject.Instantiate(Resources.Load(string.Format(OffersManager.OFFER_RESOURCES_PATH, TVFilter.GetRealName(gameObject.name)))) as GameObject;
        holder.transform.SetParent(this.transform, false);
        holder.SetActive(false); //bug fix// without this line holder some time is not displaying 
        holder.SetActive(true);

        Offer offer = holder.GetComponent<Offer>();
        offerSceneManager.InitExitOfferButton(offer);
    }

    public void OnPopupExit()
    {
        offerSceneManager.OnNextButtonClick();
    }
}

