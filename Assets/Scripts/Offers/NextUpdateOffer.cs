﻿using UnityEngine;
using System.Collections;

public class NextUpdateOffer : IsReadyAble
{

    [SerializeField]
    private OffersSceneManager offerSceneManager;

    private GameObject holder;

    public override bool IsReady()
    {
        return PlayerPrefsAdapter.TotalGameplayTime.TotalMinutes > 5 && Application.version == GTMParameters.LiveVersionNumber && GTMParameters.NextUpdateReady;
    }

    public override void SetActiveOffer()
    {
        holder = GameObject.Instantiate(Resources.Load(string.Format(OffersManager.OFFER_RESOURCES_PATH, TVFilter.GetRealName(gameObject.name)))) as GameObject;
        holder.transform.SetParent(this.transform, false);
        holder.SetActive(!true);
        holder.SetActive(true);

        Offer offer = holder.GetComponent<Offer>();
        offerSceneManager.InitExitOfferButton(offer);

    }
}
