﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class RateMe : IsReadyAble
{
    [SerializeField]
    private OffersSceneManager offerSceneManager;

    private GameObject holder;

    public override bool IsReady()
    {
        return !PlayerPrefsAdapter.WasRateMeClicked && PlayerPrefsAdapter.TotalGameplayTime.TotalMinutes > 5 && PlayerPrefsAdapter.NewWorldSeenLastSession;
    }

    public override void SetActiveOffer()
    {
        holder = GameObject.Instantiate(Resources.Load(string.Format(OffersManager.OFFER_RESOURCES_PATH, TVFilter.GetRealName(gameObject.name)))) as GameObject;
        holder.transform.SetParent(this.transform, false);
        holder.SetActive(false);
        holder.SetActive(true);

        Offer offer = holder.GetComponent<Offer>();
        offerSceneManager.InitExitOfferButton(offer);
        offer.OKButton.onClick.AddListener(OnRateMeButtonClick);
    }

    public void OnRateMeButtonClick()
    {
        PlayerPrefsAdapter.WasRateMeClicked = true;
#if UNITY_IOS
		Application.OpenURL("itms-apps://itunes.apple.com/app/id1145500015");
#else
        Application.OpenURL("market://details?id=com.crimsonpine.driftychase");
#endif
        offerSceneManager.OnNextButtonClick();
    }
}
