﻿using UnityEngine;
using System.Collections;

public class CheckpointOffer : IsReadyAble
{

    [SerializeField]
    private OffersSceneManager offerSceneManager;

    private GameObject holder;

    public override bool IsReady()
    {
        return !PrefabReferences.Instance.CheckpointsManager.IsCheckpointTimeLeft()
        && RewardAd.IsReady()
        && PrefabReferences.Instance.CarStatsDB.GetCarIndex(PlayerPrefsAdapter.SelectedCar) > 0
        && !VIP.IsVIPAccount;
    }

    public override void SetActiveOffer()
    {
        holder = GameObject.Instantiate(Resources.Load(string.Format(OffersManager.OFFER_RESOURCES_PATH, TVFilter.GetRealName(gameObject.name)))) as GameObject;
        holder.transform.SetParent(this.transform, false);
        holder.SetActive(false);
        holder.SetActive(true);

        CrimsonAnalytics.Offers.ActivateCheckpointInOffers.Displayed.LogEvent();
        Offer offer = holder.GetComponent<Offer>();
        offerSceneManager.InitExitOfferButton(offer);
        offer.ExitButton.onClick.AddListener(OnExitButtonClicked);
        offer.OKButton.onClick.AddListener(OnActivateCheckpointButtonClick);
    }

    private void OnExitButtonClicked()
    {
        CrimsonAnalytics.Offers.ActivateCheckpointInOffers.Skipped.LogEvent();
    }

    public void OnActivateCheckpointButtonClick()
    {
		PrefabReferences.Instance.GameSceneManager.RewardedAdShow ();
        RewardAd.Show(null, ActivateCheckpointWithAdReward);
    }

    public void ActivateCheckpointWithAdReward(bool wasFullyWatched)
    {
        if (wasFullyWatched)
        {
            CrimsonAnalytics.Transaction.RewardedVideo.Displayed.LogEvent();
            CrimsonAnalytics.Offers.ActivateCheckpointInOffers.Collected.LogEvent();
            PrefabReferences.Instance.CheckpointsManager.SetIsCheckpointActivated(PlayerPrefsAdapter.SelectedCar, true);
            PrefabReferences.Instance.CheckpointsManager.SetCheckpointActivatedTimeNow();
            offerSceneManager.OnNextButtonClick();
        }
		PrefabReferences.Instance.GameSceneManager.ClosedRewardedAd (wasFullyWatched);
    }
}
