﻿using UnityEngine;
using System.Collections;

public class AppleTVBank : IsReadyAble
{

    [SerializeField]
    private OffersSceneManager offerSceneManager;

    private GameObject holder;

    public override bool IsReady()
    {
#if UNITY_ANDROID
        return false;
#else
        return PlayerPrefsAdapter.TotalGameplayTime.TotalMinutes > 2 && 0.4f < Random.value;
#endif
    }

    public override void SetActiveOffer()
    {
        holder = GameObject.Instantiate(Resources.Load(string.Format(OffersManager.OFFER_RESOURCES_PATH, TVFilter.GetRealName(gameObject.name)))) as GameObject;
        holder.transform.SetParent(this.transform, false);
        holder.SetActive(!true);
        holder.SetActive(true);

        Offer offer = holder.GetComponent<Offer>();
        offerSceneManager.InitExitOfferButton(offer);

    }
}
