﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class TVOSDailyGiftCounter : MonoBehaviour
{

    [SerializeField]
    private string availableText = "READY";
    [SerializeField]
    private Text counterText;
    [SerializeField]
    private GameObject progressOffer;
    [SerializeField]
    private GameObject tvBankIsReadyOffer;

    private DateTime nextGiftTime;

    void Awake()
    {
        string nextGiftTimeInString = PrefsDataFacade.Instance.GetTVOSNextFreeGiftTime();
        if (nextGiftTimeInString != null && nextGiftTimeInString != "NO_VALUE")
        {
            nextGiftTime = DateTime.Parse(nextGiftTimeInString);
            if (IsAvailable())
            {
                tvBankIsReadyOffer.SetActive(true);
                progressOffer.SetActive(false);
            }
            else
            {
                progressOffer.SetActive(true);
                tvBankIsReadyOffer.SetActive(false);
            }
        }
        else
        {
            progressOffer.SetActive(true);
            tvBankIsReadyOffer.SetActive(false);
            nextGiftTime = DateTime.UtcNow;
        }

    }

    void Update()
    {

        if (IsAvailable())
        {
            counterText.text = string.Format("<color=#FFFFFFFF>{0}</color>", availableText);
        }
        else
        {
            counterText.text = string.Format("<color=#FFFFFFFF>{0}</color>", FormatTimeSpan(GetTimeToNextGift()));
        }
    }

    private bool IsAvailable()
    {
        return GetTimeToNextGift().TotalSeconds <= 0;
    }

    private TimeSpan GetTimeToNextGift()
    {
        return nextGiftTime - System.DateTime.UtcNow;
    }

    private string FormatTimeSpan(TimeSpan span)
    {
        return ((span.Hours) + ":" + string.Format("{0:00}", span.Minutes) + ":" + string.Format("{0:00}", span.Seconds));
    }
}
