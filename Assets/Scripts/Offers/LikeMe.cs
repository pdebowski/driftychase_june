﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class LikeMe : IsReadyAble
{


    [SerializeField]
    private OffersSceneManager offerSceneManager;

    private GameObject holder;

    public override bool IsReady()
    {
        return !PlayerPrefsAdapter.WasLikeMeClicked && PlayerPrefsAdapter.TotalGameplayTime.TotalMinutes > 10 && !PlatformRecognition.IsTV;
    }

    public override void SetActiveOffer()
    {
        holder = GameObject.Instantiate(Resources.Load(string.Format(OffersManager.OFFER_RESOURCES_PATH, TVFilter.GetRealName(gameObject.name)))) as GameObject;
        holder.transform.SetParent(this.transform, false);
        holder.SetActive(false);
        holder.SetActive(true);

        Offer offer = holder.GetComponent<Offer>();
        offerSceneManager.InitExitOfferButton(offer);
        offer.OKButton.onClick.AddListener(OnLikeMeButtonClick);
    }

    public void OnLikeMeButtonClick()
    {
        PlayerPrefsAdapter.WasLikeMeClicked = true;
        Application.OpenURL("http://www.facebook.com/crimsonpine");
        offerSceneManager.OnNextButtonClick();
    }
}
