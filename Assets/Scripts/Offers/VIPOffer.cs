﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;


public class VIPOffer : IsReadyAble
{

    [SerializeField]
    private OffersSceneManager offerSceneManager;
    [SerializeField]
    private GameObject loadingObject;

    private GameObject holder;

    void OnEnable()
    {
        IAPManager.OnPurchased += offerSceneManager.OnNextButtonClick;
    }

    void OnDisable()
    {
        IAPManager.OnPurchased -= offerSceneManager.OnNextButtonClick;
    }

    public override bool IsReady()
    {
        return !VIP.IsVIPAccount && PlayerPrefsAdapter.TotalGameplayTime.TotalMinutes > 7;
    }

    public override void SetActiveOffer()
    {
        holder = GameObject.Instantiate(Resources.Load(string.Format(OffersManager.OFFER_RESOURCES_PATH, TVFilter.GetRealName(gameObject.name)))) as GameObject;
        holder.transform.SetParent(this.transform, false);
        holder.SetActive(false);
        holder.SetActive(true);

        Offer offer = holder.GetComponent<Offer>();
        offerSceneManager.InitExitOfferButton(offer);
        offer.OKButton.onClick.AddListener(OnOkButtonClick);
    }

    public void OnOkButtonClick()
    {
        PrefabReferences.Instance.IAPManager.SetLoadingObject(loadingObject);
        PrefabReferences.Instance.IAPManager.BuyProduct(IAPManager.Products.VipAccount);
    }
}
