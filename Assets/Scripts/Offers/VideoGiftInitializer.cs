using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class VideoGiftInitializer : MonoBehaviour
{

    [SerializeField]
    private List<VideoGiftVariation> variations;
    [SerializeField]
    private Text coinsRewardText;
    [SerializeField]
    private Image giftImage;
    [SerializeField]
    private GameObject robOffer;
    [SerializeField]
    private GameObject popupHolder;

    private int coinsReward;
    private VideoGiftVariation selectedVariation;
    private bool wasOfferRewarded = false;

    void Awake()
    {
        CrimsonAnalytics.Offers.VideoGift.Displayed.LogEvent();
        foreach (var variation in variations)
        {
            variation.popup.SetActive(false);
        }
        popupHolder.SetActive(false);
        SetRandomVariation();
        robOffer.SetActive(true);
    }

    private void SetRandomVariation()
    {
        selectedVariation = variations.RandomElement();

        CarStats bestOwningCar = PrefabReferences.Instance.CarStatsDB.GetBestOwnedCar();
        int minVideoGiftValue = Mathf.RoundToInt(bestOwningCar.videoGiftMinValue);
        int maxVideoGiftValue = Mathf.RoundToInt(bestOwningCar.videoGiftMaxValue);
        float giftValue = Mathf.Lerp(minVideoGiftValue, maxVideoGiftValue, selectedVariation.giftFactor);
        coinsReward = CommonMethods.RoundCash(Mathf.RoundToInt(giftValue));
        coinsRewardText.text = CommonMethods.GetCashStringWithSpacing(coinsReward) + "   &";
        giftImage.sprite = selectedVariation.sprite;
    }

    public void OnOkButtonClick()
    {
        if (!wasOfferRewarded)
        {
            wasOfferRewarded = true;
            PrefabReferences.Instance.GameSceneManager.RewardedAdShow();
            RewardAd.Show(null, Reward);
        }
    }

    private void Reward(bool wasFullyWatched)
    {
        if (wasFullyWatched)
        {
            CrimsonAnalytics.Transaction.RewardedVideo.Displayed.LogEvent();
            CrimsonAnalytics.Offers.VideoGift.Collected.LogEvent();
            CrimsonAnalytics.Economy.CurrencySources.VideoGift.LogEvent(coinsReward);
            robOffer.SetActive(false);
            ScoreCounter.Instance.AddCash(coinsReward);
            popupHolder.SetActive(true);
            selectedVariation.popup.SetActive(true);
			PrefsDataFacade.Instance.ForceSaveAfterPause();
        }
        else
        {
            OnPopupClick();
        }
        PrefabReferences.Instance.GameSceneManager.ClosedRewardedAd(wasFullyWatched);
    }

    public void OnExitClick()
    {
        CrimsonAnalytics.Offers.VideoGift.Skipped.LogEvent();
    }

    public void OnPopupClick()
    {
        GetComponentInParent<VideoGift>().OnPopupExit();
    }
}

[System.Serializable]
public class VideoGiftVariation
{
    public Sprite sprite;
    [Range(0.0f, 1.0f)]
    public float giftFactor;
    public GameObject popup;
}
