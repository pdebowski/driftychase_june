﻿using UnityEngine;
using System.Collections;
//singleton One Scene
abstract public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
{

    private static T _instance = null;

    public static T Instance { get { return _instance; } }

    public void Awake()
    {
        if (_instance != null)
        {
            //Debug.Log("Trying to create second instance of " + gameObject.name + " - destroying second Object");
            Destroy(gameObject);
            return;
        }

        if (!(this is T)) Debug.LogError("Error type");
        _instance = this.gameObject.GetComponent<T>();

        Awakened();
    }

    protected virtual void Awakened()
    {
    }

}
