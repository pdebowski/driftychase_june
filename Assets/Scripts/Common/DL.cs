﻿using UnityEngine;
using System.Collections;

public class DL : MonoBehaviour {

    public static bool ForceLogs = false;

    public static void Log(object message)
    {
        if(IsAvailable())
        {
            Debug.Log(message);
        }
    }

    public static void LogWarning(object message)
    {
        if (IsAvailable())
        {
            Debug.LogWarning(message);
        }
    }

    public static void LogError(object message)
    {
        if (IsAvailable())
        {
            Debug.LogError(message);
        }
    }

    private static bool IsAvailable()
    {
        return Debug.isDebugBuild || ForceLogs;
    }
	
}
