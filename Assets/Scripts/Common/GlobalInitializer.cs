using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GlobalInitializer : Singleton<GlobalInitializer>
{
    public List<Transform> m_globalsPrefab;


    protected override void Awakened()
    {
        base.Awakened();

        m_globalsPrefab.ForEach(v =>
        {
            Transform tr = Instantiate(v) as Transform;
            tr.SetParent(gameObject.transform);
        });
        DontDestroyOnLoad(gameObject);
    }
    //----------------------------------------------
    [ContextMenu("Delete Player Prefs")]
    void DeletePrefs()
    {
        PlayerPrefs.DeleteAll();
    }
}
