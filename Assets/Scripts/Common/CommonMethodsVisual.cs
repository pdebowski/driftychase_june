﻿using UnityEngine;
using System.Collections;

public class CommonMethodsVisual
{

    public static Texture2D GetTexture2D(RenderTexture renderTexture)
    {
        RenderTexture currentActiveRenderTex = RenderTexture.active;
        RenderTexture.active = renderTexture;
        Texture2D resultTexture = new Texture2D(renderTexture.width, renderTexture.height);
        resultTexture.ReadPixels(new Rect(0, 0, renderTexture.width, renderTexture.height), 0, 0);
        resultTexture.Apply();
        RenderTexture.active = currentActiveRenderTex;

        return resultTexture;
    }

    //Blur, faster for blurSize = 1 then currently used blur algorythm
    /*public static Texture2D Blur(Texture2D texture, int blurSize)
    {
        for (int xx = 0; xx < texture.width; xx++)
        {
            for (int yy = 0; yy < texture.height; yy++)
            {
                float avgR = 0, avgG = 0, avgB = 0;
                int blurPixelCount = 0;

                for (int x = xx; (x < xx + blurSize && x < texture.width); x++)
                {
                    for (int y = yy; (y < yy + blurSize && y < texture.height); y++)
                    {
                        Color pixel = texture.GetPixel(x, y);

                        avgR += pixel.r;
                        avgG += pixel.g;
                        avgB += pixel.b;

                        blurPixelCount++;
                    }
                }

                avgR = avgR / blurPixelCount;
                avgG = avgG / blurPixelCount;
                avgB = avgB / blurPixelCount;


                for (int x = xx; x < xx + blurSize && x < texture.width; x++)
                    for (int y = yy; y < yy + blurSize && y < texture.height; y++)
                        texture.SetPixel(x, y, new Color(avgR, avgG, avgB));

            }
        }
        texture.Apply();
        return texture;
    }*/

    public static Texture2D Blur(Texture2D texture, int blurSize)
    {
        var bxs = BoxesForGauss(blurSize, 3);
        int h = texture.height;
        int w = texture.width;

        Color[] sourceColors = texture.GetPixels();
        Color[] resultColors = new Color[w * h];

        BoxBlur(sourceColors, resultColors, w, h, (bxs[0] - 1) / 2);
        BoxBlur(resultColors, sourceColors, w, h, (bxs[1] - 1) / 2);
        BoxBlur(sourceColors, resultColors, w, h, (bxs[2] - 1) / 2);

        texture.SetPixels(resultColors);
        texture.Apply();
        return texture;
    }

    private static int[] BoxesForGauss(float sigma, int n)  // standard deviation, number of boxes
    {
        var wIdeal = Mathf.Sqrt((12 * sigma * sigma / n) + 1);  // Ideal averaging filter width 
        int wl = (int)Mathf.Floor(wIdeal);
        if (wl % 2 == 0)
        {
            wl--;
        }
        int wu = wl + 2;

        var mIdeal = (12 * sigma * sigma - n * wl * wl - 4 * n * wl - 3 * n) / (-4 * wl - 4);
        var m = Mathf.Round(mIdeal);

        var sizes = new int[n];
        for (var i = 0; i < n; i++)
            sizes[i] = (i < m ? wl : wu);
        return sizes;
    }

    private static void BoxBlur(Color[] scl, Color[] tcl, int w, int h, int r)
    {
        for (var i = 0; i < scl.Length; i++)
        {
            tcl[i].r = scl[i].r;
            tcl[i].g = scl[i].g;
            tcl[i].b = scl[i].b;
            tcl[i].a = scl[i].a;

        }
        BoxBlurH(tcl, scl, w, h, r);
        BoxBlurT(scl, tcl, w, h, r);
    }

    private static void BoxBlurH(Color[] scl, Color[] tcl, int w, int h, int r)
    {
        float iarr = 1.0f / (float)(r + r + 1);
        for (var i = 0; i < h; i++)
        {
            int ti = i * w;
            int li = ti;
            int ri = ti + r;
            Color fv = scl[ti], lv = scl[ti + w - 1], val = (r + 1) * fv;

            for (var j = 0; j < r; j++)
            {
                val += scl[ti + j];
            }

            for (var j = 0; j <= r; j++)
            {
                val += scl[ri++] - fv;
                tcl[ti++] = val * iarr;
            }

            for (var j = r + 1; j < w - r; j++)
            {
                val += scl[ri++] - scl[li++];
                tcl[ti++] = val * iarr;
            }

            for (var j = w - r; j < w; j++)
            {
                val += lv - scl[li++];
                tcl[ti++] = val * iarr;
            }
        }
    }

    private static void BoxBlurT(Color[] scl, Color[] tcl, int w, int h, int r)
    {
        float iarr = 1.0f / (float)(r + r + 1);
        for (var i = 0; i < w; i++)
        {
            int ti = i;
            int li = ti;
            int ri = ti + r * w;
            Color fv = scl[ti], lv = scl[ti + w * (h - 1)], val = (r + 1) * fv;

            for (var j = 0; j < r; j++)
            {
                val += scl[ti + j * w];
            }

            for (var j = 0; j <= r; j++)
            {
                val += scl[ri] - fv;
                tcl[ti] = val * iarr;
                ri += w;
                ti += w;
            }

            for (var j = r + 1; j < h - r; j++)
            {
                val += scl[ri] - scl[li];
                tcl[ti] = val * iarr;
                li += w; ri += w;
                ti += w;
            }

            for (var j = h - r; j < h; j++)
            {
                val += lv - scl[li];
                tcl[ti] = val * iarr;
                li += w;
                ti += w;
            }
        }
    }

}
