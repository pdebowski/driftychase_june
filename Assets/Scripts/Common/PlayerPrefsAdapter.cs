using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Linq;

public class PlayerPrefsAdapter
{
    private const string RECORD = "Record";
    private const string DAILY_GIFT_COLLECTED_TIME = "DAILY_GIFT_COLLECTED_TIME";
    private const string SFX_VALUE = "SFX";
    private const string MUSIC_VALUE = "Music";
    private const string SELECTED_CAR = "Selected_Car";
    private const string SELECTED_CAR_NEW = "Selected_Car_NEW";
    private const string LOCALANALYTICS_START_APP_TIME = "LOCALANALYTICS_START_APP_TIME";
    private const string LOCALANALYTICS_CLOSE_APP_TIME = "LOCALANALYTICS_CLOSE_APP_TIME";
    private const string ENGLISH_MODE = "EnglishMode";
    private const string TOTAL_RUN_APP_TIMES = "TotalRunAppTimes";
    private const string WAS_RATEME_CLICKED = "WAS_RATEME_CLICKED";
    private const string WAS_LIKEME_CLICKED = "WAS_LIKEME_CLICKED";
    private const string NO_OFFERS_SHOWED_AMOUNT = "NO_OFFERS_SHOWED_AMOUNT";
    private const string REQUIRED_NO_OFFERS_AMOUNT = "REQUIRED_NO_OFFERS_AMOUNT";
    private const string COUNTRY = "Country";
    private const string CAR_TO_SELECT_ON_STORE_ENTER = "CarToSelectOnStoreEnter";
    private const string GAME_INFO = "GAME_INFO";
    private const string CLOUD_LOAD_TIME = "CLOUD_LOAD_TIME";
    private const string WAS_TUTORIAL_TRUCK_SPAWNED = "WAS_TUTORIAL_TRUCK_SPAWNED";
    private const string WAS_TRAFFIC_TUTORIAL = "WAS_TRAFFIC_TUTORIAL";
    private const string FREE_GIFTS_COLLECTED = "FREE_GIFTS_COLLECTED";
    private const string TIME_IN_APP = "TIME_IN_APP";
    private const string GAMEPLAY_TIME = "GAMEPLAY_TIME";
    private const string CHECKPOINT_ENABLED_DATETIME = "CHECKPOINT_ENABLED_DATETIME";
    private const string PLAYFAB_JSON_STRING = "PLAYFAB_JSON_STRING";
    private const string GAMES_INFO_JSON_STRING = "GAMES_INFO_JSON_STRING";
    private const string Play_Fab_Content_Version = "PLAY_FAB_CONTENT_VERSION";
    private const string Play_Fab_Version = "PLAY_FAB_VERSION";
    private const string STORE_TUTORIAL_SHOWN = "STORE_TUTORIAL_SHOWN";
    private const string INSTALL_TIME = "INSTALL_TIME";
    private const string SESSION_TIME = "SESSION_TIME";
    private const string BEST_CITY_SAW_INDEX = "BEST_CITY_SAW_INDEX";
    private const string MAX_RETENTION_DAY = "MAX_RETENTION_DAY";
    private const string ONE_GAMEPLAY_TIME = "ONE_GAMEPLAY_TIME";
    private const string OFFERS_DISPLAYED_COUNT = "OFFERS_DISPLAYED_COUNT";
    private const string NEW_WORLD_SEEN_LAST_SESSION = "NEW_WORLD_SEEN_LAST_SESSION";
    private const string WAS_EULA_ACCEPTED = "WAS_EULA_ACCEPTED";
    private static string PLAYER_ACCOUNT = "PlayerAccount";
    private static string FORCE_VIP_OFFER = "FORCE_VIP_OFFER";
    private static string WAS_RANKING_CARD_DISPLAYED = "WAS_RANKING_CARD_DISPLAYED";

    //--SECURED VALUE EXAMPLE
    //private static string EXAMPLE_VALUE = "ExampleValue";
    //static AntiHackInt exampleValue = new AntiHackInt(EXAMPLE_VALUE);

    public static int ExampleValue
    {
        get
        {
            //     exampleValue.LoadFromPrefs();
            return 1;//exampleValue.p_value;
        }
        set
        {
            //    exampleValue.p_value = value;
            //    exampleValue.SaveToPrefs();
        }
    }
    //--------------------------------------



    public static DateTime DailyGiftCollectedTime
    {
        get
        {
            string text = PlayerPrefs.GetString(DAILY_GIFT_COLLECTED_TIME, null);
            if (text != null && text.Length > 0)
            {
                return DateTime.Parse(text);
            }
            else
            {
                return new DateTime(1970, 1, 1, 0, 0, 0, 0);
            }

        }
        set
        {
            PlayerPrefs.SetString(DAILY_GIFT_COLLECTED_TIME, value.ToString());
        }
    }

    public static bool SFX
    {
        get
        {
            return Convert.ToBoolean(PlayerPrefs.GetInt(SFX_VALUE, 1));
        }
        set
        {
            PlayerPrefs.SetInt(SFX_VALUE, Convert.ToInt32(value));
        }
    }

    public static bool MUSIC
    {
        get
        {
            return Convert.ToBoolean(PlayerPrefs.GetInt(MUSIC_VALUE, 1));
        }
        set
        {
            PlayerPrefs.SetInt(MUSIC_VALUE, Convert.ToInt32(value));
        }
    }

    public static bool NewWorldSeenLastSession
    {
        get
        {
            return Convert.ToBoolean(PlayerPrefs.GetInt(NEW_WORLD_SEEN_LAST_SESSION, 0));
        }
        set
        {
            PlayerPrefs.SetInt(NEW_WORLD_SEEN_LAST_SESSION, Convert.ToInt32(value));
        }
    }

    public static CarDefinitions.CarEnum SelectedCar
    {
        get
        {
            string name = null;
            if (PlayerPrefs.HasKey(SELECTED_CAR))
            {
                name = PlayerPrefs.GetString(SELECTED_CAR, "");
                PlayerPrefs.DeleteKey(SELECTED_CAR);
                CarDefinitions.CarEnum carEnum = CarDefinitions.CarEnum.Car05;
                if (name != "")
                {
                    carEnum = (CarDefinitions.CarEnum)Enum.Parse(typeof(CarDefinitions.CarEnum), name);
                }

                SelectedCar = carEnum;
                return carEnum;
            }
            else
            {
                int enumInt = PlayerPrefs.GetInt(SELECTED_CAR_NEW, (int)CarDefinitions.CarEnum.Car05);
                var lastEnum = Enum.GetValues(typeof(CarDefinitions.CarEnum)).Cast<CarDefinitions.CarEnum>().Last();

                if (enumInt >= 0 && enumInt <= (int)lastEnum)
                {
                    return (CarDefinitions.CarEnum)enumInt;
                }
                else
                {
                    return CarDefinitions.CarEnum.Car05;
                }
            }
        }
        set
        {
            PlayerPrefs.SetInt(SELECTED_CAR_NEW, (int)(value));
        }
    }

    public static bool EnglishMode
    {
        get
        {
            return Convert.ToBoolean(PlayerPrefs.GetInt(ENGLISH_MODE, 0));
        }
        set
        {
            PlayerPrefs.SetInt(ENGLISH_MODE, Convert.ToInt32(value));
        }
    }


    public static bool WasLikeMeClicked
    {
        get
        {
            return Convert.ToBoolean(PlayerPrefs.GetInt(WAS_LIKEME_CLICKED, 0));
        }
        set
        {
            PlayerPrefs.SetInt(WAS_LIKEME_CLICKED, Convert.ToInt32(value));
        }
    }

    public static bool WasRateMeClicked
    {
        get
        {
            return Convert.ToBoolean(PlayerPrefs.GetInt(WAS_RATEME_CLICKED, 0));
        }
        set
        {
            PlayerPrefs.SetInt(WAS_RATEME_CLICKED, Convert.ToInt32(value));
        }
    }

    public static int NoOffersShowedAmount
    {
        get
        {
            return PlayerPrefs.GetInt(NO_OFFERS_SHOWED_AMOUNT, 0);
        }
        set
        {
            PlayerPrefs.SetInt(NO_OFFERS_SHOWED_AMOUNT, value);
        }
    }

    public static int RequiredNoOffersAmount
    {
        get
        {
            return PlayerPrefs.GetInt(REQUIRED_NO_OFFERS_AMOUNT, 3);
        }
        set
        {
            PlayerPrefs.SetInt(REQUIRED_NO_OFFERS_AMOUNT, value);
        }
    }

    public static int TotalRunAppTimes
    {
        get
        {
            return PlayerPrefs.GetInt(TOTAL_RUN_APP_TIMES, 0);
        }
        set
        {
            PlayerPrefs.SetInt(TOTAL_RUN_APP_TIMES, value);
        }
    }

    public static string Country
    {
        get
        {
            return PlayerPrefs.GetString(COUNTRY, UrlCountryChecker.DEFAULT_COUNTRY);
        }
        set
        {
            PlayerPrefs.SetString(COUNTRY, value);
        }
    }

    public static CarDefinitions.CarEnum CarToSelectOnStoreEnter
    {
        get
        {
            string name = PlayerPrefs.GetString(CAR_TO_SELECT_ON_STORE_ENTER, "");

            CarDefinitions.CarEnum carEnum = CarDefinitions.CarEnum.Car05;

            if (name != "")
            {
                carEnum = (CarDefinitions.CarEnum)Enum.Parse(typeof(CarDefinitions.CarEnum), name);
            }

            return carEnum;
        }
        set
        {
            PlayerPrefs.SetString(CAR_TO_SELECT_ON_STORE_ENTER, value.ToString());
        }
    }

    public static string GameInfoEncoded
    {
        get
        {
            return PlayerPrefs.GetString(GAME_INFO);
        }
        set
        {
            PlayerPrefs.SetString(GAME_INFO, value);
        }
    }

    public static DateTime NextCloudLoadTime
    {
        get
        {
            string text = PlayerPrefs.GetString(CLOUD_LOAD_TIME, null);
            if (text != null && text.Length > 0)
            {
                return DateTime.Parse(text);
            }
            else
            {
                return new DateTime(1970, 1, 1, 0, 0, 0, 0);
            }

        }
        set
        {
            PlayerPrefs.SetString(CLOUD_LOAD_TIME, value.ToString());
        }
        /*	
            get
            {
                return PlayerPrefs.GetInt(CLOUD_LOAD_TIME, 0);
            }
            set
            {
                PlayerPrefs.SetInt(CLOUD_LOAD_TIME, value);
            }
        */
    }

    public static bool WasTutorialTruckSpawned
    {
        get
        {
            return Convert.ToBoolean(PlayerPrefs.GetInt(WAS_TUTORIAL_TRUCK_SPAWNED, 0));
        }
        set
        {
            PlayerPrefs.SetInt(WAS_TUTORIAL_TRUCK_SPAWNED, Convert.ToInt32(value));
        }
    }

    public static int FreeGiftsCollected
    {
        get
        {
            return PlayerPrefs.GetInt(FREE_GIFTS_COLLECTED, 0);
        }
        set
        {
            PlayerPrefs.SetInt(FREE_GIFTS_COLLECTED, value);
        }
    }

    public static int SessionTime
    {
        get
        {
            return PlayerPrefs.GetInt(SESSION_TIME, 0);
        }
        set
        {
            PlayerPrefs.SetInt(SESSION_TIME, value);
        }
    }

    public static TimeSpan TotalTimeInApp
    {
        get
        {
            string text = PlayerPrefs.GetString(TIME_IN_APP, null);
            if (text != null && text.Length > 0)
            {
                return TimeSpan.Parse(text);
            }
            else
            {
                return TimeSpan.FromHours(0);
            }

        }
        set
        {
            PlayerPrefs.SetString(TIME_IN_APP, value.ToString());
        }
    }

    public static TimeSpan TotalGameplayTime
    {
        get
        {
            string text = PlayerPrefs.GetString(GAMEPLAY_TIME, null);
            if (text != null && text.Length > 0)
            {
                return TimeSpan.Parse(text);
            }
            else
            {
                return TimeSpan.FromHours(0);
            }

        }
        set
        {
            PlayerPrefs.SetString(GAMEPLAY_TIME, value.ToString());
        }
    }

    public static TimeSpan OneGameplayTime
    {
        get
        {
            string text = PlayerPrefs.GetString(ONE_GAMEPLAY_TIME, null);
            if (text != null && text.Length > 0)
            {
                return TimeSpan.Parse(text);
            }
            else
            {
                return TimeSpan.FromHours(0);
            }

        }
        set
        {
            PlayerPrefs.SetString(ONE_GAMEPLAY_TIME, value.ToString());
        }
    }

    public static DateTime CheckpointEnabledDateTime
    {
        get
        {
            string text = PlayerPrefs.GetString(CHECKPOINT_ENABLED_DATETIME, null);
            if (text != null && text.Length > 0)
            {
                return DateTime.Parse(text);
            }
            else
            {
                return new DateTime(1970, 1, 1, 0, 0, 0, 0);
            }

        }
        set
        {
            PlayerPrefs.SetString(CHECKPOINT_ENABLED_DATETIME, value.ToString());
        }
    }

    public static string PlayFabJosnString
    {
        get
        {
            return PlayerPrefs.GetString(PLAYFAB_JSON_STRING, "");
        }
        set
        {
            PlayerPrefs.SetString(PLAYFAB_JSON_STRING, value);
        }
    }

    public static string GamesInfoJSON
    {
        get
        {
            return PlayerPrefs.GetString(GAMES_INFO_JSON_STRING, "");
        }
        set
        {
            PlayerPrefs.SetString(GAMES_INFO_JSON_STRING, value);
        }
    }

    public static string PlayFabContentVersion
    {
        get
        {
            return PlayerPrefs.GetString(Play_Fab_Content_Version, "");
        }
        set
        {
            PlayerPrefs.SetString(Play_Fab_Content_Version, value);
        }
    }

    public static string PlayFabVersion
    {
        get
        {
            return PlayerPrefs.GetString(Play_Fab_Version, "");
        }
        set
        {
            PlayerPrefs.SetString(Play_Fab_Version, value);
        }
    }

    public static System.DateTime InstallTime
    {
        get
        {
            string dateTime = PlayerPrefs.GetString(INSTALL_TIME, System.DateTime.Now.ToString());
            return DateTime.Parse(dateTime);
        }
        set
        {
            if (PlayerPrefs.HasKey(INSTALL_TIME) == false)
            {
                PlayerPrefs.SetString(INSTALL_TIME, value.ToString());
            }
        }
    }

    public static string NotificationJSONString
    {
        get
        {
            return PlayerPrefs.GetString("LocalNotificationConfigJSON", "");
        }
        set
        {
            PlayerPrefs.SetString("LocalNotificationConfigJSON", value);
        }
    }

    public static bool WasStoreTutorialShown
    {
        get
        {
            return Convert.ToBoolean(PlayerPrefs.GetInt(STORE_TUTORIAL_SHOWN, 0));
        }
        set
        {
            PlayerPrefs.SetInt(STORE_TUTORIAL_SHOWN, Convert.ToInt32(value));
        }
    }

    public static int BestCitySawIndex
    {
        get
        {
            return PlayerPrefs.GetInt(BEST_CITY_SAW_INDEX, 0);
        }
        set
        {
            PlayerPrefs.SetInt(BEST_CITY_SAW_INDEX, value);
        }
    }

    public static int MaxRetentionDay
    {
        get
        {
            return PlayerPrefs.GetInt(MAX_RETENTION_DAY, 0);
        }
        set
        {
            PlayerPrefs.SetInt(MAX_RETENTION_DAY, value);
        }
    }

    public static bool WasEULAAccepted
    {
        get
        {
            return Convert.ToBoolean(PlayerPrefs.GetInt(WAS_EULA_ACCEPTED, 0));
        }
        set
        {
            PlayerPrefs.SetInt(WAS_EULA_ACCEPTED, Convert.ToInt32(value));
        }
    }

    public static string PlayerAccount
    {
        get
        {
            return PlayerPrefs.GetString(PLAYER_ACCOUNT, "");
        }
        set
        {
            PlayerPrefs.SetString(PLAYER_ACCOUNT, value);
        }
    }

    public static bool ForceVIPOffer
    {
        get
        {
            return Convert.ToBoolean(PlayerPrefs.GetInt(FORCE_VIP_OFFER, 0));
        }
        set
        {
            PlayerPrefs.SetInt(FORCE_VIP_OFFER, Convert.ToInt32(value));
        }
    }

    public static bool WasRankingCardDisplayed
    {
        get
        {
            return Convert.ToBoolean(PlayerPrefs.GetInt(WAS_RANKING_CARD_DISPLAYED, 0));
        }
        set
        {
            PlayerPrefs.SetInt(WAS_RANKING_CARD_DISPLAYED, Convert.ToInt32(value));
        }
    }

}
