﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Linq;
using System.Globalization;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class CommonMethods : MonoBehaviour
{

    public static string GetUserID()
    {
#if UNITY_EDITOR
        return SystemInfo.deviceUniqueIdentifier;
#elif UNITY_ANDROID
	        return DeviceIdentifier.generateDeviceUniqueIdentifier(false);
#elif UNITY_IOS
            return SystemInfo.deviceUniqueIdentifier;
#endif
    }

    public static void SetExplosionParticles(ParticleSystem[] particles, bool value, bool clear)
    {
        foreach (var particle in particles)
        {
            ParticleSystem.EmissionModule emission = particle.emission;
            emission.enabled = value;

            if (value == true)
            {
                particle.Play();
            }
            else
            {
                particle.Stop();
            }

            if (clear)
            {
                particle.Clear();
            }
        }
    }

    public static float GetXFromAnimationCurve(float startX, float endX, float stepX, float value, AnimationCurve curve)
    {
        float minDiff = Mathf.Infinity;
        float winner = 0;

        for (float i = startX; i <= endX; i += stepX)
        {
            float diff = Mathf.Abs(curve.Evaluate(i) - value);

            if (diff < minDiff)
            {
                minDiff = diff;
                winner = i;
            }
        }

        return winner;
    }

    public static byte[] GetBytes(string str)
    {
        byte[] bytes = new byte[str.Length * sizeof(char)];
        System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
        return bytes;
    }

    public static string GetString(byte[] bytes)
    {
        char[] chars = new char[Mathf.CeilToInt(bytes.Length / (float)sizeof(char))];
        System.Buffer.BlockCopy(bytes, 0, chars, 0, bytes.Length);
        return new string(chars);
    }

    public static byte[] Serialize<T>(T toSerialize)
    {
        BinaryFormatter formatter = new BinaryFormatter();

        using (MemoryStream memoryStream = new MemoryStream())
        {
            formatter.Serialize(memoryStream, toSerialize);
            return memoryStream.ToArray();
        }
    }

    public static string RandomString(int length, bool numberIncluded = true)
    {
        string chars = "ABCDEFGHIJKLMNPQRSTUVWXYZ";
        string charsNumbers = "123456789";

        if (numberIncluded)
        {
            chars += charsNumbers;
        }

        var random = new System.Random();
        return new string(Enumerable.Repeat(chars, length)
          .Select(s => s[random.Next(s.Length)]).ToArray());
    }

    public static void ActivateObjectParent(MonoBehaviour _object, bool state)
    {
        _object.transform.parent.gameObject.SetActive(state);
    }

    public static bool IsParentActive(MonoBehaviour _object)
    {
        return _object.transform.parent.gameObject.activeInHierarchy;
    }

    public static void AddArrayToList<T>(List<T> list, T[] array)
    {
        for (int i = 0; i < array.Length; i++)
        {
            list.Add(array[i]);
        }
    }

    public static Color GetColorWithAlpha(Color color, float alpha)
    {
        Color result = color;
        result.a = alpha;
        return result;
    }

    public static List<GameObject> FindChildrenWithTag(GameObject parent, string tag)
    {
        List<GameObject> childWithTagList = new List<GameObject>();


        foreach (Transform child in parent.GetComponentsInChildren<Transform>())
        {
            if (child.tag == tag)
            {
                childWithTagList.Add(child.gameObject);
            }
        }

        return childWithTagList;
    }

    public static List<GameObject> RemoveListWithDestroyObjects(List<GameObject> list)
    {
        if (list != null)
        {
            foreach (var item in list)
            {
                Destroy(item);
            }
            list.Clear();
        }
        return null;
    }

    public static List<T> MoveElement<T>(List<T> list, int fromIndex, int toIndex)
    {
        if (list != null)
        {
            T element = list[fromIndex];
            list.RemoveAt(fromIndex);
            if (list.Count <= toIndex)
            {
                list.Add(element);
            }
            else
            {
                list.Insert(toIndex, element);
            }
            return list;
        }
        return null;
    }

    public static void RemoveAndDestroy(List<GameObject> list, int index)
    {
        if (list != null && list.Count > index)
        {
            Destroy(list[index]);
            list.RemoveAt(index);
        }
    }

    public static void SetAlphaToSpriteRenderer(SpriteRenderer spriteRenderer, float alpha)
    {
        Color newColor = spriteRenderer.color;
        newColor.a = alpha;
        spriteRenderer.color = newColor;
    }

    public static void SetAlphaToMaterial(MeshRenderer meshRenderer, float alpha, string propName)
    {
        Color newColor = meshRenderer.material.GetColor(propName);
        newColor.a = alpha;
        meshRenderer.material.SetColor(propName, newColor);
    }

    public static void SetAlphaToSharedMaterial(MeshRenderer meshRenderer, float alpha, string propName)
    {
        Color newColor = meshRenderer.sharedMaterial.GetColor(propName);
        newColor.a = alpha;
        meshRenderer.sharedMaterial.SetColor(propName, newColor);
    }

    public static void SetAlphaToImage(Image image, float alpha)
    {
        Color newColor = image.color;
        newColor.a = alpha;
        image.color = newColor;
    }

    public static void SetActiveArray(GameObject[] array, bool value)
    {
        foreach (var item in array)
        {
            item.SetActive(value);
        }
    }

    public static Vector2 Direction2DFromAngle(float angle)
    {
        Vector2 result;
        result.x = Mathf.Cos(angle);
        result.y = Mathf.Sin(angle);

        return result;
    }

    public static Vector3 Vector3FromFloat(float value)
    {
        return new Vector3(value, value, value);
    }

    public static Vector2 Vector2FromFloat(float value)
    {
        return new Vector2(value, value);
    }

    public static Vector3 Vector3Multiply(Vector3 first, Vector3 second)
    {
        return new Vector3(first.x * second.x, first.y * second.y, first.z * second.z);
    }

    public static void SetXToTransformPosition(Transform transform, float X)
    {
        transform.position += new Vector3(-transform.position.x + X, 0, 0);
    }

    public static void SetYToTransformPosition(Transform transform, float Y)
    {
        transform.position += new Vector3(0, -transform.position.y + Y, 0);
    }

    public static void SetZToTransformPosition(Transform transform, float Z)
    {
        transform.position += new Vector3(0, 0, -transform.position.z + Z);
    }

    public static float GetSignNotZero(float value)
    {
        if (Mathf.Abs(value) > 0.001f)
        {
            return Mathf.Sign(value);
        }
        else
        {
            return 0.0f;
        }
    }

    public static float Round(float value, float precision)
    {
        float result = value / precision;
        result = Mathf.Round(result);
        result *= precision;
        return result;
    }

    public static string GetCrimsonAnalyticsOrder(int i)
    {
        if (i < 10)
        {
            return "0" + i.ToString();
        }
        else
        {
            return i.ToString();
        }
    }

    public static bool IsPortrait()
    {
        return (float)Screen.height > (float)Screen.width;
    }

    public static float GetRatioFactor(bool bannerVisible)
    {
        float factor = 0.0f;

        float height9_16 = 1;
        float height3_4 = 1;

        if (IsPortrait())
        {
            height9_16 = 16 / 9f;
            height3_4 = 4 / 3f;
        }
        else
        {
            height9_16 = 9 / 16f;
            height3_4 = 3 / 4f;
        }

        float screenHeight = Screen.height;
        if (bannerVisible)
        {
            screenHeight *= GlobalParameters.BANNER_CAMERA_HEIGHT;
        }

        float actualHeight = (screenHeight) / (float)Screen.width;

        float higher = height9_16 - height3_4;
        float actual = height9_16 - actualHeight;

        factor = actual / higher;


        return factor;
    }

    public static int GetRandomSign()
    {
        float value = Random.value;
        return value > 0.5f ? 1 : -1;
    }

    public static Vector3 Vector3Abs(Vector3 vector)
    {
        vector.x = Mathf.Abs(vector.x);
        vector.y = Mathf.Abs(vector.y);
        vector.z = Mathf.Abs(vector.z);
        return vector;
    }

    public static IEnumerator WaitForUnscaledSeconds(float waitTime)
    {
        float t = 0;
        while (t <= 1f)
        {
            t += Time.unscaledDeltaTime / waitTime;
            yield return 0;
        }
    }

    public static void SetTimeScaleSmoothed(float value)
    {
        Time.timeScale = value;
        Time.fixedDeltaTime = 0.02f * value;
    }

    public static int RoundWithPlaces(int value, int importantDigits)
    {
        int totalDigits = value.ToString().Length;
        float divideNumber = Mathf.Pow(10, totalDigits - importantDigits);
        float result = Mathf.RoundToInt(value / divideNumber) * divideNumber;
        return (int)result;
    }

    public static int RoundCash(int value)
    {
        int totalDigits = value.ToString().Length;

        switch (totalDigits)
        {
            case 1:
                return value;
            case 2:
                return RoundWithPlaces(value, 1);
            default:
                return RoundWithPlaces(value, 2);
        }
    }

    public static string GetCashStringWithSpacing(int value)
    {
        var nfi = (NumberFormatInfo)CultureInfo.InvariantCulture.NumberFormat.Clone();
        nfi.NumberGroupSeparator = " ";
        string formatted = value.ToString("#,0", nfi);

        return formatted;
    }

    public static bool IsFirstVersionHigherThanSecond(string first, string second)
    {
        string v1 = first;
        string v2 = second;

        var version1 = new System.Version(v1);
        var version2 = new System.Version(v2);

        var result = version1.CompareTo(version2);

        if (result > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public static string GetRandomNickname()
    {
        string[] adjectives = { "Fast", "Red", "Ultra", "Angry", "Quick", "Smooth", "Turbo", "Furious", "Lucky", "Diesel", "Brute", "Magic", "Little", "Fat", "Big", "Hard" };
        string[] nouns = { "Racer", "Drifter", "Guy", "Thief", "Buddy", "Bandit", "Bullet", "Smoke", "Devil", "Clutch", "Apex", "Snake", "Viper", "Bolt", "Duster", "Engine" };

        return adjectives.RandomElement() + nouns.RandomElement() + Random.Range(10, 999);
    }


}


public static class CommonExtensions
{
    public static string Ordinal(this int number)
    {
        const string TH = "th";
        string s = number.ToString();

        if (number < 1)
        {
            return s;
        }

        number %= 100;
        if ((number >= 11) && (number <= 13))
        {
            return s + TH;
        }

        switch (number % 10)
        {
            case 1: return s + "st";
            case 2: return s + "nd";
            case 3: return s + "rd";
            default: return s + TH;
        }
    }

    public static void CopyFrom(this BoxCollider obj, BoxCollider other)
    {
        obj.isTrigger = other.isTrigger;
        obj.sharedMaterial = other.sharedMaterial;
        obj.size = other.size;
        obj.center = other.center;
    }

    public static void SetLayerRecursively(this GameObject obj, int layer)
    {
        obj.layer = layer;

        foreach (Transform child in obj.transform)
        {
            child.gameObject.SetLayerRecursively(layer);
        }
    }

    public static void SetActive(this GameObject[] array, bool value)
    {
        foreach (var item in array)
        {
            if (item != null)
            {
                item.SetActive(value);
            }
        }
    }

    public static void SetActive(this List<GameObject> array, bool value)
    {
        foreach (var item in array)
        {
            item.SetActive(value);
        }
    }

    public static void CopyTo<T>(this List<T> array, List<T> array2)
    {
        if (array2 == null)
        {
            array2 = new List<T>();
        }

        foreach (var item in array)
        {
            array2.Add(item);
        }
    }

    public static void CopyTo<T>(this T[] array, List<T> array2)
    {
        if (array2 == null)
        {
            array2 = new List<T>();
        }

        foreach (var item in array)
        {
            array2.Add(item);
        }
    }

    public static T RandomElement<T>(this T[] list)
    {
        int selectedIndex = Random.Range(0, list.Length);
        return list[selectedIndex];
    }

    public static T RandomElement<T>(this List<T> list)
    {
        int selectedIndex = Random.Range(0, list.Count);
        return list[selectedIndex];
    }

    public static T RandomElementExcluding<T>(this List<T> list, T elementToExclude)
    {
        T candidate;

        do
        {
            candidate = list.RandomElement();
        }
        while (list.Count > 1 && candidate.Equals(elementToExclude));

        return candidate;
    }



    public static Vector3 WithIntValues(this Vector3 vector)
    {
        return new Vector3(Mathf.Round(vector.x), Mathf.Round(vector.y), Mathf.Round(vector.z));
    }

    public static Vector3 ChangeX(this Vector3 vector, float x)
    {
        return new Vector3(x, vector.y, vector.z);
    }

    public static Vector3 ChangeY(this Vector3 vector, float y)
    {
        return new Vector3(vector.x, y, vector.z);
    }

    public static Vector3 ChangeZ(this Vector3 vector, float z)
    {
        return new Vector3(vector.x, vector.y, z);
    }

    public static Vector3 MultiplyX(this Vector3 vector, float x)
    {
        return new Vector3(vector.x * x, vector.y, vector.z);
    }

    public static Vector3 MultiplyY(this Vector3 vector, float y)
    {
        return new Vector3(vector.x, vector.y * y, vector.z);
    }

    public static Vector3 MultiplyZ(this Vector3 vector, float z)
    {
        return new Vector3(vector.x, vector.y, vector.z * z);
    }

    public static Vector3 AddX(this Vector3 vector, float x)
    {
        return new Vector3(vector.x + x, vector.y, vector.z);
    }

    public static Vector3 AddY(this Vector3 vector, float y)
    {
        return new Vector3(vector.x, vector.y + y, vector.z);
    }

    public static Vector3 AddZ(this Vector3 vector, float z)
    {
        return new Vector3(vector.x, vector.y, vector.z + z);
    }

    public static bool Approximately(this Vector3 vector, Vector3 second)
    {
        return Mathf.Approximately(vector.x, second.x) && Mathf.Approximately(vector.y, second.y) && Mathf.Approximately(vector.z, second.z);
    }

    public static Color ChangeA(this Color color, float a)
    {
        Color newColor = color;
        newColor.a = a;
        return newColor;
    }



}