﻿using UnityEngine;
using System.Collections;

public class GlobalParameters : MonoBehaviour {

    public static float carDragAfterCrash { get { return 5; } }
    public static bool peopleSpawningEnabled { get { return true; } }
    public static float BANNER_CAMERA_HEIGHT = 0.92f;

}
