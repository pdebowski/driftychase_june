using UnityEngine;
using System.Collections;

public class VIP
{
    private static string VIP_NAME = "VIP";
    private static int NO_ADS__CONTROL_CODE = 7743;

    public static bool IsVIPAccount
    {
        get
        {
            AntiHackInt adsState = new AntiHackInt(VIP_NAME);
            adsState.LoadFromPrefs();
            return adsState.p_value == NO_ADS__CONTROL_CODE;
        }
    }

    public static void SetAccountAsVIP()
    {
        AntiHackInt adsState = new AntiHackInt(VIP_NAME);
        adsState.p_value = NO_ADS__CONTROL_CODE;
        adsState.SaveToPrefs();
    }
}
