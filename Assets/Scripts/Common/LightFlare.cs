using UnityEngine;
using System.Collections;

public class LightFlare : MonoBehaviour
{
    [SerializeField]
    private bool isPolice = false;
    [SerializeField]
    private float offset;
    [SerializeField]
    private float alpha = 1;

    private Vector3 startLocalPosition;
    private float startScale;
    private MeshRenderer meshRenderer;

	private Material mat;
    private int adder;

    void Awake()
    {
        adder = (int)(Random.value*5) ;
        startLocalPosition = transform.localPosition;
        startScale = transform.localScale.x;
        meshRenderer = GetComponentInChildren<MeshRenderer>();
		mat = meshRenderer.material;
        mat.SetFloat("_Alpha", alpha);        
    }

    void Update()
    {
        if (meshRenderer.isVisible && (Time.frameCount+adder)%2 ==0)
        {
			transform.localPosition = startLocalPosition;
			transform.LookAt(PrefabReferences.Instance.ActiveCameraHolder.GetActiveCamera());
			transform.position += transform.forward * offset;

            if (!isPolice)
            {
				float angle = Vector3.Angle(transform.forward, transform.parent.forward);
				float newScaleFactor = Mathf.InverseLerp(110, 100, angle);
				mat.SetFloat("_Scale", startScale * newScaleFactor);
            }

        }
    }
}
