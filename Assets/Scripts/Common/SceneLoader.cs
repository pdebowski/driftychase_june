﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class SceneLoader : Singleton<SceneLoader>
{

    class CorData
    {
        public ISceneCor m_corToRun;
        public string m_sceneName;
        public bool m_useLoadingScene;
        public bool m_additiveLoad;

        public CorData(ISceneCor corToRun, string sceneName, bool useLoadingScene, bool additiveLoad)
        {
            m_corToRun = corToRun;
            m_sceneName = sceneName;
            m_useLoadingScene = useLoadingScene;
            m_additiveLoad = additiveLoad;
        }
    }

    const float MIN_LOADING_SCENE_DURATION = 3f;
    //params: target scene
    public static System.Action<string> OnSceneWillLoad = delegate { }; // before loading
    public static System.Action<string> OnSceneLoading = delegate { }; // after worker, before load LoadingScene
    public static System.Action<string> OnSceneLoaded = delegate { }; // after loding lastest scene

    public static System.Action<string> OnAudioFadeTime = delegate { };

    bool m_changeCorRunning = false;
    string m_previousSceneName = null;

    //---------------------------------------------------------------------
    public string p_scenePreviousName
    {
        get { return m_previousSceneName; }
        private set { m_previousSceneName = value; }
    }
    //---------------------------------------------------------------------
    public string p_sceneName
    {
        get { return SceneManager.GetActiveScene().name; }
    }
    //---------------------------------------------------------------------
    public bool p_isSceneLoadingNow
    {
        get { return m_changeCorRunning; }
    }
    //---------------------------------------------------------------------
    protected override void Awakened()
    {
        base.Awakened();
        OnSceneLoaded(p_sceneName);
    }
    //---------------------------------------------------------------------
    void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoadedAction;
    }
    //---------------------------------------------------------------------
    void OnDisalbe()
    {
        SceneManager.sceneLoaded -= OnSceneLoadedAction;
    }
    //---------------------------------------------------------------------
    private void OnSceneLoadedAction(Scene scene, LoadSceneMode loadSceneMode)
    {
        if (Debug.isDebugBuild)
            OnSceneLoaded(scene.name);
    }
    //---------------------------------------------------------------------
    public void Quit()
    {
        Application.Quit();
    }
    //-------------------------------------------------------------------------
    public void LoadSceneAdditive(string sceneName)
    {
        sceneName = TVFilter.GetRealName(sceneName);

        if (m_changeCorRunning) return;

        //SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);
        StartCoroutine("ChangeLevelCor", new CorData(null, sceneName, false, true));
    }
    //------------------------------------------------------------------------
    public void LoadScene(string sceneName, bool useLoadingScene, float withUnscaledDelay)
    {
        sceneName = TVFilter.GetRealName(sceneName);

        if (m_changeCorRunning) return;


        if (Mathf.Approximately(0, withUnscaledDelay))
        {
            LoadScene(sceneName, useLoadingScene, null);
        }
        else
        {
            m_changeCorRunning = true;
            StartCoroutine(LoadSceneWithDelay(sceneName, withUnscaledDelay, useLoadingScene));

        }
    }
    //---------------------------------------------------------------------
    public void LoadScene(string sceneName, bool useLoadingScene = false, ISceneCor corToRun = null)
    {
        if (m_changeCorRunning)
        {
            return;
        }

        OnSceneWillLoad(sceneName);

        m_changeCorRunning = true;
        StartCoroutine("ChangeLevelCor", new CorData(corToRun, sceneName, useLoadingScene, false));
    }
    //---------------------------------------------------------------------
    IEnumerator LoadSceneWithDelay(string sceneName, float withUnscaledDelay = 0, bool useLoadingScene = false)
    {
        OnSceneWillLoad(sceneName);

        float t = 0;
        while (t <= 1f)
        {
            t += Time.unscaledDeltaTime / withUnscaledDelay;
            yield return 0;
        }
        yield return StartCoroutine("ChangeLevelCor", new CorData(null, sceneName, useLoadingScene, false));
    }
    //---------------------------------------------------------------------
    IEnumerator ChangeLevelCor(CorData corData)
    {
        AsyncOperation asyncOperation;
        OnSceneLoading(corData.m_sceneName);
        if (Debug.isDebugBuild)
        {

            if (corData.m_corToRun != null)
                yield return StartCoroutine(corData.m_corToRun.Worker());

            OnAudioFadeTime(corData.m_sceneName);
            p_scenePreviousName = p_sceneName;
            m_changeCorRunning = false;
            SceneManager.LoadSceneAsync(corData.m_sceneName, corData.m_additiveLoad == true ? LoadSceneMode.Additive : LoadSceneMode.Single);
            OnSceneLoaded(corData.m_sceneName);
            yield break;
        }
        else
        {
            if (corData.m_useLoadingScene)
            {
                if (corData.m_corToRun != null)
                    yield return StartCoroutine(corData.m_corToRun.Worker());

                yield return SceneManager.LoadSceneAsync("LoadingScene", LoadSceneMode.Single);
                yield return 0;
                asyncOperation = SceneManager.LoadSceneAsync(corData.m_sceneName, LoadSceneMode.Single);
                asyncOperation.allowSceneActivation = false;

                yield return StartCoroutine(LoadLevelWithProgressBar(corData.m_sceneName, asyncOperation));
            }
            else
            {
                OnAudioFadeTime(corData.m_sceneName);
                /* asyncOperation =*/
                SceneManager.LoadScene(corData.m_sceneName, corData.m_additiveLoad == true ? LoadSceneMode.Additive : LoadSceneMode.Single);
                // asyncOperation.allowSceneActivation = false;

                if (corData.m_corToRun != null)
                    yield return StartCoroutine(corData.m_corToRun.Worker());
            }

            m_changeCorRunning = false;
            p_scenePreviousName = p_sceneName;
            //asyncOperation.allowSceneActivation = true;
            //yield return asyncOperation;
            OnSceneLoaded(corData.m_sceneName);
        }



    }
    //---------------------------------------------------------------------
    IEnumerator LoadLevelWithProgressBar(string levelName, AsyncOperation asyncOperation)
    {
        Material mat = GameObject.FindWithTag("ProgressBar").GetComponent<Renderer>().material;


        float t = 0f;
        bool fadingMusicCalled = false;
        while (t <= 1f)
        {
            t += Time.deltaTime / MIN_LOADING_SCENE_DURATION;
            float progress = Mathf.Min(new float[] { t, asyncOperation.progress, 1f });

            if (t >= 0.7f && !fadingMusicCalled)
            {
                fadingMusicCalled = true;
                OnAudioFadeTime(levelName);
            }

            mat.SetFloat("_MaxX", progress);
            yield return 0;
        }
    }
    //---------------------------------------------------------------------	
    public void UnloadScene(string sceneName)
    {
        SceneManager.UnloadScene(TVFilter.GetRealName(sceneName));
    }
}

public interface ISceneCor
{
    IEnumerator Worker();
}
