﻿using UnityEngine;
using System.Collections;

public class DLManager : MonoBehaviour {

    [Tooltip("If it's true, logs will be showed even in non-development build")]
    [SerializeField]
    private bool ForceDebugLogs = false;

    void Awake()
    {
        if(ForceDebugLogs)
        {
            Debug.LogError("Debug Logs are forced in non-development build! Be Aware");
        }
        
        DL.ForceLogs = ForceDebugLogs;
    }
}
