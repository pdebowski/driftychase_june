using UnityEngine;
using System.Collections;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using FriendClassesOnlyForPrefsDataFacade;

//Singleton
public class PrefsDataFacade : SaveLoadManager<PrefsDataFacade>
{
    GameInfo gameInfo;


    protected override void Awakened()
    {
        base.Awakened();

        InitializePrefsData();
    }

    void InitializePrefsData()
    {
        try
        {
            gameInfo = Deserialize(PlayerPrefsAdapter.GameInfoEncoded);
        }
        catch (System.Exception e)
        {
            DL.LogWarning("Cannot deserialize data " + e.Message);
            gameInfo = new GameInfo();
            PlayerPrefsAdapter.GameInfoEncoded = Serialize(gameInfo);
        }
    }

    #region Store and Money and Leaderboard
    public bool IsCarBought(CarDefinitions.CarEnum carEnum)
    {
        return gameInfo.carStoreInfoList.First(v => v.carEnum == carEnum).isBought;
    }

    public void SetRecord(int newRecord, bool force)
    {
        if (GTMParameters.CustomLeaderboardsEnabled)
        {
            CrimsonLeaderboards.LeaderboardsManager.LeaderboardRefresher.ReportScore(newRecord);
        }

        if (force)
        {
            newRecord = Mathf.Min(Mathf.Max(newRecord, gameInfo.gameVariable.Record), 700);
        }

        if (gameInfo.gameVariable.Record < newRecord || force)
        {
            newRecord = Mathf.Min(newRecord, Random.Range(700, 900));
            gameInfo.gameVariable.Record = newRecord;
            CrimsonPlugins.RankingCloudAdapter.Instance.p_rankingInstance.ReportScore(CrimsonPlugins.RCLeaderboard.BEST, newRecord);
        }
    }

    public int GetRecord()
    {
        return gameInfo.gameVariable.Record;
    }

    public string GetTVOSNextFreeGiftTime()
    {
        return gameInfo.gameVariable.NextTVOSFreeGiftTime;
    }

    public void SetTVOSNextFreeGiftTime(string dateTime)
    {
        gameInfo.gameVariable.NextTVOSFreeGiftTime = dateTime;
    }

    public void SetTotalDistance(int newTotalDistance)
    {
        if (gameInfo.gameVariable.TotalDistance < newTotalDistance)
        {
            newTotalDistance = Mathf.Min(newTotalDistance, Random.Range(1000000, 2000000));
            gameInfo.gameVariable.TotalDistance = newTotalDistance;
            CrimsonPlugins.RankingCloudAdapter.Instance.p_rankingInstance.ReportScore(CrimsonPlugins.RCLeaderboard.TOTAL, newTotalDistance);
        }
    }

    public int GetTotalDistance()
    {
        return gameInfo.gameVariable.TotalDistance;
    }


    public int GetTotalCash()
    {
        return gameInfo.gameVariable.TotalCash;
    }

    public void ResetData()
    {
        gameInfo = new GameInfo();
    }

    public void AddTotalCash(int cash)
    {
        gameInfo.gameVariable.TotalCash += cash;
        LocalSave();
    }
    public void BuyCar(CarDefinitions.CarEnum carEnum)
    {
        gameInfo.carStoreInfoList.Find(v => v.carEnum == carEnum).BuyThisCar();
        LocalSave();
    }
    #endregion

    private void LocalSave()
    {
        PlayerPrefsAdapter.GameInfoEncoded = Serialize(gameInfo);
#if UNITY_EDITOR
        Deserialize(PlayerPrefsAdapter.GameInfoEncoded);
#endif
    }


    #region Other games data

    public int GetHighestAvailableLevel(GameplayMode mode)
    {
        /*OtherGameInfo otherGameInfo = gameInfo.otherGamesInfo.Find(x => x.mode == mode);
        int maxLevel = 0;
        for (int i = 0; i < otherGameInfo.levelsInfo.Count; i++)
        {
            if (otherGameInfo.levelsInfo[i] == OtherGameInfo.UNLOCKED || otherGameInfo.levelsInfo[i] == OtherGameInfo.COMPLETED)
            {
                maxLevel = i;
            }
        }
        return maxLevel;*/
        return 0;
    }

    public bool IsCompleted(GameplayMode mode, int level)
    {
        /* OtherGameInfo otherGameInfo = gameInfo.otherGamesInfo.Find(x => x.mode == mode);

         if (otherGameInfo.levelsInfo.Count() <= level)
         {
             return false;
         }
         else
         {
             return otherGameInfo.levelsInfo[level] == OtherGameInfo.COMPLETED;
         }*/
        return false;
    }

    public bool IsAvailable(GameplayMode mode, int level)
    {
        /*OtherGameInfo otherGameInfo = gameInfo.otherGamesInfo.Find(x => x.mode == mode);

        if (otherGameInfo.levelsInfo.Count() <= level)
        {
            return false;
        }
        else
        {
            return otherGameInfo.levelsInfo[level] == OtherGameInfo.UNLOCKED || otherGameInfo.levelsInfo[level] == OtherGameInfo.COMPLETED;
        }*/
        return false;
    }

    public void Unlock(GameplayMode mode, int level)
    {
       /* OtherGameInfo otherGameInfo = gameInfo.otherGamesInfo.Find(x => x.mode == mode);
        if (otherGameInfo != null)
        {
            if (otherGameInfo.levelsInfo.Count() <= level)
            {
                for (int i = otherGameInfo.levelsInfo.Count(); i <= level; i++)
                {
                    otherGameInfo.levelsInfo.Add(OtherGameInfo.UNLOCKED);
                }
            }
            else
            {
                if (otherGameInfo.levelsInfo[level] == OtherGameInfo.LOCKED)
                {
                    otherGameInfo.levelsInfo[level] = OtherGameInfo.UNLOCKED;
                }
            }
        }
        LocalSave();*/
    }



    public void SetAsCompleted(GameplayMode mode, int level)
    {
        /*OtherGameInfo otherGameInfo = gameInfo.otherGamesInfo.Find(x => x.mode == mode);
        if (otherGameInfo != null)
        {
            if (otherGameInfo.levelsInfo.Count() <= level)
            {
                for (int i = otherGameInfo.levelsInfo.Count(); i <= level; i++)
                {
                    otherGameInfo.levelsInfo.Add(OtherGameInfo.UNLOCKED);
                }

                otherGameInfo.levelsInfo[level] = OtherGameInfo.COMPLETED;
            }
            else
            {
                otherGameInfo.levelsInfo[level] = OtherGameInfo.COMPLETED;
            }
        }
        LocalSave();*/
    }

    public float GetLevelProgress(GameplayMode mode, int level)
    {
        /*OtherGameInfo otherGameInfo = gameInfo.otherGamesInfo.Find(x => x.mode == mode);

        if (otherGameInfo.levelsProgress.Count() <= level)
        {
            return 0;
        }
        else
        {
            return otherGameInfo.levelsProgress[level];
        }*/
        return 0;
    }


    public void SetLevelProgress(GameplayMode mode, int level, float progress)
    {
        /*OtherGameInfo otherGameInfo = gameInfo.otherGamesInfo.Find(x => x.mode == mode);
        if (otherGameInfo != null)
        {
            if (otherGameInfo.levelsProgress.Count() <= level)
            {
                for (int i = otherGameInfo.levelsProgress.Count(); i <= level; i++)
                {
                    otherGameInfo.levelsProgress.Add(0);
                }

                otherGameInfo.levelsProgress[level] = progress;
            }
            else
            {
                if (otherGameInfo.levelsProgress[level] < progress)
                {
                    otherGameInfo.levelsProgress[level] = progress;
                }
            }
        }
        LocalSave();*/
    }
    #endregion




    #region Cloud method
    /*public void SaveOrLoadToCloud()
    {
        long nextLoadStamp = System.Convert.ToInt64(new System.TimeSpan(PlayerPrefsAdapter.NextCloudLoadTime.Ticks).TotalSeconds);
        long unixTimestamp = System.Convert.ToInt64(new System.TimeSpan(System.DateTime.UtcNow.Ticks).TotalSeconds);
        long secondToFire = nextLoadStamp - unixTimestamp;

        //5 is a threshold
        if (secondToFire < 5)
        {
            LoadGameFromCloud();
        }
        else
        {
            SaveGameStateCloud();
        }
    }*/

    public void LoadGameFromCloud()
    {
        LoadGameState();
    }

    public void SaveGameStateCloud()
    {
        SaveGameState(PlayerPrefsAdapter.GameInfoEncoded);
    }



    public void ForceSaveAfterPause()
    {
        StopCoroutine("WaitForGoogleReconnectAndSave");
        StartCoroutine("WaitForGoogleReconnectAndSave");
    }

    IEnumerator WaitForGoogleReconnectAndSave()
    {
        yield return new WaitForSeconds(0.5f);
        while (!CrimsonPlugins.RankingCloudAdapter.Instance.p_saveGameInstance.p_IsAuthenticated)
        {
            yield return 0;
        }
        SaveGameStateCloud();
    }

    #endregion


    #region PRIVATE
    protected override string ResolveCloudConflict(string newerSnapshot, string olderSnapshot)
    {
        GameInfo newerGameInfo = null, olderGameInfo = null;

        try
        {
            newerGameInfo = Deserialize(newerSnapshot);
        }
        catch (System.Exception e)
        {
            DL.LogWarning("Cloud: IN Conflict : Cannot deserialize data from newerSnapshot" + e.Message);
        }

        try
        {
            olderGameInfo = Deserialize(olderSnapshot);
        }
        catch (System.Exception e)
        {
            DL.LogWarning("Cloud: IN Conflict : Cannot deserialize data from olderSnapshot" + e.Message);
        }

        if (olderGameInfo != null)
        {
            return newerSnapshot;
        }
        if (newerGameInfo != null)
        {
            return olderSnapshot;
        }

        string ret = newerSnapshot;

        try
        {
            ret = Serialize(ResolveConflicts(newerGameInfo, olderGameInfo));
        }
        catch (System.Exception e)
        {
            DL.LogWarning("Cloud: IN Conflict : Cannot serialize data returned by resolver" + e.Message);
        }

        return ret;
    }

    GameInfo ResolveConflicts(GameInfo newerGameInfo, GameInfo olderGameInfo)
    {
        GameInfo ret = new GameInfo();
        int unlockedCarInNewer = 0, unlockedCarInOlder = 0;

        foreach (CarDefinitions.CarEnum carEnum in System.Enum.GetValues(typeof(CarDefinitions.CarEnum)))
        {
            bool newerIsBought = newerGameInfo.carStoreInfoList.Find(v => v.carEnum == carEnum).isBought;
            bool olderIsBought = olderGameInfo.carStoreInfoList.Find(v => v.carEnum == carEnum).isBought;

            unlockedCarInNewer += newerIsBought ? 1 : 0;
            unlockedCarInOlder += olderIsBought ? 1 : 0;

            if (newerIsBought || olderIsBought)
            {
                ret.carStoreInfoList.Find(v => v.carEnum == carEnum).BuyThisCar();
            }
        }

        if (unlockedCarInOlder > unlockedCarInNewer)
        {
            ret.gameVariable.TotalCash = olderGameInfo.gameVariable.TotalCash;
        }
        else
        {
            ret.gameVariable.TotalCash = (Mathf.Max(newerGameInfo.gameVariable.TotalCash, olderGameInfo.gameVariable.TotalCash));
        }

       /* foreach (GameplayMode game in System.Enum.GetValues(typeof(GameplayMode)))
        {

            OtherGameInfo newerGameProgress = newerGameInfo.otherGamesInfo.Find(v => v.mode == game);
            OtherGameInfo olderGameProgress = olderGameInfo.otherGamesInfo.Find(v => v.mode == game);
            OtherGameInfo returnGameProgress = ret.otherGamesInfo.Find(x => x.mode == game);

            if (returnGameProgress == null)
            {
                returnGameProgress = new OtherGameInfo(game);
                ret.otherGamesInfo.Add(returnGameProgress);
            }

            List<int> olderLevels = newerGameProgress.levelsInfo;
            List<int> newerLevels = olderGameProgress.levelsInfo;
            List<int> resultLevels = returnGameProgress.levelsInfo;
            List<float> resultLevelsProgress = returnGameProgress.levelsProgress;

            int maxIndex = Mathf.Max(newerGameProgress.levelsInfo.Count, olderGameProgress.levelsInfo.Count);

            while (resultLevels.Count < maxIndex)
            {
                resultLevels.Add(OtherGameInfo.LOCKED);
                resultLevelsProgress.Add(0);
            }

            for (int i = 0; i < maxIndex; i++)
            {
                if (i < newerLevels.Count)
                {
                    if (newerLevels[i] > resultLevels[i])
                    {
                        resultLevels[i] = newerLevels[i];
                        resultLevelsProgress[i] = newerGameProgress.levelsProgress[i];
                    }
                }
                if (i < olderLevels.Count)
                {
                    if (olderLevels[i] > resultLevels[i])
                    {
                        resultLevels[i] = olderLevels[i];
                        resultLevelsProgress[i] = olderGameProgress.levelsProgress[i];
                    }
                }
            }
        }*/

        ret.gameVariable.TotalDistance = Mathf.Max(newerGameInfo.gameVariable.TotalDistance, olderGameInfo.gameVariable.TotalDistance);
        ret.gameVariable.Record = Mathf.Max(newerGameInfo.gameVariable.Record, olderGameInfo.gameVariable.Record);
        ret.gameVariable.NextTVOSFreeGiftTime = "NO_VALUE";

        return ret;
    }


    protected override void GameStateLoadedFromCloud(string gameState)
    {
        GameInfo newGameState;
        try
        {
            newGameState = Deserialize(gameState);
            GameInfo result = ResolveConflicts(newGameState, gameInfo);
            gameInfo = result;


            PlayerPrefsAdapter.GameInfoEncoded = Serialize(result);
        }
        catch (System.Exception e)
        {
            DL.LogWarning("Cloud: Cannot deserialize data on loading data " + e.Message);
        }

        ScoreCounter.OnCashOrScoreChanged();
    }

    private CarStoreInfo CreateDefaultCar(CarDefinitions.CarEnum carEnum)
    {
        bool isBoughtOnDefault = carEnum == CarDefinitions.CarEnum.Car05 ? true : false;

        return new CarStoreInfo(carEnum, isBoughtOnDefault);
    }

    string Serialize(GameInfo toSerialize)
    {
        BinaryFormatter formatter = new BinaryFormatter();
        string ret = null;

        using (MemoryStream memoryStream = new MemoryStream())
        {
            formatter.Serialize(memoryStream, toSerialize);
            ret = System.Convert.ToBase64String(memoryStream.ToArray());
        }

        return ret;
    }

    GameInfo Deserialize(string baseEncodedStream)
    {
        GameInfo ret;
        BinaryFormatter formatter = new BinaryFormatter();
        byte[] stream = System.Convert.FromBase64String(baseEncodedStream);

        using (MemoryStream memoryStream = new MemoryStream(stream))
        {
            ret = formatter.Deserialize(memoryStream) as GameInfo;
        }
        ret.PreserveCompability();

        return ret;
    }
    #endregion

}
