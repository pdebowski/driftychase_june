﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerCamera : MonoBehaviour
{

    public Vector3 ShakeValue { get; private set; }
    public bool Working { get; private set; }
    [SerializeField]
    private Transform target;
    [SerializeField]
    private Transform bankFocusTransform;
    [SerializeField]
    private float rotationLerp = 5f;
    [SerializeField]
    private float positionLerp = 5f;
    [SerializeField]
    private Vector3 positionOffset = new Vector3(0, 50, -10);
    [SerializeField]
    private Vector3 positionOffsetOnTV = new Vector3(0, 50, -10);

    [Header("Shake")]
    [SerializeField]
    private float radius;
    [SerializeField]
    private float duration;
    [Range(0.0f, 1.0f)]
    [SerializeField]
    private float shakesTimeSpan;
    [SerializeField]
    private AnimationCurve radiusOverTime;

    private PlayerController carController;
    private Vector3 orgRotation;
    private Vector3 direction;
    private Vector3 orgPosition;
    private float randomLerpFactor = 1;
    public Queue<Vector3> movingHistory;

    void Awake()
    {

        movingHistory = new Queue<Vector3>(5);
        carController = target.GetComponent<PlayerController>();
        Working = true;

        if (PlatformRecognition.IsTV)
        {
            positionOffset = positionOffsetOnTV;
        }
    }

    void Start()
    {
        orgPosition = transform.position;
        orgRotation = transform.rotation.eulerAngles;
    }

    void OnEnable()
    {
        GameplayManager.OnSessionEnded += RestartWithDelay;
        GameplayManager.OnPlayerCrash += OnCrash;
    }

    void OnDisable()
    {
        GameplayManager.OnSessionEnded -= RestartWithDelay;
        GameplayManager.OnPlayerCrash -= OnCrash;
    }

    private void OnCrash()
    {
        Shake();
    }

    private void RestartWithDelay()
    {

        StartCoroutine(RestartCor());
    }

    public void Restart()
    {
        StartCoroutine(RestartCor());
    }

    private IEnumerator RestartCor()
    {
        Working = true;
        yield return null;
        movingHistory.Clear();
        FocusTarget();
    }


    void FixedUpdate()
    {
        if (Working)
        {
            direction = Vector3.Lerp(direction, carController.GetNextTargetDirection(), 3 * Time.deltaTime);

            transform.position = orgPosition;
            Vector3 targetPosition = target.right * positionOffset.x + target.position + direction * positionOffset.z + Vector3.up * positionOffset.y;
            transform.position = Vector3.Lerp(transform.position, targetPosition + ShakeValue, positionLerp * Time.deltaTime);

            transform.rotation = GetCurrentTargetRotation();

            SaveHistory();
        }

        orgPosition = transform.position;
    }

    private void SaveHistory()
    {
        movingHistory.Enqueue(transform.position - orgPosition);
        if (movingHistory.Count > 5)
        {
            movingHistory.Dequeue();
        }
    }

    public Vector3 GetHistoryAverage()
    {
        Vector3 result = new Vector3();
        foreach (var item in movingHistory)
        {
            result += item;
        }
        result.x /= movingHistory.Count;
        result.y /= movingHistory.Count;
        result.z /= movingHistory.Count;

        return result;

    }

    public Quaternion GetCurrentTargetRotation()
    {
        Quaternion targetRotation = Quaternion.Euler(orgRotation.x, Quaternion.LookRotation(direction).eulerAngles.y, orgRotation.z);
        return Quaternion.Lerp(transform.rotation, targetRotation, rotationLerp * Time.deltaTime);
    }

    public void FocusTarget()
    {
        Vector3 targetForward = carController.GetNextTargetDirection();
        direction = targetForward;
        transform.position = target.right * positionOffset.x + target.position + targetForward * positionOffset.z + Vector3.up * positionOffset.y;
        orgPosition = transform.position;
        transform.rotation = Quaternion.Euler(orgRotation.x, Quaternion.LookRotation(targetForward).eulerAngles.y, orgRotation.z);
    }

    public void StopWorking()
    {
        Working = false;
    }

    public void StartWorking()
    {
        Working = true;
    }

    public void Shake()
    {
        StartCoroutine(ShakeCor());
    }

    private IEnumerator ShakeCor()
    {
        float timer = 0;
        while (timer < duration)
        {
            yield return CommonMethods.WaitForUnscaledSeconds(shakesTimeSpan);
            ShakeValue = Random.onUnitSphere * radius * radiusOverTime.Evaluate(timer / duration);
            timer += shakesTimeSpan;
        }

        ShakeValue = Vector3.zero;
    }

}
