﻿using UnityEngine;
using System.Collections;

public class PlayerParticles : MonoBehaviour
{

    public ParticleSystem[] DriftParticles { get { return driftParticles; } }
    public ParticleSystem[] Explosions { get { return explosions; } }

    [SerializeField]
    private ParticleSystem[] driftParticles;
    [SerializeField]
    private ParticleSystem[] explosions;

    public void Init(Transform[] modelDriftParticles, Transform[] modelExplosionParticles)
    {
        ApplyArrays(modelDriftParticles, driftParticles);
        ApplyArrays(modelExplosionParticles, explosions);
    }

    private void ApplyArrays(Transform[] from, ParticleSystem[] to)
    {
        for (int i = 0; i < from.Length; i++)
        {
            if (i < to.Length)
            {
                to[i].gameObject.SetActive(true);
                to[i].transform.position = from[i].transform.position;
                to[i].transform.rotation = from[i].transform.rotation;
                to[i].transform.localScale = from[i].transform.localScale;
            }
            else
            {
                to[i].gameObject.SetActive(false);
            }
        }

        for (int i = from.Length; i < to.Length; i++)
        {
            to[i].gameObject.SetActive(false);
        }
    }

    void FixedUpdate()
    {
        Vector3 playerRotation = transform.rotation.eulerAngles;
        playerRotation.x = -90;
        playerRotation.z = 0;

        foreach (var item in explosions)
        {
            item.transform.rotation = Quaternion.Euler(playerRotation);
        }
    }
}
