﻿using UnityEngine;
using System.Collections;

public class PlayerCollision : MonoBehaviour
{
    public BoxCollider collider;

    private const float MAX_ROLL_ANGLE = 60f;

    [SerializeField]
    private bool untouchable = false;
    [SerializeField]
    private PlayerCarModelAdapter modelAdapter;

    private ParticleSystem[] explosions;
    private bool collidable = true;
    private bool initialised = false;
    private bool wasLevelComplete = false;
    private Rigidbody rigidBody;

    public void Init(ParticleSystem[] explosions)
    {
        this.explosions = explosions;
        Restart();
    }

    void OnEnable()
    {
        GameplayManager.OnSessionEnded += Restart;
        LevelModeController.OnLevelComplete += OnLevelComplete;
        GameplayManager.OnContinueUsed += OnContinueUsedAction;
    }

    void OnDisable()
    {
        GameplayManager.OnSessionEnded -= Restart;
        LevelModeController.OnLevelComplete -= OnLevelComplete;
        GameplayManager.OnContinueUsed -= OnContinueUsedAction;
    }

    void Initialise()
    {
        rigidBody = GetComponent<Rigidbody>();
        initialised = true;
    }

    private void Restart()
    {
        if (!initialised)
        {
            Initialise();
        }

        CommonMethods.SetExplosionParticles(explosions, false, true);
        collidable = true;
        rigidBody.drag = 0;
        rigidBody.angularDrag = 0;
        wasLevelComplete = false;
    }

    private void OnLevelComplete()
    {
        wasLevelComplete = true;
    }

    void Update()
    {
        float angle = Vector3.Angle(Vector3.up, transform.up);

        if (collidable && angle > MAX_ROLL_ANGLE)
        {
            Crash(null);
        }
    }

    private void OnContinueUsedAction()
    {
        Restart();
        collidable = false;
        StartCoroutine(SetCollidableWithDelay(1.0f));
    }

    private IEnumerator SetCollidableWithDelay(float delay)
    {
        yield return new WaitForSeconds(delay);
        collidable = true;
    }

    void OnCollisionEnter(Collision collision)
    {
        if (!untouchable && collidable && collision.transform.tag == "PlayerKiller" && collision.contacts[0].thisCollider.gameObject == this.gameObject && !wasLevelComplete)
        {
            Crash(collision);
        }
    }

    private void Crash(Collision collision)
    {
        GameplayManager.OnPlayerCrash();

        collidable = false;
        CommonMethods.SetExplosionParticles(explosions, true, false);
        rigidBody.drag = GlobalParameters.carDragAfterCrash;
        rigidBody.angularDrag = GlobalParameters.carDragAfterCrash;

        if (collision != null)
        {
            Vector3 offset = transform.position - collision.contacts[0].point;
            rigidBody.AddForceAtPosition(Vector3.up * 100, transform.position + offset, ForceMode.Impulse);
        }

        if (modelAdapter.CurrentPlayerCarModel.Animator != null)
        {
            modelAdapter.CurrentPlayerCarModel.Animator.SetBool("Hit", true);
        }

    }

    public bool IsDead()
    {
        return !collidable;
    }

    public void CrashAfterExitFromPause()
    {
        Crash(null);
    }
}
