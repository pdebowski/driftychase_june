﻿using UnityEngine;
using System.Collections;

public class PlayerSirenSound : MonoBehaviour {

    private AudioSource sirenSource;

    void Awake()
    {
        sirenSource = GetComponent<AudioSource>();
    }

    void OnEnable()
    {
        GameManager.OnGameStateChanged += OnGameStateChanged;
    }

    void OnDisable()
    {
        GameManager.OnGameStateChanged -= OnGameStateChanged;
    }

	void OnGameStateChanged(GameManager.GameState newGameState)
    {
        if(newGameState == GameManager.GameState.Playing)
        {
            SoundManager.PlaySFX(sirenSource);
        }
        else
        {
            sirenSource.Stop();
        }
	}
}
