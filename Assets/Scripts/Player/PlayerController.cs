using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    public const float PLAYER_TURNING_OFFSET_PER_ONE_SPEED = 0.724897f;

    public static System.Action<TurnDirection> OnTurn = delegate { };

    public bool BlockInput { get; set; }
    public BoxCollider OuterCollider { get; private set; }
    public float Speed { get { return playerSpeed.Speed; } private set {; } }
    public bool Working { get; private set; }

    [Header("Turning")]
    [SerializeField]
    private float visualDriftMaxAngle = 35f;
    [SerializeField]
    private AnimationCurve driftVectorCurve;
    [SerializeField]
    private AnimationCurve driftRotationCurve;
    [SerializeField]
    private float driftTime = 0.5f;
    [SerializeField]
    private PlayerCarModelAdapter modelAdapter;

    private ParticleSystem[] driftParticles;
    private Vector3 targetRotation;
    private Vector3 nextTargetRotation;
    private int rotateSign = 1;
    private Vector3 startRotation;
    private Vector3 startPosition;
    private float driftPhase = 0;
    private bool initialised = false;
    private bool restarted = false;
    private Rigidbody rigidBody;
    private Vector3 precalculatedPosition;
    private bool isDrifting = false;
    private bool isReturningToRoad = false;
    private Vector3 returningToRoadDirection;
    private float returningToRoadLerpFactor = 0;
    private TurnDirection lastTurnDirection;
    private PlayerSpeedManager playerSpeed;
    private bool forcedDrift = false;
    private bool lerpedStop = false;
    private PlayerSounds playerSounds;

    void OnEnable()
    {
        GameplayManager.OnPlayerCrash += EndMoving;
        LevelModeController.OnLevelComplete += OnLevelComplete;
        GameplayManager.OnSessionEnded += Restart;
        GameplayManager.OnSessionStarted += StartMoving;
        GameplayManager.OnContinueUsed += OnContinueUsedAction;
        PlayerInput.OnTurnButtonClickedEnabled += OnTurnButtonClicked;
    }

    void OnDisable()
    {
        GameplayManager.OnPlayerCrash -= EndMoving;
        LevelModeController.OnLevelComplete -= OnLevelComplete;
        GameplayManager.OnSessionEnded -= Restart;
        GameplayManager.OnSessionStarted -= StartMoving;
        GameplayManager.OnContinueUsed -= OnContinueUsedAction;
        PlayerInput.OnTurnButtonClickedEnabled -= OnTurnButtonClicked;
    }

    public void Init(ParticleSystem[] driftParticles, BoxCollider outerCollider)
    {
        this.driftParticles = driftParticles;
        this.OuterCollider = outerCollider;
        Restart();
    }

    void FixedUpdate()
    {
        if (Working)
        {
            if (isDrifting)
            {
                driftPhase += Time.deltaTime / driftTime;

                if (driftPhase > 1)
                {
                    ScoreCounter.Instance.AddScore();
                    playerSpeed.OnPlayerTurnEnd();

                    isDrifting = false;
                    driftPhase = 0;
                    targetRotation = nextTargetRotation;
                    CommonMethods.SetExplosionParticles(driftParticles, false, false);
                    SetModelAnimatorValue("Forward", true);
                }
            }

            AdjustPosition();
            AdjustRotation();
        }
    }


    void OnTriggerStay(Collider collider)
    {
        if (collider.tag == "SideWalkTrigger")
        {
            isReturningToRoad = true;
            returningToRoadDirection = collider.transform.forward;
        }
    }

    void OnTriggerExit(Collider collider)
    {
        if (collider.tag == "SideWalkTrigger")
        {
            isReturningToRoad = false;
        }
    }

    public void OnTurnButtonClicked()
    {
        OnTurnButtonClicked(false);
    }

    public void OnTurnButtonClicked(bool forced)
    {
        if (Working && !isDrifting && (!BlockInput || forced))
        {
            StartDrift(forced);
        }
    }

    private void StartMoving()
    {
        if (restarted)
        {
            restarted = false;
            Working = true;
        }
    }

    private void EndMoving()
    {
        Working = false;
        CommonMethods.SetExplosionParticles(driftParticles, false, false);
    }

    private void OnLevelComplete()
    {
        Working = false;
        lerpedStop = true;
        CommonMethods.SetExplosionParticles(driftParticles, false, false);
        StartCoroutine(SlowDown());
    }

    private IEnumerator SlowDown()
    {
        while (lerpedStop)
        {
            rigidBody.AddForce(-rigidBody.velocity * rigidBody.mass * 5);
            rigidBody.AddTorque(-rigidBody.angularVelocity * rigidBody.mass * 20);
            yield return null;
        }
    }


    void Initialise()
    {
        rigidBody = GetComponent<Rigidbody>();
        playerSpeed = GetComponent<PlayerSpeedManager>();
        playerSounds = GetComponent<PlayerSounds>();

        targetRotation = transform.rotation.eulerAngles;
        startRotation = transform.rotation.eulerAngles;
        startPosition = transform.position;

        initialised = true;
    }

    private void StartDrift(bool forced)
    {
        forcedDrift = forced;
        TurnDirection direction;


        if (lastTurnDirection == TurnDirection.Left)
        {
            SetModelAnimatorValue("TurnR", true);
            direction = TurnDirection.Right;
        }
        else
        {
            SetModelAnimatorValue("TurnL", true);
            direction = TurnDirection.Left;
        }


        lastTurnDirection = direction;
        OnTurn(direction);
        if (direction == TurnDirection.Left)
        {
            rotateSign = -1;
        }
        else
        {
            rotateSign = 1;
        }

        CommonMethods.SetExplosionParticles(driftParticles, true, false);

        driftPhase = 0;
        nextTargetRotation = targetRotation + new Vector3(0, rotateSign * 90, 0);
        isDrifting = true;
        playerSounds.OnDriftStart();
    }

    private void SetModelAnimatorValue(string name, bool value)
    {
        if (modelAdapter.CurrentPlayerCarModel.Animator != null)
        {
            modelAdapter.CurrentPlayerCarModel.Animator.SetBool(name, value);
        }
    }


    private void AdjustPosition()
    {
        float directionFactor = driftVectorCurve.Evaluate(driftPhase);

        Vector3 direction = Vector3.Lerp(Quaternion.Euler(targetRotation) * Vector3.forward, Quaternion.Euler(nextTargetRotation) * Vector3.forward, directionFactor);
        Vector3 newRigidBodyPosition = precalculatedPosition + direction * playerSpeed.Speed * Time.deltaTime;

        Vector3 directionForward = direction;
        Vector3 directionRight = Quaternion.Euler(0, 90, 0) * direction;

        Vector3 forwardVelocity = Vector3.Project(rigidBody.velocity, directionForward);
        Vector3 forwardVelocityDelta = directionForward * playerSpeed.Speed - forwardVelocity;

        Vector3 rightVelocity = Vector3.Project(rigidBody.velocity, directionRight);
        Vector3 rightVelocityDelta = directionRight * 0 - rightVelocity;

        rigidBody.AddForce(forwardVelocityDelta * rigidBody.mass * 10);
        rigidBody.AddForce(rightVelocityDelta * rigidBody.mass * 10);


        rigidBody.AddForce((-Vector3.up * rigidBody.mass) / Time.deltaTime);

        if (isReturningToRoad)
        {
            rigidBody.AddForce(returningToRoadDirection * 1500);
        }


        precalculatedPosition += direction * playerSpeed.Speed * Time.deltaTime;
    }

    private void AdjustRotation()
    {
        Quaternion temporary = Quaternion.Lerp(Quaternion.Euler(targetRotation), Quaternion.Euler(nextTargetRotation), driftPhase);
        Quaternion endRotation = Quaternion.Euler(0, rotateSign * driftRotationCurve.Evaluate(driftPhase) * visualDriftMaxAngle, 0) * temporary;

        Vector3 newRotation = transform.rotation.eulerAngles;
        newRotation.y = endRotation.eulerAngles.y;

        Vector3 localDirection = transform.InverseTransformPoint(transform.position + returningToRoadDirection);
        float sign = Mathf.Sign(localDirection.x);
        returningToRoadLerpFactor = Mathf.Lerp(returningToRoadLerpFactor, isReturningToRoad ? 1 : 0, 3 * Time.deltaTime);
        newRotation.y += sign * 3 * returningToRoadLerpFactor;

        Quaternion finalRotation;
        finalRotation = Quaternion.Euler(newRotation);

        rigidBody.MoveRotation(finalRotation);
        rigidBody.angularVelocity = rigidBody.angularVelocity.ChangeY(0);
    }

    private float ClampAngle(float value, float maxAngle)
    {
        if (value < 180)
        {
            if (value < maxAngle)
            {
                return value;
            }
            else
            {
                return maxAngle;
            }
        }
        else
        {
            if (value < 360 - maxAngle)
            {
                return maxAngle;
            }
            else
            {
                return value;
            }
        }
    }


    private void Restart()
    {

        if (!initialised)
        {
            Initialise();
        }
        lerpedStop = false;
        BlockInput = false;
        driftPhase = 0;
        rigidBody.velocity = Vector3.zero;
        rigidBody.drag = 0;
        rigidBody.angularDrag = 0;
        rigidBody.angularVelocity = Vector3.zero;
        transform.position = startPosition;
        transform.rotation = Quaternion.Euler(startRotation);

        lastTurnDirection = TurnDirection.Left;
        targetRotation = startRotation;
        nextTargetRotation = targetRotation;

        rotateSign = -1;
        CommonMethods.SetExplosionParticles(driftParticles, false, false);
        precalculatedPosition = transform.position;
        EndMoving();
        restarted = true;
        isDrifting = false;
        isReturningToRoad = false;
        returningToRoadLerpFactor = 0;
        SetModelAnimatorValue("Forward", true);

    }

    private void OnContinueUsedAction()
    {
        driftPhase = 0;
        rigidBody.velocity = Vector3.zero;
        rigidBody.drag = 0;
        rigidBody.angularDrag = 0;
        rigidBody.angularVelocity = Vector3.zero;
        isDrifting = false;
        restarted = true;
        targetRotation = nextTargetRotation;
        transform.position = transform.position.ChangeY(startPosition.y);
        transform.rotation = Quaternion.Euler(targetRotation);
    }

    public void OnContinueCountingDown()
    {
        Working = true;
    }


    public bool IsBetweenBuildings()
    {
        Vector3 rayOrigin = transform.position;
        rayOrigin.y = 1;

        Ray ray = new Ray(rayOrigin, Quaternion.Euler(nextTargetRotation) * Vector3.forward);
        Ray ray2 = new Ray(rayOrigin, -ray.direction);

        if (Physics.Raycast(ray, 100, LayerMask.GetMask("Buildings")) || Physics.Raycast(ray2, 100, LayerMask.GetMask("Buildings")))
        {
            return true;
        }
        return false;
    }


    public Vector3 GetNextTargetDirection()
    {
        return Quaternion.Euler(nextTargetRotation) * Vector3.forward;
    }

    public void SetNextTargetRotation(Quaternion rotation)
    {
        Quaternion direction = Quaternion.Euler(nextTargetRotation);
        if (direction != rotation)
        {
            nextTargetRotation = rotation.eulerAngles;
            if (lastTurnDirection == TurnDirection.Left)
            {
                lastTurnDirection = TurnDirection.Right;
            }
            else
            {
                lastTurnDirection = TurnDirection.Left;
            }
        }
    }

    public Vector3 GetTargetRotation()
    {
        return targetRotation;
    }

    private bool GetInputDown()
    {
#if UNITY_EDITOR
        return Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.RightArrow) || Input.GetKeyDown(KeyCode.Space);
#else
        return Input.touchCount > 0 ? Input.touches[0].phase == TouchPhase.Began : false;
#endif
    }


}

public enum TurnDirection
{
    Left,
    Right,
    Forward,
    Back
}
