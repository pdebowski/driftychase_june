//
//  GTMIOS.h
//  GTMIOS
//
//  Created by Piotr Debowski on 15/02/16.
//  Copyright © 2016 Crimson Pine Games. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GTMImplementation.h"
@interface GTMIOS : NSObject

@end

extern "C"
{
    
    void _Initialize(const char* containerId, double timeout,bool isDebug);
    
    
    const char* _StringForKey(const char* key);
    bool _BoolForKey(const char* key);
    double _DoubleForKey(const char* key);
    int64_t _LongForKey(const char* key);
    void _PushValueToDataLayer(const char* key, const char* value);
    void _Refresh();
    bool _IsDefault();
    
    const char* _GetValueFromDataLayer(const char* key);
    double _LastRefreshTime();
}