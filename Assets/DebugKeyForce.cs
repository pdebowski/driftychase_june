﻿using UnityEngine;
using System.Collections;

public class DebugKeyForce : MonoBehaviour {

    [SerializeField]
    private Transform forceIndicator;
    [SerializeField]
    private float stregth = 1000;

	void FixedUpdate()
    {
        Rigidbody rigidBody = GetComponent<Rigidbody>();
        rigidBody.AddForce(forceIndicator.forward * stregth, ForceMode.Force);
    }
}
