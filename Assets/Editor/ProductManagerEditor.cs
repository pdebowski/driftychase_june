using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using System.Linq;
using CrimsonPlugins.Internal.IAP.ProductModel;
using CrimsonPlugins.Internal.IAP;
using CrimsonPlugins;


[CustomEditor(typeof(ProductManager))]
public class ProductManagerEditor : Editor {

	bool duplicatedNames=false;
	private int Rows{ get {
						return PlayerPrefs.GetInt ("__ROWS__", 0);
				} set {
						PlayerPrefs.SetInt ("__ROWS__", value);
				} 
	}

    //-------------------------------------------------------------
	public override void OnInspectorGUI()
	{
		base.OnInspectorGUI ();
		ProductManager myTarget = (ProductManager)target;
//		EditorGUILayout.ObjectField("hajni kontajdyt", myTarget.m_consumable[0]);

		int id = 0;

		updateArray (myTarget.m_consumable, ref id);
		updateArray (myTarget.m_nonConsumable, ref id);
		updateArray (myTarget.m_leveled, ref id);


		//must be before foreach
		int rowsNumber = myTarget.m_consumable.Length+myTarget.m_nonConsumable.Length+myTarget.m_leveled.Length;
		if (Rows > rowsNumber) {
			PlayerPrefs.DeleteAll();
		}
		Rows = rowsNumber;

		foreach (Item nonConsumables in myTarget.m_nonConsumable)
			nonConsumables.isConsumable = false;


		foreach (ItemLeveled it in myTarget.m_leveled) {
			it.costs=GetTrimmedArray<int>(it.costs,it);
			it.modifiers=GetTrimmedArray<float>(it.modifiers,it);
		}

		//duplicated names
		int length = myTarget.m_consumable.Length+myTarget.m_nonConsumable.Length+myTarget.m_leveled.Length;
		ItemBase[] allItems=new ItemBase[length];

		ItemBase[] items=myTarget.m_consumable.OfType<ItemBase>().ToArray();
		items.CopyTo (allItems, 0);
		
		items=myTarget.m_nonConsumable.OfType<ItemBase>().ToArray();
		items.CopyTo (allItems, myTarget.m_consumable.Length);
		
		items=myTarget.m_leveled.OfType<ItemBase>().ToArray();
		items.CopyTo (allItems, myTarget.m_consumable.Length+myTarget.m_nonConsumable.Length);

		List<ItemBase> secList = new List<ItemBase> ();

		bool duplVal = false;
		allItems.ToList ().ForEach(v=>{

			if (secList.Any(c=> c.name.Equals(v.name))) {
				duplVal=true;
				if (!duplicatedNames){

					List<int>ids=new List<int>();
					foreach (ItemBase iB in allItems){
						if (iB.name.Equals(v.name)){
							ids.Add(iB.id);
						}
					}
					string[] idsString=System.Array.ConvertAll(ids.ToArray(), x => x.ToString());
					EditorUtility.DisplayDialog("Duplicated name", "Duplicated name on ids: "+ string.Join(" and ",idsString ).ToString()  , "Ok");
					//System.Array.ConvertAll(ids.ToArray(), x => x.ToString()

					duplicatedNames=true;
				}
			
			}
			secList.Add(v);
		}
		);

		duplicatedNames = duplVal;

	}
    //-------------------------------------------------------------
	private T[] GetTrimmedArray<T>(T[] array,ItemLeveled it){
		it.numberOfLevels=Mathf.Clamp(it.numberOfLevels,0,int.MaxValue);
		
		List<T> arr=  new List<T>();
		arr.AddRange(array);
		
		//care aboout numberOflevels == arr.count
		if (arr.Count < it.numberOfLevels) {
			T[] v=new T[1];
				arr.AddRange (Enumerable.Repeat (v[0], it.numberOfLevels - arr.Count));
			}
		else 
			arr = arr.Where((val,index)=> index<it.numberOfLevels).ToList();

		return arr.ToArray ();
	}
    //-------------------------------------------------------------
	private void updateArray(ItemBase[] item, ref int id){
		foreach (ItemBase it in item?? new ItemBase[0] ) {
			it.id=id++;
		}
	}
    //-------------------------------------------------------------

}
