
#if dsjkfsdkf
using UnityEngine;
using UnityEditor;
#if UNITY_IPHONE
using UnityEditor.iOS.Xcode;
#endif
using UnityEditor.Callbacks;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System;

public class CloudPostBuild : MonoBehaviour 
{
	// Using the prime31 cloud plugin seems to create bitcode errors so you can disable it here if required
	const bool		bEnableBitcode = false;
	
	// You can enter your dev team GUID here and it will automatically get set in the xCode project
	// To get the GUID you need to set it manually once and then look in the xcode project file
	// for 'DevelopmentTeam =' to find the GUID for your team
	const string	TeamGUID = "";
	
	[PostProcessBuild]
	public static void OnPostprocessBuild(BuildTarget bt, string path)
	{
		#if UNITY_IPHONE
		UnityEngine.Debug.Log( "Post process Cloud Settings to xCode" );

		// Get xCode Project
		string projectPath = path + "/Unity-iPhone.xcodeproj/project.pbxproj";
		PBXProject project = new PBXProject();
		project.ReadFromFile( projectPath );
		string targetGuid = project.TargetGuidByName( "Unity-iPhone" );

		// Add cloudkit framework
		project.AddFrameworkToProject( targetGuid , "CloudKit.framework" , false );

		// Disable bitcode
		project.SetBuildProperty( targetGuid , "ENABLE_BITCODE" , bEnableBitcode == true ? "YES" : "NO" );

		// Copy the entitlements file, you should generate this once using xcode
		// then put it into Assets\Editor\app.entitlements
		string srcFilePath = Application.dataPath + "/Editor/CloudPostBuild/driftychase.entitlements";
		string dstFilePath = path + "/Unity-iPhone/driftychase.entitlements";
		UnityEngine.Debug.Log( string.Format( "Copy iCloud app entitlements from {0} to {1}" , srcFilePath , dstFilePath ) );
		File.Copy( srcFilePath , dstFilePath );
	
		srcFilePath = Application.dataPath +"/Plugins/IOS/GTM-K7FN2X";
		dstFilePath = "GTM-K7FN2X";
		UnityEngine.Debug.Log( string.Format( "Copy GTM file from {0} to {1}" , srcFilePath , dstFilePath ) );
		//File.Copy( srcFilePath , dstFilePath );
		project.AddFile(srcFilePath,dstFilePath,PBXSourceTree.Source);
		// Update xCode project to use entitlements and iCloud
		string projectString = project.WriteToString();

		// Add entitlements file
		projectString = projectString.Replace("/* Begin PBXFileReference section */",
			"/* Begin PBXFileReference section */\n\t\t244C317F1B8BE5CF00F39B20 /* driftychase.entitlements */ = {isa = PBXFileReference; lastKnownFileType = text.xml; name = driftychase.entitlements; path = \"Unity-iPhone/driftychase.entitlements\"; sourceTree = \"<group>\"; };");

		// Add entitlements file (again)
	/*	projectString = projectString.Replace("/* CustomTemplate */ = {\n			isa = PBXGroup;\n			children = (",
			"/* CustomTemplate */ = {\n			isa = PBXGroup;\n			children = (\n				244C317F1B8BE5CF00F39B20 /* driftychase.entitlements */,");

		// Add some kind of entitlements command
	/*	projectString = projectString.Replace("CLANG_WARN_DEPRECATED_OBJC_IMPLEMENTATIONS = YES;",
			"CLANG_WARN_DEPRECATED_OBJC_IMPLEMENTATIONS = YES;\n\t\t\t\tCODE_SIGN_ENTITLEMENTS = \"Unity-iPhone/driftychase.entitlements\";");

		// Setup team if required
		if( !String.IsNullOrEmpty( TeamGUID ) )
		{
			projectString = projectString.Replace(	"TargetAttributes = {" , 
													"TargetAttributes = {\n                    1D6058900D05DD3D006BFB54 = {\n                        DevelopmentTeam = " + TeamGUID + ";\n                    };" );
		}
 
		// Save the xCode project file
		File.WriteAllText( projectPath , projectString );
		#endif
	}
}

#endif
