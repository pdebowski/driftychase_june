﻿using UnityEngine;
using UnityEditor;
using System.IO;

public class EditorTools
{

	[MenuItem("Tools/ResetPlayerPrefs")]
	public static void ResetPlayerPrefsFun()
	{
		PlayerPrefs.DeleteAll();
		File.Delete(Application.persistentDataPath + "dataStuff.myData");
	}
}
