using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

public class FindShaderUse : EditorWindow {
	string st = "";
	string stArea = "Empty List";

	[MenuItem("Window/Find Shader Use")]
	public static void ShowWindow() {
		EditorWindow.GetWindow(typeof(FindShaderUse));
	}

	public void OnGUI() {
		GUILayout.Label("Enter shader to find:");
		st = GUILayout.TextField (st);
		if (GUILayout.Button("Find Materials")) {
			FindShader(st);
		}
		GUILayout.Label(stArea);
	}

	private void FindShader(string shaderName) {
		int count = 0;
		stArea = "Materials using shader " + shaderName+":\n\n";

		List<Material> armat = new List<Material>();

		Renderer[] arrend = (Renderer[])Resources.FindObjectsOfTypeAll(typeof(Renderer));
		foreach (Renderer rend in arrend) {
			foreach (Material mat in rend.sharedMaterials) {
			//	if (!armat.Contains (mat)) {
					armat.Add (mat);
			//	}
			}
		}

        foreach (string path in AssetDatabase.GetAllAssetPaths())
        {
            GameObject go = AssetDatabase.LoadMainAssetAtPath(path) as GameObject;
            Material mat = AssetDatabase.LoadMainAssetAtPath(path) as Material;
            if (go != null && go.GetComponent<Renderer>() != null)
            {
                armat.AddRange(go.GetComponent<Renderer>().sharedMaterials);
            }
            else if (mat != null)
            {
                armat.Add(mat);
            }
            else if (go == null)
            {
                UnityEngine.Debug.LogWarning("cannot cast prefab on path : " + path + " as Gameobject");
            }
        }

        foreach (Material mat in armat) {
			//if (mat != null && mat.shader != null && mat.shader.name != null && mat.shader.name == shaderName) {
			if (mat!=null && mat.shader.name.ToUpper().Contains("VERTEX"))
            {
				//stArea += ">"+mat.name +  " : " +mat.shader.name  + "\n";
				Debug.Log(">"+mat.name +  " : " +mat.shader.name);
				count++;
			}
		}

		stArea += "\n"+count + " materials using shader " + shaderName + " found.";
	}


}