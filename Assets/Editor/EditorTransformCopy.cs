﻿using UnityEngine;
using UnityEditor;
using System.Collections;

public class TransformCopy : MonoBehaviour
{
    // add "%#z" after "CopyTransforms" to add hotkey ctrl+shift+z
    [MenuItem("Tools/CopyTransforms")]
    public static void CopyTransforms()
    {
        foreach (var item in Selection.gameObjects)
        {
            GameObject newGameObject = new GameObject();
            newGameObject.name = item.name;
            newGameObject.transform.SetParent(item.transform.parent);
            newGameObject.transform.position = item.transform.position;
            newGameObject.transform.rotation = item.transform.rotation;
            newGameObject.transform.localScale = item.transform.localScale;
        }
    }
}
