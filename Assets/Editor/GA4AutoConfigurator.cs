using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;


[InitializeOnLoad]
public class GA4AutoConfigurator : MonoBehaviour {

    static string applicationVersion=null;
    static string bundleIdentifier = null;
    static string productName = null;

    static List<GameObject> googleV4=null;

	static double lastTimeUpdated;

    static GA4AutoConfigurator()
    {
		EditorApplication.update += () =>
        {
			if (lastTimeUpdated+1<EditorApplication.timeSinceStartup)
			{
				lastTimeUpdated=EditorApplication.timeSinceStartup;
			}
			else 
			{
				return;
			}

            if (Application.isPlaying) //Bugging in playmode
            {
                return;
            }

            googleV4 = GetGAPrefabs();

            bool somethingGoesWrong = false;

            if (applicationVersion == null || bundleIdentifier == null || productName == null 
                    || !Application.version.Equals(applicationVersion) || !Application.productName.Equals(productName) || !Application.identifier.Equals(bundleIdentifier)
               )
            {
                productName = Application.productName;
                bundleIdentifier = Application.identifier;
                applicationVersion = Application.version;
                foreach (var g in googleV4)
                {
                    GoogleAnalyticsV4 gav4 = g.GetComponent<GoogleAnalyticsV4>();
                    if (gav4 == null)
                    {
                        Debug.LogWarning("GA script removed ");
                        somethingGoesWrong = true;
                        break;
                    } //replace only if needed
                    else if (!gav4.productName.Equals(productName) ||
                            !gav4.bundleIdentifier.Equals(bundleIdentifier) ||
                            !gav4.bundleVersion.Equals(applicationVersion))
                    {
                        Debug.Log("GA Values :" +gav4.productName + " : " + productName + " : " 
                            + gav4.bundleIdentifier + " : " + bundleIdentifier + " : " + gav4.bundleVersion + " : " + applicationVersion);

                        gav4.productName = productName;
                        gav4.bundleIdentifier = bundleIdentifier;
                        gav4.bundleVersion = applicationVersion;
                        Debug.Log("Changed values in GA ");
                        EditorUtility.SetDirty(gav4);
                    }
                }

                if (somethingGoesWrong)
                {
                    googleV4 = null;
                }
                else
                {
                    AssetDatabase.SaveAssets();
                    AssetDatabase.Refresh();
                }
            }



        };
    }

    static List<GameObject> GetGAPrefabs()
    {
        List<GameObject> ret = new List<GameObject>();
        string[] allPrefabsPaths = GetAllPrefabsPaths();

        foreach (string path in allPrefabsPaths)
        {
            GameObject go = AssetDatabase.LoadMainAssetAtPath(path) as GameObject;
            if (go != null && go.GetComponent<GoogleAnalyticsV4>() != null)
            {
                ret.Add(go);
            }
            else if (go == null)
            {
                UnityEngine.Debug.LogWarning("cannot cast prefab on path : " + path + " as Gameobject");
            }
        }

        return ret;
    }

    static string[] GetAllPrefabsPaths()
    {
        string[] temp = AssetDatabase.GetAllAssetPaths();
        List<string> result = new List<string>();
        foreach (string s in temp)
        {
            if (s.Contains(".prefab")) result.Add(s);
        }
        return result.ToArray();
    }

}
