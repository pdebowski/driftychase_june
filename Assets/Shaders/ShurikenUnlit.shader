﻿Shader "Custom/ShurikenUnlit" {
	Properties{
		_Color("Color",Color) = (1,1,1)
	}

		SubShader{
			Tags{ "RenderType" = "Opaque" }
			LOD 100

			Pass{
			CGPROGRAM
		#pragma vertex vert
		#pragma fragment frag
		#pragma target 2.0
		#pragma multi_compile_fog

		#include "UnityCG.cginc"

		struct appdata_t {
			float4 vertex : POSITION;
			float4 color:COLOR;
		};

		struct v2f {
			float4 vertex : SV_POSITION;
			float4 color:COLOR;
			UNITY_FOG_COORDS(1)
		};

		uniform sampler2D _MainTex;
		uniform half4 _MainTex_ST;
		uniform half4 _Color;

		v2f vert(appdata_t v)
		{
			v2f o;
			o.vertex = UnityObjectToClipPos(v.vertex);
			o.color = v.color;
			UNITY_TRANSFER_FOG(o,o.vertex);
			return o;
		}

		fixed4 frag(v2f i) : SV_Target
		{
			return i.color;
		}
		ENDCG
		}
	}

}
