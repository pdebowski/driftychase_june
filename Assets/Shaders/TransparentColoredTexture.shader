﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'


Shader "Custom/TransparentColoredTexture" {
	Properties{
		_MainTex("Base (RGB)", 2D) = "white" {}
	_Color("Color", Color) = (1,1,1)
	}

		SubShader{
		Tags{ "Queue" = "Transparent" "IgnoreProjector" = "True" "RenderType" = "Transparent" }
		Blend SrcAlpha OneMinusSrcAlpha
		Lighting Off
		Pass{

		CGPROGRAM

#pragma fragment frag
#pragma vertex vert
#pragma fragmentoption ARB_precision_hint_fastest
#include "UnityCG.cginc"

		uniform sampler2D _MainTex;
	uniform float4 _Color;

	uniform float4 _MainTex_ST;
	struct v2f {
		float4 pos : SV_POSITION;
		float2 uv : TEXCOORD0;
	};

	v2f vert(appdata_base v)
	{
		v2f o;

		o.pos = UnityObjectToClipPos(v.vertex);
		o.uv = v.texcoord.xy * _MainTex_ST.xy + _MainTex_ST.zw;

		return o;
	}

	float4 frag(v2f i) : COLOR
	{
		//	float4 renderTex = tex2D(_MainTex, i.uv);
		return tex2D(_MainTex, i.uv)*_Color;
	}

		ENDCG

	}

	}
}



