﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/TextTransformation"
{
	Properties
	{
		 _MainTex("Sprite Texture", 2D) = "white" {}
		 _Color("Tint", Color) = (1,1,1,1)

		_StencilComp("Stencil Comparison", Float) = 8
		_Stencil("Stencil ID", Float) = 0
		_StencilOp("Stencil Operation", Float) = 0
		_StencilWriteMask("Stencil Write Mask", Float) = 255
		_StencilReadMask("Stencil Read Mask", Float) = 255
		_ColorMask("Color Mask", Float) = 15
		_YTransformationFactor("YTransformationFactor",float) = 0
		_XTransformationFactor("XTransformationFactor",float) = 1
	}

		SubShader
		 {
			 Tags
		 {
			 "Queue" = "Overlay"
			 "IgnoreProjector" = "True"
			 "RenderType" = "Transparent"
			 "PreviewType" = "Plane"
			 "CanUseSpriteAtlas" = "True"
		 }

			 Stencil
		 {
			 Ref[_Stencil]
			 Comp[_StencilComp]
			 Pass[_StencilOp]
			 ReadMask[_StencilReadMask]
			 WriteMask[_StencilWriteMask]
		 }

			 Cull Off
			 Lighting Off
			 ZWrite Off
			 ZTest Off
			 Blend SrcAlpha OneMinusSrcAlpha
			 ColorMask[_ColorMask]

			 Pass
		 {
			 CGPROGRAM
	 #pragma vertex vert
	 #pragma fragment frag
	 #include "UnityCG.cginc"


		 struct v2f
		 {
			 float4 vertex   : SV_POSITION;
			 fixed4 color : COLOR;
			 half2 texcoord  : TEXCOORD0;
		 };

		 fixed4 _Color;
		 float _YTransformationFactor;
		 float _XTransformationFactor;

		 //if u want use this shader in UI.Text component, you must add TextTransformationShaderHelper script to this Text component too.
		 // in IN.normal.xy are calculated normalized vertexes positions
		 // in IN.tangent.xy are calculated real size of TEXT area
		 v2f vert(appdata_full IN)
		 {
			 v2f OUT;
			 OUT.vertex = UnityObjectToClipPos(IN.vertex);

			 OUT.texcoord = IN.texcoord;
			 OUT.color = _Color;

			 OUT.vertex.y += (_YTransformationFactor / 1000) * IN.normal.x * IN.tangent.y;

			 OUT.vertex.x += (_XTransformationFactor / 1000) * IN.normal.y *IN.tangent.x;

	 #ifdef UNITY_HALF_TEXEL_OFFSET
			 OUT.vertex.xy += (_ScreenParams.zw - 1.0)*float2(-1,1);
	 #endif

			 return OUT;
		 }

		 sampler2D _MainTex;

		 fixed4 frag(v2f IN) : SV_Target
		 {
			 half4 color = tex2D(_MainTex, IN.texcoord);
			 color = half4(_Color.r, _Color.g, _Color.b, color.a * _Color.a);
				 clip(color.a - 0.01);
				 return color;
			 }
				 ENDCG
			 }
		 }
}