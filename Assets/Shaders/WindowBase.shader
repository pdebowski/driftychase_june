// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/WindowBase" {
		Properties{
			_Color("Main Color", Color) = (1,1,1,1)
			_WindowTexture("Window Texture", 2D) = "white" {}
			_GlowTexture("Glow Texture", 2D) = "white" {}
			_GlowStrength("Glow Strength", Float) = 1

				_Bright("Bright",Float) = 0
				_C1("C1", Float) = 1
				_C2("C2", Float) = 1
				_C3("C3", Float) = 1
				[MaterialToggle] _ISSephia("Sephia", Float) = 0
				_SephiaFact("SephiaFactor", Float) = 0
		}

			SubShader{
			Tags{ "RenderType" = "Opaque" }
			LOD 100

			Pass{
			CGPROGRAM
#pragma vertex vert
#pragma fragment frag

#include "UnityCG.cginc"

		struct appdata_t {
			float4 vertex : POSITION;
			float2 texcoord : TEXCOORD0;
		};

		struct v2f {
			float4 vertex : SV_POSITION;
			half2 texcoord : TEXCOORD0;
		};

		sampler2D _WindowTexture;
		float4 _WindowTexture_ST;
		sampler2D _GlowTexture;
		float _GlowStrength;
		half4 _Color;
		
		v2f vert(appdata_t v)
		{
			v2f o;
			o.vertex = UnityObjectToClipPos(v.vertex);
			o.texcoord = TRANSFORM_TEX(v.texcoord, _WindowTexture);
			return o;
		}

		fixed4 frag(v2f i) : SV_Target
		{
			fixed4 col = _Color * tex2D(_WindowTexture, i.texcoord) + tex2D(_GlowTexture, i.texcoord) * _GlowStrength;
			return col;
		}
			ENDCG
		}
		}

}
