﻿// Simplified Diffuse shader. Differences from regular Diffuse one:
// - no Main Color
// - fully supports only 1 directional light. Other lights can affect it, but it will be per-vertex/SH.

Shader "Custom/CarWithAO" {
	Properties{
		//_MainTex("Base (RGB)", 2D) = "white" {}
		_Color("Color", Color) = (1,1,1)
	_AO("AO", 2D) = "white" {}
	}
		SubShader{
		Tags{ "RenderType" = "Opaque" }
		CGPROGRAM
#pragma surface surf Lambert

	struct Input {
		float2 uv_Sticker;
		float2 uv2_Lightmap;
		float2 uv3_AO;
	};


	uniform sampler2D _AO;
	half4 _Color;

	void surf(Input IN, inout SurfaceOutput o) {

		fixed4 color = _Color * tex2D(_AO, IN.uv3_AO);

		o.Albedo = color;
	}



	ENDCG
	}
		Fallback "Diffuse"
}