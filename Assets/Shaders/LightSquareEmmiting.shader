// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/LightSquareEmmiting"
{
		Properties
		{
			_TintColor("Tint",Color) = (1,1,1,1)
			_MainTex("Texture", 2D) = "white" {}
			_Duration("Duration", Float) = 1.0
		}
			SubShader
			{
				Tags{ "Queue" = "Transparent" "IgnoreProjector" = "True" "RenderType" = "Transparent" "PreviewType" = "Plane" }
				Blend SrcAlpha One
				Cull Off Lighting Off ZWrite Off

				Pass
			{
				CGPROGRAM

#pragma vertex vert
#pragma fragment frag NoLighting exclude_path:prepass nolightmap noforwardadd halfasview novertexlights ARB_precision_hint_fastest
#pragma target 2.0
#pragma multi_compile_particles

#include "UnityCG.cginc"

		struct appdata
		{
			fixed4 vertex : POSITION;
			fixed2 uv : TEXCOORD0;
		};

		struct v2f
		{
			fixed2 uv : TEXCOORD0;
			fixed4 vertex : SV_POSITION;
		};

		uniform sampler2D _MainTex;
		uniform fixed _Duration;
		uniform fixed4 _TintColor;

		v2f vert(appdata v)
		{
			v2f o;
			o.vertex = UnityObjectToClipPos(v.vertex);
			o.uv = v.uv;
			return o;
		}

		fixed4 frag(v2f i) : SV_Target
		{
			fixed4 col = tex2D(_MainTex, i.uv);
			col *= _TintColor;
			col.a +=  lerp(0.1,0.2, abs(frac(_Time.y/_Duration)-0.5));
			return col;
		}
			ENDCG
		}
		}
	}
