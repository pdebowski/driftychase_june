Shader "Custom/DiffuseWithAmbient" {
	Properties{
		_MainTex("Base (RGB)", 2D) = "white" {}
	    _DiffuseStrength("DiffuseStrength", Float) = 1
		_Color("Color", Color) = (1,1,1)		
		_AmbientStrength("AmbientStrength", Float) = 1
	}
		SubShader{
		Tags{ "RenderType" = "Opaque" }
		CGPROGRAM
#pragma surface surf Lambert finalcolor:mycolor


	struct Input {
		float2 uv_MainTex;
		float3 worldPos;
	};


	uniform sampler2D _MainTex;
	uniform float4 _Color;
	uniform float _DiffuseStrength;
	uniform float _AmbientStrength;

	void surf(Input IN, inout SurfaceOutput o) {
		o.Albedo = tex2D(_MainTex,IN.uv_MainTex).rgb * _DiffuseStrength;
	}

	void mycolor(Input IN, SurfaceOutput o, inout fixed4 color)
	{
		color.rgb = color.rgb + _Color * _AmbientStrength;
	}

	ENDCG
	}
		Fallback "Diffuse"
}