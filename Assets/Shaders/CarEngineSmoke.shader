﻿Shader "Custom/CarEngineSmoke" {
	Properties{
		_MainTex("Base (RGB)", 2D) = "white" {}
		_ColorTop("ColorTop", Color) = (1,1,1)
		_ColorBottom("ColorBottom", Color) = (1,1,1)
		_StartOffset("StartOffset",Float) = 5
		_Distance("Distance", Float) = 20
		_StartAlphaOffset("StartAlphaOffset",Float) = 2
		_AlphaDistance("_AlphaDistance",Float) = 2
	}
		SubShader{
		Tags{ "RenderType" = "Opaque" }
		CGPROGRAM
#pragma surface surf Lambert alpha finalcolor:mycolor


	struct Input {
		float3 color : COLOR;
		float3 worldPos;
	};


	uniform sampler2D _MainTex;
	uniform float4 _MainTex_ST;
	uniform float _Distance;
	uniform float4 _ColorBottom;
	uniform float4 _ColorTop;
	uniform float _StartOffset;
	uniform float _AlphaDistance;
	uniform float _StartAlphaOffset;


	void surf(Input IN, inout SurfaceOutput o) {
		o.Albedo = _ColorTop;
	}

	void mycolor(Input IN, SurfaceOutput o, inout fixed4 color)
	{
		float value = (IN.worldPos.y - _StartOffset) / _Distance;
		value = clamp(value, 0, 1);
		color.rgb = value * color + (1- value) * _ColorBottom;
		float alphaFactor = (IN.worldPos.y - _StartAlphaOffset) / _AlphaDistance;
		float alpha = lerp(_ColorBottom.a, _ColorTop.a, alphaFactor);
		color.a = alpha;
	}

	ENDCG
	}
		Fallback "Diffuse"
}