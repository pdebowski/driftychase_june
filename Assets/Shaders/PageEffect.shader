﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/PageEffect"
{
	Properties
	{
		_MainTex("Page", 2D) = "white" {}
		_Corner("Corner", 2D) = "white" {}

		_StencilComp("Stencil Comparison", Float) = 8
		_Stencil("Stencil ID", Float) = 0
		_StencilOp("Stencil Operation", Float) = 0
		_StencilWriteMask("Stencil Write Mask", Float) = 255
		_StencilReadMask("Stencil Read Mask", Float) = 255
		_ColorMask("Color Mask", Float) = 15
		_Tilt("Tilt",float) = -1
		_ZeroPoint("ZeroPoint",float) = 1.5
	}

		SubShader
		{
			Tags
		{
			"Queue" = "Overlay"
			"IgnoreProjector" = "True"
			"RenderType" = "Transparent"
			"PreviewType" = "Plane"
			"CanUseSpriteAtlas" = "True"
		}

			Stencil
		{
			Ref[_Stencil]
			Comp[_StencilComp]
			Pass[_StencilOp]
			ReadMask[_StencilReadMask]
			WriteMask[_StencilWriteMask]
		}

			Cull Off
			Lighting Off
			ZWrite Off
			ZTest Off
			Blend SrcAlpha OneMinusSrcAlpha
			ColorMask[_ColorMask]

			Pass
		{
			CGPROGRAM
	#pragma vertex vert
	#pragma fragment frag
	#include "UnityCG.cginc"


		struct v2f
		{
			float4 vertex   : SV_POSITION;
			fixed4 color : COLOR;
			half2 texcoord  : TEXCOORD0;
		};

		float _Tilt;
		float _ZeroPoint;
		sampler2D _MainTex;
		sampler2D _Corner;

		v2f vert(appdata_full IN)
		{
			v2f OUT;
			OUT.vertex = UnityObjectToClipPos(IN.vertex);

			OUT.texcoord = IN.texcoord;

	#ifdef UNITY_HALF_TEXEL_OFFSET
			OUT.vertex.xy += (_ScreenParams.zw - 1.0)*float2(-1,1);
	#endif

			return OUT;
		}



		fixed4 frag(v2f IN) : SV_Target
		{
			half4 color = tex2D(_MainTex, IN.texcoord);

			float yPositionOnX_1 = _ZeroPoint + _Tilt;
			float xPositionOnY_1 = (1 - _ZeroPoint) / _Tilt;

			clip(((_Tilt * IN.texcoord.x + _ZeroPoint) - IN.texcoord.y));

			float xCornerCoord = (IN.texcoord.x - xPositionOnY_1) / (1 - xPositionOnY_1);
			float yCornerCoord = (IN.texcoord.y - yPositionOnX_1) / (1 - yPositionOnX_1);

			half4 cornerColor = tex2D(_Corner, float2(xCornerCoord, yCornerCoord));

			color = lerp(color, cornerColor, cornerColor.a);
			color.a = 1;

			clip(color.a - 0.01);
			return color;
		}
			ENDCG
		}
		}
}