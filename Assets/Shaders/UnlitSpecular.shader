// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "Custom/UnlitSpecular" {
	Properties{
		_Color("Main Color", Color) = (1,1,1,1)
		_SpecColor("Specular Color", Color) = (0.5, 0.5, 0.5, 1)
		_Shininess("Shininess", Range(0.01, 1)) = 0.078125
		_MainTex("Base (RGB) Gloss (A)", 2D) = "white" {}
	}
		SubShader{
		Tags{ "RenderType" = "Opaque" }
		LOD 300

		CGPROGRAM
#pragma surface surf UnlitBlinnPhong noforwardadd vertex:vert
#include "UnityGlobalIllumination.cginc"

	uniform sampler2D _MainTex;
	uniform fixed4 _Color;
	uniform half _Shininess;
	uniform float4 _MainTex_ST;

	inline fixed4 UnityUnlitBlinnPhongLight(SurfaceOutput s, half3 viewDir, UnityLight light)
	{
		half3 h = normalize(light.dir + viewDir);
		float nh = max(0, dot(s.Normal, h));
		float spec = pow(nh, s.Specular*128.0) * s.Gloss;

		fixed4 c;
		c.rgb = s.Albedo  + light.color *_SpecColor.rgb * spec;
		c.a = s.Alpha;

		return c;
	}

	inline fixed4 LightingUnlitBlinnPhong(SurfaceOutput s, half3 viewDir, UnityGI gi)
	{
		fixed4 c;
		c = UnityUnlitBlinnPhongLight(s, viewDir, gi.light);

#if defined(DIRLIGHTMAP_SEPARATE)
#ifdef LIGHTMAP_ON
		c += UnityUnlitBlinnPhongLight(s, viewDir, gi.light2);
#endif
#ifdef DYNAMICLIGHTMAP_ON
		c += UnityUnlitBlinnPhongLight(s, viewDir, gi.light3);
#endif
#endif

#ifdef UNITY_LIGHT_FUNCTION_APPLY_INDIRECT
		c.rgb += s.Albedo * gi.indirect.diffuse;
#endif

		return c;
	}

	inline void LightingUnlitBlinnPhong_GI(
		SurfaceOutput s,
		UnityGIInput data,
		inout UnityGI gi)
	{
		gi = UnityGlobalIllumination(data, 1.0, s.Normal);
	}


	struct Input {
		float2 texPos;
		float2 uv_MainTex;
		float2 uv_Illum;
	};

	void vert(inout appdata_full v, out Input o) {
		UNITY_INITIALIZE_OUTPUT(Input, o);



		float4 pos = mul(unity_ObjectToWorld, v.vertex);
		pos.x = pos.x;
		pos.y = pos.z;
		o.texPos = pos.xy;
		o.texPos =TRANSFORM_TEX(o.texPos, _MainTex);
/*
		float _RotationSpeed=1.1;
		float sinX = sin ( _RotationSpeed * _Time );
            float cosX = cos ( _RotationSpeed * _Time );
            float sinY = sin ( _RotationSpeed * _Time );
            float2x2 rotationMatrix = float2x2( 1, -sinX, sinY, cosX);
       //     o.texPos.xy = mul ( o.texPos.xy, rotationMatrix );
	   */
	}


	void surf(Input IN, inout SurfaceOutput o) {
		fixed4 tex = tex2D(_MainTex, IN.texPos);
		//tex *=  _Color.a;
		fixed4 c = lerp(fixed4(1,1,1,1), tex,_Color.a)* _Color;
		o.Albedo = c.rgb;
		o.Gloss = tex.a;
		o.Alpha = c.a;
		o.Specular = _Shininess;
	}
	ENDCG
	}
		FallBack "Legacy Shaders/Self-Illumin/Diffuse"
}
