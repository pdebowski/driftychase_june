// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/WindowAnimated" {
		Properties{
			_Color("Main Color", Color) = (1,1,1,1)
			_Offset("Offset", Float) = 0
			_AnimatedTexture("Animated Texture", 2D) = "white" {}
			_AnimatedStrength("Animated Strength", Float) = 1
			_WindowTexture("Window Texture", 2D) = "white" {}

			_LogoTexture("Logo Texture", 2D) = "white" {}
			_LogoColor("Logo Color", Color) = (1,1,1,1)
			_BackgroundColor("Background Color", Color) = (0,0,0,1)
			_ScaleX("ScaleX",Float) = 1
			_ScaleY("ScaleY",Float) = 1
		}

			SubShader{
			Tags{ "RenderType" = "Opaque" }
			LOD 100

			Pass{
			CGPROGRAM
#pragma vertex vert
#pragma fragment frag

#include "Effects.cginc"
#include "UnityCG.cginc"

		struct appdata_t {
			float4 vertex : POSITION;
			float2 texcoord : TEXCOORD0;
		};

		struct v2f {
			float4 vertex : SV_POSITION;
			half2 texcoord : TEXCOORD0;
		};

		sampler2D _AnimatedTexture;
		float4 _AnimatedTexture_ST;
		sampler2D _WindowTexture;
		float _Offset;
		float _AnimatedStrength;
		float _Strength;
		half4 _Color;
		
		sampler2D _LogoTexture;
		half4 _LogoColor;
		half4 _BackgroundColor;
		float _ScaleX;
		float _ScaleY;


		v2f vert(appdata_t v)
		{
			v2f o;
			o.vertex = UnityObjectToClipPos(v.vertex);
			o.texcoord = TRANSFORM_TEX(v.texcoord, _AnimatedTexture);
			return o;
		}

		fixed4 frag(v2f i) : SV_Target
		{
			float2 baseCoord = i.texcoord;
			float2 animCoord = baseCoord;
			animCoord.x -= _Offset;

			float2 logoCoord = i.texcoord;
			logoCoord -= 0.5;
			logoCoord.x *= 1 / _ScaleX;
			logoCoord.y *= 1 / _ScaleY;
			logoCoord += 0.5;

			half4 logoCol = tex2D(_LogoTexture, logoCoord);

			logoCol = _LogoColor * logoCol.a + _BackgroundColor * (1 - logoCol.a);

			fixed4 col = (_Color + logoCol + _AnimatedStrength * tex2D(_AnimatedTexture, animCoord)) * tex2D(_WindowTexture, baseCoord);
			
			return col;
		}
			ENDCG
		}
		}

}
