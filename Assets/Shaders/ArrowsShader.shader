Shader "Custom/Arrows Shader" {
	Properties{
		_ArrowColor("Arrow Color", Color) = (1,1,1,1)
		_BarColor("Bar Color", Color) = (1,1,1,1)
		_Texture("Texture", 2D) = "white" {}
	}

		Category{
		Tags{ "Queue" = "Transparent" "IgnoreProjector" = "True" "RenderType" = "Transparent" "PreviewType" = "Plane" }
		Blend SrcAlpha OneMinusSrcAlpha
		ColorMask RGB
		Cull Off Lighting Off ZWrite Off

		SubShader{
		Pass{

		CGPROGRAM
#pragma vertex vert NoLighting ARB_precision_hint_fastest
#pragma fragment frag

#include "UnityCG.cginc"

	uniform sampler2D _Texture;
	uniform float4 _Texture_ST;
	uniform fixed4 _ArrowColor;
	uniform fixed4 _BarColor;


	struct appdata_t {
		float4 vertex : POSITION;
		float2 texcoord : TEXCOORD0;
	};

	struct v2f {
		float4 vertex : SV_POSITION;
		float2 texcoord : TEXCOORD0;
	};


	v2f vert(appdata_t v)
	{
		v2f o;
		o.vertex = UnityObjectToClipPos(v.vertex);
		o.texcoord = v.texcoord.xy*_Texture_ST.xy + float2(-_Time.z, _Texture_ST.w);
		
		return o;
	}

	sampler2D_float _CameraDepthTexture;
	float _InvFade;

	fixed4 frag(v2f i) : SV_Target
	{

		float4 col = tex2D(_Texture, i.texcoord);
		col.rgb = fixed4(col.r, col.r, col.r, col.a) * _ArrowColor +float4(col.b, col.b, col.b, col.a) *_BarColor;

		return col;
	}
		ENDCG
}
}
	}}