//simple mobile diffuse shader with sincity effect

Shader "Custom/SinCity/CarWithAO_NEW" {
	Properties{
		_AO("AO", 2D) = "white" {}

	}
		SubShader{
		Tags{ "RenderType" = "Opaque" }
		CGPROGRAM

#pragma surface surf CustomLambert
#include "../Effects.cginc"


		struct Input {
		float2 uv_AO;
		float4 color;
	};



	uniform sampler2D _AO;

	float _Bright;
	float _C1;
	float _C2;
	float _C3;
	float _ISSephia;
	float _SephiaFact;


	void LightingCustomLambert_GI(
		SurfaceOutput s,
		UnityGIInput data,
		inout UnityGI gi)
	{
		gi = UnityGlobalIllumination(data, 1.0, s.Normal);
	}

	

	fixed4 LightingCustomLambert(SurfaceOutput s, UnityGI gi)
	{

		fixed4 unlitColor = fixed4(1, 1, 1, 1);
		unlitColor.rgb = s.Albedo*float3(0.5, 0.5, 0.5);

	
		unlitColor = ApplySinCityEffect(unlitColor);

		unlitColor.rgb /= float3(0.5, 0.5, 0.5);
		SurfaceOutput newS = s;
		newS.Albedo = unlitColor.rgb;

		fixed4 c;
		c = UnityLambertLight(newS, gi.light);

#if defined(DIRLIGHTMAP_SEPARATE)
#ifdef LIGHTMAP_ON
		c += UnityLambertLight(newS, gi.light2);
#endif
#ifdef DYNAMICLIGHTMAP_ON
		c += UnityLambertLight(newS, gi.light3);
#endif
#endif

#ifdef UNITY_LIGHT_FUNCTION_APPLY_INDIRECT
		c.rgb += newS.Albedo * gi.indirect.diffuse;
#endif
		
		return c;
	}


	void surf(Input IN, inout SurfaceOutput o) {

		fixed4 color = tex2D(_AO, IN.uv_AO);
		o.Albedo = color;// +float4(0.6, 0.6, 0.6, 0.6) * 2;
	}



	ENDCG
	}
		Fallback "Diffuse"
}
