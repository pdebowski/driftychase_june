﻿// Simplified Diffuse shader. Differences from regular Diffuse one:
// - no Main Color
// - fully supports only 1 directional light. Other lights can affect it, but it will be per-vertex/SH.

Shader "Custom/CarWithAO_NEW" {
	Properties{
		_AO("AO", 2D) = "white" {}
	}
		SubShader{
		Tags{ "RenderType" = "Opaque" }
		CGPROGRAM
#pragma surface surf Lambert

	struct Input {
		float2 uv_AO;
	};


	uniform sampler2D _AO;

	void surf(Input IN, inout SurfaceOutput o) {

		fixed4 color = tex2D(_AO, IN.uv_AO);
		o.Albedo = color;
	}



	ENDCG
	}
		Fallback "Diffuse"
}