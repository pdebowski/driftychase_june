// Simplified Diffuse shader. Differences from regular Diffuse one:
// - no Main Color
// - fully supports only 1 directional light. Other lights can affect it, but it will be per-vertex/SH.

Shader "Custom/BuildingsFromBlenderOld" {
	Properties{
		_Color("Color", Color) = (1,1,1)
		_Tint("Tint", Color) = (1,1,1)
		_AO("AO", 2D) = "white" {}		
	}

		SubShader{
		Tags{ "RenderType" = "Opaque" "Queue" = "Geometry" }

		CGPROGRAM


#pragma surface surf NoLighting  noambient noforwardadd

	struct Input {
		float2 uv_AO;
	};

	uniform sampler2D _AO;
	fixed4 _Color;
	fixed4 _Tint;

	fixed4 LightingNoLighting(SurfaceOutput s, fixed3 lightDir, fixed atten)
	{
		fixed4 c;
		c.rgb = s.Albedo;
		c.a = s.Alpha;
		return c;
	}

	void surf(Input IN, inout SurfaceOutput o) {

		fixed4 color = _Color * _Tint * tex2D(_AO, IN.uv_AO);
		o.Albedo = color;
	}


	ENDCG
	}

		Fallback "Mobile/VertexLit"
}