Shader "Custom/CarLightFlare"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_Scale("Scale", Float) = 1.0
	    _Alpha("Alpha", Float) = 1.0
			}
	SubShader
	{
		Tags{ "Queue" = "Transparent" "IgnoreProjector" = "True" "RenderType" = "Transparent" "PreviewType" = "Plane" }
		Blend SrcAlpha One
		Cull Off Lighting Off ZWrite Off

		Pass
		{
			CGPROGRAM

#pragma vertex vert
#pragma fragment frag NoLighting exclude_path:prepass nolightmap noforwardadd halfasview interpolateview

			#include "UnityCG.cginc"

			struct appdata
			{
				fixed4 vertex : POSITION;
				fixed2 uv : TEXCOORD0;
			};

			struct v2f
			{
				fixed2 uv : TEXCOORD0;
				fixed4 vertex : SV_POSITION;
			};

			uniform sampler2D _MainTex;
			uniform fixed _Scale;
			uniform fixed _Alpha;
			
			v2f vert (appdata v)
			{
				v2f o;
				
				o.vertex = mul(UNITY_MATRIX_P,
					mul(UNITY_MATRIX_MV, fixed4(0.0, 0.0, 0.0, 1.0))
			     	- fixed4(v.vertex.x * _Scale, v.vertex.y * _Scale, 0.0, 0.0));

				o.uv = v.uv;
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				fixed4 col = tex2D(_MainTex, i.uv);
				col.a = _Alpha;
				return col;
			}
			ENDCG
		}
	}
}
