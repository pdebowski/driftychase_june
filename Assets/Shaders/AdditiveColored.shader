﻿Shader "Custom/AdditiveColored" {
	Properties
	{
		_Color("Main Color", Color) = (1,1,1,1)
		_MainTex("Texture", 2D) = "white" {}
	}

		SubShader
	{
		Cull Off
		Tags
	{
		"Queue" = "Transparent"
		"IgnoreProjector" = "True"
		"RenderType" = "TransparentCutoff"
	}
		Pass
	{
		Blend One One
		AlphaToMask True
		ColorMask RGB


		SetTexture[_MainTex]
	{
		Combine texture, texture
	}
		SetTexture[_MainTex]{
		constantColor[_Color]
		Combine previous * constant, previous * constant
	}
	}
	}

}