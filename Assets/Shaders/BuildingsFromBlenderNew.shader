// Simplified Diffuse shader. Differences from regular Diffuse one:
// - no Main Color
// - fully supports only 1 directional light. Other lights can affect it, but it will be per-vertex/SH.

Shader "Custom/BuildingsFromBlender" {
	Properties{
		_Color("Color", Color) = (1,1,1)
		_Tint("Tint", Color) = (1,1,1)
		_AO("AO", 2D) = "white" {}
		_ColoredTexture("Colored Texture", 2D) = "white" {}
		_LightsMap("LightsMap", 2D) = "black" {}
		_LightsContrast("LightsContrast",Range(1,10)) = 5
		_LightsStrength("LightsStrength",Range(0,1)) = 1
	}

		SubShader{
		Tags{ "RenderType" = "Opaque" "Queue" = "Geometry" }

		CGPROGRAM

#include "Effects.cginc"

#pragma surface surf NoLighting noambient noforwardadd vertex:vert

	struct Input {
		float2 uv_AO;
		float2 getUV2;
	};

	uniform sampler2D _AO;
	uniform sampler2D _ColoredTexture;
	uniform sampler2D _LightsMap;
	float _LightsContrast;
	float _LightsStrength;
	fixed4 _Color;
	fixed4 _Tint;

	void vert(inout appdata_full v, out Input o)
	{
		o.uv_AO = v.texcoord1.xy;
		o.getUV2 = v.texcoord2.xy;
	}

	fixed4 LightingNoLighting(SurfaceOutput s, fixed3 lightDir, fixed atten)
	{
		fixed4 c;
		c.rgb = s.Albedo;
		return c;
	}

	void surf(Input IN, inout SurfaceOutput o)
	{
		half4 lightsColor = pow(tex2D(_LightsMap, IN.uv_AO), _LightsContrast) * _LightsStrength;
		half4 col = clamp(_Color * _Tint  * tex2D(_ColoredTexture, IN.getUV2) * tex2D(_AO, IN.uv_AO) + lightsColor,0.0,1.0);
		o.Albedo = col;
	}


	ENDCG
		}

			Fallback "Mobile/VertexLit"
}
