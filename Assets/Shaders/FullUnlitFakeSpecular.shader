// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/FullUnlitFakeSpecular"
{
	Properties
	{
		_Color("MainColor", Color) = (1,1,1,1)
		_MainTex ("Texture", 2D) = "white" {}
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" "Queue"="Geometry" }
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 texcoord : TEXCOORD0;
			};

			struct v2f
			{
				float4 vertex : SV_POSITION;
				float2 texcoord:TEXCOORD0;
				float4 cameraRotation:Color;

			};

			uniform sampler2D _MainTex;
			uniform float4 _MainTex_ST;
			uniform fixed4 _Color;
			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				float4 pos = mul(UNITY_MATRIX_MV, v.vertex);
				
				o.cameraRotation =mul((float3x3)UNITY_MATRIX_V, float3(0, 0, 1)).y;

				o.texcoord = TRANSFORM_TEX(pos, _MainTex);
		//		o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
			    
				fixed4 col = _Color + tex2D(_MainTex, i.texcoord) * max(0,(i.cameraRotation.y*1.2-0.2)) * _Color.a;
				col.a = 1;
				return col;
			}
			ENDCG
		}
	}
}
