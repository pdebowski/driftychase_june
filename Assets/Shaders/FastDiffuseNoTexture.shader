Shader "Custom/FastDiffuseNoTexture	" {
	Properties{
		_Color("Main Color", Color) = (1,1,1,1)
	}
		SubShader{
		Tags{ "RenderType" = "Opaque" }
		LOD 200

		CGPROGRAM
#pragma surface surf Lambert noforwardadd noshadow nolightmap nofog interpolateview halfasview

	fixed4 _Color;

	struct Input {
		float2 vertex;
	};

	void surf(Input IN, inout SurfaceOutput o) {
		fixed4 c =  _Color;
		o.Albedo = c.rgb;
		o.Alpha = c.a;
	}
	ENDCG
	}

		Fallback "Legacy Shaders/Diffuse"
}
