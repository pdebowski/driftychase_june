// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/Billboard" {
	Properties{
		_LogoTexture("Logo Texture", 2D) = "white" {}
		_LogoColor("Logo Color", Color) = (1,1,1,1)
		_BackgroundColor("Background Color", Color) = (0,0,0,1)
		_ScaleX("ScaleX",Float) = 1
		_ScaleY("ScaleY",Float) = 1
	}

		SubShader{
		Tags{ "RenderType" = "Opaque" }
		LOD 100

		Pass{
		CGPROGRAM
#pragma vertex vert
#pragma fragment frag

#include "UnityCG.cginc"

		struct appdata_t {
			float4 vertex : POSITION;
			float2 texcoord : TEXCOORD0;
		};

		struct v2f {
			float4 vertex : SV_POSITION;
			half2 texcoord : TEXCOORD0;
		};

		sampler2D _LogoTexture;
		float4 _LogoTexture_ST;
		half4 _LogoColor;
		half4 _BackgroundColor;
		float _ScaleX;
		float _ScaleY;

		v2f vert(appdata_t v)
		{
			v2f o;
			o.vertex = UnityObjectToClipPos(v.vertex);
			o.texcoord = TRANSFORM_TEX(v.texcoord, _LogoTexture);
			return o;
		}

		fixed4 frag(v2f i) : SV_Target
		{
			float2 coord = i.texcoord;
			coord -= 0.5;
			coord.x *= 1/_ScaleX;
			coord.y *= 1 /_ScaleY;
			coord += 0.5;

			half4 col = tex2D(_LogoTexture,coord);

			col = _LogoColor * col.a + _BackgroundColor * (1 - col.a);

			return col;
		}
			ENDCG
		}
		}

}
