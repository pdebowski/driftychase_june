
inline half4 ApplySinCityEffect(half4 color) {
	half4 ret = color;

	half ifgbLessR = max(0,sign(color.r-(color.g + color.b)));
	half ifgbElse = (1 - ifgbLessR);
	
	ret.rgb = (ifgbLessR * half3(color.r, 0, 0)) + (ifgbElse * dot(color.rgb, color.rgb) * dot(color.rgb, half4(2, 1, 2, 1)));
	
	/*
	if ((color.g + color.b) < color.r)
	{
		ret.r = color.r;
		ret.g = 0;
		ret.b = 0;

	}
	else
	{
		ret.rgb = dot(color.rgb, color.rgb) * dot(color.rgb, half4(2, 1, 2, 1));
	}*/
	ret.a = color.a;
	return ret;

}

inline half4 ApplySephia(half4 color,half factor)
{
	half4 sepiaConvert = half4 (0.191, -0.054, -0.221, 0.0)*factor ;
	half4 ret = dot(half3(0.299, 0.587, 0.114),color) + sepiaConvert;
//	ret *= factor;
	ret.a = color.a;
	return ret;
}

//NO Alpha
inline half3 ApplyBright(half3 rgb, half3 val) 
{
	rgb += val;
	return rgb;
}

//NO Alpha
inline half3 ApplyContrastVals(half3 rgb, half val)
{
	rgb = (rgb - half3(0.5, 0.5, 0.5) * half3(val, val, val)) + half3(0.5, 0.5, 0.5);
	return rgb;
}

//NO Alpha
inline half3 ApplyContrastChannels(half3 rgb, half3 val)
{
	rgb = (rgb - half3(0.5, 0.5, 0.5) * val) + half3(0.5, 0.5, 0.5);
	return rgb;
}
